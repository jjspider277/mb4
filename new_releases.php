<?php
require 'VCGcurl.php';
require 'VCGskyhook.php';
require "VCGgooglegeo.php";
require "VCGtmdb.php";
ini_set("display_errors", "On");
	if(file_exists('movie_log.txt')){unlink('movie_log.txt');}
	function logit($logarray, $class=null, $function=null){
		if(!isset($_REQUEST['debug'])){return false;}
		$filename = 'movielog.txt';
		$handle = fopen('movie_log.txt','a+');
		$time = date('Y-m-d H:i:s');
		$ip = $_SERVER['REMOTE_ADDR'];
		$content = '';
		$firstline = $time.' '.$ip.' ';
		if(!is_null($class)){$firstline .= 'Class : '.$class.' ';}
		if(!is_null($function)){$firstline .= 'Function : '.$function.' ';}
		
			if(!file_exists($filename)){
				$initialHandle = fopen($filename,'w');
				$initial = $time.' '.$ip.' START LOGGING ------------------------------------>'."/n";
				fwrite($initialHandle,$initial);
				fclose($initialHandle);
				chmod($filename,0644);				
			}
			$content .= $firstline."\n";
			if(!is_array($logarray)){
				$content .="\t\t".$logarray."\n";					
			}else{
				foreach($logarray as $k1=>$v1){
					$content .= "\t\t".'Key Level 1: '.$k1."\n";
					if(!is_array($v1)){
						$content .= "\t\t\t".'Value = '.$v1."\n";
					}else{
						foreach($v1 as $k2=>$v2){
							$content .= "\t\t\t".'Key Level 2: '.$k2."\n";
							if(!is_array($v2)){
								$content .= "\t\t\t\t".'Value = '.$v2."\n";
							}else{
								foreach($v2 as $k3=>$v3){
									$content .= "\t\t\t\t\t".'Key Level 3: '.$k3."\n";
									if(!is_array($v3)){
										$content .= "\t\t\t\t\t\t".'Value = '.$v3."\n";
									}else{
										foreach($v3 as $k4=>$v4){
											$content .= "\t\t\t\t\t\t".'Key Level 4: '.$k4."\n";
											if(!is_array($v4)){
												$content .= "\t\t\t\t\t\t\t".'Value = '.$v4."\n";
											}else{
												foreach($v4 as $k5=>$v5){
													$content .= "\t\t\t\t\t\t\t".'Key Level 4: '.$k5."\n";
													if(!is_array($v5)){
														$content .= "\t\t\t\t\t\t\t\t\t".'Value = '.$v5."\n";
													}else{
														foreach($v5 as $k6=>$v6){
															$content .= "\t\t\t\t\t\t\t\t".'Key Level 4: '.$k6."\n";
															if(!is_array($v6)){
																$content .= "\t\t\t\t\t\t\t\t\t\t\t".'Value = '.$v6."\n";													
															}							
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			fwrite($handle,$content);
			fclose($handle);
	}
	
	function get_zipcode(){
		$skyhook = new VCG\skyhook();
		$data = $skyhook->skyhook_geolocation_ip($_SERVER['REMOTE_ADDR']);
		$googledata = new VCG\googleGeo();
		$google = $googledata->latlng_to_zip($data['latitude'], $data['longitude']);
		logit($google,__CLASS__,__FUNCTION__);
		return $google['zipcode'];
	}
	
	function get_tmdb_new_releases(){
		$tmdb = new VCG\tmdb();
		$now_playing = $tmdb->get_now_playing();
		logit($now_playing,__CLASS__,__FUNCTION__);
		return $now_playing;
	}
	
	
	function encodeToIso($string) {
		return mb_convert_encoding($string, "ISO-8859-1", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
	}
	
	function get_cached_image_links(){
		$file = 'cached_movie_links.txt';
		$handle = fopen($file,'a+');
		$data = array();
		if(!file_exists('cached_movie_links.txt')){
			fwrite($handle,'movie name'."\t".'poster_path'."\t".'release_date'."\n");
			fclose($handle);
			chmod($file,0644);
		}
		while (($row = fgetcsv($handle, 1000, "\t")) !== FALSE){
			$data[$row[0]]['poster_path']= $row[1];
			$data[$row[0]]['release_date']= $row[2];
			$data[$row[0]]['overview']= $row[3];
		}
		logit($data,__CLASS__,__FUNCTION__);
		return $data;		
	}
	
	function write_to_image_cache($vars){
		$file = 'cached_movie_links.txt';
		$handle = fopen($file,'a+');
		$data = array();
		if(!file_exists('cached_movie_links.txt')){
			fwrite($handle,'movie name'."\t".'poster_path'."\t".'release_date'."\t".'overview'."\n");
			fclose($handle);
			chmod($file,0644);
		}
		fwrite($handle,$vars['name']."\t".$vars['tmdb_data']['poster_link']."\t".$vars['tmdb_data']['release_date']."\t".$vars['tmdb_data']['overview']."\n");
		fclose($handle);
	}

	
	
	function parse_xml($xml_data,$limit=null){
		$themovies = array();
		$encoding = mb_detect_encoding($xml_data);
		if($encoding == 'UTF-8'){
			$xml_data = encodeToIso($xml_data);
		}
		$xml = simplexml_load_string($xml_data,'SimpleXMLElement',LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array1 = json_decode($json,TRUE);
		$channel  = $array1['channel'];
		$countitems = count($channel['item']);
		$countT = 0;
		foreach($channel['item'] as $num=>$wholeitem){
			if($countT>4){continue;}
			//if($countitems === 1){
			//	$wholeitem = $channel['item'];
			//}else{
		//		$wholeitem = $channel['item'][$i];
		//	}
			$theater = array();
			$theater['name'] = $wholeitem['title'];
			$d =mb_convert_encoding($wholeitem['description'], 'HTML-ENTITIES', "UTF-8"); 
			$cleanpre = str_replace('<br>',null,str_replace('</p>',null,str_replace('</ul>',null,str_replace('<ul>',null,str_replace('</p>',null,str_replace('<p>',null,str_replace('</a>',null,str_replace('">','|',str_replace('<a href="',null,$d)))))))));
			$removestart = str_replace("<li>",null,substr($cleanpre,strpos($cleanpre,"<li>")));
			$lis = explode("</li>",$removestart);
			$movies = array();
			foreach($lis as $linum=>$li){
				$parts = explode('|',$li);
				if(strpos($parts[1],'(')){
					$len = strlen($parts[1]);
					$parts[1] = substr($parts[1],0,-($len-(strpos($parts[1],'('))));
				}
				if(strpos($parts[1]," presented by")){
					$len = strlen($parts[1]);
					$parts[1] = substr($parts[1],0,-($len-strpos($parts[1],'(')));
					
				}
				$parts[1] = str_replace("'",null,str_replace(':',null,trim($parts[1])));
				if($parts[1] == "Search other theaters"){continue;}
				if(strpos(strtolower($parts[1]),'3d')){continue;}
				$parts['name'] = $parts[1];
				$parts['link'] = $parts[0];
				$imageCache =  get_cached_image_links();
				if(isset($imageCache[$parts['name']])){
					$parts['tmdb_data']['poster_link'] = $imageCache[$parts['name']]['poster_path'];					
					$parts['tmdb_data']['release_date'] = $imageCache[$parts['name']]['release_date'];					
					$parts['tmdb_data']['overview'] = $imageCache[$parts['name']]['overview'];					
				}else{
						$gettmdb = new VCG\tmdb();
						$tmdbData = $gettmdb->search_movie($parts[1]);
						if(!$tmdbData){continue;}
						if(!$tmdbData['response']['results'][0]['poster_path']){
							continue;
						}else{
							$parts['tmdb_data']['poster_link'] ='https://image.tmdb.org/t/p/w300'.$tmdbData['response']['results'][0]['poster_path'];
						}
						if(!$tmdbData['response']['results'][0]['release_date']){
							continue;
						}else{
							$parts['tmdb_data']['release_date'] =$tmdbData['response']['results'][0]['release_date'];
						}
						if(!$tmdbData['response']['results'][0]['overview']){
							$parts['tmdb_data']['overview'] = '';
						}else{
							$parts['tmdb_data']['overview'] = htmlentities($tmdbData['response']['results'][0]['overview']);
						}
						write_to_image_cache($parts);
					}
					$threemonths = strtotime("-90 day");
					$today = strtotime(date('Y-m-d'));
					$release = strtotime($parts['tmdb_data']['release_date']);
					if($release > $today || $release < $threemonths){continue;}
					$movies[]=$parts;
				}
			shuffle($movies);
			$theater['movies'] = $movies;
			$themovies[] = $theater;
			if(!is_null($limit)){return $themovies;}
			$countT++;
			}
			
		/*foreach($array1['channel']['item'] as $itemnum=>$item){
			$themovies[$item['title']] = array();
			$thedescription =$item['description'];
			$cleanpre = substr($thedescription,strpos($thedescription,'<li>'));
			$cleanpost = substr($cleanpre,0,strpos($cleanpre,'<ul>'));
			
			
		}
		echo'<br><br><br>';
		var_dump($array1);
		echo'<br><br><br>';
	$list = array();
		$array = simplexml_load_string($xml_data,'SimpleXMLElement',LIBXML_NOCDATA);
		foreach($array->channel->item as $item_data){
			$theater = array();
			$descount  - $item_data->description->count();
			$theater['name'] = (string) $item_data->title[0];
			$description = (string) $item_data->description[0];
			$liposition = strpos($description,'<li>');
			$justlis = substr($description,$liposition);
			$justlis = substr($justlis,0,strpos($justlis,'</ul>'));
			$lis = explode('<li>',$justlis);
			$movielist = array();
			foreach($lis as $li){
				$movie = array();
				$chopname = substr($li,(strpos($li,'">')+2));
				$name = substr($chopname,0,strpos($li,'</a>'));
				$movie['name'] = trim($name);
				if($movie['name'] == ''){continue;}
				$getimage = '';
				$gettmdb = new VCG\tmdb();
				$tmdbData = $gettmdb->search_movie($movie['name']);
				$movie['poster'] ='https://image.tmdb.org/t/p/w200'.$tmdbData['poster_path'];
				$choplink = substr($li,(strpos($li,'"')+1));
				$movie['link'] = substr($choplink,0,strpos($choplink,'"'));
				$idnumstart = substr($movie['link'],(strpos($movie['link'],'_')+1));
				$idnumend = substr($idnumstart,0,strpos($idnumstart,'/'));
				$movie['idnum'] = $idnumend;*/
				//$movie['idnum'] = substr($movie['link'],($idnumstart+1),intval($idnumend));
						//$docs = simplexml_load_string($description'SimpleXMLElement',LIBXML_NOCDATA);
			//var_dump($docs);
		/*	$doc = new DOMDocument();
			libxml_use_internal_errors(true);
			$description = mb_convert_encoding($description, 'HTML-ENTITIES', "UTF-8");

			$doc->loadHTML($description,LIBXML_NOCDATA);
			$lis = $doc->getElementsByTagName('li');
			foreach($lis as $li){
				$movie = array();
				$movie['name'] = (string) $li->textContent;
				$as = $li->getElementsByTagName('a');
				foreach($as as $a){
				$movie['link'] = (string) $a->getAttribute('href');
					$idnumstart = strpos($movie['link'],'_');
					$idnumend = strpos(substr($movie['link'],($idnumstart+1)),'/');
					$movie['idnum'] = substr($movie['link'],($idnumstart+1),intval($idnumend));
				}*/
			//	$movielist[] = $movie;
		//	}
		//		$theater['movies'] = $movielist;
		//		$list[] = $theater;
		//	}
			//shuffle($theater['movies']);
			
		//}
		logit($themovies,__CLASS__,__FUNCTION__);
		return $themovies;
	}
	
	
	function check_tmdb_data($ffname,$tmdb){
		$match = false;
		foreach($tmdb as $movienum=>$movie_data){
			print_r($movie_data); echo $ffname;
			if($ffname != $movie_data['formatted_name']){continue;}
			if($ffname == $movie_data['formatted_name']){				
				$match = $movie_data;				
			}
		}
		logit($match,__CLASS__,__FUNCTION__);
		return $match;
	}
	
	function compare_movies($fandango,$tmdb){
		$display_movies = $fandango;
		foreach($fandango as $theaters=>$theater){
			foreach($theater['movies'] as $movies=>$movie){
				$preparefandangoname = str_replace(date('Y'),null,str_replace('3d',null,str_replace('-',null,str_replace(',',null,str_replace("'",null,str_replace('&',null,str_replace('(',null,str_replace(')',null,str_replace(':',null,str_replace('-',null,str_replace(' ',null,strtolower($movie['name']))))))))))));
				$tmdb_data = check_tmdb_data($preparefandangoname,$tmdb);
				if(!$tmdb_data){unset($fandango[$theaters]['movies'][$movies]); continue;}
				else{
					$fandango[$theaters]['movies'][$movies]['tmdb_data'] = $tmdb_data;				
				}
			}
		}
		logit($fandango,__CLASS__,__FUNCTION__);
		return $fandango;		
	}
	
	function curl_it_rss($url){
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$data = curl_exec($ch);
		if(curl_errno($ch)){
			print_r(curl_error($ch));
		}
		curl_close($ch);
		logit($data,__CLASS__,__FUNCTION__);
		return $data;	
	}
	
	function get_fandango_rss($zip){
		$result = array();
		$fandango_rss = 'http://www.fandango.com/rss/moviesnearme_'.$zip.'.rss';
		logit($fandango_rss,__CLASS__,__FUNCTION__);
		$result = curl_it_rss($fandango_rss);
		logit($result,__CLASS__,__FUNCTION__);
		return $result;	
	}
	
	function get_city($zip){
		$apikey = '30aGKgjEwl3B16HbOYM2xwLmHYbqkY0oZa4KrooYKszLzhWngJpNfbaXalnuknSj';
		$url = 'https://www.zipcodeapi.com/rest/'.$apikey.'/info.json/'.$zip.'/radians';
		$curl = new VCG\curl();
		$curl->url($url);
		$curl->returntransfer();
		$curl->json_convert();
		$result = $curl->curl_it();
		if(isset($result['response']['city'])){
			return $result['response']['city'];			
		}else{ return false;}		
	}

	function get_new_release_movies($preview = null){
		if(!is_null($preview)){
			$zipcode = get_zipcode();
		}else{
			$zipcode = '';
			if(!isset($_POST['zip'])){
				$zipcode = get_zipcode();
				//$zipcode = get_zipcode();
			}else{
				$zipcode = $_POST['zip'];	
			}
			if(!$zipcode||$zipcode == ''){
				$zipcode = '89112';
			}
		}
		$fandango_data = get_fandango_rss($zipcode);	
		if(strlen($fandango_data)<4000){
			$fandango_data = get_fandango_rss('89117');	
		}
		if(!is_null($preview)){$movie_data  = parse_xml($fandango_data,5);}
		else{$movie_data  = parse_xml($fandango_data);}
		$movie_data['city'] = get_city($zipcode);
		//$tmdb_data = get_tmdb_new_releases();
		//$movie_data = compare_movies($fandango_parsed,$tmdb_data);
		logit($movie_data,__CLASS__,__FUNCTION__);
		return $movie_data;
	}
