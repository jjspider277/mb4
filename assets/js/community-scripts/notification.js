$('.community_top_bar').click(function(e) {
 e.preventDefault();
 var clicked = $(this).attr('href');
 //write logic accordingly for the urls
  //write logic accordingly for the urls
 var url = base_url_complete+'notification/update_notification_seen_status';
 var param = '';  
 var showMsg;
 if(clicked =='#notifications') {
   param = 'notification';
   showMsg = $("#notification_badge_notification");
 }
 if(clicked =='#friend_request') {
   param = 'friend_request';
   showMsg = $("#frequest_badge_notification");
 }
  if(clicked =='#messages') {
   param = 'message';
   showMsg = $("#message_badge_notification");
 }
  if(clicked =='#likes') {
   param = 'like';
   showMsg = $("#like_badge_notification");
 }
 
 
 //now send the status change signal to the database update
  $.ajax({
                type:"POST",
                url:url,
                data:{madebyus4u_csrf_test_name:csrf_token_hash,'ntype':param},
                dataType: "json",
                cache:false,
                success:
                    function(data){
                 
                             // document.getElementById("friend_request_btn").setAttribute("data-status",'SENT'); 
                             console.log('Notifications are cleared !');
                             showMsg.text(0);                   
                         
                    },
                error:
                    function(data) {
                      alert('Error : When Tyring to clean Notifications - Please try again!');
                }
   
        });//end of ajax block  
});


//delete records notification
//this for removing records line by line
$(function(){
   $('a.remove-notification').on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      var id = $this.data('id');

     $url = base_url_complete+'notification/change_status';
      $.ajax({
          url: $url,
          data: ({'madebyus4u_csrf_test_name':csrf_hash,'id': id}),
          dataType: 'json', 
          type: "post",
          success: function(data){
                     //response = jQuery.parseJSON(data); 
                     //TODOD:CHECK RESULT                      
                      $(".seen-message").css('display','block');
                      $(".seen-message").fadeIn(2000).fadeOut(2000);
                      $this.parent().parent().parent().fadeOut(300, function(){$(this).remove();});
                   }             
      });

    
           
   });
});


//thenew
$(function(){
    $('a.remove-notification-li').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');

        $url = base_url_complete+'notification/change_status';
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'id': id}),
            dataType: 'json',
            type: "post",
            success: function(data){
                //response = jQuery.parseJSON(data);
                //TODOD:CHECK RESULT
                $(".seen-message").css('display','block');
                $(".seen-message").fadeIn(2000).fadeOut(2000);
                $this.parent().parent().fadeOut(300, function(){$(this).remove();});
            }
        });



    });
});
/**see all button ***/
$(function(){
    $('.see_all_notifications').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        location.href=base_url_complete+"sell/seller/"+id+"#notifications";
        $("#notificationContainer").css('display','none');
        activateTab('notifications');

    });

    $('.see_all_notifications_friends').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        location.href=base_url_complete+"sell/seller/"+id+"#friend_request";
        $("#notificationContainerFriends").css('display','none');
        activateTab('friends');

    });

    $('.see_all_notifications_messages').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        location.href=base_url_complete+"sell/seller/"+id+"#messages";
        $("#notificationContainerMessage").css('display','none');
        activateTab('messages');

    });

    $('.see_all_notifications_likes').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        location.href=base_url_complete+"sell/seller/"+id+"#likes";
        $("#notificationContainerLikes").css('display','none');
        activateTab('likes');

    });


});

function activateTab(tab){

    $('#tabs li').each(function() {
        $(this).removeClass('active');
        $(this).removeClass('selected');
    });

    $('#sellersTabContent div').each(function() {
        $(this).removeClass('active');
        $(this).removeClass('selected');
    });
    $('#' + tab).addClass('active');
    $('#' + tab).addClass('selected');

    $("#notificationContainer").css('display','none');
    $("#notificationContainerFriends").css('display','none');
    $("#notificationContainerMessages").css('display','none');
    $("#notificationContainerLikes").css('display','none');
};
$(document).ready(function()
{
    //alert('Hello');
    $("#notificationLink").click(function()
    {
        $("#notificationContainer").css('display','block');
        $("#notificationContainerFriends").css('display','none');
        $("#notificationContainerMessages").css('display','none');
        $("#notificationContainerLikes").css('display','none');


        $("#notification_count").fadeOut("slow");
        return false;
    });

//Document Click
    $(document).click(function()
    {
        $("#notificationContainer").hide();
        $("#notificationContainerMessages").hide();
        $("#notificationContainerFriends").hide();
        $("#notificationContainerLikes").hide();
        console.log('classic');
    });
//Popup Click
    $("#notificationContainer").click(function()
    {
        return false
    });

    //FRIENDS('Hello');
    $("#notificationFriendsLink").click(function()
    {
        $("#notificationContainerFriends").css('display','block');
        $("#notificationContainer").css('display','none');
        $("#notificationContainerMessages").css('display','none');
        $("#notificationContainerLikes").css('display','none');

        $("#notification_count").fadeOut("slow");
        return false;
    });


//Popup Click
    $("#notificationContainerFriends").click(function()
    {
        return false
    });

    //Messages('Hello');
    $("#notificationMessagesLink").click(function()
    {
        $("#notificationContainerMessages").css('display','block');
        $("#notificationContainerFriends").css('display','none');
        $("#notificationContainer").css('display','none');
        $("#notificationContainerLikes").css('display','none');

        $("#notification_count").fadeOut("slow");
        return false;
    });


//Popup Click
    $("#notificationContainerMessages").click(function()
    {
        return false
    });


    //Likes('Hello');
    $("#notificationLikesLink").click(function()
    {
        $("#notificationContainerLikes").css('display','block');
        $("#notificationContainerFriends").css('display','none');
        $("#notificationContainerMessages").css('display','none');
        $("#notificationContainer").css('display','none');

        $("#notification_count").fadeOut("slow");
        return false;
    });



//Popup Click
    $("#notificationContainerLikes").click(function()
    {
        return false
    });




});
//emd of deletion