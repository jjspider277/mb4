//send messaging
$(".send_message").click(function () {
    //alert('hello');
    $url = base_url_complete + 'message/send_message';
    var reciever_profile_id = "";
    if ($("#selected_member_from_top_location").val() != "#")
        reciever_profile_id = $("#selected_member_from_top_location").val();
    if ($("#selected_member").val() != "#")
        reciever_profile_id = $("#selected_member").val();

    //var memeber_name = $("#selected_member option:selected").text();
    var subject = $("#subject").val();
    var message_text = "";
    if ($("textarea#message_text2").val() != "")
        message_text = $("textarea#message_text2").val();
    if ($("textarea#message_text").val() != "")
        message_text = $("textarea#message_text").val();
    $.ajax({
        url: $url,
        data: ({
            'madebyus4u_csrf_test_name': csrf_hash,
            'reciever_profile_id': reciever_profile_id,
            'subject': subject,
            'message_text': message_text
        }),
        dataType: 'json',
        type: "post",
        success: function (data) {
            //response = jQuery.parseJSON(data);
            $(".sent-message").css('display', 'block');
            //TODO:save it on notification table
            if ($("textarea#message_text2").val() != "")
                $("textarea#message_text2").val("");
            if ($("textarea#message_text").val() != "")
                $("textarea#message_text").val("");

            // socket.emit('message_sent',{"profile_id":reciever_profile_id,"message": "You have sent message to "+memeber_name});
        }
    });

});

//this for removing records line by line
$(function () {
    $('a.remove').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        $url = base_url_complete + 'message/delete_message';
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name': csrf_hash, 'id': id}),
            dataType: 'json',
            type: "post",
            success: function (data) {
                //response = jQuery.parseJSON(data);
                //TODOD:CHECK RESULT
                $(".trash-message").css('display', 'block');
                $(".trash-message").fadeIn(2000).fadeOut(2000);
                $this.parent().parent().parent().fadeOut(300, function () {
                    $(this).remove();
                });
            }
        });


    });
});


//permanatrly delete messages

$(function () {
    $('a.delete').on('click', function (e) {
        e.preventDefault();

        var result = confirm("Are you sure you want to delete permanently ?");

        // alert(baseurl+pathArray[0]);

        if (result == true) {

            var $this = $(this);
            var id = $this.data('id');

            $url = base_url_complete + 'message/permanent_delete_message';

            // alert($url);
            $.ajax({
                url: $url,
                data: ({'madebyus4u_csrf_test_name': csrf_hash, 'id': id}),
                dataType: 'json',
                type: "post",
                success: function (data) {
                    //response = jQuery.parseJSON(data);
                    //TODOD:CHECK RESULT

                    $(".deleted-message").css('display', 'block');
                    $(".deleted-message").fadeIn(2000).fadeOut(2000);
                    $this.parent().parent().parent().fadeOut(300, function () {
                        $(this).remove();
                    });
                }
            });

        } //end if


    });
});

//end of deletion
function get_message(message_id) {

    var message_data = null;
    $url = base_url_complete + 'message/get_message';
    // alert($url);
    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name': csrf_hash, 'message_id': message_id}),
        dataType: 'json',
        type: "post",
        success: function (data) {
            //response = jQuery.parseJSON(data);
            //TODOD:CHECK RESULT
            message_data = data.message;
            //alert('returning' + message_data);
            return message_data;
        }
    });

}

///show full message script
//triggered when modal is about to be shown
$('.interact_lablel').on('click', function (e) {

    e.preventDefault();
    //get data-id attribute of the clicked element
    var message_id = $(this).data('message-id');

    //alert(message_id);

    var message_data = null;

    $url = base_url_complete + 'message/get_message';
    // alert($url);
    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name': csrf_hash, 'message_id': message_id}),
        dataType: 'json',
        type: "post",
        success: function (data) {
            //response = jQuery.parseJSON(data);
            //TODOD:CHECK RESULT
            message_data = data.message;
            //alert('returning'+message_data);
            $('textarea[id="message-text"]').val(message_data);

            $('#interact_with_message_modal').modal({
                show: 'false'
            });

            // /return message_data;
        }
    });


    //$('#interact_with_message_modal').show();

    //populate the textbox
    // $('message-text').val(message_data);


});

var timer;
var flag_initiial = "default"; //simply to count if it is same auto save message or diffrent load
var cur_counter = -1; //simply to count if it is same auto save message or diffrent load


var auto_save_messages = function autosave() {

    $message_text = $('textarea#message_text2').val();

    if ($message_text != '' && flag_initiial != "paused") {

        jQuery("#compose_form").each(function () {
            jQuery.ajax({
                url: base_url_complete + 'message/auto_save',
                data: jQuery(this).serialize(),
                type: 'POST',
                success: function (data) {
                    if ((JSON.parse(data)).success == true) {
                        var time = new Date().getDate();
                        $(".draft-message").fadeIn(200);
                        $("#msg").text("Saving your draft message if not Sent ");
                        $(".draft-message").fadeOut(1000);
                        // alert('last message id'+(JSON.parse(data)).message_id);
                        $("#last_saved_message_id").val((JSON.parse(data)).message_id);

                        flag_initiial = "paused";

                    } else {
                        console.log((JSON.parse(data)).message);
                        flag_initiial = "paused";
                    }
                }
            });
        });
    } //eslse just kep on checking on
}
$(document).ready(function () {
    //setTimeout(fun, 500);
    //alert('fuck yall');
    //$("#message_text2").chan
});

$("#message_text2").focusout(function () {
    console.log("Guessing a new message draft :)");
    flag_initiial = "message_area";
    //auto_save_messages();
});
$("#message_text").change(function () {
    console.log("Guessing a new message draft :)");
    flag_initiial = "message_area";
    auto_save_messages();
});

$("#message_text2").change(function () {
    console.log("Guessing a new message draft :)");
    flag_initiial = "message_area";
    auto_save_messages();
});


$('#composeNew').on('shown.bs.modal', function () {
    // will only come inside after the modal is shown
    // setInterval(auto_save_messages,1000);
    console.log('Auto save is started!');
    $(".draft-message").fadeOut(3000);
});
$('#composeNew').on('hidden.bs.modal', function () {
    // will only come inside after the modal is shown
    //clearInterval(auto_save_messages);
    console.log('Auto save is stopped!');
});

//change picture when memeber selected from drpdown
$('#selected_member_from_top_location').on('change', function() {

    console.log(this.value);

    $url = base_url_complete+"message/get_profile_image";

    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name':csrf_hash,'profile_id': this.value}),
        dataType: 'json',
        type: "post",
        success: function(data){
            response = data;
            var img1= document.getElementById('memeber_image_top_location').src;
            $("#memeber_image_top_location").attr('src', response.toString());
            //$(".alert-success").css('display','block');
        }
    });

});

//change picture when memeber selected from drpdown
$('#selected_member').on('change', function() {

    console.log(this.value);

    $url = base_url_complete+"message/get_profile_image";

    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name':csrf_hash,'profile_id': this.value}),
        dataType: 'json',
        type: "post",
        success: function(data){
            response = data;
            var img1= document.getElementById('memeber_image').src;
            $("#memeber_image").attr('src', response.toString());
            //$(".alert-success").css('display','block');
        }
    });

});


$("a.save_as_important").on('click',function(){

    $url = base_url_complete+"message/set_message_as_important";
    $message_id = document.getElementById((this).id).getAttribute("data-message-id");
    console.log("message_id"+$message_id);
    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name':csrf_hash,'message_id': $message_id}),
        dataType: 'json',
        type: "post",
        success: function(data){
            console.log("success");
            $(".important-message").css('display', 'block');
        }
    });
});

$("a.unset_message_important").on('click',function(){
    var $this = $(this);
    $url = base_url_complete+"message/unset_message_as_important";
    $message_id = document.getElementById((this).id).getAttribute("data-message-id");
    console.log("message_id"+$message_id);
    $.ajax({
        url: $url,
        data: ({'madebyus4u_csrf_test_name':csrf_hash,'message_id': $message_id}),
        dataType: 'json',
        type: "post",
        success: function(data){
            console.log("success");
            //$(".unset-important-message").css('display', 'block');
            $this.parent().parent().parent().fadeOut(300, function () {
                $(this).remove();
            });
        }
    });
});

