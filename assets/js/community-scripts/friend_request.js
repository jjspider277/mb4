//delete records freind requests
//this for removing records line by line
$(function () {

    $('a.remove-request').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var friend_id = $this.data('id');

        $url = base_url_complete + 'friend/change_request_status';

        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name': csrf_hash, 'friend_id': friend_id}),
            dataType: 'json',
            type: "post",
            success: function (data) {
                //response = jQuery.parseJSON(data);
                //TODOD:CHECK RESULT
                $(".friend_request_message").css('display', 'block');
                $(".friend_request_message").fadeIn(2000).fadeOut(2000);
                $this.parent().parent().parent().fadeOut(300, function () {
                    $(this).remove();
                });
            }
        });

    });
});

$('#friend_request_btn').on('click', function (event) {

    event.preventDefault();


    //get if request or cancel request

    status = document.getElementById((this).id).getAttribute("data-status");
    //alert(status);
    var server_url = base_url_complete + "friend/send_friend_request";

    var reciever_id = document.getElementById((this).id).getAttribute("data-reciever-id");


    swal({
            title: "Are you sure you want to do this ?",
            text: "deal with the friend request",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },

        function () {

            setTimeout(function () {


                if (status == "SENT") {
                    server_url = base_url_complete + 'friend/cancel_friend_request';
                }

                //if validation is OK! then call ajax
                $.ajax({
                    type: "POST",
                    url: server_url,
                    data: {madebyus4u_csrf_test_name: csrf_token_hash, 'reciever_id': reciever_id},
                    dataType: "json",
                    cache: false,
                    success: function (data) {

                        if (data.success == true) {

                            if (status == "SENT") {

                                $('#friend_request_btn').fadeOut("slow");
                                $('#friend_request_btn').fadeIn("slow");
                                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span>Send Friend Request');
                                //sent back to cance request status
                                document.getElementById("friend_request_btn").setAttribute("data-status", 'REQUEST');
                               window.location.reload(true);

                            }

                            if (status == "REQUEST") {

                                $('#friend_request_btn').fadeOut("slow");
                                $('#friend_request_btn').fadeIn("slow");
                                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Cancle Friend Request');
                                //sent back to send request status
                                document.getElementById("friend_request_btn").setAttribute("data-status", 'SENT');
                                window.location.reload(true);

                            }

                        }
                        window.location.reload(true);

                    },
                    error: function (data) {

                        if (data.error_message) {

                            $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Unable to send Friend Request').delay(2000);
                            $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Send Friend Request').delay(2000);

                        }
                    }


                });//end of ajax block

                swal("action finished!");

            }, 2500);
        });


});


$('#friend_accept_btn').on('click', function (event) {

    event.preventDefault();

    //get if request or cancel request
    var $t = $(this); //get this conetxt
    // alert('accept btn');
    var server_url = base_url_complete + "friend/accept_friend_request_sellers";

    var sender_profile_id = document.getElementById((this).id).getAttribute("data-sender-id");
    // alert(sender_profile_id);

    //if validation is OK! then call ajax
    $.ajax({
        type: "POST",
        url: server_url,
        data: {madebyus4u_csrf_test_name: csrf_token_hash, 'sender_profile_id': sender_profile_id},
        dataType: "json",
        cache: false,
        success: function (data) {

            if (data.success == true) {

                // document.getElementById("friend_request_btn").setAttribute("data-status",'SENT');

                $('#friend_accept_btn').fadeOut("slow");
                $('#friend_accept_btn').fadeIn("slow");
                $('#friend_accept_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Block');
                //sent back to send request status
                document.getElementById("friend_request_btn").setAttribute("data-status", 'none');
                //$t.parent().parent().fadeOut(300, function(){$(this).remove();});

            }

        },
        error: function (data) {

            if (data.error_message) {

                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Unable to accept a Friend Request').delay(2000);
                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Send Friend Request').delay(2000);

            }
        }


    });//end of ajax block

});


//accept button 

$('a.accept_btn').on('click', function (event) {

    event.preventDefault();

    //get if request or cancel request
    var $t = $(this); //get this conetxt
    //alert('accept btn');
    var server_url = base_url_complete + "friend/accept_friend_request";

    var friend_id = $(this).attr('id');
    // alert(friend_id);

    //if validation is OK! then call ajax
    $.ajax({
        type: "POST",
        url: server_url,
        data: {madebyus4u_csrf_test_name: csrf_token_hash, 'friend_id': friend_id},
        dataType: "json",
        cache: false,
        success: function (data) {

            if (data.success == true) {

                // document.getElementById("friend_request_btn").setAttribute("data-status",'SENT');

                $(".friend_request_message").css('display', 'block');
                $(".friend_request_message").html("Friend Request Accepted");
                $(".friend_request_message").fadeIn(2000).fadeOut(2000);
                $t.parent().parent().fadeOut(300, function () {
                    $(this).remove();
                });

            }

        },
        error: function (data) {

            if (data.error_message) {

                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Unable to send Friend Request').delay(2000);
                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Send Friend Request').delay(2000);

            }
        }


    });//end of ajax block

});

//deny button 

$('a.deny_btn').on('click', function (event) {

    event.preventDefault();

    //get if request or cancel request
    var $t = $(this); //get this conetxt
    //alert('accept btn');
    var server_url = base_url_complete + "friend/deny_friend_request";

    var friend_id = $(this).attr('id');
    // alert(friend_id);

    //if validation is OK! then call ajax
    $.ajax({
        type: "POST",
        url: server_url,
        data: {madebyus4u_csrf_test_name: csrf_token_hash, 'friend_id': friend_id},
        dataType: "json",
        cache: false,
        success: function (data) {

            if (data.success == true) {

                // document.getElementById("friend_request_btn").setAttribute("data-status",'SENT');

                $(".friend_request_message").css('display', 'block');
                //$(".friend_request_message").css('display','block');
                $(".friend_request_message").html("Friend Request Denied!");
                $(".friend_request_message").fadeIn(2000).fadeOut(2000);
                $t.parent().parent().fadeOut(300, function () {
                    $(this).remove();
                });

            }

        },
        error: function (data) {

            if (data.error_message) {

                $('#friend_request_btn').html(' <span class="friends-white span-right-padding" style="padding-left:30px;"></span> Unable to send Friend Request').delay(2000);
                $('#friend_request_btn').html('<span class="friends-white span-right-padding" style="padding-left:30px;"></span> Send Friend Request').delay(2000);

            }
        }


    });//end of ajax block

});
