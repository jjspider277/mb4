$('#like_link_btn').on('click', function(event) {

   //get the product id
   //check if current user has liked before or not
   //send ajax
  /***global base url path in javascript***/
 
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split( '/' );
  var base_url_complete =base_url+'/'+pathArray[1]+'/';

/***gloabl base_url path_end***/
   //alert('Hi');
   event.preventDefault();
    var $this = $(this);
    var product_id = $this.data('product-id');

    $url = base_url_complete+'likes/like_product';
    $.ajax({
    url: $url,
    data: ({'madebyus4u_csrf_test_name':csrf_hash,'product_id': product_id}),
    dataType:'json', 
    type: "post",
    success: function(data) {
      
      if(data.success) {
       console.log($this);
       $this.attr('disabled',true);
       $this.html('<span class="glyphicon glyphicon glyphicon-heart" aria-hidden="true"></span> Liked');
      }
      else {
      	alert(data.error);
      }
   
    }             

    });
  });