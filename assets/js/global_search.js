jQuery(document).ready(function () {

var $hiddenSearch = $("#search-header"),
    $displaySearch = $("#display-search"),
    $searchOverlay = $("#search-overlay"),
    $searchList = $("#search-data");

  $("#search-header").keydown(function(){ 
    $searchOverlay.show(); 
    $hiddenSearch.focus(); 
  });
  
  $searchOverlay.click(function(event){ 
    $hiddenSearch.focus(); 
    if(event.target.id == "search-overlay" || event.target.id == "close"){
      $hiddenSearch.blur(); 
      $(this).animate({"opacity": 0}, 500, function(){ 
        $(this).hide().css("opacity", 1); 
      });
    }
  });

  $hiddenSearch.keydown(function(e){
    currentQuery = $displaySearch.val(); 
    
    if(e.keyCode == 8){ 
    
      latestQuery = currentQuery.substring(0, currentQuery.length - 1); 
      
      $displaySearch.val(latestQuery.toLowerCase()); 
      //$displaySearch.show();
      updateResults(latestQuery);       
    
    }   
    else if(e.keyCode == 32 || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 48 && e.keyCode <= 57)){ 
      latestQuery = currentQuery + String.fromCharCode(e.keyCode); 
      console.log(latestQuery); 
      $displaySearch.val(latestQuery.toLowerCase()); 

      updateResults(latestQuery); 
    }
  });

  function updateResults(latestQuery){
      

    if(latestQuery.length > 1){ 
        
         console.log(latestQuery);

      $.ajax({
          url: $("#header-search-container").attr('url'),
          data: ({'madebyus4u_csrf_test_name':csrf_hash,'latestQuery': latestQuery.toLowerCase()}),
          dataType: 'json', 
          type: "post",
          success: function(message){

            console.log("we are here too");
            console.log(message);

        if(message.status){ 

            data = message.result; 
            word = message.word;

            console.log(data);
            console.log(word);

            $("#search-data li").remove(":contains('No results')");
            $("#results").show(); 
          
          previousTerms = new Array(); 

          $("#search-data li").each(function(){ 
            previousTerms.push(($(this).text()).trim()); 
          });

          keepTerms = new Array();
          console.log("previousTerms");
          for(term in previousTerms){
            console.log(previousTerms[term]);
          }

          console.log("Terms fetched");       
          for(term in data){ 
            url = data[term]; 
            console.log(word[term]);
            if($.inArray((word[term]).trim(), previousTerms ) === -1){ //if this term isn't in the previous list of terms (and isn't already being displayed)...
              $searchList.prepend('<p ><li >'+url+'  '+term+'</li></p>');
            }else{ 
              keepTerms.push(word[term].trim()); //add the term we want to keep to an array
            }                                       
          }

          console.log("keepTerms");
          for(term in keepTerms){
            console.log(keepTerms[term]);
          }

      if(Object.getOwnPropertyNames(data).length === 0 || (keepTerms.length == 0 && (previousTerms.length != 0 || $displaySearch.val() == ""))){
            $searchList.html("<li>No results</li>");
          
          }else{            
            for(term in previousTerms){ 
                
              if($.inArray((previousTerms[term]).trim(), keepTerms) === -1){
                $("#search-data li").filter(function(){
                  return $(this).text().trim() == previousTerms[term].trim()
                }).remove();
              }

            }
          }

        }else{
             $searchList.empty();
             $searchOverlay.hide();
        }
      }

      });
}
}
});
      