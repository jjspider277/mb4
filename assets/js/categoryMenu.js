


$(function(){
    $('#Vintage').on('click',function () {
              var dropDownMenu = $(this).next('.dropdown-menu');
                  dropDownMenu.css('margin-left', '-162px');
            });
    +    $('#KidBaby').on('click',function () {
              var dropDownMenu = $(this).next('.dropdown-menu');
              dropDownMenu.css('margin-left', '-70px');
          });
    $(".dropdown-toggle").on('click',function () {
        $(".trigger").removeClass('activeCategory');
        var dropDownMenu = $(this).next('.dropdown-menu');
        var activeLink = $(this).next('.dropdown-menu');
        activeLink.find('.sub-parent-categories:first').css('background-color:grey');
        activeLink.find('.trigger:first').addClass('activeCategory');

        dropDownMenu.find('.sub-menu:first').css('display','block');

    });

    $(".dropdown-menu > li > a.trigger").on('click',function(e){
        //$(this).children('.sub-parent-categories:first').css('background-color','blue');
        var current=$(this).next();


        var grandparent=$(this).parent().parent();
        if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
            $(this).toggleClass('right-caret left-caret');
        $('.trigger').removeClass('activeCategory');
        $(this).addClass('activeCategory');
        grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
        grandparent.find(".sub-menu:visible").not(current).hide();
        current.toggle();
        e.stopPropagation();
    });
    $(".dropdown-menu > li > a:not(.trigger)").on("click",function(){
        var root=$(this).closest('.dropdown');
        root.find('.left-caret').toggleClass('right-caret left-caret');
        root.find('.sub-menu:visible').hide();
    });
});
