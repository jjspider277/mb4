function save_rating($rating_input_id,$rated,$rate_type,$to_be_rated_id,$url,$csrf_token,$csrf_hash) {
     
        var csrf_token_name = $csrf_token;      
        var rated = $rated;
        var rate_type = $rate_type;
        var to_be_rated_id = $to_be_rated_id;   
        var csrf_hash = $csrf_hash; // <- get token value from hidden form input                                                                 
                         
            //if validation is OK! then call ajax
            $.ajax({
                type:"POST",
                url:$url,
                data:{madebyus4u_csrf_test_name:csrf_hash,"rated":rated,"rate_type":rate_type,"rated_item_id":to_be_rated_id},
                dataType: "json",
                cache:false,
                success:
                    function(data) {

                    if(data.error!=null) {
                         alert(data.error);
                         $('#rating_element-'+$rating_input_id).rating('update', rated); 
                         return;
                    }
                    if(data!=null && data.rated!=null){
                     $('#rating_element-'+$rating_input_id).rating('update', data.rated);
                     return;
                    
                    }
                   
                    
                                      
                   }
             
                });                   
                     
        } 