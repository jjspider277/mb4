<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpresssite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'd4_@#LGXvCHuW95TRKq');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '10s6.;BS30>xT!]O{7fVkR00J/8i$+=FdIw}rz5X3mU(geQaq0ip_1$u5o-ONC=8');
define('SECURE_AUTH_KEY',  'J]L;-,gnei.o2d6x$OTOqD@ND&r^I/(v]|D4{yrtKKMuFVnnHxQ[]LOVo9q[1%,?');
define('LOGGED_IN_KEY',    '9sFTzRn>MlOoV%SmD| AK@q_%:Wq4M-N/z}5P<j7/T#n!QG[L=pDi2P[>5eL8?`S');
define('NONCE_KEY',        '+MC@sLsPN?LQXgjzv}#&F_.zHIf~08VfPQ#::2MG=TC2g)@a?`NN]I_,:8$mbcFb');
define('AUTH_SALT',        'Z*:&$n Nd(Gn[x8G1gkNE.R:}Tcn!=(U-X_,!iL/ ~fhgo.{^,7LYq%~*BG`QGDI');
define('SECURE_AUTH_SALT', '.Jox^-ASe:uizALlL,Q.vpf}jk4g,(K!ul(vt=Ia@YHs,<6?MpjKtUB<i#:.7=ke');
define('LOGGED_IN_SALT',   'MR2(,%Q%6W$&*.bF=Ym1G0aH+ <Sg<;=Dc~[L{=glL%zl?>6A]/y faFy,,0^.3Q');
define('NONCE_SALT',       '1bRE$;+RCE}sFSgY8BB04[l)Y#-[((!HeMH_ciR+^V!bnLRPGf`edk;fkvt`<jVT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
