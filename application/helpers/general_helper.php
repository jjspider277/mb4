
<?php

function send_email($to, $subject, $message) {
	$from = 'admin@madebyus4u.com';
    $ci = get_instance();
    $message = '
        <div style="width: 500px;">
            <div style="background-color: #F3F3F3;border-radius: 10px 10px 00px  0px;">
                <img src="' . base_url().'assets/images/logo.jpg' . '" alt="MadeByus4u Logo"/>
            </div>
            <div style="min-height: 210px;padding: 20px;background-color: #F8F8F6;color: #8C8C8C">
                ' . $message .
            '</div>
            <div style="background-color: #252525;padding: 0px 10px;overflow: hidden;border-radius: 0px 0px 10px  10px ;">
                <p style="color: #717171;margin-bottom: 0px;">All Right Reserved</p>
                <p style="margin-top: 0;"><a href="" style="color: #115481;">MadeByUs4U.com</a></p>
            </div>
        </div>';
    $ci->email->from($from, 'MadeByUs4U.com');
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($message);
    $sent = $ci->email->send();
    return $sent;
}


function send_email_new ( $template_view , $from , $to ,$subject,$content ) {

    $ci = get_instance();
    $template_view = '
        <div style="width: 500px;">
            <div style="background-color: #F3F3F3;border-radius: 10px 10px 00px  0px;">
                <img src="' .base_url().'assets/images/logo.jpg' . '" alt="MadeByus4u Logo"/>
            </div>
            <div style="min-height: 210px;padding: 20px;background-color: #F8F8F6;color: #8C8C8C">
                ' . $content .
        '</div>
        <div style="background-color: #252525;padding: 0px 10px;overflow: hidden;border-radius: 0px 0px 10px  10px ;">
            <p style="color: #717171;margin-bottom: 0px;">All Right Reserved</p>
            <p style="margin-top: 0;"><a href="http://www.madebyus4u.com" style="color: #115481;">MadeByUs4U.com</a></p>
        </div>
    </div>';
    $ci->email->from($from, 'MadeByUs4u.com');
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($content);
    $sent = $ci->email->send();
}



function send_email_with_profile_detail ($to,$from,$subject,$full_name,$profile_image_path,$url_profile,$flag) {

    $ci = &get_instance(); 
    
    if($profile_image_path==null) {

        $profile_image_path = 'http://www.madebyus4u.com/dev/uploads/no-photo.jpg';
    }
    
  
    $message_additional_line = '<br/><div style="display:block;margin-top:2%;">                     
                                 <img src="'.base_url().$profile_image_path.'" alt='.$full_name.' height="100" width="170"/>
                                </div><br/>';
     //accept request
    if($flag==0){ 
        $button_to_use='<a href='.$url_profile.'>Accept Seller Friend Request</a>';
        $message_content = "You have recieved a friend request from".'<strong>'.' '.$full_name.'</strong>';
    }
    else if ($flag==1) {
        //visit friendshipt
         $button_to_use='<a href='.$url_profile.'>Vist Seller Freind Profile </a>';
         $message_content = "You Friend request accepted by ".'<strong>'.' '.$full_name.'</strong>';
    }
     else if ($flag==2) {
        //its invite email
         //visit friendshipt
         $button_to_use='<a href='.base_url('signup').'> Register and Join Your Friend ! </a>';
         $message_content = ucfirst($full_name)."has invited you to join Madebyus4u.com! <br/>Please , click on register link below to join now! ";
     }

     else if ($flag==3) {
        //the inviter sender shoudl recived the email that he have sent
         //visit friendshipt
         $button_to_use='<a href='.$url_profile.'>Vist Your Profile </a>';
         $message_content = "Hi , ".$full_name."Your Invitation email sent email succesfull! <br/> Invitation is sent to your friends to join your freind network on Madebyus4u.com! We would like to Thankyou! <br/> <a href=".site_url()."/>Go to MadeByUs4u.com </a>";
     }
     //inviter recived email about user joined from his invites
      else if ($flag==4) {
        //this a profile network joined message
         //visit friendshipt
         $button_to_use='<a href='.$url_profile.'>Visit Your Friend Profile </a>';
         $message_content = $full_name."have joined your friend network on Madebyus4u.com! Thankyou! <br/> ";
     }
     //inviter recived email about he has joined shis invute freind newtork
      else if ($flag==5) {
        //this a profile network joined message
         //visit friendshipt
         $button_to_use='<a href='.$url_profile.'>Visit Your Friend Profile </a>';
         $message_content = "Thankyou for joining Madebyus4u.com.<br/>You have automatically added to ".$full_name."network! <br/> <a href=".site_url()."/>Go to MadeByUs4u.com </a>";
     }
     //buyer accepted seller
     else if($flag==6){ 
        $button_to_use='<a href='.$url_profile.'>Vist Buyer Profile</a>';
        $message_content = "<Strong>Success!</Strong>Your product has been succesfully accpted by a buyer!".'<strong>'.' '.$full_name.'</strong>';
    }

    else if($flag==7){ 
        $button_to_use='<a href='.$url_profile.'>Contact Buyer</a>';
        $message_content = "<Strong>Your Sold Item Not Reached a Buyer!".'<strong>'.' '.$full_name.'</strong>'."</Strong>Your buyer has reported to Madebyus4u Admin about your sell. <p>Please ,contact your buyer if possible!</b>";
    }

     else if($flag==8){ 
        $button_to_use='<a href='.$url_profile.'>See Seller Profile</a>';
        $message_content = "<Strong>Sold Item Problem Reported!</Strong>A Transaction has been reported.Please check your comminut setting as well as your dashbaord to reslove the issue.";
    }


    $template_view = '
        <div style="width: 500px;">
            <div style="background-color: #F3F3F3;border-radius: 10px 10px 00px  0px;">
                <img src="' .base_url().'assets/images/logo.jpg' . '" alt="MadeByus4u Logo"/>
            </div>
            <div style="min-height: 210px;padding: 20px;background-color: #F8F8F6;color: #8C8C8C">
                ' .$message_content.$message_additional_line.$button_to_use.
        '</div>
        <div style="background-color: #252525;padding: 0px 10px;overflow: hidden;border-radius: 0px 0px 10px  10px ;">
            <p style="color: #717171;margin-bottom: 0px;">All Right Reserved</p>
            <p style="margin-top: 0;"><a href="http://www.madebyus4u.com" style="color: #115481;">MadeByUs4U.com</a></p>
        </div>
    </div>';
    $ci->email->from($from, 'MadeByUs4u.com');
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($template_view);
    $sent = $ci->email->send();
}


function send_activation_email($first_name,$user_id ,$activation_code,$email) {
    $ci =& get_instance();
    $ci->load->library('email');
    $activation_link = base_url('signup/activate') . '/' . $user_id . '/' . $activation_code;
    $signup_link = base_url('signup/');
    $headers = 'From: admin@madebyus4u.com';
    $to = $email;
    $subject = "MadeByUs4u.com Activation Required";
    $content =
        "<html><body>" .
        "<p><h1> Dear " . $first_name . ",</h1>" . "your registration was successful </p>" .
        "<h3>Your Activation code is:<small>" . $activation_code . "</small></h3>" .
        "<p>Please use the below link to activate your account.</p>" .
        "<p><a href=" . $activation_link . ">" . $activation_link . "</a> to activate</p>" .
        "<p>If this above does not work please click on signup link " .
        "<a href=" . $signup_link . ">  <b/>SignUp</b> </a>to resend your activation link again! </p>" .
        "</body></html>";

        

         $template_view = '
            <div style="width: 500px;">
                <div style="background-color: #F3F3F3;border-radius: 10px 10px 00px  0px;padding:5px;">
                    <img src="' . base_url() . 'assets/images/logo.jpg' . '" alt="MadeByus4u Logo" style="padding-left:30%;"/>
                </div>
                <div style="min-height: 210px;padding: 20px;background-color: #F8F8F6;color: #8C8C8C">
                    ' . $content .
        '</div>
            <div style="background-color: #252525;padding: 0px 10px;overflow: hidden;border-radius: 0px 0px 10px  10px ;">
                <p style="color: #717171;margin-bottom: 0px;">All Right Reserved</p>
                <p style="margin-top: 0;"><a href=""http://www.madebyus4u.com" style="color: #115481;">MadeByUs4U.com</a></p>
            </div>
        </div>';
    // mail($to,$subject,$content,$headers);
    
// Old Code 1/6/15
   $ci->email->from("admin@madebyus4u.com", 'MadeByUs4u.com');
   $ci->email->to($to);
   $ci->email->subject($subject);
   $ci->email->message($template_view);
   $sent = $ci->email->send();
   return $sent;

}

function twitter_helper($count ,$numdays) {
        $ci = &get_instance();
        $ci->load->library('twitterfetcher');

        $tweets = $ci->twitterfetcher->getTweets(array(
            'consumerKey'       => 'FeVmw5zMQSLFH0dBO6TdGIBJh',
            'consumerSecret'    => '49GuUWsqQADVWfEX9IFfrw0Uh6sWJPM8cW25HFtkIZ8ggqA3GF',
            'accessToken'       => '2371707444-fq8K4Rhnk82qg3jtirWytcyMFR9WBunzlm43GEq',
            'accessTokenSecret' => 'zD7Zmxs2uQqP344OEgwJ63rOStEKFOkwEJdtkJhQMNysb',
            'usecache'          => true,
            'count'             => intval($count),
            'numdays'           => intval($numdays)
        ));

        //var_dump($tweets);die;
       
        $twee_texts = array(); 

        foreach($tweets as $value)  {
            
          array_push($twee_texts,$value->text);

        }

        return $twee_texts; 
}

