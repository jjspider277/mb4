<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 06/04/2015
 * Time: 10:11 AM
 */

class Rating extends MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('rating_model','rating');
 
    }

    public function rate() {
 
  		 //allow only ajax request
         if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
         }

        $rated = $this->input->post('rated');
		$rate_type = $this->input->post('rate_type');
		$rated_item_id = $this->input->post('rated_item_id');
        $rated_by = $this->profile_id;

        $product_ids = $this->get_current_profile_products($rated_by);
        $store_ids = $this->get_current_profile_stores($rated_by);

		//TODO:change the profile id
		//can't rate your self or your own profile :)
		if($rate_type=='profile'&&$rated_item_id==$rated_by) {
			echo json_encode( array('error' => 'Gotcha!  You can\'t rate your profile ;)!',));
			exit;
		}

        if($rate_type=='stores'&&in_array($rated_item_id,$store_ids)) {
			echo json_encode( array('error' => 'Gotcha! You can\'t rate your store! :)',));
			exit;
		} 

		if($rate_type=='products'&&in_array($rated_item_id,$product_ids)) {
			echo json_encode( array('error' => 'Gotcha! You can\'t rate your own product :)!',));
			exit;
		} 

		dump($rate_type);dump($rated_item_id);//dump($product_ids);exit;
        $result = $this->rating->save_or_update_rating($rated_item_id,$rate_type,$rated,$rated_by);
		//do the actual calculation of rating average weight
		$rated=$this->rating->call_rating_stored_procedure($rated_item_id,$rate_type);	
		$response = array('rated' => $rated,);
		echo json_encode($response);

	   }

}