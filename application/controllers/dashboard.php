<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Deron Frederickson.
 *
 * Date: 06/29/2015
 * 
 */


class Dashboard extends  MY_Controller {

    /*category, colors, size  variables*/
	var  $load_all_catagories ;
	var  $store_lists;
	var  $colors ;
	var  $sizes ;
    var $payment_status = array();
    var $shippment_status = array();
    var $shipping_method = array();
    var $user_is_seller = true;
    var $account_types = array();

    public function __construct() {

        parent::__construct();
	    $this->pupulate_drop_downs();
		$this->load->model('order_model', 'orders');//stored procedure
	    $this->load->model('order_model','mysales'); //just a name change
	    $this->load->model('order_model','my_orders'); //just a name change
	    $this->load->model('orderdetail_model','my_order_details');
        /*product model , categories color size loaded */
		$this->load->model('product_model','products');
		$this->load->model('store_model','store');
		$this->load->model('account_model','account');
		$this->load->model('shipping_model','shipping_address');
		$this->load->model('category_model','categories');
		$this->load_all_catagories = $this->load_catagories();
		$this->load->model('category_model','menu_categories');
		$this->load_colors_sizes();

		$this->load_stores($this->profile_id);

		$this->account_types = $this->account_types;
		$this->load->library('form_validation');

        $this->load->model('product_model','products');
        $this->load->model('store_model','store');
        $this->load->model('account_model','account');
        $this->load->model('auction_model');

		$this->load->model('order_model', 'orders');
		$this->load->model('product_model', 'products');
		$this->load->model('media_model', 'media');
		$this->load->model('auction_model', 'auctions');
		$this->load->model('store_model', 'stores');
		$this->load->model('profile_model', 'profiles');
		$this->load->model('video_model', 'videos');

		$this->load_all_catagories = $this->load_catagories();

		$this->load_colors_sizes();
		$this->load_all_catagories = $this->load_catagories();

//dump($this->products->getProductDataForEdit(100));exit;

		/*//i will be back again :)
		$where_data = array('profile_id'=>$this->profile_id);
         $result_data = $this->db->get_where('temp_files', $where_data);

         if(count($result_data->result()) > 1){
         foreach ($result_data->result() as $data) {
             @unlink($data->path . $data->file_name);
             $this->db->from('temp_files')->where('id', $data->id)->delete();
         }
      }*/

		
    }

  /*category, size country methods*/
	public function load_colors_sizes() {
	    	//load color and size from config
	    	$this->colors = array_keys($this->config->item('colors'));
	    	$this->sizes = array_keys($this->config->item('sizes'));
	    
	    	$colors=array();//local variable
	    	$sizes=array();//local variable
	    
	    	foreach ($this->colors as $key => $value) {
	    		 
	    		$value = ucfirst(strtolower($value));
	    
	    		$colors[$value] = $value; //make a value,value array for drop down
	    	}
	    
	    	foreach ($this->sizes as $key => $value) {
	    		 
	    		$value = ucfirst(strtolower($value));
	    
	    		$sizes[$value] = $value; //make a value,value array for drop down
	    	}
	    	$this->colors = $colors;
	    	$this->sizes = $sizes;
	    
	    
	    	/*var_dump($this->colors);
	    	 var_dump($this->sizes);exit;
	    	*/
	    
	    }


	public function load_catagories() {

		$categories = $this->categories->get_all_parent_categories();
		$category_string_string = array();
		foreach ($categories as $key => $category) {

			$category = ucfirst(strtolower($category->category));

			$category_string_string[$category]=$category;
		}
       ///dump($category_string_string);exit;
		return $category_string_string;
	}

	public function load_stores($profile_id) {

		$stores = $this->store->get_list_of_stores($profile_id);
		$stores_id_value = array();
		foreach ($stores as $key => $store) {

			$store_list = ucfirst(strtolower($store->store_name));

			$stores_id_value[$store->id]=$store_list;
		}
		//dump($stores_id_value);exit;

		$this->store_lists = $stores_id_value;

		return $stores_id_value;
	}



    public function get_categories_variation_ajax() {
    	 
    	//allow only ajax request
    	if(!$this->input->is_ajax_request()) {
    
    		exit('Not allowed!');
    
    	}
    
    	$categories_selected = $this->input->post('category');
    	$load_all_catagories = $this->config->item('categories');

    	$result = array_values($load_all_catagories[strtoupper($categories_selected)]);
    	$subcategories = array();
    	foreach($result as $key => $value) {
    
    		if(is_array($value)){
    			continue;
    		}
    		$subcategories[$key] = $value;
    
    	}

    	if(!empty($subcategories))
    		$subcategories =  array('#'=>'Please Select Variation')+$subcategories;
    
    	header('Content-Type: application/x-json; charset=utf-8');
    	echo ( json_encode( $subcategories )  );

    }
    
    public function get_sub_variation_ajax() {

    	if(!$this->input->is_ajax_request()) {
    		exit('Not allowed!');
    	}
    	 
    	$categories_selected = $this->input->post('category');
    	$variation_selected = $this->input->post('variation');
    	$load_all_catagories = $this->config->item('categories');

    	$result = $load_all_catagories[strtoupper($categories_selected)][$variation_selected];
    	$subcategories = array();

    	if(!empty($result))
    		$result =  array('#'=>'Please Select Variation')+$result;
    
    	header('Content-Type: application/x-json; charset=utf-8');
    	echo ( json_encode( $result  )  );
    
    }
	public function load_country_state(){

		/**

		Read from
		configuration file
		and construct
		STRING,STRING Array list

		 **/
		$country_list = $this->config->item('country');
		$country = array();

		foreach ($country_list as $key => $value) {
			$country[$value] = $value;
		}


		$states_list = $this->config->item('state');

		$states = array();

		foreach ($states_list as $key => $value) {
			$states[$value] = $value;
		}

		return array('state'=>$states,'country'=>$country);
	}

    public function check_seller_or_buyer($order_id) {

    if($this->mysales->get_by(array('buyer_id'=>$this->profile_id,'o_id'=>$order_id))) {

           $this->user_is_seller = false; //its buyer do accordingly
		}
    }

    private function pupulate_drop_downs() {

    	$this->payment_status = array('Paid'=>'Paid','Refund'=>'Refund','Pending'=>'Pending','Processing'=>'ProcessingP');
    	$this->shippment_status = array('Processing'=>'Processing','Shipped'=>'Shipped','Pending'=>'Pending');
    	$this->shipping_method = array('FedEx'=>'FedEx','DHL'=>'DHL','UPS'=>'UPS');
    }

	/**
	 *
	 * load items for sell by all active seller/memebers
	 *
	 */
	public function my_dashboard(){


		$calcuation_result =  $this->orders->call_get_dashboard_calculation_stored_procedure_for_user($this->profile_id);

		//dump($calcuation_result);exit;

		//dump($this->store->getStoreDataForEdit(38));exit;

		$total_sales =  $calcuation_result->total_sales;
		$lw_total_sales = $calcuation_result->lw_total_sales;
		$lm_total_sales = $calcuation_result->lm_total_sales;
		$total_orders =  $calcuation_result->total_orders;
		$lw_total_orders =  $calcuation_result->lw_total_orders;
		$lm_total_orders =  $calcuation_result->lm_total_orders;
		$total_paid_money_minus_fee =  $calcuation_result->total_paid_money_minus_fee;
		$lm_total_paid_money_minus_fee =  $calcuation_result->lm_total_paid_money_minus_fee;
		$lw_total_paid_money_minus_fee =  $calcuation_result->lw_total_paid_money_minus_fee;
		$total_money_owned =  $calcuation_result->total_money_owned;
		$lw_total_money_owned =  $calcuation_result->lw_total_money_owned;
		$lm_total_money_owned =  $calcuation_result->lm_total_money_owned;


		$data['total_sales'] =  $total_sales;
		$data['lw_total_sales'] = $lw_total_sales;
		$data['lm_total_sales'] = $lm_total_sales;
		$data['total_orders'] =  $total_orders;
		$data['lw_total_orders'] =  $lw_total_orders;
		$data['lm_total_orders'] =  $lm_total_orders;
		$data['total_paid_money_minus_fee'] =  $total_paid_money_minus_fee;
		$data['lm_total_paid_money_minus_fee'] =  $lm_total_paid_money_minus_fee;
		$data['lw_total_paid_money_minus_fee'] =  $lw_total_paid_money_minus_fee;
		$data['total_money_owned'] =  $total_money_owned;
		$data['lw_total_money_owned'] =  $lw_total_money_owned;
		$data['lm_total_money_owned'] =  $lm_total_money_owned;

		//get sales order for admin
		$sales_overview_data = $this->orders->get_all_orders($this->profile_id); //null will be replaced with view more implementation
		$purchase_overview_data= $this->orders->get_purchases($this->profile_id);//null to be replaced
		$auctions_overview_data= $this->auctions->get_all_auctions($this->profile_id);//null to be replaced
		$product_listing_overview_data= $this->products->get_all_product_listing($this->profile_id);//null to be replaced
		$store_overview_data= $this->stores->get_all_stores_for_own_admin($this->profile_id,null,null);//null to be replaced
		$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
		$data['sub_parent_categories'] = $sub_parent_categories;

		//$store_lisits = $this->
		//dump($auctions_overview_data);exit;

		if(empty($sales_overview_data)){
			$sales_overview_data = array();
		}
		if(empty($purchase_overview_data)){
			$purchase_overview_data = array();
		} if(empty($auctions_overview_data)){
			$auctions_overview_data = array();
		}

		$data['sales_overview_data'] =  $sales_overview_data ;
		$data['purchase_overview_data'] =  $purchase_overview_data ;
		$data['auctions_overview_data'] =  $auctions_overview_data ;
		$data['product_listing_overview_data'] =  $product_listing_overview_data;
		$data['store_overview_data'] =  $store_overview_data ;


		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';

		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;

		//$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';
		$order_data = $this->mysales->get_orders_to_seller($this->profile_id,5,0);
		$data['order_data'] = $order_data;
		$revenu_sum = $this->mysales->get_revenu($this->profile_id);
		$data['revenu_sum'] = $revenu_sum;

		//new UI
		$data['account_types'] = $this->account_types;

		$stores =  $this->store->get_all_stores_for_own_admin($this->profile_id,null,null);
		//dump($stores);exit;
		$data['store_overview_data'] = $stores;



		$data = array_merge($data,$this->data);

		///kjhasjkhdkjsahdasd
		//TODO:integrate resize with every image upload
		//$this->load->library('imageutility_service');
		//$result = $this->imageutility_service->resize_upload_images('','');

		//if user has created a store before and access using the url it must not see page


		$this->load->library('pagination');

		$total_rows = $this->products->count_all();


		$config = array();

		$config["base_url"] = base_url() ."dashboard/my_stores/";
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 8;
		$config["uri_segment"] = 3;

		$config['display_pages'] = FALSE;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		///$srores = $this->products->display_all_product($config["per_page"],$page);
		$stores = //Load seller store info
		$stores =  $this->_store_information_for_seller($this->profile_id);

		//$data["products"] = $products;

		$data["links"] = $this->pagination->create_links();

		$data['product_listing'] = 'product/product_listing';

		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$product_listing = 'product/product_listing_pages';
		/**tabs***/
		$identity_validation_page = 'storesetup/identity_validation_page';
		$store_page = 'storesetup/store_page';
		$addproduct_page = 'storesetup/addproduct';
		$getpaid_page = 'storesetup/getpaid';
		$previewstore_page = 'storesetup/previewstore';
		$previewstore1_page = 'storesetup/previewstore1';
		$launchstore_page = 'storesetup/launchstore';
		$data['identity_validation_page'] = $identity_validation_page;
		$data['store_page'] = $store_page;
		$data['addproduct_page'] = $addproduct_page;
		$data['getpaid_page'] = $getpaid_page;
		$data['previewstore_page'] = $previewstore_page;
		$data['previewstore1_page'] = $previewstore1_page;
		$data['launchstore_page'] = $launchstore_page;

		$productlisting_page = 'storesetup/productlisting';
		$data['productlisting_page'] = $productlisting_page;

		$product_listing_page = 'product/product_listing';
		$data['product_listing_page'] = $product_listing_page;

		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;


		$data['account_types'] = $this->account_types;

		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';




		$data['product_listing'] = $product_listing;
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

		$list_of_country_state_data = $this->load_country_state();
		$states = $list_of_country_state_data['state'];
		$country = $list_of_country_state_data['country'];


		$data['profile_id'] = $this->profile_id ;// :)

		$this->data= $this->load_profile_detail_with_image($this->profile_id);
		// var_dump($this->data);exit;
		//get the current profile of the user
		$profile = $this->profile->get($this->profile_id);
		$data['profile'] = $profile;


		/***auctond**/
		$products = $this->products->display_auction_products($config["per_page"],$page, $this->profile_id);

		$data["products"] = $products;

		$data['product_listing'] = 'product/product_listing';
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';

		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';
		$data['my_auction'] = true;

		$data["button_text"] = "Bid Now!";


		$product_listing_page = 'product/product_listing_column';
		$data['product_listing_page'] = $product_listing_page;

		/* for auction listing */
		$set_auction_page = 'dashboard/auctions_view';
		$data['set_auction_page'] = $set_auction_page;
		/**end**/

		
		/**end**/
		$account_types = $this->config->item('account_types');
		$data['account_types'] = $account_types;

		$data = array_merge($data,$this->data);

		$data['image_path'] = base_url().'assets/images/dashboard/';
		$data['country'] = $country;
		$data['states'] = $states;

		//dump($products);exit;

            /****hello **/


		$data['catagories'] = $this->load_all_catagories;
		$data['stores'] = $this->store_lists;
		$data['colors'] = $this->colors;
		$data['sizes'] = $this->sizes;



		$this->load->view('dashboard/dashboard_main_tab_view/dashboard_view_new',$data);

	}


    /**
     * Dashboard action
     */

    public function my_orders() {

		
		$sales_data = $this->mysales->get_all_orders($this->profile_id);
		//var_dump($sales_data);exit;        
        //var_dump($this->profile_id);
		//var_dump($sales_data);exit;
		
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';

		$data['sales_data'] = $sales_data;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';
	
	    // var_dump($this->shippment_status);var_dump($this->payment_status);exit;
		$data['shippment_status'] = $this->shippment_status	;
		$data['payment_status'] = $this->payment_status;

		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$this->load->view('dashboard/orders_view',$data);

    }
	
	public function my_purchases(){

		$this->load->model('order_model','mypurchases');
		$order_data = $this->mypurchases->get_purchases($this->profile_id);
		//dump($order_data);exit;
		$order_total = $this->mypurchases->get_purchase_order_total($this->profile_id);
		
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';

		$data['order_data'] = $order_data;
		$data['order_total'] = $order_total;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';

		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$this->load->view('dashboard/purchases_view',$data);
	}
    
    public function cancel_auction($id){
			$auctions = $this->auction_model->get_auction_by_id($id);
			if($auctions){
				$this->auction_model->update_status($auctions->id, false);
				redirect(site_url('dashboard/view_auction/'.$id));
			}

		}
	


	public function my_sales(){

		$this->load->model('order_model','mypurchases');
		$order_data = $this->mypurchases->get_purchases($this->profile_id);
		//dump($order_data);exit;
		$order_total = $this->mypurchases->get_purchase_order_total($this->profile_id);
		
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';

		$data['order_data'] = $order_data;
		$data['order_total'] = $order_total;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';

		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$this->load->view('dashboard/my_sales_view',$data);
	}

		public function edit_auction($id)
		{
			$auctions = $this->auction_model->get_auction_by_id($id);
			// var_dump($auctions);die;
			if($auctions)
			$product = $this->products->get_single_product_detail($auctions->product_id, "array");
			$data['product'] = $product;
			$data['auctions'] = $auctions;
			$now = new DateTime();
          	$diff = $now->diff(new DateTime($auctions->end_date));
          	$data['diff'] = $diff;
			
			$this->load->library('pagination');
			$total_rows = $this->products->count_all();
			$config = array();
			$config["base_url"] = base_url() . "welcome/home/";
			$config["total_rows"] = $total_rows;
			$config["per_page"] = 8;
			$config["uri_segment"] = 3;
			$config['display_pages'] = FALSE;


			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$products = $this->products->display_auction_products($config["per_page"],$page);
			$data["products"] = $products;

			$data['product_listing'] = 'product/product_listing';
			$paginate_page = 'include/paginate_page';
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
			$data['header_black_menu'] = 'include/header_black_menu';

			$data['paginate_page'] = $paginate_page;
			$data['notification_bar'] = $notification_bar;
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['image_path'] = base_url().'assets/images/dashboard/';
			$data['my_auction'] = true;

      		$data["button_text"] = "Bid Now!"; 


        	$product_listing_page = 'product/product_listing_column';
			$data['product_listing_page'] = $product_listing_page;

			/* for auction listing */
			$set_auction_page = 'dashboard/auctions_view';
			$data['set_auction_page'] = $set_auction_page;
			$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
			$data['dashboard_sidemenu'] = $dashboard_sidemenu;
			$this->load->view('dashboard/edit_auction',$data);
		}

		public function view_auction($id)
		{
			
			$auctions = $this->auction_model->
			get_auction_by_id($id);
			if($auctions)
			$product = $this->products->get_single_product_detail($auctions->product_id, 
				"array");
			$data['product'] = $product;
			$data['auctions'] = $auctions;
			$now = new DateTime();
          	$diff = $now->diff(new DateTime($auctions->end_date));
          	$data['diff'] = $diff;
			
			$this->load->library('pagination');
			$total_rows = $this->products->count_all();
			$config = array();
			$config["base_url"] = base_url() . "welcome/home/";
			$config["total_rows"] = $total_rows;
			$config["per_page"] = 8;
			$config["uri_segment"] = 3;
			$config['display_pages'] = FALSE;

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$products = $this->products->display_auction_products($config["per_page"],$page);
			$data["products"] = $products;

			$data['product_listing'] = 'product/product_listing';
			$paginate_page = 'include/paginate_page';
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
			$data['header_black_menu'] = 'include/header_black_menu';

			$data['paginate_page'] = $paginate_page;
			$data['notification_bar'] = $notification_bar;
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['image_path'] = base_url().'assets/images/dashboard/';
			$data['my_auction'] = true;

      		$data["button_text"] = "Bid Now!"; 


        	$product_listing_page = 'product/product_listing_column';
			$data['product_listing_page'] = $product_listing_page;

			/* for auction listing */
			$set_auction_page = 'dashboard/auctions_view';
			$data['set_auction_page'] = $set_auction_page;
			$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
			$data['dashboard_sidemenu'] = $dashboard_sidemenu;
			$this->load->view('dashboard/my_auction_detail',$data);
		}

		public function my_auctions() {
		
		$this->load->library('pagination');
			$total_rows = $this->products->count_all();
			$config = array();
			$config["base_url"] = base_url() . "welcome/home/";
			$config["total_rows"] = $total_rows;
			$config["per_page"] = 8;
			$config["uri_segment"] = 3;
			$config['display_pages'] = FALSE;

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$products = $this->products->display_auction_products($config["per_page"],$page, $this->profile_id);
			$data["products"] = $products;

			$data['product_listing'] = 'product/product_listing';
			$paginate_page = 'include/paginate_page';
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
			$data['header_black_menu'] = 'include/header_black_menu';

			$data['paginate_page'] = $paginate_page;
			$data['notification_bar'] = $notification_bar;
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['image_path'] = base_url().'assets/images/dashboard/';
			$data['my_auction'] = true;

            $data["button_text"] = "Bid Now!"; 


        	$product_listing_page = 'product/product_listing_column';
			$data['product_listing_page'] = $product_listing_page;

			/* for auction listing */
			$set_auction_page = 'dashboard/auctions_view';
			$data['set_auction_page'] = $set_auction_page;
			$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
			$data['dashboard_sidemenu'] = $dashboard_sidemenu;
			$this->load->view('dashboard/my_auction',$data);

	}
	/*
	* fecth stores for current user and display
	* on dashbaord ,my store view
	* #dantheman
	*/

	 public function my_stores() {
		
		//TODO:integrate resize with every image upload
		//$this->load->library('imageutility_service');
		//$result = $this->imageutility_service->resize_upload_images('','');

		//if user has created a store before and access using the url it must not see page


		$this->load->library('pagination');

		$total_rows = $this->products->count_all();


		$config = array();

		$config["base_url"] = base_url() ."dashboard/my_stores/";
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 8;
		$config["uri_segment"] = 3;

		$config['display_pages'] = FALSE;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		///$srores = $this->products->display_all_product($config["per_page"],$page);
		$stores = //Load seller store info
        $stores =  $this->_store_information_for_seller($this->profile_id);

		//$data["products"] = $products;

		$data["links"] = $this->pagination->create_links();

        $data['product_listing'] = 'product/product_listing';

		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$product_listing = 'product/product_listing_pages';
		/**tabs***/
		$identity_validation_page = 'storesetup/identity_validation_page';
		$store_page = 'storesetup/store_page';
		$addproduct_page = 'storesetup/addproduct';
		$getpaid_page = 'storesetup/getpaid';
		$previewstore_page = 'storesetup/previewstore';
		$previewstore1_page = 'storesetup/previewstore1';
		$launchstore_page = 'storesetup/launchstore';
		$data['identity_validation_page'] = $identity_validation_page;
		$data['store_page'] = $store_page;
		$data['addproduct_page'] = $addproduct_page;
		$data['getpaid_page'] = $getpaid_page;
		$data['previewstore_page'] = $previewstore_page;
		$data['previewstore1_page'] = $previewstore1_page;
		$data['launchstore_page'] = $launchstore_page;

		$productlisting_page = 'storesetup/productlisting';
		$data['productlisting_page'] = $productlisting_page;

		$product_listing_page = 'product/product_listing';
		$data['product_listing_page'] = $product_listing_page;

		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		
		$data['account_types'] = $this->account_types;

		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';




		$data['product_listing'] = $product_listing;
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

		$list_of_country_state_data = $this->load_country_state();
		$states = $list_of_country_state_data['state'];
		$country = $list_of_country_state_data['country'];


		$data['profile_id'] = $this->profile_id ;// :)

		$this->data= $this->load_profile_detail_with_image($this->profile_id);
		// var_dump($this->data);exit;

		$data = array_merge($data,$this->data);

		$data['image_path'] = base_url().'assets/images/dashboard/';
		$data['country'] = $country;
		$data['states'] = $states;
		$this->load->view('dashboard/stores_view',$data);
		
		
		
		$data['catagories'] = $this->load_all_catagories;
		$data['colors'] = $this->colors;
		$data['sizes'] = $this->sizes;

    }

	public function order_detail($order_id){

		//todo:check order before loading its details
		//Now , identify the loader of the detail is buyer or seller ?
		
		//pass the status to the view
		$this->check_seller_or_buyer($order_id);
		$data['user_is_seller'] = $this->user_is_seller;	
		$this->load->model('order_model','orderdetails');
		
		// Pulls the single customer info set from order
	     // Pulls multiple products from order, if there
		 $order_details = $this->orderdetails->get_orderdetail_of_buyer($order_id);

		$data['payment_status'] = $this->payment_status;
		$data['shippment_status'] = $this->shippment_status;
		$data['shipping_method'] = $this->shipping_method;
		//add seller information on the order details
		//which can be found from the controller $this->profile
		//var_dump($this->profile-);exit;
		//var_dump($this->session->userdata('profile_fname'));exit;

		//find seller full name
		$profile_seller = $this->profile->get($order_details->seller_id);
		$order_details->seller_full_name =  ucfirst($profile_seller->fname).' '.ucfirst($profile_seller->lname);;
		$data['order_details'] = $order_details;
		
		//var_dump($data);exit;
		
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';

		$all_categories = $this->menu_categories->get_all();
		$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
		$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
		$data['all_categories'] = $all_categories;
		$data['sub_parent_categories'] = $sub_parent_categories;
		$data['parent_categories'] = $parent_categories;
		$data['column_main_menu'] = 'include/column_menu_top';

		//var_dump($order_details);exit;

		$this->load->view('dashboard/order_detail_view',$data);
		
	}

	/***
	* AJX RIME :) 
	*Update tracking info as well as shipping price for all and update database :) for a give order detail or order
	*/
 	public function update_tracking_shipping_info() {

         
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

       $order_detils_id = $this->input->post('order_details_id');
       $tracking_number = $this->input->post('tracking_number');
       $shipping_cost = $this->input->post('shipping_cost');
       $shipping_method = $this->input->post('shipping_method');

      $return_status = $this->my_order_details->update_tracking_shipping_info($order_detils_id,$tracking_number,$shipping_cost,$shipping_method);
     
     if($return_status) {
            echo json_encode(array('success' =>true ,'message'=>'order updated!'));
      }
     else {
            echo json_encode(array('error_message' =>true ,'message'=>'failed to update order!'));
      }

  }


	/***
	* AJX TIme We update the shipping address when buyer click edit address and we updated the order details and load the page again :) 
	*Update tracking info as well as shipping price for all and update database :) for a give order detail or order
	*/
 	public function update_shipping_address() {
    
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

       $order_id = $this->input->post('order_id');
       $fname = $this->input->post('first_name');
       $lname = $this->input->post('last_name');
       $street_address_1 = $this->input->post('street_address_1');
       $street_address_2 = $this->input->post('street_address_2');
       $city = $this->input->post('city');
       $state = $this->input->post('state');
       $zip_code = $this->input->post('zip_code');

       $update_data = array("order_id"=>$order_id,'first_name'=>$fname,'last_name'=>$lname,'street_address_1'=>$street_address_2,
       						'street_address_2'=>$street_address_2,'city'=>$city,'state'=>$state,'zip_code'=>$zip_code);

     $return_status = $this->my_orders->update_order_shipping_address($update_data);

     if($return_status) {
            echo json_encode(array('success' =>true ,'message'=>'shipping address has been updated!'));
      }
     else {
            echo json_encode(array('error_message' =>true ,'message'=>'Error: when updating shipping address!'));
      }
  }


    /***
	* AJX TIme We update the shipping address when seller click add shipping and we aupdate tghe ored details and load the page again :) 
	*Update tracking info as well as shipping price for all and update database :) for a give order detail or order
	*/
 	public function update_buyer_satisfaction() {
  
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

       //TODO:check empty paramter condtiotn

       $order_id = $this->input->post('order_id');
       //updatet the status of the order
       $order_update_data = array('ostatus'=>$this->config->item('order_status')['purchased_stage']);
       $order_update_result =  $this->my_orders->update($order_id,$order_update_data); 

       $order_detail_id = $this->input->post('order_detail_id');      
       $return_status = $this->my_order_details->update_buyer_satisfaction($order_detail_id);
      
	    if($return_status) {
	            echo json_encode(array('success' =>true ,'message'=>'order successfully updated!'));
	    }
	    else {
	            echo json_encode(array('error_message' =>true ,'message'=>'Error: when updating order satisfaction!'));
	    }    

	    //send notifaction as well as emails
      //get seller id 
       $seller_profile_id  = $this->my_order_details->get($order_detail_id)->seller_id;
      // var_dump($order_detail);
       $result_of_notifaction = $this->notifications->push_notifications('SELL',null,null,$seller_profile_id,'Your sell transaction has been approved by a buyer!!');
       //TODO:EMAIL
       $this->load->helper('general');
       //first send to the seller email address
       $buyer_profile_url = base_url('sell/seller').'/'. $this->profile_id;     
       $this->load->model('profile_model','profile_reciever_sender');
       $reciever_profile = $this->profile_reciever_sender->with('user')->get($this->profile_id);
     
       $sender_profile = $this->profile_reciever_sender->with('media')->get($this->profile_id);      
       $sender_full_name = ucfirst($sender_profile->fname).' '.ucfirst($sender_profile->lname);
        //email issues
       $to = $reciever_profile->user->email;
       $from = "ecommerce.noreplay@madebyus4u.com";
       $profile_image_path = $sender_profile->media->file_name;
       $subject = "Madebyus4u.com your sell transaction accepted by ".$sender_full_name;
       $is_email_success = send_email_with_profile_detail($to,$from,$subject,$sender_full_name,$profile_image_path,$buyer_profile_url,$flag=6);
  
  }


   /***
	* AJX TIme We update the shipping address when seller click add shipping and we aupdate tghe ored details and load the page again :) 
	*Update tracking info as well as shipping price for all and update database :) for a give order detail or order
	*/
 	public function report_transaction() {
  
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

       //TODO:check empty paramter condtiotn

       $order_id = $this->input->post('order_id');
       $order_detail_id = $this->input->post('order_detail_id');
       //updatet the status of the order
       $order_update_data = array('buyer_payment_status'=>'Refund Asked','ostatus'=>$this->config->item('order_status')['report_stage']);
       $order_update_result =  $this->my_orders->update($order_id,$order_update_data); 

      
      
	    if($order_update_result) {
	            echo json_encode(array('success' =>true ,'message'=>'order has been succesfully reported !'));
	    }
	    else {
	            echo json_encode(array('error_message' =>true ,'message'=>'Error: when updating order satisfaction!'));
	    }    

	   //send notification as well as emails
      //get seller id 
       $seller_profile_id  = $this->my_order_details->get($order_detail_id)->seller_id;
      // var_dump($order_detail);
       $result_of_notifaction = $this->notifications->push_notifications('SELL_REPORT_BUYER',null,null,$seller_profile_id,'One of your sell transaction has been reported to madebyus4u admin by a buyer!!');
       //TODO:EMAIL
       $this->load->helper('general');
       //first send to the seller email address
       $buyer_profile_url = base_url('sell/seller').'/'. $this->my_orders->get($order_id)->buyer_id;
       $seller_profile_url = base_url('sell/seller').'/'. $seller_profile_id;     
       $this->load->model('profile_model','profile_reciever_sender');
       $reciever_profile = $this->profile_reciever_sender->with('user')->get($seller_profile_id);
     
       $buyer_profile = $this->profile_reciever_sender->with('media')->get($this->my_orders->get($order_id)->buyer_id);      
       $buyer_full_name = ucfirst($buyer_profile->fname).' '.ucfirst($buyer_profile->lname);
        //email issues
       $to = $reciever_profile->user->email;
       $from = "ecommerce.noreplay@madebyus4u.com";
       $profile_image_path = $buyer_profile->media->file_name;
       $subject = "Madebyus4u.com your sell transaction has been reported to inspection. <br/> Buyer full name:".$buyer_full_name;
       $is_email_success = send_email_with_profile_detail($to,$from,$subject,$buyer_full_name,$profile_image_path,$buyer_profile_url,$flag=7);
      
       //send another email to admin as well as save notification for admins

       $is_email_success = send_email_with_profile_detail('vegascraig11@gmail.com',$from,"MadebyUs4u.com A Transaction has been reported by a buyer for inspection",$buyer_full_name,$profile_image_path,$seller_profile_url,$flag=8);
  }



  /*auction actions*/
  public function auctions() {
		//TODO:integrate resize with every image upload
		//$this->load->library('imageutility_service');
		//$result = $this->imageutility_service->resize_upload_images('','');

		//if user has created a store before and access using the url it must not see page

		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$product_listing = 'product/product_listing_pages';
		/**tabs***/

		$productlisting_page = 'storesetup/productlisting';
		$data['productlisting_page'] = $productlisting_page;


		$account_types = $this->account_types;
		$data['account_types'] = $account_types;

		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';

         
        $dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;



		$data['product_listing'] = $product_listing;
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;




		$data['image_path'] = base_url().'assets/images/dashboard/';



		$this->load->view('dashboard/auctions_view',$data);

	}


	/* my listing method*/
	public function my_listing(){

			$this->load->library('pagination');
			$total_rows = $this->products->count_all();
			$config = array();
			$config["base_url"] = base_url() . "welcome/home/";
			$config["total_rows"] = $total_rows;
			$config["per_page"] = 8;
			$config["uri_segment"] = 3;
			$config['display_pages'] = FALSE;

			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$products = $this->products->get_all_seller_product_lisiting($this->profile_id,$config["per_page"],$page);
			$data["products"] = $products;

			$data['product_listing'] = 'product/product_listing';
			$paginate_page = 'include/paginate_page';
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
			$data['header_black_menu'] = 'include/header_black_menu';

			$data['paginate_page'] = $paginate_page;
			$data['notification_bar'] = $notification_bar;
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['image_path'] = base_url().'assets/images/dashboard/';

			$product_listing_page = 'product/product_listing';
			$data['product_listing_page'] = $product_listing_page;

			/* for auction listing */
			$set_auction_page = 'dashboard/auctions_view';
			$data['set_auction_page'] = $set_auction_page;
			$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
			$data['dashboard_sidemenu'] = $dashboard_sidemenu;
			$this->load->view('dashboard/listing_view',$data);
	}

	public function set_auction($product_id, $auction_id=null)
	{
		date_default_timezone_set('America/Los_Angeles');
		if($this->input->post()){
			$product = $this->products->get_single_product_detail($product_id, "array");
			if($product){
				$post = $this->input->post(null, true);

				$post['product_id'] = $product_id;
				if(empty($post['name']) || empty($post['bid_price']) || 
					empty($post['reserve_price']) || empty($post['buy_now_price']) ||
					(! empty($post['start_date']) && empty($post['start_time'])) || 
					empty($post['end_date']) || empty($post['end_time']))
					{
					$message = array(
						'type' => 'error',
						'message' =>"Auction setup failed. Please try again!"
					);
              		$this->session->set_flashdata(array('message' => $message));
              		redirect(site_url('dashboard/set_auction/'.$product_id));
				}

				//If start_date is empty set current time
				if(empty($post['start_date'])){
					$post['start_date'] = date("Y-m-d H:i:s", now());
					$post['start_time'] = date("H:i:s", now());
				}
				else {
					//Convert 12hour format to 24 hour format
					$start_time = date("H:i:s", strtotime($post['start_time']));
					unset($post['start_time']);
					$post['start_date'] .= ' '. $start_time;
					$post['start_time'] = $start_time;
				}

				
				$end_time = date("H:i:s", strtotime($post['end_time']));
				unset($post['end_time']);
				$post['end_date'] .= ' '. $end_time;
				$post['end_time'] = $end_time;
				
				
				$this->load->model('auction_model');
				if(null !== $auction_id){
					$auction = $this->auction_model->get_auction_by_id($auction_id);
					if($auction){
						if($this->auction_model->update_auction($auction_id, $post)){
							$message = array(
								'type' => 'success',
								'message' =>"Auction is setup succesfully!"
							);
		              		$this->session->set_flashdata(array('message' => $message));
		              		redirect(site_url('dashboard/my_auctions/'));
						
						}
					}
				}
				if($this->auction_model->save_auction($post)){
					$message = array(
						'type' => 'success',
						'message' =>"Auction is setup succesfully!"
					);
              		$this->session->set_flashdata(array('message' => $message));
              		redirect(site_url('dashboard/set_auction/'.$product_id));
				} 
			}
			
		}
		$this->load->library('pagination');
		$total_rows = $this->products->count_all();
		$config = array();
		$config["base_url"] = base_url() . "welcome/home/";
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 8;
		$config["uri_segment"] = 3;
		$config['display_pages'] = FALSE;

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["product"] = $this->products->get_single_product_detail($product_id, "array");
		// var_dump($data['product']);die;
		$data['product_listing'] = 'product/product_listing';
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';

		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['image_path'] = base_url().'assets/images/dashboard/';

		$product_listing_page = 'product/product_listing';
		$data['product_listing_page'] = $product_listing_page;

		/* for auction listing */
		$set_auction_page = 'dashboard/auctions_view';
		$data['set_auction_page'] = $set_auction_page;
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;


		$this->load->view('dashboard/set_auction_view',$data);
	}

	private function _store_information_for_seller($profile_id) {

      $store_total_rows = $this->store->count_by(array('is_launched'=>1,'owner_profile_id'=>$profile_id));
      $all_store_data = $this->store->get_store_lisiting($profile_id);
      //dump($all_store_data);exit;
      $this->data['all_store_data'] = $all_store_data;
      $this->data['store_total_rows'] = $store_total_rows;
      //$data['product_total_rows'] = $product_total_rows;
      $data['all_store_data'] = $all_store_data; //pass store information , profile,media,store
      
      return $this->data;
   }

   public function add_store() {
	
		$this->load->library('pagination');
		$total_rows = $this->products->count_all();

		$config = array();

		$config["base_url"] = base_url() . "welcome/home/";
		$config["total_rows"] = $total_rows;
		$config["per_page"] = 8;
		$config["uri_segment"] = 3;
		$config['display_pages'] = FALSE;

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$products = $this->products->display_all_product($config["per_page"],$page);

		$data["products"] = $products;
		$data["links"] = $this->pagination->create_links();
        $data['product_listing'] = 'product/product_listing';
		$paginate_page = 'include/paginate_page';
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$product_listing = 'product/product_listing_pages';
		/**tabs***/
		$identity_validation_page = 'storesetup/identity_validation_page';
		$store_page = 'storesetup/store_page';
		$addproduct_page = 'storesetup/addproduct';
		$getpaid_page = 'storesetup/getpaid';
		$previewstore_page = 'storesetup/previewstore';
		$previewstore1_page = 'storesetup/previewstore1';
		$launchstore_page = 'storesetup/launchstore';

		$data['identity_validation_page'] = $identity_validation_page;
		$data['store_page'] = $store_page;
		$data['addproduct_page'] = $addproduct_page;
		$data['getpaid_page'] = $getpaid_page;
		$data['previewstore_page'] = $previewstore_page;
		$data['previewstore1_page'] = $previewstore1_page;
		$data['launchstore_page'] = $launchstore_page;
		$productlisting_page = 'storesetup/productlisting';
		$data['productlisting_page'] = $productlisting_page;
		$product_listing_page = 'product/product_listing';
		$data['product_listing_page'] = $product_listing_page;
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;
		$account_types = $this->account_types;
		$data['account_types'] = $account_types;
		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['product_listing'] = $product_listing;
		$data['paginate_page'] = $paginate_page;
		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

		$list_of_country_state_data = $this->load_country_state();
		$states = $list_of_country_state_data['state'];
		$country = $list_of_country_state_data['country'];


		$data['profile_id'] = $this->profile_id ;// :)
		$this->data= $this->load_profile_detail_with_image($this->profile_id);
		$data = array_merge($data,$this->data);

		$data['image_path'] = base_url().'assets/images/dashboard/';
		$data['country'] = $country;
		$data['states'] = $states;
		
		$data['store_setup_completed'] = FALSE; //
		$data['tab_status'] = true;
		
		$data['catagories'] = $this->load_all_catagories;
		$data['colors'] = $this->colors;
		$data['sizes'] = $this->sizes;

		//disable other tabs except verification tab
        $data['store_setup_completed'] = false; //
        $data['tab_status'] = TRUE; //
        $data['source'] = 'addStoreForm';
		$this->load->view('dashboard/my_store_view/add_store_view',$data);
	}
    /**
    * Edit store implemeations 
    * #dantheman
    */

	public function edit_store($store_id) {

		$tab_status = false ;
        $store_setup_completed = false;

        //populate and set data here
        $this->populate_and_set_data($store_id);

        //populate the get paid tab

        $get_paid_info = $this->populate_and_set_data_get_paid($this->profile_id);

        $data = array_merge($get_paid_info,$this->data);
        //dump($result);exit;

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_listing = 'product/product_listing_pages';
        $products = $this->products->get_all_seller_product_lisiting($this->profile_id,10,0);
	    $data["products"] = $products;

        /**tabs***/
        
        $store_page = 'dashboard/my_store_view/store_page';
        $edit_product_lisiting = 'dashboard/my_store_view/edit_product_lisiting';
        $getpaid_page = 'dashboard/my_store_view/getpaid';
        $previewstore_page = 'dashboard/my_store_view/previewstore';
        $launchstore_page = 'dashboard/my_store_view/launchstore';
          
        $data['store_page'] = $store_page;
        $data['edit_product_lisiting'] = $edit_product_lisiting;
        $data['getpaid_page'] = $getpaid_page;
        $data['previewstore_page'] = $previewstore_page;
        $data['launchstore_page'] = $launchstore_page;

        $data['account_types'] = $this->account_types;
        $data['country'] = $country;
        $data['states'] = $states;            
        $data['image_path'] = base_url().'assets/images/dashboard/';

        $main_menu = 'include/main_menu';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['product_listing'] = $product_listing;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['show_error_page'] = 'include/show_error_page';
        $data['data']['message'] = $this->message;	
		$data['store_page'] = $store_page;
		$data['edit_product_lisiting'] = $edit_product_lisiting;
		$data['getpaid_page'] = $getpaid_page;	
		$data['launchstore_page'] = $launchstore_page;
		$productlisting_page = 'product/productlisting';
		$data['productlisting_page'] = $productlisting_page;
		$product_listing_page = 'product/product_listing';
		$data['product_listing_page'] = $product_listing_page;
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;


		$account_types = $this->config->item('account_types');
		$data['account_types'] = $account_types;
		$main_menu = 'include/main_menu';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';

		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

        //disable other tabs except verification tab
        $data['store_setup_completed'] = $store_setup_completed; //
        $data['tab_status'] = $tab_status;

        $data['profile_id'] = $this->profile_id ;// :)

        $this->populate_and_set_data($store_id) ; //load edit data

        $this->data= $this->load_profile_detail_with_image($this->profile_id);
       // var_dump($this->data);exit;
        $data = array_merge($data,$this->data);

        //dump($data);


        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

       	$this->load->view('dashboard/my_store_view/edit_store_view',$data);

    }

   //retrive data from the database and populate each field with the data in database
   
   private function populate_and_set_data($for_store_id) {

       //reterive the record from database

   	    $static_store_image_path = base_url().'uploads/profile/'.$this->profile_id.'/store/';

		//get store detail for the id
		//dump($store_id);
		//retertive the data
		$profile_store_with_product_media_data = $this->store->get_store_details($for_store_id);
		//dump($profile_store_with_product_media_data);exit;
		//get get [paid info]

		$data['store_id'] = $for_store_id;

		if(!empty($profile_store_with_product_media_data)) {
		     
        $data['storename'] = $profile_store_with_product_media_data["store_name"];
        $data['store_description'] = $profile_store_with_product_media_data["desc"];
        $data['store_image_file_path'] = $static_store_image_path.$profile_store_with_product_media_data['media']->file_name;
        $data['img_h'] = $data['store_image_file_path'];

        }
       
       //load shipping_Addressper seller 
        $shipping_data = (array)$this->shipping_address->get_by(array('profile_id'=>$this->profile_id));

       if(!empty($shipping_data) && isset($shipping_data)) {

        $data['address_line_1'] =  $shipping_data['shipping_address_line_1'];
        $data['address_line_2'] = $shipping_data['shipping_address_line_2'];
        $data['city'] = $shipping_data['shipto_city'];
        $data['state'] = $shipping_data['shipto_state'];
        $data['zip_code'] = $shipping_data['shipto_zip'];
       
        }

       $this->data = $data;
       //dump($data);exit;

	    $profile_account_data = $this->account->get($this->profile_id);
		
		//dump($profile_account_data);exit;
		
		//dump($data);exit;
   }

   private function populate_and_set_data_get_paid($profile_id) {

       //get the account information
   	   //assing to each varaible
   	   //return thye data
   	$this->load->model('account_model','bankaccountinfo');

   	$get_paid_info = $this->bankaccountinfo->get_by(array('profile_id'=>$profile_id));
   	$data['account_type'] = $get_paid_info->account_type;
   	$data['account_owner'] = $get_paid_info->account_owner;
   	$data['routing_number'] = $get_paid_info->routing_number;
   	$data['account_number'] = $get_paid_info->account_number;
   	$data['bank_branch'] = $get_paid_info->bank_branch;

   	return $data;

   }

   public function  update_get_paid_ajax() {

    

      $post_data = $this->input->post();
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

       $this->load->model('account_model','bankaccountinfo');

       $result = $this->bankaccountinfo->update_get_paid_info($this->profile_id,$post_data);

       if($result)   	 {

       	echo json_encode(array("success"=>"you have successfully updated your account information"));
       }

   } 


   public function update_store($store_id) {


    $this->load->library('upload');
    //just update store data and show success message 

    $get_post_data = $this->input->post();
   
    $file_post_name = 'store_image';		

    $result = $this->store->update_store($file_post_name,$store_id,$this->profile_id,$get_post_data);
   
	    if($result==true) {
	    	  $message = array('type'=>'success','message' =>"Information on store has been updated successfully!"); 
              $data['data']['message'] = $this->message;
              $this->session->set_flashdata('message', $data['data']['message']);
	    	
	    	redirect(base_url('dashboard/my_stores/edit_store'.'/'.$store_id."#store-tab"));

	    }

	}


	public function edit_lisiting($product_id) {
    

        
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';      

        /**tabs***/

        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $data['account_types'] = $this->account_types;
        $data['country'] = $country;
        $data['states'] = $states;            
        $data['image_path'] = base_url().'assets/images/dashboard/';

        $main_menu = 'include/main_menu';

        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
     
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['show_error_page'] = 'include/show_error_page';    
		
		
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

      
        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        //call product editing
        $edit_data = $this->pupulate_product_data_before_edit($product_id);
       
              
        $edit_product = 'dashboard/my_store_view/lisiting/edit_product';
        $add_product = 'dashboard/my_store_view/lisiting/add_product';

        $data['edit_product'] = $edit_product;
        $data['add_product'] = $add_product;
        $data = array_merge($data,$edit_data);
       //dump($data);exit;

       	$this->load->view('dashboard/my_store_view/lisiting/edit_products',$data);

	}

	
	public function pupulate_product_data_before_edit($product_id) {

      $edit_product_data = $this->products->get_single_product_detail($product_id);
      // /dump($edit_product_data);exit;

      $data['product_name'] = $edit_product_data->product_name;
      $data['product_desc'] = $edit_product_data->pdescription;
      $data['product_details'] = $edit_product_data->product_details;

      $data['price'] = $edit_product_data->price;
      $data['sprice'] = $edit_product_data->sprice;

      $data['quantity'] = $edit_product_data->quantity;
      $data['shipping_price'] = $edit_product_data->shipping_price;

      $data['color'] = $edit_product_data->color;
      $data['size'] = $edit_product_data->size;

      $data['catagory'] = $edit_product_data->category;
      $data['subcategory'] = $edit_product_data->subcategory;
      $data['edit_product_data'] = $edit_product_data;

      $data['store_id'] = $edit_product_data->p_store_id;

     
      return $data;


	}

	public function save_edited_product ($store_id) {

 	 //Server Side Validation here

	  ///
	  $post = $this->input->post();

	 /*dump($_POST);
	  dump($_FILES);*/
	  //$this->detect_which_changed_image($_FILES);

	  //dump($this->products->get(26)->with("media"));

	  $product_id = $post['product_id'];
	

	  $data['show_error_page'] = 'include/show_error_page';
     
      $this->form_validation->set_rules('product_details', 'Product_details', 'trim|required|min_length[2]|xss_clean');     
      $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');
      $this->form_validation->set_rules('product_descritpion', 'Product Description', 'trim|required|xss_clean');
      $this->form_validation->set_rules('categories', 'Categories', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('colors', 'Colors', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('sizes', 'Sizes', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_message('check_default','For categories please ,select something other than the default');
      $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('price', 'Product Name', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('sprice', 'Special Price', 'trim|required|xss_clean|numeric');
     

     if ( $this->form_validation->run() == TRUE) {

     	 $this->load->model('media_model', 'media');

		 $file_post_name = 'editproductimagefiles';

		 $upload_result = $this->media->upload_media_products($this->profile_id,$product_id,$file_post_name);
         
        // dump("Upload result data");


        // dump($upload_result);exit;
         		 
         if( !empty($upload_result['error']) ) {

                         //TODO:SHOW ERROR MESSAGE
                         echo $upload_result['error'];
                         $this->message = array_merge($this->message,array('type' => 'error','message' =>$upload_result['error']));
                         $data['data']['message'] =  $this->message;
                        //disable other tabs except verification tab
                         $tab_status = TRUE ;
                         $data['store_setup_completed'] = FALSE; //
                         $data['tab_status'] = $tab_status; //

           } else { 

           	//update products details only bot umages
             $this->load->model('product_model','product');
             $product_id = $this->product->update_lisiting($post,$this->profile_id,$store_id);
   
		
             $this->message = array('type' => 'success','message' =>"You have updated your product succesfully!! <small>Now can continue editing your other store elements or add new products also.</small>");
             $data['data']['message'] = $this->message;

           }
              
	 } 

	 else {

	      $data['store_setup_completed'] = FALSE; //
	      $data['tab_status'] = TRUE; //
	      $this->message = array('type' => 'error', 'message' => validation_errors() );
	      $data['data']['message'] = $this->message;
      	}

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';      

        /**tabs***/

        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $data['account_types'] = $this->account_types;
        $data['country'] = $country;
        $data['states'] = $states;            
        $data['image_path'] = base_url().'assets/images/dashboard/';

        $main_menu = 'include/main_menu';

        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
     
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['show_error_page'] = 'include/show_error_page';    
		
		
		$dashboard_sidemenu = 'dashboard/dashboard_sidemenu';
		$data['dashboard_sidemenu'] = $dashboard_sidemenu;

		$data['notification_bar'] = $notification_bar;
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;

		$data['show_error_page'] = 'include/show_error_page';
		$data['data']['message'] = $this->message;

      
        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];
        $product_id = $this->input->post('product_id');
        //call product editing
        $edit_data = $this->pupulate_product_data_before_edit($product_id);
       
              
        $edit_product = 'dashboard/my_store_view/lisiting/edit_product';
        $add_product = 'dashboard/my_store_view/lisiting/add_product';

        $data['edit_product'] = $edit_product;
        $data['add_product'] = $add_product;
        $data = array_merge($data,$edit_data);

		$this->load->view('dashboard/my_store_view/lisiting/edit_products',$data);
  
  }

  /**
  *add new products from the other tab on edit store link
  *#dnatheman **/

  public function add_product($store_id) {

  	  $post_data = $this->input->post();

      $data['show_error_page'] = 'include/show_error_page';
      $this->form_validation->set_rules('product_details', 'Product_details', 'trim|required|min_length[2]|xss_clean');
      $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');
      $this->form_validation->set_rules('product_descritpion', 'Product Description', 'trim|required|xss_clean');
      $this->form_validation->set_rules('categories', 'Categories', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('colors', 'Colors', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('sizes', 'Sizes', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_message('check_default','For categories please ,select something other than the default');
      $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('price', 'Product Name', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('sprice', 'Special Price', 'trim|required|xss_clean|numeric');
     

     if ( $this->form_validation->run() == TRUE) {

     	 $this->load->model('media_model', 'media');

		 $file_post_name = 'addproductimagefiles';

		 $upload_result = $this->media->upload_media_products_to_store($this->profile_id,$store_id,$file_post_name);

		 dump($upload_result);

         if( !empty($upload_result['error']) ) {

                         //TODO:SHOW ERROR MESSAGE
                         echo $upload_result['error'];
                         $this->message = array_merge($this->message,array('type' => 'error','message' =>$upload_result['error']));
                         $data['data']['message'] =  $this->message;
                        //disable other tabs except verification tab
                         $tab_status = TRUE ;
                         $data['store_setup_completed'] = FALSE; //
                         $data['tab_status'] = $tab_status; //

           } else { 

           	//update products details only bot umages
             $this->load->model('product_model','product');

             $product_id = $this->product->add_lisiting($post_data,$this->profile_id,$store_id);

             dump(count($upload_result));

             foreach($upload_result as $upload_result_product){

              $product_image_id =  $this->media->save_or_update_product($this->profile_id,$store_id,$product_id,$upload_result_product);
   
		     }


             $this->message = array('type' => 'success','message' =>"You have added a new product succesfully!! <small>Now can continue editing your other store elements or add new products also.</small>");
             
             $data['data']['message'] = $this->message;

             $this->session->set_flashdata('message', $data['data']['message']);


             redirect('dashboard/my_store/edit_store/edit_lisiting/'.$product_id.'#add-product-tab');

         }
              
	 } 

	 else {

	      $data['store_setup_completed'] = FALSE; //
	      $data['tab_status'] = TRUE; //
	      $this->message = array('type' => 'error', 'message' => validation_errors() );
	      $data['data']['message'] = $this->message;
          $this->session->set_flashdata('message', $data['data']['message']);

	     // redirect('dashboard/my_store/edit_store/edit_lisiting/'.$product_id.'#add-product-tab');
      }

      
  }

	public function addStore(){

		//dump($this->input->post());exit;


		if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
         }


		//capture post data
		$post = $this->input->post();
		//var_dump($post['zip_code']);exit;

		//TODO:ADD VALIDATION HERE
		$validation = TRUE;

		//TODO: shipping information related validation needs to be added later on
		//at first User can save empty and later when edit store he should be able to save ,
		//must figure out when fields are need to be required


		//if validation success do the following
		if ( $validation == TRUE) {

			$this->load->model('media_model','media');

			$file_post_name = 'userfile';

			//TODO:resize
			//TODO:SUCCESS DO THE SAVING of STORE WITH MEDIA DATA / UPLOAD DATA

			//save store
			$store_id = $this->store->save_store($post,$this->profile_id);

			/*save shipping information
                 $shipping_address_id =  $this->shipping_address->save_shipping_for_seller_profile($post,$this->profile_id);

              if($shipping_address_id!=-1){
                 associate the memeber profile id with shipping address
                     $this->profile->update($this->profile_id,array('shipping_id' => $shipping_address_id));
            } **/


			//get store image id
			$store_image_id = $this->media->save_or_update_store($this->profile_id,$store_id);

			//save products and its media
			$this->load->model('product_model','product');

			$product_id = $this->product->add_lisiting($post,$this->profile_id,$store_id);

			//to be inserted
			$array_of_product_image_ids = array();
			$where_data = array('profile_id' =>$this->profile_id ,'type'=>'product_image');
			$result_data_products =  $this->db->get_where('temp_files',$where_data);


			if( $result_data_products->num_rows() > 0 ) {
				//insert the five product image into media table and collect inserted id

				foreach($result_data_products->result() as $row){

					$array_of_product_image_ids[] = $this->media->save_or_update_product_temp_files($row,$this->profile_id,$store_id,$product_id);

				}


				$images_found = array();

				foreach($result_data_products->result() as $row) {

					$check_image_id = substr($row->image_id, 3,1);

					$images_found[] = $check_image_id;

				}

				$missing_images = array();

				for($i=1;$i<=5;$i++) {

					if( in_array($i,$images_found) == false ) {
						 $missing_images[] = $i;
					}
				}

				foreach($missing_images as $missing_image) {

					$insert_data =  array(
							'type' => 'product_image',
							'file_name' => 'image-upload.jpg',
							'full_path' => base_url('assets/images/image-upload.jpg'),
							'profile_id' => $this->profile_id,
							'product_id'=> $product_id,
							'store_id'=> $store_id,
					);

					$this->media->insert($insert_data);
				}



				//NOW SAVE ACCOUNT INFORMATIONs

				$this->load->model('account_model','account');

				$account_id = $this->account->save_account_profile($this->profile_id,$store_id,$post);

				if($store_image_id) {

					$this->store->update($store_id,array('media_id'=>$store_image_id));

				}

				$this->message = array('type' => 'success','message' =>"You have setup you store succesfully!! <small>Now can continue viewing your store.</small>");
				$data['data']['message'] = $this->message;

				$this->clean_up_may_day($this->profile_id);

				echo json_encode($this->message);

			}

		} else {

			$data['store_setup_completed'] = FALSE; //
			$data['tab_status'] = TRUE; //
			$this->message = array('type' => 'error', 'message' => validation_errors() );
			$data['data']['message'] = $this->message;

			echo json_encode($this->message);
		}
	}

	public function getEditStoreData($store_id) {

		$edit_store_data = $this->store->getStoreDataForEdit($store_id);
		//$this->stores->get
		echo json_encode($edit_store_data);

	}

	public function updateStore($store_id) {


		$get_post_data = $this->input->post();

		//dump($get_post_data);exit;

		$product_id = $get_post_data['product_id'];

		$where_data = array('profile_id'=>$this->profile_id,'type'=>'store_image');

		$result_data = $this->db->get_where('temp_files', $where_data);

		if ($result_data->num_rows() > 0) {

			$row = $result_data->row();

			//move the file to the  correct path

			$pathToUpload = "./uploads/profile/" . $this->profile_id . "/store/";
			$tempUploadPath = "./uploads/temp/" . $this->profile_id . "/store/" . $row->file_name;


			if (!is_dir($pathToUpload))
				mkdir($pathToUpload, 0777, TRUE);

			//rename files first
			$ext = explode(".", $row->file_name);
			$new_filename = 'store_image' . rand(1, 99999) . '.' . end($ext);

			rename($tempUploadPath, $pathToUpload . $new_filename);

			$update_data = array(
					'type' => 'store_image',
					'file_name' => $new_filename,
					'full_path' => $pathToUpload . $new_filename,
					'profile_id' => $row->profile_id,
					'store_id' => $store_id
			);

			$where_update_data = array(
					'store_id'=>$store_id,
					'profile_id' =>$this->profile_id,
			);

			$where = $where_update_data;
			$data =  $update_data;

			$update_store_result = $this->media->update_by($where,$data);


		} // if product has image below

		$array_of_product_image_ids = array();
		$where_data = array('profile_id' =>$this->profile_id ,'type'=>'product_image');
		$result_data_products =  $this->db->get_where('temp_files',$where_data);

		$images_found = array();

		foreach ($result_data_products->result() as $row) {

			$check_image_id = substr($row->image_id, 3, 1);

			$images_found[] = $check_image_id;

		}


		if( $result_data_products->num_rows() > 0 ) {
			//insert the five product image into media table and collect inserted id

			foreach ($result_data_products->result() as $row_data) {

				//$this->media->save_or_update_product_temp_files($row_data, $this->profile_id, $store_id, $product_id);

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');

				//if there is at least one image and the 4;s are missing - so we insert to make it 5 images
				//so that later we can update by tracking which image has been asked to be updated

				if(null!=$medias && count($medias)==1){

					for($j=1;$j<5;$j++) {

						$insert_data =  array(
								'type' => 'product_image',
								'file_name' => 'image-upload.jpg',
								'full_path' => base_url('assets/images/image-upload.jpg'),
								'profile_id' => $this->profile_id,
								'product_id'=> $product_id,
								'store_id'=> $store_id,
						);

						$this->media->insert($insert_data);

					}
				}

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');


				$image_id = substr($row->image_id, 3, 1);

				if ( in_array( $image_id, $images_found) ){

					//move the file to the  correct path
					$pathToUpload = "./uploads/profile/" . $this->profile_id . "/products/";
					$tempUploadPath = "./uploads/temp/" . $this->profile_id . "/products/" . $row_data->file_name;

					if (!is_dir($pathToUpload))
						mkdir($pathToUpload, 0777, TRUE);

					//rename files first
					$ext = explode(".", $row_data->file_name);
					$new_filename = 'product_image' . rand(1, 99999) . '.' . end($ext);

					rename($tempUploadPath, $pathToUpload . $new_filename);
					//check if there on media befor or not

					$update_data = array(
							'type' => 'product_image',
							'file_name' => $new_filename,
							'full_path' => $pathToUpload . $new_filename,
						//'profile_id' => $this->profile_id,
						//'product_id' => $product_id,
						//'store_id' => $store_id,
					);

					$where_update_data = array(
						//'store_id' => $store_id,
							'profile_id' => $this->profile_id,
							'product_id' => $product_id,
							'type' => 'product_image',
							'id'=>$medias[intval($image_id)-1]->id,
					);

					$where = $where_update_data;
					$data = $update_data;

					$array_of_product_image_ids[] = $this->media->update_by($where, $data);
				}

			}

		}


		//clean data parmeters / values update

			$this->products->update_lisiting($get_post_data, $product_id, $this->profile_id, $store_id);

			$update_data = array(
					'store_name' => $get_post_data['storename'],
					'owner_profile_id' => $this->profile_id,
					'desc' => $get_post_data['store_description'],
			);

			$where_update_data = array(
					'id' => $store_id,
					'owner_profile_id' => $this->profile_id,
			);

			$where = $where_update_data;
			$data = $update_data;

			$this->store->update_by($where, $data);

		   $this->load->model('account_model','bankaccountinfo');

		   $result = $this->bankaccountinfo->update_get_paid_info($this->profile_id,$get_post_data,$store_id);


		$this->clean_up_may_day($this->profile_id);


		echo json_encode(true);


		}


	public function addListing($store_id)
	{

		$post = $this->input->post();

		//save products and its media
		$this->load->model('product_model','product');

		$product_id = $this->product->add_lisiting($post, $this->profile_id, $store_id);

		//to be inserted
		$array_of_product_image_ids = array();
		$where_data = array('profile_id' => $this->profile_id, 'type' => 'product_image');
		$result_data_products = $this->db->get_where('temp_files', $where_data);


		if ($result_data_products->num_rows() > 0) {
			//insert the five product image into media table and collect inserted id
			foreach ($result_data_products->result() as $row) {

				$array_of_product_image_ids[] = $this->media->save_or_update_product_temp_files($row, $this->profile_id, $store_id, $product_id);

			}


			$images_found = array();

			foreach ($result_data_products->result() as $row) {

				$check_image_id = substr($row->image_id, 3, 1);

				$images_found[] = $check_image_id;

			}

			$missing_images = array();

			for ($i = 1; $i <= 5; $i++) {

				if (in_array($i, $images_found) == false) {
					$missing_images[] = $i;
				}
			}

			foreach ($missing_images as $missing_image) {

				$insert_data = array(
						'type' => 'product_image',
						'file_name' => 'image-upload.jpg',
						'full_path' => base_url('assets/images/image-upload.jpg'),
						'profile_id' => $this->profile_id,
						'product_id' => $product_id,
						'store_id' => $store_id,
				);

				$this->media->insert($insert_data);
			}

		}

		$this->clean_up_may_day($this->profile_id);
		echo json_encode(array('success'=>true,'saved_product_id'=>$product_id));

	}

	public function addAuctionForProduct($product_id) {
		date_default_timezone_set('America/Los_Angeles');

		$message= array();

		if($this->input->post()){
			$product = $this->products->get($product_id);
			if(!empty($product)){

				$post = $this->input->post(null, true);

				$post['product_id'] = $product_id;

				if(empty($post['name']) || empty($post['start_price']) ||
						empty($post['reserve_price']) ||
						(! empty($post['start_date']) && empty($post['start_time'])) ||
						empty($post['end_date']) || empty($post['end_time']))
				{
					$message = array(
							'type' => 'error',
							'message' =>"Auction setup failed. Please try again!"
					);


				}

				//If start_date is empty set current time
				if(empty($post['start_date'])){
					$post['start_date'] = date("Y-m-d H:i:s", now());
					$post['start_time'] = date("H:i:s", now());
				}
				else {
					//Convert 12hour format to 24 hour format
					$start_time = date("H:i:s", strtotime($post['start_time']));
					unset($post['start_time']);
					$post['start_date'] .= ' '. $start_time;
					$post['start_time'] = $start_time;
				}


				$end_time = date("H:i:s", strtotime($post['end_time']));
				unset($post['end_time']);
				$post['end_date'] .= ' '. $end_time;
				$post['end_time'] = $end_time;


				if($this->auction_model->save_auction($post)){
					$message = array(
							'type' => 'success',
							'message' =>"Auction is setup successfully!"
					);

				}
			}

		}
		echo json_encode($message);

 	}

	public function getEditProductData($product_id) {

		$edit_product_data = $this->products->getProductDataForEdit($product_id);
		echo json_encode($edit_product_data);

	}

	public function updateProductListing($product_id) {

		$message= array();

		$product = $this->products->get($product_id);

		$get_post_data = $this->input->post();

		// if product has image below

		$array_of_product_image_ids = array();
		$where_data = array('profile_id' =>$this->profile_id ,'type'=>'product_image');
		$result_data_products =  $this->db->get_where('temp_files',$where_data);

		$images_found = array();

		foreach ($result_data_products->result() as $row) {

			$check_image_id = substr($row->image_id, 3, 1);

			$images_found[] = $check_image_id;

		}


		if( $result_data_products->num_rows() > 0 ) {
			//insert the five product image into media table and collect inserted id

			foreach ($result_data_products->result() as $row_data) {

				//$this->media->save_or_update_product_temp_files($row_data, $this->profile_id, $store_id, $product_id);

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');

				//if there is at least one image and the 4;s are missing - so we insert to make it 5 images
				//so that later we can update by tracking which image has been asked to be updated

				if(null!=$medias && count($medias)==1){

					for($j=1;$j<5;$j++) {

						$insert_data =  array(
								'type' => 'product_image',
								'file_name' => 'image-upload.jpg',
								'full_path' => base_url('assets/images/image-upload.jpg'),
								'profile_id' => $this->profile_id,
								'product_id'=> $product_id,
								'store_id'=> $product->store_id,
						);

						$this->media->insert($insert_data);

					}
				}

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');


				$image_id = substr($row->image_id, 3, 1);

				if ( in_array( $image_id, $images_found) ){

						//move the file to the  correct path
					$pathToUpload = "./uploads/profile/" . $this->profile_id . "/products/";
					$tempUploadPath = "./uploads/temp/" . $this->profile_id . "/products/" . $row_data->file_name;

					if (!is_dir($pathToUpload))
						mkdir($pathToUpload, 0777, TRUE);

					//rename files first
					$ext = explode(".", $row_data->file_name);
					$new_filename = 'product_image' . rand(1, 99999) . '.' . end($ext);

					rename($tempUploadPath, $pathToUpload . $new_filename);
					//check if there on media befor or not

					$update_data = array(
							'type' => 'product_image',
							'file_name' => $new_filename,
							'full_path' => $pathToUpload . $new_filename,
						//'profile_id' => $this->profile_id,
						//'product_id' => $product_id,
						//'store_id' => $store_id,
					);

					$where_update_data = array(
						//'store_id' => $store_id,
							'profile_id' => $this->profile_id,
							'product_id' => $product_id,
							'type' => 'product_image',
							'id'=>$medias[intval($image_id)-1]->id,
					);

					$where = $where_update_data;
					$data = $update_data;

					$array_of_product_image_ids[] = $this->media->update_by($where, $data);
				}

			}

		}

		//clean data parmeters / values update

		$this->products->update_lisiting($get_post_data, $product_id, $this->profile_id, $product->store_id);

		$this->clean_up_may_day($this->profile_id);

		if($this->input->post()){
			$product = $this->products->get($product_id); // repetation take out after test
			if(!empty($product)){

				$post['name'] = $get_post_data['name'];
				$post['bid_price'] = $get_post_data['start_price'];
				$post['reserve_price'] = $get_post_data['reserve_price'];
				$post['start_date'] = $get_post_data['start_date'];
				$post['start_time'] = $get_post_data['start_time'];
				$post['end_date'] = $get_post_data['end_date'];
				$post['end_time'] = $get_post_data['end_time'];

				$post['product_id'] = $product_id;

				if(empty($post['name']) || empty($post['start_price']) ||
						empty($post['reserve_price']) ||
						(! empty($post['start_date']) && empty($post['start_time'])) ||
						empty($post['end_date']) || empty($post['end_time']))
				{
					$message = array(
							'type' => 'error',
							'message' =>"Auction setup failed. Please try again!"
					);


				}

				//If start_date is empty set current time
				if(empty($post['start_date'])){
					$post['start_date'] = date("Y-m-d H:i:s", now());
					$post['start_time'] = date("H:i:s", now());
				}
				else {
					//Convert 12hour format to 24 hour format
					$start_time = date("H:i:s", strtotime($post['start_time']));
					unset($post['start_time']);
					$post['start_date'] .= ' '. $start_time;
					$post['start_time'] = $start_time;
				}


				$end_time = date("H:i:s", strtotime($post['end_time']));
				unset($post['end_time']);
				$post['end_date'] .= ' '. $end_time;
				$post['end_time'] = $end_time;


				$auction_found = $this->auction_model->get_by(array('product_id'=>$product_id));

				if($auction_found==null){
					$this->auction_model->save_auction($post);
					$message = array(
							'type' => 'success',
							'message' =>"Auction is setup successfully!"
					);

				}
				if(null !== $auction_found){
					$auction = $this->auction_model->get($auction_found->id);
					if($auction){
						if($this->auction_model->update_auction($auction_found->id, $post)){
							$message = array(
									'type' => 'success',
									'message' =>"Auction is setup successfully!"
							);

						}
					}
				}


			}

		}
		echo json_encode($message);

	}


	public function getEditAuctionData($auction_id) {

		$edit_product_data = $this->auctions->getEditAuctionData($auction_id);
		echo json_encode($edit_product_data);

	}


	public function updateAuction($auction_id) {

		$product_id = $this->input->post('product_id');

		$get_post_data = $this->input->post();

		$message= array();

		$product = $this->products->get($product_id);

		$get_post_data = $this->input->post();

		// if product has image below

		$array_of_product_image_ids = array();
		$where_data = array('profile_id' =>$this->profile_id ,'type'=>'product_image');
		$result_data_products =  $this->db->get_where('temp_files',$where_data);

		$images_found = array();

		foreach ($result_data_products->result() as $row) {

			$check_image_id = substr($row->image_id, 3, 1);

			$images_found[] = $check_image_id;

		}


		if( $result_data_products->num_rows() > 0 ) {
			//insert the five product image into media table and collect inserted id

			foreach ($result_data_products->result() as $row_data) {

				//$this->media->save_or_update_product_temp_files($row_data, $this->profile_id, $store_id, $product_id);

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');

				//if there is at least one image and the 4;s are missing - so we insert to make it 5 images
				//so that later we can update by tracking which image has been asked to be updated

				if(null!=$medias && count($medias)==1){

					for($j=1;$j<5;$j++) {

						$insert_data =  array(
								'type' => 'product_image',
								'file_name' => 'image-upload.jpg',
								'full_path' => base_url('assets/images/image-upload.jpg'),
								'profile_id' => $this->profile_id,
								'product_id'=> $product_id,
								'store_id'=> $product->store_id,
						);

						$this->media->insert($insert_data);

					}
				}

				$medias = $this->media->get_many_by(array('product_id'=>$product_id),'array');


				$image_id = substr($row->image_id, 3, 1);

				if ( in_array( $image_id, $images_found) ){

					//move the file to the  correct path
					$pathToUpload = "./uploads/profile/" . $this->profile_id . "/products/";
					$tempUploadPath = "./uploads/temp/" . $this->profile_id . "/products/" . $row_data->file_name;

					if (!is_dir($pathToUpload))
						mkdir($pathToUpload, 0777, TRUE);

					//rename files first
					$ext = explode(".", $row_data->file_name);
					$new_filename = 'product_image' . rand(1, 99999) . '.' . end($ext);

					rename($tempUploadPath, $pathToUpload . $new_filename);
					//check if there on media befor or not

					$update_data = array(
							'type' => 'product_image',
							'file_name' => $new_filename,
							'full_path' => $pathToUpload . $new_filename,
						//'profile_id' => $this->profile_id,
						//'product_id' => $product_id,
						//'store_id' => $store_id,
					);

					$where_update_data = array(
						//'store_id' => $store_id,
							'profile_id' => $this->profile_id,
							'product_id' => $product_id,
							'type' => 'product_image',
							'id'=>$medias[intval($image_id)-1]->id,
					);

					$where = $where_update_data;
					$data = $update_data;

					$array_of_product_image_ids[] = $this->media->update_by($where, $data);
				}

			}

		}

		$this->clean_up_may_day($this->profile_id);

		if($this->input->post()){
			$product = $this->products->get($product_id); // repetation take out after test
			if(!empty($product)){

				$post['name'] = $get_post_data['name'];
				$post['bid_price'] = $get_post_data['start_price'];
				$post['reserve_price'] = $get_post_data['reserve_price'];
				$post['start_date'] = $get_post_data['start_date'];
				$post['start_time'] = $get_post_data['start_time'];
				$post['end_date'] = $get_post_data['end_date'];
				$post['end_time'] = $get_post_data['end_time'];

				$post['product_id'] = $product_id;

				if(empty($post['name']) || empty($post['start_price']) ||
						empty($post['reserve_price']) ||
						(! empty($post['start_date']) && empty($post['start_time'])) ||
						empty($post['end_date']) || empty($post['end_time']))
				{
					$message = array(
							'type' => 'error',
							'message' =>"Auction setup failed. Please try again!"
					);


				}

				//If start_date is empty set current time
				if(empty($post['start_date'])){
					$post['start_date'] = date("Y-m-d H:i:s", now());
					$post['start_time'] = date("H:i:s", now());
				}
				else {
					//Convert 12hour format to 24 hour format
					$start_time = date("H:i:s", strtotime($post['start_time']));
					unset($post['start_time']);
					$post['start_date'] .= ' '. $start_time;
					$post['start_time'] = $start_time;
				}


				$end_time = date("H:i:s", strtotime($post['end_time']));
				unset($post['end_time']);
				$post['end_date'] .= ' '. $end_time;
				$post['end_time'] = $end_time;


				$auction_found = $this->auction_model->get_by(array('product_id'=>$product_id));

				if($auction_found==null){
					$this->auction_model->save_auction($post);
					$message = array(
							'type' => 'success',
							'message' =>"Auction updated successfully!"
					);

				}
				if(null !== $auction_found){
					$auction = $this->auction_model->get($auction_found->id);
					if($auction){
						if($this->auction_model->update_auction($auction_found->id, $post)){
							$message = array(
									'type' => 'success',
									'message' =>"Auction updated successfully!"
							);

						}
					}
				}


			}

		}
		echo json_encode($message);
	}


}

  
/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */

