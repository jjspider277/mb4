
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* created by daniel adenew
*/

class Category {
    var $category = "";
    var $parent_id = 'null';
    var $created_date = "";
    var $update_date = "";
}



class VariationColor {
    var $variation_color = "";
    var $created_date = "";
    var $update_date = "";
}

class ShippingOption {
    var $option_name = "";
    var $created_date = "";
    var $update_date = "";
}


class Admin extends MY_Controller
{
    var $account_types = array();
    var $user_is_seller = true;

    public function __construct(){

        parent::__construct();
        $this->load->model('order_model', 'orders');
        $this->load->model('product_model', 'products');
        $this->load->model('media_model', 'media');
        $this->load->model('auction_model', 'auctions');
        $this->load->model('store_model', 'stores');
        $this->load->model('profile_model', 'profiles');
        $this->load->model('video_model', 'videos');

        $this->load->helper('general');
        $this->load->model('product_model', 'products');
        $this->load->helper('typography');


        $this->pupulate_drop_downs();
        $this->load->model('order_model', 'orders');//stored procedure
        $this->load->model('order_model','mysales'); //just a name change
        $this->load->model('order_model','my_orders'); //just a name change
        $this->load->model('orderdetail_model','my_order_details');
        /*product model , categories color size loaded */
        $this->load->model('product_model','products');
        $this->load->model('store_model','store');
        $this->load->model('account_model','account');
        $this->load->model('shipping_model','shipping_address');
        $this->load_all_catagories = $this->load_catagories();
        $this->load_colors_sizes();
        $this->account_types = $this->account_types;
        $this->load->library('form_validation');

        $this->load->model('product_model','products');
        $this->load->model('store_model','store');
        $this->load->model('account_model','account');
        $this->load->model('auction_model');

        $this->load->model('order_model', 'orders');
        $this->load->model('product_model', 'products');
        $this->load->model('media_model', 'media');
        $this->load->model('auction_model', 'auctions');
        $this->load->model('store_model', 'stores');
        $this->load->model('profile_model', 'profiles');
        $this->load->model('video_model', 'videos');
        $this->load_all_catagories = $this->load_catagories();
        $this->load_colors_sizes();
    }
    
    public function  index() {



     if($this->is_admin == true) {

        $calcuation_result =  $this->orders->call_get_dashboard_calculation_stored_procedure();
        // / dump($calcuation_result);exit;

        $total_sales =  $calcuation_result->total_sales;
        $lw_total_sales = $calcuation_result->lw_total_sales;
        $lm_total_sales = $calcuation_result->lm_total_sales;
        $total_orders =  $calcuation_result->total_orders;
        $lw_total_orders =  $calcuation_result->lw_total_orders;
        $lm_total_orders =  $calcuation_result->lm_total_orders;
        $total_paid_money_minus_fee =  $calcuation_result->total_paid_money_minus_fee;
        $lm_total_paid_money_minus_fee =  $calcuation_result->lm_total_paid_money_minus_fee;
        $lw_total_paid_money_minus_fee =  $calcuation_result->lw_total_paid_money_minus_fee;
        $total_money_owned =  $calcuation_result->total_money_owned;
        $lw_total_money_owned =  $calcuation_result->lw_total_money_owned;
        $lm_total_money_owned =  $calcuation_result->lm_total_money_owned;


        $data['total_sales'] =  $total_sales;
        $data['lw_total_sales'] = $lw_total_sales;
        $data['lm_total_sales'] = $lm_total_sales;
        $data['total_orders'] =  $total_orders;
        $data['lw_total_orders'] =  $lw_total_orders;
        $data['lm_total_orders'] =  $lm_total_orders;
        $data['total_paid_money_minus_fee'] =  $total_paid_money_minus_fee;
        $data['lm_total_paid_money_minus_fee'] =  $lm_total_paid_money_minus_fee;
        $data['lw_total_paid_money_minus_fee'] =  $lw_total_paid_money_minus_fee;
        $data['total_money_owned'] =  $total_money_owned;
        $data['lw_total_money_owned'] =  $lw_total_money_owned;
        $data['lm_total_money_owned'] =  $lm_total_money_owned;

        $video_page = "include/video_page";
        $paginate_page = "include/paginate_page";
        $notification_bar = "include/notification_bar";
        $header_logo_white = "include/header_logo_white";
        $main_menu = "include/main_menu";
        $data['header_black_menu'] = "include/header_black_menu";
        $product_listing_column="product/product_listing_column";
        $data["product_listing_column"]=$product_listing_column;
        $data["paginate_page"] = $paginate_page;
        $data["header_logo_white"] = $header_logo_white;
        $data["main_menu"] = $main_menu;
    
        $data["video_page"] = $video_page;
        $data["paginate_page"] = $paginate_page;
        $data["notification_bar"] = $notification_bar;

        $data["footer_privacy"] = "include/footer_privacy";
        $data["footer_subscribe"] = "include/footer_subscribe";
        $data['admin_tab_content'] =  'admin/admin_tab_content' ;
        $data['sales_overview'] =  'admin/sales_overview_view' ;


        //get sales order for admin
        $sales_overview_data = $this->orders->get_all_sales_to_admin(null,null); //null will be replaced with view more implementation
        $purchase_overview_data= $this->orders->get_all_purchases_to_admin(null,null);//null to be replaced
        $auctions_overview_data= $this->auctions->get_all_auctions_for_admin(null,null);//null to be replaced
        $product_listing_overview_data= $this->products->get_all_product_listing_for_admin(null,null);//null to be replaced
        $store_overview_data= $this->stores->get_all_stores_for_admin(null,null);//null to be replaced
        $members_data =  $this->profiles->get_all_members_for_admin(8,1); // to be replaced for view more thing
        $video_data =  $this->videos->get_all_videos_for_admin(); //pagination in the future



         if(empty($sales_overview_data)){
             $sales_overview_data = array();
         }
         if(empty($purchase_overview_data)){
             $purchase_overview_data = array();
         } if(empty($auctions_overview_data)){
             $auctions_overview_data = array();
         }

         $data['sales_overview_data'] =  $sales_overview_data ;
         $data['purchase_overview_data'] =  $purchase_overview_data ;
         $data['auctions_overview_data'] =  $auctions_overview_data ;
         $data['product_listing_overview_data'] =  $product_listing_overview_data;
         $data['store_overview_data'] =  $store_overview_data ;
         $data['members_data'] =  $members_data ;
         $data['video_data'] =  $video_data ;

         //dump($video_data);exit;
         $this->load->view('admin/admin_view',$data);
      }
      else {

        echo "<h1>Your are not allowed to Visit this page ! </h1> ";
      }

      //redirect('users/logout');
	}

   

    //save_subcategory

    public function save_category() {

        //allow only ajax request
        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
        }
        $category_name = $this->input->post('category_name');
        $object = new Category();
        $object->created_date = now();
        $object->category = $category_name;
        $object->parent_id = null;
        //save category now :)
        $result = $this->db->insert('categories', $object);
        //save category
        if($result) {

            echo json_encode( array('success'=>true) );
        }
        else {

            echo json_encode( json_last_error() );
        }
    }

    //save subcategory with catgeroey as parent id
    public function save_subcategory() {

        //allow only ajax request
        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
        }
        $subcategory_name = $this->input->post('subcategory');
        $category_id = $this->input->post('category_id');
        $object = new Category();
        $object->created_date = now();
        $object->category = $subcategory_name;
        $object->parent_id = $category_id;
        $result = $this->db->insert('categories', $object);

        //save sub categories

        if($result) {

            echo json_encode( array('success'=>true) );
        }
        else {

            echo json_encode( json_last_error() );
        }

    }

    //save subcategory with save_variation_colors

    public function save_variation_colors() {

        //allow only ajax request

        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
        }

        $color = $this->input->post('color');
        $object = new VariationColor();
        $object->created_date = now();
        $object->variation_color = $color;
        $result = $this->db->insert('variation_colors', $object);

        //save sub categories

        if($result) {

            echo json_encode( array('success'=>true) );
        }
        else {

            echo json_encode( json_last_error() );
        }

    }


    //save subcategory with save_variation_colors

    public function save_shipping_options() {

        //allow only ajax request
        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
        }

        $shipping_option = $this->input->post('shipping_option');
        $object = new ShippingOption();
        $object->created_date = now();
        $object->option_name = $shipping_option;
        $result = $this->db->insert('shipping_options', $object);

        //save sub categories

        if($result) {

            echo json_encode( array('success'=>true) );
        }
        else {

            echo json_encode( json_last_error() );
        }

    }

    public function get_all_categories() {

        //allow only ajax request
        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');

        }
        $this->db->select('id,category');
        $this->db->from('categories');
        $query = $this->db->get();
        $category_json = $query->result();


        //dump($category_json);exit;


        $json = array();

        foreach($category_json as $jsons) {
            $json[] = array(
                'id' => $jsons->id,
                'value' => $jsons->category,// Don't you want the name?
            );
        }

       echo json_encode($json);

   }

    public function check_seller_or_buyer($order_id) {

        if($this->mysales->get_by(array('buyer_id'=>$this->profile_id,'o_id'=>$order_id))) {

            //$this->user_is_seller = false; //its buyer do accordingly
        }
    }

    private function pupulate_drop_downs() {

        $this->payment_status = array('Paid'=>'Paid','Refund'=>'Refund','Pending'=>'Pending','Processing'=>'ProcessingP');
        $this->shippment_status = array('Processing'=>'Processing','Shipped'=>'Shipped','Pending'=>'Pending');
        $this->shipping_method = array('FedEx'=>'FedEx','DHL'=>'DHL','UPS'=>'UPS');
    }

    public function order_detail($order_id){

        //todo:check order before loading its details
        //Now , identify the loader of the detail is buyer or seller ?

        //pass the status to the view
        $this->check_seller_or_buyer($order_id);
        $data['user_is_seller'] = $this->user_is_seller;
        $this->load->model('order_model','orderdetails');

        // Pulls the single customer info set from order
        // Pulls multiple products from order, if there
        $order_details = $this->orderdetails->get_orderdetail_of_buyer($order_id);

        $data['payment_status'] = $this->payment_status;
        $data['shippment_status'] = $this->shippment_status;
        $data['shipping_method'] = $this->shipping_method;
        //add seller information on the order details
        //which can be found from the controller $this->profile
        //var_dump($this->profile-);exit;
        //var_dump($this->session->userdata('profile_fname'));exit;

        //find seller full name
        $profile_seller = $this->profile->get($order_details->seller_id);
        $order_details->seller_full_name =  ucfirst($profile_seller->fname).' '.ucfirst($profile_seller->lname);;
        $data['order_details'] = $order_details;

        //var_dump($data);exit;

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $main_menu = 'include/main_menu';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['image_path'] = base_url().'assets/images/dashboard/';

        //var_dump($order_details);exit;

        $this->load->view('admin/order_detail_view',$data);

    }
    public function load_catagories() {

        $this->load_all_catagories = array_keys($this->config->item('categories'));
        $category_string_string = array();
        foreach ($this->load_all_catagories as $key => $value) {
            if($value == 'HL'){
                $value = "Home & Living";
            }
            if($value == 'MA'){
                $value = "Mobile Accessories";
            }

            $value = ucfirst(strtolower($value));

            $category_string_string[$value]=$value;
        }
        return $category_string_string;
    }
    public function load_colors_sizes() {
        //load color and size from config
        $this->colors = array_keys($this->config->item('colors'));
        $this->sizes = array_keys($this->config->item('sizes'));

        $colors=array();//local variable
        $sizes=array();//local variable

        foreach ($this->colors as $key => $value) {

            $value = ucfirst(strtolower($value));

            $colors[$value] = $value; //make a value,value array for drop down
        }

        foreach ($this->sizes as $key => $value) {

            $value = ucfirst(strtolower($value));

            $sizes[$value] = $value; //make a value,value array for drop down
        }
        $this->colors = $colors;
        $this->sizes = $sizes;


        /*var_dump($this->colors);
         var_dump($this->sizes);exit;
        */

    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
