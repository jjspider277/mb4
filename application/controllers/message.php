<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 *
 * Date: 07/25/2015
 * 
 */


class Message extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('message_model','messages');
    }

   /**
   * load items for sell by all active seller/memebers
   *
   */

    public function send_message() {   
       
		     //allow only ajax request
         if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
         } 

        $reciever_profile_id = $this->input->post('reciever_profile_id');

        if($reciever_profile_id==-1) {
         //this to all freinds
        

           foreach ($this->get_select_list_of_memebers($this->profile_id) as $key_as_profile_id => $value) {
                
                $post_data= array(
                'reciever_profile_id' =>$key_as_profile_id,
                'sender_profile_id' =>$this->profile_id,
                'subject' =>$this->input->post('subject'),
                'message_text' =>$this->input->post('message_text'),
                ); 

                $saved_message_id = $this->messages->send_insert_message($post_data);
                $reciever_profile_id = $key_as_profile_id;
                $saved_notification_id = $this->notifications->push_notifications("MESSAGE",$saved_message_id,NULL,$reciever_profile_id,'You recived a message!!');
                
                if($saved_notification_id) {
                      echo json_encode('Your message have been sent!');
                }
                else {
                  echo json_encode("201");
                }
           } //end of for each

        } else {

        $post_data= array(
         	'reciever_profile_id' =>$this->input->post('reciever_profile_id'),
         	'sender_profile_id' =>$this->profile_id,
         	'subject' =>$this->input->post('subject'),
         	 'message_text' =>$this->input->post('message_text'),
        );

         $saved_message_id = $this->messages->send_insert_message($post_data);

         //save notifactions
        
         $saved_notification_id = $this->notifications->push_notifications("MESSAGE",$saved_message_id,NULL,$reciever_profile_id,'You recived a message!!');
    
          if($saved_notification_id) {
              	echo json_encode('Your message have been sent!');
      		}
      		else {
      		 	echo json_encode("201");
      	  }
       } //end of else

	   }


    public function delete_message(){   
       
      $message_id = $this->input->post('id');
      //var_dump($message_id);exit;
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

      $return_id = $this->messages->update($message_id,array('message_status' =>'TRASH'));
     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
      
      if($return_id) {
          echo json_encode('Trashed succesfully!');
    }
     else {
      echo json_encode("201");
     }

     }

  public function permanent_delete_message(){   
         
        $message_id = $this->input->post('id');
        //var_dump($message_id);exit;
       //allow only ajax request
        if(!$this->input->is_ajax_request()) {
              exit('Not allowed!');
         }

        $return_id=$this->messages->update($message_id,array('message_status' =>'DELETED'));
       // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
        
        if($return_id) {
            echo json_encode('Deleted succesfully!');
      }
       else {
        echo json_encode(json_last_error(201));
       }

       }

        public function get_profile_image(){   
         
        $profile_id = $this->input->post('profile_id');
        //var_dump($profile_id);exit;
       //allow only ajax request
        if(!$this->input->is_ajax_request()) {
              exit('Not allowed!');
         }

        $image_path = $this->get_image_file_path($profile_id);
       // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
        
      if(!empty($image_path)) {
            echo json_encode(base_url($image_path),JSON_UNESCAPED_SLASHES);
      }
       else {
        echo json_encode(json_last_error());
       }

       }

    
    public function get_message(){
      

      $message_id = $this->input->post('message_id');
        //var_dump($profile_id);exit;
       //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

      $message_data = $this->messages->get($message_id);

      if(!empty($message_data)) {
            echo json_encode(array('message'=>$message_data->message));
      }
      else {
        echo json_encode(json_last_error());
      }


    }

    public function auto_save() {


        if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
        }

        $message_post = "";

        if(!empty($this->input->post('message_text2'))) {
            $message_post = $this->input->post('message_text2');
        }
        if(!empty($this->input->post('message_text'))){
            $message_post = $this->input->post('message_text');
        }

        $saved_message_id = $this->input->post('last_saved_message_id'); //no saved message at first

        if($saved_message_id==-1) { //insert new

            $post_data = array(
                'reciever_profile_id' => $this->input->post('selected_member'),
                'sender_profile_id' => $this->profile_id,
                'subject' => $this->input->post('subject'),
                'message' => $message_post,
            );

           $saved_message_id = $this->messages->save_draft_message($post_data);

            if($saved_message_id) {

                $message = array(
                    "message" => "data saved",
                    "success" => true,
                    "message_id" => $saved_message_id,
                );

            }

        } else { //update

            $last_saved_message_id = $this->input->post('last_saved_message_id');

            $post_data = array(
                'reciever_profile_id' => $this->input->post('selected_member'),
                'sender_profile_id' => $this->profile_id,
                'last_saved_message_id' => $last_saved_message_id,
                'subject' => $this->input->post('subject'),
                'message' => $message_post,
            );

            $this->messages->update_draft_message($post_data);

         }

        echo json_encode($message);

    }

    public function set_message_as_important() {


        //allow only ajax request
        if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
        }

        $message_id = $this->input->post('message_id');

        $result = $this->messages->update($message_id,array('important'=>1));

        if($result) {
            echo json_encode(array("success"=>true,));
        }


    }

    public function unset_message_as_important() {


        //allow only ajax request
        if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
        }

        $message_id = $this->input->post('message_id');

        $result = $this->messages->update($message_id,array('important'=>0));

        if($result) {
            echo json_encode(array("success"=>true,));
        }


    }


   
}
/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
