<?php
/**
 * Created by PhpStorm.
 * User: DEVELOPER4
 * Date: 12/31/2014
 * Time: 10:11 AM
 */

class Cron extends CI_Controller{

    public function __construct() {

        parent::__construct();
        $this->load->library('output');
        $this->load->model('product_model','products');
        $this->load->model('auction_model');
        $this->load->model('bid_model');
        $this->load->model('auction_winner_model');
        $this->load->library('cart');
    }

    public function find_auction_winners(){
        $auctions = $this->auction_model->get_expired_auctions();
        if($auctions){
          foreach ($auctions as $auction) {
            $bid = $this->bid_model->get_highest_bid($auction);
            if($bid){ //There's a winner
                $winner = array(
                  'profile_id' => $bid['profile_id'],
                  'bid_id' => $bid['id'],
                  'status' => "Waiting"
                );
                $this->auction_winner_model->save_winner($winner);
                $this->auction_model->close_auction($auction->id);
            }
          }
        }
    }

    public function remove_expired_auction_wins()
    {
      date_default_timezone_set('America/Los_Angeles');
      $auctions_won = $this->auction_winner_model->get_won_auctions();
      if($auctions_won){
        foreach ($auctions_won as $auction_won) {
          $now = new DateTime();
          $diff = $now->diff(new DateTime($auction_won->created_at));

          //Check if 24 hours have passed
          if($diff->y > 0 || 
            $diff->m > 0 ||
            $diff->d > 0 ||
            $diff->h >= 24 
            ){
            //The win will be revoked
            $this->auction_winner_model->expire_win($auction_won->id);

            //Product will be returned

            //Admin, seller will be notified

          }
        }
      }
    }
}