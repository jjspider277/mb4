<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 *
 * Date: 07/28/2015
 * 
 */


class Notification extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        
    }
 
    public function change_status() {   
     
     $notification_id = $this->input->post('id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

     $return_id=$this->notifications->update($notification_id,array('notification_status' =>'SEEN'));

     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );

     if($return_id) {
            
            echo json_encode('archived succesfully!');
      }
     else {
            
            echo json_encode(json_last_error(201));
      }

    }
    

     public function update_notification_seen_status() {   
     
      $notification_type = $this->input->post('ntype');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

     $updated_notification_id = $this->notifications->update_notification_seen_status($notification_type,$this->profile_id,$is_notified=true);

     //var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );

     if($updated_notification_id) {
            
            echo json_encode('notifications are now archived from top bar :)');
      }
     else {
            
            echo json_encode('error!');
      }

    }

   
}
/* End of file notifications.php */
/* Location: ./application/controllers/Dashboard.php */
