<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 *
 * Date: 07/28/2015
 * 
 */


class Friend extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('general');
        
    }

    
    public function send_friend_request() {   
     
     $reciever_id = $this->input->post('reciever_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

       //check if request is already exist or not ?

      $exist_result = $this->friends->get_by(array('sender_profile_id' =>$this->profile_id,'reciever_profile_id'=>$reciever_id));
      
      $friend_request_id = $this->friends->send_friend_request($this->profile_id,$reciever_id);

      //push notifiactions for reciver of the request

      $saved_notifiaction_id = $this->notifications->push_notifications('FRIEND_REQUEST',NULL,$friend_request_id,$reciever_id,'You have recieved a friend request!');

      //push notifiactions for sender of the request
      $saved_notifiaction_id = $this->notifications->push_notifications('SENT_FRIEND_REQUEST',NULL,$friend_request_id,$this->profile_id,'You have sent a friend request');

     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
     
      //first the recivers email address
      $sender_profile_url = base_url('sell/seller').'/'.$this->profile_id;     
      $this->load->model('profile_model','profile_reciever_sender');
      $reciever_profile = $this->profile_reciever_sender->with('user')->get($reciever_id);
      $sender_profile = $this->profile_reciever_sender->with('media')->get($this->profile_id);
      $sender_full_name = ucfirst($sender_profile->fname).' '.ucfirst($sender_profile->lname);
  
       //email issues
      $to = $reciever_profile->user->email;
      $from = "community@madebyus4u.com";
      $profile_image_path = $sender_profile->media->file_name;
      $subject = "Madebyus4u.com [ Friend Request ] from ".' '.$sender_full_name;
      $is_email_success = send_email_with_profile_detail($to,$from,$subject,$sender_full_name,$profile_image_path,$sender_profile_url,$flag=0);
     
      if($is_email_success) {
      //TODO:success
      } 

     if($friend_request_id) {
            
            echo json_encode( array('success'=>true) );
      }
     else {
            
            echo json_encode( json_last_error() );
      }

    }

    //cancle freind request 

    public function cancel_friend_request() {   
     
     $reciever_id = $this->input->post('reciever_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
      }

      //delete any data on friend request

     $friend_request_id = $this->friends->cancle_friend_request($this->profile_id,$reciever_id);
    

     if($friend_request_id) {
            
            echo json_encode( array('success'=>true) );
      }
     else {
            
            echo json_encode( json_last_error() );
      }

    }

    public function change_request_status() {   
     
     $friend_id = $this->input->post('friend_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

     $return_id = $this->friends->update($friend_id,array('seen_status' =>'SEEN'));

     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );


     if($return_id) {
            
            echo json_encode(array('success' =>true ,'message'=>'request status changed!'));
      }
     else {
            
            echo json_encode(json_last_error(201));
      }

    }

    public function accept_friend_request() {

      $friend_id = $this->input->post('friend_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

      $friends_result = $this->friends->update($friend_id,array('friend_status'=>'ACCEPT','seen_status' =>'SEEN'));

     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );

       //TODO:add friend request acceoted status to reciver and sender notification

      //get the accpted profile id from friend table
    
      $sender_profile_id = $this->friends->get($friend_id)->sender_profile_id;
     // var_dump($sender_profile_id);exit;

      //push notifiactions for reciver of the request
      $saved_notifiaction_id = $this->notifications->push_notifications('ACCEPT_FRIEND_REQUEST',NULL,$friend_id,$sender_profile_id,'Your friend request have been accepted !');

      //push notifiactions for sender of the request
      $saved_notifiaction_id = $this->notifications->push_notifications('ACCEPT_FRIEND_REQUEST',NULL,$friend_id,$this->profile_id,'Thankyou! You have accepted a friend request');


      // update notification so that user would not recived friend request notification again : 
      // 
      $updated_notification_result = $this->notifications->update_push_notification_status($friend_id,NULL,$this->profile_id,false);
      $updated_notification_id = $this->notifications->update_push_notification_status($friend_id,NULL,$sender_profile_id,false);


      //first the recivers email address
      $accept_profile_url = base_url('sell/seller').'/'.$this->profile_id;     
      $this->load->model('profile_model','profile_accepter');
      $sender_profile = $this->profile_accepter->with('user')->get($sender_profile_id);
      $accept_profile = $this->profile_accepter->with('media')->get($this->profile_id);
      $accepter_profile_full_name = ucfirst($accept_profile->fname).' '.ucfirst($accept_profile->lname);
  
       //email issues
      $to = $sender_profile->user->email;
      $from = "community@madebyus4u.com";
      $profile_image_path = $accept_profile->media->file_name;
      $subject = "Madebyus4u.com".' '.$accepter_profile_full_name."Accepted Your Friend Request";
      $is_email_success = send_email_with_profile_detail($to,$from,$subject,$accepter_profile_full_name,$profile_image_path,$accept_profile_url,$flag=1);

     if($updated_notification_result) {
            
            echo json_encode(array('success' =>true ,'message'=>'Accpeted status changed!'));
           
            //$this->load->view( $friend_view,$data);
      }
     else {
            
            echo json_encode(json_last_error());
      }

    }


    public function deny_friend_request() {

      $friend_id = $this->input->post('friend_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

      $return_id = $this->friends->update($friend_id,array('friend_status'=>'DENY','seen_status' =>'SEEN'));

     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );

     if($return_id) {
            
            echo json_encode(array('success' =>true ,'message'=>'deny status changed!'));
           
            //$this->load->view( $friend_view,$data);
      }
     else {
            
            echo json_encode(json_last_error(201));
      }

    }

     public function accept_friend_request_sellers() {

      $sender_profile_id = $this->input->post('sender_profile_id');
     
      //allow only ajax request
     
      if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }

      $where = array('sender_profile_id'=>$sender_profile_id,'reciever_profile_id'=>$this->profile_id);
      $data =  array('friend_status'=>'ACCEPT','seen_status' =>'SEEN');

      $friend_return_id = $this->friends->update_by($where,$data);


     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );

       //TODO:add friend request acceoted status to reciever and sender notification


      //push notifiactions for reciver of the request
      $saved_notification_id = $this->notifications->push_notifications('ACCEPT_FRIEND_REQUEST',NULL,$friend_return_id,$sender_profile_id,'Your friend request have been accepted !');

      //push notifiactions for sender of the request
      $saved_notification_id = $this->notifications->push_notifications('ACCEPT_FRIEND_REQUEST',NULL,$friend_return_id,$this->profile_id,'Thankyou! You have accepted a friend request');


      // update notification so that user would not recived friend request notification again : 
      // 
      $updated_notification_id = $this->notifications->update_push_notification_status($friend_return_id,NULL,$this->profile_id,false);
      $updated_notification_id = $this->notifications->update_push_notification_status($friend_return_id,NULL,$sender_profile_id,false);

      //first the recivers email address
      $accept_profile_url = base_url('sell/seller').'/'.$this->profile_id;     
      $this->load->model('profile_model','profile_accept_sender');
      $sender_profile = $this->profile_accept_sender->with('user')->get($sender_profile_id);
      $accept_profile = $this->profile_accept_sender->with('media')->get($this->profile_id);
      $accepter_profile_full_name = ucfirst($accept_profile->fname).' '.ucfirst($accept_profile->lname);
  
       //email issues
      $to = $sender_profile->user->email; 
      $from = "community@madebyus4u.com";
      $profile_image_path = $accept_profile->media->file_name;
      $subject = "Madebyus4u.com ".'  '.$accepter_profile_full_name."Accepted Your Friend Request";
      $is_email_success = send_email_with_profile_detail($to,$from,$subject,$accepter_profile_full_name,$profile_image_path,$accept_profile_url,$flag=1);


     if($updated_notification_id) {
            
            echo json_encode(array('success' =>true ,'message'=>'accept accepted changed!'));
           
            //$this->load->view( $friend_view,$data);
      }
     else {
            
            echo json_encode(json_last_error());
      }

    }

    
    

   
}
/* End of file notifications.php */
/* Location: ./application/controllers/Dashboard.php */
