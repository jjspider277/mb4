<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 06/04/2015
 * Time: 10:11 AM
 */

class Likes extends MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('like_model','likes');
        $this->load->model('product_model','products');
		$this->load->helper('text');
        ///$this->load->model('noti','likes');
 
    }

    public function like_product() {
 
  		 //allow only ajax request
         if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
         }

        $liked_product_id = $this->input->post('product_id');
		$liker_profile_id = $this->profile_id;

		//get the profile owner of the product 
		

		//var_dump($liker_profile_id);
	    
	    $product_ids = $this->get_current_profile_products($liker_profile_id);
        
		//TODO:validate the like shoudl not be liked more than once by a single profile / user
		//throw status of the button change signal to unlike 
		//can't rate your self or your own product :)

		if(in_array($liked_product_id,$product_ids)) {

			echo json_encode( array('error' => 'Gotcha! You can\'t like your own product :)!',));
			exit;

		} 

		//get the owner of the product to notify ?
		$for_profile_id = $this->products->get($liked_product_id)->profile_id;

		//var_dump($rate_type.$rated_item_id.$product_ids);exit;
        $result = $this->likes->save_product_liking($liked_product_id,$liker_profile_id,$for_profile_id);
		//do the actual calculation of rating average weight
		//$rated=$this->rating->call_rating_stored_procedure($rated_item_id,$rate_type);

		
		$by_profile_id = $liker_profile_id;

		if($result)
		{
	
	    $saved_notifiaction_id = $this->notifications->push_notifications_for_likes('LIKE',$for_profile_id,'Your product is liked!',$by_profile_id,$liked_product_id);
        //push notifiactions for sender of the request // to do add more parameters for by progfile and producy details
      	$saved_notifiaction_id = $this->notifications->push_notifications('LIKED',NULL,NULL,$liker_profile_id,'You liked a product!');	

        if($saved_notifiaction_id)		
 		 $response = array('success' =>true,);
		}
		else 
		{
		$response = array('success' =>false,);
		}

		echo json_encode($response);

	   }

}