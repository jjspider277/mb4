<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/31/2014
 * Time: 10:11 AM
 * Craig Robinson 
 * www.cytekservices.com 
 */


class Store extends  MY_Controller {

    var  $load_all_catagories ;

    var  $colors ;

    var  $sizes ;

    public function __construct() {

        parent::__construct();
        
        $this->load->model('profile_model','profile');
        $this->load->model('store_model','store');
        $this->load->model('shipping_model','shipping_address');

        $this->load->model('category_model','categories');
        $this->load_all_catagories = $this->load_catagories();

        $this->load->library('form_validation');
        $this->load_colors_sizes();
        $this->load->library('imageutility_service.php');
        //$this->session->unset_userdata('product_images');
        //$this->session->unset_userdata('store_image');
        $this->load->model('category_model','menu_categories');
        $this->load->helper('text');
        $this->load->model('category_model','menu_categories');

    }

   public function post_image() {


     $profile_id = $this->profile_id;

     //allow only ajax request
     
      /*if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');
       }*/

      $store_image_session_data = null;

      $img_id =  $this->input->post('img_src_id'); 

      if($img_id == 'store_img' || $img_id == 'store_img_edit' ) {

        //this store image upload
       
       foreach($_FILES as $file) {

          $imgData = base64_encode(file_get_contents($file['tmp_name']));
          $src = 'data:'.$file['type'].';base64,'.$imgData;
          $pathToUpload = "uploads/temp/".$profile_id . "/store/";
           //rename files first
          $temp = explode(".", $file["name"]);
          $newfilename = 'store_image' . rand(1, 99999) . '.' . end($temp);

          if (!is_dir($pathToUpload)) 
              mkdir($pathToUpload, 0777, TRUE);
              
            @move_uploaded_file($file['tmp_name'], $pathToUpload.$newfilename);
            

          $store_image_insert_data = array (     
                  'file_name'=>$newfilename,
                  'path'=>$pathToUpload,
                  'type' => 'store_image',
                  'profile_id' => $profile_id,
                  'image_id' => $img_id,
          );

          $result_data = $this->db->get_where('temp_files', array('profile_id' => $this->profile_id,'type'=>'store_image','image_id'=>$img_id,));

         // dump( $result_data );

           if( $result_data->num_rows() >0 )
           {

             $product_image_update_data = array (
                      'file_name'=>$newfilename,
                      'path'=>$pathToUpload,
                  );

               $where_data = array('profile_id' =>$profile_id ,'image_id'=>$img_id);
               $this->db->where($where_data);
               $this->db->update('temp_files', $product_image_update_data);

               $result_array = $result_data->result();
              
               $path_to_del = $pathToUpload.$result_array[0]->file_name;
               //dump($path_to_del);
               @unlink($path_to_del);

           }
           else 
           {
                $this->db->insert('temp_files',$store_image_insert_data);
           }

           echo $src;
        }

      }    //end of store coool :)

      //save for product

        $product_image_session_data = null;

         if($img_id != 'store_img' && $img_id != 'store_img_edit') {

        //this product image upload
        foreach($_FILES as $file) {

          $imgData = base64_encode(file_get_contents($file['tmp_name']));
          $src = 'data:'.$file['type'].';base64,'.$imgData;

          $pathToUpload = "uploads/temp/".$profile_id. "/products/";
        
           //rename files first
          $temp = explode(".", $file["name"]);
          $newfilename = 'product_image' . rand(1, 99999) . '.' . end($temp);

            if (!is_dir($pathToUpload))   
                mkdir($pathToUpload, 0777, TRUE);
             
             move_uploaded_file($file['tmp_name'], $pathToUpload.$newfilename);
            

            $product_image_insert_data = array (
                     'file_name'=>$newfilename,
                      'path'=>$pathToUpload,
                       'image_id'=>$img_id,
                       'type' => 'product_image',
                       'profile_id'=>$profile_id,
                  );


           $get_img_id = null;

          $result_data = $this->db->get_where('temp_files', array('profile_id' => $this->profile_id,'type'=>'product_image','image_id'=>$img_id));
           
           //dump($result_data->result());

           if( $result_data->num_rows()  > 0 )
           {
               
               $product_image_update_data = array (
                     'file_name'=>$newfilename,
                      'path'=>$pathToUpload,
                  );
               $where_data = array('profile_id' =>$profile_id ,'image_id'=>$img_id);
               $this->db->where($where_data);
               $this->db->update('temp_files', $product_image_update_data);
               
               $result_array = $result_data->result();
               $path_to_del = $pathToUpload.$result_array[0]->file_name;
               @unlink($path_to_del);
             

           }
            
            else 
            {
                 
              $this->db->insert('temp_files',$product_image_insert_data);
               
            }

           echo $src;       
         }

      }

     }

    public function load_colors_sizes() {
      //load color and size from config
      $this->colors = array_keys($this->config->item('colors'));
      $this->sizes = array_keys($this->config->item('sizes'));

      $colors=array();//local variable
      $sizes=array();//local variable

     foreach ($this->colors as $key => $value) {
       
          $value = ucfirst(strtolower($value));

          $colors[$value] = $value; //make a value,value array for drop down
      }

       foreach ($this->sizes as $key => $value) {
       
        $value = ucfirst(strtolower($value));

         $sizes[$value] = $value; //make a value,value array for drop down
      }
       $this->colors = $colors;
       $this->sizes = $sizes;


       /*var_dump($this->colors);
        var_dump($this->sizes);exit;
        */

    }

    public function load_catagories() {

        $categories = $this->categories->get_all_parent_categories();
        $category_string_string = array();
        foreach ($categories as $key => $category) {

            $category = ucfirst(strtolower($category->category));

            $category_string_string[$category]=$category;
        }
        ///dump($category_string_string);exit;
        return $category_string_string;
    }


    public function get_categories_variation_ajax() {
           
         //allow only ajax request
         if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');

         } 

         $categories_selected = $this->input->post('category');  
         $load_all_catagories = $this->config->item('categories');

        
         $result = array_values($load_all_catagories[strtoupper($categories_selected)]);
         $subcategories = array();
         foreach($result as $key => $value) {
        
           if(is_array($value)){
           continue;
           }
           $subcategories[$key] = $value;
          
         }
         
         
        if(!empty($subcategories))   
        $subcategories =  array('#'=>'Please Select Variation')+$subcategories;
            
        header('Content-Type: application/x-json; charset=utf-8'); 
        echo ( json_encode( $subcategories )  );

         
       }

    public function get_sub_variation_ajax() {
           
        if(!$this->input->is_ajax_request()) {

            exit('Not allowed!');

         } 
           
         $categories_selected = $this->input->post('category');  
         $variation_selected = $this->input->post('variation'); 
         $load_all_catagories = $this->config->item('categories');

         $result = $load_all_catagories[strtoupper($categories_selected)][$variation_selected];
         $subcategories = array();
                        
          if(!empty($result))  
          $result =  array('#'=>'Please Select Variation')+$result;   
          header('Content-Type: application/x-json; charset=utf-8'); 
          echo ( json_encode( $result  ) );

    }

    public function load_country_state(){

         /** 

          Read from
          configuration file
          and construct
          STRING,STRING Array list
         
         **/
        $country_list = $this->config->item('country');
        $country = array();

        foreach ($country_list as $key => $value) {
           $country[$value] = $value;
        }


        $states_list = $this->config->item('state');

        $states = array();

        foreach ($states_list as $key => $value) {
           $states[$value] = $value;
        }

        return array('state'=>$states,'country'=>$country);
 }



   /**
   * load items for sell by
   * all active
   * seller / members
   *
   */

    public function setup($profile_id){

        //TODO:integrate resize with every image upload
        //$this->load->library('imageutility_service');
        //$result = $this->imageutility_service->resize_upload_images('','');
        //if user has created a store before and access using the url it must not see page

        $tab_status = false ;
        $store_setup_completed = false;
        if( empty($post)&&($profile_id!=$this->profile_id) ) {

            //stay wher you are , you dangerous! :)
            $referrer = $_SERVER['HTTP_REFERER'];
            redirect($referrer);
            //redirect('sell/seller/'.$this->profile_id);
        }

       if(count( $this->store->get_by(array('owner_profile_id'=>$this->profile_id)) ) >0 ) {
         //stay where you are , we dont need you on this page anymore! STAY AWAY :)
          $referrer = $_SERVER['HTTP_REFERER'];
          redirect($referrer);
        }

        //check if user has validated or not and hide verification tab
        if( $this->profile->check_verfication($this->profile_id) ) {

              $tab_status = true ;
              $store_setup_completed = false ;
              $data['tab_status'] = $tab_status;
              $this->message = array('type' => 'success','message' =>"Since you have verified your account successfully !! <small> Now can continue creating your store. Please, press to <strong>'Continue'</strong> </small>");
              $data['data']['message'] = $this->message;
         }

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_listing = 'product/product_listing_pages';
        /**tabs***/
        $identity_validation_page = 'storesetup/identity_validation_page_new';
        $store_page = 'storesetup/store_page_new';
        $addproduct_page = 'storesetup/addproduct_new';
        $getpaid_page = 'storesetup/getpaid_new';
        $previewstore_page = 'storesetup/preview_store_new';
        $launchstore_page = 'storesetup/launchstore';
        $data['identity_validation_page'] = $identity_validation_page;
        $data['store_page'] = $store_page;
        $data['addproduct_page'] = $addproduct_page;
        $data['getpaid_page'] = $getpaid_page;
        $data['previewstore_page'] = $previewstore_page;
        $data['launchstore_page'] = $launchstore_page;

        $account_types = $this->config->item('account_types');
        $data['account_types'] = $account_types;

        $main_menu = 'include/main_menu';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';

        $data['country'] = $country;
        $data['states'] = $states;

        $data['product_listing'] = $product_listing;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;

        $data['show_error_page'] = 'include/show_error_page';
        $data['data']['message'] = $this->message;

        //disable other tabs except verification tab
        $data['store_setup_completed'] = $store_setup_completed; //
        $data['tab_status'] = $tab_status;

        $data['profile_id'] = $this->profile_id ;// :)

        $this->data= $this->load_profile_detail_with_image($this->profile_id);
       // var_dump($this->data);exit;
        $data = array_merge($data,$this->data);


        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';
        $data['categories'] = $this->load_all_catagories;
        
        $this->load->view('storesetup/store_new',$data);

    }

    public function process_payment($profile_id){


        // var_dump($post);
        //TODO:Server Side Validation here
        $tab_status = TRUE ;
        if( empty($post)&&($profile_id!=$this->profile_id) ) {
            redirect('sell/seller/'.$this->profile_id);
        }
       $data['store_setup_completed'] = FALSE; //
       $data['profile_id'] = $this->profile_id ;// :)

       if( !$this->profile->check_verfication($this->profile_id) ) {
       
               $this->load->library('payment_service');
               $result = $this->payment_service->processPayment($profile_id);

           if(!$result){
                        $data['data']['message'] = $this->message;
                        //$this->session->set_flashdata($data['data']['message']);
                        //disable other tabs except verification tab
                        $data['tab_status'] = FALSE; // :(
           }

       else {

             $this->message = array('type' => 'success','message' =>"You have verified your account successfully! ! <small>Now can continue creating your store by using the TAB menus.</small>");
             $data['data']['message'] = $this->message;
             $data['tab_status'] = TRUE; // :)
   
        }

        } else {

             $this->message = array('type' => 'success','message' =>"You have verified your account successfully! ! <small>Now can continue creating your store by using the TAB menus.</small>");
             $data['data']['message'] = $this->message;
             $data['tab_status'] = TRUE; // :)
   
        }

        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $account_types = $this->config->item('account_types');

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_listing = 'product/product_listing_pages';

        /**Tabs***/
        $identity_validation_page = 'storesetup/identity_validation_page';
        $store_page = 'storesetup/store_page';
        $addproduct_page = 'storesetup/addproduct';
        $getpaid_page = 'storesetup/getpaid';
        $previewstore_page = 'storesetup/previewstore';
        $launchstore_page = 'storesetup/launchstore';
        $data['identity_validation_page'] = $identity_validation_page;
        $data['store_page'] = $store_page;
        $data['addproduct_page'] = $addproduct_page;
        $data['getpaid_page'] = $getpaid_page;
        $data['previewstore_page'] = $previewstore_page;
        $data['launchstore_page'] = $launchstore_page;
        //*end**tab*/
        //get the current profile of the user
        $profile_data = $this->profile->get($this->profile_id);
        $data['fname'] = $profile_data->fname;
        $data['lname'] = $profile_data->lname;
        $data['zipcode'] = $profile_data->zipcode;
        $data['account_types'] = $account_types;

        $main_menu = 'include/main_menu';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['show_error_page'] = 'include/show_error_page';

        $data['country'] = $country;
        $data['states'] = $states;

        $data['product_listing'] = $product_listing;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;

        $this->data = $this->load_profile_detail_with_image($this->profile_id);
        $data = array_merge($data,$this->data);

        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';
        $data['categories'] = $this->load_all_catagories;

        $this->load->view('storesetup/store_new',$data);
    }

  public function save_store($profile_id) {

     // echo $_SERVER['HTTP_REFERER'];

      $data['column_main_menu'] = 'include/column_menu_top';

      $tab_status = TRUE ; 

      if( empty($post)&&($profile_id!=$this->profile_id) ) {
          redirect('sell/sellers_new/'.$this->profile_id);
      }

      //capture post data
      $post = $this->input->post();
     //var_dump($post['zip_code']);exit;
      
      //Server Side Validation here
      $this->form_validation->set_rules('storename', 'StoreName', 'trim|required|min_length[2]|max_length[45]|xss_clean');
      $this->form_validation->set_rules('product_details', 'Product_details', 'trim|required|min_length[2]|xss_clean');     
      $this->form_validation->set_rules('store_description', 'store_description', 'trim|required|xss_clean');
      $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|min_length[2]|max_length[30]|xss_clean');
      $this->form_validation->set_rules('product_description', 'Product Description', 'trim|required|xss_clean');
      $this->form_validation->set_rules('categories', 'Categories', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('colors', 'Colors', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_rules('sizes', 'Sizes', 'trim|required|callback_check_default|xss_clean');
      $this->form_validation->set_message('check_default','For categories please ,select something other than the default');
      $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('price', 'Product Name', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('sprice', 'Special Price', 'trim|required|xss_clean|numeric');
      $this->form_validation->set_rules('account_type', 'Account Type', 'trim|required|xss_clean');
      $this->form_validation->set_rules('bankbranch', 'Bank Branch', 'trim|required|min_length[2]|max_length[45]|xss_clean|');
      $this->form_validation->set_rules('account_owner', 'Account_owner', 'trim|required|min_length[2]|max_length[45]|xss_clean|numeric');
      $this->form_validation->set_rules('routenumber', 'Route Number', 'trim|required|min_length[5]|max_length[45]|xss_clean|numeric');
      $this->form_validation->set_rules('account_type', 'Categories', 'trim|required|callback_check_default_account|xss_clean');
      $this->form_validation->set_message('check_default_account','For account type please ,select something other than the default');
      $this->form_validation->set_rules('accountnumber', 'Account Number', 'trim|required|xss_clean|matchs[reaccountnumber]');
      $this->form_validation->set_rules('reaccountnumber', 'Re-Enter Account number', 'trim|required|xss_clean|');
      $this->form_validation->set_rules('account_owner', 'Account_owner', 'trim|required|min_length[2]|max_length[45]|xss_clean');

      //TODO: shipping information related validation needs to be added later on
      //at first User can save empty and later when edit store he should be able to save ,
      //must figure out when fields are need to be required


       //if validation success do the following
      if ( $this->form_validation->run() == TRUE) {

          $this->load->model('media_model','media');

          $file_post_name = 'userfile';

         //TODO:resize
        //TODO:SUCCESS DO THE SAVING of STORE WITH MEDIA DATA / UPLOAD DATA

        //save store
            $store_id = $this->store->save_store($post,$this->profile_id);

         /*save shipping information
              $shipping_address_id =  $this->shipping_address->save_shipping_for_seller_profile($post,$this->profile_id);

           if($shipping_address_id!=-1){
              associate the memeber profile id with shipping address
                  $this->profile->update($this->profile_id,array('shipping_id' => $shipping_address_id));
         } **/


        //get store image id
        $store_image_id = $this->media->save_or_update_store($this->profile_id,$store_id);

         //save products and its media
        $this->load->model('product_model','product');

        $product_id = $this->product->add_lisiting($post,$this->profile_id,$store_id);

          //to be inserted
          $array_of_product_image_ids = array();
          $where_data = array('profile_id' =>$this->profile_id ,'type'=>'product_image');
          $result_data_products =  $this->db->get_where('temp_files',$where_data);

          $images_found = array();

          foreach($result_data_products->result() as $row) {

              $check_image_id = substr($row->image_id, 3,1);

              $images_found[] = $check_image_id;

          }


         //to be inserted
         $array_of_product_image_ids = array();
         $where_data = array('profile_id' =>$profile_id ,'type'=>'product_image');
         $result_data_products =  $this->db->get_where('temp_files',$where_data);

       if( $result_data_products->num_rows() > 0 ) {
         //insert the five product image into media table and collect inserted id
       
        foreach($result_data_products->result() as $row){

            $array_of_product_image_ids[] = $this->media->save_or_update_product_temp_files($row,$this->profile_id,$store_id,$product_id);
         }

         $missing_images = array();

         for($i=1;$i<=5;$i++) {

               if( in_array($i,$images_found) == false ) {
                   $missing_images[] = $i;
               }
        }

       foreach($missing_images as $missing_image) {

               $insert_data =  array(
                   'type' => 'product_image',
                   'file_name' => 'image-upload.jpg',
                   'full_path' => base_url('assets/images/image-upload.jpg'),
                   'profile_id' => $this->profile_id,
                   'product_id'=> $product_id,
                   'store_id'=> $store_id,
               );

               $this->media->insert($insert_data);
       }
        //NOW SAVE ACCOUNT INFORMATION

         $this->load->model('account_model','account');

         $account_id = $this->account->save_account_profile($profile_id,$store_id,$post);

          if($store_image_id) {

              $this->store->update($store_id,array('media_id'=>$store_image_id));                  
                   
          }              
           
        
        //disable other tabs except verification tab
 
         $data['store_setup_completed'] = TRUE; //
         $data['tab_status'] = TRUE; //

         $this->message = array('type' => 'success','message' =>"You have setup you store succesfully!! <small>Now can continue viewing your store.</small>");
         $data['data']['message'] = $this->message;

          //twista YUR BODY - SNOOP DOGG //i will be back again :)

          $this->clean_up_may_day($this->profile_id);

        }

      } else {

          $data['store_setup_completed'] = FALSE; //
          $data['tab_status'] = TRUE; //
          $this->message = array('type' => 'error', 'message' => validation_errors() );
          $data['data']['message'] = $this->message;
      }

        $data['profile_id'] = $this->profile_id ;// :)
        $list_of_country_state_data = $this->load_country_state();
        $states = $list_of_country_state_data['state'];
        $country = $list_of_country_state_data['country'];

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_listing = 'product/product_listing_pages';
        /**tabs***/
        $identity_validation_page = 'storesetup/identity_validation_page_new';
        $store_page = 'storesetup/store_page_new';
        $addproduct_page = 'storesetup/addproduct_new';
        $getpaid_page = 'storesetup/getpaid_new';
        $previewstore_page = 'storesetup/preview_store_new';
        $launchstore_page = 'storesetup/launchstore_new';
        $data['identity_validation_page'] = $identity_validation_page;
        $data['store_page'] = $store_page;
        $data['addproduct_page'] = $addproduct_page;
        $data['getpaid_page'] = $getpaid_page;
        $data['previewstore_page'] = $previewstore_page;
        $data['launchstore_page'] = $launchstore_page;
        //*end**tab*/
        //get the current profile of the user
        $profile_data = $this->profile->get($this->profile_id);
        $data['fname'] = $profile_data->fname;
        $data['lname'] = $profile_data->lname;
        $data['zipcode'] = $profile_data->zipcode;

        $account_types = $this->config->item('account_types');

        $main_menu = 'include/main_menu';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';

        $data['country'] = $country;
        $data['states'] = $states;
        $data['account_types'] = $account_types;

        $data['product_listing'] = $product_listing;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;

        $data['show_error_page'] = 'include/show_error_page';
        $data['data']['message'] = $this->message;

        $this->data= $this->load_profile_detail_with_image($this->profile_id);
        $data = array_merge($data,$this->data);
        //var_dump($data);
        $data['catagories'] = $this->load_all_catagories;
        $data['colors'] = $this->colors;
        $data['sizes'] = $this->sizes;

      $all_categories = $this->menu_categories->get_all();
      $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
      $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
      $data['all_categories'] = $all_categories;
      $data['sub_parent_categories'] = $sub_parent_categories;
      $data['parent_categories'] = $parent_categories;
      $data['column_main_menu'] = 'include/column_menu_top';
      $data['categories'] = $this->load_all_catagories;

      $this->load->view('storesetup/store_new',$data);


       
  }

  public function store_listing($store_id){


        $this->load->library('pagination');   
        $config = array();
        $total_rows = $this->store->get_how_many_products_per_store($store_id);
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $config['display_pages'] = FALSE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
        $this->load->helper('general');
        //$twee_texts = twitter_helper(4,10);
        //$tweetes_content = $twee_texts;
        $tweets_page = 'include/tweets';
        $video_page = 'include/video_page';
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $main_menu = 'include/main_menu';      
        $data['header_black_menu'] = 'include/header_black_menu';
        $store_products='store/store_products';
        $data['store_products']=$store_products;
        $data['paginate_page'] = $paginate_page;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['tweets_page'] = $tweets_page;
        $data['video_page'] = $video_page;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        //$data['data']['tweetes_content'] = $tweetes_content;
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';

        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

        //fetch products
        $products_in_the_store = $this->store->get_store_lisiting_products($store_id,$config["per_page"],$page);
        $data["links"] = $this->pagination->create_links();       
          
        $data["products"] = $products_in_the_store;    

       // var_dump($products_in_the_store);exit;

        $data['total_rows'] = $total_rows;
        $data["per_page"] = $config["per_page"] ;
        $data["page"] = $page;


        $this->load->view('store/store_product_listing',$data);



  }

    function check_default($value) {
        if($value!='#'){
            return true;
        }
        return false;
    }

    function check_default_account($value) {
        if($value!='#'){
            return true;
        }
        return false;
    }

}