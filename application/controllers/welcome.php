<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {


    public function __construct() {

       parent::__construct() ;
       $this->load->model('product_model','products');
       $this->load->model('media_model','media');

        $this->load->model('category_model','menu_categories');

       $this->load->helper('text');



    }
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data['email_form'] = 'include/email_form';
        $this->load->view('coming_soon',$data);
	}

    /**
     * subscribe function that recives email address
     * register it on to database for future notifications
     * @post email_address
     */
    public function subscribe()
    {
 

     $email = $this->input->post('email_address',TRUE);


     if ( $email ) {

         $this->load->helper('general'); //load email library

         $from ='support@madebyus4u.com';
         $to = $email ;
         $subject = 'MadeByus4u.com subscription successful!';
         $content = 'Thankyou for subscribing at madebyus4u.com!';

         if (send_email_new('',$from,$to,$subject,$content)) {

             //show_error($this->email->print_debugger());
             $content = $this->email->print_debugger();

           } else {

             $content = 'Thankyou for subscription using '. $email .'</b> please check your email!';
             //send email json message for the view

          }

          $message = array (
             "message" => $content,
          );
          //TODO:save the subscribed on database

        echo json_encode($message);
     }
}

    function get_all_categories() {
 
        //fetch categories from config
        //and construct an array of [ STRING,STRING ]
        $categories_all = $this->config->item('categories_all');

        foreach($categories_all as  $cat=>$val){
            $categories_all[$val] = $val;
        }
        return  $categories_all;
       
    }

    public function home() {

       

        $this->load->library('pagination');

        $total_rows = $this->products->count_all();
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
    

        $config = array();

        $config["base_url"] = base_url() . "welcome/home/";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 8;
        $config["uri_segment"] = 3;

        $config['display_pages'] = FALSE;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $products = $this->products->display_all_product($config["per_page"],$page);
     
        $data["products"] = $products;

        $data["links"] = $this->pagination->create_links();
        
        $data['home_page_white_menu'] = 'home/home_page_white_menu';
        $data['main_menu'] = 'include/main_menu';
        $data['column_main_menu'] = 'include/column_menu_top';
        $data['footer_page'] = 'include/footer_page';
        $data['product_listing'] = 'product/product_listing_new';

        $categories_all = $this->get_all_categories();
        $data['catagories_all'] = $categories_all;
        $data['footer_findout'] = 'include/footer_findout';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';

        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>''));
        $data['all_categories'] = $all_categories;


        $this->load->view('home/home_view_new',$data);

    }

    public function shop() {

        $data['footer_page'] = 'include/footer_page';
        $this->load->view('shop/shop',$data);
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */