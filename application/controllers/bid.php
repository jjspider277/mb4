<?php
/**
 * Created by PhpStorm.
 * User: DEVELOPER4
 * Date: 12/31/2014
 * Time: 10:11 AM
 */

class Bid extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('general');
        $this->load->model('product_model','products');
        $this->load->model('category_model','menu_categories');

        $this->load->model('auction_model');
        $this->load->model('bid_model');
        $this->load->library('cart');
    }

    public function index(){


       $tweets_page = 'include/tweets';
       $video_page = 'include/video_page';
       $paginate_page = 'include/paginate_page';
       $notification_bar = 'include/notification_bar';
       $header_logo_white = 'include/header_logo_white';
       $main_menu = 'include/main_menu';



      $this->load->library('pagination');

      $config = array();

      //TODO:check a condition
      $total_rows = $this->products->count_all();
      $config["base_url"] = base_url() . "bid/index";
      $config["total_rows"] = $total_rows;
      $config["per_page"] = 12;
      $config["uri_segment"] = 3;
      $config['display_pages'] = FALSE;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $profile_id = $this->session->userdata('profile_id');
        //fecth products
      $products = $this->products->display_auction_products($profile_id,$config["per_page"],$page);
      $data["products"] = $products;
      $data["links"] = $this->pagination->create_links();
        $now_date = date('Y-m-d H:i:s');


     //acccess TWitter API
      $twee_texts = '';//twitter_helper(4,10);
      $tweetes_content = $twee_texts;
      $tweets_page = 'include/tweets';
      $video_page = 'include/video_page';
      $paginate_page = 'include/paginate_page';
      $header_logo_white = 'include/header_logo_white';
      $main_menu = 'include/main_menu';
      $data['header_black_menu'] = 'include/header_black_menu';
      $product_listing_column='product/product_listing_column';
      $data['header_black_menu'] = 'include/header_black_menu';
      $data['tweets_page'] = $tweets_page;
      $data['video_page'] = $video_page;
      $data['paginate_page'] = $paginate_page;
      $data['header_logo_white'] = $header_logo_white;
      $data['main_menu'] = $main_menu;
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';


        $data['notification_bar'] = 'include/notification_bar';
      $data['data']['tweetes_content'] = $tweetes_content;
      $data['product_listing_column']=$product_listing_column;
      $data['paginate_page'] = $paginate_page;
      $data['header_logo_white'] = $header_logo_white;
      $data['main_menu'] = $main_menu;
      $data['tweets_page'] = $tweets_page;
      $data['video_page'] = $video_page;
      $data['paginate_page'] = $paginate_page;
      $data['notification_bar'] = $notification_bar;
      $data['data']['tweetes_content'] = $tweetes_content;

      $data['footer_privacy'] = 'include/footer_privacy';
      $data['footer_subscribe'] = 'include/footer_subscribe';

      $data["products"] = $products;
      $data["button_text"] = "Bid Now!";

      $data['total_rows'] = $total_rows;
      $data["per_page"] = $config["per_page"] ;
      $data["page"] = $page;



      $data = array_merge($data,$this->data);


      $this->load->view('bid/bid_new',$data);

    }

    public function confirm_bid($id){

        date_default_timezone_set('America/Los_Angeles');
      // var_dump($product);die;
    $auctions = $this->auction_model->get_auction_by_id($id);
       // var_dump($id);die;
    if($auctions)
      $product = $this->products->get_single_product_detail($auctions->product_id);

    if(! $this->input->post('price') || empty($auctions)){
      $referrer = $_SERVER['HTTP_REFERER'];
       // var_dump($auctions);die;
       redirect($referrer);

    }
    // var_dump($this->is_logged_in);die;
    // $now = new DateTime();
    //     $diff = $now->diff(new DateTime($auctions->end_date));
    //     $data['diff'] = $diff;
    $price = $this->input->post('price');

    if($this->input->post('confirm') == 'true'){
      $bid = array(
        'profile_id' => $this->profile_id,
        'auction_id' => $auctions->id,
        'price' => $price
      );

      if($this->bid_model->save_bid($bid)){

        $return = $this->auction_model->update_current_bid($auctions->id,
          $price);
        if($price >= $auctions->buy_now_price){
          $insert_data = array(
            'id' => $product->product_id,
            'name' => $product->product_name,
            'price' => $price,
            'qty' => 1,
            'seller' => $product->profile->seller_name,
            'seller_profile' => $product->profile->id,
            'image' => $product->image[0],
            'shipping_price' => $product->shipping_price,
          );

          $this->cart->insert($insert_data);
          //$this->auction_model->update_status($auctions->id, false);

          redirect('shopcart');
        }
        $message = array(
            'type' => 'success',
            'message' =>"Auction is setup succesfully!"
          );
        $this->session->set_flashdata(array('message' => $message));
        redirect('bid/product/'.$auctions->id);
      }
    }
    if(($auctions->current_bid != null &&
      $price > $auctions->current_bid) ||
      $price > $auctions->bid_price){

      $data['header_black_menu'] = 'include/header_black_menu';
     $data['header_logo_white'] = 'include/header_logo_white';
     $data['footer_privacy'] = 'include/footer_privacy';
     $data['footer_subscribe'] = 'include/footer_subscribe';
     $data['signin_form'] = 'include/signin_form';
     $data['new_to_madeby'] = 'include/new_to_madeby';
    $notification_bar = 'include/notification_bar';
     $data['data']['message'] =  $this->message;
     $data['auctions'] = $auctions;
     $data['product'] = $product;
     $data['price'] = $price;
      $this->load->view('bid/confirm_bid', $data);
        //var_dump($data);die;
    } else {
      $referrer = $_SERVER['HTTP_REFERER'];
       redirect($referrer);
    }


    }

    public function product($auction_id){
        date_default_timezone_set('America/Los_Angeles');
        //get the owner of the product to notify ?
        //dump($id);
        //var_dump($for_profile_id);exit;
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $is_bid_buy_page = 'bid';
        $id = $this->uri->segment(3);

        $profile = $this->profile->get( $this->profile_id);
        //print_r($profile);
       // die;
        $auctions = $this->auction_model->get_auction_by_id($auction_id);

        $bids = $this->bid_model->get_bids($auctions->id);
        $data['bids'] = $bids;
        $now = new DateTime();
        $diff = $now->diff(new DateTime($auctions->end_date));
        $data['diff'] = $diff;

        //diff->invert checks if the auction's end time passed
        if( !empty($id) && !empty($auctions) && ! ($diff->invert))  {

        //manipulate like status and show accrodingly on view liked or like to be liked :{}
        $liked_or_not = $this->likes->
        check_if_liked_before($auctions->product_id,$this->profile_id);
        $product = array();
        $exist_or_not = $this->products->get($auctions->product_id);
        $config["per_page"] = 10;
        if( !empty($exist_or_not) && count($exist_or_not) >0 ) {
          $product = $this->products->get_single_product_detail($auctions->product_id);

          $data['seller_products'] = $this->products->get_all_seller_product_lisiting($product->profile->id,$product->product_id, $config["per_page"],$page);
        } else {
             $referrer = $_SERVER['HTTP_REFERER'];
             redirect($referrer);
        }


        //check if the current product is already the logged in user or not

        $product_ids = $this->get_current_profile_products($this->profile_id);
        $is_the_product_current_user = false;

        if(in_array($auctions->product_id,$product_ids)) {

            $is_the_product_current_user = false;
        }

        $this->db->select('*');
        $this->db->where("product_id", $auctions->product_id );
        $this->db->where("parent_comment_id", 0);
        $query = $this->db->get("comments");
        $comments = $query->result_array();
        $config["per_page"] =4;

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white_new';
        $product_user_details = 'product/product_user_detail';
        $product_other_details = 'bid/product_other_listing_new';
        $comment_view = 'comment/comment_view';
        $main_menu = 'include/main_menu';
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

        $data['main_menu'] = $main_menu;
        $data['header_logo_white'] = $header_logo_white;
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['social_sharing_button'] = 'include/social_sharing_buttons';


        $data['product_user_details']= $product_user_details;
        $data['product_other_details']= $product_other_details;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['comment_view'] = $comment_view;

       // $data['joined_date'] =  date_format(date_create($product->joined_date) ,'Y-m-d');

        $data['product'] = $product;
        $data['is_liked'] = $liked_or_not;
        $data['is_the_product_current_user'] = $is_the_product_current_user;

        $data['is_bid_page'] = strcmp(strtolower($is_bid_buy_page),"bid")==0;
        $data['is_buy_page'] = strcmp(strtolower($is_bid_buy_page),"buy")==0;
        $data["is_logged_in"] = $this->is_logged_in;
        $data['profile'] = $profile;
        $data['comments'] = $comments;
        $data['auctions'] = $auctions;

            $profile_id = $this->session->userdata('profile_id');
            $products = $this->products->display_other_auction_products($profile_id,$product->product_id,3,$page);
      
            $data["products"] = $products;
            //var_dump($product->profile->id);die;
            $all_seller_product = $this->products->get_all_seller_product_lisiting($product->profile->id,$product->product_id,$config["per_page"], $page);
           // var_dump($all_seller_product);exit;
            $data['all_seller_products'] = $all_seller_product;

            //var_dump($all_seller_product);die;
            $data['page'] = $page;


        $this->load->view('bid/bid_detail_new',$data);

     }  //end if varible are null show error below

     else {

     //TODO:error pages .:) well done
     $referrer = $_SERVER['HTTP_REFERER'];
     redirect($referrer);
    }
   }
}