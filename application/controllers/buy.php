<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/31/2014
 * Time: 10:11 AM
 */

class Buy extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper('general');
        $this->load->model('product_model','products');
        $this->load->model('video_model','videos');
        $this->load->model('category_model','menu_categories');
        $this->load->helper('typography');
        $this->load->helper('text');
    }

    public function index(){

        $this->load->library("pagination");   
        $config = array();
        $total_rows = $this->products->count_all();
        $config["base_url"] = base_url()."buy/index";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $config["display_pages"] = FALSE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 

        $twee_texts = twitter_helper(3,1);
        $tweetes_content = $twee_texts;
        $tweets_page = "include/tweets";
        $video_page = "include/video_page";
        $paginate_page = "include/paginate_page";
        $notification_bar = "include/notification_bar";
        $header_logo_white = "include/header_logo_white";
        $main_menu = "include/main_menu";      
        $data['header_black_menu'] = "include/header_black_menu";
        $product_listing_column="product/product_listing_column";
        $data["product_listing_column"]=$product_listing_column;
        $data["paginate_page"] = $paginate_page;
        $data["header_logo_white"] = $header_logo_white;
        $data["main_menu"] = $main_menu;
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

        $data["tweets_page"] = $tweets_page;
        $data["video_page"] = $video_page;
        $data["paginate_page"] = $paginate_page;
        $data["notification_bar"] = $notification_bar;
        $data["data"]["tweetes_content"] = $tweetes_content;
        $data["footer_privacy"] = "include/footer_privacy";
        $data["footer_subscribe"] = "include/footer_subscribe";

        //fecth products
        $products = $this->products->display_all_product($config["per_page"],$page);
        $data["links"] = $this->pagination->create_links();       
          
        $data["products"] = $products;
        $video = $this->videos->as_object()->order_by('rand()')->limit(1,0)->get_all();
        $data["video"] = $video;

        $data["total_rows"] = $total_rows;
        $data["per_page"] = $config["per_page"] ;
        $data["page"] = $page;

        $data = array_merge($data,$this->data);

  

        $this->load->view("buy/buy",$data);

    }
}