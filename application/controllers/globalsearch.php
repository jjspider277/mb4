<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/31/2014
 * Time: 10:11 AM
 * Craig Robinson 
 * www.cytekservices.com 
 */


class GlobalSearch extends  MY_Controller {
 
 public function search(){
      $message = array ();
      $latestQuery = $this->input->post('latestQuery');
      $latestQueryLength =strlen($latestQuery);
      $search_result = array();
      $words= array();
	  if(isset($_POST['search-header'])){
		  $search = $_POST['search-header'];
	  }
     
	 $query_products = "SELECT *FROM products WHERE products.name LIKE  '%".$search."%' OR products.name LIKE '".$search."%'";
	 $products_result = $this->db->query($query_products);
	 $products = $products_result->result_array();
     // $query = $this->db->get("profiles");
     // $profiles = $query->result_array();
      
	 // $this->db->select('*');
     // $query = $this->db->get("products");
     // $products = $query->result_array();

    //  $this->db->select('*');
   //   $query = $this->db->get("stores");
   //   $stores = $query->result_array();

	  $query_profiles = "SELECT * FROM profiles INNER JOIN users ON users.`id` = profiles.`user_id WHERE users.username` LIKE  '%".$search."%' OR users.unsername LIKE '".$search."%' AND profiles.`is_profile_verified`" ;
	 $profiles_result = $this->db->query( $query_profiles);
	 $profiles = $profiles_result->result_array();
	 
	 $query_stores =  "SELECT * FROM stores WHERE stores.store_name LIKE  '%".$search."%' OR stores.store_name` LIKE '".$search."%'";
	 $stores_result = $this->db->query(  $query_stores);
	 $stores = $stores_result->result_array();

	 foreach($profiles as $profile){

            $this->db->select('*');
            $this->db->where("id", $profile['profile_image_id'] );
            $query = $this->db->get("medias");
            $image = $query->result_array();

            if(count($image)>0){
                $profile_image = "uploads/profile/" . $profile['id']. "/avatar/" . $image[0]['file_name'];
            }else{
                $profile_image = "uploads/no-photo.jpg";
            }

           if (substr(strtolower($profile['fname']),0,$latestQueryLength) == strtolower($latestQuery)){
                        $words["<a "."href='".base_url('sell/seller/').'/'.$profile['id'].'#listing'."'>".$profile['fname']."</a>"]=$profile['fname'];
                        $search_result["<a "."href='".base_url('sell/seller/').'/'.$profile['id'].'#listing'."'>".$profile['fname']."</a>"]= "<img src='".base_url($profile_image)."' height='40px' width='40px' >";
             }
      }
      foreach($products as $product){

            $this->db->select('*');
            $this->db->where("product_id", $product['id'] );
            $query = $this->db->get("medias");
            $image = $query->result_array();

            if(count($image)>0){
                $product_image = "uploads/profile/" . $image[0]['profile_id']. "/products/" . $image[0]['file_name'];
            }else{
                $product_image = "uploads/no-photo.jpg";
            }

           if (substr(strtolower($product['name']),0,$latestQueryLength)== strtolower($latestQuery)){
                        $words["<a "."href='".base_url('product/detail/').'/'.$product['id'].'/buy'."'>".$product['name']."</a>"]=$product['name'];
                        $search_result["<a "."href='".base_url('product/detail/').'/'.$product['id'].'/buy'."'>".$product['name']."</a>"]= "<img src='".base_url($product_image)."' height='40px' width='40px'>";
             }
      }

      foreach($stores as $store){

            $this->db->select('*');
            $this->db->where("id", $store['media_id'] );
            $query = $this->db->get("medias");
            $image = $query->result_array();

            if(count($image)>0){
                $store_image = "uploads/profile/" . $image[0]['profile_id']. "/store/" . $image[0]['file_name'];
            }else{
                $store_image = "uploads/no-photo.jpg";
            }

           if (substr(strtolower($store['store_name']),0,$latestQueryLength)== strtolower($latestQuery)){
                        $words["<a "."href='".base_url('store/store_listing').'/'.$store['id']."'>".$store['store_name']."</a>"]=$store['store_name'];
                        $search_result["<a "."href='".base_url('store/store_listing').'/'.$store['id']."'>".$store['store_name']."</a>"]= "<img src='".base_url($store_image)."' height='40px' width='40px'>";
             }
      }


       $message = array (
                      "result" => $search_result,
                      "word" => $words,
                      "status" => true,
                      "success"=>true,
                  );


      echo json_encode($message);
      return;

   }

   public function search_products() {

     $query = $this->input->post('search-header');

     if(empty($query)){
       $query = $this->input->post('display-header');
     }
    $query = trim($query);
    //dump($query);exit;
   // apply stripslashes if magic_quotes_gpc is enabled
    if(get_magic_quotes_gpc()) 
    {
    $query = stripslashes($query); 
    }

    $query = htmlspecialchars($query, ENT_QUOTES);
    
    $this->load->library('pagination');   
    $this->load->model('product_model','products');
    $config = array(); 
    $config["per_page"] = 8; //number of pages 
    $data["per_page"]   = $config["per_page"];
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      
    $products = $this->products->search_products($config["per_page"],$page,$query);
    $total_rows = count($products); //url_connection
    $config["base_url"] = base_url() . "product/new_arrivals"; 
    $config["total_rows"] = $total_rows; //url_pages_why_
    $config["per_page"] = 8; //number of pages 
    $config["uri_segment"] = 3; // url_segment pages
    $config['display_pages'] = TRUE;
    $this->pagination->initialize($config);
    
    $data["products"] = $products;  
    $data["total_rows"] = $total_rows;
    $data["per_page"]   = $config["per_page"];
    $data["links"] = $this->pagination->create_links();
  
    $paginate_page = 'include/paginate_page';
    $notification_bar ='include/notification_bar';
    $header_logo_white ='include/header_logo_white';
    $main_menu = 'include/main_menu';
    $product_listing='product/product_listing_new';

    $data['product_listing']=$product_listing;
    $data['footer_privacy'] = 'include/footer_privacy';
    $data['footer_subscribe'] = 'include/footer_subscribe';
    $data['header_black_menu'] = 'include/header_black_menu';
    $data['paginate_page'] = $paginate_page; 
    $data['notification_bar'] = $notification_bar; 
    $data['header_logo_white'] = $header_logo_white;
    $data['main_menu'] = $main_menu;
    $data['footer_page'] = 'include/footer_page';

    $this->load->view('search/search_product',$data);


   }


}