<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by dkf4199.
 * Date: 06/10/2015
 * last Updated by #dantheman , Oct 22/2015
 */

class Shopcart extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        /* My_Controller already loads 'cart_model' */
		$this->load->library('cart');
		$this->load->library('form_validation');
		$this->load->model('state_model','state');
		$this->load->model('shipping_model','shipping');
		$this->load->model('category_model','menu_categories');


		$this->load->model('auction_winner_model');
        $this->load->model('bid_model');
        $this->load->model('auction_model');
    }

    public function index(){
		
		//include files
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$all_categories = $this->menu_categories->get_all();
		$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
		$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
		$data['all_categories'] = $all_categories;
		$data['sub_parent_categories'] = $sub_parent_categories;
		$data['parent_categories'] = $parent_categories;
		$data['column_main_menu'] = 'include/column_menu_top';


		// Data passed to view
		$data['notification_bar'] = $notification_bar;
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
	
		$this->load->view('shoppingcart/shopcartcontents',$data);
    }
	
	public function add_cart_item(){
		
		$seller = $this->input->post('seller_name');
		$pid = $this->input->post('product_id');
		
		// Set array for send data.
		$insert_data = array(
			'id' => $this->input->post('product_id'),
			'name' => $this->input->post('product_name'),
			'price' => $this->input->post('product_price'),
			'qty' => $this->input->post('product_quantity'),
			'seller' => $this->input->post('seller_name'),
			'seller_profile' => $this->input->post('seller_profile_id'),
			'image' => $this->input->post('product_image'),
			'shipping_price' => $this->input->post('shipping_price'),

		);		

		//LOAD INTO SESSION (cart) variable
		//$this->session->set_userdata(['cart'][$pid] , $insert_data);
		
        // This function add items into cart.
		$this->cart->insert($insert_data);
		
		redirect('shopcart');
		//$this->load->view('shoppingcart/dumpcontents');	//DEBUG DUMP
	}
	
	public function update_cart(){
		// Get the total number of items in cart
		$total = $this->cart->total_items();
		 
		// Retrieve the posted information
		$item = $this->input->post('rowid');
		$qty = $this->input->post('qty');
	 
		// Cycle true all items and update them
		for($i=0;$i < $total;$i++)
		{
			// Create an array with the products rowid's and quantities. 
			$data = array(
			   'rowid' => $item[$i],
			   'qty'   => $qty[$i]
			);
			 
			// Update the cart with the new information
			$this->cart->update($data);
		}
		
		//reload page
		redirect('shopcart');
		
	}
	
	public function empty_cart(){
		$this->cart->destroy(); // Destroy all cart data
		redirect('shopcart'); // Refresh the page
	}
	
	public function checkout_address(){

		//Begin checkout process
		//Display Form for user to provide shipping address
		//include files
		$shipping_address = "";
		$shipping_address = $this->shipping->get_shipping_address($this->session->userdata('profile_id'));
		
		$buyer_email = $this->shipping->get_buyer_email($this->session->userdata('profile_id'));
		
		//$states = $this->state->populate_state_dropdown();
		
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		$all_categories = $this->menu_categories->get_all();
		$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
		$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
		$data['all_categories'] = $all_categories;
		$data['sub_parent_categories'] = $sub_parent_categories;
		$data['parent_categories'] = $parent_categories;
		$data['column_main_menu'] = 'include/column_menu_top';


		// Data passed to view
		//$data['states'] = $states;
		$data['shipping_address'] = $shipping_address;
		$data['buyer_email'] = $buyer_email;
		
		$data['notification_bar'] = $notification_bar;
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
	
		$this->load->view('shoppingcart/checkout_address',$data);
		
	}
	
	public function validate_checkout_address(){
		//Verify the checkout address
				
		//set_rules parms:  1.) Name of field, 2.) Name used in error msg, 3.) Rule
		//
		$this->form_validation->set_rules('ship_first', 'First Name', 'required');
		$this->form_validation->set_rules('ship_last', 'Last Name', 'required');
		$this->form_validation->set_rules('ship_street', 'Shipping Street', 'required');
		$this->form_validation->set_rules('ship_city', 'Shipping City', 'required');
		$this->form_validation->set_rules('ship_state', 'Shipping State', 'required');
		//$this->form_validation->set_rules('userapt', 'Apt Number', 'required');
		$this->form_validation->set_rules('ship_zip', 'Shipping Zip', 'required');
		$this->form_validation->set_rules('ship_email', 'Shipping Email', 'required');
		
		$shipping_address = $this->shipping->get_shipping_address($this->session->userdata('profile_id'));
		$buyer_email = $this->shipping->get_buyer_email($this->session->userdata('profile_id'));
		
		if ($this->form_validation->run() === FALSE) {
			//$states = $this->state->populate_state_dropdown();
			//$data['states'] = $states;
			$data['shipping_address'] = $shipping_address;
			$data['buyer_email'] = $buyer_email;
			$data['post'] = $_POST;
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$all_categories = $this->menu_categories->get_all();
			$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
			$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
			$data['all_categories'] = $all_categories;
			$data['sub_parent_categories'] = $sub_parent_categories;
			$data['parent_categories'] = $parent_categories;
			$data['column_main_menu'] = 'include/column_menu_top';
			// Data passed to view
			$data['notification_bar'] = $notification_bar;
			$data['header_black_menu'] = 'include/header_black_menu';
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
		
			$this->load->view('shoppingcart/checkout_address',$data);

		} else {
			
			// Set shipping address fields in SESSION
			$this->session->set_userdata('shipping_firstname', $this->input->post('ship_first'));
			$this->session->set_userdata('shipping_lastname', $this->input->post('ship_last'));
			$this->session->set_userdata('shipping_street', $this->input->post('ship_street'));
			$this->session->set_userdata('shipping_street2', $this->input->post('ship_apt'));
			$this->session->set_userdata('shipping_city', $this->input->post('ship_city'));
			$this->session->set_userdata('shipping_state', $this->input->post('ship_state'));
			$this->session->set_userdata('shipping_zip', $this->input->post('ship_zip'));
			$this->session->set_userdata('shipping_email', $this->input->post('ship_email'));
			$this->session->set_userdata('shippinginfo_set', TRUE);
			
			// PROCEED TO VERIFY SHIPPING ADDRESS
			//
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$all_categories = $this->menu_categories->get_all();
			$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
			$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
			$data['all_categories'] = $all_categories;
			$data['sub_parent_categories'] = $sub_parent_categories;
			$data['parent_categories'] = $parent_categories;
			$data['column_main_menu'] = 'include/column_menu_top';
			// Data passed to view
			$data['notification_bar'] = $notification_bar;
			$data['header_black_menu'] = 'include/header_black_menu';
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
		
			$this->load->view('shoppingcart/verify_shipping_address',$data);
		}
		
	}
	
	public function address_verified(){
		
		$notification_bar = 'include/notification_bar';
		$header_logo_white = 'include/header_logo_white';
		$main_menu = 'include/main_menu';
		
		// Data passed to view
		$data['notification_bar'] = $notification_bar;
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$all_categories = $this->menu_categories->get_all();
		$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
		$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
		$data['all_categories'] = $all_categories;
		$data['sub_parent_categories'] = $sub_parent_categories;
		$data['parent_categories'] = $parent_categories;
		$data['column_main_menu'] = 'include/column_menu_top';
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['image_path'] = base_url().'assets/images/cart_and_checkout/';
	
		$this->load->view('shoppingcart/card_billing_info',$data);
			
	}
	
	public function validate_billing_info(){
		
		//set_rules parms:  1.) Name of field, 2.) Name used in error msg, 3.) Rule
		//
		$this->form_validation->set_rules('cardnum', 'Card Number', 'required');
		$this->form_validation->set_rules('expmonth', 'Expiration Month', 'required');
		$this->form_validation->set_rules('expyear', 'Expiration Year', 'required');
		$this->form_validation->set_rules('cardcvv', 'Card CVV', 'required');
		$this->form_validation->set_rules('cardname_first', 'Card First Name', 'required');
		$this->form_validation->set_rules('cardname_last', 'Card Last Name', 'required');
		$this->form_validation->set_rules('cardemail', 'Email', 'required');
		
		if ($this->form_validation->run() === FALSE) {
			
			$data['post'] = $_POST;
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$all_categories = $this->menu_categories->get_all();
			$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
			$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
			$data['all_categories'] = $all_categories;
			$data['sub_parent_categories'] = $sub_parent_categories;
			$data['parent_categories'] = $parent_categories;
			$data['column_main_menu'] = 'include/column_menu_top';
			// Data passed to view
			$data['image_path'] = base_url().'assets/images/cart_and_checkout/';
			$data['notification_bar'] = $notification_bar;
			$data['header_black_menu'] = 'include/header_black_menu';
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';
		
			$this->load->view('shoppingcart/card_billing_info',$data);

		} else {
			
			// Set shipping address fields in SESSION
			$this->session->set_userdata('cc_cardnum', $this->input->post('cardnum'));
			$this->session->set_userdata('cc_expmonth', $this->input->post('expmonth'));
			$this->session->set_userdata('cc_expyear', $this->input->post('expyear'));
			$this->session->set_userdata('cc_cardcvv', $this->input->post('cardcvv'));
			$this->session->set_userdata('cc_cardname_first', $this->input->post('cardname_first'));
			$this->session->set_userdata('cc_cardname_last', $this->input->post('cardname_last'));
			$this->session->set_userdata('cc_cardemail', $this->input->post('cardemail'));
			$this->session->set_userdata('cardinfo_set', TRUE);
			
			// PROCEED TO REVIEW FINAL ORDER
			//
			$notification_bar = 'include/notification_bar';
			$header_logo_white = 'include/header_logo_white';
			$main_menu = 'include/main_menu';
			$all_categories = $this->menu_categories->get_all();
			$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
			$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
			$data['all_categories'] = $all_categories;
			$data['sub_parent_categories'] = $sub_parent_categories;
			$data['parent_categories'] = $parent_categories;
			$data['column_main_menu'] = 'include/column_menu_top';
			// Data passed to view
			$data['image_path'] = base_url().'assets/images/cart_and_checkout/';
			$data['notification_bar'] = $notification_bar;
			$data['header_black_menu'] = 'include/header_black_menu';
			$data['header_logo_white'] = $header_logo_white;
			$data['main_menu'] = $main_menu;
			$data['footer_privacy'] = 'include/footer_privacy';
			$data['footer_subscribe'] = 'include/footer_subscribe';

			//compute shipping for all products and send the final

		
			$this->load->view('shoppingcart/review_final_order',$data);
		}
		
		
	}
	
	public function process_order(){
			$this->load->helper('general');
			$this->load->model('orders_model');
			$this->load->model('orderdetail_model');
			$messages = array();
			
			// FINAL STEP - PROCESS CC -> ADD ORDER DATA TO ORDERS/ORDER_DETAIL TABLES
			// Authorize.net lib
			$this->load->library('authorize_net');

			$exp_date = $this->session->userdata('cc_expmonth').'/'.$this->session->userdata('cc_expyear');
			
			//Email vars
			$ship_to_email = $this->session->userdata('shipping_email');
			 
			/* TEST */
			$auth_net = array(
				'x_card_num'			=> '4111111111111111', // Visa
				'x_exp_date'			=> $exp_date,
				'x_card_code'			=> $this->session->userdata('cc_cardcvv'),
				'x_description'			=> 'Your MadeByUs4U order',
				'x_amount'				=> $this->session->userdata('final_order_total'),
				'x_first_name'			=> $this->session->userdata('cc_cardname_first'),
				'x_last_name'			=> $this->session->userdata('cc_cardname_last'),
				'x_email'				=> $this->session->userdata('cc_cardemail')
			);
			
			
			/*
			$auth_net = array(
				'x_card_num'			=> $this->session->userdata('cc_cardnum'), // Visa
				'x_exp_date'			=> $exp_date,
				'x_card_code'			=> $this->session->userdata('cc_cardnum'),
				'x_description'			=> 'A test transaction',
				'x_amount'				=> $this->cart->total(),
				'x_first_name'			=> $this->session->userdata('cc_cardname_first'),
				'x_last_name'			=> $this->session->userdata('cc_cardname_last'),
				//'x_address_1'			=> '123 Green St.',
				//'x_address_2'			=> '123 Green St.',
				//'x_city'				=> 'Lexington',
				//'x_state'				=> 'KY',
				//'x_zip'					=> '40502'
				//'x_country'				=> 'America',
				//'x_phone'				=> '555-123-4567',
				//'x_email'				=> 'dadenewyyt@gmail.com',
				//'x_customer_ip'			=> $this->input->ip_address(),
				);
			*/
			
			$this->authorize_net->setData($auth_net);

			// Try to AUTH_CAPTURE
			if( $this->authorize_net->authorizeAndCapture() )
			{
				$this->session->set_userdata('anet_transid', $this->authorize_net->getTransactionId());
				
				//dump($this->cart->contents());

				foreach($this->cart->contents() as $items){
					 $seller_id = intval($items['seller_profile']);
					 //dump($seller_id);
					//INSERT ORDER AND ORDER DETAIL DATA INTO orders and orders_detail tables.
					$oid = $this->orders_model->insert_order_data();
					//dump($oid );				
					// ORDER DETAIL
					$this->orderdetail_model->insert_order_detail($oid,$items);
				}
				
				
				//UNSET ALL CHECKOUT DATA
				$this->session->unset_userdata('shipping_firstname');
				$this->session->unset_userdata('shipping_lastname');
				$this->session->unset_userdata('shipping_street');
				$this->session->unset_userdata('shipping_street2');
				$this->session->unset_userdata('shipping_city');
				$this->session->unset_userdata('shipping_state');
				$this->session->unset_userdata('shipping_zip');
				$this->session->unset_userdata('shipping_email');
				$this->session->unset_userdata('shippinginfo_set');
				$this->session->unset_userdata('cc_cardnum');
				$this->session->unset_userdata('cc_expmonth');
				$this->session->unset_userdata('cc_expyear');
				$this->session->unset_userdata('cc_cardcvv');
				$this->session->unset_userdata('cc_cardname_first');
				$this->session->unset_userdata('cc_cardname_last');
				$this->session->unset_userdata('cc_cardemail');
				$this->session->unset_userdata('cardinfo_set');
				$this->session->unset_userdata('final_order_total');

				//first save a notification for buyer 
				//on the community platform
				 $buyer_notifiaction_id = $this->notifications->push_notifications('PURCASE_PRODUCT',NULL,NULL,$this->profile_id,'You have purcahased a product!');	
				
				//Loop through cart and send email to all sellers
				$seller_sub = "MadeByUs4U Product Sales Notice.";
				$seller_msg = "You sold a product on MadeByUs4U.com!";
				foreach($this->cart->contents() as $items){
					$seller_email = $this->shipping->get_seller_email($items['seller_profile']);
					
					/**
					Auction winners will have products added to their shopping cart. 
					These products products will be added to their shopping cart on login. 
					They will only be removed from the shopping cart when the items are purchased.
					**/
					if(isset($items['auction_won_id'])){
						$auctions_won = $this->auction_winner_model->
		                get_won_auctions($this->session->userdata['profile_id']);
		                if($auctions_won){
		                	foreach ($auctions_won as $auction_won) {
		                		if($items['auction_won_id'] === $auction_won->id){
		                			//Update auction_winners table
		                			$this->auction_winner_model->claim_win($auction_won->id);
		                			//Close auction
		                			$bid = $this->bid_model->get_bid_by_id($auction_won->bid_id);
			            			$this->auction_model->update_status($bid->auction_id, false);
		                		}
		                	}
		                }			            
					}
					/**
					End
					**/
						
					if ($seller_email != 'notfound'){
						send_email($seller_email, $seller_sub, $seller_msg);

						 //ADD NOTIFICATION FOR COMMUNITY PURPOSES					  				      
					      					 					    
					    

					     //save notification per order or seller id	

					     $seller_notifiaction_id = $this->notifications->push_notifications('SALE_PRODUCT',NULL,NULL,$items['seller_profile'],'You have new sales order!');				     		     
					     
					     if($seller_notifiaction_id) {					            
					            echo json_encode( array('success'=>true) );
					      }
					     else {
					            echo json_encode( json_last_error() );
					      }

					}
				}

				
				$this->cart->destroy(); // Destroy all cart data
				//************ END ***********************************************/
				
				$messages[] = '<h2>You order has been successfully processed.</h2>';
				$messages[] = '<p>Your Order ID is: ' . $this->authorize_net->getTransactionId() . '</p>';
				$messages[] = '<p>You will be notified when your items ship.  Thanks again.</p>';
				//$messages[] = '<p>Approval Code: ' . $this->authorize_net->getApprovalCode() . '</p>';
				
				// PULL UP REVIEW FINAL ORDER AND DISPLAY AUTHORIZE.NET FEEDBACK
				//
				$notification_bar = 'include/notification_bar';
				$header_logo_white = 'include/header_logo_white';
				$main_menu = 'include/main_menu';
				$all_categories = $this->menu_categories->get_all();
				$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
				$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
				$data['all_categories'] = $all_categories;
				$data['sub_parent_categories'] = $sub_parent_categories;
				$data['parent_categories'] = $parent_categories;
				$data['column_main_menu'] = 'include/column_menu_top';

				// Data passed to view
				$data['messages'] = $messages;
				$data['image_path'] = base_url().'assets/images/cart_and_checkout/';
				$data['notification_bar'] = $notification_bar;
				$data['header_black_menu'] = 'include/header_black_menu';
				$data['header_logo_white'] = $header_logo_white;
				$data['main_menu'] = $main_menu;
				$data['footer_privacy'] = 'include/footer_privacy';
				$data['footer_subscribe'] = 'include/footer_subscribe';
			
				//SEND EMAIL TO PERSON AT SHIPPING ADDRESS
				// 1.) person at shipping address
				$sub = "Your MadeByUs4U Order.";
				$msg = "Thank you for MadeByUs4U order.  You can find your order details in your Account Dashboard under My Orders.";
				send_email($ship_to_email, $sub, $msg);
				
				//LOAD THANKYOU PAGE
				$this->load->view('shoppingcart/order_thankyou',$data);
			}
			else
			{
				$messages[] = '<h2>Your order could not be processed.</h2>';
				// Get error
				$messages[] = '<p>' . $this->authorize_net->getError() . '</p>';
				// Show debug data
				//$messages[] = $this->authorize_net->debug();
				
				// PULL UP REVIEW FINAL ORDER AND DISPLAY AUTHORIZE.NET FEEDBACK
				//
				$notification_bar = 'include/notification_bar';
				$header_logo_white = 'include/header_logo_white';
				$main_menu = 'include/main_menu';
				$all_categories = $this->menu_categories->get_all();
				$parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
				$sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
				$data['all_categories'] = $all_categories;
				$data['sub_parent_categories'] = $sub_parent_categories;
				$data['parent_categories'] = $parent_categories;
				$data['column_main_menu'] = 'include/column_menu_top';

				// Data passed to view
				$data['messages'] = $messages;
				$data['image_path'] = base_url().'assets/images/cart_and_checkout/';
				$data['notification_bar'] = $notification_bar;
				$data['header_black_menu'] = 'include/header_black_menu';
				$data['header_logo_white'] = $header_logo_white;
				$data['main_menu'] = $main_menu;
				$data['footer_privacy'] = 'include/footer_privacy';
				$data['footer_subscribe'] = 'include/footer_subscribe';
			
				$this->load->view('shoppingcart/review_final_order',$data);
			}
			
			
			
	}
	
}
/* End of file Shopcart.php */
/* Location: ./application/controllers/Shopcart.php */