<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 *
 * Date: 07/25/2015
 * 
 */


class Video extends  MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('video_model','videos');
    }

   /**
   * load items for sell by all active seller/memebers
   *
   */

    public function upload_video(){   
       

        $file_post_name = 'userfile';
        $result= $this->videos->save_my_videos($this->profile_id,$this->input->post('title'),$file_post_name);
        if($result) {
         // var_dump($result);
          redirect('sell/seller/'.$this->profile_id."#videos");
        }
        //var_dump($result);
	   }


    public function delete_message(){   
       
      $message_id = $this->input->post('id');
      //var_dump($message_id);exit;
     //allow only ajax request
      if(!$this->input->is_ajax_request()) {
            exit('Not allowed!');
       }

      $return_id=$this->messages->update($message_id,array('message_status' =>'TRASH'));
     // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
      
      if($return_id) {
          echo json_encode('Trashed succesfully!');
    }
     else {
      echo json_encode("201");
     }

     }

  public function permanent_delete_message(){   
         
        $message_id = $this->input->post('id');
        //var_dump($message_id);exit;
       //allow only ajax request
        if(!$this->input->is_ajax_request()) {
              exit('Not allowed!');
         }

        $return_id=$this->messages->update($message_id,array('message_status' =>'DELETED'));
       // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
        
        if($return_id) {
            echo json_encode('Deleted succesfully!');
      }
       else {
        echo json_encode(json_last_error(201));
       }

       }

        public function get_profile_image(){   
         
        $profile_id = $this->input->post('profile_id');
        //var_dump($profile_id);exit;
       //allow only ajax request
        if(!$this->input->is_ajax_request()) {
              exit('Not allowed!');
         }

        $image_path = $this->get_image_file_path($profile_id);
       // var_dump($this->messages->update($message_id,array('message_status' =>'TRASH')) );
        
        if(!empty($image_path)) {
            echo json_encode(base_url($image_path),JSON_UNESCAPED_SLASHES);
      }
       else {
        echo json_encode(json_last_error());
       }

       }



    

   
}
/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
