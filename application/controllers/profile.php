 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson 
 * Date: 12/31/2014
 * Time: 10:11 AM
 * www.cytekservices.com 
 */


class Profile extends  MY_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('profile_model', 'profile');
        $this->load->model('ion_auth_model', 'users');
        $this->load->model('media_model', 'media');
        $this->load->model('city_model', 'city');
        $this->load->model('state_model', 'state');
        $this->load->model('shipping_model', 'shippings');
        $this->load->model('category_model','menu_categories');


    }

    public function index(){


    }

    public function edit($id) {

      //$this->output->enable_profiler(TRUE);

      $profile = $this->profile->with('media')->get($id);
      $profile_image = '';

        if(count($profile->media) > 0)  {
          $profile_image = "/uploads/profile/" . $profile->id . "/avatar/" . $profile->media->file_name;
         } else {
           $profile_image = "/uploads/no-photo.jpg";
         }


      $data['header_black_menu'] = 'include/header_black_menu';
      $data['header_logo_white'] = 'include/header_logo_white';
      $data['show_error_page'] = 'include/show_error_page';
      $data['footer_findout'] = 'include/footer_findout';
      $data['footer_privacy'] = 'include/footer_privacy';
      $data['footer_subscribe'] = 'include/footer_subscribe';
      $data['signin_form'] = 'include/signin_form';
      $data['new_to_madeby'] = 'include/new_to_madeby';
      $all_categories = $this->menu_categories->get_all();
      $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
      $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
      $data['all_categories'] = $all_categories;
      $data['sub_parent_categories'] = $sub_parent_categories;
      $data['parent_categories'] = $parent_categories;
      $data['column_main_menu'] = 'include/column_menu_top';


        /**
       * Taken our old main menu
       *
      $data['navigation_top'] = 'include/navigation_top';
      **/
      $data['notification_bar'] = 'include/notification_bar';
      $data['main_menu'] = 'include/main_menu';
      $states = $this->state->populate_state_dropdown();
      $cities = $this->city->populate_city_dropdown();
      $data['cities'] = $cities;
      $data['states'] = $states;
      $data['profile'] = $profile;
      $data['profile_image']= $profile_image;
      $data['data']['message'] = $this->message;
	  $shippingresults =  $this->shippings->get_shipping_address($profile->id);
	  if(isset($shippingresults[0])){
		$data['shipping'] = $shippingresults[0];
	  }
	  $data['shipping']['shipto_state_full'] = $this->state->convert_state_abbrev($data['shipping']['shipto_state']);
	 

      $data = array_merge($data,$this->data);

      $this->load->view('profile/edit_new',$data);

    }

    public function save_profile(){
	//if (!$this->ion_auth->logged_in()){	redirect('users/login', 'refresh');}
      //$this->output->enable_profiler(TRUE);
      //validation
      $profile_image = "/uploads/no-photo.jpg";

      $profile_image_id = null;

      $tables = $this->config->item('tables','ion_auth');
      //validate form input
      $this->form_validation->set_rules('bioinfo', 'Bio information', 'required|xss_clean');
     // $this->form_validation->set_rules('website', 'Website address', 'required|xss_clean');
      $this->form_validation->set_rules('imgfile','Please upload your profile image', 'xss_clean');
      $this->form_validation->set_rules('address_line1', 'Address line 1 ', 'trim|required|xss_clean');
     // $this->form_validation->set_rules('address_line2', 'Address line 2 ', 'trim|xss_clean');

    if(!empty($this->input->post('new_email'))){
			$this->form_validation->set_rules('new_email', $this->lang->line('create_user_validation_email_label'), 'valid_email|is_unique['.$tables['users'].'.email]|matches[confirm_email]');
			$this->form_validation->set_rules('confirm_email', 'confirm email', 'required');
		}
	if(!empty($this->input->post('new_password'))){
		$this->form_validation->set_rules('password', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new_password', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');
	}	
      if($this->form_validation->run() == true) {

            //path should be profile/profileid/avatar , profile/profileid/video , profile/profileid/products
            $pathToUpload = "./uploads/profile/". $this->profile_id ."/avatar/";

            //load the configuration
            $upload_config = $this->config->item('upload_config_profile_edit');

            //replace the upload path based to be based on profile id
            $upload_config['upload_path'] = $pathToUpload;
            $file_post_name = 'imgfile';
            $profile_image_id = $this->media->save_or_update($this->profile_id ,$upload_config,$file_post_name);


           //receive the post available after validate and update profile
            $password = $this->input->post('password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
            $confirm_email = $this->input->post('confirm_email');
            $new_email = $this->input->post('new_email');
            $bioinfo = $this->input->post('bioinfo');
            $zipcode = $this->input->post('zipcode');
            $email = $this->input->post('email');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $address_line1 = $this->input->post('address_line1');
            $address_line2 = $this->input->post('address_line2');


           //update user account details
           //$updated_password = $this->users->hash_password($password,''); //can pass salt value here

          /**
           * check if email is empty or not then update
           */
		   $authorized = true;
			if(!empty($new_password)||!empty($new_email)){
				if($this->ion_auth->login($email, $password)){					
					if(!empty($new_email)){
							$change_email = $this->users->update($this->user_id, array('email' => $new_email));
							if($change_email){
								$data['email'] = $new_email;
							}
					}
					if(!empty($new_password)) {
						//$this->users->update($this->user_id, array('password' => $password));
						$this->ion_auth->change_password($email, $password, $new_password);
					}	
				}else{
					$authorized = false;
				}
			}				
          if ( $profile_image_id != -1 ) {
              //update profile information details
              $this->profile->update($this->profile_id,
                  array(
                      'bioinfo' => $bioinfo,
                       'zipcode'=>$zipcode,
                       'profile_image_id' =>$profile_image_id,
                  )
              );
          }
          //i guess this is for profile image is not changed ,:) me
            $result = $this->profile->update(
                            $this->profile_id ,
                            array ('bioinfo' =>$bioinfo,'zipcode'=>$zipcode,)
                        );

            //TODO:success page must be displayed
            //TODO:link must be provide to visit home, account
            //TODO:controller and self refreshing page
            //save shipping information anyway
          $insert_data = array(
              "address_line_1" =>$address_line1 ,
              "address_line_2" =>$address_line2,
              "city"=>$city,
              "state"=>$state,
              "zip_code"=>$zipcode,
          );

          //save shipping information
          $shipping_id =  $this->shippings->get_shipping_address($this->profile_id);
		  if(!$shipping_id){
			    $shipping_id =  $this->shippings->save_shipping_for_seller_profile($insert_data,$this->profile_id);
		  }elseif( $shipping_id < 0 ) { 
		  //update profile
              $this->profile->update($this->profile_id,array('shipping_id'=>$shipping_id));
          }



             redirect('sell/seller/'.$this->profile_id,'refresh');

            } else {

            //TODO:add message and validation error here
            // throw new Exception('error'.$e);
             $this->message['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
             $this->message = array('type' => 'error', 'message' =>  $this->message['message'] );

           }

        //now resize image
        if($profile_image_id!=null) {

            //echo json_encode("====resizing image =======");
            $profile_image = $this->media->get($profile_image_id)->file_name;
            $src_image = "./uploads/profile/".$this->profile_id."/avatar/".$profile_image;

            /**
             * Image RESIZE FEATURE TEST
             * $this->load->library('imageutility_service.php');
             * $this->imageutility_service->resize_profile_images($src_image,null);
             *
             */
            $this->load->library('imageutility_service.php');
            $this->imageutility_service->resize_profile_images($src_image,null);

        }

            //reload the newly uploaded or image profile
            $profile = $this->profile->with('media')->get($this->profile_id);

        //dump($profile);

            if( isset($profile->media) && count($profile->media) > 0 ) {

                $profile_image = "/uploads/profile/" . $profile->id . "/avatar/" . $profile->media->file_name;
            } else {

                $profile_image = "/uploads/no-photo.jpg";
            }

            $states = $this->state->populate_state_dropdown();
            $cities = $this->city->populate_city_dropdown();
            $data['cities'] = $cities;
            $data['states'] = $states;
            $data['header_black_menu'] = 'include/header_black_menu';
            $data['header_logo_white'] = 'include/header_logo_white';
            $data['main_menu'] = 'include/main_menu';
            $data['show_error_page'] = 'include/show_error_page';
            $data['footer_privacy'] = 'include/footer_privacy';
            $data['footer_subscribe'] = 'include/footer_subscribe';
            $data['signin_form'] = 'include/signin_form';
            $data['new_to_madeby'] = 'include/new_to_madeby';
            $data['navigation_top'] = 'include/navigation_top';
            $data['notification_bar'] = 'include/notification_bar';
            $data['footer_findout'] = 'include/footer_findout';

            $all_categories = $this->menu_categories->get_all();
            $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
            $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
            $data['all_categories'] = $all_categories;
            $data['sub_parent_categories'] = $sub_parent_categories;
            $data['parent_categories'] = $parent_categories;
            $data['column_main_menu'] = 'include/column_menu_top';

            $data['data']['message'] = $this->message;
            $this->session->set_flashdata('message',$this->message['message']);
            $data['profile'] = $profile;
            $data['profile_image']= $profile_image;
            $this->load->view('profile/edit_new',$data);


      }



    
}