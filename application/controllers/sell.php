<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/31/2014
 * Time: 10:11 AM
 * Craig Robinson 
 * www.cytekservices.com 
 */


class Sell extends  MY_Controller {


    var $loaded_profile_same_as_user;
    var $profile ;
    public function __construct() {

        parent::__construct();
        $this->load->helper('general'); //load email library
        $this->load->model('profile_model','profile');
        $this->load->model('media_model','media');
        $this->load->model('store_model','store');
        $this->load->model('product_model','seller_product');
        $this->load->model('message_model','messages');
        $this->load->model('photo_model','photos');
        $this->load->model('video_model','videos');
        $this->load->library('pagination');
        $this->load->helper('text');
        $this->load->model('category_model','menu_categories');



        }



   /**
   * load items for sell by
   * all active
   * seller / members
   *
   */

    public function index() {   

     
        $loggedin_profile_lists = MY_Controller::$online_profile_user_ids;
        $data['loggedin_profile_lists'] = $loggedin_profile_lists;  
       
        //get the totoal size of verified profiles ? paied only
        $total_rows = $this->profile->count_by('is_profile_verified',intval(true));
        $config = array();
        $config["base_url"] = base_url() ."sell/index";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 11;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $profiles = $this->profile->get_Verfied_Profiles($config["per_page"], $page);
        

       $data["profiles"] = $profiles;
       $data['is_store_created'] = !empty($this->is_store_created) ? $this->is_store_created : false;
       $data["links"] = $this->pagination->create_links();
       $paginate_page = 'include/paginate_page';
       $notification_bar = 'include/notification_bar';
       $header_logo_white = 'include/header_logo_white';
       $seller_lisiting_page = 'sell/seller_listing_page_new';
       $main_menu = 'include/main_menu';
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

        $data['footer_privacy'] = 'include/footer_privacy';
       $data['footer_subscribe'] = 'include/footer_subscribe';
       $data['header_black_menu'] = 'include/header_black_menu';
       $data['seller_lisiting_page'] = $seller_lisiting_page;
       $data['paginate_page'] = $paginate_page;
       $data['notification_bar'] = $notification_bar;
       $data['header_logo_white'] = $header_logo_white;
       $data['main_menu'] = $main_menu;

        $data = array_merge($data,$this->data);

        //TODO: load memeber /profile model
        //TODO:get_all who isVerfied
        //TODO:

       $this->load->view('sell/sell',$data);

    }
   
   

    public function become_seller(){   
       
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $main_menu = 'include/main_menu';
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $this->load->view('sell/become_seller_new',$data);

    }

   

   /**
   * Seller Page when clicked on Sell page detail of seller will be shown 
   * @var id . recived Id of selected seller 
   * secured methods
   */

    public function seller($id) {

        $this->messages->get_draft_messages($id);

         /*
         testing email images 
         $profile_information =(object) $this->load_profile_detail_with_image(4);
           $to = 'vegascraig11@gmail.com' ;
           $profile_image_path = $profile_information->profile_image;
           //var_dump($profile_image_path);exit;

         
          $invite_profile_full_name = ucfirst($profile_information->fname).' '.ucfirst($profile_information->lname);
          
          $from ='noreplay@madebyus4u.com';   

          $subject = ucwords($invite_profile_full_name)." invited you to MadeByus4u.com!";
   
         send_email_with_profile_detail ($to,'noreplay@gmail.com','Images are now Previewed on Emails','dan theman',$profile_image_path,'asd',3);
    */
    if( !empty($id) && !empty( $this->profile->get( intval($id) ) ) ) {

      //load current logged in users
      $loggedin_profile_lists = MY_Controller::$online_profile_user_ids;
      $data['loggedin_profile_lists'] = $loggedin_profile_lists;  
       
     //load profile related informations
      $profile_data = $this->_profile_information_for_seller($id);

      $this->db->select("user_id");
      $this->db->from('profiles');
      $this->db->where('id', $id);
      $query = $this->db->get();
      $data['id_user']= $query->result_array();


      $this->db->select("username");
      $this->db->from('users');
      $this->db->where('id', $data['id_user'][0]['user_id']);
      $query = $this->db->get();
      $data['profile_username']= $query->result_array();



      $profile = $this->profile->with('media')->get($this->profile_id);
      
      if(!empty($profile->media) > 0)  {
          $profile_img = "/uploads/profile/" . $this->profile_id . "/avatar/" . $profile->media->file_name;
         } else {
           $profile_img = "/uploads/no-photo.jpg";
         }
      
      $data['the_sender_image'] =  $profile_img;

     //print_r($id);
    // print_r($this->profile_id);
     // die;
     
      //do this on if logged_in==false
      $this->_messaging_information_for_seller($id);

      //load products
      $this->_product_information_for_seller($id); 

      //loaD FREIND REQUEST
      $this->_friend_request_information_for_seller($id,$this->loaded_profile_same_as_user);   

      //Load seller store info
      $this->_store_information_for_seller($id);

      //Load seller store info
      $this->_friend_information_for_seller($id,$this->loaded_profile_same_as_user);

      //load notification
      $this->_notification_information_for_seller($id);

     //load photos
      $this->_photo_information_for_seller($id);

      //load video
      $this->_video_information_for_seller($id);

      //load likes

      $this->_likes_information_for_seller($id);
      
        
      /**tabs***/
      $store_listing_tab = 'include/store_lisiting_tab';
      $store_page = 'storesetup/store_page';
      $addproduct_page = 'storesetup/addproduct';
      $getpaid_page = 'storesetup/getpaid';
      $openstore_page = 'storesetup/openstore';
      $launchstore_page = 'storesetup/launchstore';
      $data['store_listing_tab'] = $store_listing_tab;
      $data['store_page'] = $store_page;
      $data['addproduct_page'] = $addproduct_page;
      $data['getpaid_page'] = $getpaid_page;
      $data['openstore_page'] = $openstore_page;
      $data['launchstore_page'] = $launchstore_page;
      $data['show_error_page'] = 'include/show_error_page';
      
      //*end**tab*/
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';
      $notification_bar = 'include/notification_bar';
      $header_logo_white = 'include/header_logo_white';
      $main_menu = 'include/main_menu';
      $data['footer_privacy'] = 'include/footer_privacy';
      $data['footer_subscribe'] = 'include/footer_subscribe';
      $data['header_black_menu'] = 'include/header_black_menu';    
      $data['notification_bar'] = $notification_bar;
      $data['header_logo_white'] = $header_logo_white;
      $data['main_menu'] = $main_menu;

      $error = isset($data['message']['type'])=='error' ? true :false ;
      $data["error"] = $error;
      $data["is_logged_in"] = $this->is_logged_in;   
      //var_dump($profile_data);exit;
      //$this->data = array_merge($profile_data,$this->data); 
      $data = array_merge($data,$this->data);

      $this->load->view('sell/sellers_new',$data);


     } 

     else 
     {
         $referrer = $_SERVER['HTTP_REFERER'];
         redirect($referrer);
      }

 }

public function save_chat()
{
      $message = array ();

      $chat_room_id = $this->input->post('chat_room_id');
      $chat_sender = $this->input->post('chat_sender');
      $chat_reciever = $this->input->post('chat_reciever');
      $chat_line = $this->input->post('chat_line');

      $data = array(
        'chat_room_id' => $chat_room_id,
        'chat_sender' =>  $chat_sender,
        'chat_reciever' => $chat_reciever,
        'chat_line' =>  $chat_line
        );

     
       $this->db->insert('chat', $data);

       $message = array (
                      "message" =>"data saved",
                      "success"=>true,
                  );


      echo json_encode($message);
      return;

}

public function get_chat()
{
      $message = array ();
      $chat_history = array ();

      $chat_room_id = $this->input->post('chat_room_id');
      
      $this->db->select('*');
      $this->db->where("chat_room_id", $chat_room_id );
      $query = $this->db->get("chat");
      $chat = $query->result_array();

       $message = array (
                      "chat" =>$chat,
                      "room" =>$chat_room_id, 
                      "success"=>true,
                  );


      echo json_encode($message);
      return;

}



    /*Refer a friend Today**/

    public function send_invites()
    {
       
          //collect inputs
          $invite_profile_id  = $this->input->post('invite_profile_id',TRUE);
          $emails  = $this->input->post('emails',TRUE);

          //load model
          $this->load->model('invite_model','invites');

          //define a view communcation means using json messages array
          $message = array ();

          $insert_result = false; 
          $is_email_not_success = true;

          $collection_of_emails  = explode(',',$emails);


          //construct and get the invite profile details like imahes and so 
          
          $inivite_profile_url = base_url('sell/seller').'/'.$invite_profile_id;     

          $profile_information =(object) $this->load_profile_detail_with_image($invite_profile_id);
         
          $invite_profile_full_name = ucfirst($profile_information->fname).' '.ucfirst($profile_information->lname);
          
          $from ='noreplay@madebyus4u.com';   

          $subject = ucwords($invite_profile_full_name)." invited you to MadeByus4u.com!";
        
         //above are common so out of loop

          foreach ($collection_of_emails as $email_invites_) {
           
           $to = $email_invites_ ;
           $profile_image_path = base_url().$profile_information->profile_image;

           $is_invited_before = $this->invites->filter_sent_emails_out($invite_profile_id,$to);
          // var_dump($is_invited_before);var_dump($to);
            
            if($is_invited_before)
                continue; //do not send email we have sent before :)

             //save invites
             $insert_result = $this->invites->save_invite_for_profile($invite_profile_id,trim($to));

             if($insert_result==true) 
               $is_email_not_success = send_email_with_profile_detail ($to,$from,$subject,$invite_profile_full_name,$profile_image_path,$inivite_profile_url,$flag=2);
            }
            
            //if error true 
             if ($is_email_not_success) {

                 //show_error($this->email->print_debugger());
                 $content = $this->email->print_debugger();

               
                 //construct error
                $message = array (
                      "message" => "Error occurred , Please try again! or fix your email address!".$content,
                      "success"=>true,
                  );


                echo json_encode($message);
                return;
          
               } 

               else {

                $content = '<b> Thankyou for invitation </b>! Please , check your email.';
                //send email json message for the view
                //save into notification tables
                $notification_text = 'You have sent invitation email to '.$email_invites_.'to invite a friend '.$invite_profile_full_name;
                $this->notifications->push_notifications('EMAIL_INVITES',null,null,$this->profile_id,$notification_text);
                 
                $message = array (
                      "message" => $content,
                      "success"=>true,
                );

               $iniviter_profile_url = base_url('sell/seller').'/'.$invite_profile_id;     
              //TODO:now send email to inviter that he's inviatation was success
               $sender_profile_email = $this->session->userdata('identity'); //get email address for current user
              //send the email to the sender him self
               $to = $sender_profile_email ;
               $subject = "MadeByus4u.com invite email sent to your freinds succesfully!";

               // send emails
               $is_email_success = send_email_with_profile_detail ($to,$from,$subject,$invite_profile_full_name,$profile_image_path,$iniviter_profile_url,$flag=3);
          
              echo json_encode($message);
              return;
           
         }

        
    }


    private function _product_information_for_seller($profile_id) {

      //start loading product on listings tab
      $product_total_rows = $this->seller_product->count_by(array('profile_id'=>$profile_id));
      $config = array();
      $config["base_url"] = base_url() . "sell/seller";
      $config["total_rows"] = $product_total_rows;
      $config["per_page"] = 10;
      $config["uri_segment"] = 4;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_product_data = $this->seller_product->get_all_seller_product_lisiting($profile_id,null,$config["per_page"], $page);
      $this->data["product_links"] = $this->pagination->create_links();
      $this->data['all_product_data'] = $all_product_data;
      $this->data['product_total_rows'] = $product_total_rows;
      $this->data['products'] = $all_product_data;
      $this->data['product_listing'] = 'product/product_listing';
      $this->data['paginate_page'] = 'include/paginate_page';

      return $this->data ;
    }

    private function _profile_information_for_seller($profile_id) {

      $this->loaded_profile_same_as_user = true;//by default current user profile
     
       if( $this->is_logged_in==true) {
         
           $current_profile_id = $this->profile_id;
           $user_profile_id = $profile_id;

           if( $current_profile_id != $user_profile_id) {          
              //different user
              $this->loaded_profile_same_as_user = false;
           }
        } 

       $this->data['loaded_profile_same_as_user'] = $this->loaded_profile_same_as_user;
       $this->data['is_store_created'] = !empty($this->is_store_created) ? $this->is_store_created : 0;
       $profile_data =  $this->load_profile_detail_with_image($profile_id);
       $this->data = array_merge($profile_data,$this->data);
      
       return $this->data;


    }

    private function _store_information_for_seller($profile_id) {

      $store_total_rows = $this->store->count_by(array('is_launched'=>1,'owner_profile_id'=>$profile_id));
      $all_store_data = $this->store->get_store_lisiting($profile_id);
      $this->data['all_store_data'] = $all_store_data;
      $this->data['store_total_rows'] = $store_total_rows;
      //$data['product_total_rows'] = $product_total_rows;
      $data['all_store_data'] = $all_store_data; //pass store information , profile,media,store
      return $this->data;
    }


    private function _friend_information_for_seller($profile_id,$loaded_profile_same_as_user) {

      /*** friends ****/
     $friends_list =  $this->friends->get_my_friends($profile_id);
     //get pending friend request for current logged in profile/viewer

    if($loaded_profile_same_as_user==true)
    
     $friends_list = $this->friends->get_my_friends($profile_id);
     $friends_view = 'friends/friends_view';
     $this->current_friends_count = count($friends_list);
     $this->data['friends_view'] =  $friends_view;
     $this->data['friends_list'] =  $friends_list;
     $this->data['count_friends'] =  $this->current_friends_count;
        //var_dump($friends_list[0]->profile_id);die;
       
      


       

     return  $this->data;
    }


    private function _friend_request_information_for_seller($profile_id,$loaded_profile_same_as_user) {

      /***friend request related ***/
      $friend_requests =  $this->friends->get_friend_requests($profile_id);
      $friends_request_view = 'friends/friends_request_view';
      $this->data['friends_request_view'] =  $friends_request_view;
      $this->data['friend_requests'] =  $friend_requests;
      $this->data['count_friend_request'] =  count($friend_requests);
      //check if current profile loaded is not the owner and if not 
      //check if the logged in profile has sent a freind request to it befoer or not ? 
      $is_pending_friend_reques = false;
      
      if($this->is_logged_in) 
      {
        //get_pending_friend_requests
        $is_pending_friend_requets = in_array($profile_id,$this->current_profile_pending_requests_id);
        $this->data['is_pending_friend_requets']= $is_pending_friend_requets;
      }

      //check if the logged in profile has a request recived from the current profile loaded on sellers page.
      //so that we can show accept button instead of whatever
      $this_user_has_sent_request = false;

      if($this->is_logged_in && $loaded_profile_same_as_user==false) 
      {
    
        if(in_array($profile_id,$this->my_requests_to_approve_id)) 
        {
            $this_user_has_sent_request = true;
        }
          
        $this->data['this_user_has_sent_request']= $this_user_has_sent_request;
       
        $this_user_is_friend = false;
      
       if($this->is_logged_in && $loaded_profile_same_as_user==false)
       {
          if(in_array($profile_id,$this->current_profile_friend_lists))
           {
                    $this_user_is_friend = true;
           }

          $this->data['this_user_is_friend']= $this_user_is_friend;     
       }
       
      }

       /*
       var_dump($this->my_requests_to_approve_id);
       var_dump($this_user_is_friend)   ;
       var_dump($this_user_has_sent_request)   ;
       var_dump($is_pending_friend_requets);exit;
       */

      if( $loaded_profile_same_as_user==false ) {
        //it other user , the check if the logged in user has sent a freind request
      //  var_dump($this->profile_id);
        $request_sent_status = $this->friends->get_by(array('sender_profile_id'=>$profile_id,'reciever_profile_id'=>$this->profile_id,'seen_status'=>'UNREAD'));
        //var_dump( $request_sent_status);
        if(count($request_sent_status)>0) 
        {
           $this->data['request_sent'] = true;
        }
        else 
        {
           $this->data['request_sent'] = false;
        }
    
      }

      return $this->data;

    }

    private function _notification_information_for_seller($profile_id) {
      
      /***notification-listing***/
      $notifications= $this->notifications->get_all_notifications($profile_id);
      $notification_view = 'notifications/notification_view';
      $this->data['notification_view'] =  $notification_view ;
      $this->data['notifications'] =  $notifications;
      $this->data['count_notifications'] =  count($notifications);

       // dump($notifications);exit;

      return $this->data;

    }

    private function _video_information_for_seller($profile_id) {
  
     /***videos-listing***/
     $videos= $this->videos->get_all_videos($profile_id);
     $videos_view = 'videos/videos_view';
     $this->data['videos_view'] =  $videos_view;
     $this->data['videos'] =  $videos;
     $this->data['count_videos'] =  count($videos);
     return $this->data;
    }

    private function _photo_information_for_seller($profile_id) {

       /***photo-listing***/
      $photos= $this->photos->get_all_photos($profile_id);
      $photos_view = 'photos/photos_view';
      $this->data['photos_view'] =  $photos_view;
      $this->data['photos'] =  $photos;
      $this->data['count_photos'] =  count($photos);
       // dump($photos);exit;
      return $this->data;

    }

    private function _likes_information_for_seller($profile_id) {
     
     $liked_products= $this->likes->get_products_i_liked($profile_id);
     // /dump($liked_products);exit;
     $likes_view = 'likes/likes_view_new';
     $this->data['likes_view'] =  $likes_view;
     $this->data['liked_products'] =  $liked_products;
     $this->data['count_liked_products'] =  $this->likes->get_liked_count($profile_id);
     $this->data['products_i_like_count'] =  count($liked_products);
     //dump($this->data['count_liked_products']);exit;
     return $this->data; 
     }

    private function _messaging_information_for_seller($profile_id) {
      //for select box on compsoe message
      $this->data['members_list'] = $this->get_select_list_of_memebers($profile_id);   
      
      $this->data['is_seller_page'] = true;    
      //print json_encode({'memebers'$this->get_select_list_of_memebers());    

      //load inbox messages
      $inbox_messages=$this->messages->get_inbox_messages($profile_id,1,1);

      //load sent messages
      $sent_messages=$this->messages->get_sent_messages($profile_id,1,1);

      //load trashed messages
      $trashed_messages = $this->messages->get_trashed_messages($profile_id,1,1);

      //load trashed messages
      $draft_messages = $this->messages->get_draft_messages($profile_id);

      //load trashed messages
      $important_messages = $this->messages->get_important_messages($profile_id);


      $this->data['trashed_messages'] = $trashed_messages;
      $this->data['inbox_messages'] = $inbox_messages;
      $this->data['sent_messages'] = $sent_messages;
      $this->data['draft_messages'] = $draft_messages;
      $this->data['important_messages'] = $important_messages;
      /**inbox**/
      /**messages**/
      $this->data['messages_view'] = 'messages/messages_view_new';
      $this->data['drafts_view'] = 'messages/drafts_view_new';
      $this->data['inbox_view'] = 'messages/inbox_view_new';
      $this->data['sent_view'] = 'messages/sent_view_new';
      $this->data['trash_view'] = 'messages/trash_view_new';
      $this->data['important_view'] = 'messages/important_messages_view';
      $this->data['drafts_view'] = 'messages/drafts_view_new';
      $this->data['inbox_replay_view'] = 'messages/inbox_replay_view_new';

      /**end of messages*/
      return $this->data;

    }


  

}