<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/31/2014
 * Time: 10:11 AM
 */

class Product extends  MY_Controller {

    var $loaded_profile_same_as_user;

    public function __construct() {

        parent::__construct();
        $this->load->model('product_model','products');
        $this->load->model('like_model','likes');
        $this->load->model('profile_model','profile');
        $this->load->model('category_model','menu_categories');
    }

    public function index(){   
       
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';

        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data["is_logged_in"] = $this->is_logged_in;   

        redirect('sell/sell',$data);

    }
    
     /*
     product/browse?catageory=''&subcategory=''
     */

     public function browse($category_id){

        //get varibles
        // $category = $this->input->get('category');
        // $subcategory = $this->input->get('subcategory');

        // var_dump($category);exit;

        $this->load->library('pagination');
        $config = array();

        $config["base_url"] = base_url() . "product/new_arrivals";

        $config["per_page"] = 8;
        $config["uri_segment"] = 0;
        $config['display_pages'] = TRUE;
        $page = 0;//($this->uri->segment(2)) ? $this->uri->segment(3) : 0;
        $products = $this->products->display_product_by_category_subcategory($category_id,$config["per_page"],$page);
        $total_rows = count($products);
        $config["total_rows"] = $total_rows;
        $this->pagination->initialize($config);

        $paginate_page = 'include/paginate_page';

        $notification_bar = 'include/notification_bar';

     
        $paginate_page = 'include/paginate_page';
        $notification_bar ='include/notification_bar';
        $header_logo_white ='include/header_logo_white';
        $main_menu = 'include/main_menu';
        $product_listing='product/product_listing_new';

        $data['product_listing']=$product_listing;
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['paginate_page'] = $paginate_page; 
        $data['notification_bar'] = $notification_bar; 
        $data['header_logo_white'] = $header_logo_white;
        $data['main_menu'] = $main_menu;
        $data['footer_page'] = 'include/footer_page';

        $data["products"] = $products;  
        $data["total_rows"] = $total_rows;

        $data["per_page"]   = $config["per_page"];
        $data["links"] = $this->pagination->create_links();

         $all_categories = $this->menu_categories->get_all();
         $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
         $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
         $data['all_categories'] = $all_categories;
         $data['sub_parent_categories'] = $sub_parent_categories;
         $data['parent_categories'] = $parent_categories;
         $data['column_main_menu'] = 'include/column_menu_top';
         //dump($all_categories);exit;

        $this->load->view('product/product_category_listing',$data);

    }
    

  public function new_arrivals(){

				
		$this->load->library('pagination');		
		$config = array(); 
		$total_rows = $this->products->count_all(); //url_connection
        $config["base_url"] = base_url() . "product/new_arrivals";
		$config["total_rows"] = $total_rows; //url_pages_why_
		$config["per_page"] = 8; //number of pages 
		$config["uri_segment"] = 3; // url_segment pages
		$config['display_pages'] = TRUE;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;						
		$products = $this->products->get_new_arrival_products($config["per_page"],$page);
		$data["products"] = $products;	
        $data["total_rows"]	= $total_rows;
        $data["per_page"]   = $config["per_page"];
		$data["links"] = $this->pagination->create_links();
	
		$paginate_page = 'include/paginate_page';
		$notification_bar ='include/notification_bar';
		$header_logo_white ='include/header_logo_white';
		$main_menu = 'include/main_menu';

		$product_listing='product/product_listing_new';
        $all_categories = $this->menu_categories->get_all();
        $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
        $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
        $data['all_categories'] = $all_categories;
        $data['sub_parent_categories'] = $sub_parent_categories;
        $data['parent_categories'] = $parent_categories;
        $data['column_main_menu'] = 'include/column_menu_top';

      $data['product_listing']=$product_listing;
		$data['footer_privacy'] = 'include/footer_privacy';
		$data['footer_subscribe'] = 'include/footer_subscribe';
		$data['header_black_menu'] = 'include/header_black_menu';
		$data['paginate_page'] = $paginate_page; 
		$data['notification_bar'] = $notification_bar; 
		$data['header_logo_white'] = $header_logo_white;
		$data['main_menu'] = $main_menu;
		$data['footer_page'] = 'include/footer_page';	

		$this->load->view('product/new_arrivals',$data);
	}

    /**
     * @param $id
     */

    public function add_comment(){

      $comment = $this->input->post('comment');
      $comment_to_id = $this->input->post('comment_to_id');
      $comment_by_id = $this->input->post('comment_by_id');
      $product_id = $this->input->post('product_id');
      $parent_comment_id = $this->input->post('parent_comment_id');


      $data = array(
        'comment' => $comment,
        'commeted_to_id' =>  $comment_to_id,
        'commeted_by_id' => $comment_by_id,
        'product_id' =>  $product_id,
        'comment_date' => date('Y-m-d H:i:s'),
        'parent_comment_id' => $parent_comment_id
        );

     
       $this->db->insert('comments', $data);

       $is_bid_buy_page = $this->uri->segment(4);
       
       $id = $this->uri->segment(3);
        
       $profile = $this->profile->get( $this->profile_id);
        
       if( !empty($id) && !empty($is_bid_buy_page))  { 
            
          //manipulate like status and show accrodingly on view liked or like to be liked :{}    
          $liked_or_not = $this->likes->check_if_liked_before($id,$this->profile_id);
          $data['liked_or_not'] = $liked_or_not;

          $product = array();

          $exist_or_not = $this->products->get($id);

          if( !empty($exist_or_not) && count($exist_or_not) >0 ) {
              $product = $this->products->get_single_product_detail($id);
          } else {
               $referrer = $_SERVER['HTTP_REFERER'];
               redirect($referrer);
          }

        //check if the current product is already the logged in user or not

        $product_ids = $this->get_current_profile_products($this->profile_id);
        $is_the_product_current_user = false; 

        if(in_array($id,$product_ids)) {

            $is_the_product_current_user = false;
        } 
        
        $this->db->select('*');
        $this->db->where("product_id", $id );
        $this->db->where("parent_comment_id", 0);
        $query = $this->db->get("comments");
        $comments = $query->result_array();

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_user_details = 'product/product_user_detail';
        $product_other_details = 'product/product_other_listing';
        $comment_view = 'comment/comment_view';
        $main_menu = 'include/main_menu';

        $data['main_menu'] = $main_menu;
        $data['header_logo_white'] = $header_logo_white;
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['social_sharing_button'] = 'include/social_sharing_buttons';

  
        $data['product_user_details']= $product_user_details;
        $data['product_other_details']= $product_other_details;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['comment_view'] = $comment_view;

        $data['joined_date'] =  date_format(date_create($product->joined_date) ,'Y-m-d');
       
        $data['product'] = $product;
        $data['is_liked'] = $liked_or_not;
        $data['is_the_product_current_user'] = $is_the_product_current_user;

        $data['is_bid_page'] = strcmp(strtolower($is_bid_buy_page),"bid")==0;
        $data['is_buy_page'] = strcmp(strtolower($is_bid_buy_page),"buy")==0;       
        $data["is_logged_in"] = $this->is_logged_in;   
        $data['profile'] = $profile;
        $data['comments'] = $comments;
        $this->load->view('product/product_detail',$data);

        } else {
          //TODO:error pages .:) well done
          $referrer = $_SERVER['HTTP_REFERER'];
          redirect($referrer);
       }
    }


    public function comment_like(){

      if(!$this->input->is_ajax_request()) {
          exit('Not allowed!');
       }

      $liked_comment_id = $this->input->post('comment_id');
      $liker_profile_id = $this->profile_id;

      $this->db->select('*');
      $this->db->where("comment_id", $liked_comment_id );
      $this->db->where("liker_profile_id", $liker_profile_id);
      $query = $this->db->get("comment_likes");
      $past_comment_likes = $query->result_array();

      if(count($past_comment_likes)==0){

        $data = array(
        'comment_id' => $liked_comment_id,
        'liker_profile_id' =>  $liker_profile_id,
        'created_at' => date('Y-m-d H:i:s')
        );

      $this->db->insert('comment_likes', $data);
      $this->db->select('*');
      $this->db->where("comment_id", $liked_comment_id );
      $query = $this->db->get("comment_likes");
      $comment_likes = $query->result_array();
      $new_like = count($comment_likes);
      
      $response = array (
                      "success"=>true,
                      "past_like"=>false,
                      "like"=>$new_like,
                  );

        echo json_encode($response);

      } else {


      $this->db->select('*');
      $this->db->where("comment_id", $liked_comment_id );
      $query = $this->db->get("comment_likes");
      $comment_likes = $query->result_array();

      $new_like = count($comment_likes);

        $response = array();

       $response = array (
                      "success"=>true,
                      "past_like"=>true,
                      "like"=>$new_like,
                  );

        echo json_encode($response);

      }
        

    }

     private function _friend_information_for_seller($profile_id,$loaded_profile_same_as_user) {

      /*** friends ****/
     $friends_list =  $this->friends->get_my_friends($profile_id);
     //get pending friend request for current logged in profile/viewer

    if($loaded_profile_same_as_user==true)
    
     $friends_list = $this->friends->get_my_friends($profile_id);
     $friends_view = 'friends/friends_view';
     $this->current_friends_count = count($friends_list);
     $this->data['friends_view'] =  $friends_view;
     $this->data['friends_list'] =  $friends_list;
     $this->data['count_friends'] =  $this->current_friends_count;

     return  $this->data;
    }


    private function _friend_request_information_for_seller($profile_id,$loaded_profile_same_as_user) {

      /***friend request related ***/
      $friend_requests =  $this->friends->get_friend_requests($profile_id);
      $friends_request_view = 'friends/friends_request_view';
      $this->data['friends_request_view'] =  $friends_request_view;
      $this->data['friend_requests'] =  $friend_requests;
      $this->data['count_friend_request'] =  count($friend_requests);
      //check if current profile loaded is not the owner and if not 
      //check if the logged in profile has sent a freind request to it befoer or not ? 
      $is_pending_friend_requets = false;
      
      if($this->is_logged_in) 
      {
        //get_pending_friend_requests
        $is_pending_friend_requets = in_array($profile_id,$this->current_profile_pending_requests_id);
        $this->data['is_pending_friend_requets']= $is_pending_friend_requets;
      }

      //check if the logged in profile has a request recived from the current profile loaded on sellers page.
      //so that we can show accept button instead of whatever
      $this_user_has_sent_request = false;

      if($this->is_logged_in && $loaded_profile_same_as_user==false) 
      {
    
        if(in_array($profile_id,$this->my_requests_to_approve_id)) 
        {
            $this_user_has_sent_request = true;
        }
          
        $this->data['this_user_has_sent_request']= $this_user_has_sent_request;
       
        $this_user_is_friend = false;
      
       if($this->is_logged_in && $loaded_profile_same_as_user==false)
       {
          if(in_array($profile_id,$this->current_profile_friend_lists))
           {
                    $this_user_is_friend = true;
           }

          $this->data['this_user_is_friend']= $this_user_is_friend;     
       }
       
      }

       /*
       var_dump($this->my_requests_to_approve_id);
       var_dump($this_user_is_friend)   ;
       var_dump($this_user_has_sent_request)   ;
       var_dump($is_pending_friend_requets);exit;
       */    

      if( $loaded_profile_same_as_user==false ) {
        //it other user , the check if the logged in user has sent a freind request
      //  var_dump($this->profile_id);
        $request_sent_status = $this->friends->get_by(array('sender_profile_id'=>$profile_id,'reciever_profile_id'=>$this->profile_id,'seen_status'=>'UNREAD'));
        //var_dump( $request_sent_status);
        if(count($request_sent_status)>0) 
        {
           $this->data['request_sent'] = true;
        }
        else 
        {
           $this->data['request_sent'] = false;
        }
    
      }

      return $this->data;

    }

     private function _profile_information_for_seller($profile_id) {

      $this->loaded_profile_same_as_user = true;//by default current user profile
     
       if( $this->is_logged_in==true) {
         
           $current_profile_id = $this->profile_id;

           $user_profile_id = $profile_id;

           if( $current_profile_id != $user_profile_id) {          
              //different user
              $this->loaded_profile_same_as_user = false;
           }
        } 

       $this->data['loaded_profile_same_as_user'] = $this->loaded_profile_same_as_user;
       $this->data['is_store_created'] = !empty($this->is_store_created) ? $this->is_store_created : 0;
       $profile_data =  $this->load_profile_detail_with_image($profile_id);
       $this->data = array_merge($profile_data,$this->data);
      
       return $this->data;


    }

    private function _likes_information_for_seller($profile_id) {
     
     $liked_products= $this->likes->get_products_i_liked($profile_id);
     // /dump($liked_products);exit;
     $likes_view = 'likes/likes_view';
     $this->data['likes_view'] =  $likes_view;
     $this->data['liked_products'] =  $liked_products;
     $this->data['count_liked_products'] =  $this->likes->get_liked_count($profile_id);
     $this->data['products_i_like_count'] =  count($liked_products);
     //dump($this->data['count_liked_products']);exit;
     return $this->data; 
     }


    /**
     * Product details
     * WHen start pouring , when men ain't home  - Nate Dogg [BackDoor]
     * @param $id
     */
    public function detail($id){

       //get the owner of the product to notify ?
       //var_dump($id);exit;

        $is_bid_buy_page = $this->uri->segment(4);
        $id = $this->uri->segment(3);
      
       if( !empty($id) && !empty($is_bid_buy_page))  { 
            
        //var_dump('exit');exit;
      
        $product = array();

        $exist_or_not = $this->products->get($id);


        if( !empty($exist_or_not) && count($exist_or_not) >0 ) {

        	$product = $this->products->get_single_product_detail($id);
        } else {
             $referrer = $_SERVER['HTTP_REFERER'];
             redirect($referrer);
        }


     //get the current profile id of the product ? SNOOP DOGGY  - what's my name ? :)

      $profile_id = $this->profile_id;

      
      //product
      $product_total_rows = $this->products->count_by(array('profile_id'=>$profile_id));

      $data['product_total_rows']= $product_total_rows;
      
      //liked counts
      $this->_likes_information_for_seller($profile_id);
      
          //load profile related informations
      $profile_data = $this->_profile_information_for_seller($profile_id);
       
       //loaD FREIND REQUEST
       $this->_friend_request_information_for_seller($profile_id,$this->loaded_profile_same_as_user);   
       $data["is_logged_in"] = $this->is_logged_in;   
      
       //Load seller store info
       $this->_friend_information_for_seller($profile_id,$this->loaded_profile_same_as_user);

       //get random user profile form DATABASE
        $profile_random_data = $this->profile->get_random_profile($profile_id);

        $config["per_page"] =4 ;

        $page = 0;

        $all_product_data = $this->products->get_all_seller_product_lisiting($profile_id,$product->product_id,$config["per_page"], $page);

       // dump($all_product_data);exit;
        $data['products'] = $all_product_data;

        $data['profile_username_data'] = $profile_random_data[0]->fname." ".$profile_random_data[0]->lname;

        //dump($profile_random_data);exit;

       //check if the current product is already the logged in user or not

        $product_ids = $this->get_current_profile_products($profile_id);

           $is_the_product_current_user = false;

        if(in_array($id,$product_ids)) {

            $is_the_product_current_user = false;
        } 
        
        $this->db->select('*');
        $this->db->where("product_id", $id );
        $this->db->where("parent_comment_id", 0);
        $query = $this->db->get("comments");
        $comments = $query->result_array();

        //manipulate like status and show accordingly on view liked or like to be liked :{}
        $liked_or_not = $this->likes->check_if_liked_before($id,$this->profile_id);
        $data['liked_or_not'] = $liked_or_not;

         
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $product_user_details = 'product/product_user_detail_new';
        $product_other_details = 'product/product_other_listing_new';
        $comment_view = 'comment/comment_view_new';
        $main_menu = 'include/main_menu';

       $all_categories = $this->menu_categories->get_all();
       $parent_categories = $this->menu_categories->get_many_by(array('parent_id'=>null));
       $sub_parent_categories = $this->menu_categories->get_many_by(array('parent_id IS NOT NULL'));
       $data['all_categories'] = $all_categories;
       $data['sub_parent_categories'] = $sub_parent_categories;
       $data['parent_categories'] = $parent_categories;
       $data['column_main_menu'] = 'include/column_menu_top';


           $data['main_menu'] = $main_menu;
        $data['header_logo_white'] = $header_logo_white;
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['social_sharing_button'] = 'include/social_sharing_buttons';

  
        $data['product_user_details']= $product_user_details;
        $data['product_other_details']= $product_other_details;
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;
        $data['comment_view'] = $comment_view;

        $data['joined_date'] =  date_format(date_create($product->joined_date) ,'Y-m-d');
       
        $data['product'] = $product;
        $data['is_liked'] = $liked_or_not;
        $data['is_the_product_current_user'] = $is_the_product_current_user;

        $data['is_bid_page'] = strcmp(strtolower($is_bid_buy_page),"bid")==0;
        $data['is_buy_page'] = strcmp(strtolower($is_bid_buy_page),"buy")==0;       
        $data["is_logged_in"] = $this->is_logged_in;   
        
        $data['comments'] = $comments;

        $data = array_merge($data,$this->data);

        //dump($data);   exit;

        $this->load->view('product/product_detail_new',$data);

        
     }  //end if variable are null show error below

     else {

     //TODO:error pages .:) well done
     $referrer = $_SERVER['HTTP_REFERER'];
     redirect($referrer);
    }
   }
}