<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */


class Profile_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   public $before_create = array( 'timestamps' );

   protected function timestamps($profile_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $profile_row['created_date'] = date('Y-m-d H:i:s');
        return  $profile_row;
    }


    /**
     * Define relationship a memeber
     * have One to One , One to Many ,
     * Many to Many if any
     */

    public $belongs_to = array('media' => array( 'primary_key' => 'profile_image_id' ,'model'=>"media_model"),
                                'user' => array( 'primary_key' => 'user_id' ,'model'=>'ion_auth_model'),
                                'state' => array( 'primary_key' => 'state' ,'model'=>'state_model'),
                                'account' => array( 'primary_key' => 'account_id' ,'model'=>'account_model'),
                                'shipping' => array( 'primary_key' => 'profile_id' ,'model'=>"shipping_model")                           
                                );

    public $has_many = array('stores' => array( 'primary_key' => 'owner_profile_id' ,'model'=>"store_model") ,
                                'payments' => array( 'primary_key' => 'profile_id' ,'model'=>"payment_model"),
                                'products' => array( 'primary_key' => 'profile_id' ,'model'=>"product_model"),
                                'ratings' => array( 'primary_key' => 'profile_id' ,'model'=>"rating_model"),
                                'photos' => array( 'primary_key' => 'profile_id' ,'model'=>"photo_model"),
                                'invites' => array( 'primary_key' => 'profile_id' ,'model'=>"invite_model"),
                                'notifications'=>array('primary_key' =>'for_profile_id' ,'model'=>'notification_model'),
                                 'likes' => array( 'primary_key' => 'product_id' ,'model'=>"like_model"), 
                                );




    public function register($post)
    {


        $firstname = $post['firstname'];
        $lastname = $post['lastname'];
        $city = $post['city'];
        $state = $post['state'];
        $email = $post['email'];

        //check if user has been saved
        $this->load->model('ion_auth_model', 'users_profile');
        $user_id = $this->users_profile->register($post['username'], $post['password'], $post['email']);


        if ($this->ion_auth->errors()) {
            $this->set_error_handler($this->ion_auth->errors());
        }

        if ($user_id) {

            $member_insert_data = array(
                'fname' => ucfirst($firstname),
                'lname' => $post['lastname'],
                'zipcode' => $post['zipcode'],
                'state' => $post['state'],
                'city' => $post['city'],
                'user_id' => $user_id,
            );

            $member_id = $this->insert($member_insert_data);

            if (!$member_id) {
                return FALSE;
            } else {

                /*
                * send email about
                 * registration success and activation code link
                */
                $this->load->helper('general');
                //generate activation code
                $activation_code = sha1(md5(microtime()));
                $new_activation_data = array(
                    'activation_code' => $activation_code,
                    'active' => 0
                );

                $update_result = $this->users->update($user_id, $new_activation_data);

                //store user data and activation
                $this->session->set_userdata('user_id', $user_id);
                $this->session->set_userdata('activation_code', $activation_code);
                $this->session->set_userdata('email', $email);
                $this->session->set_userdata('firstname',ucfirst($firstname));

                //from general helpr class
                send_activation_email($firstname,$user_id,$activation_code,$email);
                return $member_id;
            }
        }

    }

    /**
    **/
    function check_verfication($profile_id) {

        return $this->get($profile_id)->is_profile_verified ;
    }

    function with_media($type='profile_image') {
        $this->_database->join('medias',$this->_table.'.'.$this->primary_key.' = medias.profile_id')->where('medias.type', $type);
   }

   function get_Verfied_Profiles($per_page,$page) {

     $verified_profiles = $this->with('media')->limit($per_page,$page)->order_by('RAND()')->get_many_by('is_profile_verified','1');
     //var_dump($verified_profiles);exit;

      //load friend model to query the friends count
      $this->load->model('friend_model','friends');

     if(count($verified_profiles)>0) {

              foreach ($verified_profiles as $profile) {

                  $profile->profile_rating = $profile->avg_rating;
                  //get the friends count
                  $friends_count = $this->friends->count_friends($profile->id);
                  
                  $profile->freinds_count =  $friends_count;
                  
                  $products_listing = $this->friends->count_products($profile->id);

                  $profile->products_count =  $products_listing;
                    
                  if(!empty($profile->media)) {

                     $profile->media->file_name = '/uploads/profile/'.$profile->id .'/avatar/'. $profile->media->file_name;
                  } 
                  else {

                     $profile->media = new stdClass();
                     $profile->media->file_name = '/uploads/no-photo.jpg';
                  }
                   
             }
                
    }
    //var_dump($verified_profiles);exit();
     return $verified_profiles;
   }

 function build_memeber_list($current_profile_id) {

   $this->load->model('friend_model','friends');
   $friend_list = $this->friends->get_my_friends($current_profile_id);
   $friends_profile_id = array();
  
   $verified_profiles = array();
   
    foreach ($friend_list as $key => $object) {
       array_push($friends_profile_id,$object->profile_id);
    }

    foreach ($friends_profile_id as $pro_id) {
        array_push($verified_profiles,$this->get($pro_id));
    }

    $select_list_profile = array();

    foreach ($verified_profiles as $key => $profile_object) {
     
      if($current_profile_id==$profile_object->id)
        continue;
      $select_list_profile[$profile_object->id] = ucfirst($profile_object->fname)." ".ucfirst($profile_object->lname);
    }
    //add select for all friends
    $select_list_profile[-1]='To all your friends';
    // dump($select_list_profile);exit;
    
  return $select_list_profile;
 }

 //get random profiles :) love this

      public function get_random_profile($except_profile_id) {

      /* 
      The old way of accessing the products but generates lot of query
      $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
      */

      $sql = "select * from profiles where id <> ? order by rand() limit 1";  

      $profile_data = $this->db->query($sql, array($except_profile_id));

      return $profile_data->result();

      }

    public function get_all_members_for_admin($per_page,$page)
    {

        $all_profiles = $this->with('media')->limit($per_page, $page)->get_all();
        //var_dump($verified_profiles);exit;

        //load friend model to query the friends count
        $this->load->model('friend_model', 'friends');

        if (count($all_profiles) > 0) {

            foreach ($all_profiles as $profile) {

                $profile->profile_rating = $profile->avg_rating;
                //get the friends count
                $products_count = $this->products->count_listing_for_seller($profile->id);
                $friends_count = $this->friends->count_friends($profile->id);
                $profile->freinds_count = $friends_count;
                $profile->products_count = $products_count;

                if (!empty($profile->media)) {

                    $profile->media->file_name = '/uploads/profile/' . $profile->id . '/avatar/' . $profile->media->file_name;
                } else {

                    $profile->media = new stdClass();
                    $profile->media->file_name = '/uploads/no-photo.jpg';
                }

            }

        }

        return $all_profiles;
    }

}









