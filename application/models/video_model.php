<?php
/**
 * Created by Daniel Adenew.
 * Date:28/07/15
 * Craig Robinson : One World Solutions LLC
 */

class Video_model extends MY_Model {

	public function __construct() {
		parent::__construct();
		$this->_database = $this->db;
	}
	
	 public $belongs_to = array (
                            'profile' => array( 'primary_key' => 'profile_id' ,'model'=>"profile_model"),                             
                           );

   public $before_create = array('timestamps' );

   public function _order_by_created_date() {
        $this->db->order_by('videos.created_date', 'desc');
        return $this;
    }

   protected function timestamps($videos_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $videos_row['created_date'] = date('Y-m-d H:i:s');
        return  $videos_row;
    }

	 /**
     * @param $profile_id
     * @param $upload_config
     * @param $file_post_name
     * @return bool|int
     */

    public function save_my_videos($profile_id,$title,$file_post_name)
    {

       $upload_data = $this->upload_videos_profiles($profile_id,$file_post_name);
     
       var_dump($upload_data);exit;
 
        if ( isset($upload_data[0]['file_name']) ) {

            $file_name = $upload_data[0]['file_name'];
            $file_path = $upload_data[0]['file_path'];

            $profile_video_id = $this->insert(array(
                'file_name' => $file_name,
                'full_path' => $file_path,
                'profile_id' => $profile_id,
                'title'=>$title,
            ));

           // var_dump($profile_photos_id);
                    
            if($profile_video_id)
            return $profile_video_id;
            else
            return -1;

          }

    }
	


	/**
     * @param $profile_id
     * @param $file_post_name
     * @return array
     */
    public function upload_videos_profiles($profile_id,$file_post_name) {


         $this->load->library('upload');
         //get the image file name for the store
         $upload_data_store = array();

         //get the image file name for the products
         $upload_data_products = array();            
     
         //load the configuration
         $upload_config = $this->config->item('upload_config_video_files');

       //rename files first
       $temp = explode(".", $_FILES[$file_post_name]["name"]);
       $newfilename = 'video' . rand(1, 99999) . '.' . end($temp);
       $upload_config['file_name'] = $newfilename;

        $pathToUpload = "./uploads/profile/" . $profile_id . "/videos/";
        //load the configuration
        $upload_config = $this->config->item('upload_config_video_files');

        $upload_config['upload_path'] = $pathToUpload;

        //rename files first
        $temp = explode(".", $_FILES[$file_post_name]["name"]);
        $newfilename = 'video' . rand(1, 99999) . '.' . end($temp);
        $upload_config['file_name'] = $newfilename;

        if (!is_dir($upload_config['upload_path']))
            mkdir($upload_config['upload_path'], 0777, TRUE);

           $this->upload->initialize($upload_config);


          if (!$this->upload->do_upload()) {
              //upload failed
              //TODO:throw th
              return (array('error' => $this->upload->display_errors('<span>', '</span>')));

          } else {
              // upload success
              $upload_data_photos[] = $this->upload->data();
          }
                      
           //this data is need for later database save on media to store relationships
            $upload_result =  $upload_data_photos;

            //var_dump($upload_result);
            return $upload_result ;

     }

     public function get_all_videos($profile_id) {

       $videos = $this->_order_by_created_date()->as_object()->get_many_by(array('profile_id' =>$profile_id));
     //  var_dump($videos->id);exit;
       $new_arragment_photos_result = array();

       //build path for each images
      if( count($videos)>1 ){
            
           foreach ($videos as $key => $video) {
              
                $new_arragment_photos_result[$key] = new stdClass();
                $new_arragment_photos_result[$key]->id=$video->id;
                $new_arragment_photos_result[$key]->profile_id=$video->profile_id;
                $new_arragment_photos_result[$key]->file_name=$video->file_name;
                $new_arragment_photos_result[$key]->created_date=$video->created_date;
                $new_arragment_photos_result[$key]->title=$video->title;

                $new_arragment_photos_result[$key]->video_path = base_url().'/uploads/profile/'.$video->profile_id.'/videos/'.$video->file_name ; //get only the first product image      

         }

      }  else if(count($videos)==1) {

                  $new_arragment_photos_result = new stdClass();
                  $new_arragment_photos_result->id=$videos[0]->id;
                  $new_arragment_photos_result->profile_id=$videos[0]->profile_id;
                  $new_arragment_photos_result->file_name=$videos[0]->file_name;
                  $new_arragment_photos_result->created_date=$videos[0]->created_date;
                  $new_arragment_photos_result->description=$videos[0]->description;
                  $new_arragment_photos_result->video_path = base_url().'uploads/profile/'.$videos[0]->profile_id.'/videos/'.$videos[0]->file_name ; //get only the first product image       
      }  

       // var_dump($new_arragment_photos_result);exit;
       return  $new_arragment_photos_result;

     }


    public function get_all_videos_for_admin() {

            $videos = $this->_order_by_created_date()->as_object()->get_all();
            //  var_dump($videos->id);exit;
            $new_arragment_photos_result = array();

            //build path for each images
            if( count($videos)>1 ){

                foreach ($videos as $key => $video) {

                    $new_arragment_photos_result[$key] = new stdClass();
                    $new_arragment_photos_result[$key]->id=$video->id;
                    $new_arragment_photos_result[$key]->profile_id=$video->profile_id;
                    $new_arragment_photos_result[$key]->file_name=$video->file_name;
                    $new_arragment_photos_result[$key]->created_date=$video->created_date;
                    $new_arragment_photos_result[$key]->title=$video->title;

                    $new_arragment_photos_result[$key]->video_path = base_url().'/uploads/profile/'.$video->profile_id.'/videos/'.$video->file_name ; //get only the first product image

                }

            }  else if(count($videos)==1) {

                $new_arragment_photos_result = new stdClass();
                $new_arragment_photos_result->id=$videos[0]->id;
                $new_arragment_photos_result->profile_id=$videos[0]->profile_id;
                $new_arragment_photos_result->file_name=$videos[0]->file_name;
                $new_arragment_photos_result->created_date=$videos[0]->created_date;
                $new_arragment_photos_result->description=$videos[0]->description;
                $new_arragment_photos_result->video_path = base_url().'uploads/profile/'.$videos[0]->profile_id.'/videos/'.$videos[0]->file_name ; //get only the first product image
            }

            // var_dump($new_arragment_photos_result);exit;
            return  $new_arragment_photos_result;

    }
	
}
/* End of 
file cart_model.php */
/* Location: ./application/models/cart_model.php */
