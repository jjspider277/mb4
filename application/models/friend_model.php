<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 07/20/15
 * Time: 5:02 PM
 *  Craig Robinson : One World Solutions LLC
 */

class Friend_model extends My_Model {

   /* abstract class MessageType
    {
        const INBOX = INBOX;
        const DRAFT = DRAFT;
        const SENT = SENT;
        // etc.
    }

    abstract class MessageStatus
    {
        const UNREAD = UNREAD;
        const SEEN = SEEN;
        const TRASH = TRASH;
        // etc.
    }
*/


    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   

   public $has_many = array( 'senders' => array( 'primary_key' => 'sender_profile_id','model'=>"profile_model"),                            
                              'recivers' => array( 'primary_key' => 'reciver_profile_id','model'=>"profile_model"),
                              'notifications' => array( 'primary_key' => 'friend_id','model'=>"notification_model"),    
                             
    );


    
   public $before_create = array('timestamps');
   public $before_update= array('change_update_date');
   public $before_delete= array('change_update_date');


    public function _order_by_created_date() {
        $this->db->order_by('friends.created_date', 'desc');
        return $this;
    }

     public function _order_by_updated_date() {
        $this->db->order_by('friends.updated_date', 'desc');
        return $this;
    }
 


    protected function change_update_date($freinds_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $freinds_row['updated_date'] = date('Y-m-d H:i:s');
        return  $freinds_row;
    }

   protected function timestamps($freinds_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $freinds_row['created_date'] = date('Y-m-d H:i:s');
        $freinds_row['request_date'] = date('Y-m-d H:i:s');
        $freinds_row['updated_date']= date('Y-m-d H:i:s');
        $freinds_row['created_by']=   'USER';
        $freinds_row['friend_status']= 'REQUEST';
        $freinds_row['seen_status']= 'UNREAD';
        return  $freinds_row;
    }


   public function send_friend_request($sender_profile_id,$reciever_profile_id) {
    //TODO:save_message
    $saved_request_id = $this->insert(
        array(
             'sender_profile_id' =>$sender_profile_id ,
             'reciever_profile_id' =>$reciever_profile_id ,
         )
      );

   

     if (!$saved_request_id) {
      return -1;
     }
      
     return  $saved_request_id;
   
 }

 //cancel

  public function cancle_friend_request($sender_profile_id,$reciever_profile_id) {
    //TODO:save_message
    $result = $this->delete_by(
        array(
             'sender_profile_id' =>$sender_profile_id ,
             'reciever_profile_id' =>$reciever_profile_id ,
         )
      );

     if (!$result ) {
      return -1;
     }
      
     return $result ;
 
 }


  public function get_friend_requests($reciever_profile_id) {
    
      $this->db->select('* , friends.id as friend_id,friends.request_date as request_date, friends.created_date as request_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id,');
      $this->db->from('friends');
      $this->db->join('profiles', 'profiles.id = friends.sender_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('friends.reciever_profile_id', $reciever_profile_id);
      $this->db->where('friends.friend_status','REQUEST');
      $this->db->where('friends.seen_status','UNREAD');
      $this->db->group_by('friends.id');
      //$this->db->limit($per_page, $page);
      $this->_order_by_created_date();
      $query = $this->db->get();
      $all_inbox_messages = $query->result();     

      $friends_profile_media_object = array();

      foreach ($all_inbox_messages as $key => $friend_object) {

        $friends_profile_media_object[$key] = new stdClass(); 
        $friends_profile_media_object[$key]->friend_id = $friend_object->friend_id;
        $friends_profile_media_object[$key]->profile_id = $friend_object->profile_id;
        $friends_profile_media_object[$key]->city = $friend_object->city;
        $friends_profile_media_object[$key]->state = $friend_object->state;
        $friends_profile_media_object[$key]->full_name = ucfirst($friend_object->fname); 
        $friends_profile_media_object[$key]->date = date_format(date_create($friend_object->request_created_date),'l jS F Y');
        $friends_profile_media_object[$key]->time = date_format(date_create($friend_object->request_created_date),'G:ia');


         //load profile imagess
          if(!empty($friend_object->file_name!="")) {
             $friends_profile_media_object[$key]->media = new stdClass();
             $friends_profile_media_object[$key]->media->file_name = '/uploads/profile/'.$friend_object->profile_id .'/avatar/'. $friend_object->file_name;
          } 
          else {

             $friends_profile_media_object[$key]->media = new stdClass();
             $friends_profile_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }

      }

      return $friends_profile_media_object;
  }  

   public function get_my_friends($profile_id) {

    //$friends_list = $this->friends->get_many_by(array('friend_status' =>'ACCEPT','reciever_profile_id'=>$profile_id));
    
     $friends_list = array();
    
     //serch a friendship if created based on the profile id ib receuvcers side other wise do sender side below query
      //this helps when loading a difffernt profiles which are not always a friend reques accepters or recievers
      //since one their request accepted by the receiver then they should know who really theier friends are

      $this->db->select('* , friends.id as friend_id,friends.request_date as request_date, friends.created_date as request_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id,');
      $this->db->from('friends');
      $this->db->join('profiles', 'profiles.id = friends.sender_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('friends.reciever_profile_id',$profile_id);
      $this->db->where('friends.friend_status','ACCEPT');
      $this->db->group_by('friends.id');
      $this->_order_by_created_date();
      $query = $this->db->get();
      $friends_list = $query->result();   

      //if match by reciever profile id dosent work search if there has been a 
      //freind relationship by senders side 
      //echo "freind lisy";
   

      if( empty($friends_list) ) {

        $this->db->select('* , friends.id as friend_id,friends.request_date as request_date, friends.created_date as request_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id,');
        $this->db->from('friends');
        $this->db->join('profiles ', 'profiles.id = friends.reciever_profile_id');
        $this->db->join('medias', 'medias.profile_id = profiles.id');
        $this->db->where('friends.sender_profile_id',$profile_id);
        $this->db->where('friends.friend_status','ACCEPT');
        $this->db->group_by('friends.id');
        $this->_order_by_created_date();
        $query = $this->db->get();
        $friends_list = $query->result();  


      }

      //build the object for view

      $friends_profile_media_object = array();

       foreach ($friends_list as $key => $friend_object) {

        $friends_profile_media_object[$key] = new stdClass(); 
        $friends_profile_media_object[$key]->friend_id = $friend_object->friend_id;
        $friends_profile_media_object[$key]->profile_id = $friend_object->profile_id;
        $friends_profile_media_object[$key]->city = $friend_object->city;
        $friends_profile_media_object[$key]->state = $friend_object->state;
        $friends_profile_media_object[$key]->full_name = ucfirst($friend_object->fname); 
        $friends_profile_media_object[$key]->date = date_format(date_create($friend_object->request_created_date),'l jS F Y');
        $friends_profile_media_object[$key]->time = date_format(date_create($friend_object->request_created_date),'G:ia');
        $friends_profile_media_object[$key]->profile_rating = $friend_object->p_avg_rating;
        $friends_profile_media_object[$key]->friends_of_friends_count= $this->count_friends($friend_object->profile_id);
        $friends_profile_media_object[$key]->product_count_of_friends= $this->count_products($friend_object->profile_id);

         //load profile imagess
          if(!empty($friend_object->file_name!="")) {
             $friends_profile_media_object[$key]->media = new stdClass();
             $friends_profile_media_object[$key]->media->file_name = '/uploads/profile/'.$friend_object->profile_id .'/avatar/'. $friend_object->file_name;
          } 
          else {

             $friends_profile_media_object[$key]->media = new stdClass();
             $friends_profile_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }

      }
    
      return $friends_profile_media_object;
      
   }


    public function count_friends($profile_id) {

    
       $query = $this->db->query("(select count(id) as num_friends 
                           from friends where friend_status='ACCEPT' 
                           and 
                           (reciever_profile_id=$profile_id or sender_profile_id=$profile_id));");
       $result =  $query->result();
        
        if($result[0]->num_friends > 0 ) 
          return $result[0]->num_friends;
        else 
          return 0;
       //TODO:
    }
    public function count_products($profile_id) {


        $query = $this->db->query("(select count(id) as num_products from products where profile_id=$profile_id                            
                          );");
        $result =  $query->result();

        if($result[0]->num_products > 0 )
            return $result[0]->num_products;
        else
            return 0;
        //TODO:
    }


    function update_by($where = array(),$data=array())
  {
        $this->db->where($where);
        $query = $this->db->update($this->_table,$data);
        return $this->db->get($this->_table)->row()->id;
  }


  
} 