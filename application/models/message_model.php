<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 07/20/15
 * Time: 5:02 PM
 *  Craig Robinson : One World Solutions LLC
 */

class Message_model extends My_Model {

   /* abstract class MessageType
    {
        const INBOX = INBOX;
        const DRAFT = DRAFT;
        const SENT = SENT;
        // etc.
    }

    abstract class MessageStatus
    {
        const UNREAD = UNREAD;
        const SEEN = SEEN;
        const TRASH = TRASH;
        // etc.
    }
*/


    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   

   public $has_many = array(
                              'senders' => array( 'primary_key' => 'sender_profile_id','model'=>"profile_model"),                            
                              'recivers' => array( 'primary_key' => 'reciver_profile_id','model'=>"profile_model"),
                              'notifications' => array( 'primary_key' => 'message_id','model'=>"notification_model"),                                                   
    );
    


   public $before_create = array( 'timestamps' );
   public $before_update= array( 'change_update_date' );
   public $before_delete= array( 'change_update_date' );


   public function _order_by_created_date() {
        $this->db->order_by('messages.created_date', 'desc');
        return $this;
    }

     public function _order_by_updated_date() {
        $this->db->order_by('messages.updated_date', 'desc');
        return $this;
    }
 


    protected function change_update_date($message_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $message_row['updated_date'] = date('Y-m-d H:i:s');
        return  $message_row;
    }

   protected function timestamps($message_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $message_row['created_date'] = date('Y-m-d H:i:s');
        $message_row['updated_date']= date('Y-m-d H:i:s');
        $message_row['message_status']= 'UNREAD';
        return  $message_row;
    }


   public function send_insert_message($data) {
    //TODO:save_message


    $saved_message_id = $this->insert(
        array(
             'sender_profile_id' =>$data['sender_profile_id'] ,
             'reciever_profile_id' =>$data['reciever_profile_id'] ,
             'subject' =>$data['subject'] ,
             'message' =>$data['message_text'] ,
             'message_type'=>'SENT',
         )
      );

   

     if (!$saved_message_id) {
      return -1;
     }
      
     return  $saved_message_id;
   
 }


  public function get_inbox_messages($reciever_profile_id,$per_page, $page) {

      $this->db->select('* , messages.id as message_id, messages.created_date as message_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id , messages.id as message_id');
      $this->db->from('messages');
      $this->db->join('profiles', 'profiles.id = messages.sender_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('messages.reciever_profile_id', $reciever_profile_id);
      $this->db->where('messages.message_type', 'SENT');
      $this->db->where('messages.message_status', 'UNREAD');
      $this->db->where('medias.type', 'profile_image');
      $this->db->group_by('messages.id');
     //$this->db->limit($per_page, $page);
      $this->_order_by_created_date();
      $query = $this->db->get();
      $all_inbox_messages = $query->result();
     // var_dump($all_inbox_messages);

      $profile_message_media_object = array();

      foreach ($all_inbox_messages as $key => $messages_object) {
        //# code...
        $profile_message_media_object[$key] = new stdClass(); 
        $profile_message_media_object[$key]->id = $messages_object->message_id;
        $profile_message_media_object[$key]->profile_id = $messages_object->profile_id;
        $profile_message_media_object[$key]->city = $messages_object->city;
        $profile_message_media_object[$key]->state = $messages_object->state;
        $profile_message_media_object[$key]->full_name = ucfirst($messages_object->fname)." ".ucfirst($messages_object->lname);
        $profile_message_media_object[$key]->subject = ucfirst($messages_object->subject);
        $profile_message_media_object[$key]->message_text = $messages_object->message;
        $profile_message_media_object[$key]->date = date_format(date_create($messages_object->message_created_date),'l jS F Y');
        $profile_message_media_object[$key]->time = date_format(date_create($messages_object->message_created_date),'G:ia');


       //dump($messages_object);exit;

         //load profile imagess
          if(!empty($messages_object->file_name!="")) {
             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/profile/'.$messages_object->profile_id .'/avatar/'. $messages_object->file_name;
          } 
          else {

             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }



      }
     //var_dump($profile_message_media_object);exit;
      return $profile_message_media_object;
    


  }  

  public function get_sent_messages($sender_profile_id) {

  $this->db->select('* ,  messages.id as message_id,messages.created_date as message_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id , messages.id as message_id');
      $this->db->from('messages');
      $this->db->join('profiles', 'profiles.id = messages.reciever_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('messages.sender_profile_id', $sender_profile_id);
      $this->db->where('messages.message_type', 'SENT');
      $this->db->where('messages.message_status', 'UNREAD');
      $this->db->where('medias.type', 'profile_image');
      $this->db->group_by('messages.id');
     //$this->db->limit($per_page, $page);
      $this->_order_by_created_date();
      $query = $this->db->get();
      $all_inbox_messages = $query->result();
      //var_dump($all_inbox_messages);exit;

      $profile_message_media_object = array();

      foreach ($all_inbox_messages as $key => $messages_object) {
        //# code...
        $profile_message_media_object[$key] = new stdClass(); 
        $profile_message_media_object[$key]->id = $messages_object->message_id;
        $profile_message_media_object[$key]->profile_id = $messages_object->profile_id;
        $profile_message_media_object[$key]->city = $messages_object->city;
        $profile_message_media_object[$key]->state = $messages_object->state;
        $profile_message_media_object[$key]->full_name = ucfirst($messages_object->fname)." ".ucfirst($messages_object->lname);
        $profile_message_media_object[$key]->subject = ucfirst($messages_object->subject);
        $profile_message_media_object[$key]->message_text = $messages_object->message;
        $profile_message_media_object[$key]->date = date_format(date_create($messages_object->message_created_date),'l jS F Y');
        $profile_message_media_object[$key]->time = date_format(date_create($messages_object->message_created_date),'G:ia');


         //load profile imagess
          if(!empty($messages_object->file_name!="")) {
             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/profile/'.$messages_object->profile_id .'/avatar/'. $messages_object->file_name;
          } 
          else {

             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }



      }
     //var_dump($profile_message_media_object);exit;
      return $profile_message_media_object;

  }

  public function get_trashed_messages($owner_profile_id,$per_page, $page) {

      //var_dump($owner_profile_id)
      $this->db->select('* , messages.id as message_id, messages.updated_date as message_updated_date,messages.created_date as message_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id , messages.id as message_id');
      $this->db->from('messages');
      $this->db->join('profiles', 'profiles.id = messages.sender_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('medias.type', 'profile_image'); 
      $this->db->where("( messages.reciever_profile_id = '$owner_profile_id' OR messages.sender_profile_id = '$owner_profile_id') AND messages.message_status = 'TRASH'");
      $this->db->group_by('messages.id');
      //$this->db->limit($per_page, $page);
      $this->_order_by_updated_date();
      $query = $this->db->get();
      $all_inbox_messages = $query->result();
      //var_dump($all_inbox_messages);exit;

      $profile_message_media_object = array();

      foreach ($all_inbox_messages as $key => $messages_object) {
        //# code...
        $profile_message_media_object[$key] = new stdClass(); 
        $profile_message_media_object[$key]->id = $messages_object->message_id;
        $profile_message_media_object[$key]->profile_id = $messages_object->profile_id;
        $profile_message_media_object[$key]->city = $messages_object->city;
        $profile_message_media_object[$key]->state = $messages_object->state;
        $profile_message_media_object[$key]->full_name = ucfirst($messages_object->fname)." ".ucfirst($messages_object->lname);
        $profile_message_media_object[$key]->subject = ucfirst($messages_object->subject);
        $profile_message_media_object[$key]->message_text = $messages_object->message;
        $profile_message_media_object[$key]->date = date_format(date_create($messages_object->message_updated_date),'l jS F Y');
        $profile_message_media_object[$key]->time = date_format(date_create($messages_object->message_updated_date),'G:ia');


         //load profile imagess
          if(!empty($messages_object->file_name!="")) {
             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/profile/'.$messages_object->profile_id .'/avatar/'. $messages_object->file_name;
          }
          else {

             $profile_message_media_object[$key]->media = new stdClass();
             $profile_message_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }



      }
     //var_dump($profile_message_media_object);exit;
      return $profile_message_media_object;

  }

    public function save_draft_message($data) {
        //TODO:save_message


        $saved_message_id = $this->insert(
            array(
                'sender_profile_id' =>$data['sender_profile_id'] ,
                'reciever_profile_id' =>$data['reciever_profile_id'] ,
                'subject' =>$data['subject'] ,
                'message' =>$data['message'] ,
                'message_type'=>'DRAFT_PENDING',
                'message_status'=>'UNREAD',
         )
      );

        if (!$saved_message_id) {
         return -1;
     }

     return  $saved_message_id;

 }


    public function update_draft_message($data) {

        $update_data = array(
            'reciever_profile_id' => $data['reciever_profile_id'],
            'sender_profile_id' => $data['sender_profile_id'],
            'subject' => $data['subject'],
            'message' => $data['message'],
        );

        $result = $this->update($data['last_saved_message_id'],$update_data);
        return $result;

    }

  public function get_draft_messages($sender_profile_id) {

      $this->db->select('* , messages.id as message_id, messages.created_date as message_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id , messages.id as message_id');
      $this->db->from('messages');
      $this->db->join('profiles', 'profiles.id = messages.sender_profile_id');
      $this->db->join('medias', 'medias.profile_id = profiles.id');
      $this->db->where('messages.sender_profile_id', $sender_profile_id);
      $this->db->where('messages.message_type', 'DRAFT_PENDING');
      $this->db->where('messages.message_status', 'UNREAD');
      $this->db->where('medias.type', 'profile_image');
      $this->db->group_by('messages.id');
      //$this->db->limit($per_page, $page);
      $this->_order_by_created_date();
      $query = $this->db->get();
      $all_inbox_messages = $query->result();
      // var_dump($all_inbox_messages);

      $profile_message_media_object = array();

      foreach ($all_inbox_messages as $key => $messages_object) {
          //# code...
          $profile_message_media_object[$key] = new stdClass();
          $profile_message_media_object[$key]->id = $messages_object->message_id;
          $profile_message_media_object[$key]->profile_id = $messages_object->profile_id;
          $profile_message_media_object[$key]->city = $messages_object->city;
          $profile_message_media_object[$key]->state = $messages_object->state;
          $profile_message_media_object[$key]->full_name = ucfirst($messages_object->fname)." ".ucfirst($messages_object->lname);
          $profile_message_media_object[$key]->subject = ucfirst($messages_object->subject);
          $profile_message_media_object[$key]->message_text = $messages_object->message;
          $profile_message_media_object[$key]->date = date_format(date_create($messages_object->message_created_date),'l jS F Y');
          $profile_message_media_object[$key]->time = date_format(date_create($messages_object->message_created_date),'G:ia');


          //dump($messages_object);exit;

          //load profile imagess
          if(!empty($messages_object->file_name!="")) {
              $profile_message_media_object[$key]->media = new stdClass();
              $profile_message_media_object[$key]->media->file_name = '/uploads/profile/'.$messages_object->profile_id .'/avatar/'. $messages_object->file_name;
          }
          else {

              $profile_message_media_object[$key]->media = new stdClass();
              $profile_message_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
          }



      }
      //var_dump($profile_message_media_object);exit;
      return $profile_message_media_object;

  }


    public function get_important_messages($sender_profile_id) {

        $this->db->select('* , messages.id as message_id, messages.created_date as message_created_date , profiles.avg_rating as p_avg_rating,medias.id as media_id,profiles.id as profile_id , messages.id as message_id');
        $this->db->from('messages');
        $this->db->join('profiles', 'profiles.id = messages.sender_profile_id');
        $this->db->join('medias', 'medias.profile_id = profiles.id');
        $this->db->where('messages.sender_profile_id', $sender_profile_id);
        $this->db->where('messages.important', true); //1
        $this->db->where('messages.message_status', 'UNREAD');
        $this->db->where('medias.type', 'profile_image');
        $this->db->group_by('messages.id');
        //$this->db->limit($per_page, $page);
        $this->_order_by_created_date();
        $query = $this->db->get();
        $all_inbox_messages = $query->result();
        // var_dump($all_inbox_messages);

        $profile_message_media_object = array();

        foreach ($all_inbox_messages as $key => $messages_object) {
            //# code...
            $profile_message_media_object[$key] = new stdClass();
            $profile_message_media_object[$key]->id = $messages_object->message_id;
            $profile_message_media_object[$key]->profile_id = $messages_object->profile_id;
            $profile_message_media_object[$key]->city = $messages_object->city;
            $profile_message_media_object[$key]->state = $messages_object->state;
            $profile_message_media_object[$key]->full_name = ucfirst($messages_object->fname)." ".ucfirst($messages_object->lname);
            $profile_message_media_object[$key]->subject = ucfirst($messages_object->subject);
            $profile_message_media_object[$key]->message_text = $messages_object->message;
            $profile_message_media_object[$key]->date = date_format(date_create($messages_object->message_created_date),'l jS F Y');
            $profile_message_media_object[$key]->time = date_format(date_create($messages_object->message_created_date),'G:ia');


            //dump($messages_object);exit;

            //load profile imagess
            if(!empty($messages_object->file_name!="")) {
                $profile_message_media_object[$key]->media = new stdClass();
                $profile_message_media_object[$key]->media->file_name = '/uploads/profile/'.$messages_object->profile_id .'/avatar/'. $messages_object->file_name;
            }
            else {

                $profile_message_media_object[$key]->media = new stdClass();
                $profile_message_media_object[$key]->media->file_name = '/uploads/no-photo.jpg';
            }



        }
        //var_dump($profile_message_media_object);exit;
        return $profile_message_media_object;

    }



}