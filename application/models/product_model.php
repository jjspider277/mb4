<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */

class Product_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
    }

    public $before_create = array( 'timestamps' );

    public $primary_key = 'id';

    public $belongs_to = array( 'profile' => array('primary_key' => 'profile_id', 'model' => 'profile_model' ),
        'store' => array('primary_key' => 'store_id', 'model' => 'store_model'));


    public $has_many = array(
        'comments' => array( 'primary_key' => 'parent_post_id' ),
        'categories' => array( 'primary_key' => 'catag' ),
        'medias' => array( 'primary_key' => 'product_id','model'=>"media_model"),
        'ratings' => array( 'primary_key' => 'product_id' ,'model'=>"rating_model"),
        'likes' => array( 'primary_key' => 'product_id' ,'model'=>"like_model"),

    );


    public $validate = array(
        //rules when nessary

    );

    protected function timestamps($product_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $product_row['created_date'] = date('Y-m-d H:i:s');
        return  $product_row;
    }


    public function _order_by_created_date() {
        $this->db->order_by('products.created_date', 'desc');
        return $this;
    }



    /**
     * @param $post
     * @param $profile_id
     * @param $store_id
     * @return bool
     */
    public function add_lisiting($post,$profile_id,$store_id)
    {

        $product_name = $post['product_name'];
        $product_description = $post['product_description'];
        $category = $post['categories'];
        $variation = isset( $post['variation'])? $post['variation'] : '';
        //$sub_variation = isset( $post['sub_variation'] )? $post['sub_variation'] : '';
        $quantity = $post['quantity'];
        $price = $post['price'];
        $sprice = $post['sprice'];
        $color=$post['colors'];
        $size = $post['sizes'];
        $shipping_method=$post['shipping_method'];
        $shipping_price=$post['shipping_price'];
        $product_details ="";

        if(isset($post['product_details'])) {
            $product_details = $post['product_details'];
        }

        $insert_data = array(
            'name' =>$product_name ,
            'desc' =>$product_description ,
            'price' =>$price ,
            'sprice' =>$sprice ,
            'category'=>$category,
            'variation'=>$variation, //renamed to sub category :), :=>} , $;+]
            'shipping_method'=>$shipping_method,
            'shipping_price'=>$shipping_price,
            'profile_id' => $profile_id,
            'quantity'=>$quantity,
            'store_id'=>intval($store_id),
            'product_details'=>$product_details,
            'created_date'=>date('Y-m-d'),
            'color'=>$color,
            'size'=>$size,
        );

        $product_id = $this->insert($insert_data);

        return $product_id;

    }

    /**
     * @param $post
     * @param $profile_id
     * @return bool
     */
    public function update_lisiting($post,$product_id,$profile_id,$store_id)
    {
        $product_name = $post['product_name'];
        $product_description = $post['product_description'];
        $category = $post['categories'];
        $variation = isset( $post['variation'])? $post['variation'] : '';
        //$sub_variation = isset( $post['sub_variation'] )? $post['sub_variation'] : '';
        $quantity = $post['quantity'];
        $price = isset($post['price'])?$post['price']:0;
        $sprice = $post['sprice'];
        $color=$post['colors'];
        $size = $post['sizes'];
        $product_details= isset($post['product_details']) ?$post['product_details'] : "" ;
        $shipping_price = $post['shipping_price'];
        $shipping_method = $post['shipping_method'];

        $update_data = array(
            'name' =>$product_name ,
            'desc' =>$product_description ,
            'price' =>$price ,
            'sprice' =>$sprice ,
            'shipping_price' =>$shipping_price ,
            'shipping_method'=>$shipping_method,
            'category'=>$category,
            'variation'=>$variation, //renamed to sub category :), :=>} , $;+]
            //'sub_variation'=>$sub_variation,
            'quantity'=>$quantity,
            'product_details'=>$product_details,
            'created_date'=>date('Y-m-d'),
            'color'=>$color,
            'size'=>$size,
        );

        $where_update_data = array(
            'store_id'=>$store_id,
            'profile_id' =>$profile_id,
            'id'=>$product_id,
        );

        $where = $where_update_data;
        $data =  $update_data;

        $updated_product_id = $this->update_by($where,$data);

        return $updated_product_id;

    }

    public function display_all_product($per_page, $page) {


        //The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->with('profile')->order_by('rand()')->get_all();

        /*
         $this->db->select('* , products.avg_rating as p_avg_rating,products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
         $this->db->from('products');
         $this->db->join('medias', 'medias.product_id = products.id');
         $this->db->join('profiles', 'profiles.id = products.profile_id');
         $this->db->where('profiles.is_profile_verified', intval(true));
         $this->db->group_by('profiles.id');
         $this->db->order_by ('RAND()');
         $this->db->limit($per_page, $page);
         $query = $this->db->get();
         $all_products = $query->result();

         var_dump($all_products); */

        //var_dump($all_products[0]->medias[0]->file_name);exit;
        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            // dump($product_object);exit;
            $product_details[$key]['product_id'] = $product_object->id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->desc;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['sprice'] = $product_object->sprice;
            $product_details[$key]['category'] = $product_object->category;

            $product_details[$key]['product_details'] = $product_object->product_details;

            //TODO:fetch already calculated avg_rating

            $product_details[$key]['product_rating'] = !isset($product_object->avg_rating)?0:$product_object->avg_rating;
            $product_details[$key]['seller_rating'] = !isset($product_object->profile->avg_rating)?0:$product_object->profile->avg_rating;

            $product_details[$key]['profile_id'] = $product_object->profile->id;
            $this->load->model('media_model','medias');
            $medias = $this->medias->get_by(array('product_id'=>$product_object->id,'file_name <> "image-upload.jpg"'));
            //dump($medias);exit;
            if(count($medias) > 0) {
                $product_details[$key]['image'] = base_url() . 'uploads/profile/' . $product_object->profile_id . '/products/' . $medias->file_name; //get only the first product image
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->profile->fname));
        }

//        /dump($product_details);exit;


        return ((array)$product_details);

    }

    public function display_auction_products($profile_id,$per_page, $page) {
        date_default_timezone_set('America/Los_Angeles');
        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */

        $this->db->select('* , products.avg_rating as p_avg_rating,

        auctions.start_date as auctions_starting_date,
        auctions.start_time as auction_starting_time,
        auctions.end_date as auctions_end_date,
        auctions.end_time as auctions_end_time,
        auctions.bid_price as auctions_bid_price,
        products.desc as pdescription,
        bids.id as bids,
        auctions.id as auction_id,
        medias.id as media_id,products.id as product_id ,
        profiles.id as profile_id');

        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('auctions', 'auctions.product_id = products.id');
        $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->order_by('rand()');
        $now_date = date('Y-m-d H:i:s');
        //var_dump($profile_id);die;
        if(null !== $profile_id){
            $this->db->where('products.profile_id !=', $profile_id);
            $this->db->where('auctions.start_date <=', $now_date);
            $this->db->where('auctions.end_date >=', $now_date);
        } else {
            $this->db->where('auctions.start_date <=', $now_date);
            $this->db->where('auctions.end_date >=', $now_date);
            //$this->db->where('auctions.status', true);
        }


        $this->db->group_by('products.id, auctions.id');
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        //var_dump($page);die;

        $all_products = $query->result();
        //var_dump($all_bids);die;
        // var_dump($all_products);die;
        $bid_details = array();


        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            $product_details[$key]['product_id'] = $product_object->product_id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->pdescription;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['bids']= $product_object->bids;
            $product_details[$key]['auctions_bid_price'] = $product_object->auctions_bid_price;


            $product_details[$key]['auction_id'] = $product_object->auction_id;
            $this->db->from('bids');

            $this->db->where('auction_id', $product_details[$key]['auction_id'] );
            $query2 = $this->db->get();


            $bid_counter = count($query2->result());
            $product_details[$key]['bid_counter'] = $bid_counter;
            $product_details[$key]['auctions_end_date'] = $product_object->auctions_end_date;
            $product_details[$key]['auctions_starting_date'] = $product_object->auctions_starting_date;
            $product_details[$key]['sprice'] = $product_object->sprice;
            $product_details[$key]['product_details'] = $product_object->product_details;

            //TODO:fetch already calculated avg_rating

            $product_details[$key]['product_rating'] = !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating;
            $product_details[$key]['seller_rating'] = !isset($product_object->avg_rating)?0:$product_object->avg_rating;

            $product_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }
        //var_dump($product_details);die;

        return ((array)$product_details);

    }
    public function display_other_auction_products($profile_id,$product_id,$per_page, $page) {

        date_default_timezone_set('America/Los_Angeles');
        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */

        $this->db->select('* , products.avg_rating as p_avg_rating,
        auctions.start_date as auctions_starting_date,
        auctions.start_time as auction_starting_time,
        auctions.end_date as auctions_end_date,
        auctions.end_time as auctions_end_time,
        auctions.bid_price as auctions_bid_price,
        products.desc as pdescription,
        products.category as category,
        count(bids.id) as bids,
        auctions.id as auction_id,
        medias.id as media_id,products.id as product_id ,
        profiles.id as profile_id');

        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('auctions', 'auctions.product_id = products.id');
        $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $now_date = date('Y-m-d H:i:s');
        //var_dump($profile_id);die;
        $this->db->where('products.id !=', $product_id);
        $this->db->where('products.profile_id !=', $profile_id);
        $this->db->where('auctions.start_date <=', $now_date);
        $this->db->where('auctions.end_date >=', $now_date);
        $this->db->where('auctions.status', true);
        $this->db->group_by('products.id, auctions.id');
        $this->db->limit($per_page);
        $query = $this->db->get();


        $all_products = $query->result();
        //var_dump($all_bids);die;
        // var_dump($all_products);die;
        $bid_details = array();


        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            $product_details[$key]['product_id'] = $product_object->product_id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->pdescription;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['bids'] = $product_object->bids;
            $product_details[$key]['category'] = $product_object->category;
            $product_details[$key]['auctions_bid_price'] = $product_object->auctions_bid_price;


            $product_details[$key]['auction_id'] = $product_object->auction_id;
            $this->db->from('bids');

            $this->db->where('auction_id', $product_details[$key]['auction_id'] );
            $query2 = $this->db->get();


            $bid_counter = count($query2->result());
            $product_details[$key]['bid_counter'] = $bid_counter;

            $product_details[$key]['auctions_end_date'] = $product_object->auctions_end_date;
            $product_details[$key]['auctions_starting_date'] = $product_object->auctions_starting_date;
            $product_details[$key]['sprice'] = $product_object->sprice;
            $product_details[$key]['product_details'] = $product_object->product_details;

            //TODO:fetch already calculated avg_rating

            $product_details[$key]['product_rating'] = !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating;
            $product_details[$key]['seller_rating'] = !isset($product_object->avg_rating)?0:$product_object->avg_rating;

            $product_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }
        //var_dump($product_details);die;

        return ((array)$product_details);

    }

    public function get_new_arrival_products($per_page, $page)
    {

        $this->db->select('* , products.avg_rating as p_avg_rating,products.desc as pdescription,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->group_by('profiles.id');
        $this->db->limit($per_page, $page);
        $this->_order_by_created_date();
        $this->db->order_by('RAND()');
        $query = $this->db->get();
        $all_new_arrival_products = $query->result();


        //ToDO:Construct arrray of products with media and p
        //profile details as array of objects and pass to the view simpley

        $new_arrival_product_details = array();

        foreach ($all_new_arrival_products as $key => $product_object) {

            $new_arrival_product_details[$key]['product_id'] = $product_object->product_id;
            $new_arrival_product_details[$key]['name'] = $product_object->name;
            $new_arrival_product_details[$key]['desc'] = $product_object->pdescription;
            $new_arrival_product_details[$key]['price'] = $product_object->price;
            $new_arrival_product_details[$key]['sprice'] = $product_object->sprice;
            $new_arrival_product_details[$key]['shipping_price'] = $product_object->shipping_price;
            $new_arrival_product_details[$key]['product_details'] = $product_object->product_details;

            $new_arrival_product_details[$key]['profile_id'] = $product_object->profile_id;


            //TODO:some calcualtions
            $new_arrival_product_details[$key]['product_rating'] = !isset($product_object->p_avg_rating) ? 0 : $product_object->p_avg_rating;
            $this->load->model('media_model','medias');
            $medias = $this->medias->get_by(array('product_id' => $product_object->id, 'file_name <> "image-upload.jpg"'));

            //TODO:When there are many products on the database this code need to be fixed or properly adujsted
            if ($medias != null) {
                $new_arrival_product_details[$key]['image'] = base_url() . '/uploads/profile/' . $product_object->profile_id . '/products/' . $medias->file_name; //get only the first product image
            }

            $new_arrival_product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }

        return ((array)$new_arrival_product_details);

    }

    /***
     * @param $product_id
     * @param string $return_type
     * @return array|int|stdClass
     */
    public function get_single_product_detail($product_id, $return_type="object") {

        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */

        $this->db->select(
						'* , 
						products.store_id as p_store_id, 
						products.avg_rating as p_avg_rating,
						products.desc as pdescription,
						profiles.id as profile_id,
						products.id as product_id , 
						products.color as color , 
						products.size as size , 
						products.sub_variation as subcategory , 
						products.variation as category , 
						profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where('products.id', $product_id);
        $this->db->group_by('products.id');
        //$this->db->order_by('products.id');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $product_detail = $query->result();
        //$product_detail = $this->with('medias')
        //->get($id);
        // $product_detail = (object)$product_detail;

        if(count($product_detail) >0 ) {
            //var_dump($product_detail);exit;
	
			
            //$product = array();
            $product = new stdClass();
            $product->p_store_id = $product_detail[0]->p_store_id;
            $product->product_id = $product_detail[0]->product_id;
            $product->product_name =  $product_detail[0]->name;
            $product->pdescription =  $product_detail[0]->pdescription;
            $product->profile_id =  $product_detail[0]->profile_id;
            $product->price =  $product_detail[0]->price;
            $product->sprice =  $product_detail[0]->sprice;
            $product->p_avg_rating =  $product_detail[0]->p_avg_rating;
            $product->quantity =  $product_detail[0]->quantity;
            $product->shipping_price =  $product_detail[0]->shipping_price;
            $product->color =  $product_detail[0]->color;
            $product->size =  $product_detail[0]->size;
            $product->category =  $product_detail[0]->variation;
            $product->subcategory =  $product_detail[0]->sub_variation;
            $product->product_details = $product_detail[0]->product_details;
            $product->joined_date = $product_detail[0]->join_date;
			
				// have to get images  here we go...

            //load profile as object

            $product->profile =new stdClass();
            $product->profile->id =  $product_detail[0]->profile_id;
            $product->profile->fname =  $product_detail[0]->fname;
            $product->profile->lname =  $product_detail[0]->lname;
            $product->profile->city =  $product_detail[0]->city;
            $product->profile->state =  $product_detail[0]->state;
            $product->profile->seller_name =  ucfirst($product_detail[0]->fname).' '.ucfirst($product_detail[0]->lname);
            $product->profile->profile_image_id =  $product_detail[0]->profile_image_id;
            $product->profile->avg_rating =  $product_detail[0]->avg_rating;

            //load profile image
            $this->db->select('* ,medias.id as media_id');
            $this->db->from('medias');
            $this->db->where('type','profile_image');
            $this->db->where('medias.profile_id',$product->profile->id);
            $profile_medias = $this->db->get()->result();

            if($profile_medias!=null) $product->profile->profile_image = base_url().'uploads/profile/'.$product->profile->id.'/avatar/'.$profile_medias[0]->file_name ; //get only the first product image
            else $product->profile->profile_image = base_url().'uploads/no-photo.jpg'; //get only the first product image


            //load medias as object

            $this->db->select('*');
            $this->db->from('medias');
            $this->db->where('type','product_image');
            $this->db->where('medias.product_id',$product->product_id);
            $medias = $this->db->get()->result();

			
            foreach ($medias as $key => $product_images) {
				
                $product->medias[$key] = new stdClass();
                $product->medias[$key]->media_id = $product_images->media_id;
                $product->medias[$key]->file_name = $product_images->file_name;
                $product->medias[$key]->full_path = $product_images->full_path;
            }

		
            //TODO:fetch avg calcualtions rate
            //get profile rating
            $product->profile->profile_rating =  !isset($product->profile->avg_rating)?0:$product->profile->avg_rating; ;
            //dump($product);exit;
            // $product_details[$key]['seller_rating'] =  !isset($product->seller_avg_rating)?0:$product->seller_avg_rating; ;

            if(!empty($product->medias) && is_array($product->medias)){

                foreach ( $medias as $key => $media) {

                    if ($media->file_name != 'image-upload.jpg') {

                        $product->image[$key] = base_url('uploads/profile/' .
                            $product->profile_id . '/products/' . $media->file_name);
                    } else {

                        $product->image[$key]  = base_url('assets/images/image-upload.jpg');
                    }

                    //$product->image[$key] = base_url().'uploads/profile/'.$product->profile->id.'/products/'.$media->file_name ; //get only the first product image
                }
            }

            // var_dump($product);die;
            return ($return_type === "array") ? (array) $product: $product;

        }

        return -1;
    }



    public function get_all_seller_product_lisiting ($profile_id,$product_id,$per_page, $page) {

        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */

        $this->db->select('* ,products.avg_rating as p_avg_rating, products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where('profiles.id', $profile_id);
        $this->db->where('medias.file_name <> "image-upload.jpg"');
        if($product_id!=null or $product_id!="")
            $this->db->where('products.id !=', $product_id); //where not equal
        $this->db->group_by('products.id',$product_id);
        $this->db->limit($per_page, $page);
        $query = $this->db->get();
        $all_products = $query->result();


        //var_dump($all_products);exit;

        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            $product_details[$key]['product_id'] = $product_object->product_id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->pdescription;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['sprice'] = $product_object->sprice;
            $product_details[$key]['product_details'] = $product_object->product_details;

            //TODO:some calcualtions
            $product_details[$key]['product_rating'] =  !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating; ;
            //TODO:some calcualtions
            $product_details[$key]['seller_rating'] =  !isset($product_object->seller_avg_rating)?0:$product_object->seller_avg_rating; ;

            $product_details[$key]['profile_id'] = $product_object->profile_id;
            //dump($product_object);exit;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }


        return ((array)$product_details);

    }

    public function display_product_by_category_subcategory($category_id,$per_page, $page) {


       /* if( strcmp($category,$subcategory)==0){
            //if they are the same know that its the category menu item pressed
            $subcategory='#';
        }*/



        $this->load->model('category_model','categories');
        $result= $this->categories->get(array('id'=>$category_id));

        $this->db->select('* ,products.avg_rating as p_avg_rating, products.desc as pdescription,medias.id as media_id,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where("medias.file_name <> 'image-upload.jpg'");
        $this->db->like('products.category',strval($result->category));

        /*if($subcategory!="#")
            $this->db->like('products.variation', isset($subcategory)?ucfirst($subcategory):null);
*/
        $this->db->group_by('profiles.id');
        $this->db->limit($per_page, $page);
        $this->_order_by_created_date();
        $query = $this->db->get();
        $category_variation_products = $query->result();

//        /dump($category_variation_products);

        if($category_variation_products==null && $result->parent_id != null) {

            $parent_result = $this->categories->get(array('id'=>$result->parent_id));
            $parent_category_name  = $parent_result->category;
            $this->db->select('* ,products.avg_rating as p_avg_rating, products.desc as pdescription,products.id as product_id , profiles.id as profile_id');
            $this->db->from('products');
            $this->db->join('profiles', 'profiles.id = products.profile_id');
            $this->db->where('profiles.is_profile_verified', intval(true));
            $this->db->like('products.category',strval($parent_category_name));

            /*if($subcategory!="#")
                $this->db->like('products.variation', isset($subcategory)?ucfirst($subcategory):null);
    */
            $this->db->group_by('profiles.id');
            $this->db->limit($per_page, $page);
            $this->_order_by_created_date();
            $query = $this->db->get();
            $category_variation_products = $query->result();
        }

        //ToDO:Construct arrray of products with media and p
        //profile details as array of objects and pass to the view simpley

        $category_variation_products_details = array();

        foreach ($category_variation_products as $key => $product_object) {

            $category_variation_products_details[$key]['product_id'] = $product_object->product_id;
            $category_variation_products_details[$key]['name'] = $product_object->name;
            $category_variation_products_details[$key]['desc'] = $product_object->pdescription;
            $category_variation_products_details[$key]['price'] = $product_object->price;
            $category_variation_products_details[$key]['sprice'] = $product_object->sprice;
            $category_variation_products_details[$key]['product_details'] = $product_object->product_details;

            $category_variation_products_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:some calcualtions
            $category_variation_products_details[$key]['product_rating'] =  $product_object->p_avg_rating;
            $this->load->model('media_model','medias');
            $medias = $this->medias->get_by(array('product_id'=>$product_object->id,'file_name <> "image-upload.jpg"'));

            //TODO:When there are many products on the database this code need to be fixed or properly adujsted
            if($medias!=null) {
                $category_variation_products_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$medias->file_name ; //get only the first product image
            }

            $category_variation_products_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }

        return ((array)$category_variation_products_details);

    }

    public function search_products($per_page, $page,$query) {
        $this->output->enable_profiler();

        $this->db->select('* , products.avg_rating as p_avg_rating,products.desc as pdescription,medias.id as media_id,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->like('products.name', $query, 'both');
        $this->db->or_like('products.desc', $query, 'both');
        $this->db->group_by('profiles.id');
        $this->db->limit($per_page, $page);
        $this->_order_by_created_date();
        $query = $this->db->get();
        $all_new_arrival_products = $query->result();


        //ToDO:Construct arrray of products with media and p
        //profile details as array of objects and pass to the view simpley

        $new_arrival_product_details = array();

        foreach ($all_new_arrival_products as $key => $product_object) {

            $new_arrival_product_details[$key]['product_id'] = $product_object->product_id;
            $new_arrival_product_details[$key]['name'] = $product_object->name;
            $new_arrival_product_details[$key]['desc'] = $product_object->pdescription;
            $new_arrival_product_details[$key]['price'] = $product_object->price;
            $new_arrival_product_details[$key]['sprice'] = $product_object->sprice;
            $new_arrival_product_details[$key]['shipping_price'] =  $product_object->shipping_price;
            $new_arrival_product_details[$key]['product_details'] = $product_object->product_details;

            $new_arrival_product_details[$key]['profile_id'] = $product_object->profile_id;


            //TODO:some calcualtions
            $new_arrival_product_details[$key]['product_rating'] = !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating;

            //TODO:When there are many products on the database this code need to be fixed or properly adujsted
            if(!empty($product_object->media_id)) {
                $new_arrival_product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            }

            else  {$new_arrival_product_details[$key]['image'] ='';}

            $new_arrival_product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }

        return ((array)$new_arrival_product_details);

    }

    //get all product lisitings for admin

    public function get_all_product_listing_for_admin($per_page,$page) {

        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */

        $this->db->select('* ,products.created_date as product_added_date, products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->group_by('products.id');
        $this->db->order_by('product_added_date');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $all_products = $query->result();


        //var_dump($all_products);exit;

        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            $product_details[$key]['product_id'] = $product_object->product_id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->pdescription;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['added_date'] = $product_object->product_added_date;

            $product_details[$key]['product_details'] = $product_object->product_details;


            $product_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }

        //dump((array)$product_details);exit;

        return ((array)$product_details);
    }

    public function get_all_product_listing($profile_id) {
	
        /*
        The old way of accessing the products but generates lot of query
        $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
        */
		
		$this->db->select('
	stores.id as store_id,
	stores.store_name as store_name,
    products.id as product_id,
    products.name as name,
    products.desc as pdescription,
    products.price as price,
	products.product_details as product_details,
    products.category as product_category,
    products.quantity as product_quantity,
	products.created_date as product_added_date,
	products.color as product_color,
	products.size as product_size,
	products.variation as product_variation,
	products.sub_variation as product_sub_variation,
	products.shipping_price as product_shipping_price,
	products.shipping_method as product_shipping_method,
	products.avg_rating as product_avg_rating,
	products.lead_time_days as lead_time_days,
    media.id as media_id,
    media.file_name as file_name,
    media.full_path as media_path,
    profiles.id as profile_id,
	profiles.fname as fname');
	$this->db->from('stores');
	$this->db->join('products','products.store_id = stores.id');
	$this->db->join('profiles','profiles.id = stores.owner_profile_id');
    $this->db->join('(SELECT *, MIN(medias.product_id) FROM medias GROUP BY medias.product_id) media','media.product_id = products.id');
    $this->db->where('profiles.id', $profile_id);
	$this->db->where('profiles.is_profile_verified',1);
	$this->db->order_by('product_added_date');
    
		/*$this->db->select('* ,products.created_date as product_added_date, products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
        $this->db->from('products');
        $this->db->join('medias', 'medias.product_id = products.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where('profiles.id', $profile_id);
        $this->db->group_by('products.id');
        $this->db->order_by('product_added_date');
		*/
		
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $all_products = $query->result();


        //var_dump($all_products);exit;

        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley

        $product_details = array();

        foreach ($all_products as $key => $product_object) {

            $product_details[$key]['product_id'] = $product_object->product_id;
            $product_details[$key]['name'] = $product_object->name;
            $product_details[$key]['desc'] = $product_object->pdescription;
            $product_details[$key]['price'] = $product_object->price;
            $product_details[$key]['added_date'] = $product_object->product_added_date;
            $product_details[$key]['category'] = $product_object->product_category;
            $product_details[$key]['color'] = $product_object->product_color;
            $product_details[$key]['size'] = $product_object->product_size;
            $product_details[$key]['quantity'] = $product_object->product_quantity;
            $product_details[$key]['variation'] = $product_object->product_variation;
            $product_details[$key]['sub_variation'] = $product_object->product_sub_variation;
            $product_details[$key]['shipping_price'] = $product_object->product_shipping_price;
            $product_details[$key]['shipping_method'] = $product_object->product_shipping_method;
            $product_details[$key]['avg_rating'] = $product_object->product_avg_rating;
            $product_details[$key]['lead_time_days'] = $product_object->lead_time_days;

            $product_details[$key]['product_details'] = $product_object->product_details;


            $product_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image   *** done above... GK 7/24/16
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
        }

        //dump((array)$product_details);exit;

        return ((array)$product_details);
    }

    public function count_listing_for_seller($profile_id) {

        $product_count =   $this->count_by(array('profile_id'=>$profile_id));
        return $product_count;

        /* $this->db->select('count(prduct_id) as product_counts');
         $this->db->from('products');
         $this*/

    }

    //get all stores for admin
    public function getProductDataForEdit($product_id) {


        $this->db->select('*,products.id as product_id,products.desc as p_desc , stores.id as store_id,stores.desc as sdescription');
        $this->db->from('products');
        $this->db->join('stores', 'stores.id = products.store_id');
        $this->db->group_by('products.id');
        $this->db->where('products.id',$product_id);
        $this->db->order_by('products.created_date');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();

        $product_admin_data = $query->result();

        //dump($product_admin_data);exit;

        $product_admin_data_new =  array();

        if(!empty($product_admin_data) ) {

            foreach ($product_admin_data as $key => $product_result_object) {
                //append product_data_for_edit_store

                $product_admin_data_new[$key] = new stdClass();
                $product_admin_data_new[$key]->store = new stdClass();
                $product_admin_data_new[$key]->product = new stdClass();
                $product_admin_data_new[$key]->store->store_id = $product_result_object->store_id;
                $product_admin_data_new[$key]->store->name = $product_result_object->store_name;
                $product_admin_data_new[$key]->store->desc = $product_result_object->sdescription;

                $this->load->model('media_model', 'medias_products');
                $product_medias = $this->medias_products->get_many_by(array('type' => 'product_image', 'product_id' => $product_result_object->product_id));



                $product_admin_data_new[$key]->media_products = array();

                foreach ($product_medias as $keys => $product_media) {

                    if ($product_media->file_name != 'image-upload.jpg') {

                        $product_admin_data_new[$key]->media_products[$keys] = base_url('uploads/profile/' .
                            $product_result_object->profile_id . '/products/' . $product_media->file_name);
                    } else {

                        $product_admin_data_new[$key]->media_products[$keys]  = base_url('assets/images/image-upload.jpg');
                    }

                }


                //populate product
                $product_admin_data_new[$key]->product->product_id = $product_result_object->product_id;
                $product_admin_data_new[$key]->product->name = $product_result_object->name;
                $product_admin_data_new[$key]->product->desc = $product_result_object->p_desc;
                $product_admin_data_new[$key]->product->price = $product_result_object->price;
                $product_admin_data_new[$key]->product->sprice = $product_result_object->sprice;
                $product_admin_data_new[$key]->product->shipping_price = $product_result_object->shipping_price;
                $product_admin_data_new[$key]->product->shipping_method = $product_result_object->shipping_method;
                $product_admin_data_new[$key]->product->color = $product_result_object->color;
                $product_admin_data_new[$key]->product->size = $product_result_object->size;
                $product_admin_data_new[$key]->product->categories = $product_result_object->category;
                $product_admin_data_new[$key]->product->quantity = $product_result_object->quantity;

                //dump($store_admin_data_new[$key]->product->quantity);exit;
                //get paid information
                $product_admin_data_new[$key]->auction = new stdClass();

                $this->load->model('auction_model', 'auction');

                @$auction_info = $this->auction->get_by(array('product_id' => $product_result_object->product_id));
                //dump($get_paid_info);exit;
                $product_admin_data_new[$key]->auction->name = @$auction_info->name;
                $product_admin_data_new[$key]->auction->starting_price = @$auction_info->bid_price;
                $product_admin_data_new[$key]->auction->reserve_price = @$auction_info->reserve_price;
                $product_admin_data_new[$key]->auction->start_date = @$auction_info->start_date;
                $product_admin_data_new[$key]->auction->start_time = @$auction_info->start_time;
                $product_admin_data_new[$key]->auction->end_date = @$auction_info->end_date;
                $product_admin_data_new[$key]->auction->end_time = @$auction_info->end_time;
                $product_admin_data_new[$key]->auction->status = @$auction_info->status;
                $product_admin_data_new[$key]->auction->current_bid = @$auction_info->current_bid;

                $this->load->model('bid_model', 'bid');
                $product_admin_data_new[$key]->bid = new stdClass();
                $product_admin_data_new[$key]->bid->current_bid_count = $this->bid->count_by(array('auction_id' => @$auction_info->id));
                //$product_admin_data_new[$key]->bid->minimum_amount = $this->bid->get_by(array('min(price)'));
                //dump($product_admin_data_new);
            }
        }
        return $product_admin_data_new;

    }



}