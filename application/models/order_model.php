<?php

/**
 * Created by dkf4199.
 * Date: 07/28/2015
 */
class Order_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_database = $this->db;
    }

    protected $primary_key = 'o_id';


    public function get_purchase_order_total($pid)
    {
        $sql = "SELECT SUM(order_total) as ordertotal FROM orders WHERE buyer_id = ? ";
        $rs = $this->db->query($sql, array($pid));

        $row = $rs->row();
        return $row->ordertotal;

    }

    public function get_purchases($profile_id)
    {

        //var_dump($rs->result_array());exit;

        $this->db->select('* ,seller_user.email as seller_email,buyer_user.email as buyer_email,buyer_profile.fname as buyer_fname,buyer_profile.lname as buyer_lname,seller_profile.fname as seller_fname,seller_profile.lname as seller_lname');
        $this->db->from('orders');
        $this->db->join('order_detail', 'order_detail.order_id = orders.o_id');
        $this->db->join('profiles seller_profile ', 'seller_profile.id = order_detail.seller_id', 'left');
        $this->db->join('users seller_user', 'seller_user.id = seller_profile.user_id', 'left');
        $this->db->join('profiles buyer_profile', 'buyer_profile.id = orders.buyer_id', 'left');
        $this->db->join('users buyer_user', 'buyer_user.id = buyer_profile.user_id', 'left');
        /*$this->db->where('order_detail.buyer_delivery_confirmed','N');
        $this->db->where('order_detail.item_shipped','N');
        $this->db->where('orders.ostatus',$this->config->item('order_status')['purchased_stage']);
        $this->db->or_where('orders.ostatus',$this->config->item('order_status')['report_stage']);
        */
        $this->db->where('orders.buyer_id', $profile_id);
        $this->db->group_by('orders.o_id');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $my_purschase = $query->result();
        //var_dump($my_purschase);exit;

        return $my_purschase;

    }

    public function get_all_orders($profile_id)
    {


        $this->db->select('*,seller_user.email as seller_email,buyer_user.email as buyer_email,buyer_profile.fname as buyer_fname,buyer_profile.lname as buyer_lname,seller_profile.fname as seller_fname,seller_profile.lname as seller_lname');
        $this->db->from('order_detail');
        $this->db->join('orders', 'orders.o_id = order_detail.order_id');
        $this->db->join('profiles seller_profile ', 'seller_profile.id = order_detail.seller_id', 'left');
        $this->db->join('users seller_user', 'seller_user.id = seller_profile.user_id', 'left');
        $this->db->join('profiles buyer_profile', 'buyer_profile.id = orders.buyer_id', 'left');
        $this->db->join('users buyer_user', 'buyer_user.id = buyer_profile.user_id', 'left');

        $this->db->where('orders.ostatus', $this->config->item('order_status')['order_stage']);
        $this->db->where('order_detail.seller_id', $profile_id);
        $this->db->or_where('order_detail.seller_id', $profile_id);
        $this->db->where('order_detail.buyer_delivery_confirmed', 'N');
        $this->db->or_where('order_detail.buyer_delivery_confirmed', null);
        $this->db->group_by('orders.o_id');
        $this->db->order_by('orders.created_date', 'desc');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $my_sales_orders = $query->result();

        //dump($my_sales_orders);exit;

        //nooption we need the emails collected for buyers
        /*   $this->db->or_where('order_detail.item_shipped','N');
         $this->db->or_where('order_detail.item_shipped','N');$this->db->or_where('orders.ostatus',$this->config->item('order_status')['order_stage']);
         $new_sale $this->db->or_where('orders.ostatus',$this->config->item('order_status')['order_stage']);_orders = array();
         foreach ($all_products as $key => $value) {
             $new_sale_orders[$key] = new stdClass();
             $new_sale_orders[$key]->order_id = 	$value->o_id;
             $new_sale_orders[$key]->order_id = 	$value->o_id;
             $new_sale_orders[$key]->order_id = 	$value->o_id;
         }
         */

        return $my_sales_orders;


    }

    public function get_orders_to_seller($seller_profile_id, $per_page, $page)
    {


        $this->db->select('* ,buyer_profile.fname as buyer_fname,buyer_profile.lname as buyer_lname');
        $this->db->from('order_detail');
        $this->db->join('orders', 'orders.o_id = order_detail.order_id');
        $this->db->join('profiles buyer_profile', 'buyer_profile.id = orders.buyer_id', 'left');
        $this->db->join('users buyer_user', 'buyer_user.id = buyer_profile.user_id', 'left');
        $this->db->where('order_detail.seller_id', $seller_profile_id);
        $this->db->where('orders.ostatus', $this->config->item('order_status')['purchased_stage']);
        $this->db->group_by('order_detail.seller_id');
        $this->db->order_by('orders.created_date', 'desc');

        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
//		/$this->as_object();
        $my_sales_orders = $query->result();

        //nooption we need the emails collected for buyers
        /*
         $new_sale_orders = array();
         foreach ($all_products as $key => $value) {
             $new_sale_orders[$key] = new stdClass();
             $new_sale_orders[$key]->order_id = 	$value->o_id;
             $new_sale_orders[$key]->order_id = 	$value->o_id;
             $new_sale_orders[$key]->order_id = 	$value->o_id;
         }
         */
        //$my_sales_orders['orders_count'] = new stdClass();

        if (count($my_sales_orders) > 0 && !empty($my_sales_orders)) {

            $my_sales_orders[0]->orders_count = 0;
            $my_sales_orders[0]->money_owned = new stdClass();
            $my_sales_orders[0]->money_owned = 0;
            $my_sales_orders[0]->orders_count = count($my_sales_orders);

            foreach ($my_sales_orders as $sales_order) {
                //var_dump($sales_order->order_total);
                if (isset($sales_order->order_total))
                    $my_sales_orders[0]->money_owned = floatval($my_sales_orders[0]->money_owned) + floatval($sales_order->order_total);

            }

            return $my_sales_orders;

        }

        return null;


    }


    public function get_orderdetail_of_buyer($order_id)
    {

        //to do security checks on orders

        $this->db->select('*');
        $this->db->from('order_detail');
        $this->db->join('orders', 'orders.o_id = order_detail.order_id');
        $this->db->join('profiles', 'profiles.id = orders.buyer_id', 'left');
        $this->db->where('orders.o_id', $order_id);
        $this->db->group_by('order_detail.seller_id');
        $this->db->order_by('orders.created_date', 'desc');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $order_details_by_order_id_result = $query->result();

        $order_details_by_order_id_result[0]->order_products = new stdClass();
        $order_details_by_order_id_result[0]->order_products = array();
        $order_details_by_order_id_result[0]->order_products = $this->get_orderdetail_products($order_id);
        $order_details_by_order_id_result[0]->created_date = date_format(date_create($order_details_by_order_id_result[0]->created_date), 'l jS F Y');
        $order_details_by_order_id_result[0]->buyer_full_name = new stdClass();
        $order_details_by_order_id_result[0]->buyer_full_name = ucfirst($order_details_by_order_id_result[0]->fname) . ' ' . ucfirst($order_details_by_order_id_result[0]->lname);
        $order_details_by_order_id_result[0]->seller_full_name = new stdClass();

        //var_dump($order_details_by_order_id_result[0]);exit;

        return $order_details_by_order_id_result[0];

    }

    function asDollars($value)
    {
        return '$' . number_format($value, 2);
    }

    public function get_orderdetail_products($oid)
    {

        $sql = "SELECT 	b.order_id,
						b.seller_id, 
						b.product_id,
						c.name as productname,						
						b.quantity, 
					    b.price, 
					    b.item_shipped,
						b.tracking_number,
						b.buyer_delivery_confirmed,
					    b.created_date
				FROM orders a 
				INNER JOIN order_detail b ON a.o_id = b.order_id
				INNER JOIN products c on b.product_id = c.id
				WHERE b.order_id = ? 
				 ";
        $rs = $this->db->query($sql, array($oid));

        return $rs->result_array();

    }


    //accept update data of array with order id
    //update orders table this used for updating shipping address of buyer currently

    public function update_order_shipping_address($updated_data)
    {

        //ORDER table update
        $order_id = $updated_data['order_id'];
        //var_dump($order_id);
        $order_update_data = array(
            'shipto_fname' => $updated_data['first_name'],
            'shipto_lname' => $updated_data['last_name'],
            'shipto_street' => $updated_data['street_address_1'],
            'shipto_street2' => $updated_data['street_address_2'],
            'shipto_city' => $updated_data['city'],
            'shipto_state' => $updated_data['state'],
            'shipto_zip' => $updated_data['zip_code']
        );

        $result = $this->update($order_id, $order_update_data);

        if ($result)
            return true;
        else
            return false;

    }

    public function get_revenu($seller_id)
    {

        $this->db->select('sum(ord.price*ord.quantity) as revenu_sum');
        $this->db->from('order_detail ord');
        $this->db->where('ord.seller_id', $seller_id);
        $this->db->where('ord.buyer_delivery_confirmed', 'Y');
        $this->db->group_by('ord.order_id');
        $query = $this->db->get();
        $revenu_sum = $query->result();

        if (empty($revenu_sum))
            return 0;
        else
            return $revenu_sum[0]->revenu_sum;


    }

    /*
   * Execute stored procedure for calculating dashboard:)
   * Created by Daniel Adenew
   **/
    function call_get_dashboard_calculation_stored_procedure_for_user($profile_id)
    {
        //$arags = implode(" ",func_get_args());
        $call_stored_procedure = "CALL GetDashBoardCalculationsForUser($profile_id,
                                                            @total_sales,
															@lw_total_sales,
															@lm_total_sales,
															@total_orders,
															@lw_total_orders,
															@lm_total_orders,
															@total_paid_money_minus_fee,
															@lm_total_paid_money_minus_fee ,
															@lw_total_paid_money_minus_fee,
															@total_money_owned,
															@lw_total_money_owned ,
															@lm_total_money_owned)";

        $this->db->query($call_stored_procedure);
        //collect individaul results
        $calculated_results = '
							select @total_sales as total_sales,
							@lw_total_sales as lw_total_sales,
							@lm_total_sales as lm_total_sales,
							@total_orders as total_orders,
							@lw_total_orders as lw_total_orders,
							@lm_total_orders as lm_total_orders,
							@total_paid_money_minus_fee as total_paid_money_minus_fee,
							@lm_total_paid_money_minus_fee as lm_total_paid_money_minus_fee ,
							@lw_total_paid_money_minus_fee as lw_total_paid_money_minus_fee,
							@total_money_owned as total_money_owned,
							@lw_total_money_owned as lw_total_money_owned ,
							@lm_total_money_owned as lm_total_money_owned


						';

        $query = $this->db->query($calculated_results);

        $calculated_results = $query->result();


        return $calculated_results[0];
    }
    /*
    * Execute stored procedure for calculating dashboard:)
    * Created by Daniel Adenew
    **/
    function call_get_dashboard_calculation_stored_procedure()
    {
        //$arags = implode(" ",func_get_args());
        $call_stored_procedure = "CALL GetDashBoardCalculations( @total_sales,
															@lw_total_sales,
															@lm_total_sales,
															@total_orders,
															@lw_total_orders,
															@lm_total_orders,
															@total_paid_money_minus_fee,
															@lm_total_paid_money_minus_fee ,
															@lw_total_paid_money_minus_fee,
															@total_money_owned,
															@lw_total_money_owned ,
															@lm_total_money_owned)";

        $this->db->query($call_stored_procedure);
        //collect individaul results
        $calculated_results = '
							select @total_sales as total_sales,
							@lw_total_sales as lw_total_sales,
							@lm_total_sales as lm_total_sales,
							@total_orders as total_orders,
							@lw_total_orders as lw_total_orders,
							@lm_total_orders as lm_total_orders,
							@total_paid_money_minus_fee as total_paid_money_minus_fee,
							@lm_total_paid_money_minus_fee as lm_total_paid_money_minus_fee ,
							@lw_total_paid_money_minus_fee as lw_total_paid_money_minus_fee,
							@total_money_owned as total_money_owned,
							@lw_total_money_owned as lw_total_money_owned ,
							@lm_total_money_owned as lm_total_money_owned


						';

        $query = $this->db->query($calculated_results);

        $calculated_results = $query->result();


        return $calculated_results[0];
    }



    public function get_all_sales_to_admin($per_page, $page)
    {

        $this->db->select('* ,buyer_profile.fname as buyer_fname,buyer_profile.lname as buyer_lname');
        $this->db->from('order_detail');
        $this->db->join('orders', 'orders.o_id = order_detail.order_id');
        $this->db->join('profiles buyer_profile', 'buyer_profile.id = orders.buyer_id', 'left');
        $this->db->join('users buyer_user', 'buyer_user.id = buyer_profile.user_id', 'left');
        $this->db->where('orders.ostatus', $this->config->item('order_status')['purchased_stage']);
        $this->db->where('order_detail.buyer_delivery_confirmed', 'Y');
        $this->db->where('order_detail.item_shipped','Y');
        $this->db->group_by('orders.buyer_id');
        $this->db->order_by('orders.created_date', 'desc');

        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $my_sales_orders = $query->result();

        if (count($my_sales_orders) > 0 && !empty($my_sales_orders)) {

            $my_sales_orders[0]->orders_count = 0;
            $my_sales_orders[0]->money_owned = new stdClass();
            $my_sales_orders[0]->money_owned = 0;
            $my_sales_orders[0]->orders_count = count($my_sales_orders);

            foreach ($my_sales_orders as $sales_order) {
                //var_dump($sales_order->order_total);
                if (isset($sales_order->order_total))
                    $my_sales_orders[0]->money_owned = floatval($my_sales_orders[0]->money_owned) + floatval($sales_order->order_total);

            }

            return $my_sales_orders;

        }

        return null;

    }
    //get purchase overview

    public function get_all_purchases_to_admin($per_page, $page)
    {

        $this->db->select('* ,seller_user.email as seller_email,buyer_user.email as buyer_email,buyer_profile.fname as buyer_fname,buyer_profile.lname as buyer_lname,seller_profile.fname as seller_fname,seller_profile.lname as seller_lname');
        $this->db->from('orders');
        $this->db->join('order_detail', 'order_detail.order_id = orders.o_id');
        $this->db->join('profiles seller_profile ', 'seller_profile.id = order_detail.seller_id', 'left');
        $this->db->join('users seller_user', 'seller_user.id = seller_profile.user_id', 'left');
        $this->db->join('profiles buyer_profile', 'buyer_profile.id = orders.buyer_id', 'left');
        $this->db->join('users buyer_user', 'buyer_user.id = buyer_profile.user_id', 'left');
        $this->db->where('order_detail.buyer_delivery_confirmed','Y');
        $this->db->where('order_detail.item_shipped','Y');
        $this->db->where('orders.ostatus',$this->config->item('order_status')['purchased_stage']);
        $this->db->or_where('orders.ostatus',$this->config->item('order_status')['report_stage']);


        $this->db->group_by('orders.o_id');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();
        $my_purschase = $query->result();
        //var_dump($my_purschase);exit;

        return $my_purschase;

    }


 }
