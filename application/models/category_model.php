<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/26/14
 * Time: 6:52 PM
 */

class Category_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

    public function get_all_parent_categories() {

        $categories = $this->order_by('category','ASC')->get_many_by(array('parent_id is not NULL'));

        return $categories;

    }

}