<?php
/**
 * Created by Deron Frederickson.
 * Date: 06/06/15
 * Craig Robinson : One World Solutions LLC
 */

class Cart_model extends MY_Model {

	public function __construct() {
		parent::__construct();
		$this->_database = $this->db;
	}
	
	// Function to return number of items in the cart
	public function cart_count(){
		return $this->cart->total_items();
	}
	
	
}
/* End of file cart_model.php */
/* Location: ./application/models/cart_model.php */
