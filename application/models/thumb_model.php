<?php
/**
 * Created by PhpStorm.
 * User: DEVELOPER4
 * Date: 12/26/14
 * Time: 1:14 PM
 */

class Thumb_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

    /**
     * Define relationship a memeber
     * have One to One , One to Many ,
     * Many to Many if any
     */

    public $belongs_to = array (
                             'media' => array( 'primary_key' => 'media_id' ,'model'=>"media_model"),
                           );


    /**
     * @param $profile_id
     * @param $upload_config
     * @param $file_post_name
     * @return bool|int
     */
    public function save_thumb_for_media($media_id,$file_name)
    {
      
      $insert_data = $this->insert(array(
                    'media_id' => $media_id,
                    'file_name' => $file_name,
                ));       
              
      $thumbs_id = $this->insert($insert_data);

      return $thumbs_id;
        
    }




    /**
     * @param $profile_id
     * @param $store_id
     * @param $upload_data
     * @return bool|int
     */

     public function save_or_update_store($profile_id,$store_id,$upload_data) {


        if (isset($upload_data['file_name'])) {

            $file_name = $upload_data['file_name'];
            $file_path = $upload_data['file_path'];
           

              $store_image_id = $this->insert(array(
                    'type' => 'store_image',
                    'file_name' => $file_name,
                    'full_path' => $file_path,
                    'profile_id' => $profile_id,
                    'store_id'=> $store_id
                ));       
              
               
             return $store_image_id;
         }
         
         return -1;
        
    }

    /**
     * @param $profile_id
     * @param $product_id
     * @param $upload_data
     * @return bool|int
     */
    public function save_or_update_product($profile_id,$store_id,$product_id,$upload_data) {


        if (isset($upload_data['file_name'])) {

            $file_name = $upload_data['file_name'];
            $file_path = $upload_data['file_path'];

            $insert_data =  array(
                'type' => 'product_image',
                'file_name' => $file_name,
                'full_path' => $file_path,
                'profile_id' => $profile_id,
                'product_id'=> $product_id,
                'store_id'=> $store_id,
            );

            $product_image_id = $this->insert($insert_data);

            return $product_image_id;
        }

        return -1;

    }


    /**
     * @param $profile_id
     * @param $product_id
     * @param $upload_data
     * @return bool|int
     */
    public function update_product_images($media_id,$upload_data) {

      // dump("media details"); 
      // dump("product_id"+$product_id); 
      // dump($upload_data);


        if (isset($upload_data['file_name'])) {

           /// dump("updating details");  

            $file_name = $upload_data['file_name'];
            $file_path = $upload_data['file_path'];

            $update_data =  array(
                'type' => 'product_image',
                'file_name' => $file_name,
                'full_path' => $file_path,
            );

            $update_where_data = array('id'=>$media_id);
            //$this->output->enable_profiler(true);
            $media_id =  $this->update_by($update_where_data,$update_data);
            //dump("media id".$media_id);  
            return $media_id;
        }

        return -1;

    }


    /**
     * @param $upload_config
     * @param $file_post_name
     * @return array
     */
    public function upload_media($upload_config,$file_post_name){

        $this->load->library('upload', $upload_config);

        if (!is_dir($upload_config['upload_path']))
            mkdir($upload_config['upload_path'], 0777, TRUE);
          
        //rename files first      
        $temp = explode(".",$_FILES[$file_post_name]["name"]);
        $newfilename = 'profile_image'.rand(1,99999) . '.' .end($temp);
        $upload_config['file_name'] = $newfilename;

        $this->upload->initialize($upload_config);

        if (!$this->upload->do_upload($file_post_name)) {
            //upload failed
            //TODO:throw exception
            return array('error' => $this->upload->display_errors('<span>', '</span>'));

        } else {
            // upload success
            $upload_data = $this->upload->data();
        }

        return $upload_data;
    }

    /**
     * @param $profile_id
     * @param $file_post_name
     * @return array
     */
    public function upload_media_store_and_products($profile_id,$file_post_name) {

         $this->load->library('upload');
         //get the image file name for the store
         $upload_data_store = array();

         //get the image file name for the products
         $upload_data_products = array();

         $files = $_FILES;

         $total_count_of_files = count($_FILES[$file_post_name]['name']);

        //count FILES only who have a value / filename;

              for($i=0; $i < $total_count_of_files; $i++) {

                  //chop out the empty files
                  if(!empty($files[$file_post_name]['name'][$i])) {

                      $_FILES[$file_post_name]['name'] = $files[$file_post_name]['name'][$i];
                      $_FILES[$file_post_name]['type'] = $files[$file_post_name]['type'][$i];
                      $_FILES[$file_post_name]['tmp_name'] = $files[$file_post_name]['tmp_name'][$i];
                      $_FILES[$file_post_name]['error'] = $files[$file_post_name]['error'][$i];
                      $_FILES[$file_post_name]['size'] = $files[$file_post_name]['size'][$i];

                      //TODO :Resize IMAGES

                      //store image first
                      if ($i == 0) {

                          $pathToUpload = "./uploads/profile/" . $profile_id . "/store/";
                          //load the configuration
                          $upload_config = $this->config->item('upload_config_profile_edit');

                          $upload_config['upload_path'] = $pathToUpload;

                          //rename files first
                          $temp = explode(".", $_FILES[$file_post_name]["name"]);
                          $newfilename = 'store_image' . rand(1, 99999) . '.' . end($temp);
                          $upload_config['file_name'] = $newfilename;

                          if (!is_dir($upload_config['upload_path']))
                              mkdir($upload_config['upload_path'], 0777, TRUE);

                          $this->upload->initialize($upload_config);


                          if (!$this->upload->do_upload()) {
                              //upload failed
                              //TODO:throw th
                              return (array('error' => $this->upload->display_errors('<span>', '</span>')));

                          } else {
                              // upload success
                              $upload_data_store[] = $this->upload->data();
                          }


                      } else {

                          //chop out the empty files

                          $_FILES[$file_post_name]['name'] = $files[$file_post_name]['name'][$i];
                          $_FILES[$file_post_name]['type'] = $files[$file_post_name]['type'][$i];
                          $_FILES[$file_post_name]['tmp_name'] = $files[$file_post_name]['tmp_name'][$i];
                          $_FILES[$file_post_name]['error'] = $files[$file_post_name]['error'][$i];
                          $_FILES[$file_post_name]['size'] = $files[$file_post_name]['size'][$i];

                          $pathToUpload = "./uploads/profile/" . $profile_id . "/products/";
                          //load the configuration
                          $upload_config = $this->config->item('upload_config_profile_edit');

                          $upload_config['upload_path'] = $pathToUpload;

                          //rename files first
                          $temp = explode(".", $_FILES[$file_post_name]["name"]);
                          $newfilename = 'product_image' . rand(1, 99999) . '.' . end($temp);
                          $upload_config['file_name'] = $newfilename;

                          if (!is_dir($upload_config['upload_path']))
                              mkdir($upload_config['upload_path'], 0777, TRUE);

                          $this->upload->initialize($upload_config);

                          if (!$this->upload->do_upload()) {
                              //upload failed
                              //TODO:throw th
                              return (array('error' => $this->upload->display_errors('<span>', '</span>')));

                          } else {
                              // upload success
                              $upload_data_products[] = $this->upload->data();
                          }

                      }
                  }

           }
           //this data is need for later database save on media to store relationships
           $upload_result = array_merge($upload_data_store,$upload_data_products);
           
          // var_dump($upload_result);
           
            return $upload_result ;

     }


      /**
     * @param $profile_id
     * @param $file_post_name
     * @return array
     */
    public function upload_media_products($profile_id,$product_id,$file_post_name) {

         $this->load->library('upload');
         //get the image file name for the store
         $upload_data_store = array();

         //get the image file name for the products
         $upload_data_products = array();
         
         $files = $_FILES;
         $non_empty_files_index = array();
         $total_count_of_files = count($_FILES[$file_post_name]['name']);

          
        //count FILES only who have a value / filename;

              for($i=0; $i < $total_count_of_files; $i++) {

                  //dump($files[$file_post_name]['name'][$i]);
                  //chop out the empty files
                  if(!empty($files[$file_post_name]['name'][$i])) {

                          //TODO :Resize IMAGES

                         //chop out the empty files

                          $non_empty_files_index[] = $i; //collect the index of no empy file post sent

                          $_FILES[$file_post_name]['name'] = $files[$file_post_name]['name'][$i];
                          $_FILES[$file_post_name]['type'] = $files[$file_post_name]['type'][$i];
                          $_FILES[$file_post_name]['tmp_name'] = $files[$file_post_name]['tmp_name'][$i];
                          $_FILES[$file_post_name]['error'] = $files[$file_post_name]['error'][$i];
                          $_FILES[$file_post_name]['size'] = $files[$file_post_name]['size'][$i];

                          $pathToUpload = "./uploads/profile/" . $profile_id . "/products/";
                          //load the configuration
                          $upload_config = $this->config->item('upload_config_profile_edit');

                          $upload_config['upload_path'] = $pathToUpload;

                          //rename files first
                          $temp = explode(".", $_FILES[$file_post_name]["name"]);
                          $newfilename = 'product_image' . rand(1, 99999) . '.' . end($temp);
                          $upload_config['file_name'] = $newfilename;

                          if (!is_dir($upload_config['upload_path']))
                              mkdir($upload_config['upload_path'], 0777, TRUE);

                          $this->upload->initialize($upload_config);

                          if (!$this->upload->do_upload($file_post_name)) {
                              //upload failed
                              //TODO:throw th
                              //dump($this->upload->display_errors());exit;
                              return (array('error' => $this->upload->display_errors('<span>', '</span>')));

                          } else {
                              // upload success
                              //dump("success".$this->upload->data());
                              $upload_data_products[] = $this->upload->data();
                          }
                      
                  }

           }
           //this data is need for later database save on media to store relationships
           $upload_result = array_merge($upload_data_store,$upload_data_products);

           //dump($upload_result);exit;

        $media_ids = array();

        $this->load->model('product_model','products');

        $product_with_media = $this->products->with("medias")->get($product_id);

        if(!empty($product_with_media->medias)) {

          foreach ($product_with_media->medias as $media) {
            
                   $media_ids[] = $media->id;

          } 

        }
           
          // dump($upload_result);exit;

               $array_of_product_image_ids = array();

               $count = 0;

                  //insert the five product image into media table and collect inserted id
                  foreach($upload_result as $upload_result_product){
                    //dump('currently reading  ?'+count($upload_result_product));
                    $media_id = $media_ids[$non_empty_files_index[$count]];
                    $array_of_product_image_ids[] = $this->update_product_images($media_id,$upload_result_product);
                  //dump('array of images ids  ?'+count($array_of_product_image_ids));
                  $count++;
            }
           
            return $upload_result ;

     }


       /**
     * @param $profile_id
     * @param $file_post_name
     * @return array
     */
    public function upload_media_products_to_store($profile_id,$store_id,$file_post_name) {

         $this->load->library('upload');
         //get the image file name for the store
         $upload_data_store = array();

         //get the image file name for the products
         $upload_data_products = array();
         
         $files = $_FILES;
         $non_empty_files_index = array();
         $total_count_of_files = count($_FILES[$file_post_name]['name']);

          
        //count FILES only who have a value / filename;

              for($i=0; $i < $total_count_of_files; $i++) {

                  //dump($files[$file_post_name]['name'][$i]);
                  //chop out the empty files
                  if(!empty($files[$file_post_name]['name'][$i])) {

                          //TODO :Resize IMAGES

                         //chop out the empty files

                          $non_empty_files_index[] = $i; //collect the index of no empy file post sent

                          $_FILES[$file_post_name]['name'] = $files[$file_post_name]['name'][$i];
                          $_FILES[$file_post_name]['type'] = $files[$file_post_name]['type'][$i];
                          $_FILES[$file_post_name]['tmp_name'] = $files[$file_post_name]['tmp_name'][$i];
                          $_FILES[$file_post_name]['error'] = $files[$file_post_name]['error'][$i];
                          $_FILES[$file_post_name]['size'] = $files[$file_post_name]['size'][$i];

                          $pathToUpload = "./uploads/profile/" . $profile_id . "/products/";
                          //load the configuration
                          $upload_config = $this->config->item('upload_config_profile_edit');

                          $upload_config['upload_path'] = $pathToUpload;

                          //rename files first
                          $temp = explode(".", $_FILES[$file_post_name]["name"]);
                          $newfilename = 'product_image' . rand(1, 99999) . '.' . end($temp);
                          $upload_config['file_name'] = $newfilename;

                          if (!is_dir($upload_config['upload_path']))
                              mkdir($upload_config['upload_path'], 0777, TRUE);

                          $this->upload->initialize($upload_config);

                          if (!$this->upload->do_upload($file_post_name)) {
                              //upload failed
                              //TODO:throw th
                              //dump($this->upload->display_errors());exit;
                              return (array('error' => $this->upload->display_errors('<span>', '</span>')));

                          } else {
                              // upload success
                              //dump("success".$this->upload->data());
                              $upload_data_products[] = $this->upload->data();
                          }
                      
                  }

           }
           //this data is need for later database save on media to store relationships
           $upload_result = array_merge($upload_data_store,$upload_data_products);

           //dump($upload_result);exit;
           
            return $upload_result ;

     }

}









