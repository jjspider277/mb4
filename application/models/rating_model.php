<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 04/06/15
 * Time: 1:14 PM
*  Craig Robinson : One World Solutions LLC
 */

class Rating_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

    public $before_create = array( 'timestamps' );

    protected function timestamps($rating_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $rating_row['created_date'] = date('Y-m-d H:i:s');
        $rating_row['updated_date'] = date('Y-m-d H:i:s');
        return  $rating_row;
    }

    /**
     * Define relationship a memeber
     * have One to One , One to Many ,
     * Many to Many if any
     */

    public $belongs_to = array (
                             'product' => array( 'primary_key' => 'product_id' ,'model'=>"product_model"),
                             'profile' => array( 'primary_key' => 'profile_id' ,'model'=>"profile_model"),
                             'media'=>array('primary_key'=>'media_id','model'=>'media_model'),
                             'store'=>array('primary_key'=>'store_id','model'=>'store_model'),
                           );



   public function save_or_update_rating($rated_item_id,$rate_type,$rated,$created_by) {


    $rate_id = -1;

   //first find if the database already has the rating for the to be rated item id

    /*echo($rated_item_id);
    echo($rate_type);
    echo($rated);
    echo($created_by);exit;*/
    $rating_update_insert_check = true;
    if($rate_type=='product')
    $rating_update_insert_check = $this->get_by(array('product_id'=>$rated_item_id,'rate_type'=>$rate_type,'created_by'=>$created_by));
    if($rate_type=='profile')
    $rating_update_insert_check = $this->get_by(array('profile_id'=>$rated_item_id,'rate_type'=>$rate_type,'created_by'=>$created_by)); 
    if($rate_type=='store')
    $rating_update_insert_check = $this->get_by(array('store_id'=>$rated_item_id,'rate_type'=>$rate_type,'created_by'=>$created_by));  

    if(empty($rating_update_insert_check)) {
    
        if($rate_type=="product") {
           $insert_data = array(
                          'product_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );          

            $rate_id = $this->insert($insert_data);

          
            /* //update products table
            $product_update_data = array(                          
                          'rate_id' =>$rate_id ,                                             
                          );

            $this->load->model('product_model','product_rating');
            *
            $this->product_rating->update($rated_item_id,$product_update_data);
            */

        } 

        if($rate_type=="profile") {
             $insert_data = array(
                          'profile_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );

            $rate_id = $this->insert($insert_data);

            /* //update products table
            $profile_update_data = array(                          
                          'rate_id' =>$rate_id ,                                             
                          );

            $this->load->model('profile_model','profile_rating');
            $this->profile_rating->update($rated_item_id,$profile_update_data);
            **/


        }

        if($rate_type=="store") {
             $insert_data = array(
                          'store_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );

            $rate_id = $this->insert($insert_data);

            /*//update products table
            $store_update_data = array('rate_id' =>$rate_id ,);

            $this->load->model('store_model','store_rating');
            $this->store_rating->update($rated_item_id,$profile_update_data);
          */

        }

        return $rate_id;

    }
    else {

          
         if($rate_type=="product") {
            $update_data = array(
                          'product_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );

            $rate_id = $this->update($rating_update_insert_check->id,$update_data);

        } 

        if($rate_type=="profile") {
             $update_data = array(
                          'profile_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );

            $rate_id = $this->update($rating_update_insert_check->id,$update_data);
        }

        if($rate_type=="store") {
             $update_data = array(
                          'store_id' =>$rated_item_id ,
                          'rating' =>$rated ,
                          'created_by' =>$created_by ,
                          'updated_by' =>$created_by ,
                          'rate_type'=>$rate_type                      
                          );

            $rate_id = $this->update($rating_update_insert_check->id,$update_data);
        }


     return $rate_id;

    }

     //TODO:

   }

/**
    Execute stored procedure for rating:)
    Created by Daniel Adenew
    **/
    function call_rating_stored_procedure($rated_item_id,$type){
     //$arags = implode(" ",func_get_args());    
    $call_stored_procedure ="CALL ratingProcedure('$rated_item_id', '$type', @avg_rating)";
    $this->db->query($call_stored_procedure);
    $avg_rating_result = 'SELECT @avg_rating as avg_rating';
    $query = $this->db->query($avg_rating_result);
    $avg_rating_result = $query->result();
    return round($avg_rating_result[0]->avg_rating,2); //done :)
}

}









