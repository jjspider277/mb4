<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */

class State_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   // protected $return_type = 'array';
    public $belongs_to = array( 'profile' => array( 'primary_key' => 'state' ,'model'=>'state_model'));

    public function populate_state_dropdown(){

        $populated_states =  $this->as_array()->get_all();

        $states = array();

        foreach($populated_states as $populated_state){
            $states[$populated_state['state']] = $populated_state['state'];
        }

        return $states;
    }
	
	public function convert_state_abbrev($abbrev){
		$all_states = array(
				'Al'=>'Alabama',
				'Ak'=>'Alaska',
				'Az'=>'Arizona',
				'Ar'=>'Arkansas',
				'Ca'=>'California',
				'Co'=>'Colorado',
				'Ct'=>'Connecticut',
				'De'=>'Delaware',
				'Dc'=>'District Of Columbia',
				'Fl'=>'Florida',
				'Ga'=>'Georgia',
				'Hi'=>'Hawaii',
				'Id'=>'Idaho',
				'Il'=>'Illinois',
				'In'=>'Indiana',
				'Ia'=>'Iowa',
				'Ks'=>'Kansas',
				'Ky'=>'Kentucky',
				'La'=>'Louisiana',
				'Me'=>'Maine',
				'Md'=>'Maryland',
				'Ma'=>'Massachusetts',
				'Mi'=>'Michigan',
				'Mn'=>'Minnesota',
				'Ms'=>'Mississippi',
				'Mi'=>'Missouri',
				'Mt'=>'Montana',
				'Nb'=>'Nebraska',
				'Nv'=>'Nevada',
				'Nh'=>'New Hampshire',
				'Nj'=>'New Jersey',
				'Nm'=>'New Mexico',
				'Nc'=>'North Carolina',
				'Nd'=>'North Dakota',
				'Oh'=>'Ohio',
				'Ok'=>'Oklahoma',
				'Or'=>'Oregon',
				'Pa'=>'Pennsylvania',
				'Ri'=>'Rhode Island',
				'Sc'=>'South Carolina',
				'Sd'=>'South Dakota',
				'Tn'=>'Tennessee',
				'Tx'=>'Texas',
				'Ut'=>'Utah',
				'Vt'=>'Vermont',
				'Va'=>'Virginia',
				'Wa'=>'Washington',
				'Wv'=>'West Virginia',
				'Wi'=>'Wisconsin',
				'Wy'=>'Wyoming',
				'So'=>'South Carolina');
		return $all_states[$abbrev];
	}

    
} 