<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */


class Store_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;     
        $this->_database = $this->db;
    }

    public $before_create = array( 'timestamps' );

    protected function timestamps($table)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $table['created_date'] = date('Y-m-d H:i:s');
        return $table;
    }

    /**
     * Define relationship a memeber
     * have One to One , One to Many ,
     * Many to Many if any
     */


    public $has_many = array('products' => array( 'primary_key' => 'store_id' ,'model'=>"product_model"),
                             'ratings' => array( 'primary_key' => 'store_id' ,'model'=>"rating_model") );
//'medisas' => array( 'primary_key' => 'media_id' ,'model'=>"media_model") 
    public $belongs_to = array('profile' => array( 'primary_key' => 'owner_profile_id' ,'model'=>"profile_model"),
                                'media' => array( 'primary_key' => 'media_id' ,'model'=>"media_model"));
    

    /**
     * @param $post
     * @param $profile_id
     * @return bool
     */
    public function save_store($post,$profile_id)
    {


        $storename = $post['storename'];

        $store_description = $post['store_description'];

        $insert_data = array(
               'store_name' =>$storename ,
               'desc' =>$store_description ,
               'owner_profile_id'=>$profile_id,
               'is_launched' =>1,
               'is_paid'=>1
         );

        $store_id = $this->insert($insert_data) ;
        return $store_id;

    }

    public function get_store_lisiting($profile_id) {



  //$all_stores_data = $this->with('products')->with('profile')->with('media')->get_by('owner_profile_id',$profile_id);

   // var_dump($all_stores_data);exit;
   //var_dump($all_stores_data);
   
    $this->db->select('
		stores.id as store_id,
		stores.desc as sdescription,
		stores.created_date as s_created_date,
		stores.avg_rating as s_avg_rating,
		stores.store_name,
		profiles.id as profile_id,
		profiles.fname,
		profiles.lname,
		profiles.avg_rating as pro_avg_rating,
		media.id as media_id,
		media.file_name,
		media.full_path,
		media.avg_rating as m_avg_rating'
		);
		 $this->db->from('stores');
		  $this->db->join('profiles', 'profiles.id = stores.owner_profile_id');
		   $this->db->join('(SELECT *, MIN(medias.store_id) FROM medias GROUP BY medias.store_id) media','media.store_id = stores.id');
		   $this->db->where('stores.owner_profile_id', $profile_id);
			$this->db->group_by('stores.id');
  /*    $this->db->select('*,stores.created_date as s_created_date ,medias.avg_rating as m_avg_rating,profiles.avg_rating as pro_avg_rating,stores.avg_rating as s_avg_rating,stores.id as store_id,stores.desc as sdescription ,medias.id as media_id, profiles.id as profile_id');
      $this->db->from('stores');    
      $this->db->join('medias', 'medias.store_id = stores.id');     
      $this->db->join('profiles', 'profiles.id = stores.owner_profile_id');
      $this->db->where('stores.owner_profile_id', $profile_id);
      $this->db->group_by('stores.id');
	 */
      //$this->db->limit($per_page, $page);
      $query = $this->db->get();
     
      $all_stores_data = $query->result();

      /*$all_stores_data[$key]->product = new stdClass();
         $all_stores_data[$key]->product->id = $store_result_obejct->product_id;   
         $all_stores_data[$key]->product->name = $store_result_obejct->name;
         $all_stores_data[$key]->product->desc = $store_result_obejct->pdescription;
         $all_stores_data[$key]->product->price = $store_result_obejct->price;   
         $all_stores_data[$key]->product->sprice = $store_result_obejct->sprice;
         $all_stores_data[$key]->product->desc = $store_result_obejct->pdescription;
         $all_stores_data[$key]->product->quantity = $store_result_obejct->quantity;*/
     //var_dump($all_stores_data);exit;

      if( is_array($all_stores_data) ) {


        foreach ($all_stores_data as $key =>$store_result_obejct) {

         $all_stores_data[$key]->store = new stdClass();
         $all_stores_data[$key]->store->store_id = $store_result_obejct->store_id;   
         $all_stores_data[$key]->store->name = $store_result_obejct->store_name;
         $all_stores_data[$key]->store->desc = $store_result_obejct->sdescription;
         $all_stores_data[$key]->store->created_date = $store_result_obejct->s_created_date;
         //get listing count
         $all_stores_data[$key]->store->number_of_products = $this->get_how_many_products_per_store($store_result_obejct->store_id);

         $all_stores_data[$key]->profile = new stdClass();
         $all_stores_data[$key]->profile->id = $store_result_obejct->profile_id;   
         $all_stores_data[$key]->profile->fname = $store_result_obejct->fname;
         $all_stores_data[$key]->profile->lname = $store_result_obejct->lname;
         $all_stores_data[$key]->profile->profile_rating = $store_result_obejct->pro_avg_rating;

         $all_stores_data[$key]->media = new stdClass();
         $all_stores_data[$key]->media->id = $store_result_obejct->media_id;   
         $all_stores_data[$key]->media->file_name = $store_result_obejct->file_name;
         $all_stores_data[$key]->media->full_path = $store_result_obejct->full_path;  
         $all_stores_data[$key]->profile->media_rating = $store_result_obejct->m_avg_rating;
        
        //TODO:some calculations
       
        $all_stores_data[$key]->store_rating = isset($store_result_obejct->s_avg_rating)?$store_result_obejct->s_avg_rating:0; 
      
        $all_stores_data[$key]->media->file_name = base_url('uploads/profile/'.$store_result_obejct->profile_id.'/store/'.$store_result_obejct->file_name);
        }

          return $all_stores_data;
        }

  else {

       //todo:Verify when this will happen and modify accordingly

        $new_array = array();

        $all_stores_data->media->file_name = base_url('uploads/profile/'.$all_stores_data->profile->id.'/store/'.$all_stores_data->media->file_name);
        array_push($new_array,$all_stores_data);
        return $new_array;

        }
    
    }

     public function get_store_details($store_id) {
      
      $profile_store_with_product_media_data =$this->with('products')->with('media')->get($store_id);
     
      //dump($profile_store_with_product_media_data);exit;
      //arrange the array for display
      $key_value = array();

      foreach ($profile_store_with_product_media_data as $key => $value) {
        
        $key_value[$key] = $value;

      }
      //dump($profile_store_with_product_media_data);exit;
      return $key_value;

    }

    public function get_store_lisiting_products($id,$per_page,$page) {

      //var_dump($id);
	  
	  $this->db->select('
	stores.id as id,
	stores.store_name as store_name,
	stores.created_date as opened,
    products.id as product_id,
    products.name as name,
    products.desc as pdescription,
    products.price as price,
    products.sprice as sprice,
	products.product_details as product_details,
    products.category as product_category,
    products.quantity as product_quantity,
	products.created_date as product_added_date,
	products.color as product_color,
	products.size as product_size,
	products.variation as product_variation,
	products.sub_variation as product_sub_variation,
	products.shipping_price as product_shipping_price,
	products.shipping_method as product_shipping_method,
	products.avg_rating as product_avg_rating,
	products.lead_time_days as lead_time_days,
    media.id as media_id,
    media.file_name as file_name,
    media.full_path as media_path,
    profiles.id as profile_id,
	profiles.fname as fname,
	profiles.lname as lname');
	$this->db->from('stores');
	$this->db->join('products','products.store_id = stores.id');
	$this->db->join('profiles','profiles.id = stores.owner_profile_id');
    $this->db->join('(SELECT *, MIN(medias.product_id) FROM medias GROUP BY medias.product_id) media','media.product_id = products.id');
    $this->db->where('stores.id', $id);
	$this->db->order_by('product_added_date');
/*
      $this->db->select('*,products.avg_rating as p_avg_rating,stores.avg_rating as s_avg_rating,products.id as product_id,products.desc as pdescription');
      $this->db->from('products');
      $this->db->join('stores', 'stores.id = products.store_id');
      $this->db->where('stores.id', intval($id)); 
      $this->db->group_by('stores.id'); 
	 */
      $query = $this->db->get();
      $this->db->limit($per_page, $page);
      $products_in_the_store = $query->result();

    //var_dump($products_in_the_store);
     $product_details = array();     
	 

     foreach ($products_in_the_store as $key => $product_object) {
        $product_details[$key] = new stdClass();
        $product_details[$key]->id = $product_object->id; 
        $product_details[$key]->store_name = $product_object->store_name; 
        $product_details[$key]->opened = $product_object->opened; 
        $product_details[$key]->owner_fname = $product_object->fname; 
        $product_details[$key]->owner_lname = $product_object->lname; 
        $product_details[$key]->product_id = $product_object->product_id;      
        $product_details[$key]->name = $product_object->name;
        $product_details[$key]->desc = $product_object->pdescription;
        $product_details[$key]->price = $product_object->price;
        $product_details[$key]->sprice = $product_object->sprice;
        $product_details[$key]->product_details = $product_object->product_details;     

        //TODO:some calcualtions
        $product_details[$key]->product_rating = isset($product_object->p_avg_rating)?$product_object->p_avg_rating:0; ;

        $product_details[$key]->profile_id = $product_object->profile_id;
		
		// *** removed GK 7/24/2016
       //  $this->load->model('media_model','medias');
        
		// $medias = $this->medias->get_by(array('product_id' => $product_object->product_id, 'file_name <> "image-upload.jpg"'));
		 
		 // *** removed GK 7/24/2016
         //TODO:When there are many products on the database this code need to be fixed or properly adujsted
        // if ($medias != null) { 
            // $product_details[$key]->image= base_url() . '/uploads/profile/' . $product_object->profile_id . '/products/' . $medias->file_name; //get only the first product image
       //  }
		$product_details[$key]->image= base_url() . '/uploads/profile/' . $product_object->profile_id . '/products/' . $product_object->file_name; //get only the first product image
        $product_details[$key]->seller_name = $this->session->userdata('profile_fname');
      }

      //var_dump($product_details);exit;
      return (object)$product_details;

    }

   public function get_how_many_products_per_store($id) {

      $this->db->select('COUNT(p.id) as total_products_in_store');
      $this->db->from('products p');
      $this->db->join('stores s', 's.id = p.store_id');
      $this->db->where('s.id', intval($id));   
      $query = $this->db->get();
      $products_in_the_store_count = $query->result();
      
      return intval($products_in_the_store_count);

    }
    /**
    **/
    public function get_all_store_details($store_id,$profile_id) {


      $this->db->select('*,medias.avg_rating as m_avg_rating,profiles.avg_rating as pro_avg_rating,stores.avg_rating as s_avg_rating,stores.id as store_id,stores.desc as sdescription ,medias.id as media_id, profiles.id as profile_id');
      $this->db->from('stores');    
      $this->db->join('medias', 'medias.store_id = stores.id');     
      $this->db->join('profiles', 'profiles.id = stores.owner_profile_id');
      $this->db->where('stores.owner_profile_id', $profile_id);
      $this->db->where('stores.store_id', $store_id);
      $this->db->group_by('stores.id');
      //$this->db->limit($per_page, $page);
      $query = $this->db->get();
     
      $all_stores_data = $query->result();

      // /dump($all_stores_data);exit;


      if( is_array($all_stores_data) ) {  
    
        foreach ($all_stores_data as $key =>$store_result_obejct) {

         $all_stores_data[$key]->store = new stdClass();
         $all_stores_data[$key]->store->store_id = $store_result_obejct->id;   
         $all_stores_data[$key]->store->name = $store_result_obejct->store_name;
         $all_stores_data[$key]->store->desc = $store_result_obejct->sdescription;
         //get listing count
         $all_stores_data[$key]->store->number_of_products = $this->get_how_many_products_per_store($store_result_obejct->id);

         $all_stores_data[$key]->profile = new stdClass();
         $all_stores_data[$key]->profile->id = $store_result_obejct->profile_id;   
         $all_stores_data[$key]->profile->fname = $store_result_obejct->fname;
         $all_stores_data[$key]->profile->lname = $store_result_obejct->lname;
         $all_stores_data[$key]->profile->profile_rating = $store_result_obejct->pro_avg_rating;

         $all_stores_data[$key]->media = new stdClass();
         $all_stores_data[$key]->media->id = $store_result_obejct->media_id;   
         $all_stores_data[$key]->media->file_name = $store_result_obejct->file_name;
         $all_stores_data[$key]->media->full_path = $store_result_obejct->full_path;  
         $all_stores_data[$key]->profile->media_rating = $store_result_obejct->m_avg_rating;
        
        //TODO:some calcualtions
       
        $all_stores_data[$key]->store_rating = isset($store_result_obejct->s_avg_rating)?$store_result_obejct->s_avg_rating:0; 
      
        $all_stores_data[$key]->media->file_name = base_url('uploads/profile/'.$store_result_obejct->profile_id.'/store/'.$store_result_obejct->file_name);
        }

          return $all_stores_data;
        }

  else {

       //todo:Veirfy when this will happen and modify accordingly

        $new_array = array();

        $all_stores_data->media->file_name = base_url('uploads/profile/'.$all_stores_data->profile->id.'/store/'.$all_stores_data->media->file_name);
        array_push($new_array,$all_stores_data);
        return $new_array;

        }

    }

   //update store data
   public function update_store($file_post_name,$store_id,$profile_id,$get_post_data) {
     
      //uploda media 
      //TODO:needs to be moved to the MEDIA MODLE 
      $upload_data_store = null;

     if(!empty($_FILES[$file_post_name]['name'])) {
      
      //load the configuration
      $upload_config = $this->config->item('upload_config_profile_edit')
      ;
      $this->load->library('upload',$upload_config);

      $pathToUpload = "./uploads/profile/" . $profile_id . "/store/";
     

      $upload_config['upload_path'] = $pathToUpload;

      //rename files first
      $temp = explode(".", $_FILES[$file_post_name]["name"]);
      $newfilename = 'store_image' . rand(1, 99999) . '.' . end($temp);
      $upload_config['file_name'] = $newfilename;

      if (!is_dir($upload_config['upload_path']))
          mkdir($upload_config['upload_path'], 0777, TRUE);

      $this->upload->initialize($upload_config);

      if (!$this->upload->do_upload($file_post_name)) {
          //upload failed
          //TODO:throw th
          echo (array('error' => $this->upload->display_errors('<span>', '</span>')));

      } else {
          // upload success
          $upload_data_store[] = $this->upload->data();
      }
      
    }

       //now update the store database
       if (!is_null($upload_data_store)) {

            $file_name = $upload_data_store[0]['file_name'];
            $file_path = $upload_data_store[0]['file_path'];
           
           $array_update_data = array(
                    'type' => 'store_image',
                    'store_id'=>$store_id,
                    'file_name' => $file_name,
                    'full_path' => $file_path,                                      
                );

              $this->load->model('media_model','medias');

              $media_id = $this->store->get($store_id)->media_id;

              $result = $this->medias->update($media_id,$array_update_data); 

              $array_update_data = array(                                     
                    'store_name'=>$get_post_data['storename'],
                    'owner_profile_id' => $profile_id,
                    'desc'=>$get_post_data['store_description'],
                    'media_id'=>$media_id,          
                );

             $result = $this->update($store_id,$array_update_data); 
               
             return $result;
         }

         else {
                   
            $array_update_data = array(                                     
                    'store_name'=>$get_post_data['storename'],
                    'owner_profile_id' => $profile_id,
                    'desc'=>$get_post_data['store_description'],          
                );

              $result = $this->update($store_id,$array_update_data);       
              
                       
              return $result;
         }
         
         return -1;


    }
    //get all stores for admin
    public function get_all_stores_for_admin($per_page,$page) {

        $this->db->select('*,stores.id as store_id,stores.desc as sdescription ,medias.id as media_id');
        $this->db->from('stores');
        $this->db->join('medias', 'medias.store_id = stores.id');
        $this->db->group_by('stores.id');
        $this->db->order_by('stores.created_date');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();

        $store_admin_data = $query->result();
        //dump($store_admin_data);exit;
        $store_admin_data_new =  array();

        if( is_array($store_admin_data) ) {

            foreach ($store_admin_data as $key =>$store_result_obejct) {
                $store_admin_data_new[$key] = new stdClass();
                $store_admin_data_new[$key]->store = new stdClass();
                $store_admin_data_new[$key]->store->store_id = $store_result_obejct->id;
                $store_admin_data_new[$key]->store->name = $store_result_obejct->store_name;
                $store_admin_data_new[$key]->store->desc = $store_result_obejct->sdescription;
                $store_admin_data_new[$key]->store->created_date = $store_result_obejct->created_date;
                $store_admin_data_new[$key]->media = new stdClass();
                $store_admin_data_new[$key]->media->file_name = base_url('uploads/profile/'.$store_result_obejct->profile_id.'/store/'.$store_result_obejct->file_name);
            }
        }

        else {

            //todo:Veirfy when this will happen and modify accordingly

            $new_array = array();

            $store_admin_data_new->media->file_name = base_url('uploads/profile/'.$store_admin_data->profile->id.'/store/'.$store_admin_data->media->file_name);
            array_push($new_array,$store_admin_data_new);
            $store_admin_data_new = $new_array ;
        }
        //dump($store_admin_data_new);exit;
        return $store_admin_data_new;
    }


    //get all stores for admin
    public function get_all_stores_for_own_admin($profile_id,$per_page,$page) {

        $this->db->select('*,stores.id as store_id,stores.desc as sdescription ,medias.id as media_id');
        $this->db->from('stores');
        $this->db->join('medias', 'medias.store_id = stores.id');
        $this->db->join('profiles', 'profiles.id = stores.owner_profile_id');
        $this->db->where('stores.owner_profile_id', $profile_id);
        $this->db->group_by('stores.id');
        $this->db->order_by('stores.created_date');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();

        $store_admin_data = $query->result();
        //dump($store_admin_data);exit;
        $store_admin_data_new =  array();

        if( is_array($store_admin_data) ) {

            foreach ($store_admin_data as $key =>$store_result_obejct) {
                $store_admin_data_new[$key] = new stdClass();
                $store_admin_data_new[$key]->store = new stdClass();
                $store_admin_data_new[$key]->profile = new stdClass();
                //get listing count
                $store_admin_data_new[$key]->store->number_of_products = $this->get_how_many_products_per_store($store_result_obejct->id);
                $store_admin_data_new[$key]->profile->id = $store_result_obejct->profile_id;
                $store_admin_data_new[$key]->profile->full_name = ucfirst($store_result_obejct->fname).' '.ucfirst($store_result_obejct->lname);
                //load user model
                $this->load->model('user_model','users');
                $user_email = $this->users->get($store_result_obejct->user_id)->email;
                $store_admin_data_new[$key]->profile->email = $user_email;
                //dump($user_email);exit;
                $store_admin_data_new[$key]->store->store_id = $store_result_obejct->store_id;
                $store_admin_data_new[$key]->store->name = $store_result_obejct->store_name;
                $store_admin_data_new[$key]->store->desc = $store_result_obejct->sdescription;
                $store_admin_data_new[$key]->store->created_date = $store_result_obejct->created_date;
                $store_admin_data_new[$key]->media = new stdClass();
                $store_admin_data_new[$key]->media->file_name = base_url('uploads/profile/'.$store_result_obejct->profile_id.'/store/'.$store_result_obejct->file_name);
            }
        }

        else {

            //todo:Veirfy when this will happen and modify accordingly

            $new_array = array();

            $store_admin_data_new->media->file_name = base_url('uploads/profile/'.$store_admin_data->profile->id.'/store/'.$store_admin_data->media->file_name);
            array_push($new_array,$store_admin_data_new);
            $store_admin_data_new = $new_array ;
        }
        //dump($store_admin_data_new);exit;

        return $store_admin_data_new;

    }



    //get all stores for admin
    public function getStoreDataForEdit($store_id) {

        //dump($store_id);exit;

        $this->db->select('*,products.id as product_id,products.desc as p_desc , stores.id as store_id,stores.desc as sdescription ,medias.id as media_id');
        $this->db->from('stores');
        $this->db->join('products', 'products.store_id = stores.id');
        $this->db->join('medias', 'medias.store_id = stores.id');
        $this->db->group_by('stores.id');
        $this->db->where('stores.id',$store_id);
        $this->db->order_by('stores.created_date');
        //$this->db->limit($per_page, $page);
        $query = $this->db->get();

        $store_admin_data = $query->result();

       // dump($store_admin_data);exit;

        $store_admin_data_new =  array();

        if( is_array($store_admin_data) ) {

            foreach ($store_admin_data as $key =>$store_result_obejct) {
                //append product_data_for_edit_store

                $store_admin_data_new[$key] = new stdClass();
                $store_admin_data_new[$key]->store = new stdClass();
                $store_admin_data_new[$key]->product = new stdClass();
                $store_admin_data_new[$key]->store->store_id = $store_result_obejct->store_id;
                $store_admin_data_new[$key]->store->name = $store_result_obejct->store_name;
                $store_admin_data_new[$key]->store->desc = $store_result_obejct->sdescription;
                $store_admin_data_new[$key]->store->created_date = $store_result_obejct->created_date;
                $store_admin_data_new[$key]->media_store = new stdClass();
                $store_admin_data_new[$key]->media_store->file_name = base_url('uploads/profile/'.$store_result_obejct->profile_id.'/store/'.$store_result_obejct->file_name);

                $this->load->model('media_model','medias_products');
                $product_medias =  $this->medias_products->get_many_by(array('type'=>'product_image','product_id'=>$store_result_obejct->product_id));

                //dump($product_medias);exit;

                $store_admin_data_new[$key]->media_products = array();

                foreach($product_medias as $keys => $product_media) {

                    if($product_media->file_name!='image-upload.jpg') {

                        $store_admin_data_new[$key]->media_products[$keys] = base_url('uploads/profile/' .
                            $store_result_obejct->profile_id . '/products/' . $product_media->file_name);
                    }
                    else {

                        $store_admin_data_new[$key]->media_products[$keys]  = base_url('assets/images/image-upload.jpg');
                    }

                }



                //populate product
                $store_admin_data_new[$key]->product->product_id = $store_result_obejct->product_id;
                $store_admin_data_new[$key]->product->name = $store_result_obejct->name;
                $store_admin_data_new[$key]->product->desc = $store_result_obejct->p_desc;
                $store_admin_data_new[$key]->product->price = $store_result_obejct->price;
                $store_admin_data_new[$key]->product->sprice = $store_result_obejct->sprice;
                $store_admin_data_new[$key]->product->shipping_price = $store_result_obejct->shipping_price;
                $store_admin_data_new[$key]->product->shipping_method = $store_result_obejct->shipping_method;
                $store_admin_data_new[$key]->product->color = $store_result_obejct->color;
                $store_admin_data_new[$key]->product->size = $store_result_obejct->size;
                $store_admin_data_new[$key]->product->categories = $store_result_obejct->category;
                $store_admin_data_new[$key]->product->quantity = $store_result_obejct->quantity;

                //dump($store_admin_data_new[$key]->product->quantity);exit;
                //get paid information
                $store_admin_data_new[$key]->get_paid = new stdClass();

                $this->load->model('account_model','get_paid');

                @$get_paid_info = $this->get_paid->get_by(array('store_id'=>$store_result_obejct->store_id));
                //dump($get_paid_info);exit;
                    $store_admin_data_new[$key]->get_paid->account_type = @$get_paid_info->account_type;;
                    $store_admin_data_new[$key]->get_paid->branch = @$get_paid_info->account_owner;
                    $store_admin_data_new[$key]->get_paid->owner_name = @$get_paid_info->account_owner;
                    $store_admin_data_new[$key]->get_paid->account_number = @$get_paid_info->account_number;;
                    $store_admin_data_new[$key]->get_paid->routing_number = @$get_paid_info->routing_number;

                //dump($store_admin_data_new);
                return $store_admin_data_new;


            }
           // exit;
        }

        else {

            //todo:Verify when this will happen and modify accordingly

            $new_array = array();

            $store_admin_data_new->media->file_name = base_url('uploads/profile/'.$store_admin_data->profile->id.'/store/'.$store_admin_data->media->file_name);
            array_push($new_array,$store_admin_data_new);
            $store_admin_data_new = $new_array ;
        }

       // dump($store_admin_data_new);

        return $store_admin_data_new;

    }

    public function get_list_of_stores($profile_id) {

       $store_list = $this->order_by('store_name','ASC')->get_many_by(array('owner_profile_id'=>$profile_id));
        return $store_list;
    }

}









