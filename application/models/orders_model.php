<?php

/**
 * Created by dkf4199.
 * Modifide by dantheman
 * Date: 03/29/2016
 */
class Orders_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_database = $this->db;
    }

    public $before_create = array('timestamps');


    protected function timestamps($orders_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $orders_row['created_date'] = date('Y-m-d H:i:s');
        $orders_row['ostatus'] = $this->config->item('order_status')['order_stage'];
        $orders_row['seller_payment_status'] = $this->config->item('payment_status')['pending'];
        $orders_row['buyer_payment_status'] = $this->config->item('payment_status')['paid'];

        return $orders_row;
    }

    public function insert_order_data()
    {

        $rightnow = date("Y-m-d H:i:s");
        //ORDER table insert
        $order_insert_data = array(
            'invoice_id' => $this->session->userdata('anet_transid'),
            'anet_transid' => $this->session->userdata('anet_transid'),
            'buyer_id' => $this->session->userdata('profile_id'),
            'order_total' => $this->cart->total(),
            'shipto_fname' => $this->session->userdata('shipping_firstname'),
            'shipto_lname' => $this->session->userdata('shipping_lastname'),
            'shipto_street' => $this->session->userdata('shipping_street'),
            'shipto_street2' => $this->session->userdata('shipping_street2'),
            'shipto_city' => $this->session->userdata('shipping_city'),
            'shipto_state' => $this->session->userdata('shipping_state'),
            'shipto_zip' => $this->session->userdata('shipping_zip'),
        );

        $order_id = $this->insert($order_insert_data);

        return $order_id;

    }






}
