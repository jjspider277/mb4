<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/26/14
 * Time: 6:52 PM
 * www.cyteck.com
 */

class Invite_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   // protected $return_type = 'array';
    public $belongs_to = array( 'profile' => array( 'primary_key' => 'profile_id' ,'model'=>'profile_model'));



   public $has_many = array(
                              'senders' => array( 'primary_key' => 'sender_profile_id','model'=>"profile_model"),                            
                              'recivers' => array( 'primary_key' => 'reciver_profile_id','model'=>"profile_model"),
                              'notifications' => array( 'primary_key' => 'message_id','model'=>"notification_model"),                                                   
    );
    


   public $before_create = array( 'timestamps' );
   public $before_update= array( 'change_update_date' );
   public $before_delete= array( 'change_update_date' );


   public function _order_by_created_date() {
        $this->db->order_by('invites.created_date', 'desc');
        return $this;
    }

     public function _order_by_updated_date() {
        $this->db->order_by('invites.updated_date', 'desc');
        return $this;
    }
 


    protected function change_update_date($invites_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $invites_row['updated_date'] = date('Y-m-d H:i:s');
        return  $invites_row;
    }

   protected function timestamps($invites_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $invites_row['created_date'] = date('Y-m-d H:i:s');
        //$invites_row['updated_date']= date('Y-m-d H:i:s');
        return  $invites_row;
    }

    /**
     * @param $profile_id
     * @param $postdata / insert data
     */
    public function save_invite_for_profile($profile_id,$email){
       
        $insert_data = array(
            'to_email'=>$email,
            'profile_id'=>$profile_id,
            'status'=>intval(true), //not yet decied on what to do on status
        );

        $insert_id = $this->insert($insert_data);

        return $insert_id ;

    }

    public function filter_sent_emails_out($profile_id,$email) {

      $query = array('profile_id'=>$profile_id,'to_email'=>$email);
      $exisit_email = $this->get_by($query);
      
      if($exisit_email!=null) {
        return true;
      }
      return false;

     }

     public function get_inviated_user_by_email($email) {

      $query = array('to_email'=>$email);
      $result_array = $this->get_by($query);
      
      if($result_array!=null) {
        return $result_array;
      }
      return false;

     }
  

} 