<?php

class Bid_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }


    /**
     * @param $profile_id
     * @param $postdata / insert data
     */
    public function save_bid($bid){
        $insert_id = $this->insert($bid);
        return $insert_id ;
    }

    public function get_bid_by_id($id){
      $query = array('id'=>$id);
      $result = $this->get_by($query);
      if($result!=null) {
        return $result;
      }
      return false;
     }

    public function get_bids($auction_id){
      $query = array('auction_id'=>$auction_id);
      $result_array = $this->get_many_by($query);
      if($result_array!=null) {
        return $result_array;
      }
      return false;
     }

     public function get_highest_bid($auction)
     {

          $this->db->select('*');
          $this->db->from('bids');
          $this->db->where('bids.auction_id', $auction->id);
          $this->db->where('bids.created_at <=', $auction->end_date);
          $this->db->group_by('created_at desc');
          
          $query = $this->db->get();
          $result = $query->result();
          if($result && isset($result[0]))
            return (array) $result[0];

          return false;
     }

     // public function get_auction_by_product($product_id) {
     //  $query = array('product_id'=>$product_id);
     //  $result_array = $this->get_by($query);
      
     //  if($result_array!=null) {
     //    return $result_array;
     //  }
     //  return false;

     // }
  

} 