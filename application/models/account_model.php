<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/26/14
 * Time: 6:52 PM
 */

class Account_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   // protected $return_type = 'array';
    public $belongs_to = array( 'profile' => array( 'primary_key' => 'profile_id' ,'model'=>'profile_model'));

    /**
     * @param $profile_id
     * @param $post
     */
    public function save_account_profile($profile_id,$store_id,$post){

        $account_type = $post['account_type'];
        $bank_branch = $post['bankbranch'];
        $route_number= $post['routenumber'];
        $account_number = $post['accountnumber'];
        $account_owner = $post['account_owner'];

        $insert_data = array(
            'account_type'=>$account_type,
            'account_owner'=>$account_owner,
            'account_number'=>$account_number,
            'routing_number'=>$route_number,
            'account_owner'=>$account_owner,
            'bank_branch'=>$bank_branch,
            'profile_id'=>$profile_id,
            'store_id'=>$store_id,
        );

        $account_id = $this->insert($insert_data);

        return $account_id ;

    }

    public function update_get_paid_info($profile_id,$post,$store_id){

         $account_id = -1;
         $account_type = $post['account_type'];
         $bankbranch = $post['bankbranch'];
         $account_owner = $post['account_owner'];
         $routenumber = $post['routenumber'];
         $accountnumber = $post['accountnumber'];

        $account_exists = $this->get_by(array('store_id'=>$store_id));

        if(null != $account_exists) {

            $update_data = array(
                'account_type' => $account_type,
                'account_owner' => $account_owner,
                'account_number' => $accountnumber,
                'routing_number' => $routenumber,
                'account_owner' => $account_owner,
                'bank_branch' => $bankbranch,
                'profile_id' => $profile_id,

            );

            $update_where_data = array('profile_id' => $profile_id, 'store_id' => $store_id);
            //$this->output->enable_profiler(true);
            $account_id = $this->update_by($update_where_data, $update_data);

        }
        else {

            $account_id =$this->save_account_profile($profile_id,$store_id,$post);

        }
        return $account_id ;

    }
  
} 