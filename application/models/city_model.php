<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */

class City_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

    // protected $return_type = 'array';
   // public $belongs_to = array( 'profile' => array( 'primary_key' => 'state' ,'model'=>'state_model'));

    public function populate_city_dropdown(){

        $populated_cities =  $this->as_array()->get_all();

        $cities = array();

        foreach($populated_cities as $populated_city){
            $cities[$populated_city['city']] = $populated_city['city'];
        }
        //dump($cities);exit;

        return $cities;
    }

}