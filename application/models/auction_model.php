<?php

class Auction_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }


    /**
     * @param $profile_id
     * @param $postdata / insert data
     */
    public function save_auction($auction){
        $insert_id = $this->insert($auction);
        return $insert_id ;
    }

    public function update_current_bid($id, $price){
      return $this->update($id, array(
        'current_bid' => $price,
      ));
    }

    public function close_auction($id){
      return $this->update($id, array(
        'status' => false,
      ));
    }

    public function update_auction($id, $auction){
      return $this->update($id, $auction);
    }

    public function update_status($id, $status){
      return $this->update($id, array(
          'status' => $status,
        )
      );
    }
    public function get_auction_by_id($id) {
        //dump($id);
      $this->db->select('auctions.*, count(bids.id) as bids');
      $this->db->from('auctions');
      $this->db->join('products', 'products.id = auctions.product_id');
      $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
      $this->db->join('profiles', 'profiles.id = products.profile_id');
      $this->db->where('profiles.is_profile_verified', intval(true));
      $this->db->where('auctions.id', $id);

      $this->db->group_by('auctions.id');
      $query = $this->db->get();
      $auction = $query->result();
      // var_dump($auction);die;
        //dump($auction);
      // var_dump($auction);die;
      // $query = array('product_id'=>$product_id);
      // $result_array = $this->get_by($query);
      //var_dump($auction[0]->id);die;
       // $this->db->from('bids');

       // $this->db->where('auction_id', $auction[0]->id );
       // $query2 = $this->db->get();


        //$bid_counter = count($query2->result());$product_details = array();
        //$product_details[$key]['product_id'] = $auction
        //$auction [0] = $bid_counter;
      if($auction[0]) {
        return $auction[0];
      }
      return false;

     }
     public function get_auction_by_product($product_id) {

         $this->db->select('auctions.*, count(bids.id) as bids');
      $this->db->from('auctions');
      $this->db->join('products', 'products.id = auctions.product_id');
      $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
      // $this->db->join('bids', 'bids.auction_id = auctions.id');
      $this->db->join('profiles', 'profiles.id = products.profile_id');
      $this->db->where('profiles.is_profile_verified', intval(true));

      $this->db->group_by('auctions.id');
      $query = $this->db->get();
      $auction = $query->result();
      // var_dump($auction);die;
      // $query = array('product_id'=>$product_id);
      // $result_array = $this->get_by($query);
         var_dump($auction->auction_id);die;
         $product_details = array();
         $product_details[$key]['auction_id'] = $auction->auction_id;
         $this->db->from('bids');

         $this->db->where('auction_id', $product_details[$key]['auction_id'] );
         $query2 = $this->db->get();


         $bid_counter = count($query2->result());
      
      if($auction[0]) {
        return $auction[0];
      }

      return false;

     }

     public function get_expired_auctions() {
      date_default_timezone_set('America/Los_Angeles');
      /* 
      The old way of accessing the products but generates lot of query
      $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
      */
      
      $this->db->select('*');
      $this->db->from('auctions');
      $now_date = date('Y-m-d H:i:s');
      $this->db->where('auctions.end_date <=', $now_date);
      $this->db->where('auctions.status', true);
      
      $query = $this->db->get();
      $result = $query->result();

      return (array) $result;

    }

    public function get_all_auctions_for_admin($per_page,$page) {

        $this->db->select('*, count(bids.id) as total_bids');
        $this->db->from('auctions');
        $this->db->join('products', 'products.id = auctions.product_id');
        $this->db->join('medias', 'medias.product_id = products.id');
        $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));

        $this->db->group_by('auctions.id');
        $query = $this->db->get();
        $auctions = $query->result();

        //dump($auction);exit;
        return $auctions;
    }

    public function  get_all_auctions($profile_id) {

        $this->db->select('*, auctions.name as auction_name ,max(bids.price) as current_bid_price,profiles.id as profile_id , bids.id as bid_id , auctions.id as auction_id,products.id as product_id,count(bids.id) as total_bids');
        $this->db->from('auctions');
        $this->db->join('products', 'products.id = auctions.product_id');
        $this->db->join('medias', 'medias.product_id = products.id');
        $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where('profiles.id', $profile_id);
        $this->db->group_by('auctions.id');
        $this->db->order_by('auctions.end_date','dec');
        $query = $this->db->get();
        $auctions = $query->result();

        //dump($auction);exit;
        return $auctions;
    }



    //get all stores for admin
    public function getEditAuctionData($auction_id) {


        $this->db->select('*, auctions.name as auction_name ,max(bids.price) as current_bid_price,profiles.id as profile_id , bids.id as bid_id , auctions.id as auction_id,products.id as product_id,count(bids.id) as total_bids');
        $this->db->from('auctions');
        $this->db->join('products', 'products.id = auctions.product_id');
        $this->db->join('bids', 'bids.auction_id = auctions.id', 'left');
        $this->db->join('profiles', 'profiles.id = products.profile_id');
        $this->db->where('profiles.is_profile_verified', intval(true));
        $this->db->where('auctions.id', $auction_id);
        $this->db->group_by('auctions.id');
        $this->db->order_by('auctions.end_date','dec');
        $query = $this->db->get();
        $product_admin_data = $query->result();


        //dump($product_admin_data);exit;

        $product_admin_data_new =  array();

        if(!empty($product_admin_data) ) {

            foreach ($product_admin_data as $key => $product_result_object) {
                //append product_data_for_edit_store

                $product_admin_data_new[$key] = new stdClass();
                $this->load->model('media_model', 'medias_products');
                $product_medias = $this->medias_products->get_many_by(array('type' => 'product_image', 'product_id' => $product_result_object->product_id));
                $product_admin_data_new[$key]->media_products = array();
                foreach ($product_medias as $keys => $product_media) {

                    if ($product_media->file_name != 'image-upload.jpg') {

                        $product_admin_data_new[$key]->media_products[$keys] = base_url('uploads/profile/' .
                            $product_result_object->profile_id . '/products/' . $product_media->file_name);
                    } else {

                        $product_admin_data_new[$key]->media_products[$keys]  = base_url('assets/images/image-upload.jpg');
                    }

                }

                //dump($store_admin_data_new[$key]->product->quantity);exit;
                //get paid information
                $product_admin_data_new[$key]->auction = new stdClass();

                //dump($get_paid_info);exit;
                $product_admin_data_new[$key]->auction->auction_id = @$product_result_object->auction_id;
                $product_admin_data_new[$key]->auction->name = @$product_result_object->auction_name;
                $product_admin_data_new[$key]->auction->starting_price = @$product_result_object->bid_price;
                $product_admin_data_new[$key]->auction->reserve_price = @$product_result_object->reserve_price;
                $product_admin_data_new[$key]->auction->start_date = @$product_result_object->start_date;
                $product_admin_data_new[$key]->auction->start_time = @$product_result_object->start_time;
                $product_admin_data_new[$key]->auction->end_date = @$product_result_object->end_date;
                $product_admin_data_new[$key]->auction->end_time = @$product_result_object->end_time;
                $product_admin_data_new[$key]->auction->status = @$product_result_object->status;
                $product_admin_data_new[$key]->auction->current_bid = @$product_result_object->current_bid;
                $product_admin_data_new[$key]->auction->product_id = @$product_result_object->product_id;

                $this->load->model('bid_model', 'bid');
                $product_admin_data_new[$key]->bid = new stdClass();
                $product_admin_data_new[$key]->bid->current_bid_count = $this->bid->count_by(array('auction_id' => @$product_result_object->auction_id));
                //$product_admin_data_new[$key]->bid->minimum_amount = $this->bid->get_by(array('min(price)'));
                //dump($product_admin_data_new);
            }
        }
        return $product_admin_data_new;

    }

  

} 