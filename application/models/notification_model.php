<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 07/20/15
 * Last Updated:15/10/15
 * Time: 5:02 PM
 *  Craig Robinson : One World Solutions LLC
 */

class Notification_model extends My_Model {

    //NOTIFICATION TYPES as STATIC VARIBLES
    /*
    see constants.php :)
    const  MESSAGE = 'MESSAGE';
    const  LIKE = 'LIKE';
    const  FRIEND_REQUEST = 'FRIEND_REQUEST';
    const  UNREAD = 'UNREAD';
    const  SEEN = 'SEEN';*/

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   public $belongs_to =  array( 'friend' => array('primary_key' => 'id', 'model' => 'friend_model' ),
                                'message' => array('primary_key' => 'id', 'model' => 'message_model'),
                                'profile' => array('primary_key' => 'id', 'model' => 'profile_model'),                           
                          );

    
   public $before_create = array('timestamps');

   public function _order_by_created_date() {
        $this->db->order_by('notifications.created_date', 'desc');
        return $this;
    }
 

   protected function timestamps($notification_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $notification_row['created_date'] = date('Y-m-d H:i:s');
        $notification_rownotification_row['notification_status']= UNREAD;
        return  $notification_row;
    }


   /*public function push_notifications($data,$src,$is_urgent,$is_broadcast) {
    //TODO:save_message
    $saved_notification_id = $this->insert(
        array(
             'for_profile_id' =>$data['for_profile_id'] ,            
             'notification_text' =>$data['notification_text'] ,
             'message_source' =>$data['message_source'] ,
             'status'=>'UNREAD',
         )
      );

   

     if (!$saved_notification_id) {
      return -1;
     }

     return  $saved_notification_id;
   
 }*/


  public function count_messages_notification($for_profile_id) {
      $this->db->select('count(id) as messages');
      $this->db->from('notifications');
      $this->db->where('is_notified', intval(false));
      $this->db->where('notification_status',UNREAD);
      $this->db->where('notification_for', MESSAGE);
      $this->db->where('notifications.for_profile_id', $for_profile_id);
      $query = $this->db->get();
      $count_messages_notification = $query->row();
      return $count_messages_notification->messages;
  }  

  public function count_total_notification($for_profile_id) {

 

  }

 public function count_chat_notification($for_profile_id) {

 

  }

  public function count_likes_notification($for_profile_id) {

      $this->db->select('count(id) as likes');
      $this->db->from('notifications');
      $this->db->where('is_notified', intval(false));
      $this->db->where('notification_status', UNREAD);
      $this->db->where('notification_for', LIKE);
      $this->db->where('notifications.for_profile_id', $for_profile_id);
      $query = $this->db->get();
      $count_friend_notification = $query->row();
     //var_dump($count_friend_notification->friend_request);exit;
      return $count_friend_notification->likes;
      

  }

   public function count_freinds_request_notification($for_profile_id) {
      
      $this->db->select('count(id) as friend_request');
      $this->db->from('notifications');
      $this->db->where('is_notified', intval(false));
      $this->db->where('notification_status', UNREAD);
      $this->db->where('notification_for', FRIEND_REQUEST);
      $this->db->where('notifications.for_profile_id', $for_profile_id);
      $query = $this->db->get();
      $count_friend_notification = $query->row();
      //var_dump($count_friend_notification->friend_request);exit;
      return $count_friend_notification->friend_request;
      

  }

    public function list_friend_request_notification($for_profile_id) {

        $this->db->select(' *, reciever_media.file_name as reciver_image,sender_media.file_name as sender_image');
        $this->db->from('notifications');
        $this->db->join('friends','friends.id = notifications.friend_id');
        $this->db->join('profiles reciever_profile', 'reciever_profile.id = friends.reciever_profile_id');
        $this->db->join('profiles sender_profile', 'sender_profile.id = friends.sender_profile_id');
        $this->db->join('medias reciever_media', 'reciever_media.profile_id = reciever_profile.id');
        $this->db->join('medias sender_media', 'sender_media.profile_id = sender_profile.id');
        $this->db->where('is_notified', intval(false));
        $this->db->where('notification_status', UNREAD);
        $this->db->where('notification_for', FRIEND_REQUEST);
        $this->db->where('notifications.for_profile_id', $for_profile_id);
        $this->db->group_by('notifications.id');
        $this->db->order_by('notifications.created_date');
        $query = $this->db->get();
        $notifications = $query->result();

       // dump($notifications);exit;
        //ToDO:Construct array of products with media and p
        //profile details as array of objects and pass to the view simpley
      /*
        $notification_object = array();

        foreach ($notifications as $key => $notification_object) {


            $notification_object[$key]['id'] = $notification_object->product_id;
            $notification_object[$key]['notification_text'] = $notification_object->name;
            $notification_object[$key]['for_profile_id'] = $notification_object->pdescription;
            $notification_object[$key]['notification_status'] = $notification_object->notification_status;
            $notification_object[$key]['sprice'] = $notification_object->sprice;
            $notification_object[$key]['product_details'] = $notification_object->product_details;

            //TODO:fetch already calculated avg_rating

            $product_details[$key]['product_rating'] = !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating;
            $product_details[$key]['seller_rating'] = !isset($product_object->avg_rating)?0:$product_object->avg_rating;

            $product_details[$key]['profile_id'] = $product_object->profile_id;

            //TODO:When there are many products on the database this code need to be fixed or properly adjusted
            if(!empty($product_object->media_id)) {
                $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
            } else  {
                $product_details[$key]['image'] ='';
            }

            $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
*/
    return $notifications;

    }

    public function list_message_notification($for_profile_id)
    {

        $this->db->select(' *, reciever_media.file_name as reciver_image,sender_media.file_name as sender_image');
        $this->db->from('notifications');
        $this->db->join('messages', 'messages.id = notifications.message_id');
        $this->db->join('profiles reciever_profile', 'reciever_profile.id = messages.reciever_profile_id');
        $this->db->join('profiles sender_profile', 'sender_profile.id = messages.sender_profile_id');
        $this->db->join('medias reciever_media', 'reciever_media.profile_id = reciever_profile.id');
        $this->db->join('medias sender_media', 'sender_media.profile_id = sender_profile.id');
        $this->db->where('is_notified', intval(false));
        $this->db->where('notification_status', UNREAD);
        $this->db->where('notification_for', MESSAGE);
        $this->db->where('notifications.for_profile_id', $for_profile_id);
        $this->db->group_by('notifications.id');
        $this->db->order_by('notifications.created_date');
        $query = $this->db->get();
        $notifications = $query->result();
        return $notifications;
    }

    public function list_likes_notification($for_profile_id)
    {

        $this->db->select(' *,products_media.file_name as product_images, reciever_media.file_name as reciver_image,sender_media.file_name as sender_image');
        $this->db->from('likes');
        $this->db->join('products', 'products.id = likes.like_product_id');
        $this->db->join('medias products_media', 'products_media.product_id = likes.like_product_id');
        $this->db->join('profiles sender_profile', 'sender_profile.id = likes.like_profile_id');
        $this->db->join('profiles reciever_profile', 'reciever_profile.id = likes.liked_for_profile_id');
        $this->db->join('medias reciever_media', 'reciever_media.profile_id = reciever_profile.id');
        $this->db->join('medias sender_media', 'sender_media.profile_id = sender_profile.id');
        $this->db->where('likes.liked_for_profile_id', $for_profile_id);
        $this->db->group_by('likes.id');
        $this->db->order_by('likes.like_date');
        $query = $this->db->get();
        $like_notifications = $query->result();
        //dump($like_notifications);exit;
        return $like_notifications;
    }



    public function get_all_notifications($for_profile_id) {
         
      $notifications= (object) $this->_order_by_created_date()->get_many_by(array('for_profile_id' =>$for_profile_id ,'notification_status'=>'UNREAD','is_notified'=>intval(false)));
      //dump($notifications);exit;
      if(count($notifications)>0)
      return $notifications;
      return -1; //error
  }

    public function count_all_notifications($for_profile_id) {
         
      $notifications= $this->get_many_by(array('for_profile_id' =>$for_profile_id ,'notification_status'=>'UNREAD','is_notified'=>intval(false)));
     //var_dump($notifications);exit;
      
      return count($notifications);
  }

  //to do friend id more paramters to be taken outside
  public function push_notifications($notification_type,$message_id,$friend_id,$for_profile_id,$notification_text) {

        $insert_data = array(
            'for_profile_id' =>$for_profile_id ,
            'friend_id'=>$friend_id,
            'notification_for'=>$notification_type,
            'message_id'=>$message_id,
            'notification_type'=>'WEB_SYETEM',
            'notification_text'=>$notification_text,
            'notification_status'=>UNREAD,
            'is_notified'=>intval(false),
            'is_broadcast'=>intval(false),
             );

       $saved_notification_id = $this->insert($insert_data);

       return  $saved_notification_id;
    }

 //to do merge with the above and add by profile and product id with tlationsips
  public function push_notifications_for_likes($notification_type,$for_profile_id,$notification_text,$liked_by_profile_id,$liked_product_id) {

        $insert_data = array(
            'for_profile_id' =>$for_profile_id ,
            'notification_for'=>$notification_type,
            'notification_type'=>'WEB_SYETEM',
            'notification_text'=>$notification_text,
            'notification_status'=>UNREAD,
            'is_notified'=>intval(false),
            'is_broadcast'=>intval(false),
            'liked_product_id'=>$liked_product_id,
            'liked_by_profile_id'=>$liked_by_profile_id,
            );

       $saved_notification_id = $this->insert($insert_data);

       return  $saved_notification_id;
    }


  //ahahh i die
  public function update_push_notification_status($friend_id,$message_id,$for_profile_id,$is_notified) {

      $update_data = array(
            'friend_id'=>$friend_id,
            'for_profile_id' =>$for_profile_id,   
            'notification_for'=>FRIEND_REQUEST,        
           );

      $where = $update_data;
      $data =  array('notification_status'=>'SEEN', 'is_notified'=>intval($is_notified));

      $updated_notification_id = $this->update_by($where,$data);
      return  $updated_notification_id;
    }


  function update_by($where = array(),$data=array())
  {
        $this->db->where($where);
        $query = $this->db->update($this->_table,$data);
        return $this->db->get($this->_table)->row()->id;
  }

  function update_by_where_not_in($column_list,$columns,$lists,$data) 
  {
   
        $this->db->where_not_in($column_list,$lists);
        $this->db->where($columns); 
        $query = $this->db->update($this->_table,$data);
        return $this->db->get($this->_table)->row()->id;
  }

  //update notifiction for community bar based ibn type

  public function update_notification_seen_status($ntype,$for_profile_id,$is_notified=true) {

     //DECODE NOTIFICATION TYPES
   
     if($ntype=='friend_request') 
     {
       $ntype ='FRIEND_REQUEST';
     }
     if($ntype=='messages') 
     {
       $ntype ='MESSAGE';
     }
     if($ntype=='likes') 
     {
       $ntype ='LIKE';
     }

     $updated_notification_id = -1; //we assume by default everything goes wrong ,pretty cautious
     //var_dump($ntype);
     //$this->output->enable_profiler(TRUE);
      if($ntype=='notification')
      {
          $lists = array('MESSSAGE','LIKE','FRIEND_REQUEST');
          $columns = array(
                'for_profile_id' =>$for_profile_id,       
               );
          $update_data =  array('is_notified'=>intval($is_notified));
          $column_list='notification_for';
          
          $updated_notification_id = $this->update_by_where_not_in($column_list,$columns,$lists,$update_data);
          //var_dump($updated_notification_id);
      }
      else {
         $update_data = array(
                'for_profile_id' =>$for_profile_id,   
                'notification_for'=>'$ntype',        
               );

          $where = $update_data;
          $data =  array('is_notified'=>intval($is_notified));

          $updated_notification_id = $this->update_by($where,$data);
      }
    
      return  $updated_notification_id;
    }


      



  
} 