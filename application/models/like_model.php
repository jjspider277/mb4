<?php
/**
 * Created by Danile Adenew.
 * User: Craig Robinson
 * Date: 14/10/15
 * Time: 4:52 am
 */

class Like_Model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

   // protected $return_type = 'array';
    public $belongs_to = array('profile' => array( 'primary_key' => 'like_profile_id' ,'model'=>'profile_model'),
                               'product' => array( 'primary_key' => 'like_product_id' ,'model'=>'product_model'));

   public $before_create = array('timestamps');
   public $before_update= array('change_update_date');
   public $before_delete= array('change_update_date');

    protected function timestamps($table)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $table['created_date'] = date('Y-m-d H:i:s');
        $table['like_date'] = date('Y-m-d H:i:s');
        return $table;
    }



   public function _order_by_created_date() {
        $this->db->order_by('likes.created_date', 'desc');
        return $this;
    }

     public function _order_by_updated_date() {
        $this->db->order_by('likes.updated_date', 'desc');
        return $this;
    }
 


    protected function change_update_date($likes_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $likes_row['updated_date'] = date('Y-m-d H:i:s');
        return  $likes_row;
    }

    public function check_if_liked_before($product_id,$profile_id){

        $result = $this->get_by(array('like_product_id'=>$product_id,'like_profile_id'=>$profile_id));

        if($result) {
            return true;
        }
        return false;
    }



    /**
     * @param $profile_id
     * @param $post
     */
    public function save_product_liking($product_id,$profile_id,$for_profile_id){

        $like_product_id = $product_id;
        $like_profile_id = $profile_id;
        $for_profile_id = $for_profile_id;

        //give it product ,profile as params
        if(!$this->check_if_liked_before($product_id,$profile_id)) { 
        //if not true
        $insert_data = array(
            'like_profile_id'=>$like_profile_id,
            'like_product_id'=>$like_product_id,
            'liked_for_profile_id'=>$for_profile_id,
        );

        $likes_id = $this->insert($insert_data);        
        return isset($likes_id) ? $likes_id : false;
       }

       return false;
    }

     public function get_products_i_liked($profile_id) {

    
    // public function get_all_seller_product_lisiting ($profile_id,$per_page, $page) {

      /* 
      The old way of accessing the products but generates lot of query
      $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
      */
      
      //get the profile id of the product but no need :P

      $this->db->distinct();
      $this->db->select('* ,products.avg_rating as p_avg_rating, products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
      $this->db->from('products');
      $this->db->join('medias', 'medias.product_id = products.id', 'left');
      $this->db->join('profiles', 'profiles.id = products.profile_id');
      //$this->db->where('profiles.is_profile_verified', intval(true));
      $this->db->where('products.id in ( select like_product_id  from likes where like_profile_id = '.$profile_id.')');
      $this->db->group_by('products.id');
      //$this->_order_by_created_date();
      //$this->db->limit($per_page, $page);
      $query = $this->db->get();
      $all_products = $query->result();

  
      //var_dump($all_products);exit;
   
      //ToDO:Construct array of products with media and p
      //profile details as array of objects and pass to the view simpley

      $product_details = array();      

      foreach ($all_products as $key => $product_object) {
  
        $product_details[$key]['product_id'] = $product_object->product_id;
        $product_details[$key]['name'] = $product_object->name;
        $product_details[$key]['desc'] = $product_object->pdescription;
        $product_details[$key]['price'] = $product_object->price;
        $product_details[$key]['sprice'] = $product_object->sprice;
        $product_details[$key]['product_details'] = $product_object->product_details;

        //TODO:some calcualtions
        $product_details[$key]['product_rating'] =  !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating; ;

        $product_details[$key]['profile_id'] = $product_object->profile_id;

              //TODO:When there are many products on the database this code need to be fixed or properly adjusted
              if(!empty($product_object->media_id)) {
                    $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
              } else  {
                    $product_details[$key]['image'] ='';
              } 

        $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
      }

        //var_dump($product_details);exit;
      return ((array)$product_details);

    }



    public function get_liked_products_profile_id($product_id) {
    
      $this->db->select('profile_id');
      $this->db->from('products');
      $this->db->where('products.id', $product_id);
      $query = $this->db->get();
      $profile_ids = $query->result();
      return $profile_id;

    }

    public function get_liked_count($profile_id) {
      $this->db->select('count(id) as liked');
      $this->db->from('likes');
      $this->db->where('liked_for_profile_id',$profile_id);
      $query = $this->db->get();
      $get_liked_count = $query->result();
      return   $get_liked_count[0]->liked;
    }

    public function get_liked_products($profile_id) {
    
     // public function get_all_seller_product_lisiting ($profile_id,$per_page, $page) {

      /* 
      The old way of accessing the products but generates lot of query
      $all_products = $this->limit($per_page, $page)->select('*')->with('profile')->get_all();
      */
      
      //get the profile id of the product but no need :P

      $this->db->distinct();
      $this->db->select('* ,products.avg_rating as p_avg_rating, products.desc as pdescription, medias.id as media_id,products.id as product_id , profiles.id as profile_id');
      $this->db->from('products');
      $this->db->join('medias', 'medias.product_id = products.id', 'left');
      $this->db->join('profiles', 'profiles.id = products.profile_id');
      //$this->db->where('profiles.is_profile_verified', intval(true));
      $this->db->where('products.id in ( select like_product_id  from likes ) ');
      $this->db->where('products.profile_id',$profile_id);
      $this->db->group_by('products.id');
      //$this->_order_by_created_date();
      //$this->db->limit($per_page, $page);
      $query = $this->db->get();
      $all_products = $query->result();

  
      //var_dump($all_products);exit;
   
      //ToDO:Construct array of products with media and p
      //profile details as array of objects and pass to the view simpley

      $product_details = array();      

      foreach ($all_products as $key => $product_object) {
  
        $product_details[$key]['product_id'] = $product_object->product_id;
        $product_details[$key]['name'] = $product_object->name;
        $product_details[$key]['desc'] = $product_object->pdescription;
        $product_details[$key]['price'] = $product_object->price;
        $product_details[$key]['sprice'] = $product_object->sprice;
        $product_details[$key]['product_details'] = $product_object->product_details;

        //TODO:some calcualtions
        $product_details[$key]['product_rating'] =  !isset($product_object->p_avg_rating)?0:$product_object->p_avg_rating; ;

        $product_details[$key]['profile_id'] = $product_object->profile_id;

              //TODO:When there are many products on the database this code need to be fixed or properly adjusted
              if(!empty($product_object->media_id)) {
                    $product_details[$key]['image'] = base_url().'/uploads/profile/'.$product_object->profile_id.'/products/'.$product_object->file_name ; //get only the first product image
              } else  {
                    $product_details[$key]['image'] ='';
              } 

        $product_details[$key]['seller_name'] = (ucfirst($product_object->fname));
      

        //var_dump($product_details);exit;
      return ((array)$product_details);

    }


    }
  
} 