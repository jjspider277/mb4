<?php
/**
 * Created by dkf4199.
 * Date: 07/20/2015
 */

class Orderdetail_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }
	
	public $before_create = array( 'timestamps' );

    protected function timestamps($order_detail)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');

        ///TODO:add status to configuaration file
        $order_detail['created_date'] = date('Y-m-d H:i:s');
        $order_detail['payment_status'] = 'Pending';
        $order_detail['shipping_status'] = 'Processing';
        $order_detail['item_shipped'] = 'N';
        $order_detail['buyer_delivery_confirmed'] = 'N';
        return  $order_detail;
    }

    public $_table = 'order_detail'; //give the the table  name  if hard to guess

    protected $primary_key = 'od_id'; //thanks to you deron for messing up the project :())=== 
	
	
	public function insert_order_detail($oid,$items) {
			
		//Insert Each Order Detail Line
		//foreach($this->cart->contents() as $items) {
			
			$order_detail_data = array(
				'order_id' => $oid,
				'seller_id' => $items['seller_profile'],
				'product_id' => $items['id'],
				'quantity' => $items['qty'],
				'shipping_cost' => $items['shipping_price'],
				'price' => $items['price']
			);
			
			$order_detail_id = $this->insert($order_detail_data);
			
		//}//
		
	}


	public function update_tracking_shipping_info($order_detail_id,$tracking_number,$shipping_cost,$shipping_method) {
         
        //default shipping method is FEDEX
        $data = array('tracking_number'=>$tracking_number,'shipping_cost'=>$shipping_cost,'shipping_method'=>$shipping_method,'shipping_status'=>'Shipped','item_shipped'=>'Y');
		//first find the order detail info by id	
		$result = $this->update_by(array('od_id'=>$order_detail_id),$data);

		if($result) {
			return true;
		}
		return false;
		
		
	}

	//accept update data of array with order id 
	//update orders table this used for updating shipping address of buyer currently

	public function update_buyer_satisfaction($order_detail_id) {

		//ORDER  details table update
		$order_detail_id = $order_detail_id;

		$order_detail_update_data = array(
				'buyer_delivery_confirmed' => 'Y',
				'updated_date'=>date('Y-m-d H:i:s'),	
			    'order_detail.item_shipped'=>'Y',						
			);

		$result = $this->update($order_detail_id,$order_detail_update_data);
		
		if($result)
			return true;
		else 
			return false;

	}


}
