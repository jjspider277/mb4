<?php

class Auction_winner_model extends My_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }


    /**
     * @param $profile_id
     * @param $postdata / insert data
     */
    public function save_winner($winner){
        $insert_id = $this->insert($winner);
        return $insert_id ;
    }

    public function expire_win($id)
    {
        return $this->update($id, array(
            'status' => 'Expired',
          ));
    }

    public function claim_win($id)
    {
        return $this->update($id, array(
            'status' => 'Claimed',
          ));
    }

    public function get_won_auctions($profile_id=null){
        $query = array();
        if(null !== $profile_id){
            $query = array(
            'profile_id' => $profile_id,
            'status' => 'Waiting',
            );
        } else {
            $query = array(
            'status' => 'Waiting',
            );
        }
      
      $result_array = $this->get_many_by($query);
      
      if($result_array!=null) {
        return $result_array;
      }
      return false;
     }

     // public function get_highest_bid($auction)
     // {

     //      $this->db->select('*');
     //      $this->db->from('bids');
     //      $this->db->where('bids.auction_id', $auction->id);
     //      $this->db->where('bids.created_at <=', $auction->end_date);
     //      $this->db->group_by('created_at desc');
          
     //      $query = $this->db->get();
     //      $result = $query->result();
     //      if($result && isset($result[0]))
     //        return (array) $result[0];

     //      return false;
     // }

     // public function get_auction_by_product($product_id) {
     //  $query = array('product_id'=>$product_id);
     //  $result_array = $this->get_by($query);
      
     //  if($result_array!=null) {
     //    return $result_array;
     //  }
     //  return false;

     // }
  

} 