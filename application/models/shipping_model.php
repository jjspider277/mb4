<?php
/**
 * Created by Daniel Adenew.
 * User: Daniel Adenew
 * Date: 12/26/14
 * Time: 1:14 PM
 * Craig Robinson : One World Solutions LLC
 */


class Shipping_model extends MY_Model {

    public function __construct() {
        parent::__construct() ;
        $this->_database = $this->db;
    }

    public $before_create = array( 'timestamps' );

   protected function timestamps($profile_row)
    {
        //$table['created_date'] = $table['updated_date'] = date('Y-m-d H:i:s');
        $profile_row['created_date'] = date('Y-m-d H:i:s');
        return  $profile_row;
    }

    public $_table = 'shipping_addresses'; //giv the the table  name  if hard to guess

    /**
     * Define relationship a memeber
     * have One to One , One to Many ,
     * Many to Many if any
     */

    public $belongs_to = array( 'profile' => array( 'primary_key' => 'shipping_id' ,'model'=>"profile_model"),                                                           
                                );
    


  public function save_shipping_for_seller_profile($data,$profile_id) {

    $insert_data = array( "profile_id"=>$profile_id,
                          "shipping_address_line_1" => $data['address_line_1'] ,
                          "shipping_address_line_2" =>$data['address_line_2'] ,
                          "shipto_city"=>$data['city'],
                          "shipto_state"=>$data['state'],
                          "shipto_zip"=>$data['zip_code'],
                          );

    $inserted_shipping_id = $this->insert($insert_data);

    return $inserted_shipping_id ? $inserted_shipping_id : -1;
 }
   
    public function get_shipping_address($pid){
		
		$sql = "SELECT shipping_address_line_1,
						shipping_address_line_2, 
						shipto_city, 
						shipto_state, 
						shipto_zip
				FROM shipping_addresses 
				WHERE profile_id = ".$pid." ORDER BY `id` DESC LIMIT 1";
		$rs = $this->db->query($sql, array($pid));
			
		return $rs->result_array();
		
	}
	
	public function get_buyer_email($buyer_profile_id){
		
		$sql = "SELECT b.email as buyermail 
				FROM profiles a 
				INNER JOIN users b ON a.user_id = b.id 
				WHERE a.id = ? LIMIT 1";
		$rs = $this->db->query($sql, array($buyer_profile_id));
		
		if ($rs->num_rows() > 0) {
		   $row = $rs->row(); 
		   return $row->buyermail;
		} else {
			return "notfound";
		}
	
	}
	
	// Seller Email in Users table.
	public function get_seller_email($seller_profile_id){
	
		$sql = "SELECT b.email as sellermail 
				FROM profiles a 
				INNER JOIN users b ON a.user_id = b.id 
				WHERE a.id = ? LIMIT 1";
		$rs = $this->db->query($sql, array($seller_profile_id));
		/*foreach ($rs->result_array() as $row){
			return $row['sellermail'];
		}
		*/
		if ($rs->num_rows() > 0) {
		   $row = $rs->row(); 
		   return $row->sellermail;
		} else {
			return "notfound";
		}
		
	}
	
}
