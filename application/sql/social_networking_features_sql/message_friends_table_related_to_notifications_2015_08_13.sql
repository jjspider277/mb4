---friends table shoud have notification

ALTER TABLE `mbu4u`.`friends` ADD COLUMN `notification_id` INT NULL  AFTER `seen_status` , 

  ADD CONSTRAINT `fk_friends_notifications`

  FOREIGN KEY (`notification_id` )

  REFERENCES `mbu4u`.`notifications` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_friends_notifications` (`notification_id` ASC) ;


-- message notifications

ALTER TABLE `mbu4u`.`messages` ADD COLUMN `notification_id` INT NULL  AFTER `updated_date` , 

  ADD CONSTRAINT `fk_notification_id`

  FOREIGN KEY (`notification_id` )

  REFERENCES `mbu4u`.`notifications` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_notification_id` (`notification_id` ASC) ;

