
-- create invite table

CREATE TABLE `mbu4u`.`invites` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `profile_id` INT NULL COMMENT '',
  `emails` TEXT NULL COMMENT '',
  `created_date` DATETIME NULL COMMENT '',
  `updated_date` DATETIME NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_invite_profile_idx` (`profile_id` ASC)  COMMENT '',
  CONSTRAINT `fk_invite_profile`
    FOREIGN KEY (`profile_id`)
    REFERENCES `mbu4u`.`profiles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

--sql alter save emails per sent for better filteruing and spam filtering
ALTER TABLE `mbu4u``invites` 
CHANGE COLUMN `emails` `to_email` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;
