CREATE  TABLE `mbu4u`.`photos` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `profile_id` INT NULL ,

  `image_path` VARCHAR(45) NULL ,

  `file_name` VARCHAR(255) NULL ,

  `full_path` VARCHAR(255) NULL ,

  PRIMARY KEY (`id`) ,

  INDEX `fk_profile_photo` (`profile_id` ASC) );

