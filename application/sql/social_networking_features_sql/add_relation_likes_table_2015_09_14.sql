ALTER TABLE `mbu4u`.`likes` 
ADD INDEX `fk_like_profile_idx` (`like_profie_id` ASC)  COMMENT '',
ADD INDEX `fk_like_product_idx` (`like_product_id` ASC)  COMMENT '';
ALTER TABLE `mbu4u`.`likes` 
ADD CONSTRAINT `fk_like_profile`
  FOREIGN KEY (`like_profie_id`)
  REFERENCES `mbu4u`.`profiles` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_like_product`
  FOREIGN KEY (`like_product_id`)
  REFERENCES `mbu4u`.`products` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
