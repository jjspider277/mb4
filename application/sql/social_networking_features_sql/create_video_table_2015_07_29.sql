CREATE  TABLE `mbu4u`.`videos` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `profile_id` INT NULL ,

  `description` VARCHAR(45) NULL ,

  `created_date` VARCHAR(45) NULL ,

  `full_path` VARCHAR(45) NULL ,

  `file_name` VARCHAR(45) NULL ,

  `thubmnail_path` VARCHAR(45) NULL ,

  PRIMARY KEY (`id`) ,

  INDEX `fk_profile_id` (`profile_id` ASC) );

