-- query 1
CREATE  TABLE `mbu4u`.`messages` (

  `id` INT NULL ,

  `sender_profile_id` INT NULL ,

  `reciever_profile_id` INT NULL ,

  `message` TEXT NULL ,

  `message_status` VARCHAR(45) NULL ,

  `message_type` VARCHAR(45) NULL ,

  `created_date` DATETIME NULL ,

  PRIMARY KEY (`id`) ,

  INDEX `fk_sender_profile_id` (`sender_profile_id` ASC) ,

  INDEX `fk_reciver_profile_id` (`reciever_profile_id` ASC) );

-- query 2
ALTER TABLE `mbu4u`.`messages` ADD COLUMN `subject` VARCHAR(45) NULL  AFTER `reciever_profile_id` ;

--query 3
ALTER TABLE `mbu4u`.`messages` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT  ;

