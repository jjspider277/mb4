--friend--
delimiter $$

CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_profile_id` int(11) DEFAULT NULL,
  `reciever_profile_id` int(11) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `accept_date` datetime DEFAULT NULL,
  `friend_status` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `seen_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_friend_sender_profile_id` (`sender_profile_id`),
  KEY `fk_freinds_reciver_profile` (`reciever_profile_id`),
  CONSTRAINT `fk_freinds_reciver_profile` FOREIGN KEY (`reciever_profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_friend_sender_profile_id` FOREIGN KEY (`sender_profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1$$

select * from friends;