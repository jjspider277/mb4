CREATE  TABLE `mbu4u`.`notifications` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `notification_text` TEXT NULL ,

  `for_profile_id` INT NULL ,

  `status` VARCHAR(45) NULL ,

  `is_broadcast` INT NULL ,

  `mesasge_id` INT NULL COMMENT 'Incase notifaction store thier message in messages table then message id will be used to refer that message content' ,

  `message_source` VARCHAR(45) NULL DEFAULT 'system' ,

  `is_urgent` INT NULL COMMENT 'if priorty then first listing will be given to this notifaction types' ,

  `created_date` DATETIME NULL ,

  PRIMARY KEY (`id`) ,

  INDEX `fk_for_profile_notifications` (`for_profile_id` ASC) ,

  INDEX `fk_message_notification` (`mesasge_id` ASC) );

--alter
ALTER TABLE `mbu4u`.`notifications` ADD COLUMN `notification_for` VARCHAR(45) 
NULL  AFTER `mesasge_id` , ADD COLUMN `is_notified` INT NULL  AFTER `created_date` , 
CHANGE COLUMN `message_source` `notification_source` VARCHAR(45) NULL DEFAULT 'system'  ;

