CREATE TABLE `orders` (
  `o_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(25) NOT NULL,
  `anet_transid` varchar(25) DEFAULT NULL,
  `buyer_id` int(10) unsigned DEFAULT NULL,
  `order_total` decimal(10,2) unsigned DEFAULT NULL,
  `shipto_fname` varchar(45) DEFAULT NULL,
  `shipto_lname` varchar(45) DEFAULT NULL,
  `shipto_street` varchar(50) DEFAULT NULL,
  `shipto_street2` varchar(45) DEFAULT NULL,
  `shipto_city` varchar(45) DEFAULT NULL,
  `shipto_state` char(2) DEFAULT NULL,
  `shipto_zip` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`o_id`),
  KEY `fk_buyer_id_idx` (`buyer_id`),
  CONSTRAINT `fk_buyer_id` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
