
---
ALTER TABLE `mbu4u`.`order_detail` 
ADD COLUMN `payment_type` VARCHAR(45) NULL COMMENT '' AFTER `shipping_status`,
ADD COLUMN `shipping_method` VARCHAR(45) NULL COMMENT '' AFTER `payment_type`;
