ALTER TABLE `mbu4u`.`order_detail` 
ADD COLUMN `shipping_cost` FLOAT NULL COMMENT '' AFTER `shipping_method`,
ADD COLUMN `grand_total_payed` FLOAT NULL COMMENT '' AFTER `shipping_cost`;
