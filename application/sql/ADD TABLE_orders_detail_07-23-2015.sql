CREATE TABLE `order_detail` (
  `od_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) DEFAULT NULL,
  `seller_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `quantity` smallint(5) unsigned DEFAULT NULL,
  `price` float unsigned DEFAULT NULL,
  `item_shipped` char(1) NOT NULL DEFAULT 'N',
  `tracking_number` varchar(45) DEFAULT NULL,
  `buyer_delivery_confirmed` char(1) NOT NULL DEFAULT 'N',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`od_id`),
  UNIQUE KEY `od_id_UNIQUE` (`od_id`),
  KEY `fk_seller_id_idx` (`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
