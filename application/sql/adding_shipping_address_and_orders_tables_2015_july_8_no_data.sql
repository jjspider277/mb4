CREATE TABLE `shipping_addresses` (
  `sa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `shipto_street` varchar(50) DEFAULT NULL,
  `shipto_street2` varchar(45) DEFAULT NULL,
  `shipto_city` varchar(45) DEFAULT NULL,
  `shipto_state` char(2) DEFAULT NULL,
  `shipto_zip` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`sa_id`),
  UNIQUE KEY `sa_id_UNIQUE` (`sa_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `orders` (
  `o_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `anet_transid` varchar(25) DEFAULT NULL,
  `buyer_id` int(10) unsigned DEFAULT NULL,
  `order_total` decimal(10,2) unsigned DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  PRIMARY KEY (`o_id`),
  UNIQUE KEY `invoice_id_UNIQUE` (`invoice_id`),
  KEY `fk_buyer_id_idx` (`buyer_id`),
  CONSTRAINT `fk_buyer_id` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `order_detail` (
  `od_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `seller_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `quantity` smallint(5) unsigned DEFAULT NULL,
  `price` float unsigned DEFAULT NULL,
  `item_shipped` char(1) NOT NULL DEFAULT 'N',
  `tracking_number` varchar(45) DEFAULT NULL,
  `buyer_delivery_confirmed` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`od_id`),
  UNIQUE KEY `od_id_UNIQUE` (`od_id`),
  UNIQUE KEY `order_id_UNIQUE` (`order_id`),
  KEY `fk_seller_id_idx` (`seller_id`),
  CONSTRAINT `fk_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`o_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_seller_id` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
