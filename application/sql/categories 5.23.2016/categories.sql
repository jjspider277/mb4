-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2016 at 04:23 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mbu4u`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `update_date` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `description`, `parent_id`, `created_date`, `update_date`) VALUES
(25, 'Clothing & Accessories', NULL, NULL, '1463734667', ''),
(26, 'Jewlery', NULL, NULL, '1463734699', ''),
(27, 'Craft Supplies & Tools', NULL, NULL, '1463734721', ''),
(28, 'Weedings', NULL, NULL, '1463734726', ''),
(29, 'Entertainment', NULL, NULL, '1463734741', ''),
(30, 'Home & Living', NULL, NULL, '1463734752', ''),
(31, 'Kid & Baby', NULL, NULL, '1463734764', ''),
(32, 'Vinatage', NULL, NULL, '1463734769', ''),
(33, 'Accessories ', NULL, 25, '1463749037', ''),
(34, 'Clothing', NULL, 25, '1463749112', ''),
(35, 'Shoes', NULL, 25, '1463749947', ''),
(36, 'Belts & Suspenders', NULL, 33, '1463750630', ''),
(37, 'Black Belt', NULL, 36, '1463753207', ''),
(38, 'Hair Accessories', NULL, 33, '1463755433', ''),
(39, 'Rings', NULL, 26, '1463765292', ''),
(40, 'Diamond Rings', NULL, 39, '1463765323', ''),
(41, 'Toxido', NULL, 28, '1463771301', ''),
(42, 'Black Toxido ', NULL, 41, '1463771327', ''),
(43, 'Braclet ', NULL, 26, '1463815178', ''),
(44, 'Silver braclet', NULL, 43, '1463815258', ''),
(45, 'Gold ring', NULL, 39, '1463815286', ''),
(46, 'White texido', NULL, 41, '1463817582', ''),
(47, 'Body Jewlery', NULL, 26, '1463993602', ''),
(48, 'Cuff Link & Tie Clips', NULL, 26, '1463993625', ''),
(49, 'Earings', NULL, 26, '1463993645', ''),
(50, 'Necklaces', NULL, 26, '1463993673', ''),
(51, 'Bags & Purse', NULL, 25, '1463993768', ''),
(52, 'Fiber & Textile Supplies', NULL, 27, '1463993900', ''),
(53, 'Food Craft Supplies', NULL, 27, '1463993920', ''),
(54, 'General Supplies', NULL, 27, '1463993943', ''),
(55, 'Jewlery & Beading', NULL, 27, '1463993967', ''),
(56, 'Patterns & Tutorials', NULL, 27, '1463993991', ''),
(57, 'Accessories', NULL, 28, '1463994688', ''),
(58, 'Clothing', NULL, 28, '1463994703', ''),
(59, 'Decoration', NULL, 28, '1463994753', ''),
(60, 'Gift & Mementos', NULL, 28, '1463995137', ''),
(61, 'Invitations & Paper', NULL, 28, '1463995871', ''),
(62, 'Books, Movies & Music', NULL, 29, '1463996039', ''),
(63, 'Electronics & Accessories', NULL, 29, '1463996058', ''),
(64, 'Toys & Gamess', NULL, 29, '1463996073', ''),
(65, 'Art & Collectibles ', NULL, 30, '1464002681', ''),
(66, 'Bath & Beauty', NULL, 30, '1464002697', ''),
(67, 'Paper & Party Supplies', NULL, 30, '1464002738', ''),
(68, 'Pet Supplies', NULL, 30, '1464002749', ''),
(69, 'Baby & Child Care', NULL, 31, '1464002877', ''),
(70, 'Baby & Toddler Toys', NULL, 31, '1464002896', ''),
(71, 'Baby Accessories', NULL, 31, '1464002912', ''),
(72, 'Baby Blankets', NULL, 31, '1464002927', ''),
(73, 'Baby Boys'' Clothing', NULL, 31, '1464002956', ''),
(74, 'Baby Headbands', NULL, 31, '1464002971', ''),
(75, 'Accessories', NULL, 32, '1464003164', ''),
(76, 'Art & Collectibles ', NULL, 32, '1464003202', ''),
(77, 'Bags & Purses', NULL, 32, '1464003224', ''),
(78, 'Clothing', NULL, 32, '1464003238', ''),
(79, 'Crafts & Supplies', NULL, 32, '1464003258', ''),
(80, 'Home & Living', NULL, 32, '1464003294', ''),
(81, 'Jewlery', NULL, 32, '1464003309', ''),
(82, 'Toys & Games', NULL, 32, '1464003330', ''),
(83, 'Bag Packs', NULL, 51, '1464003507', ''),
(84, 'Diaper Bags', NULL, 51, '1464003541', ''),
(87, 'Boys'' Clothing', NULL, 34, '1464003679', ''),
(88, 'Girls'' Clothing', NULL, 34, '1464003711', ''),
(89, 'Womens'' Clothing', NULL, 34, '1464003736', ''),
(90, 'Boys'' Shoes', NULL, 35, '1464003760', ''),
(91, 'Girls'' Shoes', NULL, 35, '1464003767', ''),
(92, 'Mens'' Shoes', NULL, 35, '1464003775', ''),
(93, 'Womens'' Shoes', NULL, 35, '1464003781', ''),
(94, 'Anklets ', NULL, 47, '1464003824', ''),
(95, 'Arm Band', NULL, 47, '1464003836', ''),
(96, 'Barbells', NULL, 47, '1464003855', ''),
(97, 'Belly Chains', NULL, 47, '1464003873', ''),
(98, 'Belly Rings', NULL, 47, '1464003889', ''),
(99, 'Bangles ', NULL, 43, '1464003913', ''),
(100, 'Beaded Bracelets ', NULL, 43, '1464003965', ''),
(101, 'Chain & Link Bracelets ', NULL, 43, '1464004006', ''),
(102, 'Cuff Links', NULL, 48, '1464004040', ''),
(103, 'Shirt Studs', NULL, 48, '1464004055', ''),
(104, 'Tie Clips & Tacks ', NULL, 48, '1464004085', ''),
(105, 'Chandelier Earrings', NULL, 49, '1464004122', ''),
(106, 'Clip on Earrings', NULL, 49, '1464004142', ''),
(107, 'Cluster Earrings', NULL, 49, '1464004156', ''),
(108, 'Beaded Necklaces', NULL, 50, '1464004205', ''),
(109, 'Bib Necklaces', NULL, 50, '1464004219', ''),
(110, 'Cameo Necklaces', NULL, 50, '1464004237', ''),
(111, 'Chains', NULL, 50, '1464004251', ''),
(112, 'Charm Necklaces ', NULL, 50, '1464004269', ''),
(113, 'Chokers', NULL, 50, '1464004281', ''),
(114, 'Bands', NULL, 39, '1464004310', ''),
(115, 'Midi Rings', NULL, 39, '1464004324', ''),
(116, 'Mulitstone Rings', NULL, 39, '1464004343', ''),
(117, 'Ring Guards & Spaces', NULL, 39, '1464004372', ''),
(118, 'Signet Rings', NULL, 39, '1464004388', ''),
(119, 'Solitaire Rings', NULL, 39, '1464004401', ''),
(120, 'Stackable Rings', NULL, 39, '1464004416', ''),
(121, 'Batic', NULL, 52, '1464004456', ''),
(122, 'Felting', NULL, 52, '1464004466', ''),
(123, 'Knitting & Crocheting', NULL, 52, '1464004499', ''),
(124, 'Macrame', NULL, 52, '1464004512', ''),
(125, 'Rugmaking', NULL, 52, '1464004530', ''),
(126, 'Spining', NULL, 52, '1464004541', ''),
(127, 'Bakeware ', NULL, 53, '1464004574', ''),
(128, 'Baking & Candy', NULL, 53, '1464004586', ''),
(129, 'Canning', NULL, 53, '1464004596', ''),
(130, 'Fermentation ', NULL, 53, '1464004610', ''),
(131, 'Adhesives & Tape ', NULL, 54, '1464004652', ''),
(132, 'Appliques ', NULL, 54, '1464004667', ''),
(133, 'Button Makers', NULL, 54, '1464004683', ''),
(134, 'Clay', NULL, 54, '1464004696', ''),
(135, 'Containers & Funnels ', NULL, 54, '1464004717', ''),
(136, 'Beads', NULL, 55, '1464004734', ''),
(137, 'Buttons', NULL, 55, '1464004744', ''),
(138, 'Charms', NULL, 55, '1464004757', ''),
(139, 'Jewelry & Beading Kits', NULL, 55, '1464004781', ''),
(140, 'Architecture & Home Improvement ', NULL, 56, '1464004847', ''),
(141, 'Dollhouse and Dollmaking', NULL, 56, '1464004872', ''),
(142, 'Drawing', NULL, 56, '1464004885', ''),
(143, 'Fiber Arts', NULL, 56, '1464004895', ''),
(144, 'Bags & Purses', NULL, 57, '1464004976', ''),
(145, 'Cover Ups & Scarves', NULL, 57, '1464005083', ''),
(146, 'Cummerbunds  ', NULL, 57, '1464005126', ''),
(147, 'Hair Accessories ', NULL, 57, '1464005147', ''),
(148, 'Dresses', NULL, 58, '1464005169', ''),
(149, 'Lingerie & Carters', NULL, 58, '1464005198', ''),
(150, 'Suits', NULL, 58, '1464005204', ''),
(151, 'Baskets & Boxes ', NULL, 59, '1464005235', ''),
(152, 'Bouquets ', NULL, 59, '1464005244', ''),
(153, 'Cake Toppers', NULL, 59, '1464005276', ''),
(154, 'Candles & Holders', NULL, 59, '1464005292', ''),
(155, 'Album & Scrapbooks ', NULL, 60, '1464005327', ''),
(156, 'Bridesmaids'' Gifts', NULL, 60, '1464005346', ''),
(157, 'Gifts'' For The Couple', NULL, 60, '1464005370', ''),
(158, 'Grooms'' Men Gifts', NULL, 60, '1464005389', ''),
(159, 'Announcements ', NULL, 61, '1464005412', ''),
(160, 'Greeting Cards', NULL, 61, '1464005425', ''),
(161, 'Invitation Kits', NULL, 61, '1464005437', ''),
(162, 'Books', NULL, 62, '1464005498', ''),
(163, 'Movies', NULL, 62, '1464005508', ''),
(164, 'Music', NULL, 62, '1464005519', ''),
(165, 'Audio', NULL, 63, '1464005540', ''),
(166, 'Camera', NULL, 63, '1464005546', ''),
(167, 'Car Parts & Accessories ', NULL, 63, '1464005565', ''),
(168, 'Cell Phones & Accessories ', NULL, 63, '1464005583', ''),
(169, 'Computers & Peripherals', NULL, 63, '1464005607', ''),
(170, 'DIY Kits', NULL, 63, '1464005622', ''),
(171, 'Decals & Skins', NULL, 63, '1464005635', ''),
(172, 'Game & Puzzles ', NULL, 64, '1464005657', ''),
(173, 'Sports & Outdoor Recreations', NULL, 64, '1464005680', ''),
(174, 'Toys', NULL, 64, '1464005700', ''),
(175, 'Artist Trading Cards', NULL, 65, '1464005768', ''),
(176, 'Clip Art', NULL, 65, '1464005782', ''),
(177, 'Collectibles', NULL, 65, '1464005794', ''),
(178, 'Dolls & Miniatures ', NULL, 65, '1464005810', ''),
(179, 'Bath Accessories ', NULL, 66, '1464005827', ''),
(180, 'Essential Oils', NULL, 66, '1464005841', ''),
(181, 'Fragrances', NULL, 66, '1464005853', ''),
(182, 'Hair Care', NULL, 66, '1464005866', ''),
(183, 'Makeup & Cosmetics ', NULL, 66, '1464005894', ''),
(184, 'Paper', NULL, 67, '1464005944', ''),
(185, 'Party Supplies', NULL, 67, '1464005963', ''),
(186, 'Pet Bedding', NULL, 68, '1464005986', ''),
(187, 'Pet Carriers & House', NULL, 68, '1464006006', ''),
(188, 'Pet Clothing Accessories & Shoes', NULL, 68, '1464006029', ''),
(189, 'Pet Feeding', NULL, 68, '1464006047', ''),
(190, 'Belts & Suspenders', NULL, 75, '1464006102', ''),
(191, 'Gloves & Mittens', NULL, 75, '1464006118', ''),
(192, 'Hair Accessories', NULL, 75, '1464006130', ''),
(193, 'Hats & Caps', NULL, 75, '1464006144', ''),
(194, 'Artist Trading Cards', NULL, 76, '1464006176', ''),
(195, 'Collectibles ', NULL, 76, '1464006195', ''),
(196, 'Drawing & Illustrations', NULL, 76, '1464006213', ''),
(197, 'Fiber Arts', NULL, 76, '1464006230', ''),
(198, 'Accessory Cases ', NULL, 77, '1464006260', ''),
(199, 'Back Packs', NULL, 77, '1464006272', ''),
(200, 'Hand Bags', NULL, 77, '1464006287', ''),
(201, 'Luggage & Travel', NULL, 77, '1464006305', ''),
(202, 'Boys'' Clothing', NULL, 78, '1464006329', ''),
(203, 'Girls'' Clothing', NULL, 78, '1464006337', ''),
(204, 'Mens'' Clothing', NULL, 78, '1464006347', ''),
(205, 'Womens'' Clothing', NULL, 78, '1464006354', ''),
(206, 'Fabric', NULL, 79, '1464006380', ''),
(207, 'Framing', NULL, 79, '1464006391', ''),
(208, 'Jewelry & Beading  ', NULL, 79, '1464006419', ''),
(209, 'Bathroom', NULL, 80, '1464006446', ''),
(210, 'Bedding', NULL, 80, '1464006456', ''),
(211, 'Curtains & Window Treatments', NULL, 80, '1464006481', ''),
(212, 'Floor & Rugs', NULL, 80, '1464006498', ''),
(213, 'Braclets', NULL, 81, '1464006513', ''),
(214, 'Brooches', NULL, 81, '1464006527', ''),
(215, 'Earrings', NULL, 81, '1464006543', ''),
(216, 'Jewelry Sets', NULL, 81, '1464006561', ''),
(217, 'Necklaces ', NULL, 81, '1464006578', ''),
(218, 'Rings', NULL, 81, '1464006588', ''),
(219, 'Games & Puzzles', NULL, 82, '1464006609', ''),
(220, 'Sports & Outdoor Games', NULL, 82, '1464006631', ''),
(221, 'Toys', NULL, 82, '1464006643', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
