#total orders
SELECT  count(ordr.o_id) as total_orders FROM orders as ordr
inner join order_detail ordrd on ordrd.order_id = ordr.o_id
where ordrd.buyer_delivery_confirmed = 'N' 
and LOWER(ordrd.shipping_status) = LOWER('Processing') and
lower(ostatus)= lower("ORDER_STAGE") and
LOWER(buyer_payment_status) =LOWER('PAID')
and LOWER(seller_payment_status) =LOWER('PENDING');

##total for last month
SELECT  ordr.created_date as total_orders FROM orders as ordr
inner join order_detail ordrd on ordrd.order_id = ordr.o_id
where ordrd.buyer_delivery_confirmed = 'N' 
and LOWER(ordrd.shipping_status) = LOWER('Processing') and
lower(ostatus)= lower("ORDER_STAGE") and
LOWER(buyer_payment_status) =LOWER('PAID') and
LOWER(seller_payment_status) =LOWER('PENDING') and 
year(ordr.created_date) >= year(curdate()) and 
month(ordr.created_date) >= month(curdate());

##last month orders detals
SELECT 
count(orders.o_id) as counts, 
DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') as from_date, 
 DATE_FORMAT(NOW() ,'%Y-%m-01') as to_date
FROM orders 
WHERE created_date >= DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND created_date < DATE_FORMAT(NOW() ,'%Y-%m-01')

#total orders last week
SELECT  count(ordr.o_id) as total_orders FROM orders as ordr
inner join order_detail ordrd on ordrd.order_id = ordr.o_id
where ordrd.buyer_delivery_confirmed = 'N' 
and LOWER(ordrd.shipping_status) = LOWER('Processing') and
lower(ostatus)= lower("ORDER_STAGE") and
LOWER(buyer_payment_status) =LOWER('PAID')
and LOWER(seller_payment_status) =LOWER('PENDING')
and ordr.created_date >= DATE_SUB(NOW(),INTERVAL 14 DAY) 
AND ordr.created_date < DATE_SUB(NOW(),INTERVAL 7 DAY);