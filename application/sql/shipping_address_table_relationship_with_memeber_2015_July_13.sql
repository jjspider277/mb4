set foreign_key_checks= false;
-- drop first

drop table if exists `mbu4u`.`shipping_addresses`;

-- CREATE TABLE SHIPPING

delimiter $$

CREATE TABLE if not exists `shipping_addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `shipping_address_line_1` varchar(45) DEFAULT NULL,
  `shipto_city` varchar(45) DEFAULT NULL,
  `shipto_state` char(2) DEFAULT NULL,
  `shipto_zip` varchar(10) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `shipping_address_line_2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sa_id_UNIQUE` (`id`),
  KEY `fk_shipping_profile` (`profile_id`),
  CONSTRAINT `fk_shipping_profile` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1$$



--chanege to unsigined int  and modify profile table to be related to shipping

--modify profile table 
ALTER TABLE if exists `mbu4u`.`profiles` ADD COLUMN `shipping_id` INT(11) NULL ;

ALTER TABLE if exists `mbu4u`.`profiles` CHANGE COLUMN `shipping_id` `shipping_id` INT(11) UNSIGNED NULL DEFAULT NULL  , 

  ADD CONSTRAINT `fk_shipping_id`

  FOREIGN KEY (`shipping_id` )

  REFERENCES `mbu4u`.`shipping_addresses` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_shipping_id` (`shipping_id` ASC) ;

set foreign_key_checks= true;