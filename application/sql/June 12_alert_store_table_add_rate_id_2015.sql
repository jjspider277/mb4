
---ALTER TABLE `mbu4u`.`stores` ADD COLUMN `rate_id`

ALTER TABLE `mbu4u`.`stores` ADD COLUMN `rate_id` INT NULL  AFTER `media_id` , 

  ADD CONSTRAINT `fk_store_rating_id`

  FOREIGN KEY (`rate_id` )

  REFERENCES `mbu4u`.`ratings` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_store_rating_id` (`rate_id` ASC) ;

