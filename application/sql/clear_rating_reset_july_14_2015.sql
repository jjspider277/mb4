-- Clear the rating system reset every item and rating table

set foreign_key_checks = false;
SET SQL_SAFE_UPDATES = 0;
update products set products.avg_rating=0;
update stores set avg_rating=0;
update profiles set avg_rating=0;


truncate table ratings;