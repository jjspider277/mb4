ALTER TABLE `mbu4u`.`products` 
CHANGE COLUMN `shipping` `shipping_price` VARCHAR(45) NULL DEFAULT NULL ,
ADD COLUMN `shipping_method` VARCHAR(45) NULL AFTER `shipping_price`;
