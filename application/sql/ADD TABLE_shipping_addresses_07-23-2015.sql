CREATE TABLE `shipping_addresses` (
  `sa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `shipto_street` varchar(50) DEFAULT NULL,
  `shipto_street2` varchar(45) DEFAULT NULL,
  `shipto_city` varchar(45) DEFAULT NULL,
  `shipto_state` char(2) DEFAULT NULL,
  `shipto_zip` varchar(10) DEFAULT NULL,
  `shipto_email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sa_id`),
  UNIQUE KEY `sa_id_UNIQUE` (`sa_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
