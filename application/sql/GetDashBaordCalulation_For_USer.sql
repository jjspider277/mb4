DROP PROCEDURE IF EXISTS mbu4u.GetDashBoardCalculationsForUser;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetDashBoardCalculationsForUser`(IN profile_id INT,
  OUT total_sales Double,
  OUT lw_total_sales Double,
  OUT lm_total_sales Double,
  OUT total_orders integer,
  OUT lw_total_orders integer,
  OUT lm_total_orders integer,
  OUT total_paid_money_minus_fee Double,
  OUT lm_total_paid_money_minus_fee Double,
  OUT lw_total_paid_money_minus_fee Double,
  OUT total_money_owned Double,
  OUT lw_total_money_owned Double,
  OUT lm_total_money_owned Double
)
begin

    #total SALES COLLECTED
    SELECT sum(order_total) AS total_sales into total_sales from orders where orders.buyer_id = profile_id;

    #total money owned last_week
    select sum(order_total) AS lw_total_sales into lw_total_sales from orders
    where created_date >= DATE_SUB(NOW(),INTERVAL 14 DAY) AND
          created_date < DATE_SUB(NOW(),INTERVAL 7 DAY) AND orders.buyer_id = profile_id;

    #total money owned last _month
    select sum(order_total) AS lm_total_sales into lm_total_sales from orders
    where lower(orders.ostatus) = lower('PURCHASE_STAGE') AND
          created_date >= DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01')
          AND created_date < DATE_FORMAT(NOW() ,'%Y-%m-01') and orders.buyer_id = profile_id;

    #total orders
    SELECT count(ordr.o_id) AS total_orders into total_orders FROM orders AS ordr
      INNER JOIN order_detail ordrd ON ordrd.order_id = ordr.o_id
    WHERE ordrd.buyer_delivery_confirmed = 'N' AND
          lower(ostatus)= lower("ORDER_STAGE") and order_detail.seller_id = profile_id ;
    ##last month orders detals
    SELECT
      count(orders.o_id) as lm_total_orders into lm_total_orders FROM orders
    WHERE created_date >= DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01')
          AND created_date < DATE_FORMAT(NOW() ,'%Y-%m-01') and order_detail.seller_id = profile_id ;

    #total orders last week
    SELECT count(ordr.o_id) AS lw_total_orders into lw_total_orders  FROM orders as ordr
      INNER JOIN order_detail ordrd on ordrd.order_id = ordr.o_id
    WHERE ordrd.buyer_delivery_confirmed = 'N' AND
          lower(ostatus)= lower("ORDER_STAGE") AND
          ordr.created_date >= DATE_SUB(NOW(),INTERVAL 14 DAY) AND
          ordr.created_date < DATE_SUB(NOW(),INTERVAL 7 DAY) and order_detail.seller_id = profile_id ;

    #total paid out done
    SELECT (sum(order_total)) - ( round((10/100)*sum(order_total),2)) AS total_paid_money_without_fee into total_paid_money_minus_fee from orders
    WHERE lower(orders.ostatus) = lower('PURCHASE_STAGE') and order_detail.seller_id = profile_id ;

    #total paid out last month
    SELECT (sum(order_total)) - ( round((10/100)*sum(order_total),2)) AS lm_total_paid_money_without_fee into lm_total_paid_money_minus_fee from orders
    WHERE lower(orders.ostatus) = lower('PURCHASE_STAGE') AND
          created_date >= DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01')
          AND created_date < DATE_FORMAT(NOW() ,'%Y-%m-01') and order_detail.seller_id = profile_id ;

    #total paid last week
    SELECT (sum(order_total)) - ( round((10/100)*sum(order_total),2)) AS lw_total_paid_money_minus_fee into lw_total_paid_money_minus_fee  from orders
    WHERE lower(orders.ostatus) = lower('PURCHASE_STAGE') AND
          created_date >= DATE_SUB(NOW(),INTERVAL 14 DAY) AND
          created_date < DATE_SUB(NOW(),INTERVAL 7 DAY) and order_detail.seller_id = profile_id ;

    #total money owned
    select (round((10/100)*sum(order_total),2)) AS total_money_owned into total_money_owned from orders
    where lower(orders.ostatus) = lower('PURCHASE_STAGE') and order_detail.seller_id = profile_id  ;

    #total money owned last_week
    select (round((10/100)*sum(order_total),2)) AS lw_total_money_owned into lw_total_money_owned from orders
    where lower(orders.ostatus) = lower('PURCHASE_STAGE') AND
          created_date >= DATE_SUB(NOW(),INTERVAL 14 DAY) AND
          created_date < DATE_SUB(NOW(),INTERVAL 7 DAY) and order_detail.seller_id = profile_id ;

    #total money owned last _month
    select (round((10/100)*sum(order_total),2)) AS lm_total_money_owned into lm_total_money_owned from orders
    where lower(orders.ostatus) = lower('PURCHASE_STAGE') AND
          created_date >= DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01')
          AND created_date < DATE_FORMAT(NOW() ,'%Y-%m-01') and order_detail.seller_id = profile_id ;


    if total_sales > 0 then
      set total_sales=ROUND(total_sales,2);
    else
      set total_sales=0;
    End if;
    if lw_total_sales > 0 then
      set lw_total_sales=ROUND(lw_total_sales,2);
    else
      set lw_total_sales=0;
    End if;
    if lm_total_sales > 0 then
      set lm_total_sales=ROUND(lm_total_sales,2);
    else
      set lm_total_sales=0;
    End if;
    if total_orders > 0 then
      set total_orders=total_orders;
    else
      set total_orders=0;
    End if;
    if lw_total_orders > 0 then
      set lw_total_orders=lw_total_orders;
    else
      set lw_total_orders=0;
    End if;
    if lm_total_orders > 0 then
      set lm_total_orders=lm_total_orders;
    else
      set lm_total_orders=0;
    End if;
    if total_paid_money_minus_fee > 0 then
      set total_paid_money_minus_fee=ROUND(total_paid_money_minus_fee,2);
    else
      set total_paid_money_minus_fee=0;
    End if;
    if lm_total_paid_money_minus_fee > 0 then
      set lm_total_paid_money_minus_fee=ROUND(lm_total_paid_money_minus_fee,2);
    else
      set lm_total_paid_money_minus_fee=0;
    End if;

    if lw_total_paid_money_minus_fee > 0 then
      set lw_total_paid_money_minus_fee=lw_total_paid_money_minus_fee;
    else
      set lw_total_paid_money_minus_fee=0;
    End if;

    if total_money_owned > 0 then
      set total_money_owned=ROUND(total_money_owned,2);
    else
      set total_money_owned=0;
    End if;

    if lw_total_money_owned > 0 then
      set lw_total_money_owned=ROUND(lw_total_money_owned,2);
    else
      set lw_total_money_owned=0;
    End if;
    if lm_total_money_owned > 0 then
      set lm_total_money_owned = ROUND(lm_total_money_owned,2);
    else
      set lm_total_money_owned=0;
    End if;
  end;