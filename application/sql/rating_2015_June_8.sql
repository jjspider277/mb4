delimiter $$

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `rating` float NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `rate_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profile_ratings_id` (`profile_id`),
  KEY `fk_products_rating_id` (`product_id`),
  KEY `fk_media_rating_id` (`media_id`),
  CONSTRAINT `fk_media_rating_id` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_rating_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_ratings_id` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1$$
