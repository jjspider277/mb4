
---ALTER TABLE `mbu4u`.`profiles` ADD COLUMN `rate_id

ALTER TABLE `mbu4u`.`profiles` ADD COLUMN `rate_id` INT NULL  AFTER `account_id` , 

  ADD CONSTRAINT `fk_memeber_rate_id`

  FOREIGN KEY (`rate_id` )

  REFERENCES `mbu4u`.`ratings` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_memeber_rate_id` (`rate_id` ASC) ;

