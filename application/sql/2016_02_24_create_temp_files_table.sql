CREATE TABLE `temp_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(45) DEFAULT NULL,
  `image_id` varchar(65) DEFAULT NULL,
  `path` varchar(65) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
