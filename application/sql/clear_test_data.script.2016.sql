set foreign_key_checks = false;
delimiter //

drop procedure if exists trunacte_tables //
create procedure trunacte_tables()
begin
    DECLARE done int default false;
    DECLARE table_names CHAR(255);

    DECLARE cur1 cursor for SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables 
        WHERE TABLE_SCHEMA = "mbu4u" and TABLE_NAME <> "users" and TABLE_NAME <> "groups" ;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = false;
    open cur1;

    myloop: loop
        fetch cur1 into table_names;
select table_names;
        if done then
            leave myloop;
        end if;
           select table_names as "Gofmann";
           set @sql = CONCAT('truncate table ', table_names);
        prepare stmt from @sql;
        execute stmt;
        drop prepare stmt;
    end loop;

    close cur1;
end //

delimiter ;

call trunacte_tables(); //call :)
