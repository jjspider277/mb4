set foreign_key_checks = false

-- Remove the avg_rating column we dont need it
ALTER TABLE `mbu4u`.`ratings` DROP COLUMN `avg_rating` ;

-- Remove RateId from Profile
ALTER TABLE `mbu4u`.`profiles` DROP COLUMN `rate_id` ;

-- Remove RateId from Store
ALTER TABLE `mbu4u`.`stores` DROP FOREIGN KEY `fk_store_rating_id` ;ALTER TABLE `mbu4u`.`stores` DROP COLUMN `rate_id` , DROP INDEX `fk_store_rating_id` ;

-- Remove RateId from Media
ALTER TABLE `mbu4u`.`medias` DROP COLUMN `rate_id` ;

set foreign_key_checks = true;

-- add avg_rating tp product table
ALTER TABLE `mbu4u`.`products` ADD COLUMN `avg_rating` FLOAT NULL  AFTER `created_date` ;

-- add avg_rating to profiles
ALTER TABLE `mbu4u`.`profiles` ADD COLUMN `avg_rating` FLOAT NULL  AFTER `account_id` ;


-- add avg_rating to stores
ALTER TABLE `mbu4u`.`stores` ADD COLUMN `avg_rating` FLOAT NULL  AFTER `media_id` ;

--Updated Stored Procedure raingCalaculation

DELIMITER $$
DROP PROCEDURE IF EXISTS mbu4u.ratingProcedure;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ratingProcedure`(IN rate_item_id int,IN type varchar(10),OUT average float)
BEGIN

   DECLARE avg_rating float DEFAULT 0.0;


   IF (type = 'profile') THEN

        select ( sum(r.total_rate)/ n.total_no_users) as avg_rating into avg_rating
        from
        (select sum(rating) as total_rate  from ratings where rate_type=type and ratings.profile_id=rate_item_id) as r ,
        (select count(distinct created_by) as total_no_users from ratings where rate_type=type and ratings.profile_id=rate_item_id) as n ;
        -- update the avg_rating_column of profiles table
        update profiles set profiles.avg_rating=ROUND(avg_rating,2) where profiles.id=rate_item_id ;

   END IF;

   IF (type = 'store') THEN
        -- do the actual rating calacualtion count of users give rating
        -- avaraged on  the avg_rating_column of ratings table of counted and sumed rating value for a give prfile id
        select ( sum(r.total_rate)/ n.total_no_users) as avg_rating into avg_rating
        from
        (select sum(rating) as total_rate  from ratings where rate_type=type and ratings.store_id=rate_item_id) as r ,
        (select count(distinct created_by) as total_no_users from ratings where rate_type=type and ratings.store_id=rate_item_id) as n ;
        -- update the avg_rating_column of stores table
        update stores set stores.avg_rating=ROUND(avg_rating,2) where stores.id=rate_item_id ;

   END IF;

    IF (type = 'product') THEN
      select ( sum(r.total_rate)/ n.total_no_users) as avg_rating into avg_rating
        from
        (select sum(rating) as total_rate  from ratings where rate_type=type and ratings.product_id=rate_item_id) as r ,
        (select count(distinct created_by) as total_no_users from ratings where rate_type=type and ratings.product_id=rate_item_id) as n ;
         -- update the avg_rating_column of products table
        update products set products.avg_rating=avg_rating where products.id=rate_item_id ;
  END IF;
     IF (type = 'media') THEN
      select ( sum(r.total_rate)/ n.total_no_users) as avg_rating into avg_rating
        from
        (select sum(rating) as total_rate  from ratings where rate_type=type and ratings.media_id=rate_item_id) as r ,
        (select count(distinct created_by) as total_no_users from ratings where rate_type=type and ratings.media_id=rate_item_id) as n ;
         -- update the avg_rating_column of medias table
        update medias set medias.avg_rating=ROUND(avg_rating,2) where medias.id=rate_item_id ;

   END IF;

   if avg_rating > 0 then
      set average=ROUND(avg_rating,2);
   else
     set avg_rating=0;
  end if;


END;
-- end of Procedure