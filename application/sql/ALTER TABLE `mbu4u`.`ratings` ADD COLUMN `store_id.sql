ALTER TABLE `mbu4u`.`ratings` ADD COLUMN `store_id` INT NULL  AFTER `rate_type` , 

  ADD CONSTRAINT `fk_store_with_rating_id`

  FOREIGN KEY (`store_id` )

  REFERENCES `mbu4u`.`stores` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_store_with_rating_id` (`store_id` ASC) ;

