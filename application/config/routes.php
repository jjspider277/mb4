<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome/home";
$route['404_override'] = '';

//messing up with routes 
//TODO:Daniel
$route['home'] = "welcome/home";  
$route['home/(:num)'] = "welcome/home/$1";
$route['dashboard/my_stores/edit_store/(:num)'] = "dashboard/edit_store/$1";
$route['dashboard/my_stores/add_store'] = "dashboard/add_store/";
$route['dashboard/my_store/edit_store/edit_lisiting/(:num)'] = "dashboard/edit_lisiting/$1";
$route['dashboard/my_store/edit_store/add_product/(:num)'] = "dashboard/add_product/$1";
$route['contact-us'] = "contact-us";


//$route['buy/index/(:num)'] = "buy/;



/* End of file routes.php */
/* Location: ./application/config/routes.php */