<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * created by danie adenew
 */

$config['public_urls'] = array (
'/',
'users/signin',
'users/logout',
'users/login',
'users/forgot_password',
'users/reset_password',
'welcome/index',
'welcome/subscribe',
'welcome/home',
'signup/index',
'signup/register',
'signup/',
'signup/activate',
'signup/resend_activation',
'buy/',
'sell/',
'sell/become_seller',
'sell/sell',
'sell/seller',
'sell/index',
'bid/',
'bid/index',
'bid/product',
'bid/confirm_bid',
'buy/index',
'buy/buy',
'product/detail',
'product/new_arrivals',
'sell/send_invites',
'store/store_listing',
'product/browse',
'cron/',
'globalsearch/search',
'globalsearch/search_products',
 'notification/update_notification_seen_status'
);

$config['upload_config_profile_edit'] = array (
        'upload_path' => '',
        'file_name'=>'',
        'allowed_types' => "gif|jpeg|jpg|png|mp4|3gp|flv",
        'overwrite' => TRUE,
        'max_size' => "5120", // Can be set to particular file size , here it is 5 MB( 5,120 KB)
        'max_height' => "2068",
        'max_width' => "4024"
    );

$config['upload_config_video_files'] = array (
        'upload_path' => '',
        'file_name'=>'',
        'allowed_types' => "mpg|mov|mp4|mpeg|avi|3gp|flv",
        'overwrite' => TRUE,
        'max_size' => "111250240", // Can be set to particular file size , here it is 5 MB( 5,120 KB)
        'max_width'=>'200000000',
        'max_height'=>'1000000000000',
    );

$config['fee_setting'] = array (
    'create_store'=>'1.00',
);

$config['account_types'] =
    array("Checking"=>"Checking", "Saving"=>"Saving");

$config['order_status'] =
    array("order_stage"=>"ORDER_STAGE","purchased_stage"=>"PURCHASE_STAGE", "report_stage"=>"REPORTED_STAGE");

$config['payment_status'] =
    array("paid"=>"Paid", "pending"=>"Pending");
?>

