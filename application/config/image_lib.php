<?php
/**
 * Created by Daniel Adenew.
 * make by 
 * Date: 3/27/2015
 * Time: 12:53 PM
 */

$config['image_resize_config'] = array(
  'image_library' => 'gd2',
  'create_thumb' => FALSE,
  'maintain_ratio' => false,
  'width'=> 250,
  'height'=> 300,
);

$config['image_resize_config_profile'] = array(
  'image_library' => 'gd2',
  'create_thumb' => FALSE,
  'maintain_ratio' => true,
  'width'=> 225,
  'height'=> 225,
);

$config['image_resize_store_config'] = array(
  'image_library' => 'gd2',
  'create_thumb' => true,
  'maintain_ratio' => true,
  'thumb_marker' => '_thumb',
  'width'=> 225,
  'height'=> 225,
);

$config['image_resize_product_config'] = array(
  'image_library' => 'gd2',
  'create_thumb' => true,
  'maintain_ratio' => true,
  'thumb_marker' => '_thumb',
  'width'=> 225,
  'height'=> 225,
);



