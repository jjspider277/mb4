<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadeByUs4U.con | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="detectify-verification" 
    content="b6c199df5e2fe0baa937af3f8d367af7" />
    <link rel="icon" href="<?=base_url()?>/favicon.ico" type="image/gif">
    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/light-box/css/lightbox.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/home.css";?> rel="stylesheet">
     <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">

</head>

<body>

<style type="text/css">
.row5 {
    padding: 2px;
    border: 2px solid #F3F3F2;
    border-top: 7px solid grey;
    border-radius: 11px;
}
    .menu-column_submenu  {
        margin: 0;
        padding: 0;
        font-family: 'Oswald',sans-serif;
        font-weight: 300;
        font-size:medium ;

    }

#search-header {
    width: 436px;
}
#search{
    background: #d3d3d3;
    cursor: pointer;
    font-size: 24px;
    font-weight: bold;
    text-transform: lowercase;
    padding: 20px 2%;
    width: 96%;
}
#search-overlay{
    background: black;
    background: rgba(255, 255, 255, 255);
    color: black;
    display: none;
    font-size: 18px;
    height: 200px;
    padding: 0px;
    margin-top:28px;
    position: absolute;
    width: 436px;
    z-index: 100;
    opacity: 0.95;
    border-radius: 4%;
    border: 2px solid #efefef;
    overflow: auto;
}
#display-search{
    border: none;
    color: black;
    font-size: 14px;
    margin: 5px 0 0 0;
    width: 400px;
    height: 20px;
    padding: 0 0 0 10px;
    display: none;
}

#hidden-search{
    left: -10000px;
    position: absolute;

}

#results{
    display: none;
    width: 300px;
    list-style: none;
}
#results ul {
    list-style:none;
    padding-left:0;
}​
#results ul li{
    list-style: none;
    padding-left:0;
}

#results ul li a{
    color:#2676af;
    font-size: 12px;
    font-weight: bold;
}
}
#search-data{
    font-size: 14px;
    line-height: 20%;
    padding: 0 0 0 20px;

}

h2.search-data{
    margin: 10px 0 30px 0;

}
</style>

<header>

<?php $this->load->view($home_page_white_menu); ?>

</header>

<!-- Responsive design
================================================== -->
<section id="responsive" >
<div class="container container2">

<div class="row row2"  >
 <div class="col-md-3 col-md-offset-5" style="background-color:balck;">
         <a href="<?php echo base_url('welcome/home'); ?>">
         <img src="<?php echo base_url()."assets/images/mbu4ulogo.png";?>"/></a>
 </div>

 <div class="col-md-2 col-md-offset-1 col-md-offset-1-1" style="margin-top: 1%;">


              <?= form_dropdown('catagories', array_merge(array(''=>'All  Catagories'), $catagories_all) ,'','class="form-control"  tabindex="7"' ) ;?>
          </div>
  <div class="col-sm-1" style="margin-top: 1%;">  <input type='button' class="btn btn-primary form-control" value="search"> </div>

  </div> 

  <div class="row">

          <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                <a class="navbar-brand" href="#">
              
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <style type="text/css">
  .main-navigation {
    background-color: #FAFAFA;
}
.navbar-default {
  background-color: inherit; 
  border: none; 
}
</style>
          <?php $this->load->view($column_main_menu);?>

          </div><!-- /.container-fluid -->
        </div>
         <div class="container container4" id='header_image'>
                  
            <div class=" row row3-background">
                      <!-- /.this the large header image -->
                  </div>
              </div>

         <div class="container">
                          <div class="row row4">

                              <div class="col-md-8 col-md-offset-4">
                                  <h3> <strong>Welcome To</strong> <strong style="color:blue;    ">MadeByUs4U!</strong></h3>

                              </div>
                              <div class="col-md-8 col-md-offset-3" style="padding-bottom:12px;">
                                  <h5> The best way to <strong>shop</strong> , <strong>sell</strong> & <strong>bid</strong> online and communicate seller-to-buyer.</h5>

                              </div>



                            
                             <hr>
                          </div>
                </div>
  </div>
<div class="container">
    <div class="row row5">

         <div class="col-md-4 module_images_upload col-sm-4 col-xs-4 " style="border-radius: 8px;">

                <h4>Upload your product</h4>
                <p>You upload your product we do the rest while you make 90% of the sale price</p>
                <a href="<?php echo base_url('sell/become_seller');?>" class="btn btn-default">EARN CASH</a>
          
     
         </div>

          <div class="col-md-4 col-md-offset-1 col-sm-4 col-xs-4 module_images_newarrival img-responsive">

              <a href="<?php echo base_url('product/new_arrivals') ;?>" class="btn btn-primary">NEW ARRIVALS</a>
         
          </div>

         <div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-1 module_images_meet_people img-responsive " >

                <h4><a href='<?php echo base_url('sell');?>' style="text-decoration:none;">MEET NEW MEMBERS DAILY</a></h4>
                <a href="<?php echo base_url('sell') ;?>" class="btn btn-primary browse-btn">BROWSE</a>
         </div>
    
     </div>

    <div class="row row6">

            <div class="col-md-6 col-md-offset-3" >
            <div class="content-bottom">
                 <h2><a href='<?php echo base_url('buy');?>' style="text-decoration:none;color:inherit;"> Our Latest Arrivals</a> </h2>
               <p style="color:#8B8A8A"> Check our latest offers that just arrived to the store. <span>New Nonummy</span> for you to wear</p>  

            </div>
           
            </div> 
    </div>

<div class="row row7 product_navigation">

    <div class="row">
        <div class='pull-right'>
                            <p><?php echo $links; ?></p>
            
        </div>
     </div>

</div>

    <?php $this->load->view($product_listing,$products); ?>

  </div>

  </div>



</section>
<footer class="footer">
<?php $this->load->view($footer_page); ?>
</footer>
<!-- Bootstrap and Jquery and Other JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/global_search.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/light-box/js/lightbox.js";?>"></script>
<script>

   /***global base url path in javascript***/
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split('/');
  var base_url_complete = base_url+'/'+pathArray[1]+'/';
  var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ; 
  var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $(".rating").rating('refresh', 
            {showClear: false,disabled:true,showCaption: false,hoverEnabled:false,starCaptions: {5.0:'5 Stars'}
          });
         $('.rating').on('rating.change', function() {
           if(!readOnly) {
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,'product',profile_id,server_url,csrf_token,csrf_hash);

          }
           else {
            window.location.assign("<?=site_url('users/login');?>");
           }

        });
       
    });
</script>

<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
</script>

<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});
$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>



</body>
</html>
