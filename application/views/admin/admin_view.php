<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bid.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">


    <link href=<?php echo base_url()."assets/css/collective_common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/seller.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/slimbox2.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/admin.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/product.css";?> rel="stylesheet">

</head>


<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>
    <?php $this->load->view($header_logo_white); ?>
    <!--load tab menu here -->
    <div class="row row-menu">
        <div class="container tab-menu-container">

            <div class="tab-menu col-md-12" id="tabs">
                   <a href="#dashboard" class="selected" aria-controls="listing" role="tab" data-toggle="tab" class='tab-links'><span id="dashboardImg" class="active"><i class="fa fa-tachometer"></i> DashBoard</a></span>
                    <a href="#orders"data-toggle="tab"  class='tab-links'><span id="orderImg" class="span-right-padding"></span><span>Orders</a></span>
                <a href="#purchases" data-toggle="tab" class='tab-links'><span id="purchaseImg" class="span-right-padding"></span><span>Purchases </a></span>
                <a href="#stores"  data-toggle="tab" class='tab-links'><span id="storeImg" class="span-right-padding"></span><span> Stores</a></span>
                <a href="#listings"  data-toggle="tab" class='tab-links'><span id="listingsImg" class="span-right-padding"></span><span>Lisiting</a></span>
                <a href="#auctions"  data-toggle="tab" class='tab-links'><span id="auctionsImg" class="span-right-padding"></span><span>Auctions</a></span>
                <a href="#members"  data-toggle="tab" class='tab-links'><span id="membersImg" class="span-right-padding" ></span><span>Members</a></span>
                <a href="#videos" data-toggle="tab" class='tab-links'><span id="videosImg" class="span-right-padding"></span><span> Videos</a></span>

            </div>

        </div>


    </div>


</header>
<!-- Responsive design
================================================== -->

<section id="responsive" style="background-color:#f5f5f5;">



    



    <?php $this->load->view('admin/admin_tab_content.php');?>

    </row>



</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script  type="text/javascript" src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/global_search.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/jscolor.js";?>"></script>
<script type="text/javascript" src="assets/plugins/rating/js/star-rating.min.js"></script>
<script type="text/javascript" src="assets/js/rating_ajax.js"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/video-photo-page.js";?>"></script>


<!--
<script type="text/javascript" src="assets/plugins/bootstrap/js/tooltip.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-rating.min.js"></script>
-->

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    /***global base url path in javascript***/
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split('/');
    var base_url_complete = base_url+'/'+pathArray[1]+'/';
    var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;
    var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";
    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);
    //console.log(pathArray);
    if(base_url!="http://localhost")
        base_url_complete = base_url+"/";

    /*    function rate_item($rating_input_id,$rating_value) {
     alert('in');
     $("#"+$rating_input_id).rating('rate', $rating_value);
     }

     function get_rating_value($rating_input_id,$to_rated_item_id) {

     save_rating($rating_input_id,"products",$to_rated_item_id);
     }


     $(function () {


     $('#programmatically-rating43').click(function () {

     $('#programmatically-rating43').rating('rate', $('#programmatically-value3').val());
     });
     $('#programmatically-rating43').change(function () {
     alert($('#programmatically-rating43').rating('rate'));
     });

     $('.rating-tooltip').rating({
     extendSymbol: function (rate) {
     $(this).tooltip({
     container: 'body',
     placement: 'bottom',
     title: 'Rate: ' + rate
     });
     }
     });



     });*/


</script>
<script>


    $(function(){

        var menu = $('.tab-menu');

        // Mark the clicked item as selected

        menu.on('click', 'a', function(){
            var a = $(this);

            a.siblings().removeClass('selected');
            a.addClass('selected');
        });
    });
</script>
<script>
    jQuery(document).ready(function () {

        var readOnly = "<?= $this->is_logged_in==true?false:true;?>";


        $(".rating").rating('refresh',
            {showClear: false,diabled:true,hoverEnabled:false, showCaption: false,size: 'xs',starCaptions: {5.0:'5 Stars'},
            });


        $('.rating').on('rating.change', function() {

            if(!readOnly) {
                var value =  $(this).val();
                var static_id_text=("rating_element-").length;
                var profile_id =  ((this).id).slice(static_id_text);
                var rated = $(this).val();
                var server_url = "<?php echo base_url('rating/rate');?>";
                var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

                save_rating(profile_id,value,'profile',profile_id,server_url,csrf_token,csrf_hash);

            }
            else {
                window.location.assign("<?=site_url('users/login');?>");
            }




        });




    });
</script>

<script type="text/javascript">

    $('#display-search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });
    $('#search-header').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });

    $('#home_page_search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });



</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#tabs a").click(function(){
            $("#tabs span").removeClass("active");
            $("span",this).addClass("active");
        });
    })
    $('.set-auction-for-edit').click(function(){

        $("#setAuction").css('display','block');
        var $this = $(this);
        $(this).css('display','none');
        $(".previewAuctionButtonForEditListing").css('display','block');
        //alert('jquery-clicked   ');

    });
</script>
<script>
    $('#previewStoreInAdmin').click(function(){


        var $this = $(this);
        $(this).css('display','none');


    });
</script>



<script type="text/javascript">

    //save all the lookup bullshits here

    $("#add-btn-category").click(function(){

        var $url = base_url_complete+'admin/save_category';
        var $category = $("#category_input").val();
        console.log($category);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'category_name': $category}),
            dataType: 'json',
            type: "post",
            success: function(data){
               console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the category ($category) successfully saved!")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });

        $.getJSON(base_url_complete+'admin/get_all_categories','', function(data){

            var options = '';
            for (var x = 0; x < data.length; x++) {
                options += '<option value="' + data[x]['id'] + '">' + data[x]['value'] + '</option>';
            }
            $('#category_list').html(options); //AMAZING CODE

        });


    });

    //save all the sub-categories

    $("#btn_save_subcategory").click(function(){

        var $url = base_url_complete+'admin/save_subcategory';
        var $sub_category = $("#subcategory_input").val();
        var $category_id = $("#category_list option:selected").val();
        console.log($sub_category);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'subcategory': $sub_category,'category_id':$category_id}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $(".showCategoryModal-sm").hide().delay(400);
                $("#message_display").html("<strong>Success!</strong> We have saved sub category ($subcategory) into category ($category) successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });

    });

    //save all the lookup bullshits here

    $("#add-btn-variation-color").click(function(){

        var $url = base_url_complete+'admin/save_variation_colors';
        var $variation_colors = $("#color").val();
        console.log($variation_colors);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'color': $variation_colors}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the color variation successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });


    });


    //save all the lookup bullshits here

    $("#add_btn_shipping_options").click(function(){

        var $url = base_url_complete+'admin/save_shipping_options';
        var $shipping_option = $("#shipping_input").val();
        console.log($shipping_option);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'shipping_option': $shipping_option}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the shipping option successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });


    });
    

</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){

       $.getJSON(base_url_complete+'admin/get_all_categories','', function(data){

           var options = '';
           for (var x = 0; x < data.length; x++) {
               options += '<option value="' + data[x]['id'] + '">' + data[x]['value'] + '</option>';
           }
           $('#category_list').html(options); //AMAZING CODE

    });

    });
</script>


</body>
</html>
