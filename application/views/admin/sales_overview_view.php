<h3>Sales Orders Overview</h3>

<table class="table table-striped medium-font" >
    <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th> <th>Order #</th> <th>Date</th> <th>Customer</th> <th>Payment Status</th><th>Shipping Status</th><th>Ammount<th></th> </tr> </thead>
    <tbody>

    <?php foreach( $sales_overview_data as $sales_items):?>
    <tr><td></td>
        <td><a href="<?php echo base_url('admin/order_detail/'.$sales_items->o_id);?>">#<?= $sales_items->invoice_id;?></a></td>
        <td>
            <p class="no-margin"><?= $sales_items->created_date;?></p>
            <p class="less-medium-font no-margin">@11:15pm</p></td>
        <td>
            <a> <p class="underline"><?= ucfirst($sales_items->buyer_fname)." ";?><?= ucfirst($sales_items->buyer_lname);?></p></a>
        </td>
        <td class="btn-right-padding"><button class="btn btn-success" > Paid </button></td>
        <td class="btn-right-padding"><button class="btn btn-danger" >Delivered </button></td>
        <td>$<?= $sales_items->price;?></td>
        <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
    </tr>
    <?php endforeach;?>

    </tbody>
</table>
<div class="col-md-12 view-more-border">
    <div class="col-md-2 col-md-offset-5 "><a class="underline dark-grey-font"><p>View all Sales Orders <span class="fa fa-caret-down"></span></p></a></div>
</div>