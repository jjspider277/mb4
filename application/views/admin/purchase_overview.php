<h3>Purchases Overview</h3>
<table class="table table-striped medium-font" >
    <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th> <th>Invoice#</th> <th>Seller</th> <th>Date</th> <th>Type</th><th>Status</th><th>Ammount<th></th> </tr> </thead>
    <tbody>
    <?php foreach($purchase_overview_data as $purchase_items):?>
    <tr ><td></td>
        <td><a href="<?php echo base_url('admin/order_detail/'.$purchase_items->o_id);?>">#<?=$purchase_items->invoice_id;?></a></td>
        <td>
            <a>
                <p class="underline"><?=ucfirst($purchase_items->seller_fname).' '.ucfirst($purchase_items->seller_lname);?></p>

            </a>
            <p class="less-medium-font no-margin"><?=$purchase_items->seller_email;?></p>

        <td>
            <p class="no-margin"><?=$purchase_items->updated_date;?></p>

        </td>
        <td class="btn-right-padding"><button class="btn btn-success" > Sale</button></td>
        <td class="btn-right-padding"><button class="btn btn-primary" >Processing</button></td>
        <td>$<?= $purchase_items->order_total;?></td>
        <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
    </tr>
    <?php endforeach;?>

    </tbody> </table>
<div class="col-md-12 view-more-border">
    <div class="col-md-2 col-md-offset-5"><a class="underline dark-grey-font"><p>View all Purchases<span class="fa fa-caret-down"></span></p></a></div>
</div>
