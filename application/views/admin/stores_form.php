
<div class="col-md-12  inner-white">

    <div class="alert alert-success" id="message_display" style="display: none;">
        <strong>Success!</strong> Indicates a successful or positive action.
    </div>

    <div class="col-md-12 blue-border">
        <div class="col-md-6 ">

            <div class="col-md-9 no-left-right-padding lg-input">
                <label for="category">Create Category</label>
                <input  class="form-control" id="category_input" name = "category_input" placeholder="" type="text">
            </div>
            <div class="col-md-3 no-right-padding">
                <button class="btn btn-primary add-btn" id="add-btn-category" name="add-btn-category">Add</button>
            </div>

            <div class="col-md-6 no-left-right-padding md-input">
                <label for="category">Variation (Color)</label>

                <div class="input-group">
                          <span class="input-group-btn">
                            <button onclick="document.getElementById('color').jscolor.show()" class="btn btn-default color-picker" type="button"><img src="assets/icons/color-picker.png"/> </button>
                          </span><input id="color" class="form-control jscolor"  placeholder=" "  >
                    <span><a href="#"></a></span>
                </div>

            </div>

            <div class="col-md-3 no-right-padding">
                <button class="btn  btn-primary add-btn " id="add-btn-variation-color">Add</button>
            </div>

            <div class="col-md-9 no-left-right-padding lg-input">
                <label for="category">Shipping Options</label>
                <input  class="form-control" id="shipping_input" placeholder="" type="text">
            </div>
            <div class="col-md-3 no-right-padding">
                <button class="btn  btn-primary add-btn" id="add_btn_shipping_options" >Add</button>
            </div>


        </div>
        <div class="col-md-6">

            <div class="col-md-9 no-left-right-padding lg-input">
                <label for="category">Create sub-Category</label>
                <input  class="form-control" id="subcategory_input" placeholder="" type="text">
            </div>
            <div class="col-md-3 no-right-padding">
                <button class="btn  btn-primary add-btn" data-toggle="modal" data-target=".showCategoryModal-sm">Add</button>
            </div>

            <!--modal for subcategory selection -->
            <!-- Small modal -->

            <div class="modal fade showCategoryModal-sm" tabindex="-1" role="dialog" aria-labelledby="showCategoryModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Select Category Window</h4>
                        </div>
                        <div class="modal-body">
                            <p>To which category you want to add the sub category ? select below and click save.</p>
                            <select class="form-control" id='category_list' >
                               <options></options>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn-save" id="btn_save_subcategory">Save</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>

            <div class="col-md-6 no-left-right-padding md-input">
                <label for="category">Sub Variation (Size)</label>
                <input  class="form-control" id="category" placeholder="" type="text">
            </div>
            <div class="col-md-3 no-right-padding">
                <button class="btn  btn-primary add-btn">Add</button>
            </div>

        </div>
    </div>
</div>