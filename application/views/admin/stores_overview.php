<h3>Stores Overview</h3>
<table class="table table-striped medium-font col-md-6" >
    <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th> <th>Item listing</th><th>Date Added</th> <th></th> </tr> </thead>
    <tbody>

    <?php foreach($store_overview_data as $store_items):?>


    <tr ><td></td>
        <td><a type="button" href="#storesDetail" data-toggle="tab"><img  class="img-thumbnail" src="<?php echo $store_items->media->file_name;?>" height="50" width="50"/><?php echo $store_items->store->name;?></a></td>


        <td>
            <p class="no-margin"><?php $store_items->store->created_date;?></p>
            <p  class="less-medium-font no-margin"><?php date('h i:A',strtotime($store_items->store->created_date));?></p>

        </td>

        <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
    </tr>
 <?php endforeach;?>


    </tbody> </table>
<div class="col-md-12 view-more-border">
    <div class="col-md-5 col-md-offset-3"><a class="underline dark-grey-font"><p>View all Stores<span class="fa fa-caret-down"></span></p></a></div>
</div>