<style>
    .black-thead{
        background: #000000;
        color: #ffffff;
    }
    .glyphicon-trash{
        color:#d9312b;
    }
    .btn-danger, .btn-success{
        width: 100%;
    }
    .medium-font{
        font-size: 16px;
    }
    .less-medium-font{
        font-size: 14px;
    }
    .table > tbody > tr > td{
        padding: 16px 8px;
    }
    .btn-right-padding{
        padding-right: 58px !important;
    }
    .underline{
        text-decoration: underline;
    }
    .no-margin{
        margin:0 0 0px;
    }
    .no-left-padding{
        padding-left:0px;
    }
    .no-right-padding{
        padding-right:0px;
    }
    .no-left-right-padding{
        padding-left:0px;

        padding-right:0px;
    }
    .fa-caret-down{
        font-size: 16px;
        padding-left: 11px;
    }
    .dark-grey-font{
        color:#5e5e5e;
    }
    table{
        border: 1px solid rgb(224, 224, 224);
    }
    .view-more-border{
        padding: 12px;
        top: -20px;
        border-bottom: 1px solid rgb(224, 224, 224);
        border-left: 1px solid rgb(224, 224, 224);
        border-right: 1px solid rgb(224, 224, 224);
    }
    .table-striped > tbody > tr:nth-child(2n+1) {
        background-color: #ffffff;
    }
    hr{

        border-color:#e0e0e0;
    }
    .blue-border{
        border: 3px solid #80b1cd;
        border-radius: 6px;
    }
    .add-btn{
        margin-bottom: -62px;
    }
   .inner-white{
        margin-bottom:30px;
        padding:10px;
    }
    .inner-white,.blue-border{
        background: #ffffff;
        padding: 20px 30px;
    }
    .lg-input, .md-input{
        padding-bottom: 24px;
    }

     .span-right-padding{
         padding-right: 34px;
     }
    #orderImg{
        background:url("assets/icons/orders.png")no-repeat ;
    }
   
    #purchaseImg{
        background:url("assets/icons/purchases.png")no-repeat ;
    }
    #storeImg{
        background:url("assets/icons/stores.png")no-repeat ;
    }
    #listingsImg{
        background:url("assets/icons/listings.png")no-repeat ;
    }
    #auctionsImg{
        background:url("assets/icons/auctions.png")no-repeat ;
    }
    #membersImg{
        background:url("assets/icons/members.png")no-repeat ;
    }
    #videosImg{
        background:url("assets/icons/videos.png")no-repeat ;
    }
    #dashboardImg.active{
        /**background:url("assets/icons/dashboard-active.png")no-repeat;*/
        color:rgb(22, 103, 160);

    }
    #purchaseImg.active{
        background:url("assets/icons/purchases-active.png")no-repeat;

    }
    #orderImg.active{
        background:url("assets/icons/orders-active.png")no-repeat;

    }
    #storeImg.active{
        background:url("assets/icons/stores-active.png")no-repeat;
    }
    #listingsImg.active{
        background:url("assets/icons/listings-active.png")no-repeat;
    }
    #auctionsImg.active{
        background:url("assets/icons/auctions-active.png")no-repeat;
    }
    #membersImg.active{
        background:url("assets/icons/members-active.png")no-repeat;
    }
    #videosImg.active{
        background:url("assets/icons/videos-active.png")no-repeat;
    }
    /**.active{
        color:rgb(22, 103, 160);
    }
       */
    /*.margin-top-3{*/
        /*margin-top: 3%;*/
    /*}*/

</style>

<div class="row" style="padding: 17px 0 17px;">

    <!-- Tab panes -->

    <div id="adminDashbaordTab"  class="tab-content row" style="padding-left: 40px;">

        <div role="tabpanel" class="tab-pane active" id="dashboard" >
            <div class='row row-blue' style="background-color:#0b69a0;">

                <div class="container container-blue-magic" style="padding-bottom: 4%;;">
                    <div class="col-md-12" style="padding-left: 27px;"><h3 class="white-font">Account Overview</h3>
                        <div class="col-xs-12 col-sm-6 col-md-2 border-radius" style="background-color:#3d8bbd; padding: 22px 6px;">
                            <div class="col-md-4" style="padding: 8px 13px;"><img src="<?php echo base_url().'assets/icons/sales.png';?>"></div>
                            <div class="col-md-8 white-font">
                                <p class="light-blue-font less-medium-font no-margin">Total Sales</p>
                                <p class="large-font no-margin word-wrap">$<?=$total_sales;?></p></div>
                            <div class="col-md-4 no-right-padding">
                                <p class="light-blue-font small-font no-margin">Last Month</p>
                                <p class="medium-font white-font no-margin word-wrap">$<?=$lm_total_sales;?></p></div>
                            <div class="col-md-4 col-md-offset-4 no-left-right-padding">
                                <p class="light-blue-font small-fon no-margin">Last Week</p>
                                <p class="medium-font white-font no-margin word-wrap">$<?=$lw_total_sales;?></p></div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-2 border-radius" style="background-color:#d9534e; padding: 19px 6px;">
                            <div class="col-md-4" style="padding: 8px 13px;"><img src="<?php echo base_url().'assets/icons/order.png'?>"></div>
                            <div class="col-md-8 white-font">
                                <p class="light-red-font less-medium-font no-margin">Total Orders</p>
                                <p class="large-font no-margin word-wrap"><?=$total_orders;?></p></div>
                            <div class="col-md-4 no-right-padding">
                                <p class="light-red-font small-font no-margin">Last Month</p>
                                <p class="medium-font white-font no- word-wrap"><?=$lm_total_orders;?></p></div>
                            <div class="col-md-4 col-md-offset-4 no-left-right-padding">
                                <p class="light-red-font small-fon no-margin">Last Week</p>
                                <p class="medium-font white-font no-margin word-wrap"><?=$lw_total_orders;?></p></div>
                        </div>
                        <div class=" col-xs-12 col-sm-6 col-md-offset-1 col-md-2 border-radius" style=";background-color:#eab33d; padding: 22px 6px;">
                            <div class="col-md-4" style="padding: 8px 13px;"><img src="<?php echo base_url().'assets/icons/moneyowed.png'?>"></div>
                            <div class="col-md-8 white-font">
                                <p class="light-yellow-font less-medium-font no-margin">Money Owned</p>
                                <p class="large-font no-margin word-wrap">$<?=$total_money_owned;?></p></div>
                            <div class="col-md-4 no-right-padding">
                                <p class="light-yellow-font small-font no-margin">Last Month</p>
                                <p class="medium-font white-font no-margin word-wrap">$<?=$lm_total_money_owned;?></p></div>
                            <div class="col-md-4 col-md-offset-4 no-left-right-padding">
                                <p class="light-yellow-font small-fon no-margin">Last Week</p>
                                <p class="medium-font white-font no-margin wrap" style=" ">$<?=$lw_total_money_owned;?></p></div>
                        </div>
                        <div class=" col-xs-12 col-sm-6 col-md-offset-1 col-md-2 border-radius" style="background-color:#4aba77; padding: 22px 6px;">
                            <div class="col-md-4" style="padding: 8px 13px;"><img src="<?php echo base_url().'/assets/icons/moneypaid.png';?>"></div>
                            <div class="col-md-8 white-font">
                                <p class="light-green-font less-medium-font no-margin">Money Paid</p>
                                <p class="large-font no-margin word-wrap">$<?=$total_paid_money_minus_fee;?></p></div>
                            <div class="col-md-4 no-right-padding">
                                <p class="light-green-font small-font no-margin">Last Month</p>
                                <p class="medium-font white-font no-margin word-wrap">$<?=$lm_total_paid_money_minus_fee;?></p></div>
                            <div class="col-md-4 col-md-offset-4 no-left-right-padding">
                                <p class="light-green-font small-fon no-margin">Last Week</p>
                                <p class="medium-font white-font no-margin word-wrap">$<?=$lw_total_paid_money_minus_fee;?></p></div>
                        </div>
                    </div>

                </div>

            </div>


            <!--sales order preview-->
            <div class="container">
           <?php $this->load->view('admin/sales_overview_view.php');?>

            <div class="col-sm-12 no-left-right-padding"> <hr></div>
            </div>

            <!--End of sales order preview-->

            <!--Purchase overview-->
            <div class="container">
            <?php $this->load->view('admin/purchase_overview.php');?>

            <div class="col-sm-12 no-left-right-padding"> <hr></div>
            </div>
            <!--End of Purchase overview-->

            <!--Listing overview-->
            <div class="container">
            <div class="col-md-6 no-left-padding">
                <?php $this->load->view('admin/listings_overview.php');?>

            </div>

            <!--End of Listing overview-->
            <!--Stores overview-->

            <div class="col-md-6 no-right-padding">
                <?php $this->load->view('admin/stores_overview.php');?>

            </div>
            </div>

            <!--End of Stores overview-->
            <div class="col-md-12 no-left-right-padding">
                <hr style="margin-top: 23px;">
            </div>


            <!--Auctions overview-->
            <div class="container">
            <?php $this->load->view('admin/auctions_overview.php');?>
            </div>




            <!--End of auctions overview -->


        </div>
        <div role="tabpanel" class="tab-pane container margin-top-3"  id="orders">


            <?php $this->load->view('admin/sales_overview_view.php');?>

        </div>
        <!--Purchases-->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="purchases">

            <?php $this->load->view('admin/purchase_overview.php');?>

        </div>
        <!--end purchases-->

        <!--stores-->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="stores" >
            <?php $this->load->view('admin/stores_form.php');?>

            <?php $this->load->view('admin/stores_overview.php');?>
        </div>
        <div role="tabpanel" class="tab-pane container margin-top-3" id="storesDetail">
        <div class="col-md-12 no-left-right-padding white-bg">
            <?php $this->load->view('admin/preview_store_view/store_page_new.php');?>
            <?php $this->load->view('admin/preview_store_view/addproduct_new.php');?>
            <?php $this->load->view('admin/preview_store_view/getpaid_new.php');?>
            
        </div>
        </div>

        <!--Stores end-->
        <!--listings-->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="listings">
            <?php $this->load->view('admin/listings_overview.php');?>

        </div>
        <div role="tabpanel" class="tab-pane container" id="listingDetail">
            <div class="col-md-12 no-left-right-padding white-bg">
               
                     <?php $this->load->view('admin/preview_listing/product_new.php');?>
               
            </div>

        </div>
        <!--end of listings-->

        <!-- auctions -->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="auctions">

            <?php $this->load->view('admin/auctions_overview.php');?>

        </div>
        <!-- end of auctions -->

        <!--video tab-->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="members">

            <?php $this->load->view('admin/members_view.php');?>

        </div>


        <!--end of message tab-->
        <!--photos tab-->
        <div role="tabpanel" class="tab-pane container margin-top-3" id="videos">
            <?php $this->load->view('admin/videos_view.php');?>
        </div>
        <!--end of  tab-->

