
<style type="text/css">

.xpens {

     -webkit-background-size: cover; /* For WebKit*/
     -moz-background-size: cover;    /* Mozilla*/
     -o-background-size: cover;      /* Opera*/
     background-size: cover;         /* Generic*/
  }

.thumbnail .caption h4 {
  margin-top: 0px;
  font-size: larger;
  overflow: hidden;
}

.thumbnail .caption {
  padding: 2px;
}

</style>

<style type="text/css">

    .xpens {

        -webkit-background-size: cover; /* For WebKit*/
        -moz-background-size: cover;    /* Mozilla*/
        -o-background-size: cover;      /* Opera*/
        background-size: cover;         /* Generic*/
    }
    .thumbnail{
        border:none;
    }
    .thumbnail .caption h4 {
        font-size: larger;
        overflow: hidden;
        height: 35px;
        margin-top: 7px;
        margin-bottom:5px;
    }
    .delete_btn {
        background-color: #3E3F3F;
        color: white;
        float:right;
        border-radius: 2px;
    }
    .thumbnail .caption {
        padding: 2px 7px !important;
    }
    .rating_class {
        float: right;
    }
    .rating-xs {
        font-size: 1.6em;
    }

    .rating-gly-star {
        font-family: 'Glyphicons Halflings';
        padding-left: 2px;
        font-size: smaller;
    }

    .star-rating .caption, .star-rating-rtl .caption {
        color: #999;
        display: inline-block;
        vertical-align: middle;
        float: right;
        font-size: 80%;
    }

    .edit-delete-toggle {
        display: none;
    }
    .seller-name{
        padding-left:4px;color: #1f72ad;
    }
    .rating-container{
        vertical-align: top;
    }
    .view-more-btn{

        background: transparent;
        width: 100%;
        padding: 8px 0px 0px;
    }
    .buy-text-p{
        margin-top: 0% !important;
    }
</style>



<?php if(count($products)>0): ?>

    <?php $products_array = (array)$products; ?>
	<div class="col-md-12 no-left-padding no-right-padding" style="display:block; width:99%; padding-right:0px !important; margin-bottom:10px; background: white; border-bottom:1px solid dark gray; ">
				<span style="font-size:1.8em; display:block; width:96%; padding-right:2%; margin-bottom:-6px; float:right;text-align:right; text-indent:5px; line-height:1.6em; font-weight:bold;font-style: italic; text-transform: uppercase;"><?php echo $products_array[0]->store_name; ?></span><br>
  				<span style="display:block; vertical-align:middle; float:right; width:98%;padding-bottom:4px; padding-right:2%; text-align:right; font-size:.7em; color:blue; text-indent:5px;" class="">Open Since <?php echo date('m/d/Y',strtotime($products_array[0]->opened));?> </span>
	</div>		

	<?php foreach($products as $product): ?>

        <div class="col-sm-3 col-md-3 no-left-padding">

            <div class="thumbnail" style="height:383px">
                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product->product_id,'buy')): site_url(array('product', 'detail', $product->product_id,'bid')) ;?>">

                        <img  alt="<?php echo $product->name;?>"  class="img-thumbnail img-responsive xpens"
                              src="<?php echo $product->image ;?>"
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>

                <div class="caption">
                    <div class="col-sm-10 no-left-right-padding" style="height: 31px;">
                        <p class='product_name_heading'><?php echo character_limiter($product->name,20);?></p>
                        <!--<p class="product_description"><?php //echo $product['desc'];?></p> -->
                    </div>
                    <div class="col-sm-2">
                        <button id="heartBtn" class="btn heart-btn active"><i class="glyphicon glyphicon-heart"></i></button>
                    </div>
                    <div class="col-sm-12  no-left-right-padding" style="padding: 7px 0px 0px;">
                        <div class="pull-left" style="height: 61px;">
                            <span class="dollar-sign" style="padding-top:0px">$</span><span style="padding-top:0px" class='price no-left-padding'><?php echo $product->price;?></span>
                            <p class="buy-text-p"><small class="buy-text">USD</small></p>
                        </div>
                        <div class="pull-right" >
						  <span class="buy_btn viewDetails">
							<a  href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product->product_id,'buy')): site_url(array('product', 'detail', $product->product_id,'bid')) ;?>" class="btn btn-primary" role="button">BUY NOW</a>
						  </span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-12 no-left-right-padding">
                        <hr>
                    </div>
                    <div class="col-sm-6 no-left-padding">
                        <span class="user-sm"></span>
				   <span class="black-font">

					<a href="<?php echo base_url('sell/seller').'/'.$product->profile_id;?>">
                        <b class="black-font"><?php  echo $product->seller_name ;?></b></a>
				   </span>
                    </div>
                    <div class="col-sm-6" style="padding-left: 28px;">
                        <p>
                            <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product->product_id;?>" value="<?= $product->product_rating ;?>" data-type="product" />
                        </p>


                    </div>

                </div>
           


            </div>
      </div>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>

