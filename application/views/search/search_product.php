<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/buy.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">

</head>


<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

      <?php $this->load->view($header_black_menu); ?>
      <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">
    
     <!--load menu here -->
     <?php $this->load->view($main_menu); ?>

<div class="container">
   
           

               <div class="showings">

                        <div class="col-md-12 showing_text">
                 
                       <div class="col-md-offset-1">
                                  
                                   
                      <span class='showing'>Showing &nbsp;( <span class='element_count'><?=$total_rows;?>,<?=$per_page;?></span> )&nbsp;&nbsp;results </span>
                        

                      <span class='pull-right showing-item'>ITEM PER PAGE:&nbsp; (<span class='element_count'><?=$per_page;?>,<?=$total_rows;?>,</span>)&nbsp;&nbsp;<a href="#">12</a>&nbsp;<a href="#">15</a>&nbsp;<a href="#">18</a>&nbsp;<a href="#">21</a></span>
                      </div>
                                
                               
                        
                        </div>
            </div>   

     <hr class="hr_border" style="width: 97%;">

            <div class="products">                 
          
               <div class="row row-no-margin">                  
            
                    <?php $this->load->view($product_listing,$products); ?>
           
        


             </div><!--nio margin row end-->
            <div class="col-md-12" style="padding-bottom:6%;">
                        <hr class="hr_border_bottom" style="margin-top:0px;">
                        <div class="pull-right">
                                <?php $this->load->view($paginate_page,$links); ?>
                        </div>    
                </div>

        </div>


    
    </div>


</div>





</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
  <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>

  <script>
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
       
        $(".rating").rating('refresh', 
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {
          
          
           if(!readOnly) {
           //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");      
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

          }
           else {
            window.location.assign("<?=site_url('users/login');?>");
           }

          

        });

 
 
       
    });
</script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>

<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>
</body>
</html>
