<div class="tab-pane fade col-md-6 col-md-offset-3 white-bg" id="second" role="tabpanel">
    <div class="col-md-12">
        <h3 class="blue-font">1. Enter Your Shipping Adress</h3>
        <hr class="no-top-margin">
    </div>
    <div class="col-md-12 ">
        <div class="row bottom-margin-10">
            <div class="col-md-6">
                <label for="firstName">First Name</label>
                <input class="form-control" id="firstName">
            </div>

            <div class="col-md-6">
                <label for="lastName">Last Name</label>
                <input class="form-control" id="lastName">
            </div>
        </div>
        <div class="row bottom-margin-10">

            <div class="col-md-12">
                <label for="emailAdress">Email Adress</label>
                <input class="form-control" id="emailAdress">
            </div>
        </div>
        <div class="row bottom-margin-10">
            <div class="col-md-7">
                <label for="street">Street</label>
                <input class="form-control" id="street">
            </div>
            <div class="col-md-5">
                <label class="col-md-12 no-left-right-padding" for="street">Apt/Suit/Other<span class="dark-grey-font pull-right font-size-11 padding-top-6">optional</span></label>
                <input class="form-control" id="street">
            </div>
        </div>
        <div class="row bottom-margin-10">
            <div class="col-md-4">
                <label for="city">City</label>
                <input class="form-control" id="city">
            </div>
            <div class="col-md-4">
                <label for="state">State</label>
                <select class="form-control" id="street">
                    <option></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="postalCode">Postal Code</label>
                <input class="form-control" id="postalCode">
            </div>
        </div>
        <a href="#third" role="tab" data-toggle="tab" class="btn btn-primary full-width"><span class="font-size-16">Continue</span></a>

    </div>

</div>
    