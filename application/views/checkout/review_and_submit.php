<div class="tab-pane fade col-md-12 white-bg padding-top" id=fourth role="tabpanel">
    <h3 class="blue-font">3. Review and Submit Your Order  </h3>
    <hr>
    <div class="col-md-12 no-left-padding">
        <div class="col-md-4 no-left-padding ">

            <h4>Shipping Adress <span class="blue-font font-size-11 bold"><a>Change</a></span></h4>
            <div class="col-md-12 border border-radius">
                <p class="dark-grey-font no-margin">Jonatan Daaseet</p>
                <p class="dark-grey-font">jona@email.com</p>

                <p class="dark-grey-font bold no-margin">1236 Chestrut Streee</p>
                <p class="dark-grey-font">San francisoc, CA 099977</p>
            </div>


        </div>
        <div class="col-md-4 ">

            <h4>Payment Method <span class="blue-font font-size-11 bold"><a>Change</a></span></h4>
            <div class="col-md-12 border border-radius padding-top-6 fixed-hegiht-94">
                <div class="col-md-1 no-left-padding">
                    <img src="<?php echo base_url()."assets/images/cart_and_checkout/card-icon.png"; ?>">
                </div>
                <div class="col-md-4">
                    <p class=""><span class="dark-grey-font">Visa...</span><span class="bold black-font">2434</span></p>
                    <p class=""><span class="dark-grey-font">Exp:09/21/2018</span></p>

                </div>


            </div>


        </div>
        <div class="col-md-4">

            <h4>Choose a shipping method</h4>
            <div class="col-md-12 border border-radius padding-top-6 fixed-hegiht-94">
                <div class="radio col-md-12 no-bottom-margin">
                    <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        FedEx
                    </label>
                    <span class="pull-right">$26.45</span>
                </div>
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        UPS
                    </label>
                    <span class="pull-right">$26.45</span>
                </div>
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        DHL
                    </label>
                    <span class="pull-right">$26.45</span>
                </div>


            </div>


        </div>
    </div>
    <div class="col-md-12"><h3 class="bold">3 Items in Shopping Cart</h3></div>
    <div class="col-md-8 padding-top border border-radius">
        <div class="col-md-12 no-left-right-padding">
            <div class="col-md-3 no-left-padding">
                <div class="thumbnail col-md-12">
                    <img class="thumbnail-img-height" src="<?php echo base_url()."assets/images/big-watch.jpg"; ?>">
                </div>


            </div>

            <div class="col-md-9 no-left-right-padding">
                <div class="col-md-8 no-left-padding">
                    <p class="blue-font no-top-margin font-size-20">Samsung Smart Watch - Bluetooth Compatible with Red Face</p>
                </div>
                <div class="col-md-4 no-left-right-padding">


                    <button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

                    <span class="red-font">Remove this Item</span>


                </div>


                <div class="col-md-12 padding-top no-left-padding">
                    <div class="col-md-2 no-left-padding padding-top-6">
                        <p class="font-size-16 dark-grey-font">Quantity</p>
                    </div>

                    <div class="col-md-2 no-left-padding">

                        <select class="form-control" id="quantity">
                            <option>1</option>
                        </select>
                    </div>
                    <div class="col-md-2 no-left-padding ">
                        <p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24">49.99</span><span class="blue-font">&nbspUSD</span></p>
                    </div>

                </div>
                <div class="col-md-12 padding-top no-left-padding">
                    <p class="padding-top-6">
                    <div class="col-md-3"><span class="dark-grey-font ">Item Listed By:</span></div>
                    <div class="col-md-5 no-left-padding">
                        <span class="pull-left"><img src="<?php echo"assets/icons/user_sm.png"?>"></span>
                        <span class="bold">&nbsp Nora Feticmer</span>
                        </p>
                    </div>




                </div>
            </div>

            <hr class="col-md-12 no-left-right-padding">

        </div>
        <div class="col-md-12 no-left-right-padding">
            <div class="col-md-3 no-left-padding">
                <div class="thumbnail col-md-12">
                    <img class="thumbnail-img-height" src="<?php echo base_url()."assets/images/big-watch.jpg"; ?>">
                </div>


            </div>

            <div class="col-md-9 no-left-right-padding">
                <div class="col-md-8 no-left-padding">
                    <p class="blue-font no-top-margin font-size-20">Samsung Smart Watch - Bluetooth Compatible with Red Face</p>
                </div>
                <div class="col-md-4 no-left-right-padding">


                    <button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

                    <span class="red-font">Remove this Item</span>


                </div>


                <div class="col-md-12 padding-top no-left-padding">
                    <div class="col-md-2 no-left-padding padding-top-6">
                        <p class="font-size-16 dark-grey-font">Quantity</p>
                    </div>

                    <div class="col-md-2 no-left-padding">

                        <select class="form-control" id="quantity">
                            <option>1</option>
                        </select>
                    </div>
                    <div class="col-md-2 no-left-padding ">
                        <p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24">49.99</span><span class="blue-font">&nbspUSD</span></p>
                    </div>

                </div>
                <div class="col-md-12 padding-top no-left-padding">
                    <p class="padding-top-6">
                    <div class="col-md-3"><span class="dark-grey-font ">Item Listed By:</span></div>
                    <div class="col-md-5 no-left-padding">
                        <span class="pull-left"><img src="<?php echo"assets/icons/user_sm.png"?>"></span>
                        <span class="bold">&nbsp Nora Feticmer</span>
                        </p>
                    </div>




                </div>
            </div>

            <hr class="col-md-12 no-left-right-padding">

        </div>



    </div>
    <div class="col-md-4">
        <div class="col-md-12 waterish-blue-border border-radius no-left-right-padding padding-bottom-16">
            <div class="col-md-12 blue-bg">
                <h4 class="white-font">Shopping Cart Summary <span class="
glyphicon glyphicon-chevron-down font-size-11"></span></h4>

            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5"><p class="font-size-16 pull-right padding-top-6">Item Total:</p></div>
                    <div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$425.00 </span><span class="blue-font bold">USD</span></p></div>
                </div>
                <div class="row">
                    <div class="col-md-5 no-left-padding"><p class="font-size-16 pull-right padding-top-6">Shipping Cost:</p></div>
                    <div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$425.00 </span><span class="blue-font bold">USD</span></p></div>
                </div>
                <div class="row">
                    <div class="col-md-5 no-left-padding"><p class="font-size-16 pull-right padding-top-6">Shipping Total:</p></div>
                    <div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$425.00 </span><span class="blue-font bold  ">USD</span></p></div>
                </div>

                <div class="col-md-12 grey-bg padding-top border-radius">
                    <div class="col-md-5 no-left-padding padding-top-6">
                        <p><span class="font-size-16 bold">Order Total:</span></p>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <p><span class="font-size-24 dark-grey-font">$</span><span class="font-size-24">209.97</span><span class="blue-font bold font-size-16">USD</span></p>
                    </div>
                </div>

                <div class="col-md-12 no-left-right-padding padding-top-6">
                    <hr  class="no-top-margin">
                </div>
                <div class="col-md-12 no-left-right-padding">
                    <button class="btn btn-primary full-width"><h4><span><img src="<?php echo base_url()."assets/icons/shopping-cart-big-white.png";?>"></span>&nbsp Confirm Order</h3></button>
                </div>


            </div>

        </div>
        <div class="col-md-12 padding-top padding-bottom-10">
            <div class="col-md-2 no-left-padding"><img src="<?php echo base_url()."assets/images/cart_and_checkout/user-circle.png"; ?>"> </div>
            <div class="col-md-10 no-left-padding">Add an optioal note to the seller</div>

        </div>
        <div class="col-md-12 border border-radius padding-top"><p class="dark-grey-font">Write your note to a seller here  by tagging there username.</p></div>
    </div>


</div>