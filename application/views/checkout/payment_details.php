<div class=" tab-pane fade col-md-6 col-md-offset-3 white-bg" id="third" role="tabpanel">
    <div class="col-md-12">
        <h3 class="blue-font">2. Enter Your Payment Details</h3>
        <hr class="no-top-margin">
    </div>
    <div class="col-md-12 ">
        <div class="col-md-12 padding-top-6 no-left-padding">
            <p class=" font-size-16 bottom-margin-10">Payment Methods</p>
            <div class="row bottom-margin-10">
                <form>
                    <div class="col-md-2 radio no-margin">
                        <label>
                            <input type="radio" id="payment" value="1" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/visa.png";?>"></span>
                        </label>

                    </div>
                    <div class="col-md-2 radio">
                        <label>
                            <input type="radio" id="payment" value="2" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/master.png";?>"></span>
                        </label>
                    </div>
                    <div class="col-md-2 radio">
                        <label>
                            <input type="radio" id="payment" value="3" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/pay1.png";?>"></span>
                        </label>
                    </div>
                    <div class="col-md-2 radio">
                        <label>
                            <input type="radio" id="payment" value="4" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/pay2.png";?>"></span>
                        </label>
                    </div>
                    <div class="col-md-2 radio">
                        <label>
                            <input type="radio" id="payment" value="5" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/discover.png";?>"></span>
                        </label>
                    </div>


            </div>
            </form>
        </div>

    </div>
    <div class="col-md-12">
        <div class="row bottom-margin-10">

            <div class="col-md-12">
                <label for="emailAdress">Card Number</label>

                <div class="input-group">
                    <div class="input-group-addon"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/card-icon.png";?>"></span></div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="">

                </div>
            </div>
        </div>

        <div class="row bottom-margin-10">
            <div class="col-md-3">
                <label for="expire">Expiration Date</label>
                <input class="form-control" id="expire">
            </div>
            <div class="col-md-3">
                <label for=""> &nbsp</label>
                <select class="form-control" id="">
                    <option></option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="postalCode">Security Code</label>
                <input class="form-control" id="postalCode">
            </div>
            <div class="col-md-1 padding-que">
                <button class="no-border bg-transparent"> <img src="<?php echo base_url()."assets/images/cart_and_checkout/que2.png";?>"></button>

            </div>
        </div>
        <div class="row bottom-margin-10">

            <div class="col-md-12">
                <label for="emailAdress">Email Adress</label>
                <input class="form-control" id="emailAdress">
            </div>
        </div>

        <a href="#fourth" role="tab" data-toggle="tab" class="btn btn-primary full-width"><span class="font-size-16">Review Your Checkout Cart</span></a>

    </div>


</div>