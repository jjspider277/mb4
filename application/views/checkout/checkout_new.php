<div class="tab-content">
    <div class="row tab-pane fade in active" role="tabpanel">
        <div class="container">
            <div class="col-md-12 blue-top-border white-bg no-left-right-padding padding-top  bottom-margin-28">
                <div class="col-md-1">
                    <img src="<?php echo base_url()."assets/icons/checkout-blue.png";?>"/>

                </div>

                <div class="col-md-3 no-left-padding">
                    <h4><span class="blue-font">3 Itmes</span> in your MadeByUs4u</h4>

                </div>
                <div class="col-md-3  col-md-offset-3">
                    <h4 class="pull-right italic"> Not finished Shopping?</h4>
                </div>
                <div class="col-md-2 no-right-padding">
                    <button class="btn btn-primary black-bg no-border ">Continue Shopping</button>
                </div>

            </div>




            <div class="col-md-12 white-bg padding-top">
                <div class="col-md-8 padding-top border border-radius">
                    <div class="col-md-12 no-left-right-padding">
                        <div class="col-md-3 no-left-padding">
                            <div class="thumbnail col-md-12">
                                <img class="thumbnail-img-height" src="<?php echo base_url()."assets/images/big-watch.jpg"; ?>">
                            </div>
                            <div class="col-md-12 no-left-padding">


                                <button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

                                <span class="red-font">Remove this Item</span>


                            </div>

                        </div>

                        <div class="col-md-9">
                            <div class="col-md-11 no-left-padding">
                                <h3 class="blue-font no-top-margin">Samsung Smart Watch - Bluetooth Compatible with Red Face</h3>
                            </div>


                            <div class="col-md-12 padding-top no-left-padding">
                                <div class="col-md-2 no-left-padding padding-top-6">
                                    <p class="font-size-16 dark-grey-font">Quantity</p>
                                </div>

                                <div class="col-md-2 no-left-padding">

                                    <select class="form-control" id="quantity">
                                        <option>1</option>
                                    </select>
                                </div>
                                <div class="col-md-1 ">
                                    <p class="font-size-22 dark-grey-font">|</p>
                                </div>
                                <p class="padding-top-6">
                                    <span class="dark-grey-font">Item Listed By:</span>
                                    <span><img src="<?php echo"assets/icons/user_sm.png"?>"></span>
                                    <span class="bold">Nora Feticmer</span>
                                </p>
                            </div>
                            <div class="col-md-12 padding-top no-left-padding">
                                <div class="col-md-2 no-left-padding ">
                                    <p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24">49.99</span><span class="blue-font">&nbspUSD</span></p>
                                </div>


                                <div class="col-md-1 col-md-offset-2 ">
                                    <p class="font-size-22 dark-grey-font">|</p>
                                </div>
                                <p class="padding-top-6">
                                    <span class="dark-grey-font">Shipping Cost:</span>

                                    <span class="bold">$22.45</span>
                                </p>
                            </div>
                        </div>

                        <hr class="col-md-12 no-left-right-padding">

                    </div>

                    <div class="col-md-12 no-left-right-padding">
                        <div class="col-md-3 no-left-padding">
                            <div class="thumbnail col-md-12">
                                <img class="thumbnail-img-height" src="<?php echo base_url()."assets/images/big-watch.jpg"; ?>">
                            </div>
                            <div class="col-md-12 no-left-padding">


                                <button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

                                <span class="red-font">Remove this Item</span>


                            </div>

                        </div>

                        <div class="col-md-9">
                            <div class="col-md-11 no-left-padding">
                                <h3 class="blue-font no-top-margin">Samsung Smart Watch - Bluetooth Compatible with Red Face</h3>
                            </div>


                            <div class="col-md-12 padding-top no-left-padding">
                                <div class="col-md-2 no-left-padding padding-top-6">
                                    <p class="font-size-16 dark-grey-font">Quantity</p>
                                </div>

                                <div class="col-md-2 no-left-padding">

                                    <select class="form-control" id="quantity">
                                        <option>1</option>
                                    </select>
                                </div>
                                <div class="col-md-1 ">
                                    <p class="font-size-22 dark-grey-font">|</p>
                                </div>
                                <p class="padding-top-6">
                                    <span class="dark-grey-font">Item Listed By:</span>
                                    <span><img src="<?php echo"assets/icons/user_sm.png"?>"></span>
                                    <span class="bold">Nora Feticmer</span>
                                </p>
                            </div>
                            <div class="col-md-12 padding-top no-left-padding">
                                <div class="col-md-2 no-left-padding ">
                                    <p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24">49.99</span><span class="blue-font">&nbspUSD</span></p>
                                </div>


                                <div class="col-md-1 col-md-offset-2 ">
                                    <p class="font-size-22 dark-grey-font">|</p>
                                </div>
                                <p class="padding-top-6">
                                    <span class="dark-grey-font">Shipping Cost:</span>

                                    <span class="bold">$22.45</span>
                                </p>
                            </div>
                        </div>

                        <hr class="col-md-12 no-left-right-padding">

                    </div>

                </div>
                <div class="col-md-4">
                    <div class="col-md-12 waterish-blue-border border-radius no-left-right-padding padding-bottom-16">
                        <div class="col-md-12 blue-bg">
                            <h4 class="white-font">Shopping Cart Summary <span class="
glyphicon glyphicon-chevron-down font-size-11"></span></h4>

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12 padding-top-6 no-left-padding">
                                <p class="dark-grey-font font-size-16 no-bottom-margin">Payment Methods</p>
                                <img src="<?php echo base_url()."assets/images/cart_and_checkout/payment.jpg";?>">

                            </div>
                            <div class="col-md-12 no-left-right-padding padding-top-6">
                                <hr  class="no-top-margin">
                            </div>
                            <div class="col-md-12 grey-bg padding-top border-radius">
                                <div class="col-md-5 no-left-padding padding-top-6">
                                    <p><span class="font-size-16 dark-grey-font">Item Total:</span></p>
                                </div>
                                <div class="col-md-6 col-md-offset-1">
                                    <p><span class="font-size-24 dark-grey-font">$</span><span class="font-size-24">209.97</span><span class="blue-font bold">USD</span></p>
                                </div>
                            </div>
                            <div class="col-md-12 g padding-top border-radius">
                                <div class="col-md-1 no-left-padding ">
                                    <img src="<?php echo base_url()."assets/icons/shipping-ico.png";?>">
                                </div>
                                <div class="col-md-10">
                                    <p><span class="blue-font bold">Get Shipping Cost</span></p>
                                </div>
                            </div>
                            <div class="col-md-12 no-left-right-padding padding-top-6">
                                <hr  class="no-top-margin">
                            </div>
                            <div class="col-md-12 no-left-right-padding">
                                <a href="#first" role="tab" data-toggle="tab" class="btn btn-primary full-width"><h4><span><img src="<?php echo base_url()."assets/icons/shopping-cart-big-white.png";?>"></span>&nbspProceed to Checkout</h3></a>
                            </div>


                        </div>

                    </div>
                </div>


            </div>

        </div>


    </div>
    <!--confirm shipping adrress-->
    <?php $this->load->view('checkout/confirm_shipping_adress');?>

    <!--end of confirm shipping address
    <!--shipping form step-1 -->
    <?php $this->load->view('checkout/shipping_adress');?>
    <!-- end of form -->

    <!-- shipping form step-2 -->
    <div class=" tab-pane fade col-md-6 col-md-offset-3 white-bg" id="second" role="tabpanel">
        <div class="col-md-12">
            <h3 class="blue-font">2. Enter Your Payment Details</h3>
            <hr class="no-top-margin">
        </div>
        <div class="col-md-12 ">
            <div class="col-md-12 padding-top-6 no-left-padding">
                <p class=" font-size-16 bottom-margin-10">Payment Methods</p>
                <div class="row bottom-margin-10">
                    <form>
                        <div class="col-md-2 radio no-margin">
                            <label>
                                <input type="radio" id="payment" value="1" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/visa.png";?>"></span>
                            </label>

                        </div>
                        <div class="col-md-2 radio">
                            <label>
                                <input type="radio" id="payment" value="2" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/master.png";?>"></span>
                            </label>
                        </div>
                        <div class="col-md-2 radio">
                            <label>
                                <input type="radio" id="payment" value="3" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/pay1.png";?>"></span>
                            </label>
                        </div>
                        <div class="col-md-2 radio">
                            <label>
                                <input type="radio" id="payment" value="4" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/pay2.png";?>"></span>
                            </label>
                        </div>
                        <div class="col-md-2 radio">
                            <label>
                                <input type="radio" id="payment" value="5" name="payment"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/discover.png";?>"></span>
                            </label>
                        </div>


                </div>
                </form>
            </div>

        </div>
        <div class="col-md-12">
            <div class="row bottom-margin-10">

                <div class="col-md-12">
                    <label for="emailAdress">Card Number</label>

                    <div class="input-group">
                        <div class="input-group-addon"><span><img src="<?php echo base_url()."assets/images/cart_and_checkout/card-icon.png";?>"></span></div>
                        <input type="text" class="form-control" id="exampleInputAmount" placeholder="">

                    </div>
                </div>
            </div>

            <div class="row bottom-margin-10">
                <div class="col-md-3">
                    <label for="expire">Expiration Date</label>
                    <input class="form-control" id="expire">
                </div>
                <div class="col-md-3">
                    <label for=""> &nbsp</label>
                    <select class="form-control" id="">
                        <option></option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="postalCode">Security Code</label>
                    <input class="form-control" id="postalCode">
                </div>
                <div class="col-md-1 padding-que">
                    <button class="no-border bg-transparent"> <img src="<?php echo base_url()."assets/images/cart_and_checkout/que2.png";?>"></button>

                </div>
            </div>
            <div class="row bottom-margin-10">

                <div class="col-md-12">
                    <label for="emailAdress">Email Adress</label>
                    <input class="form-control" id="emailAdress">
                </div>
            </div>

            <a href="#third" role="tab" data-toggle="tab" class="btn btn-primary full-width"><span class="font-size-16">Review Your Checkout Cart</span></a>

        </div>


    </div>

    <!-- end of form-->
    <!--payment form -->
    <?php $this->load->view('checkout/payment_details');?>
    <!--end of payment form-->

    <!-- shipping preview -->
    <?php $this->load->view('checkout/review_and_submit');?>
    <!-- shipping preview ends here -->

</div>