< <div class="tab-pane fade col-md-6 col-md-offset-3 white-bg" id="first"role="tabpanel">
    <div class="col-md-12">
        <h3 class="blue-font">1. Confirm Your Shipping Adress</h3>
        <hr class="no-top-margin">
    </div>
    <div class="col-md-12 ">
        <p class="dark-grey-font">
            To ensure accurate delivery, MBU4U recommends theat you review your shipping address one last time before moving forward.
        </p>
        <div class="col-md-6 no-left-right-padding">
            <div class="radio col-md-12 no-left-padding">
                <label for="shippingInfo">
                    <input type="radio" checked id="shippingInfo"><span class="bold font-size-16">Your Shopping Information</span>
                </label>
            </div>
            <div class="col-md-11 col-md-offset-1 no-left-padding"> <p class="dark-grey-font font-size-16 no-margin">Johnatan Doeset</p>
                <p class="dark-grey-font font-size-16">1236 Francisco St.</p></div>
            <div class="col-md-11 col-md-offset-1 no-left-padding"> <p class="bold font-size-16 no-margin">Johnatan Doeset</p>
                <p class="dark-grey-font font-size-16">Sanfrancisco, Ca 89787</p></div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12 no-left-right-padding bottom-margin-10">
                <a href="#third" role="tab" data-toggle="tab" class="btn green-bg btn-lg white-font full-width">Use This Adress</a>
            </div>
            <div class="col-md-12 no-left-right-padding">
                <a href="#second" role="tab" data-toggle="tab" class="btn black-bg btn-lg white-font full-width"> Change Adress</a>
            </div>
        </div>
    </div>


</div>