<?php if($is_logged_in==true && $loaded_profile_same_as_user==true) :?>

<!--apply ccs only on logged in --> 
 <style type="text/css">
 .margin-tops {
   margin-top: 13px;"
 }
 .thumbnail{
   border: 1px solid #ededed;
 }
</style> 
<div class="col-md-3 col-md-offset-9" style="margin-bottom: 16px;">

    <button class="btn btn-primary col-md-12" id="show_button">Add New Photo</button>
    <button class="btn btn-danger col-md-12" id="hide_button" style="display:none;">No ,Thankyou!</button>
    
</div>
<?php endif;?>

  <div class="col-md-12" id="upload_image_div" style="display:none;">
          <?php echo $error;?>
          <?php echo form_open_multipart('photo/upload_image');?>

                    <div class="row col-md-5 col-md-offset-7 blue-border white-bg border-radius-6 padding-8" style="margin-bottom: 10px;">
                     <div class="col-md-4">
                     <img id='preview_my_photos' name='preview_my_photos' class="thumbnail" src="<?php echo base_url('uploads/no-photo.jpg');?>" alt="..." height="100">
                     </div>
                           <div class="col-md-8">
                                            
                              <div class="col-md-4">                     
                                   
                                     <input id="my_photos_img" name="userfile" type="file" class="btn btn-primary btn-sm" accept="image/*"/> 
                                     <br/>
                                     <input id="upload_my_photos" type='submit' class="btn btn-primary btn-default" value="Upload"/>
                                      
                              </div>
                          </div>
                              
                  </div>

          </form>
                            
  </div>



<!--start listing-->



<style type="text/css">
  
.xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
.thumbnail .caption h4 {
  margin-top: 0px;
  font-size: larger;
  overflow: hidden;
}

.thumbnail .caption {
  padding: 2px;
}
 .rating_class {
  float: right;
}
.rating-xs {
  font-size: 1.6em;
}

.rating-gly-star {
  font-family: 'Glyphicons Halflings';
  padding-left: 2px;
  font-size: smaller;
  }

.star-rating .caption, .star-rating-rtl .caption {
  color: #999;
  display: inline-block;
  vertical-align: middle;
  float: right;
  font-size: 80%;
}
.view-more-btn{

    background: transparent;
    width: 100%;
    padding: 8px 0px 0px;
}
.thumbnail{

}
</style>
<br/>
<div class='row product-lisiting-pages'>
    <div class="col-md-12 ">

        <div class="col-md-4 "><h4>Viewing all Photos</h4></div>

        <div class="col-md-12"><hr></div>
    </div>
<div class='col-md-12 col-sm-6 ' style="padding: 19px 16px;">
    <div class="col-md-12 white-bg">

<?php if(count($photos)>1): ?>


    <?php foreach($photos as $photo): ?>

      <div class="col-sm-3 col-md-3 margin-tops">

            <div class="thumbnail" style="border-radius:7px; ">

                    <a class ="lightbox" rel="lightbox-photos" href="<?php echo $photo->photo_path ;?>">    
                      <img  alt="<?php echo $photo->description;?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $photo->photo_path;?>"
                              style=' text-align: center; width: 100%;height: 200px;'/> 
                    </a>
                         
            </div>
            <div>
            <h4 class="no-top-padding"><span class="black-font"><?php echo $photo->description;?></span></h4>
            <p class="grey-font"><?php echo $photo->created_date;?></p>
            </div>
      </div>



<?php endforeach ;?>
<div class="col-sm-12 no-left-right-padding"> <hr></div>
<?php elseif(count($photos)==1): ?>
  <div class="col-sm-3 col-md-3 margin-tops">
            <div class="thumbnail">

                        <img  alt="<?php echo $photos->description;?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $photos->photo_path ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'/>
                             
            </div>
      </div>
<?php else: ?>
   <div class='col-m-12 alert alert-info'><h3 class='text-center'>Photos Are Not Available!!</h3></div>
<?php endif ;?>

        </div>

</div>

    <?php //if(count($photos) > 0){?>
        <div class="col-sm-12"> <hr></div>

        <div class="col-md-12 " style="padding: 16px;">
            <div class="col-md-2 col-md-offset-5"><a class="underline dark-grey-font view-more-btn btn btn-default" type="button"><p>View More <span class="fa fa-caret-down"></span></p></a></div>
        </div>
    <?php //} ?>

</div>