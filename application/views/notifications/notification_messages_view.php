<div id="notificationContainerMessages">
    <div id="notificationTitle">Notifications</div>

    <!--show success message -->
    <div  class="alert seen-message alert-info alert-dismissible" role="alert" style="display:none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong> notification status changed. </strong>
    </div>

    <style>
        .img-circle-notification {
            border-radius: 50%;
            width:100px;
            height: 100px;
        }
    </style>



    <div id="notificationsBody" class="notifications">
        <?php if ($count_notifications > 0) : ?>
            <div class="table-responsive">
                <table class="table">

                    <thead> <tr> <th></th> <th></th> <th></th> </tr> </thead>
                    <tbody>
                    <?php foreach ($message_notifications as $notification) { ?>
                    <!-- Table -->
                    <tr><td><img class="img-circle-notification" src="<?=base_url('/uploads/profile/')."/".$notification->sender_profile_id."/"."avatar/".$notification->sender_image;?>"></td>
                        <td>
                            <?= "Sent You a Message";?>
                            <p><a href="#" class="see_all_notifications_messages">Click on this link to <button class="btn-success">View</button> or <button class="btn-info">Replay</button></a></p>
                        </td>
                        <td><?= $notification->created_date;?></td>
                        <td>
                            <a href="" class="remove-notification-li"  data-id="<?=$notification->id;?>"> <i class='glyphicon glyphicon-remove' style='color:#1e282c;padding-right:2px;'></i></a></li>
                        </td>


                        <?php };?>

                </table>
            </div>
        <?php endif;?>
    </div>
    <?php if(true==true) :?>
        <div id="notificationFooter">
            <a href="#" class="see_all_notifications_messages" data-id="<?=$this->profile_id;?>">See All</a>
        </div>
    <?php else:?>
    <?php endif;?>
</div>