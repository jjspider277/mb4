<div class="table">
  <!-- Default panel contents -->
  <div class="panel-notification panel-heading" style="background-color:rgb(66, 62, 62);color:white;font-weight:700;">My Notifications</div>
  <!--show success message -->
        <div  class="alert seen-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4> Success! Notification Arcived! </h4>
        </div>

<?php if ($count_notifications > 0) : ?>

<?php foreach ($notifications as $notification) { ?>

  <!-- Table -->
<div class="col-md-12" style="background-color:white;padding:15px;">
 

   <div class="col-md-2 column_border">

      <div class="full_name">         
          <?= $notification->created_date;?>
      </div>
     
    </div>

    <div class="col-md-2 column_border">

      <div class="full_name">         
          <a href="<?= base_url('sell/seller').'/'.$profile->id;?>"><?php echo ucfirst($profile->fname).'FromMBU4U';?></a>
      </div>
     
    </div>

    <div class="col-md-2 column_border">

      
        <div class="full_name">
       
          <?= isset($notification->product_name)? 'Has purhcased product'.' '.$notification->product_name:$notification->notification_text;?>
        
        </div>
     
    </div>

  <div class="col-md-2">

   <div class="message_content column_border" >
       
        <p class='messages'>
             <?= isset($notification->product_name)?'Qnty.'.' '. $notification->qty.'@'.$notification->unit_price:'-';?>
        </p>
        
        </div>
 </div>
  <div class="col-md-3">
   <div class="" >
        <span class=""> <?php isset($notifications->total_price)?'Total Price ='.'$'.$notification->total_price:'-'; ?></span>
        <p class='messages'><?php  echo isset($notification->fee) ? 'Processing Fee '.' <b>minus</b> '.$notification->fee:'-'; ?></p>
         
        </div>
 </div>
 <div class="col-md-1" >
   <div class="message_content" >
      <a href="" class="remove-notification"  data-id="<?=$notification->id ;?>"> <i class='glyphicon glyphicon-remove' style='color:red;padding-right:2px;'></i></a>
       </a> </div>
     
 </div>
 
 </div>
 <?php } ?>

<?php else:?>
   <!-- Table -->
<div class="col-md-12" style="background-color:white;padding:15px;">
 

   <div class="col-md-2 column_border">

      <div class="full_name">         
          <?= $notifications->created_date;?>
      </div>
     
    </div>

    <div class="col-md-2 column_border">

      <div class="full_name">         
          <a href="<?= base_url('sell/seller').'/'.$profile->id;?>"><?php echo ucfirst($profile->fname).'FromMBU4U';?></a>
      </div>
     
    </div>

    <div class="col-md-2 column_border">

      
        <div class="full_name">
         
         <?= isset($notifications->product_name)? 'Has purhcased product'.' '.$notification->product_name:$notification->notification_text;?>
        
        
        </div>
     
    </div>

  <div class="col-md-2">

   <div class="message_content column_border" >
       
        <p class='messages'>

           <?= isset($notifications->qty)?'Qnty.'.' '. $notifications->qty.'@'.$notifications->unit_price:'-';?>

        </p>
        
        </div>
 </div>
  <div class="col-md-3">
   <div class="" >
        <span class=""> <?php echo $notifications->total_price!=0?'Total Price ='.'$'.$notifications->total_price:'-'; ?></span>
        <p class='messages'><?php  echo isset($notifications->fee) ? 'Processing Fee '.' <b>minus</b> '.$notifications->fee:'-'; ?></p>
         
        </div>
 </div>
 <div class="col-md-1" >
   <div class="message_content" >
      <a href="" class="remove-notification"  data-id="<?=$notifications->id;?>"> <i class='glyphicon glyphicon-remove' style='color:red;padding-right:2px;'></i></a>
        </div>
     
 </div>
 
 </div>
<?php endif;?>

<div class="container col-md-12" style="padding:5px;border-top:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center;border-bottom:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center">

<strong>View more</strong>


</div>

</div>


