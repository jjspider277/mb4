
<style>
    div.dialog {
        display: none;
        position: absolute;
        width: 350px;
        background-color: #ffffff;
        border: 9px solid #ffffff;
        border-radius: 5px;
        color: #3c3c3c;
        padding: 10px 0;
    }


    div.dialog#chat-request-dialog .dialog-content > p {
        background-color: #FFFFFF;
    }
    div.dialog#chat-request-dialog .dialog-content > div#buttons {
        text-align: center;
    }
    div.dialog#chat-request-dialog .dialog-content > div#buttons a {
        border-radius: 3px;
        display: inline-block;
        text-decoration: none;
        cursor: pointer;
        padding: 3px 8px;
    }
    div.dialog#chat-request-dialog .dialog-content > div#buttons a#decline-chat {
        background-color: #9d2424;
        border: 1px solid #9d2424;
        color: #0C58A4;
        margin-right: 5px;
    }
    div.dialog#chat-request-dialog .dialog-content > div#buttons a#accept-chat {
        background-color: #419736;
        border: 1px solid #419736;
        border-radius: 3px;
        color: #FFFFFF;
    }
    div.dialog div.dialog-header h2 {

        margin: -5px 0 0 0;
        font-size: 22px;

    }
    div.dialog div.dialog-content {
        padding: 5px 5px;
    }
    div.dialog i.close-dialog {
        display: block;
        position: absolute;
        top: 10px;
        right: 10px;
        color: #9c9c9c;
        font-size: 18px;
        cursor: pointer;
    }
    div.dialog i.close-dialog:hover {
        color: #000000;
    }
    div.overlay {
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        min-width: 1160px;
    }
    .dialog.confirmation-dialog {
        border: 0 none;
        border-radius: 0;
        padding: 0;
    }
    .dialog.confirmation-dialog .dialog-header {
        background-color:  #1e5b86;
        margin: 0;
        padding: 10px 10px 2px;
    }

    div.dialog.confirmation-dialog .dialog-header > h2 {
        border-bottom: 0 none;
        color: white;
        float: right;
        font-size: 20px;
        font-weight: 100;
        width: 330px;
    }

    div.dialog.confirmation-dialog .dialog-content {
        padding: 5px;
    }
    div.dialog.confirmation-dialog .dialog-content #profile_picture {
        margin-left: 50px;
    }
    div.dialog.confirmation-dialog .dialog-content div.right-content {
        float: left;
        width: 300px;
        height: auto;
        margin-left: 0px;
        margin-bottom: 10px;
        padding: 7px;
    }
    div.dialog.confirmation-dialog p {
        clear: both;
        height: 20px;
        font-size: 16px;
        margin-bottom: 10px;
        padding: 5px;
    }
    div.dialog.confirmation-dialog p.username {
        display: inline-block;
        float: left;
        font-size: 14px;
        height: 30px;
        margin-bottom: 0px;
        margin-left: 0px;
        margin-top: 0;
        width: 370px;
    }
    div.dialog.confirmation-dialog p.username > span {
        color: #ff0000;
        font-style: bold;
    }
    div.dialog.confirmation-dialog a.button {
        background-color: #409535;
        border: 1px solid #ff0000;
        color: #fff;
        display: inline-block;
        font-size: 18px;
        font-weight: 100;
        padding: 3px 5px;
        text-align: center;
        text-decoration: none;
        width: 100px;
        height:;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.message {
        background-color: #34cf95;
        border: 1px solid #34cf95;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.hello {
        background-color: #c30d64;
        border: 1px solid #c30d64;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.favorite {
        background-color: #ea9a3b;
        border: 1px solid #ea9a3b;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.chat {
        background-color: #ff0000;
        border: 1px solid #ff0000;
        margin-left: 50px;
        border-radius: 0;
    }

    #chat-request-sent-dialog .no-button,
    #chat-request-response-dialog .no-button {
        margin-top: 7px;
        margin-left: 100px;
        background-color: #409435;
        border: 1px solid #409435;
    }


    div#chat-request-dialog{
        top:300px;
        left:420px;

    }
    div#chat-request-response-dialog{
        top:300px;
        left:420px;

    }
    div#chat-confirmation-dialog{
        top:300px;
        left:420px;

    }

    div#chat-base-url{
        display:none;
    }
    .dialog.confirmation-dialog {
        border: 0 none;
        border-radius: 0;
        padding: 0;
    }
    .dialog.confirmation-dialog .dialog-header {
        background-color:  #1e5b86;
        height: 30px;
        margin: 0;
        padding: 10px 10px 2px;
        position:relative;
    }

    div.dialog.confirmation-dialog .dialog-header > h3 {
        border-bottom: 0 none;
        color: white;
        float: left;
        font-size: 18px;
        font-weight: 100;
        width: 330px;

    }

    div.dialog.confirmation-dialog a.button {
        background-color: #a32e2e;
        border: 1px solid #a32e2e;
        color: #fff;
        display: inline-block;
        font-size: 18px;
        font-weight: 100;
        padding: 3px 5px;
        text-align: center;
        text-decoration: none;
        width: 100px;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.message {
        background-color: #34cf95;
        border: 1px solid #34cf95;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.hello {
        background-color: #c30d64;
        border: 1px solid #c30d64;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.favorite {
        background-color: #ea9a3b;
        border: 1px solid #ea9a3b;
        margin: 0;
        border-radius: 0;
    }
    div.dialog.confirmation-dialog a.button.chat {
        background-color: #419736;
        border: 1px solid #419736;
        width: 100px;
        margin-left: 50px;
        border-radius: 0;
    }
    div#lefties{
        float: left;
        color: white;
        height: 20px;
        padding-top: 0px;
    }
    .chatbox {
        position: fixed;
        position: expression("absolute");
        width: 420px;
        display: none;
        z-index: 5;
    }
    .chatbox div.chatboxhead {
        background-color: #1e5b86;
        border-left: 1px solid #1e5b86;
        border-right: 1px solid #1e5b86;
        color: #FFFFFF;
        padding: 7px;
    }
    .chatbox div.chatboxhead div.chatboxtitle {
        float: left;
        font-size: 15px;
        font-weight: bold;

    }
    .chatbox div.chatboxhead div.chatboxoptions {
        float: right;
    }
    .chatbox div.chatboxhead div.chatboxoptions a {
        color: #FFFFFF;
        display: inline-block;
        font-family: pt sans;
        font-weight: bold;
        margin-left: 5px;
        text-decoration: none;
    }
    .chatbox div.chatboxcontent {
        background-color: #FFFFFF;
        border-bottom: 1px solid #EEEEEE;
        border-left: 1px solid #CCCCCC;
        border-right: 1px solid #CCCCCC;
        color: #333333;
        font-family: arial, sans-serif;
        font-size: 13px;
        line-height: 1.3em;
        overflow: auto;
        width: 418px;
    }
    .chatbox div.chatboxcontent div.chat-container {
        border-right: 1px solid #CCCCCC;
        float: left;
        width: 230px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat {
        border-bottom: 1px solid #EEEEEE;
        height: 200px;
        overflow: auto;
        padding: 8px 8px 15px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat p.chat-msg {
        margin-bottom: 5px;
        margin-top: 5px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat p.chat-msg span.user {
        color: #262626;
        font-size: 12px;
        font-weight: bold;
        margin-right: 5px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat p.chat-msg span.blue-text {
        color: #1e5b86;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat p.chat-msg span.message {
        color: #949494;
    }
    .chatbox div.chatboxcontent div.chat-container div.chat p.chat-notification {
        color: #9C9C9C;
        font-size: 11px;
        margin-bottom: 5px;
        margin-top: 5px;
        padding-left: 10px;
        padding-right: 5px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chatboxinput {
        padding: 5px;
    }
    .chatbox div.chatboxcontent div.chat-container div.chatboxinput .chatboxtextarea {
        width: 213px;
        height: 44px;
        padding: 3px 0pt 3px 3px;
        border: 1px solid #eeeeee;
        margin: 1px;
        overflow: hidden;
    }
    .chatbox div.chatboxcontent div.chat-container div.chatboxinput .chatboxtextareaselected {
        border: 2px solid #1e5b86;
        margin: 0;
    }
    .chatbox div.chatboxcontent div.users-container {
        float: left;
        width: 184px;
    }
    .chatbox div.chatboxcontent div.users-container div.add-user {
        border-bottom: 1px solid #EEEEEE;
        padding: 3px;
    }
    .chatbox div.chatboxcontent div.users-container div.add-user select {
        border: 1px solid #D9D9D9;
        padding: 3px;
        width: 179px;
    }
    .chatbox div.chatboxcontent div.users-container div.add-user input {
        border: 1px solid #D9D9D9;
        padding: 3px;
        width: 173px;
    }
    .chatbox div.chatboxcontent div.users-container div.add-user a {
        background-color: #1e5b86;
        border-radius: 3px;
        color: #FFFFFF;
        display: block;
        font-size: 14px;
        font-weight: bold;
        margin: 5px 1px;
        padding: 5px 0;
        text-align: center;
        text-decoration: none;
    }
    .chatbox div.chatboxcontent div.users-container div.users {
        color: #000000;
        font-size: 12px;
        font-weight: bold;
        padding: 0px;
    }
</style>

<?php  $uname = $this->session->userdata('username');  ?>

<div id="profile-data">
    <div id="logged-in-user-notification"><?php echo $uname; ?></div>
    <div id='profile-img-add'><?php echo base_url($profile_image);?></div>
    <div id='sender-img-add'><?php echo base_url($the_sender_image);?></div>
    <i class="close-dialog fa fa-times-circle-o"></i>
</div>


<div id="chat-request-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
        <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?> </div>
        <div id="lefties"> <h2>New Chat Request</h2></div>
    </div>
    <div class="dialog-content">
        <div id="profile_picture">
            <img src="" height ="150" class="dialog-logo"/>
        </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username"><span class="sender"></span> has sent you a Chat Request.<span class="subject"></span></p>
            <p>Do you accept this invite?</p>
            <a class="button chat confirm-receive-chat" id="accept-chat" data-status="accept" href="#">Accept</a>
            <a class="button" id="decline-chat" data-status="decline" href="#">Decline</a>
        </div>
    </div>
</div>


<div id="chat-request-sent-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
        <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
        <div id="lefties"><h2>Chat Request Sent</h2></div>
    </div>
    <div class="dialog-content">
        <div id="profile_picture">
            <img src="" height ="150" class="dialog-logo"/>
        </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username">You have sent <span></span> a chat request.</p>
            <a class="button no-button" href="#">OK</a>
        </div>
    </div>
</div>


<div id="chat-request-response-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
        <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
        <div id="lefties"> <h2>Chat Request Rejected</h2></div>
    </div>
    <div class="dialog-content">
        <div id="profile_picture">
            <img src="" height ="150" class="dialog-logo"/>
        </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username"><span></span> has declined your chat request.</p>
            <a class="button no-button" href="#">OK</a>
        </div>
    </div>
</div>


<div id="chat-confirmation-dialog" class="dialog confirmation-dialog">

    <div class="dialog-header">
        <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
        <div id="lefties"><h2>New Chat Request</h2></div>
    </div>
    <div class="dialog-content">

        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username">You are sending a Chat Request to <span></span>!</p>
            <p>Do you want to send now?</p>
            <a class="button chat yes-button confirm-send-chat" href="#" data-dialog = "chat-request-sent-dialog" data-username="">Yes</a>
            <a class="button no-button" href="#">No</a>
        </div>
    </div>
</div>
