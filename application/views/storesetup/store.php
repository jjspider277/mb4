<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to MadebyUs4u.com | Seller Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Le styles -->
        <link href=<?php echo base_url() . "assets/plugins/bootstrap/css/bootstrap.min.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/common.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/collective_common.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/store_tabs.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/plugins/picedit/css/font-css.css";?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/plugins/picedit/css/picedit.min.css";?> rel="stylesheet">
   


         <!--start of row-->

        <style type="text/css">
                                .tab_lables_sup {
                                font-weight:400;
                                top:-3.79px;
                                font-size:13px;'
                                }
        </style>

    </head>
                    <body>
                        
                        <?php $this->load->view($notification_bar); ?>

                        <header> 
                            <?php $this->load->view($header_black_menu); ?>
                            <?php $this->load->view($header_logo_white); ?>
                        </header>

                        <!-- Responsive design
                        ================================================== -->
                        <section id="responsive" style="background-color:#f5f5f5;">
                            <!--load menu here -->
                            <?php $this->load->view($main_menu); ?>
                            <div class="container container-listing">
                                <!--store setup-->
                                <div role="tabpanel" class="tab-pane active" id="store">
                                   
                                    <div class="row-storesetup row">
                                        <div id="divsetup-content" name="divsetup-content" class="divsetup-content col-md-12" style="display:block;">
                                            <h2 style="font-size: 26px;font-weight: 600;">Store <span style='font-weight: 400;'>Setup</span>
                                            <sup id='tab_lables_sup'class='tab_lables_sup' id='verify-text'><?php echo $tab_status==true ? "" :"- Step 1. Validate Identity"  ;?></sup>
                                            <small class='pull-right' style='color:rgb(216, 62, 62);margin-top: 8px;font-size: small;'>* ( Mandatory filed )</small>  </h3>
                                            <hr>
                                            <div role="tabpanel" class="tabpane-storesetup">
                                                <!-- Nav tabs -->


                                                <ul class="nav nav-pills" role="tablist" id='createNotTab'>
                                                   
                                                    <li role="presentation"><a href="#verify-tab" aria-controls="verify-tab" role="tab" data-toggle="tab" >
                                                    <!--validate page-->
                                                    <?php echo $tab_status==true ? "Verfication complete!" :"1. Validate Identity"  ;?>

                                                    <i class="fa"></i></a>

                                                    </li>
                                                    
                                                    <!--this is a form for all tab submission after verfiication-->
                                                    <li role="presentation" ><a href="#store-tab" aria-controls="store-tab" role="tab" data-toggle="tab" ><!--store page--> 2. Store Name<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#product-tab" aria-controls="product-tab" role="tab" data-toggle="tab">3. Add Listing<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#getpaid-tab" aria-controls="getpaid-tab" role="tab" data-toggle="tab" >4. Get Paid<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#openstore-tab" aria-controls="openstore-tab" role="tab" data-toggle="tab" >5. Preview Store<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#launchstore-tab" aria-controls="launchstore-tab" role="tab" data-toggle="tab" >6. Launch Store<i class="fa"></i></a></li>

                                                </ul>
                <?php if ($tab_status): ?>
                                                            
                    <?php 
                    
                    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'myForm', 'name' => 'myForm',);
                    $form_post_id= isset($profile_id) ? $profile_id : $profile->id ; 
                       echo form_open_multipart('store/save_store/'.$form_post_id, $attributes);
                    ?>

                <?php endif; ?>

                                                <!-- Tab panes -->
                                                <div class="tab-content">

                                                    <div role="tabpanel" class="tab-pane fade in tab-pane active " id="verify-tab"><?php $this->load->view($identity_validation_page); ?></div>

                                                    <!--DISABLE TAB -->
                                                    <?php   if ($tab_status): ?>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane  " id="store-tab"><?php $this->load->view($store_page); ?></div>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane" id="product-tab"><?php $this->load->view($addproduct_page); ?></div>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane" id="getpaid-tab"><?php $this->load->view($getpaid_page); ?></div>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane" id="openstore-tab"><?php $this->load->view($previewstore_page); ?></div>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane" id="launchstore-tab"><?php $this->load->view($launchstore_page); ?></div>
                                                        <div role="tabpanel" class="tab-pane fade tab-pane" id="message-tab">Message MEssage Messae</div>

                                                        <?php  endif; ?>
                                                    </div>
                                                <?php if ($tab_status): ?>    </form>    
                                                <?php endif; ?>

                                        </div>

                                    </div>
                                </div>
                                </div><!--end of row-->
                                </div><!--end of store setup-->
                            </div>
                        </section>

                        <footer class="footer">
                            <?php
$this->load->view($footer_subscribe);
$this->load->view($footer_privacy);
?>
                        </footer>
                        <!-- Bootstrap core JavaScript
                        ================================================== -->
                        <!-- Placed at the end of the document so the pages load faster -->
                        <script src="<?php echo base_url() . "assets/plugins/jquery/jquery.min.js"; ?>"></script>
                        <!-- Latest compiled and minified JavaScript -->
                        <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/tooltip.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/bootstrap.min.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/subscribe_ajax.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/multiple_image_uploads.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/bootstrapValidator.js"; ?>"></script>
                      <script src=<?php echo base_url()."assets/plugins/picedit/picedit.js";?>> </script>

                <script type="text/javascript">                                                                  
                                              

                        function disable_tabs_when_completed() {
                        var store_setup_completed = '<?php  echo $store_setup_completed; ?>';

                        if (store_setup_completed) {
                        $('.nav-pills a[href="#' + 'launchstore-tab' + '"]').tab('show');
                        $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
                        } else {
                        $('.nav-pills a[href="#' + 'verify-tab' + '"]').tab('show');
                        $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
                        }
                        }
                        function misellenous_help_preview_store_and_image() {
                        //change the produt name div conetnt to same as this one
                        // $('.product_name').text($(e.currentTarget).data('asdasdasd');
                        $("#product_name").change(function(e) {
                        $('#product_name_h4').text($(this).val());
                        });
                        $("#product_descritpion").change(function(e) {
                        $('#descritpion_paragraph').text($(this).val());
                        });
                        $("#price").change(function(e) {
                        $('#price_tag_label').text("$" + $(this).val());
                        });
                        $("#category").change(function(e) {
                        $('#category_label').text($(this).val());
                        });
                        //display the image on previw store page as
                        $(document).on('change', '#product_image', function(event) {
                        var output = document.getElementById("product_preview_image");
                        output.src = URL.createObjectURL(event.target.files[0]);
                        });
                        }
                        function change_labels_when_tabs_activated() {
                         $('a[href="#verify-tab"]').click(function() {
                        var  verified_identiy = '<?php  echo $tab_status; ?>';
                            if(verified_identiy){
                                $('#tab_lables_sup').empty();
                                $('#tab_lables_sup').append(document.createTextNode(""));
                            } else {

                                $('#tab_lables_sup').empty();
                                $('#tab_lables_sup').append(document.createTextNode("- Step 1. Validate Identity"));
                            }
                        });
                            
                        $('a[href="#store-tab"]').click(function() {
                        $('#tab_lables_sup').empty();
                        $('#tab_lables_sup').append(document.createTextNode("- Step 2. Create Store"));
                        });

                        $('a[href="#product-tab"]').on('shown.bs.tab', function (e) {
                        $('#tab_lables_sup').empty();
                        $('#tab_lables_sup').append(document.createTextNode("- Step 3. Add Product Lisiting"));
                        });

                        $('a[href="#getpaid-tab"]').on('shown.bs.tab', function (e) {
                        $('#tab_lables_sup').empty();
                        $('#tab_lables_sup').append(document.createTextNode("- Step 4. Get Paid Wire Your Account"));
                        });

                        $('a[href="#openstore-tab"]').on('shown.bs.tab', function (e) {
                        $('#tab_lables_sup').empty();
                        $('#tab_lables_sup').append(document.createTextNode("- Step 5. Preview Your Store"));

                        });

                        $('a[href="#launchstore-tab"]').on('shown.bs.tab', function (e) {
                        $('#tab_lables_sup').empty();
                        $('#tab_lables_sup').append(document.createTextNode("- Step 6. Launch Your Store"));
                        });
                        }
                        $( document ).ready(function() {
                       
                        disable_tabs_when_completed();
                        misellenous_help_preview_store_and_image();
                        change_labels_when_tabs_activated();




                        //disable tabs not active so that enable them when validated

                        /**
                        this a continue button after successfull identity verfication
                        **/
                        $('#btn_next_page').click( function() {

                        $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');

                        });
                        //next page for store
                        $('#btn_store_next_page').click( function() {
                        var self = this;


                        //TODOD: check store image file attached or not
                        $store_image_status = $("#store_img").attr('src');

                        if( ($store_image_status!='http://localhost/madebyus4u/uploads/no-photo.jpg') & ($store_image_status!='') )  {

                        self.store_image = true;
                        $('#imgfile_store').closest('.form-group').removeClass('has-error').addClass('has-success');
                        $('#store_validation_message').css('color','#3c763d');

                        } else if($store_image_status==undefined || $store_image_status=='') {
                        self.store_image = false;
                        $('#imgfile_store').closest('.form-group').removeClass('has-success').addClass('has-error');
                        $('#store_validation_message').css('color','#a94442');
                        }


                        if(self.store_image==true) {
                        $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
                        }


                        });
                        //next page for product
                        $('#btn_product_next_page').click( function() {

                        var self = this;

                        //TODOD: check store image file attached or not
                        $product_image_status = $("#img1").attr('src');

                        if( ($product_image_status != 'http://localhost/madebyus4u/uploads/no-photo.jpg') & ($product_image_status != '') )  {

                        self.product_image=true;
                        $('#preview_product_image').css('border','1px solid #3c763d');
                        $(".product_validation_message").css('color','#3c763d');

                        } else if( $product_image_status == undefined || $product_image_status == '' ) {

                        // self.product_image=false;
                        $('#preview_product_image').css('border','1px solid #a94442');
                        $(".product_validation_message").css('color','#a94442');
                        }


                        if(self.product_image == true ) {

                        $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
                        }

                        });
                        //next page for store
                        $('#btn_next_page_getpaid').click( function() {

                        var self = this;
                        self.isValid=true;


                        if(self.isValid==true) {
                        $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
                        }

                        });

                        //call function for image upload
                        // multiple_image_upload();

                        });


                        /***
                        * Created by Daniel Adenew , 2014
                        * Submit email subscription using ajax
                        * Send email address
                        * Send controller
                        * Receive response
                        */
                        var url =  "<?php echo site_url('welcome/subscribe'); ?>";
                        subscribe_using_ajax(url);

                        $('#btnstoresetup').click( function() {
                        $('#divstoresetup').hide("400",function(){
                        // alert( "Animation complete." );
                        // $('#divsetup-content').css('display','block');
                        window.location="<?php echo base_url('store'); ?>";
                        });
                        }) ;
                        /*care for cascading drop down boxes **/
                        $('#category').change(function(){
                        var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

                        var categorey_selected = $("#category option:selected").text();

                        $("#variation > option").remove(); //first of all clear select items
                        // alert(categorey_selected);
                        $.ajax({
                        url: "<?php echo base_url('store/get_categories_variation_ajax'); ?>",
                        async: false,
                        cache: false,
                        data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected},
                        type: 'post',
                        datatype:'json',
                        success: function(variation_array_json){
                        // alert(variation_array_json);
                        $.each(variation_array_json,function(id,value) //here we're doing a foeach loop round each city with id as the key and city as the value
                        {

                        var opt = $('<option />'); // here we're creating a new select option with for each city
                            opt.val(value);
                            opt.text(value);
                            $('#variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                            });
                            $('#variation').val('#');
                            }
                            });
                            //sub variation
                            $('#variation').change(function(){
                            var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

                            var variation_selected = $("#variation option:selected").text();
                            var categorey_selected = $("#category option:selected").text();

                            $("#sub_variation > option").remove(); //first of all clear select items
                            // alert(categorey_selected);
                            $.ajax({
                            url: "<?php echo base_url('store/get_sub_variation_ajax'); ?>",
                            async: false,
                            cache: false,
                            data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected,variation:variation_selected},
                            type: 'post',
                            datatype:'json',
                            success: function(variation_array_json){
                            //alert(variation_array_json);
                            $.each(variation_array_json,function(id,value) //here we're doing a foreach loop round each category with id as the key and city as the value
                            {

                            var opt = $('<option />'); // here we're creating a new select option with for each city
                                opt.val(value);
                                opt.text(value);
                                $('#sub_variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                                });
                                $('#sub_variation').val('#');
                                }          });
                                });
                                });

                                </script>


                                <script>

$(document).ready(function(){

$( "#preview_store_image" ).change(function() {
 alert( "Handler for .change() called." );


 var fileInput = $(this).find("input[type=file]")[0],
 file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            //if( width == 400 && height == 300 ) {
                alert("ABC");
            //}
            //else {
                //fail
           // }
        };
    }

     alert( "Handler for .change() called." );
});
   
});
 
                                </script>


<!-- Modal -->
<div class="modal fade" id="imageUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog" role="document">
  
    <div class="modal-content">
         
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Image Upload</h4>
          </div>

      <div>
     
      <form></form>
     
      <div contenteditable="true" style="position: absolute; left: -999px; width: 0px; height: 0px; overflow: hidden; outline: 0px; opacity: 0;"></div>
     <?php 
                    
    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'myForms', 'name' => 'myForms',);
           
     echo form_open_multipart('store/post_image/', $attributes);
    ?>

<input type="hidden" name="img_src_id" id="img_src_id">

<div style="margin:10% auto 0 auto; display: table;">

 <!-- begin_picedit_box -->
 <div class="picedit_box">
    <!-- Placeholder for messaging -->
    <div class="picedit_message">
         <span class="picedit_control ico-picedit-close" data-action="hide_messagebox"></span>
        <div></div>
    </div>
    <!-- Picedit navigation -->
    <div class="picedit_nav_box picedit_gray_gradient">
        <div class="picedit_pos_elements"></div>
       <div class="picedit_nav_elements">
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-pencil" title="Pen Tool"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_3">
                    <label class="picedit_colors">
                      <span title="Black" class="picedit_control picedit_action picedit_black active" data-action="toggle_button" data-variable="pen_color" data-value="black"></span>
                      <span title="Red" class="picedit_control picedit_action picedit_red" data-action="toggle_button" data-variable="pen_color" data-value="red"></span>
                      <span title="Green" class="picedit_control picedit_action picedit_green" data-action="toggle_button" data-variable="pen_color" data-value="green"></span>
                    </label>
                    <label>
                        <span class="picedit_separator"></span>
                    </label>
                    <label class="picedit_sizes">
                      <span title="Large" class="picedit_control picedit_action picedit_large" data-action="toggle_button" data-variable="pen_size" data-value="16"></span>
                      <span title="Medium" class="picedit_control picedit_action picedit_medium" data-action="toggle_button" data-variable="pen_size" data-value="8"></span>
                      <span title="Small" class="picedit_control picedit_action picedit_small" data-action="toggle_button" data-variable="pen_size" data-value="3"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
                <span class="picedit_control picedit_action ico-picedit-insertpicture" title="Crop" data-action="crop_open"></span>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-redo" title="Rotate"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_1">
                    <label>
                      <span>90° CW</span>
                      <span class="picedit_control picedit_action ico-picedit-redo" data-action="rotate_cw"></span>
                    </label>
                    <label>
                      <span>90° CCW</span>
                      <span class="picedit_control picedit_action ico-picedit-undo" data-action="rotate_ccw"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
           <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-arrow-maximise" title="Resize"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_2">
                    <label>
                        <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="resize_image"></span>
                        <span class="picedit_control picedit_action ico-picedit-close" data-action=""></span>
                    </label>
                    <label>
                      <span>Width (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_width" value="0">
                    </label>
                    <label class="picedit_nomargin">
                        <span class="picedit_control ico-picedit-link" data-action="toggle_button" data-variable="resize_proportions"></span>
                    </label>
                    <label>
                      <span>Height (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_height" value="0">
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
       </div>
    </div>
    <!-- Picedit canvas element -->
    <div class="picedit_canvas_box">
        <div class="picedit_painter">
            <canvas></canvas>
        </div>
        <div class="picedit_canvas">
            <canvas></canvas>
        </div>
        <div class="picedit_action_btns active">
          <div class="picedit_control ico-picedit-picture" data-action="load_image"></div>
          <div class="picedit_control ico-picedit-camera" data-action="camera_open"></div>
          <div class="center">or copy/paste image here</div>
        </div>
    </div>
    <!-- Picedit Video Box -->
    <div class="picedit_video">
        <video autoplay=""></video>
        <div class="picedit_video_controls">
            <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="take_photo"></span>
            <span class="picedit_control picedit_action ico-picedit-close" data-action="camera_close"></span>
        </div>
    </div>
    <!-- Picedit draggable and resizeable div to outline cropping boundaries -->
    <div class="picedit_drag_resize">
        <div class="picedit_drag_resize_canvas"></div>
        <div class="picedit_drag_resize_box">
            <div class="picedit_drag_resize_box_corner_wrap">
            <div class="picedit_drag_resize_box_corner"></div>
            </div>
            <div class="picedit_drag_resize_box_elements">
                <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="crop_image"></span>
                <span class="picedit_control picedit_action ico-picedit-close" data-action="crop_close"></span>
            </div>
       </div>
    </div>
</div>
<input type="file" style="display:none" accept="image/*">
<!-- end_picedit_box -->
</div>
 <div class="modal-footer" style="margin-top:30px; text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="btnSubmit" class="btn btn-primary">Upload</button>
      </div>
</form>


      </div>
      
    </div>
  </div>
</div>


<script type="text/javascript">
    var img_id; //global
     var img_src; //global

        $( "#img1_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img2_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img3_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $("#img4_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         $( "#img5_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         //store

          $( "#store_img_edit_btn" ).click(function() {
          
          var $this = $(this);
         console.log($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });




      $('#imageUpload').on('show.bs.modal', function (event) {
       // alert('xx');
             var button = $(event.relatedTarget); // Button that triggered the modal
             window.img_id = button.data('imageid') ;// Extract info from data-* attributes
             window.img_src = document.getElementById(window.img_id).getAttribute('src').toString();
             console.log ("image id global set to ="+ window.img_id);
             console.log("hello docrore"+window.img_src);
             document.getElementById("img_src_id").value = window.img_id;        
             
     });

       $('#imageUpload').on('shown.bs.modal', function (event) {;
               
            // $('.picedit_box').picEdit("set_default_image",window.img_src);
       });

    
    $(function() {
        document.getElementById('myForms').reset();
        console.log("DOc"+'http://localhost/madebyus4u/uploads/no-photo.jpg');

       

        $('.picedit_box').picEdit({
            imageUpdated: function(img){
                //console.log('when image changed ');
              //  console.log (img);
            },
            formSubmitted: function(){
                //console.log('when image submitted ');
               // document.getElementById('img1').setAttribute( 'src', '' );
            },
            imageUpdated: function(img){
            //console.log('Image updated!'+img);
            },
            formSubmitted: function(response){
            document.getElementById("myForms").reset();
            console.log('Form submitted! with response'+response.responseText);
            document.getElementById(window.img_id).setAttribute('src',response.responseText );
            },
            maxWidth: 600, 
            maxHeight: 'auto',
            aspectRatio: true, 
            redirectUrl: false,
            defaultImage: false,
           
        });
                                
    

    });

    
</script>
<script type="text/javascript">
 $("#btn_save").click(function(){
console.log('saved');

 }) ;  
 

</script>
<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>

</body>
</html>

