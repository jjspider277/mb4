<style type="text/css">
</style>
<div class="col-md-12 col-md-store-content">
   

    <div class='row row-table-p col-md-12 blue-border transparent-bg' style="padding-top: 20px;">
        <div class='col-md-12' style="padding-bottom:10px;">
            <div class="col-md-12 get-paid-container">
             
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Account Type</label>
                          <?= form_dropdown('account_type', array_merge(array('#'=>'Please select account type'), $account_types) ,'#','class="form-control select-drop-down-arrow"  tabindex="7"' ) ;?>
                    </div>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Bank Branch</label>
                        <input value="<?php echo set_value('bankbranch');?>" type="text" id="bankbranch" name='bankbranch' class="form-control" required>
                    </div>

                    <div class="col-md-12 no-left-right-padding">
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="storename">Account Owners Name</label>
                        <input value="<?php echo set_value('account_owner');?>" type="text" id="account_owner" name='account_owner' class="form-control" required>
                        <p class="help-block">same as your bank book account .</p>
                    </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <h3>Pic of Check</h3>
                        <img src="<?php echo base_url('assets/images/storesetup/check.jpg');?>">
                    </div>


                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Rounting Number </label>
                        <input value="<?php echo set_value('routenumber');?>" type="text" id="routenumber" name='routenumber' class="form-control col-md-6" required>
                    </div>
                    

                    <div class='col-md-12 no-left-right-padding'>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Account Number</label>
                        <input value="<?php echo set_value('accountnumber');?>" type="text" id="accountnumber" name='accountnumber' class="form-control" required>
                    </div>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="storename">Re-enter Account Number</label>
                        <input value="<?php echo set_value('reaccountnumber');?>" type="text" id="reaccountnumber" name='reaccountnumber' class="form-control" required>

                    </div>  
                    </div>   

                </div>

    
        </div>
         <hr  class='hr_store_form'>
            
         
    </div>
    <div class="form-group col-md-2 col-md-offset-5 top-padding">
              <input type='button' id='btn_next_page_getpaid' name='btn_next_page_getpaid' class="btn btn-primary btn-lg" value='Continue To Step 6'>
            </div>

</div>