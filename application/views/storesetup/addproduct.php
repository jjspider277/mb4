<style type="text/css">
    .ico-picedit-checkmark {
      font-size: xx-large;
      color:blue;
    }
    .ico-picedit-close {
        font-size: xx-large;
        color:blue;
    }
    .picedit_drag_resize_box_corner {
        font-size: xx-large;
        color:blue;
    }
</style>
<div class="col-md-12 col-md-store-content">
    <div class="col-md-12 pull-left">
        <p>
        <span class='tab_content_title'>Add a Product</span>

        </p>
    </div>
    <div class='row row-table-p col-md-12'>
        
        <div class="col-md-10 col-md-offset-1" style="margin-top:2%;">
           

             <div class="form-group row col-md-4">
                    <span class='required_star'>*</span>
                    <label class='control-lable' for="product_name">Product Name</label>
                    <input value="<?php echo set_value('product_name');?>" type="text" id='product_name' name="product_name" class='form-control' placeholder="Product Name"
                    length="20"  style="margin-top: 13px;" required>
                      
                </div>

                <div class="form-group row col-md-12 ">
                    <span class='required_star'>*</span>
                    <label class='control-lable' for="product_description">Add product description</label>
                    <textarea class="form-control" rows="4" id='product_description' name="product_description" required><?php echo set_value('product_descritpion');?></textarea>
                </div>

                <div class="form-group row col-md-12 ">
                    <span class='required_star'>*</span>
                    <label class='control-lable' for="product_details">Add product detail information (used for Social Networking aspect)</label>
                    <textarea class="form-control" rows="5" id='product_details' name="product_details" required><?php echo set_value('product_details');?></textarea>
                </div>

              <div class="form-group row col-md-5">
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="color_variation">Color</label>
                       

                      <?php  
                         //var_dump($catagories);
                       -
                          $colors['#'] = 'Please Select color';
                         ?>

                          <select class="form-control" id="colors" name='colors' style="background-color:rgba(51, 48, 48, 0.51);font-weight:bold;">
                                <?php foreach ($colors as $key => $value) { ?>
                                   <option value="<?=$value?>" style='font-weight:bold;border 1px solid;color:<?=$value;?>'><?=$value;?></option>
                                <?php }?>
                        </select>
                    </div>
                </div>

                <div class="form-group row col-md-6 col-md-offset-1">
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="color_variation">Size</label>
                       

                      <?php  
                         //var_dump($catagories);
                       
                         $sizes['#'] = 'Please Select size';
                          //array_push($sizes,array('small'=>'samll','medium'=>'medium','large'=>'large','x-large'=>'x-large','x-small'=>'x-small'));
                          echo form_dropdown('sizes', $sizes,'#','id="sizes"
                          class="form-control"'); ?>
                    </div>
                </div>

            

                <div class="col-md-4 col-md-select" >
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="category">Select Category</label>
                       

                      <?php  
//var_dump($catagories);
                      $catagories['#'] = 'Please Select Category';
                          echo form_dropdown('categories', $catagories,'#','id="category"
                          class="form-control"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-select">
                    <div class="form-group ">
                        <label for="variations">Select Sub Category</label>
                       
                         <?php  $variation['#'] = 'Please Select';
                         echo form_dropdown('variation',$variation,'#',
                         'id="variation" class="form-control"'); ?> 
                    </div>
                </div>

                <!-- <div class="col-md-4 col-md-select">
                    <div class="form-group">
                       
                        <label for="sub_variation">Sub Variation</label>
                       
                          <?php  $sub_variation['#'] = 'Please Select';
                         echo form_dropdown('sub_variation',$sub_variation,'#',
                         'id="sub_variation" class="form-control"'); ?>
                    </div>
                </div> -->

                <div class="form-group row col-md-12 has-feedback">                                


                    <label for="preview_produt_image">Photos</label>
               
                   <style type="text/css">
                    
                    .col-md-3 {
                     width: 20%;
                     padding-right: 2px;
                     padding-left: 2px;
                    }
                    
                   .col-sm-6 .col-md-3 {
                     padding-right: -3px; 
                     padding-left: 10px;
                     width: 20%;
                     padding-right: 4px;
                      padding-left: 4px;
                   }
                
                
                   </style>

                   <div class="row"> 
                   <div class="col-sm-6 col-md-3" > 
                   <div class="thumbnail"> 
                   <img id = 'img1' data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;">
                    <div class="caption"> 
                            <button id="img1_btn" data-imageid="img1" type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload" >
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="img1_btn_edit" type="button" class="btn btn-default btn-lg" data-imageid="img1" >
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                     </div> 
                     </div> 
                     </div> 
                        <div class="col-sm-6 col-md-3"> 
                   <div class="thumbnail"> 
                   <img id = 'img2'  data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;">
                    <div class="caption"> 
                           <button id="img2_btn" data-imageid="img2"type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload">
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="img2_btn_edit" type="button" class="btn btn-default btn-lg" data-imageid="img2" data-toggle="modal" data-target="#imageUploadEdit">
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                     </div> 
                     </div> 
                     </div>
                  <div class="col-sm-6 col-md-3"> 
                   <div class="thumbnail"> 
                   <img id = 'img3'  data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;">
                    <div class="caption"> 
                        <button id="img3_btn" data-imageid="img3" type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img3_btn_edit" type="button" class="btn btn-default btn-lg" data-imageid="img3" data-toggle="modal" data-target="#imageUploadEdit">
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                     </div> 
                     </div> 
                     </div>
                     <div class="col-sm-6 col-md-3"> 
                     <div class="thumbnail"> 
                     <img id = 'img4'  data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;"> 
                     <div class="caption">
                         <button id="img4_btn" data-imageid="img4" type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img4_btn_edit" type="button" class="btn btn-default btn-lg" data-imageid="img4" >
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                     </div>

                      </div> </div> 
                     <div class="col-sm-6 col-md-3">
                      <div class="thumbnail"> 
                      <img id = 'img5' data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;"> 
                       <div class="caption">
                        <button id="img5_btn" data-imageid="img5"type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img5_btn_edit" type="button" class="btn btn-default btn-lg" data-imageid="img4">
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                      </div>


                       </div> </div> </div>

                    <label class='product_validation_message'>Upload clear and good quality pictures</label>
                    <p class='product_validation_message'>
                    <span class='required_star'>*</span>
                    At least one image is manadatory</p>
                </div>

                 <div class="row col-md-4 form-group ">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Quantity</label>
                       <input  value="<?php echo set_value('quantity');?>" type="number" class="form-control" name='quantity' id="quanity" required>
                </div>

                 <div class="col-md-12 col-price">

                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Price Before Tax</label>
                       <input value="<?php echo set_value('price');?>" type="number" class="form-control" name='price' id="price" required>
                   </div>

                <div class="col-md-4 form-group ">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Special Price </label>
                       <input value="<?php echo set_value('sprice');?>" type="text" class="form-control" name='sprice' id="sprice" required>
                       
                </div>
                       
                </div>
             
          
        </div>
         <hr  class='hr_store_form'>
              <div class="form-group col-md-2 col-md-offset-5">
              <input type='button' id='btn_product_next_page' name='btn_product_next_page' class="btn btn-primary btn-lg" value="Continue" />
            
            </div>
    </div>
</div>




