<div class="panel-notification panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-notification panel-heading" style="background-color:rgb(66, 62, 62);color:white;font-weight:700;">Friend Request</div>
  <!--show success message -->
        <div  class="friend_request_message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success </strong>request is ingnored !
        </div>



<?php if ($count_friend_request > 0) { ?>

<?php foreach ($friend_requests as $friend_request) { ?>

<!-- Table -->
<div class="col-md-12" style="background-color:white;padding:15px;">
 
  <div class="col-md-2">
   
   <a href="#">
          <img class="media-object" style="" src="<?=base_url($friend_request->media->file_name) ;?>" data-holder-rendered="true" style="width: 80px; height: 80px;float:left:padding:5;">
        </a>
  </div>

   <div class="col-md-2 column_border">
          
         <p>
         <a href="<?= base_url('sell/seller').'/'.$friend_request->profile_id;?>">
         <?php echo ucfirst($friend_request->full_name).'FromMBU4U';?></a>
         </p>

         <p class='state' style="font-weight:100;"> <?php echo $friend_request->city.', '.$friend_request->state;?></p>


    </div>

    <div class="col-md-3 column_border">
      
      <p>This User has sent you a freind request! </p>
      <p><?php echo $friend_request->date; ?> <span>@<?php echo $friend_request->time ;?></span> </p>
     
    </div>

    <div class="col-md-3 column_border">
          
    <a href=""  id='<?=$friend_request->friend_id ;?>' class="accept_btn col-md-6  btn btn-default btn-success">Accept</a>
    <a href=""  id='<?= $friend_request->friend_id ;?>' class="deny_btn col-md-offset-1 col-md-5 btn btn-default btn-danger">Deny</a>
           
    </div>

    <div class="col-md-1" >
   
    <style type="text/css">
    .message_content_freinds {
    padding-top: 6px;
    float: right;    font-weight: 800;
    }
</style>
        <div class="message_content_freinds">
          <a href="" class="remove-request"  data-id="<?=$friend_request->friend_id;?>"> <i class='glyphicon glyphicon-remove' style='color:red;padding-right:2px;'></i></a>
          </div>

    </div>
 
 </div>
<?php } ?>


<div class="container col-md-12" style="padding:5px;border-top:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center;border-bottom:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center">
<strong>View more</strong>
</div>

<?php } else { ?>

 <div class="col-md-12 col-md-offset-4" ><h4>You have no friend requests yet!</h4></div>

<?php }?>

</div>

