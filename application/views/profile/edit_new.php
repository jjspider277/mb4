<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Edit Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/edit_profile_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">


</head>

<body>
<?php 

$uname = $this->session->userdata('username');
$email = $this->session->userdata('email');

 ?>
<?php $this->load->view($notification_bar); ?>

<div class='header'>

<?php $this->load->view($header_black_menu);?>
<?php $this->load->view($header_logo_white); ?>

</div>
<style type="text/css">
  .blue_border_container {
    border:3px solid #428BCA;

    padding: 11px;
  }
.bee_seen h3 {
  color:#1667A0;
}
.bee_seen h1 {
  color:#1667A0;
  font-weight: 700;
}
.having_photo_text {
}

.circular {
  width: 150px;
  height: 150px;
  border-radius: 150px;
  -webkit-border-radius: 150px;
  -moz-border-radius: 150px;
  background: no-repeat;
  box-shadow: 0 0 8px rgba(0, 0, 0, .8);
  -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
  -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
  }
  .navbar {
      margin-bottom: 0px;
  }
  .row5 {
      padding: 2px;
      border: 2px solid #F3F3F2;
      border-top: 7px solid grey;
      border-radius: 11px;
  }
  .menu-column_submenu  {
      margin: 0;
      padding: 0;
      font-family: 'Oswald',sans-serif;
      font-weight: 300;
      font-size:medium ;

  }

 
  #search{
      background: #d3d3d3;
      cursor: pointer;
      font-size: 24px;
      font-weight: bold;
      text-transform: lowercase;
      padding: 20px 2%;
      width: 96%;
  }
  #search-overlay{
      background: black;
      background: rgba(255, 255, 255, 255);
      color: black;
      display: none;
      font-size: 18px;
      height: 200px;
      padding: 0px;
      margin-top:28px;
      position: absolute;
      width: 436px;
      z-index: 100;
      opacity: 0.95;
      border-radius: 4%;
      border: 2px solid #efefef;
      overflow: auto;
  }
  #display-search{
      border: none;
      color: black;
      font-size: 14px;
      margin: 5px 0 0 0;
      width: 400px;
      height: 20px;
      padding: 0 0 0 10px;
      display: none;
  }

  #hidden-search{
      left: -10000px;
      position: absolute;

  }

  #results{
      display: none;
      width: 300px;
      list-style: none;
  }
  #results ul {
      list-style:none;
      padding-left:0;
  }​
   #results ul li{
       list-style: none;
       padding-left:0;
   }

  #results ul li a{
      color:#2676af;
      font-size: 12px;
      font-weight: bold;
  }
  }
  #search-data{
      font-size: 14px;
      line-height: 20%;
      padding: 0 0 0 20px;

  }

  h2.search-data{
      margin: 10px 0 30px 0;

  }
</style>
<section >

    <div class=" row white-bg middle_naviagtion">
        <hr class="" style="margin: 0px;">
        <div class="container white-bg " style="">

            <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">

                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <style type="text/css">
                        .main-navigation {
                            background-color: #FAFAFA;
                        }
                        .navbar-default {
                            background-color: inherit;
                            border: none;
                        }
                        .blue-font {
                            color: #2f97cc;
                        }
                        .grey-background{
                            background: #ebebeb;
                        }
                        .bottom-blue-border{
                            border-bottom: 4px solid #216da1;
                        }
                        .wrapper {
                            text-align: center;
                        }

                        .start-shopping-btn {
                            position: absolute;
                            top: 83%;
                            left: 42%;
                            font-size: 27px;
                            background: #0b69a0;
                        }

                        .black-btn{
                            background:#303030;
                        }
                        .box-height{
                            height: 251px;
                        }
                        .pagination{

                            float: right !important;
                        }
                        .pull-right{

                        }

                    </style>
                    <?php $this->load->view($column_main_menu);?>

                </div><!-- /.container-fluid -->
        </div>

    </div>

    <h3 class="container">Setup Your Profile</h3>


    <div class="container container-content">
   

     <div class="col-md-12 add-border" style="background-color: white;">
      
         <div class='col-md-12 blue_border_container'>

			<div class="col-md-3" style="border-radius: 2px; border:2px solid rgba(128, 128, 128, 0.09);">
				<div class="col-md-12 bee_seen">
					<h1>BEE SEEN</h1>
					<h3 style="margin-top: -8px;">Get more noticed!</h3>
                </div>
                <div class='col-md-12 you-instantly'>
                    <h4 class="text-center">You Instantly have a <span class="blue-font">40%</span> higher chance of being <span class="blue-font">noticed</span> by uploading your profile photo <span class="blue-font">today!</span></h4>
                </div>
                </h4>
                <p class="having_photo_text">By having a photo, you become more available visably 
                        to existing users on the platform. Show your personality 
                        with MBU4U members today  & upload a picture.
                 </p>
				<div class="col-md-12 col-md-background-image"></div>
            </div>
       
            <div class="col-md-9 edit-profile" style="color:#999999;">
				<?php $this->load->view($show_error_page,$data);?>
				<h4 class="heading_color profile_personal_info_text">Personal Information</h4> 
				<hr class="hr_below_heading">
				<div class="row">
					<div class="col-md-6 form-group">
						<label for="full_name">Full Name:</label>
						<label class="profile_info_text">
							<?php echo ucfirst(strtolower($profile->fname));?>
						</label>
						<label class="profile_info_text">
							<?php echo ucfirst(strtolower($profile->lname));?>
						</label>
					 </div>
					<div class="col-md-6 form-group">
						<label for="username">Username:</label>
						<label class="profile_info_text"><?php echo $uname;?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 form-group">
						<label for="location">Location:</label>
						<label class="profile_info_text"><?php echo $profile->city.' '.$profile->state .' ,'.$profile->zipcode;?></label>
					</div>
				 </div>
				<div>
				 <?php 
						$attributes = array('class'=>'form-inline', 'id'=>'profile_edit' );
						echo form_open_multipart('/profile/save_profile',$attributes);
					?>
					
					<div class="col-md-6 no-left-padding form-group">
						<label>Email Address:</label>
						<br/>
						<input type="hidden" name='email' value="<?php echo $email; ?>">
						<label class="email-text"><?php echo $email; ?></label>
						<label style="font-size:.8em; color:darkblue;"onclick="document.getElementById('hidecurrentpass').style.display='block'; document.getElementById('hideEmailInput').style.display='block';"> (Change Email)</label>
					</div>
					<div class="col-md-6 no-left-padding form-group">
						<label>Password</label>
						<br/>
						<label>************</label>
						<label style="font-size:.8em; color:darkblue;" onclick="document.getElementById('hidecurrentpass').style.display='block'; document.getElementById('hidePassInput').style.opacity='100';"> (Change Password)</label>
					</div>
					<br/>
					<!-- start hidden email entry opens on click.... does not close... -->
					<div style="display:none" id="hidecurrentpass" >
						<div class="col-md-4 no-left-padding form-group" >
							<label class="profile_info_text">Password</label>
							<br/>
							<input type='password' class="form-control input-sm full-width" name="password" id="password"></input>
						</div>
						<div id="hidePassInput" style ="opacity:0;">
							<div class="col-md-4 form-group" id="newPass2">
								<label class="profile_info_text"> New Password</label>
								<br/>
								<input type='password' class="form-control input-sm full-width" id="new_password" name="new_password"></input>
							</div>
							<div class="col-md-4 form-group" id="newPass2">
								<label class="profile_info_text"> Confirm Password</label>
								<br/>
								<input type='password' class="form-control input-sm full-width" id="confirm_password" name="confirm_password"></input>
							</div>
						</div>
					</div>
					<div id="hideEmailInput" style ="display:none;">
					<div class="col-md-12 no-left-right-padding"></div>
						<div class="col-md-6 no-left-padding padding-top form-group">
							<label class="profile_info_text"> New Email Address</label>			
							<br/>
							<input type="email" class="form-control input-sm full-width"  id="new_email" name="new_email"></input>
						</div>
						<div class="col-md-6 form-group padding-top">
							<label class="profile_info_text"> Confirm Email Address</label>
							<br/>
							<input type='email' class="form-control input-sm full-width" id="confirm_email" name='confirm_email'></input>
						</div>	
						<br/>
					</div>
						<!-- ends hidden email area-->
					<br/>
					<div class="col-md-12 no-left-right-padding"></div>
					

					<!-- start hidden password entry opens on click.... does not close... -->
					
					
					<br/>
					 <div class="form-group col-md-12 top-padding">
						<hr class="hr_below_heading">
						<h4 class="">Profile Image </h4>
					</div>
					<div class="col-md-12 form-group no-left-right-padding">
						<div class="col-md-1 no-left-right-padding">
							<img id="preview" class='small-img' src="<?php base_url($profile_image);?>">
						</div>
						<div class="col-md-5 no-left-right-padding">    
							<input  type="text" id='file_path' name="file_path" class='form-control input-sm full-width' placeholder="No file selected" disabled="disabled" style="" />
						</div>       
						<div class="btn btn-primary fileUpload ">
							<span>Choose File</span>
							<input id="imgfile" name="imgfile" type="file" class="upload input-sm" accept="image/*" />
						</div>
					</div>
					<br/>
			  
					<hr class="" />     
					 <div class="form-group col-md-12 top-padding" >
						<label class="profile_info_text"> A Little About Myself</label>
						<br/>
						<textarea class="form-control full-width" cols="80" rows="2" id="bioinfo" name='bioinfo'><?php echo trim($profile->bioinfo) ;?></textarea>
					</div>

               

                
              

                <!-- Job infromation-->
                 <!--<h4 class="heading_color">Job Information </h4>
                <hr>
               

                 <div class="form-group">
                 <label class="sr-only">Job Title</label>
                  <label class="profile_info_text">Job Title</label>
                 <br/>
                  <input type="text" name="job_title" class="form-control input-sm" id="job_title" value="<?php echo $profile->job_title ;?>">
                </div>

                <div class="form-group">
                  <label class="sr-only">Company Information</label>
                  <label class="profile_info_text">Company Information</label>
                 <br/>
                   <input type="text" name="company_name" class="form-control input-sm" id="company_name" value="<?php echo $profile->company_name ;?>">
                </div> 
                 <div class="form-group">
                  <label class="sr-only">Website</label>
                  <label class="profile_info_text">Website Address</label>
                 <br/>
                   <input type="text" name="website" class="form-control input-sm" id="website" value="<?php echo  $profile->website ;?>">
                </div>-->

              

               <!--
                <h4 class="heading_color">Address Information </h4>

                <div class="address" style="padding-bottom:50px;">
                        
                        <div class="col-sm-4">
                          <label class="col-sm-2 control-label">City</label>
                           <input type="text" name="city" class="form-control input-sm" id="city" value="<?php echo $profile->city ;?>">
                        </div>

                         <div class="col-sm-4">
                <label class="col-sm-2 control-label">State</label>
                <?php
                $default = array(''=>'Select State') ;
                echo form_dropdown('state', $profile->state,array_merge($default,$states),'class="form-control input-sm"  tabindex="7"' );
                ?>

                        </div>
                         <div class="col-sm-4">
                <label class="col-sm-2 control-label">Zip</label>
                <input type="text" name="zip" class="form-control input-sm" id="zip" value="<?php echo $profile->zipcode ;?>">

                        </div>
                </div>
                 <hr>

                <h4 class="heading_color">Job Information </h4>
               
                      <div class="form-group">
                        <label class="sr-only">Job Title</label>
                           <label class="profile_info_text"> Job Title</label>
                      </div>
                      <div class="form-group">
                        <label for="job_title" class="sr-only">Job Title</label>
                        <input type="text" name="job_title" class="form-control input-sm" id="job_title" value="<?php echo $profile->job_title ;?>">
                      </div>
                    &nbsp;
                       <div class="form-group">
                        <label class="sr-only">CompanyName</label>
                         <label class="profile_info_text"> Company Name</label>
                      </div>
                      <div class="form-group">
                        <label for="company_name" class="sr-only">Company Name</label>
                        <input type="text" name="company_name" class="form-control input-sm" id="company_name" value="<?php echo $profile->company_name ;?>">
                      </div>
            </div>
            <br/> -->
			<div class="form-group col-md-12 top-padding"></div>
			<hr class="hr_below_heading">
			<h4 class="">Shopping information</h3>

			
			<div class="col-md-6 no-left-padding form-group ">
				<label class="profile_info_text">
					<span class="required_star">*</span>Address Line 1
				</label>
				<br/>
				<input type='text' class="form-control input-sm full-width" name="address_line1"  value="<?php if($shipping['shipping_address_line_1'] != ''){echo $shipping['shipping_address_line_1'];} ?>" ></input>
			</div>                

			<div class="col-md-6 no-left-padding ">
				<label class="profile_info_text">Address Line 2</label>
				<br/>
				<input type='text' class="form-control input-sm full-width" name="address_line2" value="<?php if($shipping['shipping_address_line_2'] != ''){echo $shipping['shipping_address_line_2'];} ?>" >
			</div>
			<div class="form-group col-md-12"></div>
			<div class="col-md-4 no-left-padding form-group top-padding">
				<label class="profile_info_text">
					<span class="required_star">*</span>City
				</label>
				<br/>
				<input type='text' class="form-control input-sm full-width" name="city" value="<?php if($shipping['shipto_city'] != ''){echo $shipping['shipto_city'];} ?>"></input>
			</div> 
			<div class="col-md-3 no-left-padding col-md-offset-1 form-group top-padding" >
				<label class="profile_info_text">
					<span class="required_star">*</span>State
				</label>
				<br/>
				<?php  echo form_dropdown('state', array_merge(array('#'=>'Please select state'), $states) ,set_value('state',((!empty($shipping['shipto_state_full'])) ? $shipping['shipto_state_full'] :'' )),'class="form-control"  tabindex="-1" placeholder="State" style="color:grey;"' ) ;?>
			</div>
			<div class="col-md-2  col-md-offset-1 form-group top-padding">
				<label class="profile_info_text"><span class="required_star">*</span>Zip Code</label>
				<br/>
				<input type='text' class="form-control input-sm full-width" name="zipcode" value="<?php if($shipping['shipto_zip'] != ''){echo $shipping['shipto_zip'];} ?>">
			</div>
			<br/>       
			
			<div class="col-md-2 col-md-offset-3 top-padding form-group">
				<button class="btn btn-primary  save-profile-btn  btn-default center full-width">Save Profile</button>
             </div>
             <div class="col-md-2 top-padding col-md-offset-3" >
				<button class="btn cancel-btn   btn-default center full-width">Cancel</button>
             </div>
		</div>
                <!--<a href="<?php echo base_url('payment/payment');?>" class="btn btn-default" style="margin-left:5px;">SKIP</a>
               -->
                <?php  echo form_close(); ?>
        </div>
     
          </div>

    </div>


</section>



<footer class="footer">

    <?php
    $this->load->view($footer_findout);
    $this->load->view($footer_subscribe);
    //$this->load->view($footer_privacy); 
    
    ?>

</footer>

<!-- Bootstrap and Jquery and Other JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>

<script type="text/javascript">

    /***global base url path in javascript***/
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split('/');
    var base_url_complete = base_url+'/'+pathArray[1]+'/';
    var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;
    var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

   /*
   /* prepare profile image to be previewed before actual upload
   /* this will be called on change even of the file / upload component
   /*
    *
    * */
     $( "#imgfile" ).change(function(event) {
        var output = document.getElementById('preview');
        output.src = URL.createObjectURL(event.target.files[0]);
        $( "#file_path").val('file selected');
      });

       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);  

</script>
</body>
</html>
