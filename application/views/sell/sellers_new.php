<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Seller Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/sellers_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/slimbox2.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
</head>

<body>       
     <?php $this->load->view($notification_bar); ?>
<header>
    <style>
        .alert-success {
            color: #0B69A0;
        }
        .menu-column_submenu  {
            margin: 0;
            padding: 0;
            font-family: 'Oswald',sans-serif;
            font-weight: 300;
            font-size:medium ;

        }
    </style>
      <?php $this->load->view($header_black_menu); ?>   
      <?php $this->load->view($header_logo_white); ?>
</header>
<!-- Responsive design
================================================== -->
<section id="responsive" >

   <!--load menu here -->
<div class="row white-bg">
    <hr class="" style="margin: 0px;">
    <div class="container white-bg " style="">

        <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">

                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <style type="text/css">
                    .main-navigation {
                        background-color: #FAFAFA;
                    }
                    .navbar-default {
                        background-color: inherit;
                        border: none;
                    }
                    .blue-font {
                        color: #2f97cc;
                    }
                    .grey-background{
                        background: #ebebeb;
                    }
                    .bottom-blue-border{
                        border-bottom: 4px solid #216da1;
                    }
                    .wrapper {
                        text-align: center;
                    }

                    .start-shopping-btn {
                        position: absolute;
                        top: 83%;
                        left: 42%;
                        font-size: 27px;
                        background: #0b69a0;
                    }

                    .black-btn{
                        background:#303030;
                    }
                    .box-height{
                        height: 251px;
                    }
                    .pagination{

                        float: right !important;
                    }
                    .pull-right{

                    }

                </style>
                <?php $this->load->view($column_main_menu);?>

            </div><!-- /.container-fluid -->
    </div>
    </div>
            
             

  <div class='container container-seller-details'>
      <?php $this->load->view('chat/chat_dialog_html');?>

              <!--new blue row-->

<div class="row row-details blue-bg" style="margin-top: -11px;padding: 37px 141px 27px 100px;">
                    
                    <div class="col-xs-12 col-sm-12 col-md-9 detail_info white-font" style="padding-left: 12px;">
                   
                      <div class="media">
                          <div class="col-md-3">
                            <a class="media-left media-top" href="#" style="padding-bottom: 9px">
                              <img class="img-circle pull-left user-small-image" src="<?php echo base_url($profile_image);?>" alt="..." height="150" width="150">
                            </a>
                          </div>  
                         <div class="media-body col-md-4 no-left-padding" >
                              <div class="bodys">
                               <h3 class="media-heading"><span class="user-big span-right-padding" style="padding-left:30px;"></span><?php echo ucfirst($profile->fname) .' ' .ucfirst($profile->lname) ;?><small> </small></h3>

                                <p class="online_offline_p">
                                  <?php //dump($is_logged_in);
                                  //dump(in_array($profile->id,$loggedin_profile_lists));
                                  ;?>

                                 <!--check if user is online or not start -->

                                   <?php if($is_logged_in===TRUE && in_array($profile->id,$loggedin_profile_lists) ){ ?>

                                    <span class="online-status span-right-padding" style="padding-left:30px;"></span>
                                    <span class="">Online</span>

                                   <?php } else { ?>

                                        <span class="offline-status span-right-padding" style="padding-left:30px;"></span>
                                        <span class="">Offline</span>

                                    <?php } ?>

                                <!--check if user is online or not end -->
                                
                                 </p>

                                <p>Job Title: <span> <?php echo trim(ucfirst($profile->job_title));?></span></p>
                                <p>Company:  <span><?php echo trim(ucfirst($profile->company_name));?></span></p>
                                <p>Located In: <span> <?php echo $profile->state ; ?>, USA</span></p>
                                 
                 
                        
                             </div>
                           </div>

                           <div class="col-md-5"  style="border-left: 2px solid rgb(48, 131, 179);height: 153px;">

                           <h4 style="padding-top: 0px;">About Myself</h4>

                               <p>
                                <?php echo character_limiter($profile->bioinfo,185) ; ?>
                            </p>

                           </div>
                        </div>



                    </div>



                      <div class="col-xs-12 col-sm-12 col-md-3 buttons ">
                          <div class="form-inline pull-right">
                           <div class="form-groups profile-user-top-btns">

                                 <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==true):?>

                                   <button id="refer_a_friend_button" type="button" class="btn btn-primary btn-sm pull-left full-width" data-toggle="modal" data-target="#inviteModal">
                                       <i class='glyphicon glyphicon-bullhorn'></i>Refer a Friend Today
                                   </button>

                                   <!-- Refer a Friend Model -->
                                   <div class="modal fade" id="inviteModal" tabindex="-1" role="dialog" aria-labelledby="inviteModalLabel" aria-hidden="true" style="top:30%;">
                                       <div class="modal-dialog">
                                           <div class="modal-content">
                                               <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                   <h4 class="modal-title" id="myModalLabel" style="font-style:bold;">Refer a Friend to Join Madebyus4U.com!</h4>

                                               </div>
                                               <div class="modal-body col-md-12">

                                                   <div class="form-inline form-group">

                                                       <input type="text" class="form-control" id="recipients" style="width:400px;background-color: rgb(245, 244, 244);" value="Send multiple emails separated by comma">
                                                   </div>

                                               </div>
                                               <div class="modal-footer">

                                                   <button type="button" id='send_invites_btn' data-toinvited-id="<?= $profile->id ;?>"  class="btn btn-primary">Send Invites</button>
                                               </div>
                                           </div>
                                       </div>
                                   </div>


                               <?php endif; ?>

                               <?php if($is_logged_in===false):  ?>

                                       <a href="<?php echo base_url('signup');?>" class="btn btn-danger btn-sm pull-left ">
                                           <span class="friends-white span-right-padding" style="padding-left:30px;"></span>Friend Request
                                       </a>

                                       <a href="<?php echo base_url('signup');?>" class="btn btn-warning btn-sm pull-left" >
                                           <i class="glyphicon glyphicon-envelope" style="color: #ffffff !important"></i>Send Message
                                       </a>

                                       <a href="<?php echo base_url('signup');?>" class="btn btn-success btn-sm pull-left">
                                           <span class="chatIcon span-right-padding" style="padding-left:30px;"></span>Start a chat
                                       </a>

                               <?php endif; ?>

                               <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==false):  ?>

                                   <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
                                       <a href="#" id='friend_request_btn' data-status="REQUEST" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-danger btn-sm pull-left full-width">
                                           <span class="friends-white span-right-padding" style="padding-left:30px;"></span>Friend Request

                                       </a>
                                   <?php endif;?>

                                   <?php if($this_user_has_sent_request==true && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
                                       <a href="#" id='friend_request_btn' data-status="<?= ($this_user_has_sent_request==true)? 'SENT' : 'REQUEST'; ?>" data-reciever-id="<?php echo $profile->id;?>"class="btn btn-danger btn-sm pull-left full-width">
                                           <span class="friends-white span-right-padding" style="padding-left:30px;"></span><?= ($this_user_has_sent_request==true)? 'Cancel Friend Request' : 'Send Friend Request'; ?>
                                       </a>

                                   <?php endif;?>

                                   <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==true && $this_user_is_friend==false) :?>
                                       <a href="#" id='friend_accept_btn' data-status="Accept" data-sender-id="<?php echo $profile->id;?>"  class="btn btn-danger btn-sm pull-left full-width">
                                           <span class="friends-white span-right-padding" style="padding-left:30px;"></span>Accept Friend Request
                                       </a>
                                   <?php endif;?>

                               <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==true): ?>

                                       <button type="button" href="#" data-target="#composeNew2" data-toggle="modal" aria-controls="inbox" type="button" class="btn btn-warning btn-sm pull-left" >
                                         <i class="glyphicon glyphicon-envelope" style="color: #ffffff !important"></i>Send Message
                                      </button>

                                       <button type="button" class="send-chat-request btn btn-success btn-sm pull-left" data-confirmation-dialog ="chat-confirmation-dialog" data-username ="<?= $profile_username[0]['username'] ?>" data-full-name ="<?= $profile->fname ?>">
                                         <span class="chatIcon span-right-padding" style="padding-left:30px;"></span>Start a chat
                                      </button>

                                       <a href="#" id='' data-status="" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-sm pull-left btn-gray margin-btm"><i class="block-icon"></i>BLOCK FRIEND</a>

                                <?php endif;?>

                               <?php endif; ?>

                          <!-- Refer a Friend Model -->
                              <div class="modal fade" id="inviteModal" tabindex="-1" role="dialog" aria-labelledby="inviteModalLabel" aria-hidden="true" style="top:30%;">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      <h4 class="modal-title" id="myModalLabel" style="font-style:bold;">Refer a Friend to Join Madebyus4U.com!</h4>
                                      
                                    </div>
                                    <div class="modal-body col-md-12">
                                      
                                      <div class="form-inline form-group">
                                       
                                      <input class="form-control" id="recipients" style="width:400px;background-color: rgb(245, 244, 244);" value="Send multiple emails separated by comma" type="text">
                                      </div>
          
                                    </div>
                                    <div class="modal-footer">
                                     
                                      <button type="button" id="send_invites_btn" data-toinvited-id="3" class="btn btn-primary full-width">Send Invites</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                               <!--END OF MODAL-->
                               <!--SEND MESSAGE-->
                               <!-- Button trigger modal -->

                               <!-- Modal -->
                               <div class="modal fade" id="composeNew2" tabindex="-1" role="dialog" aria-labelledby="composeNew">
                                   <div class="modal-dialog model-dialog-sellers" role="document">
                                       <div class="modal-content">
                                           <div class="modal-header">
                                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                               <h4 class="modal-title" id="composeNew2"><i class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true" style='padding-right:5px;'></i> Compose a Message</h4>
                                           </div>
                                           <div class="modal-body" style="font-color:grey;font-style:italic;">
                                               <!--show success message -->
                                               <div  class="sent-message alert alert-success alert-dismissible" role="alert" style="display:none;">
                                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                   <strong>Success!</strong> Your message sent successfully!
                                               </div>

                                               <form>
                                                   <div class="input-group">

                                               <span class="input-group-addon">
                                               <img id="memeber_image_top_location" class='' src="<?php echo base_url().'/uploads/no-photo.jpg';?>" height=80 width=80/>
                                               </span>
                                                       <?php  //var_dump($catagories);
                                                       $members_list['#'] = 'Select a Memeber';
                                                       echo form_dropdown('selected_member_from_top_location', $members_list,'#','id="selected_member_from_top_location"
                                                            class="form-control" required style="color:grey;font-style:italic;"'); ?>


                                                   </div>
                                                   <p></p>
                                                   <div class="form-group">
                                                       <input class="form-control" id="subject" placeholder="Subject:" required></input>
                                                   </div>
                                                   <div class="form-group" style="display:block;margin-top:2%;">
                                                       <textarea class="form-control" rows='5' id="message_text" required placeholder="Type your message here.."></textarea>
                                                   </div>
                                               </form>

                                           </div>
                                           <div class="modal-footer">
                                               <button type="button" class='send_message' id="send_message2" name="send_message2" class="btn btn-primary pull-right"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i>Send message</button>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <!--ENE OF SEND MESSAGE -->

                           </div>

                          </div>
              <div class="clearfix"> </div>
                                        
                    </div>

              </div>


       <!--end of blue row-->  
       <div class='row' style="margin: -11px -90px 0px -90px;"> 

              
              <?php if($error):?>

                <div class="error-message col-md-12">  
                  <?php $this->load->view($show_error_page,$data);?> 

                </div>

              <?php endif;?>

              <?php if(!$error): ?>

       <div class="row row-under-container" >
     
        <!--<div class="col-md-1">Web | <span class='webaddres'><a href='<?php echo $profile->website;?>' target="_blank"><?php echo $profile->website;?></a> </span>  </div> -->
		 <!--<div class="col-md-1 rating_left rating_col">
			<input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$profile->id;?>" value="<?= $profile->avg_rating ;?>" data-type="profile"/>
		 </div>-->        
        <div class="tab-menu">
          <a href="#listing" class="selected" aria-controls="listing" role="tab" data-toggle="tab" class='tab-links'><span id="listingsImg" class="span-right-padding"style="padding-left:30px;"></span><span> Listings</span><span>&nbsp(<?=$product_total_rows;?>)</a></span>
          <a href="#store" data-toggle="tab"  class='tab-links'><span id="storeImg" class="span-right-padding" style="padding-left:30px;"></span><span> Stores</span><span>&nbsp(<?=$store_total_rows; ?>)</span></a>
          <a href="#likes" aria-controls="likes" role="tab" data-toggle="tab" class='tab-links'><span id="whishlistImg" class=" glyphicon glyphicon-heart" ></span><span > Whishlists</span><span>&nbsp (<?php echo $products_i_like_count;?>)</span> </a>
          <a href="#friends" aria-controls="friends" role="tab" data-toggle="tab" class='tab-links'><span id="friendsImg" class="span-right-padding" style="padding-left:30px;"></span><span >Friends</span><span>&nbsp(<?=$count_friends; ?>)</span></a>
          <a href="#photos" aria-controls="videos" role="tab" data-toggle="tab" class='tab-links'><span id="photosImg" class="span-right-padding"style="padding-left:30px;"> </span>Photos<span >&nbsp(<?=$count_photos; ?>)</span></a>
          <a href="#videos" aria-controls="videos" role="tab" data-toggle="tab" class='tab-links'><span id="videosImg" class="span-right-padding"style="padding-left:30px;"> </span>Videos<span >&nbsp(<?=$count_videos; ?>)</span></a>
        </div>

      </div>


              <div class="container" style="padding: 15px 18px;">
               
                <!-- Tab panes -->
               
                <div id="sellersTabContent"  class="tab-content">


                  <div role="tabpanel" class="tab-pane " id="listing">

                            <div class="alert alert-success listing_wrapper" style="border:none">

                            <?php 

                              if($product_total_rows> 0) { ?>

                               <!-- <h4> Showing  (<?=$product_total_rows;?>) Listing </h4>
                                <hr class="hr_border">-->
                                    <?php $this->load->view('product/product_listing_new'); ?>
                                <div class="col-md-12 pagination_section">
                                              <div class="pull-right">
                                                      <?php $this->load->view($paginate_page,$product_links); ?>
                                              </div>    
                                      </div>

                                  <?php } else {
                          
                                   echo "<div class='alert alert-success'> <h4> We are sorry , no products to added yet! </h4></div>";
                          
                            } ?>
                          
                                          
                          </div>


                  </div>

                  <div role="tabpanel" class="tab-pane"  id="store">
                         <div class="listing_wrapper alert alert-success">

                               <?php $this->load->view('include/store_listing_tab_new'); ?></h3>
                                                                                                                  
                        </div>
                    


                  </div>
                <!--likes-->
                  <div role="tabpanel" class="tab-pane" id="likes">

                     <div class="listing_wrapper alert alert-success">

                            <?php 

                              if($products_i_like_count> 0) { ?>

                               <!-- <h4> Showing  (<?=$products_i_like_count;?>) Listing </h4>
                                <hr class="hr_border">-->
                                    <?php $this->load->view($likes_view,$liked_products); ?>
                                    

                                  <?php } else {
                          
                                   echo "<div class='alert alert-success'> <h4> We are sorry , you have not liked any products! </h4></div>";
                          
                            } ?>
                          </div>

              
                  </div>
                <!--end likes-->

                   <!--friends-->
                   <div role="tabpanel" class="tab-pane" id="friends">

                      <div class="listing_wrapper alert alert-success">

                          <?php 

                          if($count_friends> 0) { ?>

                          <!--  <h4> Showing  (<?=$count_friends;?>) Listing </h4>

                           <hr class="hr_border">-->
                          
                            <?php $this->load->view('friends/friends_view_new');?>
                         
                          <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no friends added yet! </h4></div>";
                          
                          } ?>

                           </div>
                   </div>

                   <!--freind end--> 


                  <!--video tab-->
                    <div role="tabpanel" class="tab-pane" id="videos">
                         
                         <div class="col-md-12 listing_wrapper alert alert-success">
                    
                       <!--  <h4> Showing  (<?=$count_videos;?>) Listing </h4>

                           <hr class="hr_border">-->
                           
                              <?php $this->load->view('videos/videos_view_new');?>
                   
                          </div>

                    </div>
                  <!--end of video tbas-->

                  <!--message tab -->
                  
                  <?php if($is_logged_in===TRUE): ?>

                     <?php if(count($inbox_messages?$inbox_messages:0)+count($sent_messages)+count($trashed_messages)> 0) { ?>

                       <div role="tabpanel" class="tab-pane" id="messages">
               
                            <div class="listing_wrapper alert alert-success">

                               <!--   <h4> Showing  (<?=count($inbox_messages?$inbox_messages:0)+count($sent_messages)+count($trashed_messages);?>) Listing </h4>-->

                                  <hr class="hr_border">

                                    <?php $this->load->view('messages/messages_view_new'); ?>

                              </div>

                       <?php } else {
                          
                             echo "<div class='alert alert-success'> <h4> We are sorry , no messages to show ! </h4></div>";
                          
                          } ?>

              
                  </div>                   
                  <?php endif ?>

                  <!--end of message tab-->
                   <!--photos tab-->
                    <div role="tabpanel" class="tab-pane" id="photos">

                          
                         
                                    <div class="listing_wrapper alert alert-success">                                       
                                   <!--   <h4> Showing  (<?=$count_photos; ?>)Listing </h4>                                      <hr class="hr_border">-->
                                       <?php $this->load->view('photos/photos_view_new'); ?>
                                    </div>

                          


                   </div>
                   <!--end of photos tab-->


                  <!--notification tab-->
                   <?php if($is_logged_in===TRUE): ?>

                   <div role="tabpanel" class="tab-pane" id="notifications">


                          <?php if( $this->current_notify_notifications_counts > 0) { ?>

                                      <div class="listing_wrapper alert alert-success">                                                                               
                                        <h4> Showing  (<?=$this->current_notify_notifications_counts; ?>) Listing </h4>
                                        <hr class="hr_border">
                                        <?php $this->load->view($notification_view,$notifications); ?>                                                           
                                     </div>

                       <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no notifications recieved ! </h4></div>";
                          
                          } ?>



                   </div>                   
                   <?php endif ?>
                    <!--end of notification tab-->
                   <?php if($is_logged_in===TRUE): ?>
                    <!--friend requests-->
                   <div role="tabpanel" class="tab-pane" id="friend_request">

                      <div class="listing_wrapper alert alert-success">
                          <?php if( $this->current_notify_notifications_counts > 0) { ?>
                            
                            <h4> Showing  (<?=$count_friend_request;?>) Listing </h4>
                            <hr class="hr_border">
                           <?php $this->load->view($friends_request_view);?>
                      </div>
                          <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no notifications recieved ! </h4></div>";
                          
                          } ?>



                   </div>
                   <?php endif ?>              


<?php endif ?>          

                   <!--freind request end--> 
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script src="<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/tab_selected.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/load_images_for_options.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/friend_request.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/photo-image-loaders.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/messages.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/video-photo-page.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/refer-a-friend.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/activate_link_tab_per_hash.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/jquery/slimbox2.js";?>"></script>
     <script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>
<!--chat -->
<script type="text/javascript" src="<?php echo base_url()."assets/ckeditor/ckeditor.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/chat_server/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/chat_server/node_modules/node-uuid/uuid.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/chat_js.js";?>"></script>
<!--chat ned -->
<!--
<script type="text/javascript" src="http://localhost:3000/socket.io/socket.io.js"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/notification_client.js";?>"></script>
-->
<script type="text/javascript">
  /***global base url path in javascript***/
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split('/');
  var base_url_complete = base_url+'/'+pathArray[1]+'/';
  var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ; 
  var csrf_token = "<?= $this->security->get_csrf_token_name();?>";
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

   // $('.lightbox').lightBox();

 
  /***gloabl base_url path_end***/
    
  function beepSound(filename) {
    // @param filename The name of the file WITHOUT ending
    var path="assets/sounds/"+ filename + ".mp3";
    document.getElementById('sound').innerHTML="<audio autoplay=autoplay><source src='"+path+"' type=audio/mpeg/></audio>";
  } 

  /***
   * Created by Daniel Adenew
   * Submit email subscription using ajax
   * Send email address
   * Send controller
   * Recive response
   */
  var url =  "<?php echo site_url('welcome/subscribe');?>";
  subscribe_using_ajax(url);
</script>



<script>



</script>

<script type="text/javascript">
  
  //add the id of the user to server to route notification per profile id
  $(function(){
    var profile_id = "<?php echo $this->profile_id;?>";
   // alert(profile_id);
     //socket.emit('add client',{"profile_id":profile_id });
  });

</script>


<script>
var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

         $(".rating").rating('refresh',
            {showClear: false, showCaption:false,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });

          $('.rating').on('rating.change', function() {


           if(!readOnly) {

           var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
           var to_extract_from_id_text = null;

           var value =  $(this).val();

           var result = confirm("Are you sure you want to rate this "+ type_of_item_rated+" "+value+" ?");

           if(result==true) {

               if(type_of_item_rated=='store') {
                   to_extract_from_id_text = "rating_element-store";
               }
               if(type_of_item_rated=='product') {
                   to_extract_from_id_text = "rating_element-";
               }
               if(type_of_item_rated=='profile') {
                   to_extract_from_id_text = "rating_element-";
               }


           var static_id_text=(to_extract_from_id_text).length;
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();
           var server_url = base_url_complete+'rating/rate';

           save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

          }
         }
           else {
            window.location.assign(base_url_complete+'users/login');
           }

        });




    $(function(){

          var menu = $('.tab-menu');

         // Mark the clicked item as selected

          menu.on('click', 'a', function(){
              var a = $(this);

              a.siblings().removeClass('selected');
              a.addClass('selected');
          });
      });
</script>


<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".tab-menu a").click(function(){
            $(".tab-menu span").removeClass("active");
            $("span",this).addClass("active");
        });
    })
</script>



</body>
</html>

