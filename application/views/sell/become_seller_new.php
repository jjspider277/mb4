<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Sell</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/sell.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">

    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">



</head>
<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>

    <style>
/*        .low_fees_box{*/
/*            background:url('*/<?php //echo base_url()."assets/images/sell/low_fees_box_1.png" ; ?>/*') no-repeat;*/
/*        }*/
/*        .power-full-tools{*/
/**/
/*        }*/
        .box-padding{
            padding: 63px 28px 0px;
            height: 242px;

        }
        .box-padding-4th-box{
            padding: 63px 28px 0px;
            height: 172px;
            margin-bottom: 17%;
        }
        .icons-in-box{
            z-index: 900;
            margin: -70px 107px;
            position: absolute;
        }
        .icons-in-box-4th{
            z-index: 900;
            margin: -52px 117px;
            position: absolute;
            padding: 10px;
        }
.navbar {
    margin-bottom: 0px;
}
.row5 {
    padding: 2px;
    border: 2px solid #F3F3F2;
    border-top: 7px solid grey;
    border-radius: 11px;
}
.menu-column_submenu  {
    margin: 0;
    padding: 0;
    font-family: 'Oswald',sans-serif;
    font-weight: 300;
    font-size:medium ;

}


#search{
    background: #d3d3d3;
    cursor: pointer;
    font-size: 24px;
    font-weight: bold;
    text-transform: lowercase;
    padding: 20px 2%;
    width: 96%;
}
#search-overlay{
    background: black;
    background: rgba(255, 255, 255, 255);
    color: black;
    display: none;
    font-size: 18px;
    height: 200px;
    padding: 0px;
    margin-top:28px;
    position: absolute;
    width: 436px;
    z-index: 100;
    opacity: 0.95;
    border-radius: 4%;
    border: 2px solid #efefef;
    overflow: auto;
}
#display-search{
    border: none;
    color: black;
    font-size: 14px;
    margin: 5px 0 0 0;
    width: 400px;
    height: 20px;
    padding: 0 0 0 10px;
    display: none;
}

#hidden-search{
    left: -10000px;
    position: absolute;

}

#results{
    display: none;
    width: 300px;
    list-style: none;
}
#results ul {
    list-style:none;
    padding-left:0;
}​
 #results ul li{
     list-style: none;
     padding-left:0;
 }

#results ul li a{
    color:#2676af;
    font-size: 12px;
    font-weight: bold;
}
}
#search-data{
    font-size: 14px;
    line-height: 20%;
    padding: 0 0 0 20px;

}

h2.search-data{
    margin: 10px 0 30px 0;

}
    </style>
</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <!-- menu goes here -->
    <div class="row white-bg">
        <hr class="" style="margin: 0px;">
    <div class="container white-bg " style="">

        <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">

                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <style type="text/css">
                    .main-navigation {
                        background-color: #FAFAFA;
                    }
                    .navbar-default {
                        background-color: inherit;
                        border: none;
                    }
                    .blue-font {
                        color: #2f97cc;
                    }
                    .grey-background{
                        background: #ebebeb;
                    }
                    .bottom-blue-border{
                        border-bottom: 4px solid #216da1;
                    }
                    .wrapper {
                        text-align: center;
                    }

                    .start-shopping-btn {
                        position: absolute;
                        top: 83%;
                        left: 42%;
                        font-size: 27px;
                        background: #0b69a0;
                    }

                    .black-btn{
                        background:#303030;
                    }
                    .box-height{
                        height: 251px;
                    }
                    .pagination{

                        float: right !important;
                    }
                    .pull-right{

                    }

                </style>
                <?php $this->load->view($column_main_menu);?>

            </div><!-- /.container-fluid -->
    </div>
    </div>






    <!--menu ends here -->

    <div class="row become-seller-head white-bg" style="background-image:url('<?php echo base_url()."assets/images/sell/become_seller_header.jpg" ; ?>');background-repeat: no-repeat;padding-bottom: 30px;;    background-size: 100%;">
        <div class="container" style="padding: 341px 0px 141px;">

            <div class="col-md-2 col-md-offset-5">
                <a class="btn btn-primary btn-lg open-store-btn blue-bg" href="<?php echo base_url('signup');?>" role="button">Open your MB4U store</a>
            </div>
        </div>

    </div>
    <div class="container" >
        <div class="col-md-12 " style="margin-bottom: 7%">
            <h2 class="text-center" style="padding: 10px 11px;font-size: 35px;">
                Become a seller today and start building your ecommerce relationship
                with your customers around the world
            </h2>
            <hr>
        </div>
        <div class="col-md-12 become-seller-first-inner-box">
            <div class="col-md-4 col-sm-6 ">
                <div class="col-md-12 low-fee  low_fees_box white-bg box-padding" >
                    <h3 class="text-center">Low Fees</h3>
                    <p class="text-center">
                        Build your personal profile, including
                        products, stores, photos, videos and
                        more! Become friends with your
                        buyers and Live Chat with your
                        customers direclty.
                    </p>
                </div>
                <div class="icons-in-box"><img src="<?php echo base_url('assets/images/sell/low-fee.png');?>"></div>

            </div>
            <div class="col-md-4  col-sm-6 ">
                <div class="col-md-12 power-full-tools white-bg box-padding">
                    <h3 class="text-center">Powerful Tools</h3>
                    <p class="text-center">
                        Our tools and services make it easy to
                        manage, promote and grow your
                        business. While having the ability
                        to communicate with other sellers
                        and buyers.
                    </p>
                </div>
                <div class="icons-in-box"><img src="<?php echo base_url('assets/images/sell/powerfull_tools.png');?>"></div>

            </div>
            <div class="col-md-4 col-md-offset-0  col-sm-6 col-sm-offset-3">
                <div class="col-md-12 promote-your-store white-bg box-padding">
                    <h3 class="text-center">Promote Your Store</h3>
                    <p class="text-center">
                        Upload and Sell! It’s that easy,
                        create your own custom store and
                        promote to your customers
                        using our refer to friend feature.
                    </p>
                </div>
                <div class="icons-in-box"><img src="<?php echo base_url('assets/images/sell/promote-store.png');?>"></div>

            </div>
            <div class="col-md-4 col-md-offset-4  col-sm-6 col-sm-offset-3" style="margin-top: 98px;">
                <div class="col-md-12 start-selling-today grey-border box-padding-4th-box">
                    <h4 class="text-center">Start Selling Today</h4>
                    <div class="col-md-8 col-md-offset-2 no-left-right-padding">
                        <button class="btn btn-primary full-width open-store-btn" href="<?php echo base_url('signup');?>" role="button">Open Your MB4U Store</button>
                    </div>
                </div>
                <div class="icons-in-box-4th grey-bg " ><img src="<?php echo base_url('assets/images/sell/start-selling-today.png');?>"></div>
            </div>
        </div>

    </div>
    <div class="row white-bg">
        <div class="container" style="padding:53px 0px">
            <div class="col-md-offset-4 col-md-4 no-left-right-padding">
                <h2 class="text-center">Help When You Need It</h2>
                <p class="text-center"> We’re committed to helping our 1.5 million sellers thrive,
                    with support and education for shops big and small.</p>
            </div>
            <div class="col-md-12 help" style="padding: 12px 54px;">
                <div class="col-md-6 ">
                    <div class="col-md-3">
                        <img src="<?php echo base_url()."assets/images/talk_to_us.png";?>">
                    </div>
                    <div class="col-md-8">
                        <h3>Talk to us</h3>
                        <p>Reach our support staff by email or
                            request a phone call whenever you have
                            a question.</p>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="col-md-3">
                        <img src="<?php echo base_url()."assets/images/shoppers_blog.png";?>">
                    </div>
                    <div class="col-md-8">
                        <h3>Shoopers Blog</h3>
                        <p>Learn more about whats going on with
                            MBU4U. Read about our latest news,
                            promotions, and general information. </p>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="col-md-3">
                        <img src="<?php echo base_url()."assets/images/sellers_newsletter.png";?>">
                    </div>
                    <div class="col-md-8">
                        <h3>Sellers Newsletter</h3>
                        <p>Sign up for the MBU4Success newsletter
                            for insights about improvingyour shop
                            delivered straight to your inbox.</p>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="col-md-3">
                        <img src="<?php echo base_url()."assets/images/get_advice.png";?>">
                    </div>
                    <div class="col-md-8">
                        <h3>Get Advice</h3>
                        <p>Ask questions and find a comumunity of
                            sellers like you in MBU4U’s forums and
                            Teams.</p>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row black-bg">
        <div class="container" style="padding:21px 24px 0px">
            <div class="col-md-6">
                <p class="white-font" style="font-size: 20px;">Want to <span class="blue-font">Earn Money?</span> Why wait</p>
                <p style="color:#939090;">Become a seller on MadeByUs4u and start making money today.</p>

            </div>
            <div class="col-md-2 col-md-offset-2">
                <a class="btn btn-primary" href="<?php echo base_url('signup');?>" role="button">START SELLING TODAY</a>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>



<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    $(document).ready(function() {


    });

    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);

</script>
</body>
</html>
