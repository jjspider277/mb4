

<style type="text/css">

    .rating_class {
        float: right;

    }
    .rating-xs {
        font-size: 1.4em;
    }

    .rating-gly-star {
        font-family: 'Glyphicons Halflings';
        padding-left: 2px;
        font-size: smaller;
    }

    .star-rating .caption, .star-rating-rtl .caption {
        color: #999;
        float: right;
        display: inline-block;
        vertical-align: middle;
        font-size: 80%;
    }
    /**added by eyayu */
    .no-left-padding{
        padding-left:0px;
    }
    .no-right-padding{
        padding-right: 0px;
    }
    .no-left-right-padding{
        padding-left:0px;
        padding-right:0px;

    }
    .member-container{

        margin-bottom:36px;*/

    }
    div.member-container > .item-lisiting-inner{
        border:none;
        color:#444444;
        margin:0px;
        padding: 15px 0px 0px 0px;
    }
    div.rating_social > div.col-sm-3 > p{
        padding: 7px 26px;
    }
    .member_name{
        color:#444444;
    }
    .chat-request-btn{
        width:100%;
        background: #0cb07f;
        border:0px;
        font-weight: bold;
        border-radius: 0px;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
    }
    .glyphicon-user{
        padding-right: 11px;
        font-size: 16px;
    }
    .fa-comment{
        font-size: 16px;

        padding-right: 16px;
    }
    .blue-fa-thumbs-up{
        color:#1a72a6 !important;
        font-size:16px;
    }
    .fa-shopping-cart,.fa-users{
        color:#ababab;
    }
    .white-background{
        background:#ffffff;
    }

    div.thumbnail > div.text_content > div.col-sm-12 > div.col-sm-6 > span{
        padding-right:8px;
    }
    div.thumbnail > div.text_content > div.col-sm-12 > div.friends {
        border-right: 1px solid rgb(203, 203, 203);
    }
    div.thumbnail > div.col-sm-1 > button.btn{
        background: #ffffff;
        border:1px solid #dbdbdb;
    }
    div.text_content > div.seller_location > h4 {
        padding-top:0px;
    }
    input[type=checkbox] {

        transform: scale(1.5);


        -webkit-transform: scale(1.5);
        -moz-appearance:normal;
        -webkit-appearance:normal;
        -o-appearance:normal;
    }
    /** end of css added by eyayu */
    @media (min-width: 992px) {
        .col-md-3 {
            width: 25%;
        }
    }
</style>






<div class="row">
    <div class="col-md-3 white-bg no-left-right-padding" style="border: 1px solid rgb(233, 225, 225);border-radius: 4px;">
        <div class="col-md-12 black-bg " style="border-top-right-radius:4px;border-top-left-radius: 4px;">
            <h5 class="white-font"><span class="glyphicon glyphicon-search" style="padding-right: 10px;"></span><strong>Filter Search</strong></h5>
        </div>
        <div class="col-md-12" style="padding: 22px 15px">
            <div class="col-md-3 no-left-right-padding" style="padding-top: 7px;">
                <p class="dark-grey-font">Within:</p>
            </div>
            <div class="col-md-9 no-left-right-padding">
                <select class="form-control grey-bg full-width select-drop-down-arrow-2">
                    <option>30 Miles</option>
                </select>
            </div>
        </div>
        <div class="col-md-12 no-left-right-padding grey-bg" style="padding: 11px 0px;">
            <div class="col-md-12 form-group">
                 <label for="keywords" class="dark-grey-font no-font-weight">Keywords:</label>
                <input id="keywords" type="text" class="form-control" placeholder="Search a keyword..">
            </div>
            <div class="col-md-12 checkbox">
                <label class="no-font-weight">
                    <input type="checkbox" class=""><span style="padding-left:11px;">Online Only</span>
                </label>
            </div>

        </div>
        <div class="col-md-12" style="padding:13px;">
            <button class="btn btn-primary blue-bg">Search</button>
        </div>
    </div>
        <?php foreach ($profiles as $key => $profile) { ?>


            <div class="col-sm-3 col-md-3 member-container">
                <div class="thumbnail item-lisiting-inner col-sm-12">
                    <div class="col-sm-8 col-sm-offset-2">
                        <a href="<?php echo base_url('sell/seller/').'/'.$profile->id;?>">

                            <img  class="img-circle img-responsive" src="<?php echo base_url($profile->media->file_name);?>" style="height: 135px;display: block;"></a></div>
                    <div class="col-sm-1 no-left-padding"><button class="btn"><i class="fa fa-thumbs-up"></i></button></div>
                    <div class='text_content text-center col-sm-12'>
                        <div class="seller_name">
                            <h4 ><span class="user-sm"></span> </i></small> <span class="member_name"><?php echo ucfirst($profile->fname).' '.ucfirst($profile->lname); ?></span></h4>
                        </div>

                        <div class="seller_location">
                            <h4 class="text-center"><small><span class="member_location"><?php echo $profile->city; ?>, <?php echo $profile->state; ?> </span></small></h4>
                        </div>

                        <hr> <!--horizonal line-->

                        <div class="rating_social col-sm-12 white-background no-left-right-padding">
                            <div class="col-sm-3 col-sm-offset-1 no-left-padding"><p >Ratings:</p></div>
                            <div class="col-sm-8 ">
                                <p>
                                    <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$profile->id;?>" value="<?= $profile->profile_rating ;?>" data-type="profile"/>
                                </p>

                            </div>

                        </div>
                        <div class="col-sm-12 no-left-right-padding" style="padding-bottom: 10px;">
                            <div class="col-sm-6 friends no-left-padding"><span class="fa fa-users"></span>Friends:(<?= $profile->freinds_count;?>)</div>
                            <div class="col-sm-6 no-right-padding "><span class="fa fa-shopping-cart"></span>Listings: (<?= $profile->products_count; ?>)</div>
                        </div>
                    </div> <!--end of text content -->


                </div>

                <div class="col-sm-12 no-left-right-padding">
                    <button type="submit" class="btn btn-primary chat-request-btn" ><i class="fa fa-comment"></i>SEND CHAT REQUEST</button>
                </div>

            </div>
        <?php } ?>

</div>