<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Seller Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/css/seller.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/slimbox2.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">

   
</head>

 <!--#dantheman to be moved when UI fix done -->
        
<style type="text/css">
  .alert-success h4 {
    font-size: larger;
    color: #2676AF;
  }

@media (min-width: 768px)
.col-sm-6 {
  /*width: 50%; */
}
.listing_wrapper 
{

  background-color: #F5F5F5;
  border-radius: 0px;
  border-color:#E8E8E8;
}

.listing_wrapper hr {
    border-top-color: #CCD2C6;
}


@media screen and (max-width: 768px) {
    .col-sm-6 {
  /*width: 50%; */
}

}

.product_lsiting {

    color: #2676AF;
    background-color: rgba(235, 238, 241, 0);
    border-color: #d6e9c6;
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px
}

</style>

<body>       
     <?php $this->load->view($notification_bar); ?>
<header>
      <?php $this->load->view($header_black_menu); ?>   
      <?php $this->load->view($header_logo_white); ?>
</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

   <!--load menu here -->
   <?php $this->load->view($main_menu);?>
            
             

  <div class='container container-seller-details'> 
  <?php  $uname = $this->session->userdata('username');  ?>

<div id="profile-data">
  <div id="logged-in-user-notification"><?php echo $uname; ?></div>
  <div id='profile-img-add'><?php echo base_url($profile_image);?></div>
  <div id='sender-img-add'><?php echo base_url($the_sender_image);?></div>
  <i class="close-dialog fa fa-times-circle-o"></i>
</div>



<div id="chat-request-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
       <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?> </div>
        <div id="lefties"> <h2>New Chat Request</h2></div>
    </div>
    <div class="dialog-content">
        <div id="profile_picture">
        <img src="" height ="150" class="dialog-logo"/>
        </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username"><span class="sender"></span> has sent you a Chat Request.<span class="subject"></span></p>
            <p>Do you accept this invite?</p>
            <a class="button chat confirm-receive-chat" id="accept-chat" data-status="accept" href="#">Accept</a>
            <a class="button" id="decline-chat" data-status="decline" href="#">Decline</a>
        </div>
    </div>
</div>


<div id="chat-request-sent-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
        <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
        <div id="lefties"><h2>Chat Request Sent</h2></div>
    </div>
    <div class="dialog-content">
      <div id="profile_picture">
        <img src="" height ="150" class="dialog-logo"/>
      </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username">You have sent <span></span> a chat request.</p>
            <a class="button no-button" href="#">OK</a>
        </div>
    </div>
</div>


<div id="chat-request-response-dialog" class="dialog confirmation-dialog">
    <div class="dialog-header">
       <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
       <div id="lefties"> <h2>Chat Request Rejected</h2></div>
    </div>
    <div class="dialog-content">
      <div id="profile_picture">
        <img src="" height ="150" class="dialog-logo"/>
      </div>
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username"><span></span> has declined your chat request.</p>
            <a class="button no-button" href="#">OK</a>
        </div>
    </div>
</div>


<div id="chat-confirmation-dialog" class="dialog confirmation-dialog">
 
    <div class="dialog-header">
       <div id="lefties"> <?php //echo Asset::img('user_chat.png'); ?></div>
       <div id="lefties"><h2>New Chat Request</h2></div>
    </div>
    <div class="dialog-content">
        
        <div class="right-content">
            <?php //echo Asset::img('chat_03.png'); ?>
            <p class="username">You are sending a Chat Request to <span></span>!</p>
            <p>Do you want to send now?</p> 
            <a class="button chat yes-button confirm-send-chat" href="#" data-dialog = "chat-request-sent-dialog" data-username="">Yes</a>
            <a class="button no-button" href="#">No</a>
        </div>
    </div>
</div> 

       <div class='container container-seller-details'> 

              
              <?php if($error):?>

                <div class="error-message col-md-12">  
                  <?php $this->load->view($show_error_page,$data);?> 

                </div>

              <?php endif;?>

              <?php if(!$error): ?>

               <div class="row row-details">
                    
                    <div class="col-xs-12 col-sm-12 col-md-8 detail_info pull-left">
                   
                      <div class="media">

                            <a class="media-left media-top" href="#" style="padding-bottom: 9px">
                              <img class='pull-left user-small-image' src="<?php echo base_url($profile_image);?>" height='150' alt="...">
                            </a>

                         <div class="media-body">
                              <div class='bodys'>
                               <h3 class="media-heading"><?php echo ucfirst($profile->fname) .' ' .ucfirst($profile->lname) ;?> <small>| &nbsp;<i class='glyphicon glyphicon-thumbs-up' style='color:#2676af;'></i>&nbsp;<span class='likes'>Likes <span class='likes_count'>( <?php echo $count_liked_products;?>  )</span></span></small></h3>
                                 <p><i class='glyphicon glyphicon-briefcase'></i>
                                 <span class='jobtitle'><?php echo trim(ucfirst($profile->job_title));?>r</span> | 
                                 <span class='companyname'><?php echo trim(ucfirst($profile->company_name));?></span>
                                 </p>
                                 <p class="located">Located In :&nbsp;<strong><span style='color:#5F5757;'><?php echo $profile->state ; ?></span></strong> ,
                                 <span class='location' style='color:#5F5757;'><?php echo $profile->city ; ?></span> | 
                                 <span class='country'><i class='glyphicon glyphicon-map-marker'></i>United States</span>
                                 <p><?php echo $profile->bioinfo;?></p>
								 <a href="#" class="small read-more-blue">Read more about me</a>
                                 <!--
                                 <div class="rating_left">
                                    <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$profile->id;?>" value="<?= $profile->avg_rating ;?>" data-type="profile"/>
                                 </div>
                                 
                                 <div class="">                            
                                   <div class="memeber_month">
                                       <p>
                                         <span class='memeber'>Member Since |</span>  
                                         <span class="month"><?php echo date_format(date_create($profile->created_date),'F Y');?></span>
                                       </p>
                                   </div>
                               </div> -->
                             </div>
                           </div>
                        </div>



                    </div>



                      <div class="col-xs-12 col-sm-12 col-md-4 buttons ">
                          <div class="form-inline pull-right">
                           <div class="form-groups profile-user-top-btns">
                             <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==false):  ?>

                            <?php if($this_user_has_sent_request==true && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
                             <a href="#" id='friend_request_btn' data-status="<?= ($this_user_has_sent_request==true)? 'SENT' : 'REQUEST'; ?>" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i><?= ($this_user_has_sent_request==true)? 'CANCEL FRIEND REQUEST' : 'SEND FRIEND REQUEST'; ?></a>                             
                            <?php endif;?>
                            
                            <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==true && $this_user_is_friend==false) :?>
                             <a href="#" id='friend_accept_btn' data-status="Accept" data-sender-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i>ACCEPT FRIEND REQUEST</a>                                                    
                            <?php endif;?>

                            <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
                             <a href="#" id='friend_request_btn' data-status="REQUEST" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i>SEND FRIEND REQUEST</a>                             
                            <?php endif;?>

                            <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==true): ?>
							              
                              <a href="#" class ="btn btn2 btn-default btn-sm margin-btm send-chat-request" data-confirmation-dialog ="chat-confirmation-dialog" data-username ="<?= $profile_username[0]['username'] ?>" data-full-name ="<?= $profile->fname ?>" ><i class='glyphicon glyphicon-conversation'></i> Send Chat Request </a> 
                                                            
                               <!-- <a href="#" id='' data-status="" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-danger btn-sm" style="padding-top: 0px;width: 146px;height: 28px"><i class='glyphicon glyphicon-user'></i>BlOCK FRIEND</a>                            -->
                               <a href="#" class="btn btn-default btn-sm margin-btm" data-target="#composeNew2" data-toggle="modal" aria-controls="inbox"><i class='glyphicon glyphicon-envelope'></i>Send a Message</a>

                                            <a href="#" id='' data-status="" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm btn-gray margin-btm"><i class="block-icon"></i>BLOCK FRIEND</a>
                              
                            <?php endif;?>          

                            <!-- Button trigger modal -->

                              <!-- Modal -->
                              <div class="modal fade" id="composeNew2" tabindex="-1" role="dialog" aria-labelledby="composeNew">
                                <div class="modal-dialog model-dialog-sellers" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="composeNew2"><i class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true" style='padding-right:5px;'></i> Compose a Message</h4>
                                    </div>
                                    <div class="modal-body" style="font-color:grey;font-style:italic;">
                                    <!--show success message -->
                                      <div  class="sent-message alert alert-success alert-dismissible" role="alert" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong> Your message sent successfully!
                                      </div>
                                      
                                      <form>
                                            <div class="input-group">                                         

                                               <span class="input-group-addon">
                                               <img id="memeber_image" class='' src="<?php echo base_url().'/uploads/no-photo.jpg';?>" height=80 width=80/>
                                               </span>
                                               <?php  //var_dump($catagories);
                                                        $members_list['#'] = 'Select a Memeber';
                                                            echo form_dropdown('selected_member', $members_list,'#','id="selected_member"
                                                            class="form-control" required style="color:grey;font-style:italic;"'); ?>
                                                      
                                       
                                            </div>
                                            <p></p>
                                            <div class="form-group">           
                                              <input class="form-control" id="subject" placeholder="Subject:" required></input>
                                            </div>
                                            <div class="form-group" style="display:block;margin-top:2%;">           
                                              <textarea class="form-control" rows='5' id="message_text" required placeholder="Type your message here.."></textarea>
                                            </div>
                                      </form>

                                    </div>
                                      <div class="modal-footer">
                                      <button type="button" id="send_message" name="send_message" class="btn btn-primary pull-right"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i>Send message</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                                                        
                           <?php endif; ?>


                          <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==true):?> 
                              
                              <button type="button" class="btn btn-primary btn-sm pull-left" data-toggle="modal" data-target="#inviteModal">
                                 <i class='glyphicon glyphicon-bullhorn'></i>Refer a Friend Today
                              </button>
                              
                          <!-- Refer a Friend Model -->
                              <div class="modal fade" id="inviteModal" tabindex="-1" role="dialog" aria-labelledby="inviteModalLabel" aria-hidden="true" style="top:30%;">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="myModalLabel" style="font-style:bold;">Refer a Friend to Join Madebyus4U.com!</h4>
                                      
                                    </div>
                                    <div class="modal-body col-md-12">
                                      
                                      <div class="form-inline form-group">
                                       
                                      <input type="text" class="form-control" id="recipients" style="width:400px;background-color: rgb(245, 244, 244);" value="Send multiple emails separated by comma">
                                      </div>
          
                                    </div>
                                    <div class="modal-footer">
                                     
                                      <button type="button" id='send_invites_btn' data-toinvited-id="<?= $profile->id ;?>"  class="btn btn-primary">Send Invites</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                                
                           <?php endif; ?>                      
                      
                           </div>

                          </div>
						  <div class="clearfix"> </div>
                          <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==false && $this_user_is_friend==true && in_array($profile->id,$loggedin_profile_lists) ): ?>
							<div class="profile-user-bottom-btns">
								<span class="user-online-notice">
									 This User is Online
								  </span>   &nbsp;
								  <button class="btn btn-primary btn-sm " data-toggle="modal" data-target="">
									 <i class="chat-icon"></i>START A CHAT
								  </button> 
                           <!--<a href="#" class="btn btn-default btn-start-chat "><i class='glyphicon glyphicon-comment'></i>Start Chat</a>
                               
                           <a href="#" class="btn btn-default btn-chat-status btn-sm pull-right"><i class='glyphicon glyphicon-record' style='color:#69DA32;'></i>This user is online</a> -->
						   </div>
                         <?php endif; ?>
							
                    </div>

              </div> <!--end of row -->

    <div class="row row-under-container" >
     
        <!--<div class="col-md-1">Web | <span class='webaddres'><a href='<?php echo $profile->website;?>' target="_blank"><?php echo $profile->website;?></a> </span>  </div> -->
		 <div class="col-md-1 rating_left rating_col">
			<input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$profile->id;?>" value="<?= $profile->avg_rating ;?>" data-type="profile"/>
		 </div>        
        <div class="tab-menu">
          <a href="#listing" class="selected" aria-controls="listing" role="tab" data-toggle="tab" class='tab-links'><span><?=$product_total_rows;?> Listings</a></span>
          <a href="#store" data-toggle="tab"  class='tab-links'><span><?=$store_total_rows; ?> Store</span></a>
          <a href="#likes" aria-controls="likes" role="tab" data-toggle="tab" class='tab-links'><span  ><?php echo $products_i_like_count;?> Likes </a></span>                       
          <a href="#friends" aria-controls="friends" role="tab" data-toggle="tab" class='tab-links'><span ><?=$count_friends; ?> Friends</a></span>
          <a href="#photos" aria-controls="videos" role="tab" data-toggle="tab" class='tab-links'><span ><?=$count_photos; ?> Photos</a></span>
          <a href="#videos" aria-controls="videos" role="tab" data-toggle="tab" class='tab-links'><span ><?=$count_videos; ?>  Videos</a></span>                          
        </div>

      </div>


              <div class="row">
               
                <!-- Tab panes -->
               
                <div id="sellersTabContent"  class="tab-content">


                  <div role="tabpanel" class="tab-pane " id="listing">

                            <div class="alert alert-success listing_wrapper">

                            <?php 

                              if($product_total_rows> 0) { ?>

                                <h4> Showing  (<?=$product_total_rows;?>) Listing </h4>
                                <hr class="hr_border">
                                    <?php $this->load->view($product_listing); ?>
                                <div class="col-md-12 pagination_section">
                                              <div class="pull-right">
                                                      <?php $this->load->view($paginate_page,$product_links); ?>
                                              </div>    
                                      </div>

                                  <?php } else {
                          
                                   echo "<div class='alert alert-success'> <h4> We are sorry , no products to added yet! </h4></div>";
                          
                            } ?>
                          
                                          
                          </div>


                  </div>

                  <div role="tabpanel" class="tab-pane"  id="store">

               
                         <div class="listing_wrapper alert alert-success">
                                
             
                              <h4> Showing  (<?=$store_total_rows; ?>) Listing </h4>

                              <hr class="hr_border">
                               <?php $this->load->view($store_listing_tab); ?></h3>
                                                                                                                  
                        </div>
                    
                  </div>
                <!--likes-->
                  <div role="tabpanel" class="tab-pane" id="likes">

                     <div class="listing_wrapper alert alert-success">

                            <?php 

                              if($products_i_like_count> 0) { ?>

                                <h4> Showing  (<?=$products_i_like_count;?>) Listing </h4>
                                <hr class="hr_border">
                                    <?php $this->load->view($likes_view,$liked_products); ?>
                                    

                                  <?php } else {
                          
                                   echo "<div class='alert alert-success'> <h4> We are sorry , you have not liked any products! </h4></div>";
                          
                            } ?>
                          
                                          
                          </div>

              
                  </div>
                <!--end likes-->

                   <!--friends-->
                   <div role="tabpanel" class="tab-pane" id="friends">

                      <div class="listing_wrapper alert alert-success">

                          <?php 

                          if($count_friends> 0) { ?>

                            <h4> Showing  (<?=$count_friends;?>) Listing </h4>

                           <hr class="hr_border">
                          
                            <?php $this->load->view($friends_view);?>
                         
                          <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no freinds added yet! </h4></div>";
                          
                          } ?>

                           </div>
                   </div>

                   <!--freind end--> 


                  <!--video tab-->
                    <div role="tabpanel" class="tab-pane" id="videos">
                         
                         <div class="listing_wrapper alert alert-success">
                    
                         <h4> Showing  (<?=$count_videos;?>) Listing </h4>

                           <hr class="hr_border">
                           
                              <?php $this->load->view($videos_view);?>
                   
                          </div>

                    </div>
                  <!--end of video tbas-->

                  <!--message tab -->
                  
                  <?php if($is_logged_in===TRUE): ?>

                     <?php if(count($inbox_messages?$inbox_messages:0)+count($sent_messages)+count($trashed_messages)> 0) { ?>

                       <div role="tabpanel" class="tab-pane" id="messages">
               
                            <div class="listing_wrapper alert alert-success">

                                  <h4> Showing  (<?=count($inbox_messages?$inbox_messages:0)+count($sent_messages)+count($trashed_messages);?>) Listing </h4>

                                  <hr class="hr_border">

                                    <?php $this->load->view($messages_view); ?>

                              </div>

                       <?php } else {
                          
                             echo "<div class='alert alert-success'> <h4> We are sorry , no messages to show ! </h4></div>";
                          
                          } ?>

              
                  </div>                   
                  <?php endif ?>

                  <!--end of message tab-->
                   <!--photos tab-->
                    <div role="tabpanel" class="tab-pane" id="photos">

                          
                         
                                    <div class="listing_wrapper alert alert-success">                                       
                                      <h4> Showing  (<?=$count_photos; ?>)Listing </h4>                                      <hr class="hr_border">
                                       <?php $this->load->view($photos_view); ?>                                                                  
                                    </div>

                          


                   </div>
                   <!--end of photos tab-->


                  <!--notification tab-->
                   <?php if($is_logged_in===TRUE): ?>

                   <div role="tabpanel" class="tab-pane" id="notifications">


                          <?php if( $this->current_notify_notifications_counts > 0) { ?>

                                      <div class="listing_wrapper alert alert-success">                                                                               
                                        <h4> Showing  (<?=$this->current_notify_notifications_counts; ?>) Listing </h4>
                                        <hr class="hr_border">
                                        <?php $this->load->view($notification_view,$notifications); ?>                                                           
                                     </div>

                       <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no notifications recieved ! </h4></div>";
                          
                          } ?>



                   </div>                   
                   <?php endif ?>
                    <!--end of notification tab-->
                   <?php if($is_logged_in===TRUE): ?>
                    <!--friend requests-->
                   <div role="tabpanel" class="tab-pane" id="friend_request">

                      <div class="listing_wrapper alert alert-success">
                          <?php if( $this->current_notify_notifications_counts > 0) { ?>
                            
                            <h4> Showing  (<?=$count_friend_request;?>) Listing </h4>
                            <hr class="hr_border">
                           <?php $this->load->view($friends_request_view);?>
                      </div>
                          <?php } else {
                          
                             echo "<div class='alert alert-success'><h4> We are sorry , no notifications recieved ! </h4></div>";
                          
                          } ?>



                   </div>
                   <?php endif ?>              


<?php endif ?>          

                   <!--freind request end--> 
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script src="<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/tab_selected.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/load_images_for_options.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/friend_request.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/messages.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/video-photo-page.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/photo-image-loaders.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/refer-a-friend.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/activate_link_tab_per_hash.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/ckeditor/ckeditor.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/chat_server/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/chat_server/node_modules/node-uuid/uuid.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/chat_js.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/jquery/slimbox2.js";?>"></script>

<!--
<script type="text/javascript" src="http://localhost:3000/socket.io/socket.io.js"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/notification_client.js";?>"></script>
-->
<script type="text/javascript">
  /***global base url path in javascript***/
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split('/');
  var base_url_complete = base_url+'/'+pathArray[1]+'/';
  var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ; 
  var csrf_token = "<?= $this->security->get_csrf_token_name();?>";
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

   // $('.lightbox').lightBox();

 
  /***gloabl base_url path_end***/
    
  function beepSound(filename) {
    // @param filename The name of the file WITHOUT ending
    var path="assets/sounds/"+ filename + ".mp3";
    document.getElementById('sound').innerHTML="<audio autoplay=autoplay><source src='"+path+"' type=audio/mpeg/></audio>";
  } 

  /***
   * Created by Daniel Adenew
   * Submit email subscription using ajax
   * Send email address
   * Send controller
   * Recive response
   */
  var url =  "<?php echo site_url('welcome/subscribe');?>";
  subscribe_using_ajax(url);
</script>



<script>



</script>

<script type="text/javascript">
  
  //add the id of the user to server to route notification per profile id
  $(function(){
    var profile_id = "<?php echo $this->profile_id;?>";
   // alert(profile_id);
     //socket.emit('add client',{"profile_id":profile_id });
  });

</script>


<script>
var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

         $(".rating").rating('refresh', 
            {showClear: false, showCaption:false,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });
     
          $('.rating').on('rating.change', function() {
          

           if(!readOnly) {

           var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type"); 
           var to_extract_from_id_text = null;
          
           var value =  $(this).val();
           
           var result = confirm("Are you sure you want to rate this "+ type_of_item_rated+" "+value+" ?");
         
           if(result==true) {
               
               if(type_of_item_rated=='store') {
                   to_extract_from_id_text = "rating_element-store";
               }
               if(type_of_item_rated=='product') {
                   to_extract_from_id_text = "rating_element-";
               }
               if(type_of_item_rated=='profile') {
                   to_extract_from_id_text = "rating_element-";
               }
             
           
           var static_id_text=(to_extract_from_id_text).length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = base_url_complete+'rating/rate';
           
           save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

          }
         }
           else {
            window.location.assign(base_url_complete+'users/login');
           }

        });
       



    $(function(){

          var menu = $('.tab-menu');

         // Mark the clicked item as selected

          menu.on('click', 'a', function(){
              var a = $(this);

              a.siblings().removeClass('selected');
              a.addClass('selected');
          });
      });
</script>


<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>



</body>
</html>
