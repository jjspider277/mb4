<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Become Seller!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">


</head>

<style type="text/css">

 .navbar {
        margin-bottom: 0px;
    }
    .row5 {
        padding: 2px;
        border: 2px solid #F3F3F2;
        border-top: 7px solid grey;
        border-radius: 11px;
    }
    .menu-column_submenu  {
        margin: 0;
        padding: 0;
        font-family: 'Oswald',sans-serif;
        font-weight: 300;
        font-size:medium ;

    }


    #search{
        background: #d3d3d3;
        cursor: pointer;
        font-size: 24px;
        font-weight: bold;
        text-transform: lowercase;
        padding: 20px 2%;
        width: 96%;
    }
    #search-overlay{
        background: black;
        background: rgba(255, 255, 255, 255);
        color: black;
        display: none;
        font-size: 18px;
        height: 200px;
        padding: 0px;
        margin-top:28px;
        position: absolute;
        width: 436px;
        z-index: 100;
        opacity: 0.95;
        border-radius: 4%;
        border: 2px solid #efefef;
        overflow: auto;
    }
    #display-search{
        border: none;
        color: black;
        font-size: 14px;
        margin: 5px 0 0 0;
        width: 400px;
        height: 20px;
        padding: 0 0 0 10px;
        display: none;
    }

    #hidden-search{
        left: -10000px;
        position: absolute;

    }

    #results{
        display: none;
        width: 300px;
        list-style: none;
    }
    #results ul {
        list-style:none;
        padding-left:0;
    }​
     #results ul li{
         list-style: none;
         padding-left:0;
     }

    #results ul li a{
        color:#2676af;
        font-size: 12px;
        font-weight: bold;
    }
    }
    #search-data{
        font-size: 14px;
        line-height: 20%;
        padding: 0 0 0 20px;

    }

    h2.search-data{
        margin: 10px 0 30px 0;

    }

    </style>


<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;padding-top:50px;">

    <div class="row white-bg">
        
        <div class="container white-bg " style="">

            <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">

                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <style type="text/css">
                        .main-navigation {
                            background-color: #FAFAFA;
                        }
                        .navbar-default {
                            background-color: inherit;
                            border: none;
                        }
                        .blue-font {
                            color: #2f97cc;
                        }
                        .grey-background{
                            background: #ebebeb;
                        }
                        .bottom-blue-border{
                            border-bottom: 4px solid #216da1;
                        }
                        .wrapper {
                            text-align: center;
                        }

                        .start-shopping-btn {
                            position: absolute;
                            top: 83%;
                            left: 42%;
                            font-size: 27px;
                            background: #0b69a0;
                        }

                        .black-btn{
                            background:#303030;
                        }
                        .box-height{
                            height: 251px;
                        }
                        .pagination{

                            float: right !important;
                        }
                        .pull-right{

                        }

                    </style>
                    <?php $this->load->view($column_main_menu);?>

                </div><!-- /.container-fluid -->
        </div>
    </div>



    <div class="row">
          
          <style type="text/css">
.carousel-indicators {
bottom: 36px;
}
.carousel-caption{
  bottom: 109px;
  right: 16%;
}
.carousel-caption .btn {
  background-color: #174664;
  border:none;
  opacity: 0.5;
  filter: alpha(opacity=50); /* For IE8 and earlier */
  font-weight: bold;
  
}


.carousel-caption .btn-lg{
  padding: 4px;
}
body {
  background-color: #fafafa;
}

.showing_text {
    margin-left: 0;
    padding-bottom: 10px;
}
.hr_border {
border-top: 2px dotted #818181;
margin-top: 33px;
margin-bottom: 22px;
width: 100%;
}

.hr_border_bottom {
border-top: 2px dotted #818181;
width: 100%;
}
.showing_text .showing {
font-weight: 800;
font-size: 20px;
color:#595959;
}

.showing_text .small_text{
font-size: 18px;
font-weight: normal;
}
.showing_text .showing-item {
   color:#bfbfbf;
   font-weight: 600;
}
.carousel {
position: relative;
border-bottom: 1px solid #e9e9e9;

}

          </style>
           <!-- Carousel

    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" >
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="<?php echo base_url()."assets/images/sell/carsoul_imag1.png";?>" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
             <!-- <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              -->
              <p><a class="btn btn-lg btn-primary pull-right" href="<?php echo base_url('signup');?>" role="button">Sign Up Today!!!</a></p>

            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php echo base_url()."assets/images/sell/carsoul_imag1.png";?>" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <!-- <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              -->
              <p><a class="btn btn-lg btn-primary pull-right" href="<?php echo base_url('signup');?>" role="button">Sign Up Today!!!</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php echo base_url()."assets/images/sell/carsoul_imag1.png";?>" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <!-- <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              -->
              <p><a class="btn btn-lg btn-primary pull-right" href="<?php echo base_url('signup');?>" role="button">Sign Up Today!!!</a></p>

          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


</div>
  <style type="text/css">
.container-paragraph{
   padding-bottom: 40px;
}
.text_content_paragraph {
  background-color: #ffffff;
 border:3px solid #f4f4f4;


}
.text_content_paragraph p {
  text-align: justify;
  padding: 10px;
   color: #8e8e8e;
}
.more_info {
  padding-top: 20px;
}
     </style>
<div class="container container-paragraph">

  <div class="col-md-12 showing_text">

                    <div class="more_info">

                      <span class="showing">More
                      <span class="small_text">Information</span>
                      on becoming <span class="small_text">seller</span> !</span>

                    </div>

            </div>


<hr class="hr_border" style="width:100%;">

<div class="text_content_paragraph" >
<p>
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariat
ur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
</p>
<p>
Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
</p>
<p>
"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis it
quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit lab
oriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum
 qui dolorem eum fugiat quo voluptas nulla pariatur? Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore mi
agna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in vo
luptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>
<p>
Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis it
quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius
modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit lab
oriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum
 qui dolorem eum fugiat quo voluptas nulla pariatur?  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore ia
gna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in vol
uptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</p>
</div>


</div>

</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
  <script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>">
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    $(document).ready(function() {

      
    });

       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>
