
<style type="text/css">
  
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
</style>

<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php  if(count($all_store_data)>0): ?>

<?php foreach($all_store_data as $store) : ?>


      <div class="col-sm-3 col-md-3">

            <div class="thumbnail item-lisiting-inner">
                <a href="<?php echo base_url('store/store_listing').'/'.$store->store_id ;?>">

                        <img  alt="<?php echo $store->store_name;?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $store->media->file_name ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
            <div class="caption">
                       <h4><?php echo $store->store_name;?></h4>
                       <p><?php echo $store->desc;?></p>
                       <p></p>                      
                          
                      
            
             <hr>

                  <p>
                      <span class='seller_name'><i class='glyphicon glyphicon-user'></i>                  
                      <a href="<?php echo base_url('sell/seller').'/'.$store->profile->id ;?>">
                      
                      <b style='padding-left:4px;color: #1f72ad;'><?= ucfirst($store->profile->fname).' '.ucfirst($store->profile->lname);?></b>
                      </a>
                        
                        <!--<span class='buy_btn'> 
                            <a href="<?php echo base_url('store/store_listing').'/'.$store->store_id ;?>" class="btn btn-primary" role="button">View Inside</a>
                          </span> -->
                    </span>
                         
                     <p>
                         <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-store<?=$store->id;?>" value="<?= $store->store_rating ;?>" data-type='store'/>
                          
                     </p>
                   </p>
				</div> <!-- end of caption -->
            </div>
      </div>

<?php endforeach; ?>
<?php else: ?>
    <?php if($is_logged_in===TRUE && $this->is_store_created==0): ?>
   <div class="row-storesetup row" >
                        <div id='divstoresetup' class="store-setup-col col-md-offset-1 col-md-8  center-header" style="text-align:center;display:block;">
                                    <h3>Store Setup </h3>
                                    <hr>
                                    <p>You currently have no stores to create you must verify your account.To begin the store setup process</p>
                                    <a href="<?php echo base_url('store/setup').'/'.$this->profile_id;?>" id="btnstoresetup" name="btnstoresetup" class="btn btn-primary btn-lg col-md-offset-1" style="margin-bottom:5%;"> Setup Your Store Now! </a>
                        </div>
                        </div>
    <?php endif; ?>
<?php endif; ?>

</div>
</div>