
<style type="text/css">
  
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
  .thumbnail{
      border-radius: 3px !important: ;
  }

 
</style>



<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php  if(count($all_store_data)>0): ?>

<?php foreach($all_store_data as $store) : ?>

      <div class="col-sm-3 col-md-3">

            <div class="thumbnail item-lisiting-inner col-sm-12 white-bg" style="height: 339px">
            <a href="<?php echo base_url('store/store_listing').'/'.$store->store_id ;?>">

                        <img  alt="<?php echo $store->store_name;?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $store->media->file_name ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
                 <div class="caption">
                  <div class="col-sm-10 no-left-right-padding">
                       <p class='product_name_heading'><?php echo character_limiter($store->store_name, 20);?></p>
                       <!--<p class="product_description"><?php //echo $product['desc'];?></p> -->    
                    </div>
                    <div class="col-sm-2">
                    <button id="heartBtn" class="btn heart-btn"><i class="glyphicon glyphicon-heart"></i></button>
                    </div>        
            <div class="col-sm-12 no-left-right-padding" style="height: 52px;">
                <p><?php echo character_limiter($store->sdescription, 55);?></p>

            
            </div>  
          <div class="col-sm-12 no-left-right-padding">     
                      <hr style="">
           </div>  
                   <div class="col-sm-7 no-left-right-padding">
                   <span class="user-sm"></span> <a href="<?php echo base_url('sell/seller').'/'.$store->profile->id ;?>">
                      
                      <b class="black-font"><?php $fullName = ucfirst($store->profile->fname).' '.ucfirst($store->profile->lname);?>
                                                  <?= character_limiter($fullName, 9);?></b>
                      </a>
          
                    </div>
                    <div class="col-sm-5 no-left-right-padding" style="padding-left: 10px;">
                    <p>
                         <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-store<?=$store->store_id;?>" value="<?= $store->store_rating ;?>" data-type='store'/>
                          
                     </p>
                    </div>
        
                  
              </div>          
            </div>

      </div>
<?php endforeach ;?>
<?php else: ?>

    <?php if($is_logged_in===TRUE && $this->is_store_created==0): ?>
        <div class="row-storesetup row" >
            <div class="col-md-12">
                <div class="col-md-4 no-left-padding">
                    <h3 class="black-font">Store Setup</h3>
                </div>
            </div>
            <div class="col-md-12 white-bg" style="    padding: 18px 0px 0px;">
            <div id='divstoresetup' class="store-setup-col col-md-offset-2 col-md-8  center-header white-bg" style="text-align:center;display:block;">


                <p class="black-font">You currently have no stores to create you must verify your account.To begin the store setup process</p>
                <a href="<?php echo base_url('store/setup').'/'.$this->profile_id;?>" id="btnstoresetup" name="btnstoresetup" class="btn btn-primary btn-lg blue-bg col-md-offset-1" style="margin-bottom:5%;"> Setup Your Store Now! </a>
            </div>
            </div>
        </div>
    <?php endif; ?>

<?php endif ;?>

</div>
</div>