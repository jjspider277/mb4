<div class='col-md-12 no-left-right-padding' style='max-height: 208px;;overflow-y: scroll'>
<table class='table table-striped no-left-right-padding'>
    <?php foreach ($notifications as $notification) { ?>
        <tr>
            <td style='width:170px;'>User <span class='blue-font'><a href='<?= base_url('sell/seller').'/'.$profile->id;?>'><?php echo ucfirst($profile->fname).' '.ucfirst($profile->lname);?></a></span>
                <p>Purchased a product</p>
            </td>
            <td style=''> <?= $notification->created_date;?></td>
            <td>  <a href='' class='remove-notification'  data-id='<?=$notification->id ;?>'> <i class='glyphicon glyphicon-remove' style='color:#d31317;padding-right:2px;'></i></a></td>

        </tr>
        <tr>
            <td style='width:170px;'><span class='blue-font'><a href=''>Auction Name</a></span>
                <p>Has Ended</p>
            </td>
            <td => <?= $notification->created_date;?></td>
            <td>  <a href='' class='remove-notification'  data-id='<?=$notification->id ;?>'> <i class='glyphicon glyphicon-remove' style='color:#d31317;padding-right:2px;'></i></a></td>

        </tr>
        <tr>
            <td style='width:170px;'>User <span class='blue-font'><a href='<?= base_url('sell/seller').'/'.$profile->id;?>'><?php echo ucfirst($profile->fname).' '.ucfirst($profile->lname);?></a></span>
                <p>You have a new bid</p>
            </td>
            <td > <?= $notification->created_date;?></td>
            <td>  <a href='' class='remove-notification'  data-id='<?=$notification->id ;?>'> <i class='glyphicon glyphicon-remove' style='color:#d31317;padding-right:2px;'></i></a></td>

        </tr>


    <?php } ?>
</table>
</div>
<div class='col-md-offset-4 col-md-3' ><span class='blue-font'>See All</span><span class=' fa fa-angle-down blue-font' style='padding-left:4px;'></span></div>