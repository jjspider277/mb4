<style>

    #nav{list-style:none;margin-top: 50px;
        padding: 0px;}
    #nav li {
        float: left;
        margin-right: 20px;
        font-size: 14px;
        font-weight:bold;
    }
    #nav li a{color:#333333;text-decoration:none}
    #nav li a:hover{color:#006699;text-decoration:none}
    #notification_li{position:relative}
    #notificationContainer {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 30px;
        margin-left: -170px;
        width: 400px;
        z-index: 1;
        display: none;
    }
    #notificationContainer:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 188px;
    }
    #notificationTitle {
        z-index: 1;
        font-weight: bold;
        padding: 8px;
        font-size: 13px;
        position:relative;
        background-color: #ffffff;
        width: 384px;
        border-bottom: 1px solid #dddddd;
    }
    #notificationsBody {
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
        position:relative;
    }
    #notificationFooter {
        background-color: #e9eaed;
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
    }
    #notification_count {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 77px;
        border-radius: 9px;
        position: absolute;
        margin-top: -11px;
        font-size: 11px;
    }
</style>

<div class="row" style="height: 100px;">
<ul id="nav">
<li><a href="#">Link1</a></li>
<li><a href="#">Link2</a></li>
<li><a href="#">Link3</a></li>
    <!---notification---window --->
    <li id="notification_li">
        <span id="notification_count">3</span>
        <a href="#" id="notificationLink">Notifications</a>
        <div id="notificationContainer" >
            <div id="notificationTitle">Notifications</div>
            <div id="notificationsBody" class="notifications">
            </div>
            <div id="notificationFooter"><a href="#">See All</a></div>
        </div>
    </li>
    <!---end-->
</li>
<li><a href="#">Link4</a></li>
</ul>
</div>


