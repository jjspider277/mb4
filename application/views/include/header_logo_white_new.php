<style type="text/css">
    .row2 {
        background-color: #ffffff;
    }
    .community_badges .badge{
        border-radius: 6px;
        padding: 4px 6px;
        background: #216da1;
    }

    .search-input {
        /**margin-top: 18px;*/


        border-left: 0px none;
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }

    .row2{
        margin-top: 50px;
    }


    .community_badges a:link {
        text-decoration: none ;
        color: inherit;
    }
    .glyphicon-user {
        padding-right:0px !important;
    }
    .fa-thumbs-up, .glyphicon-user,.glyphicon-envelope,.glyphicon-bell {
        color:#6f6f6f !important;
    }
    .community_badges a:visited {
        text-decoration: none;
    }

    .community_badges a:hover {
        text-decoration: none;
        color: #2676af;
    }

    .community_badges a:active {
        text-decoration: none;
        color: #2676af;
    }

    #search-header {

    }
    #search{
        background: #d3d3d3;
        cursor: pointer;
        font-size: 24px;
        font-weight: bold;
        text-transform: lowercase;
        padding: 20px 2%;
        width: 96%;
    }
    #search-overlay{
        background: black;
        background: rgba(255, 255, 255, 255);
        color: black;
        display: none;
        font-size: 18px;
        height: 200px;
        padding: 0px;
        margin: auto;
        position: absolute;
        width: 436px;
        z-index: 100;
        opacity: 0.95;
        border-radius: 4%;
        border: 2px solid #efefef;
        overflow: auto;
    }
    #display-search{
        border: none;
        color: black;
        font-size: 14px;
        margin: 5px 0 0 0;
        width: 400px;
        height: 20px;
        padding: 0 0 0 10px;
        display: none;
    }

    #hidden-search{
        left: -10000px;
        position: absolute;

    }

    #results{
        display: none;
        width: 300px;
        list-style: none;
    }
    #results ul {
        list-style:none;
        padding-left:0;
    }​
     #results ul li{
         list-style: none;
         padding-left:0;
     }

    #results ul li a{
        color:#2676af;
        font-size: 12px;
        font-weight: bold;
    }
    }
    #search-data{
        font-size: 14px;
        line-height: 20%;
        padding: 0 0 0 20px;

    }

    h2.search-data{
        margin: 10px 0 30px 0;

    }
    .product_search_btn{
        background: rgb(22, 103, 160);
        color: #ffffff;
    }
    .popover{
        left:6px !important;
        width:356px !important;
        max-width: 400px !important;
    }
    .popover-friends{
        left: 42px !important;
    }
    .popover-messages{
        left: 84px !important;
    }
    .popover-likes{
        left: 124px !important;
    }
    .arrow{
        left: 5.928% !important;
    }
    .popover-content {
        padding: 0px 0px;
    }
    .table > tbody > tr > td, .table > tfoot > tr > td {

        border-top: none;
        border-bottom: 1px solid #ebebeb;;
    }
    .table-striped > tbody > tr:nth-child(2n+1) {
        background-color: #ffffff;
    }
    .table-striped > tbody > tr:nth-child(2n) {
        background-color: #f7f7f7;
    }

</style>



<div class="row row2">

    <div class="container">
        <div class="col-sm-12">

            <!--div for notification -->
            <div class="col-sm-3">

                <?php if( ($this->is_logged_in) && ($this->is_admin!=true) ) :?>

                    <div class='community_badges col-md-12' style="margin-top: 28px;">
                        <?php if( isset($is_seller_page) && $is_seller_page==true && $loaded_profile_same_as_user==true ) :?>
                      <!--  <a href="#notifications" aria-controls="notifications-tab" role="tab" data-toggle="tab" class='community_top_bar' ><i class="glyphicon glyphicon-bell" style='background-color:'></i> <a data-toggle="tooltip" title="Notifications"><sup id="notification_badge_notification" class="badge"><?php echo  $this->current_notify_notifications_counts; ?></sup></a>-->

                            <a href="#" data-placement="bottom" tabindex="0" role="button"  data-trigger="focus"    data-toggle="notification-bell" data-html="true" data-template='<div class="popover container"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'     class='community_top_bar'

                           data-content="<?php if ($count_notifications > 0) : ?>
                                                <?php $this->load->view('include/notification_bell_popover'); ?>
                                             <?php endif ;?>">

                            <i class="glyphicon glyphicon-bell" style='background-color:'></i>
                            <a data-toggle="tooltip" title="Notifications">
                                <sup id="notification_badge_notification" class="badge">
                                    <?php echo  $this->current_notify_notifications_counts; ?></sup></a>





                            <?php else:?>
                            <a href="<?php echo base_url('sell/seller').'/'.$this->profile_id.'#notifications';?>" class='community_top_bar'><i class="glyphicon glyphicon-bell" style='background-color:'></i> <a data-toggle="tooltip" title="Notifications"><sup id="notification_badge_notification" class="badge"><?php echo  $this->current_notify_notifications_counts; ?></sup></a>
                                <?php endif;?>

                                <?php if( isset($is_seller_page) && $is_seller_page==true && $loaded_profile_same_as_user==true ) :?>
                                <!-- <a href="#friend_request" aria-controls="freinds-tab" role="tab" data-toggle="tab" class='community_top_bar'><i class="glyphicon glyphicon-user" style='background-color:'></i> <a data-toggle="tooltip" title="Friend Requests"> <sup id="frequest_badge_notification" class="badge"><?php echo $this->current_friend_notifications_counts; ?></sup></a>-->

                                <a href="#" role="button"  tabindex="0" data-trigger="focus" data-placement="bottom" data-template='<div class="popover popover-friends"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' data-toggle="friend-notification" data-html="true" class='community_top_bar'
                                   data-content="<?php if ($count_friend_request >= 0) : ?>
                                                <?php $this->load->view('include/friend_notification_popover'); ?>
                                             <?php endif ;?>">

                                    <i class="glyphicon glyphicon-user" style='background-color:'></i> <a data-toggle="tooltip" title="Friend Requests"> <sup id="frequest_badge_notification" class="badge"><?php echo $this->current_friend_notifications_counts; ?></sup></a>


                                    <?php else:?>
                                    <a href="<?php echo base_url('sell/seller').'/'.$this->profile_id.'#friend_request';?>" class='community_top_bar'><i class="glyphicon glyphicon-user" style='background-color:'></i><a data-toggle="tooltip" title="Friend Requests"> <sup id="frequest_badge_notification" class="badge"><?php echo $this->current_friend_notifications_counts; ?></sup></a>

                                        <?php endif;?>

                                        <?php if( isset($is_seller_page) && $is_seller_page==true && $loaded_profile_same_as_user==true) :?>
                                        <!--     <a href="#messages" aria-controls="messages-tab" role="tab" data-toggle="tab" class='community_top_bar'><i class="glyphicon glyphicon-envelope" style='background-color:'></i> </a>  <a data-toggle="tooltip" title="Messages"><sup id="message_badge_notification" class="badge"><?php echo $this->current_message_notifications_counts ;?></sup></a>-->
                                        <a href="#" role="button"  tabindex="0"  data-placement="bottom" data-template='<div class="popover popover-messages"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' data-toggle="message-notification" data-trigger="focus" data-html="true" class='community_top_bar'
                                           data-content="<?php if ($this->current_message_notifications_counts == 0) : ?>
                                                <?php $this->load->view('include/message_notification_popover'); ?>
                                             <?php endif ;?>">

                                            <i class="glyphicon glyphicon-envelope" style='background-color:'></i> <a data-toggle="tooltip" title="Messages"> <sup id="message_badge_notification" class="badge"><?php echo $this->current_message_notifications_counts?></sup></a>


                                            <?php else:?>
                                                <a href="<?php echo base_url('sell/seller').'/'.$this->profile_id.'#messages';?>"><i class="glyphicon glyphicon-envelope" style='background-color:'></i> </a> <sup id="message_badge_notification" class="badge"><?php echo $this->current_message_notifications_counts;?></sup>

                                            <?php endif;?>

                                            <?php if( isset($is_seller_page) && $is_seller_page==true && $loaded_profile_same_as_user==true) :?>
                                            <!-- <a href="#likes" aria-controls="likes-tab" role="tab" data-toggle="tab" class='community_top_bar'><i class="fa fa-thumbs-up" style='background-color:'></i><a data-toggle="tooltip" title="Likes"><a data-toggle="tooltip" title="Likes"><sup id="like_badge_notification" class="badge"><?php echo $this->current_profile_likes ;?></sup></a>-->
                                            <a href="#" role="button"  tabindex="0"  data-placement="bottom" data-template='<div class="popover popover-likes"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' data-toggle="likes-notification" data-trigger="focus" data-html="true" class='community_top_bar'
                                               data-content="<?php if ($this->current_message_notifications_counts == 0) : ?>
                                                <?php $this->load->view('include/likes_notification_popover'); ?>
                                             <?php endif ;?>">

                                                <i class="fa fa-thumbs-up" style='background-color:'></i> <a data-toggle="tooltip" title="Messages"> <sup id="message_badge_notification" class="badge"><?php echo $this->current_message_notifications_counts?></sup></a>


                                                <?php else:?>
                                                <a href="<?php echo base_url('sell/seller').'/'.$this->profile_id.'#likes';?>"><i class="fa fa-thumbs-up" style='background-color:'></i><a data-toggle="tooltip" title="Likes"><a data-toggle="tooltip" title="Likes"><sup id="like_badge_notification" class="badge"><?php echo $this->current_profile_likes ;?></sup></a>
                                                        <?php endif;?>
                    </div>

                <?php else :?>

                    <!-- <div class='community_badges col-md-1' style="margin-top: 28px;">
                         <div class="">
                             <small style="display:inline-block;font-weight: 500;font-size: 17px;color:#6B6B6B;">Cart/ $ 0.00</small>
                             <span class="glyphicon glyphicon-shopping-cart" style="color: grey;"></span>
                             <sup class="badge">0</sup>
                         </div>
                     </div> -->

                <?php endif ;?>

            </div>

            <!--div for logo -->
            <div class="col-sm-3 col-sm-offset-1">
                <a href="<?php echo base_url('welcome/home'); ?>"><img style="height: 85px; margin-top: -10px;" src="<?php echo base_url()."assets/images/mbu4ulogo.png";?>"/></a>
            </div>

            <!--div for search -->
            <div class="col-sm-3 col-sm-offset-2" style="padding: 24px;">
                <div id="header-search-container" url="<?php echo base_url('globalsearch/search');?>">
                    <?php

                    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'search_frm', 'name' => 'search_frm',);

                    echo form_open_multipart('globalsearch/search_products/', $attributes);
                    ?>
                    <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-btn">
                            <button class="btn btn-defaul product_search_btn" type="button"><i class="glyphicon glyphicon-search"></i></button>
                          </span><input  id="search-header" class="form-control search-input" name="search-header" placeholder="Search by product" type="text">
                            <span><a href = "#"></a></span>
                        </div>
                        <div id="search-overlay">

                            <input id="display-search"  class="form-control search-input" type="text" autocomplete="off" readonly="readonly" /> <!--mirrored input that shows the actual input value-->
                        </div>
                    </div>
                </div

                <div id="search-overlay">

                    <input id="display-search"  class="form-control search-input" type="text" autocomplete="off" readonly="readonly" placehosl/> <!--mirrored input that shows the actual input value-->
                    </form>
                    <div id="results">

                        <ul id="search-data"></ul>
                    </div>
                </div>

            </div>
            <!-- <input type="text" class="form-control search-input" placeholder='search'> -->
        </div>

    </div>


</div>


</div>
</div>
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="notification-bell"]').popover();
        $('[data-toggle="friend-notification"]').popover();
        $('[data-toggle="message-notification"]').popover();
        $('[data-toggle="likes-notification"]').popover();
    });
</script>

