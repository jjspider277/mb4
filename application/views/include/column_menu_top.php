
<style>
    .dropdown-menu>li
    {	position:relative;
        -webkit-user-select: none; /* Chrome/Safari */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* IE10+ */
        /* Rules below not implemented in browsers yet */
        -o-user-select: none;
        user-select: none;
        cursor:pointer;
    }
    .dropdown-menu .sub-menu {
        left: 100%;
        position: absolute;
        top: 0;
        display:none;
        margin-top: -1px;
        border-top-left-radius:0;
        border-bottom-left-radius:0;
        border-left-color:#fff;
        box-shadow:none;
    }
    .right-caret:after,.left-caret:after
    {	content:"";
        border-bottom: 5px solid transparent;
        border-top: 5px solid transparent;
        display: inline-block;
        height: 0;
        vertical-align: middle;
        width: 0;
        margin-left:5px;
    }
    .right-caret:after
    {	border-left: 5px solid #000000;

    }
    .left-caret:after
    {	border-right: 5px solid #000000;
    }
    .activeCategory{
        background-color:#f5f5f5;
    }

</style>


<nav class="navbar navbar-default menu-column_submenu" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">MadeByUs4U</a>
    </div>
    <!--/.navbar-header-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

            <?php
            $ci =&get_instance();
            $ci->load->model('product_model','products');
            $random_product = @(object) $ci->products->display_all_product(1,0,'object')[0];
//           / dump($random_product->image);

            $pxCounter = -6;
            $px='px';
            foreach($parent_categories as $category) {
                    //$category_stripped_name = preg_replace('/[^A-Za-z0-9\.-]/' ,'',$category->category);
                echo"<li class='dropdown' style='position:relative'>
                <a class='dropdown-toggle' id='$category->id' data-toggle='dropdown'>$category->category<span class='caret'></span></a>
                
                <ul class='dropdown-menu ' id='parent' style='height: 335px;max-height: 336px;border-radius-bottom-right: 0px;border-bottom-right-radius: 0px;left: -200px '>";

                foreach($sub_parent_categories as $sub_category){

                    if ($sub_category->parent_id == $category->id) {
                        echo "<li class='sub-parent-categories'>  
                        <a class='trigger right-caret'>$sub_category->category </a>
                        
                        <ul  class='dropdown-menu sub-menu col-md-12' style='width: 437px;margin-top: $pxCounter$px !important; padding: 4px;padding-bottom: 0px !important; overflow-hidden;'>";
                        echo"<div class='col-md-7' style='height: 330px;max-height: 330px; overflow-y: hidden;'>";

                        foreach ($all_categories as $all_category) {
                            if ($all_category->parent_id == $sub_category->id) {
                                echo "<div class='col-md-12 no-left-right-padding'><li class=''><a href=".base_url('product/browse').'/'.$all_category->id.">$all_category->category</a></li><li class='divider'></li></div>";
                            }
                        }
                        echo "</div>";



                        echo"<div class='thumbnail item-lisiting-inner col-md-5 grey-border'>
                                        <a href='$random_product->image' data-lightbox='image-1' data-title='My caption'>


                                            <img alt='jeans men' class='img-thumbnail img-responsive xpens' src='$random_product->image' style=' text-align: center; width: 100%;height: 120px;'>
                                        </a>

                                        <div class='caption col-md-12 no-left-right-padding' style='height: 33px;'>
                                            <div class='col-md-6 no-left-right-padding'>
                                                <h4 class='product_name_heading'>$random_product->name</h4>
                                            </div>
                                                <!--<p class='product_description'></p> -->
                                                
                                            <div class='col-md-6 no-left-right-padding'>
                                                        <span class='price' style='font-size:15px;'>&#36;$random_product->price</span> <br>
                                                        
                                            </div>
                                        </div>
                                         <hr>
                                        <div class='col-md-12 no-left-right-padding' style='padding: 0px 0px 2px;'>
                                            <div class='col-md-7 no-left-right-padding'>
                                                         <span class=''>
                                                   <i class='glyphicon glyphicon-user'></i>
                                                    <a href=".base_url('sell/seller').'/'.$random_product->profile_id.">
                                                        <b class='selle-name'>$random_product->seller_name</b></a>
                                                     </span>
                                            </div>
                                            <div class='col-md-4 no-left-right-padding' >
                                              <span class='buy_btn viewDetails'>
                                                <a href=".base_url('product/detail/').'/'.$random_product->product_id.'/buy'." class='btn btn-primary btn-sm' style='font-size: 12px;background-color:#3071a9 !important;' role='button'>BUY NOW</a>
                                              </span>
                                             </div>
                                         </div>
                                               
                                         
                                           
				  


                                           

                                     
                                    </div>";
                        echo" </ul>
                    </li>";
                        $pxCounter = $pxCounter-32;
                    }


                }
                $pxCounter= -6;

                echo"</ul>
              
            </li>"; } ?>



        </ul>
    </div>
    <!--/.navbar-collapse-->
</nav>


