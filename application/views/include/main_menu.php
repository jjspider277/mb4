
 <div class="row row-1">

        <div class="nav navbar navbar-default main-navigation" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                    
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="madebyus4u-mobile-responsive-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                           
                            <?php 

                             $men = array_values($this->config->item('categories')['MEN']);
                                                
                            $category='MEN';

                            array_unshift($men,ucfirst(strtolower($category)));
                            
                            ?>

                            <a href="<?=base_url('product/browse?subcategory=').$category;?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Men <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            

                            <?php foreach($men as $value) : ?>
                                          
                                    <?php if(!is_array($value)):?>
                                     <li>
                                     <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?>
                                     </li>

                                    <?php endif;?> 

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Women <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $women = array_values($this->config->item('categories')['WOMEN']);
                            $category='WOMEN';
                            array_unshift($women,ucfirst(strtolower($category)));
                            ?>

                             <?php foreach($women as $value) : ?>

                                   <?php if(!is_array($value)) :?>
                                     <li>
                                      <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>

                                   </li>
                                    <?php endif;?>

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bottoms<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                            <?php 
                            
                            $bottoms = array_values($this->config->item('categories')['BOTTOMS'] );
                            $category='BOTTOMS';
                             array_unshift($bottoms,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($bottoms as $value) : ?>
               
                                     <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>

                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dresses <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $dresses = array_values($this->config->item('categories')['DRESSES'] );
                            $category='dresses';
                            array_unshift($dresses,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($dresses as $value) : ?>
               
                                     <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">OutWear <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $outwear = array_values($this->config->item('categories')['OUTWEAR'] );
                            $category='outwear';
                            array_unshift($outwear,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($outwear as $value) : ?>
               
                                      <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>

                         <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Shoes <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">


                            <?php 
                            
                            $shoes = array_values($this->config->item('categories')['SHOES'] );
                            $category='SHOES';
                            array_unshift($shoes,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($shoes as $value) : ?>
               
                                   <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                       <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sleep<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $si = array_values($this->config->item('categories')['SI'] );
                            $category='SI';
                            array_unshift($si,ucfirst(strtolower($category)));

                            ?>


                             <?php foreach($si as $value) : ?>
                            
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>

                         <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Swim<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                           <?php 
                            
                            $sc = array_values($this->config->item('categories')['SC'] );
                            $category='SC';
                            array_unshift($sc,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($sc as $value) : ?>
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tops<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $tops = array_values($this->config->item('categories')['TOPS'] );
                            $category='TOPS';
                            array_unshift($tops,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($tops as $value) : ?>
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Art<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $art = array_values($this->config->item('categories')['ART'] );
                            $category='ART';
                            array_unshift($art,ucfirst(strtolower($category)));

                            ?>


                             <?php foreach($art as $value) : ?>
               
                                     <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Living<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $hl = array_values($this->config->item('categories')['HL'] );
                            $category='HL';
                            array_unshift($hl,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($hl as $value) : ?>
               
                                      <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                           <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mobile<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $ma = array_values($this->config->item('categories')['MA'] );
                            $category='MA';
                            array_unshift($ma,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($ma as $value) : ?>
               
                                      <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                           <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Jewelry<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $jewelry = array_values($this->config->item('categories')['Jewelry'] );
                            $category='Jewelry';
                            array_unshift($jewelry,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($jewelry as $value) : ?>
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>

                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Kids<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">


                            <?php 
                            
                            $kids = array_values($this->config->item('categories')['Kids'] );
                            $category='Kids';
                            array_unshift($kids,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($kids as $value) : ?>
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                              <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Vintage<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            <?php 
                            
                            $vintage = array_values($this->config->item('categories')['Vintage'] );
                            $category='Vintage';
                            array_unshift($vintage,ucfirst(strtolower($category)));

                            ?>


                             <?php foreach($vintage as $value) : ?>
               
                                     <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                               <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Toys<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                             <?php 
                            
                            $toys = array_values($this->config->item('categories')['Toys'] );
                            $category='Toys';
                            array_unshift($toys,ucfirst(strtolower($category)));

                            ?>

                             <?php foreach($toys as $value) : ?>
               
                                    <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                               <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Crafts<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                          <?php 
                            
                            $craftsupplies = array_values($this->config->item('categories')['Craft Supplies'] );
                            $category='Craft Supplies';
                            array_unshift($craftsupplies,ucfirst(strtolower($category)));

                            ?>
                             <?php foreach($craftsupplies as $value) : ?>
               
                                   <li>
                                       <?= anchor('product/browse?category='.strtolower($category).'&'.'subcategory='.strtolower($value), $value) ;?></li>
                                     </li>
              

                             <?php endforeach;?>                     
                               
                               
                            </ul>
                        </li>
                       
                       
                  
                      
                      
                       
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </div>

  </div>