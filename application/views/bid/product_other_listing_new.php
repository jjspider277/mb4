<div class="col-sm-12 col-give-margin-top" style="padding: 0px 14px;">
    <p class='listedby'>
        <strong>OtherListing By This Seller: <i class='caret blue-font'></i>
        </strong>


    </p>
</div>
<div class="col-md-12">

    <?php if (count($all_seller_products) > 0){?>

    <?php foreach($all_seller_products as $other_listing) { ?>
        

    <div class="col-md-6 no-left-padding " style="padding: 5px;">
        <div class="col-md-12 no-left-right-padding" style="border: 1px solid rgb(233, 228, 228);border-radius: 4px;">
            <div class="col-md-12 thumbnail">
                <img  style="height:164px;" src="<?php echo  $other_listing['image'];?>">
            </div>
            <div class="col-md-12" style="padding: 0px 4px;"><p class="blue-font no-margin"><strong><?php echo $other_listing['name'];?></strong></p></div>
            <div class="col-md-12" style="padding: 0px 4px;">
                <div class="col-md-6 no-left-right-padding">
                    <h4><strong>$<?php echo $other_listing['price'];?></strong></h4>
                </div>
                <div class="col-md-6 other-listing-rating" style="padding: 4px 13px;">
                    <input class="rating pull-right" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?php echo $other_listing['product_id'];?>" value="<?php echo $other_listing['product_rating'] ;?>" data-type="product"/>

                </div>
            </div>

        </div>

    </div>

    <?php } ?>
    <?php } else echo "<div class='col-md-12 alert alert-danger'>User has no other listings</div>"; ?>

</div>
    <div class="col-md-4 col-md-offset-4" style="padding: 11px 16px;"><p class="underline">View More  <i class="caret" style="color:#11619a;font-size:20px;"></i></p></div>

</div>
<style type="text/css">

    .thumbnail {
        padding: 0px;
        margin-bottom: 15px;
        border: none;
        border-radius: 0px;
        background-color: #f5f5f5;
    }
    @media (min-width: 992px){
        .col-other-listing .col-md-6 {width: 48%;}
    }
    .description {
        padding-top: 4px;
        font-size: 15px;
        font-weight: 500;
        color: #226da0;
        text-align: left;
    }
    .price {
        margin-top: 0px;
        font-size: 19px;
        font-weight: 500;
        color: #111111;
        text-align: left;
    }
    div.other-listing-rating > div > div.rating-container {
        font-size: 13px !important;
    }

</style>

