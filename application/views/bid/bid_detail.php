<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Welcome to MadebyUs4u.com | Bid Details </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/product.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">

</head>
<style type="text/css">
  div.sub-comment-popup{
  
  position: absolute;
  width: 550px;
  background-color: #ffffff;
  border: 9px solid #ffffff;
  border-radius: 5px;
  color: #3c3c3c;
  padding: 10px 0;
  margin-top: 1000px;
  margin-left: 400px;
 }
 .blue{
        color: #2676af;
    }

 .auction_detail > div{
    min-height: 30px;
    margin-top: 10px;
 }
 .auction_detail .star-rating{
  float:left !important;
  margin-right: 10px;
  margin-bottom: 30px;
 }

 div.overlay {
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.7);
  width: 100%;
  height: 100%;
  min-width: 1160px;
}
.close-buttons{
position:absolute;
right:-30px;
top:-25px;
cursor:pointer;
}

</style>

<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>
     <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">
<?php $this->load->view($main_menu); ?>
  
<div class="container">


    <div class="col-md-7" style="">
    
      <div class="col-md-9 col-md-large-image" style="">
        <div class="thumbnail rec thmbnail-large">

            <?php 


            $image_found = isset($product->image)?true:false ;
            $image_many = is_array($product->image);

            ?>

             <img class="img-thumbnail-large-0 img-responsive" src="<?php echo $product->image[0];?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height:450px">       
            <img class="img-thumbnail-large-1 img-responsive" src="<?php echo  !empty($product->image[1])? $product->image[1]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height:450px"> 
            <img class="img-thumbnail-large-2 img-responsive" src="<?php echo  !empty($product->image[2])? $product->image[2]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true" width="100%" style="margin-left:0px;height:450px">
            <img class="img-thumbnail-large-3 img-responsive" src="<?php echo  !empty($product->image[3])? $product->image[3]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true" width="100%" style="margin-left:0px;height:450px">
              <img class="img-thumbnail-large-4 img-responsive" src="<?php echo  !empty($product->image[4])? $product->image[4]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true" width="100%" style="margin-left:0px;height:450px">
        
        
        </div>
      </div>

     <div class="col-md-3 col-md-small-images">
       <img class="img-thumbnail-small-1 img-thumbnail img-responsive small-images" src="<?php echo !empty($product->image[1])?$product->image[1]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 90px;width:100%;"> 
       <img class="img-thumbnail-small-2 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[2])? $product->image[2]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 90px;width:100%;">       
       <img class="img-thumbnail-small-3 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[3])? $product->image[3]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 90px;width:100%;">   
       <img class="img-thumbnail-small-4 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[4])? $product->image[4]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 90px;width:100%;">   
     <img class="img-thumbnail-small-0 img-thumbnail img-responsive small-images" src="<?php echo $product->image[0];?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height:90px">       
           
    </div>

    </div>

    <div class="col-md-5">
          
          <h3><?php echo $product->product_name;?></h3>
            <p style="margin-top:-2px;margin-bottom:4px;">
            <span class='seller_name'> 
            <i class='glyphicon glyphicon-user'>
            <b style='padding-left:4px;color: #3C4144;;'>By <?php echo $product->profile->seller_name;?></b>
            </i>
            </span>
           
            <input class="rating" data-stars="5" data-step="1" data-size="sm" id="rating_element-<?=$product->profile->id;?>" value="<?= $product->profile->profile_rating ;?>" data-type="profile" />
                         
                                        
        </p>
        <hr class="hr_border">

        <p class="text-justify" style="font-family:"> <?php echo $product->pdescription;?>  </p>
        <hr class="hr_line">
      
      <?php if($is_bid_page):?>
        <div class="bid_details">
          <div class="row">
            <div class="col-md-6">
              <strong>Current Bid Amount</strong>
            </div>
            <div class="col-md-6 text-right">
              <strong>$ <?php echo empty($auctions->current_bid)? $auctions->reserve_price : $auctions->current_bid ?></strong>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <strong>Minimum Bid Amount</strong>
            </div>
            <div class="col-md-6 text-right">
              <strong><small>$ <?php echo $auctions->reserve_price ?></small></strong>
            </div>
          </div>
          <hr>
          <div class="row" >
            <div style="background-color: #e8ebed;
            padding: 10px 0;overflow:hidden;margin: 0 15px">
              <div class="col-md-6">
                <strong>Your Bid Amount:</strong>
              </div>
              <div class="col-md-6">
              <?php echo form_open("bid/confirm_bid/".$auctions->id, array('id' => 'bid_form'));?>
              <form action="<?=site_url('bid/confirm_bid');?>" method="post" id="bid_form">
                <input type="text" class="form-control" name="price" id="price" 
                style="width: 100%;border:1px solid #ccc;border-radius: 2px;
                margin-right:-10px" 
                >
                </form>
              </div>
            </div>
          </div>
          <hr style="border-color: transparent" />
          <div class="row">
            <div class="col-md-8">
              <strong>Remaining Bid Time: 
              <?php echo ($diff->d > 1)? $diff->d.' Days,' : $diff->d . ' Day,' ?> 
              <?php echo ($diff->h > 1)? $diff->h.' Hours' : $diff->h . ' Hour,' ?>
              <?php if($diff->h === 0): ?>
                  <?php echo  ($diff->i > 1)? $diff->i . ' Minutes' : $diff->i .' Minute'; ?>
              <?php endif;?>

              </strong>
            </div>
            <div class="col-md-4 text-right">
              <button class="btn btn-primary pull-left btn-lg btn-bid" id="submit_bid" 
              style="border-radius: 0px; padding:5px 20px;
               vertical-align:middle" 
              >
                Submit Bid
              </button>
            </div>
          </div>
        </div>
         <!--  <strong style="color:#515151;">
          <strong>Latest Bid:</strong> 
          <span class="">$<?php echo $product->sprice;?>         
          <span class="timeleft"><strong style="color: #a8a8a8;">Time Left:</strong>
          <span class="left_text">00:04:14</span> </span> 
          </strong> -->
       <?php else:?>
          <strong style="color:#515151;">
          <strong>Price:</strong> 
          <span class="">$ <?php echo $product->sprice;?>
          <small style="text-decoration: line-through;"><?php echo $product->price;?></small>
          <small style="text-left">You save $<?= floatval($product->price) - floatval($product->sprice) ."!" ;?></small></span>
         
          </strong>
      <?php endif;?>
        

      <?php if($is_buy_page):?>                 
         <hr class="hr_line">  
         <strong style='color:#515151;'>Quantity:</strong> 
               
            <div class="center" >
          
                   <div class="input-group col-md-4">
                  
                       <span class="input-group-btn">
                          <button type="button" id="decrement_button" class="btn btn-default btn-number btn-sm" data-type="minus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-minus"></span>
                          </button>
                       </span>

            <input type="text" id="p_quantity" name="p_quantity" class="form-control input-number input-sm" value="1" min="1" max="10">
                    
              <span class="input-group-btn">
              <button type="button" id="increment_button" class="btn btn-default btn-number btn-sm" data-type="plus" data-field="quant[1]">
                <span class="glyphicon glyphicon-plus"></span>
              </button>
              </span>
              
                  </div>
              </div>       

            <?php endif;?>     
     
          <!-- <hr class="hr_line"> -->
          

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div style="float:left;">
        <?php //$this->load->view($social_sharing_button) ;?>
        </div>
    
    </div>

    <?php

    $this->db->select('quantity');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("order_detail");
    $product_order = $query->result_array();

    $quantity = 0;

    if(count($product_order) != 0){
       foreach($product_order as $order){
           $quantity+=$order['quantity'];

        }
     }

    $this->db->select('*');
    $this->db->where("like_product_id", $product->product_id);
    $query = $this->db->get("likes");
    $product_order = $query->result_array();
    
    $no_of_recommendation = count($product_order);

    $this->db->select('*');
    $query = $this->db->get("users");
    $total_users = $query->result_array();

    $recommendation_in_percent = ($no_of_recommendation/count($total_users))*100;

    $this->db->select('rating');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("ratings");
    $product_ratings = $query->result_array();

    $no_of_ratings = count( $product_ratings);
    $total_ratings = 0;
    $average_rating = 0;

    if(count($product_ratings) != 0){
       foreach($product_ratings as $ratings){
           $total_ratings+=$ratings['rating'];

        }
        $average_rating = ($total_ratings/$no_of_ratings);
     }



     ?>
  
  <div class="col-md-12 col-md-12-2" style="margin-top:3%;border:1px solid #eaeaea;background-color:white;padding:8px;">
  <div class="col-md-4">
       <div class="col-sm-6">
        <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon01.png";?>">&nbsp;<?php echo $quantity; ?>+</p>
        </div>
        <div class="col-sm-6"><span class="short_text">Customers bought
        this product</span>
      </div>
  </div>
  <div class="col-md-4">

    <div class="col-sm-6">
          <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon02.png";?>">&nbsp;<?php echo round($recommendation_in_percent); ?>%</p>
          </div>
          <div class="col-sm-6"><span class="short_text">people Recommend
          this product</span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="col-sm-6">
          <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon03.png";?>">&nbsp;<?php print round($average_rating, 1);?>/5</p>
        </div>
          <div class="col-sm-6"><span class="short_text">Is average rating
        for this product</span>
        </div>

    </div>
</div>

<div class="col-md-12 auction_detail" style="margin-top:3%;border:1px solid #eaeaea;background-color:white;padding:10px 15px">
    <h2>Auction Details</h2><hr class="hr_border">
    <div class="col-md-6">
      <strong>Starting Bid Amount:</strong> 
      <span class="blue"><?php echo $auctions->bid_price ?></span>
    </div>
    <div class="col-md-6">
      <strong>Status:</strong>
      <button class="btn btn-<?php echo ($auctions->status)? 'success' : 'danger' ?>">
      <?php echo ($auctions->status)? 'Active' : 'Not Active' ?>
      </button>
    </div>
    <div class="col-md-12">
      <strong>Action Ends:</strong>
      <span class="blue">
      <?php $end_date = date_create($auctions->end_date); 
            echo date_format($end_date,"l, F d, Y h:i:s A") ?>
            </span>
    </div>
    <div class="col-md-12">
      <strong>Bid History:</strong>
      <span class="blue"><?php echo ($bids === false ? '0':  count($bids)) ?></span>
    </div>
    <div class="col-md-12">
      <strong>Action Started:</strong>
      <span class="blue">
        <?php $start_date = date_create($auctions->start_date); 
            echo date_format($start_date,"l, F d, Y h:i:s A") ?>
      </span>
    </div>
    <div class="col-md-6">
      <strong>Listed By: </strong>
      <span class="blue">
        <?php echo $product->profile->seller_name; ?>
      </span>
    </div>
    <div class="col-md-6">
      <span class="blue" >
        <input class="rating" data-stars="5" data-step="1" data-size="sm" id="rating_element-<?=$product->profile->id;?>" 
        value="<?= $product->profile->profile_rating ;?>" data-type="profile" />
        <?php echo $product->profile->city. ', '. $product->profile->state . ' United States';?>
      </span>
    </div>
    <hr class="hr_line" style="margin-top: 30px" />
    <div class="col-md-10">
      <p>Other Listings By <i class="small glyphicon glyphicon-play"></i>
      <a href="<?php echo site_url('sell/seller/'.$product->profile->id) ?>" class="blue"><?php echo $product->profile->seller_name; ?></a>
      </p>
      <?php if(count($seller_products)>0): ?>

    <?php foreach($seller_products as $prd): ?>

      <div class="col-sm-3 col-md-3">

            <div class="thumbnail">
                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $prd['product_id'],'buy')): site_url(array('product', 'detail', $prd['product_id'],'bid')) ;?>">

                        <img  alt="<?php echo $prd['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $prd['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
                 <div class="caption">
                       <h4 class='product_name_heading blue'><?php echo $prd['name'];?></h4>
                        <p>
                          <span class='price'>$<?php echo $prd['price'];?></span>
                          
                      </p>
                      <hr>

                  
              </div>          
            </div>

      </div>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products found!</h4></div>
<?php endif ;?>
    </div>
    <div class="col-md-2">
      <a href="<?php echo site_url('sell/seller/'.$product->profile->id) ?>" class="btn btn-primary">View Sellers Profile</a>
    </div>

</div>

<!--main containet-->
    
        
   

</div>






</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/likes.js";?>"></script>


<script type="text/javascript">
//globals
var server_url = "<?php echo base_url('rating/rate');?>";
var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;  
var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
</script>

<script>
jQuery(document).ready(function () {

$(".sub-comment-popup").hide();

// $(".comment-reply a").click(function(){
  $(document).on('click',".comment-reply a",function(event){
      //console.log("are we here");
      event.preventDefault();
      var comment_id = $(this).data("comment-id");
      //console.log("parent");
      //console.log(comment_id);
      $(".sub-comment-popup").each(function(){
          var c_id = $(this).data("comment-id");
          //console.log("child");
          console.log("parent"+comment_id);
          console.log("child"+c_id);

          if(c_id === comment_id){
            
            console.log("this is the one");
            $("body").append("<div class='overlay'></div>");
            $(".overlay").height($('body').height());
            $(".overlay").css({
            'z-index': '3'
            })
            $(".overlay").append($(this));

          }else{
            console.log("this is not the one");
          }

      });
      $("#more-comment-"+ comment_id).show();

});

$(".close-buttons").click(function(){
      var comment_id = $(this).data("comment-id");

      $(".sub-comment-popup").each(function(){
          var c_id = $(this).data("comment-id");
          console.log(c_id);

          if(c_id === comment_id){
            $(this).css('display','none');          
            $(".comment_body").append($(this));
             $(".overlay").detach();

          }else{
            console.log("this is not the one");
          }

      });

       
});

$("#like-thumb a").click(function(){
    var comment_id = $(this).data("comment-id");
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split( '/' );
    var base_url_complete =base_url+'/'+pathArray[1]+'/';
    $url = base_url_complete+'product/comment_like';
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

    console.log(comment_id);
    console.log("i am here");


    $.ajax({
    url: $url,
    data: ({'madebyus4u_csrf_test_name':csrf_hash,'comment_id': comment_id}),
    dataType:'json', 
    type: "post",
    success: function(data) {
    if(data.success) {
    if(data.past_like){
    var new_like = data.like;
    var the_link = "#like-link-"+comment_id;
    var the_link_text = "Liked";

    var the_span = "#count-of-like-"+comment_id;
    var the_like = "("+ new_like +")";

    console.log(the_link_text);
    $(the_link).text(the_link_text);
    $(the_span).html(the_like);

    }else{

    console.log("the like is now");
    var new_like = data.like;
    var the_link = "#like-link-"+comment_id;
    var the_link_text = "Liked";

    var the_span = "#count-of-like-"+comment_id;
    var the_like = "("+ new_like +")";

    $(the_link).text(the_link_text);
    $(the_span).html(the_like);
    }

    }

    }             

});


});
       
$(".rating").rating('refresh', 
    {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
  });


$('.rating').on('rating.change', function() {

if(!readOnly) {
 var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");      
 var value =  $(this).val();
 var static_id_text=("rating_element-").length;       
 var profile_id =  ((this).id).slice(static_id_text);
 var rated = $(this).val();             
 save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

}
 else {
  window.location.assign("<?=site_url('users/login');?>");
}
  });
});



</script>

<script type="text/javascript">

    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Receive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
</script>

<script type="text/javascript">

$(document).ready(function() {

  $("#submit_bid").click(function(e){
      var current_bid = Number("<?php echo empty($auctions->current_bid)? $auctions->reserve_price : $auctions->current_bid ?>");
      var bid_price = $("#price").val();
      if(bid_price <= current_bid){
        alert("Bid must be greater than current bid price.");
        return false;
      }

      $("#bid_form").submit();

  });
    
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>"
    $('.img-thumbnail-large-0').show();
    $('.img-thumbnail-large-1,.img-thumbnail-large-2,.img-thumbnail-large-3,.img-thumbnail-large-4').hide();
  $(document).on('click','.img-thumbnail-small-1',function(){
    $('.img-thumbnail-large-0,.img-thumbnail-large-2,.img-thumbnail-large-3,.img-thumbnail-large-4').hide();
    $('.img-thumbnail-large-1').show();
     
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-2',function(){
    $('.img-thumbnail-large-0,.img-thumbnail-large-1,.img-thumbnail-large-3,.img-thumbnail-large-4').hide();
      $('.img-thumbnail-large-2').show();
     
      return false;          
  });
  
  $(document).on('click','.img-thumbnail-small-3',function(){
    $('.img-thumbnail-large-0,.img-thumbnail-large-1,.img-thumbnail-large-2,.img-thumbnail-large-4').hide();
    $('.img-thumbnail-large-3').show();
        
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-4',function(){
    $('.img-thumbnail-large-0,.img-thumbnail-large-1,.img-thumbnail-large-2,.img-thumbnail-large-3').hide();
      $('.img-thumbnail-large-4').show();
     
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-0',function(){
    $('.img-thumbnail-large-4,.img-thumbnail-large-1,.img-thumbnail-large-2,.img-thumbnail-large-3').hide();
      $('.img-thumbnail-large-0').show();
     
     return false;          
  });

  // Quantity buttons add and decerase
    $("#increment_button").click(function(){
      //alert("Increase quantity.");
      var qty = parseInt($("#p_quantity").val(),10);
      //alert(qty);
      if (qty < 10){
        newqty = qty + 1;
        $("#p_quantity").val(newqty.toString());
        $("#product_quantity").val(newqty.toString());
        //alert($("#product_quantity").val());
      }
    });
    
    $("#decrement_button").click(function(){
      //alert("Increase quantity.");
      var qty = parseInt($("#p_quantity").val(),10);
      //alert(qty);
      if (qty > 1){
        newqty = qty - 1;
        $("#p_quantity").val(newqty.toString());
        $("#product_quantity").val(newqty.toString());
        //alert($("#product_quantity").val());
      }
    });
   
});//end of document ready

</script>

<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});
$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});
$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>csr