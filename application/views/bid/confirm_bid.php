<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Confirm Bid</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/login.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <style type="text/css">
    .hr_border{
          border-top: 2px dotted #818181;
  width: 100%;
  margin-top: 0px;
  margin-bottom: 4px;
    }
    .blue{
        color: #2676af;
    }
    </style>
</head>

<body>
<header>
<?php $this->load->view($header_black_menu);?>
<?php $this->load->view($header_logo_white); ?>


</header>

<section>

<div class="row" style="margin-top:50px;">
    <div class="col-md-10 col-md-offset-1 grey-border" style="background-color:#fff;padding:20px 10px;margin-bottom: 20px;">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">

                <div class="col-md-10 blue-border" style="padding-bottom: 20px;border-radius: 6px;">
                    <h3 class="blue-font"><?php echo $product->product_name;?></h3>
                    <hr class="" />
                    <p>
                        <?php echo $product->pdescription;?>
                    </p>
                    <div class="col-md-4">
                        <img class="img-thumbnail-large-4 img-responsive"
                             src="<?php echo  !empty($product->image[4])?
                                 $product->image[0]:base_url().'/uploads/no-photo.jpg';?>" >

                    </div>
                    <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="light-green-bg grey-border" style="padding: 10px 0;overflow:hidden;margin: 10px 0">
                            <div class="col-md-12 text-center">
                                <h4>Confirm Your Bid Amount of: <span class="blue">$ <?php echo $price ?></span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="grey-border grey-bg" style="padding: 10px 0;overflow:hidden;margin: 10px 0">
                            <div class="col-md-12 text-center">
                                <strong>
                                    <?php $end_date = date_create($auctions->end_date);
                                    echo date_format($end_date,"l, F d, Y h:i:s A") ?>
                                </strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 10px;">

                        <?php echo form_open('bid/confirm_bid/'.$auctions->id, array('id' =>'confirm_form'))?>
                        <input type="hidden" name="price" value="<?php echo $price;?>">
                        <button name="confirm" value="true"<?php if(!$this->is_logged_in) echo 'disabled';?> class="btn btn-primary full-width" id="confirm">Confirm Your Bid</button>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <a href="<?php echo site_url('bid/product/'.$auctions->id.'bid');?>" class="btn btn-danger full-width" id="cancel">Cancel</a>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
            <div class="row">


            </div>

        </div>
        <?php if(! $this->is_logged_in):?>
            <div class="col-md-4">
                <?php $this->load->view($signin_form,$data); ?>
            </div>
        <?php endif;?>


    </div>
</div>

</section>

<footer class="footer">

    <?php 

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    
    ?>

</footer>

<!-- Bootstrap and Jquery and Other JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    $(document).ready(function() {
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);

       $('#confirm').click(function(e){
            $("#confirm_form").submit();
       });
    });

</script>

<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>

</body>
</html>
