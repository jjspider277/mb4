<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Welcome to MadebyUs4u.com | Bid Details </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/product.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">


</head>
<style type="text/css">
    div.sub-comment-popup{

        position: absolute;
        width: 550px;
        background-color: #ffffff;
        border: 9px solid #ffffff;
        border-radius: 5px;
        color: #3c3c3c;
        padding: 10px 0;
        margin-top: 1000px;
        margin-left: 400px;
    }
    .blue{
        color: #2676af;
    }

    .auction_detail > div{
        min-height: 30px;
        margin-top: 10px;
    }
    .auction_detail .star-rating{
        float:left !important;
        margin-right: 10px;
        margin-bottom: 30px;
    }

    div.overlay {
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        min-width: 1160px;
    }
    .close-buttons{
        position:absolute;
        right:-30px;
        top:-25px;
        cursor:pointer;
    }

    div.star-rating > div.clear-rating{
        display: none;
    }
    div.other-listing-rating > div.star-rating > div.caption{
        display: none;
    }
    div.other-listing-rating > div > div.rating-container {
        top:-6px;
    } .navbar {
          margin-bottom: 0px;
      }
    .row5 {
        padding: 2px;
        border: 2px solid #F3F3F2;
        border-top: 7px solid grey;
        border-radius: 11px;
    }
    .menu-column_submenu  {
        margin: 0;
        padding: 0;
        font-family: 'Oswald',sans-serif;
        font-weight: 300;
        font-size:medium ;

    }

  
    #search{
        background: #d3d3d3;
        cursor: pointer;
        font-size: 24px;
        font-weight: bold;
        text-transform: lowercase;
        padding: 20px 2%;
        width: 96%;
    }
    #search-overlay{
        background: black;
        background: rgba(255, 255, 255, 255);
        color: black;
        display: none;
        font-size: 18px;
        height: 200px;
        padding: 0px;
        margin-top:28px;
        position: absolute;
        width: 436px;
        z-index: 100;
        opacity: 0.95;
        border-radius: 4%;
        border: 2px solid #efefef;
        overflow: auto;
    }
    #display-search{
        border: none;
        color: black;
        font-size: 14px;
        margin: 5px 0 0 0;
        width: 400px;
        height: 20px;
        padding: 0 0 0 10px;
        display: none;
    }

    #hidden-search{
        left: -10000px;
        position: absolute;

    }

    #results{
        display: none;
        width: 300px;
        list-style: none;
    }
    #results ul {
        list-style:none;
        padding-left:0;
    }​
     #results ul li{
         list-style: none;
         padding-left:0;
     }

    #results ul li a{
        color:#2676af;
        font-size: 12px;
        font-weight: bold;
    }
    }
    #search-data{
        font-size: 14px;
        line-height: 20%;
        padding: 0 0 0 20px;

    }

    h2.search-data{
        margin: 10px 0 30px 0;

    }
    


</style>

<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>
    <?php $this->load->view($header_logo_white); ?>

</header>
<!-- Responsive design
================================================== -->
<section id="responsive" >
    <div class="row white-bg">
        <hr class="" style="margin: 0px;">
        <div class="container white-bg " style="">

            <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">

                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <style type="text/css">
                        .main-navigation {
                            background-color: #FAFAFA;
                        }
                        .navbar-default {
                            background-color: inherit;
                            border: none;
                        }
                        .blue-font {
                            color: #2f97cc;
                        }
                        .grey-background{
                            background: #ebebeb;
                        }
                        .bottom-blue-border{
                            border-bottom: 4px solid #216da1;
                        }
                        .wrapper {
                            text-align: center;
                        }

                        .start-shopping-btn {
                            position: absolute;
                            top: 83%;
                            left: 42%;
                            font-size: 27px;
                            background: #0b69a0;
                        }

                        .black-btn{
                            background:#303030;
                        }
                        .box-height{
                            height: 251px;
                        }
                        .pagination{

                            float: right !important;
                        }
                        .pull-right{

                        }

                    </style>
                    <?php $this->load->view($column_main_menu);?>

                </div><!-- /.container-fluid -->
        </div>

    </div>
    <style>
        p.other-bids-rating >div.rating-xs{
            font-size:17px;
        }
        p.bid-detail-rating>div.rating-xs{
            font-size:20px;
        }
    </style>
    <div class="container">
        <div class='col-md-12 col-sm-6 white-bg' style="padding-top: 10px;top: 44px;">
            <div class="col-md-7 no-left-padding" style="">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php


                        // $image_found = isset($product->image)?true:false ;
                        // = is_array($product->image);

                        ?>
                        <img class="img-thumbnail img-thumbnail-large-0 img-responsive" src="<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height: 420px;;">



                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" id="prev_button" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" id="next_button" href="#carousel-example-generic" role="button" data-slide="next" style="right:4px;">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <div class="col-md-12 col-md-large-image no-left-padding" style="height:0px !important;">


                    <?php


                    $image_found = isset($product->image)?true:false ;
                    $image_many = is_array($product->image);

                    ?>



                    <div class="col-md-12 col-md-small-images no-left-right-padding" style="padding-top: 14px;">
                        <div class="col-md-2 no-left-right-padding" style="width:16.66666667% !important;">
                            <button class="btn btn-sm transparent-bg" style="border:1px solid #c2c2c2;"><i class="glyphicon glyphicon-search "></i> </button><span style="padding-left:3px;">Zoom</span>
                        </div>
                        <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                            <img class="img-thumbnail-small-1 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo !empty($product->image[1])?$product->image[1]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                        </div>
                        <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                            <img class="img-thumbnail-small-2 img-thumbnail img-responsive small-images dark-blue-border"src="<?php echo  !empty($product->image[2])? $product->image[2]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                        </div>
                        <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                            <img class="img-thumbnail-small-3 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo  !empty($product->image[3])? $product->image[3]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                        </div>
                        <div class="col-md-2 no-right-padding" style="width:16.66666667% !important;">
                            <img class="img-thumbnail-small-4 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo  !empty($product->image[4])? $product->image[4]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                        </div>
                        <div class="col-md-2 no-right-padding" style="width:16.66666667% !important;">
                            <img class="img-thumbnail-small-0 img-thumbnail img-responsive small-images" src="<?php echo $product->image[0];?>" data-holder-rendered="true"  width="100%" style="height:113px;width:100%;display: none;">

                        </div>

                    </div>
                </div>



            </div>
            <div class="col-md-1" style="width:3.33%;"></div>
            <div class="col-md-4  blue-border" style="padding-bottom: 2%;border-radius: 5px;width: 38.333333%;">
                <h3 class="blue-font col-md-12"><?php echo $product->product_name;?></h3>
                <div class="col-md-12">
                    <p class="bid-detail-rating">
                        <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?php echo $product->product_id;?>" value="<?php echo  $product->p_avg_rating ;?>" data-type="product" />
                    </p>
                    <hr>
                </div>
                <div class="col-md-12">
                    <small class="grey-font">Description</small>
                    <p class="black-font"><?php echo $product->pdescription;?></p>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12 light-green-bg grey-border border-radius">
                        <div class="col-md-6 no-left-right-padding" style="padding-top: 14px;">
                            <p class="no-margin black-font pull-right" style="font-size: 17px;">Current Bid Amount:</p>

                        </div>
                        <div class="col-md-6 no-left-right-padding">
                            <span class="grey-font pull-left" style="font-size:34px;">&nbsp$</span><span style="font-size:34px;">&nbsp<?php echo empty($auctions->current_bid)? $auctions->reserve_price : $auctions->current_bid ?></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 no-left-right-padding" style="padding-top: 11px;">
                            <h5 class="no-margin black-font pull-right">Minimum Bid Amount:</h5>

                        </div>
                        <div class="col-md-6 no-left-right-padding">
                            <span class="grey-font pull-left" style="font-size:28px;">&nbsp$</span><span style="font-size:28px;">&nbsp <?php echo $auctions->reserve_price ?></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 no-left-right-padding">
                            <h5 class="no-margin black-font pull-right" style="padding-top: 7px;">Bid History:</h5>

                        </div>
                        <div class="col-md-6 no-left-right-padding">
                            <span class="green-font" style="font-size:24px;">&nbsp<?= $auctions->bids ?></span>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="col-md-12">
                    <h4 class="black-font">Enter Your Bid Amount:</h4>
                    <div  class="col-md-12 grey-bg border-radius" style="padding-top:10px;margin-bottom: 10px">
                        <div class="col-md-12 no-left-right-padding" >
                            <div class="form-group">

                                <?php   echo form_open("bid/confirm_bid/".$auctions->id, array('id' => 'bid_form'));?>
                                <form action="<?=site_url('bid/confirm_bid');?>" method="post" id="bid_form">

                                    <input type="text" <?php echo ($auctions->status)? ' ' : 'disabled' ?> class="form-control  full-width" name="price" id="price"  placeholder="$0.00">

                            </div>
                        </div>
                    </div>

                    <div  class="col-md-12 light-green-bg grey-border border-radius" style="margin-bottom: 10px">
                        <div class="col-md-12 no-left-right-padding" >
                            <h4 class="text-center" style="padding-top:0px;"><span><img src="<?php echo base_url()."assets/icons/timer.png"; ?>"> </span> <span>Auction End Time: </span> <span class="green-font">

                            <?php echo ($diff->d > 1)? $diff->d.'d' : $diff->d . 'd' ?>
                            <?php echo ($diff->h > 1)? $diff->h.'h' : $diff->h . ' Hour,' ?>

                            <?php echo  ($diff->i > 1)? $diff->i . 'm' : $diff->i .' Minute'; ?>

                        </span></h4>
                        </div>
                    </div>
                    <div class="col-md-12 no-left-right-padding">
                        <button class="full-width btn btn-success" <?php echo ($auctions->status)? ' ' : 'disabled' ?> id="submit_bid"  "><span><img src="<?php echo base_url()."assets/icons/auctions_white.png "?>"></span><span style="font-size:18px;">&nbspSubmit Bid</span></button>

                    </div>
                </div>


            </div>
            <div class="col-md-12 no-left-right-padding" style="margin-top: 20px;">
                <div class="col-md-7 no-left-padding" >
                    <div class="col-md-12 " style="border: 1px solid rgb(233, 228, 228);border-radius: 4px;">
                        <h3> Auction Details</h3>
                        <hr class="">
                        <div class="col-md-6 grey-bg" style="border-radius: 4px;padding-top:10px;">
                            <div class="col-md-6 no-left-right-padding">
                                <h4 class="pull-right">Auction Status:</h4>
                            </div>
                            <div class="col-md-5 no-right-padding">

                                <button class="btn btn-<?php echo ($auctions->status)? 'success' : 'danger' ?> full-width">
                                    <?php echo ($auctions->status)? 'Active' : 'Not Active' ?>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <div class="col-md-8 no-left-right-padding">
                                <div class="col-md-6 no-left-padding col-offset-1">
                                    <h4 class="pull-right">Starting Bid Amount:</h4>
                                </div>
                                <div class="col-md-5 no-left-padding">
                                    <h4 class="blue-font pull-left">$<?php echo $auctions->bid_price ?></h4>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <hr class="no-margin">
                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <div class="col-md-9 no-left-right-padding">
                                <div class="col-md-5 no-left-right-padding">
                                    <h4 class="pull-right">Auction End Date:</h4>
                                </div>
                                <div class="col-md-7 no-right-padding">
                                    <h4 class="blue-font pull-left"><?php $end_date = date_create($auctions->end_date);
                                        echo date_format($end_date," F d, Y @ h:i A") ?></h4>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <hr class="no-margin">
                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <div class="col-md-9 no-left-right-padding">
                                <div class="col-md-5 no-left-right-padding">
                                    <h4 class="pull-right">Auction Started:</h4>
                                </div>
                                <div class="col-md-7 no-right-padding">
                                    <h4 class="blue-font pull-left"> <?php $start_date = date_create($auctions->start_date);
                                        echo date_format($start_date," F d, Y @ h:i A") ?></h4>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <hr class="no-margin">
                        </div>
                        <div class="col-md-12 no-left-right-padding">
                            <div class="col-md-9 no-left-right-padding">
                                <div class="col-md-5 no-left-right-padding">
                                    <h4 class="pull-right">Bid History:</h4>
                                </div>
                                <div class="col-md-5 ">
                                    <h4 class="blue-font pull-left"><?= $auctions->bids; ?> Bids</h4>
                                </div>
                            </div>

                        </div>



                    </div>


                    <div class="col-md-12 no-left-right-padding" style="padding-top: 18px;"><h4>Other Auctions You May Like</h4>  <hr></div>

                    <?php $this->load->view('bid/other_bids_listing_new'); ?>





                </div>
                <div class="col-md-1" style="width:3.333%"></div>
                <div class="col-md-4  blue-border no-left-right-padding" style="border:1px solid #ececec;background-color:white;border-radius:4px;width: 37.333333%;">
                    <?php $this->load->view('product/product_user_detail_new'); ?>
                    <?php $this->load->view('bid/product_other_listing_new'); ?>
                </div>
            </div>

        </div>
        <!--test-->

    </div>

    <!-- Bootstrap core JavaScript
 ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
    <!-- Latest compiled and minified JavaScript -->

    <script type="text/javascript" src="<?php echo base_url()."assets/js/global_search.js";?>"></script>
    <script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
    <script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $("#submit_bid").click(function (e) {
                var current_bid = Number("<?php echo empty($auctions->current_bid) ? $auctions->reserve_price : $auctions->current_bid ?>");
                var bid_price = $("#price").val();
                if (bid_price <= current_bid) {
                    alert("Bid must be greater than current bid price.");
                    return false;
                }

                $("#bid_form").submit();

            });
        });
    </script>


    <script type="text/javascript">

        $(document).ready(function() {

            var current_position = 0;

            var  start_image_path = "<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>";

            //alert(current_position);
            var csrf_hash = "<?= $this->security->get_csrf_hash();?>"
            $('.img-thumbnail-large-0').show();


            $(document).on('click','#prev_button',function(){
                if(current_position>0) {
                    current_position = current_position -1;
                    $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                    console.log('.img-thumbnail-small-'+current_position);
                }
                //alert(current_position);

            });

            $(document).on('click','#next_button',function(){

                if(current_position<=4){
                    current_position = current_position+1;
                    $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                    console.log('.img-thumbnail-small-'+current_position);
                }

            });



            $(document).on('click','.img-thumbnail-small-1',function(){
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-1').attr('src'));

                //replace image one on this pic place


                return false;
            });

            $(document).on('click','.img-thumbnail-small-2',function(){
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-2').attr('src'));
                //replace image one on this pic place


                return false;
            });

            $(document).on('click','.img-thumbnail-small-3',function(){

                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
                //replace image one on this pic place


                return false;
            });

            $(document).on('click','.img-thumbnail-small-4',function(){
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-4').attr('src'));
                //replace image one on this pic place
                return false;
            });

            $(document).on('click','.img-thumbnail-small-0',function(){
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
                //replace image one on this pic place
                $('.img-thumbnail-small-3').attr('src',start_image_path);


                return false;
            });

            // Quantity buttons add and decerase
            $("#increment_button").click(function(){
                //alert("Increase quantity.");
                var qty = parseInt($("#p_quantity").val(),10);
                //alert(qty);
                if (qty < 10){
                    newqty = qty + 1;
                    $("#p_quantity").val(newqty.toString());
                    $("#product_quantity").val(newqty.toString());
                    //alert($("#product_quantity").val());
                }
            });

            $("#decrement_button").click(function(){
                //alert("Increase quantity.");
                var qty = parseInt($("#p_quantity").val(),10);
                //alert(qty);
                if (qty > 1){
                    newqty = qty - 1;
                    $("#p_quantity").val(newqty.toString());
                    $("#product_quantity").val(newqty.toString());
                    //alert($("#product_quantity").val());
                }
            });

        });//end of document ready

    </script>

    <script>

        var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $(".rating").rating('refresh',
            {showClear: false, showCaption:false,size: 'xs',starCaptions: {5.0:'5 Stars'},
            });

        $('.rating').on('rating.change', function() {


            if(!readOnly) {

                var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
                var to_extract_from_id_text = null;

                var value =  $(this).val();

                var result = confirm("Are you sure you want to rate this "+ type_of_item_rated+" "+value+" ?");

                if(result==true) {

                    if(type_of_item_rated=='store') {
                        to_extract_from_id_text = "rating_element-store";
                    }
                    if(type_of_item_rated=='product') {
                        to_extract_from_id_text = "rating_element-";
                    }
                    if(type_of_item_rated=='profile') {
                        to_extract_from_id_text = "rating_element-";
                    }


                    var static_id_text=(to_extract_from_id_text).length;
                    var profile_id =  ((this).id).slice(static_id_text);
                    var rated = $(this).val();
                    var server_url = base_url_complete+'rating/rate';

                    save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

                }
            }
            else {
                window.location.assign(base_url_complete+'users/login');
            }

        });

    </script>