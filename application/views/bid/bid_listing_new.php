
<style type="text/css">

    .xpens {

        -webkit-background-size: cover; /* For WebKit*/
        -moz-background-size: cover;    /* Mozilla*/
        -o-background-size: cover;      /* Opera*/
        background-size: cover;         /* Generic*/
    }
    .thumbnail{
        border:none;
    }
    .thumbnail .caption h4 {
        font-size: larger;
        overflow: hidden;
        height: 35px;
        margin-top: 7px;
        margin-bottom:5px;
    }
    .delete_btn {
        background-color: #3E3F3F;
        color: white;
        float:right;
        border-radius: 2px;
    }
    .thumbnail .caption {
        padding: 2px 7px !important;
    }
    .rating_class {
        float: right;
    }
    .rating-xs {
        font-size: 1.6em;
    }

    .rating-gly-star {
        font-family: 'Glyphicons Halflings';
        padding-left: 2px;
        font-size: smaller;
    }

    .star-rating .caption, .star-rating-rtl .caption {
        color: #999;
        display: inline-block;
        vertical-align: middle;
        float: right;
        font-size: 80%;
    }

    .edit-delete-toggle {
        display: none;
    }
    .seller-name{
        padding-left:4px;color: #1f72ad;
    }
    .rating-container{
        vertical-align: top;
    }
    .view-more-btn{

        background: transparent;
        width: 100%;
        padding: 8px 0px 0px;
    }
    .font-size-10{
        font-size:10px;;
    }
</style>



<div class='col-md-12 col-sm-6'>
    <h4>Latest Auctions</h4>
    <hr>
    <?php if(count($products)>0): ?>
            <?php //var_dump($products);die;?>
    <?php foreach($products as $product): ?>

    <div class="col-sm-3 col-md-3 no-left-padding" style="margin-bottom: 10px;">

        <div class="thumbnail item-lisiting-inner col-sm-12 white-bg" style="margin-bottom: 0px;height: 307px;border: 1px solid rgb(230, 230, 230);">
            <a href="<?php echo site_url(array('bid', 'product', $product['auction_id'],'bid'));?>" role="button" data-lightbox="image-1" data-title="My caption">


                <img  alt="<?php  echo $product['name'];?>"  class="img-thumbnail img-responsive xpens"
                      src="<?php echo $product['image'] ;?>"
                      style=' text-align: center; width: 100%;height: 200px;'>
            </a>

            <div class="caption">
                <div class="col-sm-10 no-left-right-padding" style="height: 31px;">
                    <p class='product_name_heading'><?php echo $product['name'];?></p>

                </div>
                <div class="col-sm-2">
                    <button id="heartBtn" class="btn heart-btn active"><i class="glyphicon glyphicon-heart"></i></button>
                </div>
                <div class="col-sm-12  no-left-right-padding" style="padding: 7px 0px 0px;height: 47px;">
                    <div class="col-md-6 pull-left no-left-padding" style="height: 61px;">
                        <span class="dollar-sign" style="padding-top:0px">$</span><span style="padding-top:0px" class='price no-left-padding'><?php echo $product['auctions_bid_price'];?></span>
                        <p class="green-font" style="padding-left: 23px;"><small class="buy-text"><?php echo $product['bid_counter'];?>  BID</small></p>
                    </div>
                    <div class="col-md-6 pull-right no-right-padding" >
                          <span class="buy_btn viewDetails">
                            <a href="<?php echo  site_url(array('bid', 'product', $product['auction_id'],'bid')) ;?>" class="btn btn-primary bid-btn btn-lg" role="button">Bid Now</a>
                          </span>
                    </div>
                    <div class="clearfix"></div>
                </div>


            </div>
        </div>
        <div class="col-sm-12 no-left-right-padding " style="height: 36px;padding-top: 7px;border-left: 1px solid #e6e6e6;border-right: 1px solid #e6e6e6;">
            <div class="col-sm-12  " >

                <div class="col-sm-7 no-left-right-padding">
                    <span class="user-sm"></span>
                   <span class="black-font">

                    <a href="<?php echo base_url('sell/seller').'/'.$product['profile_id'];?>">
                        <span class="black-font"><?php  echo $product['seller_name'] ;?></span></a>
                   </span>
                </div>
                <div class="col-sm-5 no-left-right-padding" style="">
                    <p>
                        <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['profile_id'];?>" value="<?= $product['seller_rating'] ;?>" data-type="profile" />

                    </p>

                    <p class='edit-delete-toggle'>
                        <a href="#" class="btn btn-default btn-default btn-sm " role="button">Edit</a>
                        <a href="<?php echo base_url().'dashboard/set_auction/'.$product['product_id'] ?>" class="btn  btn-primary btn-sm pull-right" role="button">Set as Auction</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-left-right-padding green-bg white-font" style="padding-top:8px;border-bottom-left-radius: 2px;border-bottom-right-radius: 2px;">
            <div class="col-sm-12">
                <div class="col-sm-1 no-left-padding">
                    <img src="<?php echo base_url()."assets/icons/time_left.png";?>">
                </div>
                <div class="col-sm-4 no-left-right-padding">
                    <p>Time Left:</p>
                </div>
                <div class="col-sm-7 no-left-right-padding ">
                   <?php $starting_date = $product['auctions_starting_date'];
                    $end_date = strtotime($product['auctions_end_date']);
                    $current_date = strtotime(date('Y-m-d h:i:s a', time()));
                    $diff = abs($end_date-$current_date);
                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                    $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                    $minutes  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                    $time_left =$days . "d " . $hours . "h " . $minutes . "m";
                    ?>

                    <p style="text-align:right; "><?= $time_left ?></p>
                </div>
            </div>
        </div>

    </div>


    <?php endforeach ;?>
    <div class="col-md-12 ">
        <hr style="border-top:1px solid #e0e0e0;">
    </div>

    <div class="col-md-12 " style="padding: 25px;">
        <div class="col-md-2 col-md-offset-5"><a class="underline dark-grey-font view-more-btn btn btn-default" type="button"><p>View More <span class="fa fa-caret-down"></span></p></a></div>
    </div>
    <?php else: echo "<div class='col-md-12 alert alert-info'><h3 class='text-center'>We are sorry ,Auctions Not Available!</h3></div> ";?>


    <?php endif ;?>

</div>
