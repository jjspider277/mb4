
<style type="text/css">
  
.xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
.thumbnail .caption h4 {
  margin-top: 0px;
  font-size: larger;
  overflow: hidden;
}

.thumbnail .caption {
  padding: 2px;
}

    .price {
     padding-left: 6px;       
    }

  .bid_count_text {
      display: block;
      color: #428bca;    
      padding-left: 6px;                      
    }
    .rating_class {
      float: right;

    }
.rating-xs {
font-size: 1.4em;
}

.rating-gly-star {
  font-family: 'Glyphicons Halflings';
  padding-left: 2px;
  font-size: smaller;
  }

.star-rating .caption, .star-rating-rtl .caption {
  color: #999;
    float: right;
  display: inline-block;
  vertical-align: middle;
  font-size: 80%;
}

.glyphicon-user {
margin-top: 7px;
}

</style>




<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php if(count($products_i_like_count)>0): ?>

 <?php foreach($liked_products as $product): ?>

      <div class="col-sm-3 col-md-3">

            <div class="thumbnail">
                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>">

                        <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $product['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
            <div class="caption">
                       <h4 class='product_name_heading'><?php echo $product['name'];?></h4>
                       <p class="product_description"><?php echo $product['desc'];?></p>
                        <p>
                          <span class='price'>$<?php echo $product['price'];?></span>
                          <span class='buy_btn viewDetails'> 
                            <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>" class="btn btn-primary" role="button">View Details</a>
                          </span>
                      </p>
            </div>
             <hr>

                  <p>
                        <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['product_id'];?>" value="<?= $product['product_rating'] ;?>" data-type="products" />
                        <span class='seller_name'><i class='glyphicon glyphicon-user'></i>
                        <a href="<?php echo base_url('sell/seller').'/'.$product['profile_id'];?>"> 
                        <b style='padding-left:4px;color: #1f72ad;'><?php echo $product['seller_name'];?></b></a>
                    
                      </span> 
                       
                   </p>
                

                
               
<!--
                <p>
                <a href="#product-edit-tab" aria-controls="product-tab" role="tab" data-toggle="tab">

                    <span class="edit-image" style="margin-top: 10px;"><strong>Edit</strong></span></a>
                    <a href="#"><span class="delete-image" style="margin-top: 10px;"><strong>Delete</strong></span><i class="fa listing"></i>
                </p>-->
            </div>
      </div>
      
<?php endforeach ;?>
<?php else: ?>

<div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>


</div>
</div>
           