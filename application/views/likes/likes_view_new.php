
           
<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php if(count($products_i_like_count)>0): ?>

 <?php foreach($liked_products as $product): ?>


      <div class="col-sm-3 col-md-3">

            <div class="thumbnail item-lisiting-inner col-sm-12 white-bg">
            <a href="<?php echo $product['image'];?>" data-lightbox="image-1" data-title="My caption">
                

                        <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $product['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
                 <div class="caption">
                 	<div class="col-sm-10 no-left-right-padding">
                       <p class='product_name_heading'><?php echo character_limiter($product['name'], 21);?></p>
                       <!--<p class="product_description"><?php //echo word_limiter($product['desc'], 9);?></p>-->
                    </div>
                    <div class="col-sm-2">
                    <button id="heartBtn" class="btn heart-btn active"><i class="glyphicon glyphicon-heart"></i></button>
                    </div>  		  
					<div class="col-sm-12  no-left-right-padding" style="padding: 7px 0px;">
					  <div class="pull-left">
						 <span class="dollar-sign">$</span><span class='price no-left-padding'><?php echo $product['price'];?></span> <br/>
						  <small class="buy-text">USD</small>
					  </div>
					  <div class="pull-right" >
						  <span class="buy_btn viewDetails"> 
							<a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>" class="btn btn-primary" role="button">BUY NOW</a>
						  </span>
					  </div>
					  <div class="clearfix"></div>
					</div>
         		
					<div class="col-sm-12 no-left-right-padding">		  
                      <hr>
                    
          </div>  
                   <div class="col-sm-6 no-left-padding">
                     <span class="user-sm"></span>
				              <span class="black-font">
				   
				            	<a href="<?php echo base_url('sell/seller').'/'.$product['profile_id'];?>"> 
				              	<b class="black-font"><?php  echo character_limiter($product['seller_name'], 7) ;?></b></a>
				              </span>
				              </div>
				    <div class="col-sm-6 no-right-padding" >
                  <p>
                   <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['product_id'];?>" value="<?= $product['product_rating'] ;?>" data-type="product" />                   </p>
											   
                   <p class='edit-delete-toggle'>
                   <a href="#" class="btn btn-default btn-default btn-sm " role="button">Edit</a>
                   <a href="<?php echo base_url().'dashboard/set_auction/'.$product['product_id'] ?>" class="btn  btn-primary btn-sm pull-right" role="button">Set as Auction</a> 
                  </p>
                  </div>
                  
              </div>          
            </div>

      </div>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>

</div>
</div>