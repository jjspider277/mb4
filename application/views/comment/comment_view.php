<style type="text/css">
  .comment-box {
    background-color: white;
    border:1px solid #ececec;
    padding: 10px;
    padding-bottom: 0px;
    margin-bottom: 20%; 
  }
  .user-small-image {
    margin-top: 1px;
  }
  .comment-text .form-control {
    height: 26px;
    border-radius: 2px;
    border: none;
  }
  .comment-text{
    padding-left: 2px;
    }
  .comment-button .btn {
    padding: 3px 12px;
    border-radius: 2px;
     border: none;
  }
  .comment-input-row {
    background-color: #ededed;
    padding: 5px;
    height: 35px;
    margin-top: 50px;
  }
  .comment-button .btn {
    background-color: #0e5e97;
    color: white;
    font-weight: 500;
  }

  .comment-button .btnico {
 background-color: #4a4a4a;
  }
  .comment-content {

   padding-top: 6px;
   text-align: justify;
   color: #989797;
  }
  .comment_date {
    float:right;
    /*/margin-right: 30px;*/
  }
  .social {

    margin-left: 11.666667%;
    color: black;
    padding-top: 5px;
  }
   .social span {
    color: #11619a;
    font-weight: bold;
   }
   .comment_body {
    padding: 10px;
    padding-bottom: 0px;
   }
   .comment_body hr {
    padding-bottom: 0px;
    margin-bottom: 15px;
    margin-top: 15px;
   }
   .view-more p {

    font-size: 15px;
    padding: 4px;

   }
   .social-buttons{
    float: left;
   }
   .social-buttons-inner{
    float: left;
   }
</style>
      
      <div class="col-md-12 comment-box">
               <?php 
                 if($is_logged_in===TRUE){
               ?>
                  <?php echo form_open('product/add_comment'); ?>
                    <div class='comment-input-row'>
                        
                        <img class='pull-left user-small-image' src="<?php echo  $product->profile->profile_image;?>" height='24' width='3%'alt="...">
                          
                          <div class="col-md-8 comment-text" style="padding-right:0px;">
                            <input type="text" class="form-control" name="comment" id="comment" placeholder="Your comment!">
                          </div>
                          <?php $parent = 0;?>
                          <?php echo form_hidden('product_id', $product->product_id); ?>
                          <?php echo form_hidden('parent_comment_id', $parent); ?>
                          <?php echo form_hidden('comment_to_id', $product->profile_id); ?>
                          <?php echo form_hidden('comment_by_id', $profile->id); ?>                       
                         <div class="btn-group pull-right comment-button" role="group" aria-label="...">
                           <button type="submit" class="btn btn-default"> Leave Your Comment</button>
                           <button type="button" class="btn btn-default btnico"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button>
                         </div>

                    </div>
                  <?php echo form_close(); ?>
                  <?php 
                }else{

                  ?>
                  <div class='comment-input-row'>
                        
                        <img class='pull-left user-small-image' src="<?php echo  $product->profile->profile_image;?>" height='24' width='3%'alt="...">
                          
                          <div class="col-md-8 comment-text" style="padding-right:0px;">
                            <input type="text" class="form-control" placeholder="Sign in to post a comment!">
                          </div>
                       
                         <div class="btn-group pull-right comment-button" role="group" aria-label="...">
                           <button type="button" class="btn btn-default">Login to Leave a Comment</button>
                           <button type="button" class="btn btn-default btnico"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button>
                         </div>

                  </div>
                <?php
              }
              ?>

             
                  <div class="comment_body">
                    <h3 style="color:#595959;font-weight:600;">Comments</h3>
                    <hr class="hr_border">
              <?php if(count($comments)>0): ?>
                   

                  <?php foreach($comments as $comment): ?>

                               <?php 
                                $this->db->select('*');
                                $this->db->where("parent_comment_id", $comment['id'] );
                                $query = $this->db->get("comments");
                                $comment_replies = $query->result_array();
 
                               ?>
                               <?php $profile_c = $this->profile->with('media')->get( $comment['commeted_by_id']);  ?>
                               <?php //var_dump($profile_c);?>
                               <?php $profile_image = '';

                              if(isset($profile_c->media))  {
                                     $profile_image = "uploads/profile/" . $profile_c->id . "/avatar/" . $profile_c->media->file_name;
                                 } else {
                                         $profile_image = "uploads/no-photo.jpg";
                               } ?>



                               <?php 
                                $this->db->select('*');
                                $this->db->where("comment_id", $comment['id']);
                                $query = $this->db->get("comment_likes");
                                $comment_likes = $query->result_array();

                                //var_dump($comment_likes);

                                $this->db->select('*');
                                $this->db->where("comment_id", $comment['id']);
                                $this->db->where("liker_profile_id", $this->profile_id);
                                $query = $this->db->get("comment_likes");
                                $current_user_likes = $query->result_array();

                                //var_dump($current_user_likes);
 
                               ?>

                          <div class="media">
                                <a class="media-left" href="#">
                                  <img class='pull-left user-small-image' src="<?php echo base_url().$profile_image?>" height='100' alt="...">
                                </a>
                                <div class="media-body">
                                  <h4 class="media-heading" style="width:650px;"><?php echo $profile_c->fname; echo " "; echo $profile_c->lname; ?><small class="col-md-4 comment_date"><?php echo $comment['comment_date']; ?></small></h4>
                                  <p class='comment-content'><?php echo $comment['comment']; ?></p>
                                </div>
                                <div class='row'>
                                <div class='col-md-5 col-md-offset-2 social'>
                                <div class="social-buttons" id="like-thumb" data-comment-id="<?php echo $comment['id'];?>">
                                  <i class='glyphicon glyphicon-thumbs-up' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='#' id="like-link-<?= $comment['id']?>" data-comment-id="<?php echo $comment['id'];?>"><?php if(count($current_user_likes)!=0){echo "Liked";}else{ echo "Like";} ?></a> &nbsp;<span id="count-of-like-<?= $comment['id']?>"style='font-weight:400;'>(<?php echo count($comment_likes);?>)</span>
                                  &nbsp; &nbsp;
                                </div>
                                <div  class="comment-reply" data-comment-id="<?php echo $comment['id'];?>">
                                  <i class='glyphicon glyphicon-comment' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='-<?= $comment['id']?>#' data-comment-id="<?php echo $comment['id'];?>">Comment</a> &nbsp; <span style='font-weight:400;'>(<?php echo count($comment_replies);?>)</span>
                                </div>
                                </div>
                               </div>
                          </div>
                          
                          <div class='col-md-12'>
                            <hr class="width:100%;">
                          </div>


                <div class="sub-comment-popup" data-comment-id="<?php echo $comment['id'];?>" id="more-comment-<?= $comment['id']?>">
                      
                      <a  class="close-buttons" data-comment-id="<?php echo $comment['id'];?>" id="close-<?= $comment['id']?>"><img src=<?php echo base_url()."assets/images/close1.png" ?> ></a>

                      <div class="comment-popup">
                         <?php $profile_c = $this->profile->with('media')->get( $comment['commeted_by_id']);  ?>
                               <?php //var_dump($profile_c);?>
                               <?php $profile_image = '';

                              if(isset($profile_c->media))  {
                                     $profile_image = "uploads/profile/" . $profile_c->id . "/avatar/" . $profile_c->media->file_name;
                                 } else {
                                         $profile_image = "uploads/no-photo.jpg";
                               } ?>
                              <div class="media">
                                <a class="media-left" href="#">
                                  <img class='pull-left user-small-image' src="<?php echo base_url().$profile_image?>" height='100' alt="...">
                                </a>
                                <div class="media-body">
                                  <h4 class="media-heading" style="width:400px;"><?php echo $profile_c->fname; echo " "; echo $profile_c->lname; ?><small class="comment_date"><?php echo $comment['comment_date']; ?></small></h4>
                                  <p class='comment-content'><?php echo $comment['comment']; ?></p>
                                </div>
                                <div class='row'>
                                <div class='col-md-5 col-md-offset-2 social' style="width:400px;">
                                <div class="social-buttons" id="like-thumb" data-comment-id="<?php echo $comment['id'];?>">
                                  <i class='glyphicon glyphicon-thumbs-up' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='#' id="like-link-<?= $comment['id']?>" data-comment-id="<?php echo $comment['id'];?>"><?php if(count($current_user_likes)!=0){echo "Liked";}else{ echo "Like";} ?></a> &nbsp;<span id="count-of-like-<?= $comment['id']?>"style='font-weight:400;'>(<?php echo count($comment_likes);?>)</span>
                                  &nbsp; &nbsp;
                                </div>
                                <!--<div class="social-buttons-inner" id="comment-reply" data-comment-id="<?php echo $comment['id'];?>">
                                  <i class='glyphicon glyphicon-comment' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='#'>Comment</a> &nbsp; <span style='font-weight:400;'>(15)</span>
                                </div>-->
                                </div>
                               </div>
                          
                        </div>

                          <div class='col-md-12'>
                            <hr class="width:100%;">
                          </div>
                       
                      <?php  
                                

                        if(isset($comment_replies)){
                            foreach($comment_replies as $reply){
                                          ?>
                               <?php $profile_r = $this->profile->with('media')->get( $reply['commeted_by_id']);  ?>
                               <?php //var_dump($profile_c);?>
                               <?php $profile_image = '';

                              if(isset($profile_r->media))  {
                                     $profile_image_r = "/uploads/profile/" . $profile_r->id . "/avatar/" . $profile_r->media->file_name;
                                 } else {
                                         $profile_image_r = "/uploads/no-photo.jpg";
                               } ?>

                                <?php 
                                $this->db->select('*');
                                $this->db->where("comment_id", $reply['id']);
                                $query = $this->db->get("comment_likes");
                                $reply_likes = $query->result_array();

                                //var_dump($comment_likes);

                                $this->db->select('*');
                                $this->db->where("comment_id", $reply['id']);
                                $this->db->where("liker_profile_id", $profile->id);
                                $query = $this->db->get("comment_likes");
                                $current_user_reply_likes = $query->result_array();

                               // var_dump($current_user_reply_likes);
 
                               ?>



                              <div class="reply-group" style="margin-left:50px;">

                                <div class="media">
                                <a class="media-left" href="#">
                                  <img class='pull-left user-small-image' src="<?php echo base_url().$profile_image_r?>" height='100' alt="...">
                                </a>
                                <div class="media-body">
                                  <h4 class="media-heading" style="width:350px;"><?php echo $profile_r->fname; echo " "; echo $profile_r->lname; ?><small class="comment_date"><?php echo $comment['comment_date']; ?></small></h4>
                                  <p class='comment-content'><?php echo $reply['comment']; ?></p>
                                </div>
                                <div class='row'>
                                <div class='col-md-5 col-md-offset-2 social' style="width:350px;">
                                <div class="social-buttons" id="like-thumb" data-comment-id="<?php echo $reply['id'];?>">
                                  <i class='glyphicon glyphicon-thumbs-up' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='#' id="like-link-<?= $reply['id']?>" data-comment-id="<?php echo $reply['id'];?>"><?php if(count($current_user_reply_likes)!=0){echo "Liked";}else{ echo "Like";} ?></a> &nbsp;<span id="count-of-like-<?= $reply['id']?>"style='font-weight:400;'>(<?php echo count($reply_likes);?>)</span>
                                  &nbsp; &nbsp;
                                </div>
                               <!-- <div id="comment-re" class="social-buttons-inner">
                                <i class='glyphicon glyphicon-comment' style='color:#11619a;font-size:20px;'></i> &nbsp;<a href='#'>Comment</a> &nbsp; <span style='font-weight:400;'>(15)</span>
                                </div>-->
                                </div>
                               </div>
                          </div>
                        </div>
                          
                          <div class='col-md-12' width="20px">
                            <hr class="width:100%;">
                          </div>
                                <?php
                                      }

                                }

                          ?>

                                <?php echo form_open('product/add_comment'); ?>
                                <div class='comment-input-row'>
                        
                                <img class='pull-left user-small-image' src="<?php echo  $product->profile->profile_image;?>" height='24' width='3%'alt="...">
                          
                                <div class="col-md-8 comment-text" style="padding-right:0px; width:300px">
                                    <input type="text" class="form-control" name="comment" id="comment" placeholder="Your comment!">
                                </div>
                                  <?php echo form_hidden('product_id', $product->product_id); ?>
                                  <?php echo form_hidden('parent_comment_id', $comment['id']); ?>
                                  <?php echo form_hidden('comment_to_id', $product->profile_id); ?>
                                  <?php echo form_hidden('comment_by_id', $profile->id); ?>                       
                                <div class="btn-group pull-right comment-button" role="group" aria-label="...">
                                <button type="submit" class="btn btn-default"> Leave Your Comment</button>
                                <button type="button" class="btn btn-default btnico"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button>
                                </div>

                                </div>
                               <?php echo form_close(); ?>

                  </div>
              </div>
                         

          <?php endforeach ;?>
      <?php endif ; ?>       
                          <!--comment2-->

                          <div class='view-more' style="text-align:center;">
                             <p>View More Comments <i class='caret' style='color:#11619a;font-size:20px;'></i></p>
                          </div>
                  </div>
              
   </div>  