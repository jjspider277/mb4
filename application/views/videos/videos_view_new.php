
<?php if($is_logged_in==true && $loaded_profile_same_as_user==true) :?>

    <!--apply ccs only on logged in -->
    <style type="text/css">
        .margin-tops {
            margin-top: 13px;"
        }
    </style>

    <div class="col-md-3 col-md-offset-9" style="margin-bottom: 16px;">

        <button class="btn btn-primary col-md-12" id="video_show_button">Add New Video</button>
        <button class="btn btn-danger col-md-12" id="video_hide_button" style="display:none;">No ,Thankyou!</button>

    </div>
<?php endif;?>
<div class="col-md-12" id="upload_video_div" style="display:none;">

    <?php echo $error;?>

    <?php echo form_open_multipart('video/upload_video');?>

    <div class="col-md-5 col-md-offset-7 white-bg border-radius-6 padding-8 blue-border">
        <div class="col-md-4">
            <input id='title' name='title' class="form-control grey-bg" placeholder='title here'>
        </div>
        <div class="col-md-8">

            <div class="col-md-4">

                <input id="my_videos_img" name="userfile" type="file" class="btn btn-primary btn-sm" accept="videos/*"/>
                <br/>
                <input id="upload_videos_photos" type='submit' class="btn btn-primary btn-default" value="Upload"></input>

            </div>
        </div>

    </div>

    </form>

</div>


<style type="text/css">

    .rating_class {


    /**added by eyayu */
    .no-left-padding{
        padding-left:0px;
    }
    .no-right-padding{
        padding-right: 0px;
    }
    .no-left-right-padding{
        padding-left:0px;
        padding-right:0px;

    }
    .video-container{

        margin-top:36px;

    }


    .video_name{
        color:#444444;
    }




    .white-background{
        background:#ffffff;

    }

    div.thumbnail > div.text_content > div.col-sm-12 > div.col-sm-6 > span{
        padding-right:8px;
    }
    div.thumbnail > div.text_content > div.col-sm-12 > div.friends {
        border-right: 1px solid rgb(203, 203, 203);
    }
    div.thumbnail > div.col-sm-1 > button.btn{
        background: #ffffff;
        border:1px solid #dbdbdb;
    }
    div.text_content > div.seller_location > h4 {
        padding-top:0px;
    }
    /** end of css added by eyayu */
    @media (min-width: 992px) {
        .col-md-3 {
            width: 25%;
        }
    }
    .border{
        border:1px solid #000000;
    }
    .view-more-btn{
        border:1px solid #ABABAB;
        background: transparent;
    }
    .view-more-btn > p{
        padding: 0px 14px;
        font-weight: 900;
        margin-top: 7px;
    }
    hr{
        margin-top:0px !important;
        border-color:#e0e0e0;
    }


</style>



<div class="row ">

    <div class="col-md-4 "><h4>Viewing all Videos</h4></div>

    <div class="col-md-12"><hr></div>
</div>

<div class="col-md-12 white-background " style="padding: 13px 0px ">
    <?php if(count($videos) > 0){?>
    <?php foreach ($videos as $video_item) { ?>
        <div class="col-sm-3 col-md-3 video-container">
            <div class="item-listing-inner col-sm-12 no-left-right-padding">
                <div class="col-sm-12 no-left-right-padding ">
                        <a href="">

                            <video  width="250" height='180' style="margin-top:-34px;"controls>
                                <source src="<?= $video_item->video_path ;?>" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                        </a>

                </div>
                 <!--start of text content -->
                <div class="col-sm-12">
                    <h5><?=$video_item->title?></h5>
                </div> <!--end of text content -->

            </div>
        </div>

    <?php } ?>


    <?php } else echo "<div class='col-m-12 alert alert-info'><h3 class='text-center'>Videos Are Not Available!!</h3></div>";?>




</div>
<?php if(count($videos) > 0){?>
<div class="col-sm-12"> <hr></div>

<div class="col-md-12 " style="padding: 16px;">
    <div class="col-md-2 col-md-offset-5"><a class="underline dark-grey-font view-more-btn btn btn-default" type="button"><p>View More <span class="fa fa-caret-down"></span></p></a></div>
</div>
<?php } ?>

