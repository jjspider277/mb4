<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Purchases</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">

</head>

<style type="text/css">
.main-nav nav{
background-color: white;
}
.btn-red {
    background: #e74c3c;;
    color: #fff;
    padding: 5px 10px;
    border-radius: 3px;
}
</style>
<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>

     <?php $this->load->view($header_logo_white); ?>


  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
	 <div class="container"> 
		<h2>Account Overview</h2>
		<div class="dot"></div>
		
			<!--import side menu -->

	<?php $this->load->view($dashboard_sidemenu); ?>

    <?php //if(count($order_data)>0) :?>
		<div class="col-md-10 middle-section">
		<h3>My Purchase Transactions</h3>
		<div class="dot"></div>
		<form class="form-inline search-form" role="form" method="" action="">
		  <div class="form-group">
			<input type="text" class="form-control" id="invoiceId" placeholder="Invoice ID or Email">
		  </div>
		  <div class="form-group">
			<select name="cusomer-id" class="form-control">
				<option value="0">Customer ID</option>
			</select>			
			<select name="shipment-stat" class="form-control">
				<option value="0">All Shipment Statuses</option>
			</select>	
			<div class="form-group">
			    <div class="input-group">
				<input type="text" class="form-control" id="dateRange" placeholder="Enter Date Range">
				<div class="input-group-addon input-group-date-picker">
					<img src="<?php echo $image_path.'date_picker.jpg';?>"></div>
				</div>
			  </div>				
			<button type="submit" class="btn btn-primary">
			<img src="<?php echo $image_path.'search.png';?>"></button>			  
		  </div>		  
		</form>
		<table class="table order-tbl">
			<thead>
				<tr>
				<th>Invoice #</th>
				<th>Seller</th>
				<th>Date</th>
				<th>Type</th>
				<th>Status</th>
				<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($order_data as $ord): ?>
					<tr>
					<th style='text-align:center;'><?php echo $ord->invoice_id;?></th>
					<th><?php echo $ord->seller_fname.' '.$ord->seller_lname;?><br/> <span class="gray table-stext"><?php echo $ord->seller_email;?></span></th>				
					<th><span class=""><?php echo $ord->created_date;?></span></th>
					
					<th><span class="btn-green"><?php echo 'Sale';?></span></th>

					<?php if($ord->ostatus == "REPORTED_STAGE"): ;?>
					<th><span class="btn-red"><?php echo 'Reported'?></span></th>
				    <?php elseif($ord->ostatus == "PURCHASED_STAGE"):;?>
					<th><span class="btn-green"><?php echo 'Success'?></span></th>
				    <?php else:;?>
					<th><span class="btn-green"><?php echo 'Processing'?></span></th>
				   <?php endif ;?>
					<th><?php echo $ord->order_total;?><br/> 
					<span class="gray table-stext">Qty: <?php echo $ord->quantity ;?></span></th>
					</tr>	
				<?php endforeach ;?>					
			</tbody>
		</table>
	</div>
	<div class="clearfix"></div>	
	 </div>
	</div>
	
	

</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
<script type="text/javascript">
	

  $(document).ready(function(event){
      
     var p = $("#my_transaction_link").find('p');
     var images = $("#my_transaction_link").find('img');
     console.log(p.first().removeClass("gray").addClass("white"));    
     console.log(images[0].src="<?php echo $image_path.'purchases1.png';?>");
     $("#my_transaction_link").find('div').first().addClass('side-active');
 });

</script>
</body>
</html>

