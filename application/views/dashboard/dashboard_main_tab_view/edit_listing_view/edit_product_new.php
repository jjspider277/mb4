<style type="text/css">
    .ico-picedit-checkmark {
      font-size: xx-large;
      color:blue;
    }
    .ico-picedit-close {
        font-size: xx-large;
        color:blue;
    }
    .picedit_drag_resize_box_corner {
        font-size: xx-large;
        color:blue;
    }
    .blue-border{
        padding:0px !important
    }
</style>
<div class="col-md-12 col-md-store-content" style="padding:10px;">

    <div class=' col-md-12 blue-border no-left-right-padding'>

        <div class="col-md-12 " style="margin-top: 2%;padding: 10px 26px;">


            <div class="form-group row col-md-4">
                <span class='required_star'>*</span>
                <label class='control-lable' for="product_name">Product </label>
                <input type="text" id='product_name_edit_listing' name="product_name_edit_listing" class='form-control' placeholder="Product Name"
                       length="20"  style="margin-top: 13px;" required>

            </div>

            <div class="form-group row col-md-12 ">
                <span class='required_star'>*</span>
                <label class='control-lable' for="product_descritpion">Add product description</label>
                <textarea class="form-control" rows="4" id='product_description_edit_listing' name="product_description_edit_listing" required></textarea>
            </div>



            <div class="col-md-4 col-md-select" >
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="category">Select Category</label>


                    <?php
                    //var_dump($catagories);
                    $catagories['#'] = 'Please Select Category';
                    echo form_dropdown('category_edit_listing', $catagories,'#','id="category_edit_listing"
                          class="form-control select-drop-down-arrow"'); ?>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="color_variation">Color</label>


                    <?php
                    //var_dump($catagories);
                    -
                    $colors['#'] = 'Please Select color';
                    ?>

                    <select class="form-control select-drop-down-arrow" id="colors_edit_listing" name='colors_edit_listing' style="background-color:rgba(51, 48, 48, 0.51);font-weight:bold;">
                        <?php foreach ($colors as $key => $value) { ?>
                            <option value="<?=$value?>" style='font-weight:bold;border 1px solid;color:<?=$value;?>'><?=$value;?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-4 ">
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="color_variation">Size</label>


                    <?php
                    //var_dump($catagories);

                    $sizes['#'] = 'Please Select size';
                    //array_push($sizes,array('small'=>'samll','medium'=>'medium','large'=>'large','x-large'=>'x-large','x-small'=>'x-small'));
                    echo form_dropdown('sizes', $sizes,'#','id="sizes_edit_listing"
                          class="form-control select-drop-down-arrow"'); ?>
                </div>
            </div>


            <!-- <div class="col-md-4 col-md-select">
                    <div class="form-group">

                        <label for="sub_variation">Sub Variation</label>

                          <?php  $sub_variation['#'] = 'Please Select';
            echo form_dropdown('sub_variation',$sub_variation,'#',
                'id="sub_variation" class="form-control"'); ?>
                    </div>
                </div> -->

            <div class="form-group row col-md-12 has-feedback">


                <label for="preview_produt_image">Add Photos</label>

                <style type="text/css">

                    .col-md-3 {
                        width: 20%;
                        padding-right: 2px;
                        padding-left: 2px;
                    }

                    .col-sm-6 .col-md-3 {
                        padding-right: -3px;
                        padding-left: 10px;
                        width: 20%;
                        padding-right: 4px;
                        padding-left: 4px;
                    }


                </style>

                <div class="row">
                    <div class="col-sm-6 col-md-3" >
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img1_edit_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img1_btn" data-imageid="img1_edit_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload" >
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img1_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img1" >
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img2_edit_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img2_btn" data-imageid="img2_edit_listing"type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img2_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img2" data-toggle="modal" data-target="#imageUploadEdit">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img3_edit_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img3_btn" data-imageid="img3_edit_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img3_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img3" data-toggle="modal" data-target="#imageUploadEdit">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img4_edit_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img4_btn" data-imageid="img4_edit_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img4_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4" >
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>

                        </div> </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img5_edit_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img5_btn" data-imageid="img5_edit_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img5_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>


                        </div> </div> </div>

                <label class='product_validation_message black-font'>Upload clear and good quality pictures</label>
                <p class='product_validation_message grey-font'>
                    <span class='required_star '>*</span>
                    At least one image is manadatory</p>
            </div>

            <div class="col-md-4 form-group no-left-padding ">
                <span class='required_star'>*</span>
                <label for="descritpion">Quantity</label>
                <input  type="number" class="form-control" name='quantity_edit_listing' id="quantity_edit_listing" required>
            </div>
            <div class="col-md-4 form-group no-left-padding ">

                <label for="price">Shipping Price</label>
                <input   type="text" class="form-control" name='shipping_price_edit_listing' id="shipping_price_edit_listing" required>
            </div>
            <div class="col-md-4 form-group no-left-padding ">

                <label for="method">Shipping Method</label>
                <select id="shipping_method_edit_listing" class="form-control select-drop-down-arrow" >
                    <option>FedEx</option>
                    <option>UPS</option>
                    <option>DHL</option>
                </select>
            </div>



            <div class="col-md-4 form-group no-left-padding">
                <span class='required_star'>*</span>
                <label for="description">Price Before Tax</label>
                <input type="number" class="form-control" name='price_edit_listing' id="price_edit_listing" required>
            </div>

            <div class="col-md-4 form-group no-left-padding ">

                <label for="descritpion">Special Price </label>
                <input type="text" class="form-control" name='sprice_edit_listing' id="sprice_edit_listing" required>

            </div>

        </div>

         <hr  class='hr_store_form'>
        <div  class="tab-content col-md-12 no-left-right-padding" >
        <div role="tabpanel" class="tab-pane fade" id="setAuction">
           <div class="col-md-12 light-green-bg">
               <h3>Set Item as Auction</h3>
               <div class="col-md-4 form-group no-left-padding ">

                   <label for="nameOfAuction">Name of Auction </label>
                   <input value="" type="text" class="form-control" name='name_of_auction_edit_listing' id="name_of_auction_edit_listing" >

               </div>
               <div class="col-md-4 form-group ">

                   <label for="startingBidPrice">Starting Bid Price </label>
                   <input value="" type="text" class="form-control" name='starting_price_bid_edit_listing' id="starting_price_bid_edit_listing" >

               </div>
               <div class="col-md-4 form-group ">

                   <label for="reservePrice">Reserve Price </label>
                   <input value="" type="text" class="form-control" name='reserve_price_edit_listing' id="reserve_price_edit_listing" >

               </div>
               <div class="col-md-4 form-group no-left-padding ">

                   <label for="auctionStartDate">Auction Start Date / Time </label>
                   <input  type="date" class="form-control" name='auction_startdate_edit_listing' id="auction_startdate_edit_listing"" >
                   <small>Leave blank to start immediately</small>

               </div>
               <div class="col-md-4 form-group  ">

                   <label for="auctionStartDate">&nbsp </label>
                   <input value="" type="time" class="form-control" name='auction_start_time_edit_listing' id="auction_start_time_edit_listing" >
                   <small>Leave blank to start immediately</small>
               </div>
               <div class="col-md-12 no-left-right-padding">
                   <div class="col-md-4 form-group no-left-padding ">

                       <label for="auctionStartDate">Auction End Date / Time </label>
                       <input value="" type="date" class="form-control" name='auction_enddate_edit_listing' id="auction_enddate_edit_listing" >
                       <small>Leave blank to start immediately</small>
                   </div>
                   <div class="col-md-4 form-group  ">

                       <label for="auctionStartDate">&nbsp </label>
                       <input value="" type="time" class="form-control" name='auction_endtime_edit_listing' id="auction_endtime_edit_listing" >
                       <small>Leave blank to start immediately</small>
                   </div>
                   <div class="col-md-4 form-group  ">
                       <label for="status" style="margin-left: -323px;">&nbsp Status </label>
                       <div class="btn-group col-md-6" id="toggle_auction_status_for_edit_listing"  style="top:25px">
                           <button type="button" class="btn btn-info locked_active" style="width:auto!important;">OFF</button>
                           <button type="button" class="btn btn-default unlocked_inactive">ON</button>
                       </div>
                       <div class="col-md-6 alert alert-danger" id="switch_status_edit_listing" style="padding: 9px;top: 26px;">Auction not Active.</div>
                   </div>
               </div>

           </div>

        </div>
            </div>
              
    </div>

    <div class="col-md-12 top-padding" >
        <div class="col-md-offset-2 col-md-2">
            <button id="update_listing_btn" class="btn btn-primary blue-bg pull-right ">Update All</button>
        </div>
        <div class="col-md-1 ">
            <a href=""  data-toggle="tab"  class="btn white-font black-bg " id="cancelEditListing">Cancel</a>
        </div>
        <div class="col-md-2">
            <a href="#setAuction" role="tab" data-toggle="tab" class='tab-links btn btn-success set-auction-for-edit setAuctionButton' type="button" >Set as Auction</a>
            <a href="#previewAuctionEditForListing" role="tab" data-toggle="tab" class='tab-links btn btn-success previewAuctionButtonForEditListing' style="display: none;" type="button" >Preview Auction</a>
        </div>

    </div>

</div>




