<h3>Listings Overview</h3>
<table class="table table-striped medium-font" >
    <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th> <th>Item listing</th> <th>Price</th> <th>Date Added</th> <th></th> </tr> </thead>
    <tbody>
    <!-- product items listings starts here-->

    <?php  foreach($product_listing_overview_data as $product_items):?>

    <tr>
        <td></td>
        <td><img class="img-thumbnail" src="<?=$product_items['image'];?>" height="50" width="50"/><a href="#editLisiting" data-toggle="tab" class='tab-links editlistingsProduct' data-id="<?php echo $product_items['product_id'];?>" id="<?php echo $product_items['product_id'];?>"><?=$product_items['name'];?></a></td>
        <td>

            <p class="medium-font no-margin"><?php echo money_format('%(#10n', $product_items['price']);?></p>

        <td>
            <p class="no-margin"><?=date('h:i A', strtotime($product_items['added_date']));?></p>
            <p  class="less-medium-font no-margin"> <?= $newDateTime = date('h:i A', strtotime($product_items['added_date']));  ;?></p>

        </td>

        <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
    </tr>

   <?php endforeach;?>


    </tbody> </table>
<div class="col-md-12 view-more-border">
    <div class="col-md-5 col-md-offset-3"><a class="underline dark-grey-font"><p>View all Listings <span class="fa fa-caret-down"></span></p></a></div>
</div>
