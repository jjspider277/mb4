<div class="col-md-12" id="storesOverviewTable">
    <div class="col-md-4 no-left-padding">
        <h3>Stores Overview </h3>
    </div>
    <div class="col-md-4 col-md-offset-4 no-right-padding">
        <a href="#addStore" type="button" data-toggle="tab" class='btn btn-primary blue-bg pull-right'
           id="addToStoreListing">Create Store</a>
    </div>
    <table class="table table-striped medium-font col-md-6">
        <thead class="black-thead" style="font-size: 15px;">
        <tr>
            <th></th>
            <th>Store</th>
            <th>Item listing</th>
            <th>Account Owner</th>
            <th>Date Added</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <!-- store items listing starts here-->
        <?php foreach ($store_overview_data as $store_items): ?>


            <tr>
                <td></td>
                <td>
                    <img class="img-thumbnail" src="<?php echo $store_items->media->file_name; ?>" height="50"
                         width="50"/>
                    <a href="#editStore" data-toggle="tab" class='tab-links editlistingsStore' data-id="<?php echo $store_items->store->store_id;?>" id="<?php echo $store_items->store->store_id;?>"><?php echo $store_items->store->name; ?></a>


                </td>

                <td>
                    <p class="no-margin"><?php echo  $store_items->store->number_of_products; ?></p>

                </td>
                <td>
                    <p class="no-margin"><?php echo $store_items->profile->full_name; ?></p>

                    <p class="small no-margin"><?php echo $store_items->profile->email; ?></p>
                </td>
                <td>
                    <p class="no-margin"><?php echo $store_items->store->created_date; ?></p>

                    <p class="less-medium-font no-margin"><?php echo date('h i:A', strtotime($store_items->store->created_date)); ?></p>

                </td>

                <td>
                    <button class="btn"><span class="glyphicon glyphicon-trash"></span></button>
                </td>

            </tr>

        <?php endforeach; ?>

        </tbody>
    </table>
    <div class="col-md-12 view-more-border">
        <div class="col-md-4 col-md-offset-4"><a class="underline dark-grey-font text-center"><p>View all Stores<span
                        class="fa fa-caret-down"></span></p></a></div>
    </div>
</div>

<div class="tab-content " >
    <div role="tabpanel" class="tab-pane " id="editStore">
        <h3>Edit Store</h3>

        <div class="col-md-12 no-left-right-padding white-bg">

        <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_store_view/store_page_new.php'); ?>
        <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_store_view/addproduct_new.php'); ?>
        <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_store_view/getpaid_new.php'); ?>
        <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_store_view/preview_store_new.php'); ?>

        </div>

    </div>
    <div role="tabpanel" class="tab-pane " id="addStore">
        <h3>Add Store</h3>

        <div class="col-md-12 no-left-right-padding white-bg">
            <?php $this->load->view('dashboard/dashboard_main_tab_view/add_store_view/store_page_new.php'); ?>
            <?php $this->load->view('dashboard/dashboard_main_tab_view/add_store_view/addproduct_new.php'); ?>
            <?php $this->load->view('dashboard/dashboard_main_tab_view/add_store_view/getpaid_new.php'); ?>
            <?php $this->load->view('dashboard/dashboard_main_tab_view/add_store_view/preview_store_new.php'); ?>
        </div>

    </div>
</div>

