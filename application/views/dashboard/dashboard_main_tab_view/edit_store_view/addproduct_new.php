<style type="text/css">
    .ico-picedit-checkmark {
      font-size: xx-large;
      color:blue;
    }
    .ico-picedit-close {
        font-size: xx-large;
        color:blue;
    }
    .picedit_drag_resize_box_corner {
        font-size: xx-large;
        color:blue;
    }
</style>
<div class="col-md-12 col-md-store-content">
    <h4 class="blue-font">Step 2.Edit Your Products</h4>
    <div class=' col-md-12 grey-bg'>
        
        <div class="col-md-12 " style="margin-top: 2%;padding: 10px 26px;">
           

             <div class="form-group row col-md-4">
                    <span class='required_star'>*</span>
                    <label class='control-lable' for="product_name_edit_store">Product </label>
                    <input value="<?php echo set_value('product_name');?>" type="text" id='product_name_edit_store' name="productNameEditStore" class='form-control' placeholder="Product Name"
                    length="20"  style="margin-top: 13px;" required>
                      
                </div>

                <div class="form-group row col-md-12 ">
                    <span class='required_star'>*</span>
                    <label class='control-lable' for="product_description">Add product description</label>
                    <textarea class="form-control" rows="4" id='product_description_edit_store' name="product_description_edit_store" required><?php echo set_value('product_descritpion');?></textarea>
                </div>



                <div class="col-md-4 col-md-select" >
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="category_edit_store">Select Category</label>

                        <select class="form-control" id="category_edit_store" name="category_edit_store" >
                            <?php foreach ($sub_parent_categories as $category) { ?>
                                <option value="<?=$category->category?>"><?=$category->category;?></option>
                            <?php }?>
                        </select>
                     
                    </div>
                </div>
                 <div class="form-group col-md-4">
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="color_variation">Color</label>
                       

                      <?php  
                         //var_dump($catagories);
                       -
                          $colors['#'] = 'Please Select color';
                         ?>

                          <select class="form-control  " id="colors_edit_store" name='colorsEditStore' style="background-color:rgba(51, 48, 48, 0.51);font-weight:bold;">
                                <?php foreach ($colors as $key => $value) { ?>
                                   <option value="<?=$value?>" style='font-weight:bold;border 1px solid;color:<?=$value;?>'><?=$value;?></option>
                                <?php }?>
                        </select>
                    </div>
                </div>
                 <div class="form-group col-md-4 ">
                    <div class="form-group ">
                        <span class='required_star'>*</span>
                        <label for="color_variation">Size</label>
                       

                      <?php  
                         //var_dump($catagories);
                       
                         $sizes['#'] = 'Please Select size';
                          //array_push($sizes,array('small'=>'samll','medium'=>'medium','large'=>'large','x-large'=>'x-large','x-small'=>'x-small'));
                          echo form_dropdown('sizes', $sizes,'#','id="sizes_edit_store" name="sizesEditStore" class="form-control"'); ?>
                    </div>
                </div>
               

                <!-- <div class="col-md-4 col-md-select">
                    <div class="form-group">
                       
                        <label for="sub_variation">Sub Variation</label>
                       
                          <?php  $sub_variation['#'] = 'Please Select';
                         echo form_dropdown('sub_variation',$sub_variation,'#',
                         'id="sub_variation_edit_store"','name="subVariationEditStore"',' class="form-control"'); ?>
                    </div>
                </div> -->

                <div class="form-group row col-md-12 has-feedback">                                


                    <label for="preview_produt_image">Add Photos</label>
               
                   <style type="text/css">
                    
                    .col-md-3 {
                     width: 20%;
                     padding-right: 2px;
                     padding-left: 2px;
                    }
                    
                   .col-sm-6 .col-md-3 {
                     padding-right: -3px; 
                     padding-left: 10px;
                     width: 20%;
                     padding-right: 4px;
                      padding-left: 4px;
                   }
                
                
                   </style>

                   <div class="row"> 
                   <div class="col-sm-6 col-md-3" >
                       <div class="thumbnail" style="padding-top: 70px;">
                           <img id = 'img1_edit_store' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                    <div class="caption" style="padding-left: 19px !important"> 
                            <button id="img1_btn_edit_store" data-imageid="img1_edit_store" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload" >
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="img1_btn_edit_edit_store" type="button" class="btn btn-default btn-lg no-border" data-imageid="img1_edit_store" >
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg no-border">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                     </div> 
                     </div> 
                     </div> 
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail" style="padding-top: 70px;">
                                <img id = 'img2_edit_store' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                    <div class="caption" style="padding-left: 19px !important"> 
                           <button id="img2_btn_edit_store" data-imageid="img2_edit_store" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="img2_btn_edit_edit_store" type="button" class="btn btn-default btn-lg no-border" data-imageid="img2" data-toggle="modal" data-target="#imageUploadEdit">
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg no-border">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                     </div> 
                     </div> 
                     </div>
                  <div class="col-sm-6 col-md-3">
                      <div class="thumbnail" style="padding-top: 70px;">
                          <img id = 'img3_edit_store' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                    <div class="caption" style="padding-left: 19px !important"> 
                        <button id="img3_btn_edit_store" data-imageid="img3_edit_store" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img3_btn_edit_edit_store" type="button" class="btn btn-default btn-lg no-border" data-imageid="img3_edit_store" data-toggle="modal" data-target="#imageUploadEdit">
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg no-border">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                     </div> 
                     </div> 
                     </div>
                     <div class="col-sm-6 col-md-3">
                         <div class="thumbnail" style="padding-top: 70px;">
                             <img id = 'img4_edit_store' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                     <div class="caption" style="padding-left: 19px !important">
                         <button id="img4_btn_edit_store" data-imageid="img4_edit_store" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img4_btn_edit_edit_store" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4_edit_store" >
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg no-border">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                     </div>

                      </div> </div> 
                     <div class="col-sm-6 col-md-3">
                         <div class="thumbnail" style="padding-top: 70px;">
                             <img id = 'img5_edit_store' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                       <div class="caption" style="padding-left: 19px !important">
                        <button id="img5_btn_edit_store" data-imageid="img5_edit_store" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                          <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                        </button>
                        <button id="img5_btn_edit_edit_store" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4_edit_store">
                          <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-lg no-border">
                          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        </button>
                      </div>


                       </div> </div> </div>

                    <label class='product_validation_message black-font'>Upload clear and good quality pictures</label>
                    <p class='product_validation_message grey-font'>
                    <span class='required_star '>*</span>
                    At least one image is manadatory</p>
                </div>

                 <div class="col-md-4 form-group no-left-padding ">
                        <span class='required_star'>*</span>
                        <label for="quantity_edit_store">Quantity</label>
                       <input  type="number" class="form-control" name='quantity_edit_store' id="quantity_edit_store" required>
                </div>
                <div class="col-md-4 form-group no-left-padding ">
                        
                        <label for="price_edit_store">Shipping Price</label>
                       <input   type="text" class="form-control" name='shipping_price_edit_store' id="shipping_price_edit_store" required>
                </div>
                <div class="col-md-4 form-group no-left-padding ">
                       
                        <label for="method_edit_store">Shipping Method</label>
                      <select id="method_edit_store" name="method_edit_store" class="form-control select-drop-down-arrow" >
                      	<option>FedEx</option>
                      	<option>UPS</option>
                      	<option>DHL</option>
                      </select>
                </div>

                 

                    <div class="col-md-4 form-group no-left-padding">
                        <span class='required_star'>*</span>
                        <label for="product_descritpion_edit_store">Price Before Tax</label>
                       <input type="number" class="form-control" name='price_before_tax_edit_store' id="price_before_tax_edit_store" required>
                   </div>

                <div class="col-md-4 form-group no-left-padding ">
                        
                        <label for="sprice_edit_store">Special Price </label>
                       <input type="text" class="form-control" name='sprice_edit_store' id="sprice_edit_store" required>
                       
                </div>
                       
            
             
          
        </div>
         <hr  class='hr_store_form'>
              
    </div>

</div>




