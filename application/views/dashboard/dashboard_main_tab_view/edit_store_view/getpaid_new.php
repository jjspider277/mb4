<style type="text/css">
</style>
<div class="col-md-12 col-md-store-content">
    <h4 class="blue-font">Step 3.Get Paid</h4>
   

    <div class=' col-md-12  grey-bg' style="padding-top: 20px;">
        <div class='col-md-12' style="padding-bottom:10px;">
            <div class="col-md-12 get-paid-container">
             
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="account_type_edit_store">Account Type</label>
                          <?= form_dropdown('account_type_edit_store', array_merge(array('#'=>'Please select account type'), $account_types) ,'#','id="account_type_edit_store" class="form-control select-drop-down-arrow"  tabindex="7"','id="account_type_edit_store"','name="accountTypeEditStore' ) ;?>
                    </div>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="descritpion">Bank Branch</label>
                        <input  type="text" id="bankbranch_edit_store" name='bankbranch_edit_store' class="form-control" required>
                    </div>

                    <div class="col-md-12 no-left-right-padding">
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="ccount_owner_edit_store">Account Owners Name</label>
                        <input  type="text" id="account_owner_edit_store" name='account_owner_edit_store' class="form-control" required>
                        <p class="help-block">same as your bank book account .</p>
                    </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <h3>Pic of Check</h3>
                        <img src="<?php echo base_url('assets/images/storesetup/check.jpg');?>">
                    </div>


                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="routing_number_edit_store">Rounting Number </label>
                        <input type="text" id="routing_number_edit_store" name='routing_number_edit_store' class="form-control col-md-6" required>
                    </div>
                    

                    <div class='col-md-12 no-left-right-padding'>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="account_number_edit_store">Account Number</label>
                        <input type="text" id="account_number_edit_store" name='account_number_edit_store' class="form-control" required>
                    </div>
                    <div class="col-md-4 form-group">
                        <span class='required_star'>*</span>
                        <label for="re_account_number_edit_store">Re-enter Account Number</label>
                        <input type="text" id="re_account_number_edit_store" name='re_account_number_edit_store' class="form-control" required>

                    </div>  
                    </div>   

                </div>

    
        </div>
         <hr  class='hr_store_form'>
            
         
    </div>

</div>