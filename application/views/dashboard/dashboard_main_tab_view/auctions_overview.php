<div class="col-sm-12 no-left-right-padding">
<h3>Auctions Overview</h3>
<table class="table table-striped medium-font" >
    <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th> <th>Auction Item</th> <th>Total # of Bids</th> <th>Starting Price</th> <th>Reserve Price</th><th>Buy Now Price</th><th>Auction Status<th></th> </tr> </thead>
    <tbody>
    <!--auction items listing starts here-->
        <?php foreach($auctions_overview_data as $auctions_item):?>
    <tr><td></td>
        <td><a href="#editAuctionForAuction"  data-toggle="tab" class='tab-links' id="productInAuction">
                <img class="img-thumbnail" src="<?php echo base_url().'uploads/profile/'.$auctions_item->profile_id.'/products/'.$auctions_item->file_name;?>" height="50" width="50" /><?= $auctions_item->name;?></a></td>
        <td>

            <p class="no-margin"><?=$auctions_item->total_bids ;?></p>


        </td>
        <td>
            <p class="no-margin"><?php echo money_format('%(#10n', $auctions_item->bid_price);?></p>
        </td>
        <td>
            <p class="no-margin"><?php echo money_format('%(#10n', $auctions_item->reserve_price);?></p>

        </td>
        <td>
            <p class="no-margin">$<?=$auctions_item->buy_now_price ;?></p>

        </td>
        <td class="btn-right-padding">
            <?php if($auctions_item->status==0):?>
                <button class="btn btn-success" >Live</button>
            <?php endif;?>

            <?php if($auctions_item->status==1):?>
                <button class="btn btn-primary" >Closed</button>
            <?php endif;?>

            </td>

        <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
    </tr>

    <?php endforeach;?>

    </tbody> </table>
        <div class="col-md-12 view-more-border">
            <div class="col-md-2 col-md-offset-5"><a class="underline dark-grey-font"><p>View all Auctions <span class="fa fa-caret-down"></span></p></a></div>
        </div>

    </div>