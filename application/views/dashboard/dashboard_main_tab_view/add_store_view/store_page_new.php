<style type="text/css">
.validation-text {

 color: #737373;

 margin-top: 5px;
 margin-bottom: 10px;
 margin-left: -87px;
}
.shipping-col {
    margin-left: 4%;
}
</style>   

<div class="col-md-12 col-md-store-content">

 
       <!-- my store setup begin-->
           
              <!--ERROR MESSAGE-->
              <style type="text/css">
              .error-message {
                margin-top: 1%;
         
              }
              </style>

              <div class="error-message col-md-12">  
                <?php //$this->load->view($show_error_page,$message);?>
              </div>
      <!--END OF ERROR MESSAGE-->

    <?php

    $attributes = array('novalidate class' => 'form', 'novalidate' => 'novalidate', 'id' => 'storeForm', 'name' => 'storeForm',);
    $form_post_id= isset($profile_id) ? $profile_id : $profile->id ;
    echo form_open_multipart('store/save_store/'.$form_post_id, $attributes);
    ?>

   <h4 class="blue-font">Step 1. Name Your Store</h4>
    <p class="col-md-12 panel no-border no-left-padding" style="box-shadow: none;">Your store name appears in the mbu4u store community. Pick a name that relates to you or helps identify whats in your store.</p>
    <div class=' col-md-12 grey-bg'>


     

        <div class='col-md-4 noleft-padding'>
            <div class="col-md-12" style="margin-top:5%;">                                       

                <div class="form-group">

                   <div class="col-sm-8 col-md-12" > 
                   <div class="thumbnail">
                   <img id = 'store_img' name="store_img" data-src="holder.js/100%x200" alt="100%x200" src="<?php echo base_url('uploads/no-photo.jpg');?>" data-holder-rendered="true" style="height: 258px; width: 100%; display: block;">
                    <div class="caption" style="padding-left: 53px;">
                            <button id="store_img_btn" data-imageid="store_img" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload" >
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="store_img_edit_btn" data-imageid="store_img" type="button" class="btn btn-default btn-lg no-border">
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg no-border no-border">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                     </div> 
                     </div> 
                     </div> 
                    
                </div>

               
                        
            </div>

        </div>

        <div class='col-md-7 no-left-padding' style="margin-top:2%;">
         <div class="col-md-12 col-md-select"  style="margin-top: 13px;">
                    <div class="form-group no-left-right-padding">

                        <span class='required_star'>*</span>
                        <label for="storename">Your Store Name</label>
                        <input type="text" id="storename" name='storename' value="<?php echo set_value('storename');?>" id='storename' length="10" class="form-control" required >
                        <p class="help-block">You can change your store name later.</p>
                    </div>


                </div>

            <div class="form-group no-left-right-padding">
                <span class='required_star'>*</span><label for="store_descritpion">Your Store Description</label>
                <textarea class="form-control" name='store_description' id='store_description' rows="7"><?php echo set_value('store_description');?> </textarea>
            </div>

        </div>
        
          <!--edit shipping address information-div with row -->   

            <!--end of edit shipping address information-div with row -->   

        <hr  class='hr_store_form'>
       
       


    </div>



    </div>      
 

