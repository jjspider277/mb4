<style type="text/css">
    .ico-picedit-checkmark {
        font-size: xx-large;
        color:blue;
    }
    .ico-picedit-close {
        font-size: xx-large;
        color:blue;
    }
    .picedit_drag_resize_box_corner {
        font-size: xx-large;
        color:blue;
    }
    .blue-border{
        padding:0px !important
    }
</style>
<div class="col-md-12 col-md-store-content" style="padding:10px;">

    <div class='col-md-12 blue-border no-left-right-padding'>

        <div id="add_listing_div" class="col-md-12 " style="margin-top: 2%;padding: 10px 26px;">

            <div class="form-group col-md-4">
                <span class='required_star'>*</span>
                <label class='control-lable' for="product_name">Product </label>
                <input type="text" id='product_name_add_listing' name="product_name_add_listing" class='form-control' placeholder="Product Name"
                       length="20"  style="margin-top: 13px;" required>

            </div>
            <div class="form-group col-md-4 col-md-offset-1">
                <span class='required_star'>*</span>
                <label class='control-lable' for="stores">Select a Store </label>
                <?php
                $stores['#'] = 'Please Select Store';
                echo form_dropdown('stores', $stores,'#','id="store_add_listing"
                          class="form-control"'); ?>
            </div>

            <div class="form-group row col-md-12 ">
                <span class='required_star'>*</span>
                <label class='control-lable' for="product_descritpion_add_listing">Add product description</label>
                <textarea class="form-control" rows="4" id='product_description_add_listing' name="product_description_add_listing" required><?php echo set_value('product_descritpion');?></textarea>
            </div>



            <div class="col-md-4 col-md-select" >
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="category">Select Category</label>


                    <?php
                    //var_dump($catagories);
                    $catagories['#'] = 'Please Select Category';
                    echo form_dropdown('categories', $catagories,'#','id="category_add_listing"
                          class="form-control "'); ?>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="color_variation">Color</label>


                    <?php
                    //var_dump($catagories);
                    -
                    $colors['#'] = 'Please Select color';
                    ?>

                    <select class="form-control select-drop-down-arrow" id="colors_add_listing" name='colors_add_listing' style="background-color:rgba(51, 48, 48, 0.51);font-weight:bold;">
                        <?php foreach ($colors as $key => $value) { ?>
                            <option value="<?=$value?>" style='font-weight:bold;border 1px solid;color:<?=$value;?>'><?=$value;?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-4 ">
                <div class="form-group ">
                    <span class='required_star'>*</span>
                    <label for="color_variation">Size</label>


                    <?php
                    //var_dump($catagories);

                    $sizes['#'] = 'Please Select size';
                    //array_push($sizes,array('small'=>'samll','medium'=>'medium','large'=>'large','x-large'=>'x-large','x-small'=>'x-small'));
                    echo form_dropdown('sizes', $sizes,'#','id="sizes_add_listing"
                          class="form-control "'); ?>
                </div>
            </div>


            <!-- <div class="col-md-4 col-md-select">
                    <div class="form-group">

                        <label for="sub_variation">Sub Variation</label>

                          <?php  $sub_variation['#'] = 'Please Select';
            echo form_dropdown('sub_variation',$sub_variation,'#',
                'id="sub_variation" class="form-control"'); ?>
                    </div>
                </div> -->

            <div class="form-group row col-md-12 has-feedback">


                <label for="preview_produt_image">Add Photos</label><br/>
                <label class='product_validation_message black-font'>Upload clear and good quality pictures</label>
                <p class='product_validation_message grey-font'>
                    <span class='required_star '>*</span>
                    At least one image is mandatory</p>

                <style type="text/css">

                    .col-md-3 {
                        width: 20%;
                        padding-right: 2px;
                        padding-left: 2px;
                    }

                    .col-sm-6 .col-md-3 {
                        padding-right: -3px;
                        padding-left: 10px;
                        width: 20%;
                        padding-right: 4px;
                        padding-left: 4px;
                    }


                </style>

                <div class="row">
                    <div class="col-sm-6 col-md-3" >
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img1_add_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img1_btn" data-imageid="img1_add_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload" >
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img1_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img1" >
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img2_add_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img2_btn" data-imageid="img2_add_listing"type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img2_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img2" data-toggle="modal" data-target="#imageUploadEdit">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img3_add_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img3_btn" data-imageid="img3_add_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img3_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img3" data-toggle="modal" data-target="#imageUploadEdit">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img4_add_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img4_btn" data-imageid="img4_add_listing" type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img4_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4" >
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>

                        </div> </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" style="padding-top: 70px;">
                            <img id = 'img5_add_listing' src="<?php echo base_url('assets/images/image-upload.jpg');?>" data-holder-rendered="true" style="height: 104px;  display: block; ">
                            <div class="caption" style="padding-left: 19px !important">
                                <button id="img5_btn" data-imageid="img5_add_listing"type="button" class="btn btn-default btn-lg no-border" data-toggle="modal" data-target="#imageUpload">
                                    <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                </button>
                                <button id="img5_btn_edit" type="button" class="btn btn-default btn-lg no-border" data-imageid="img4">
                                    <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-lg no-border">
                                    <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>


                        </div> </div> </div>


            </div>

            <div class="col-md-4 form-group no-left-padding ">
                <span class='required_star'>*</span>
                <label for="descritpion">Quantity</label>
                <input type="number" class="form-control" name='quantity_add_listing' id="quantity_add_listing" required>
            </div>
            <div class="col-md-4 form-group no-left-padding ">

                <label for="price">Shipping Price</label>
                <input   type="text" class="form-control" name='shipping_price_add_listing' id="shipping_price_add_listing" required>
            </div>
            <div class="col-md-4 form-group no-left-padding ">

                <label for="method">Shipping Method</label>
                <select id="shipping_method_add_listing" class="form-control" >
                    <option>FedEx</option>
                    <option>UPS</option>
                    <option>DHL</option>
                </select>
            </div>





            <div class="col-md-4 form-group no-left-padding ">

                <label for="sprice">Selling Price </label>
                <input value="" type="text" class="form-control" name='price_add_listing' id="price_add_listing" required>

            </div>




        </div>
        <hr  class='hr_store_form'>
        <div  class="tab-content col-md-12 no-left-right-padding" >
            <div role="tabpanel" class="tab-pane fade" id="setAuctionForAddListing">
                <div class="col-md-12 light-green-bg">
                    <h3>Set Item as Auction</h3>
                    <div class="col-md-4 form-group no-left-padding ">

                        <label for="nameOfAuction">Name of Auction </label>
                        <input value="" type="text" class="form-control" name='name_of_auction_add_listing' id="name_of_auction_add_listing" >

                    </div>
                    <div class="col-md-4 form-group ">

                        <label for="startingBidPrice">Starting Bid Price </label>
                        <input value="" type="text" class="form-control" name='starting_price_bid_add_listing' id="starting_price_bid_add_listing" >

                    </div>
                    <div class="col-md-4 form-group ">

                        <label for="reservePrice">Reserve Price </label>
                        <input value="" type="text" class="form-control" name='reserve_price_bid_add_listing' id="reserve_price_bid_add_listing" >

                    </div>
                    <div class="col-md-4 form-group no-left-padding ">

                        <label for="auctionStartDate">Auction Start Date / Time </label>
                        <input  type="date" class="form-control" name='auction_startdate_add_listing' id="auction_startdate_add_listing"" >
                        <small>Leave blank to start immediately</small>

                    </div>
                    <div class="col-md-4 form-group  ">

                        <label for="auctionStartDate">&nbsp </label>
                        <input value="" type="time" class="form-control" name='auction_start_time_add_listing' id="auction_start_time_add_listing" >
                        <small>Leave blank to start immediately</small>
                    </div>
                    <div class="col-md-12 no-left-right-padding">
                        <div class="col-md-4 form-group no-left-padding ">

                            <label for="auctionStartDate">Auction End Date / Time </label>
                            <input value="" type="date" class="form-control" name='auction_end_date_add_listing' id="auction_end_date_add_listing" >
                            <small>Leave blank to start immediately</small>
                        </div>
                        <div class="col-md-4 form-group  ">

                            <label for="auctionStartDate">&nbsp </label>
                            <input value="" type="time" class="form-control" name='auction_end_time_add_listing' id="auction_end_time_add_listing" >
                            <small>Leave blank to start immediately</small>
                        </div>
                        <div class="col-md-4 form-group  ">
                            <label for="status" style="margin-left: -323px;">&nbsp Status </label>
                            <div class="btn-group col-md-6" id="toggle_auction_status_for_add_listing"  style="top:25px">
                                <button type="button" class="btn btn-info locked_active">OFF</button>
                                <button type="button" class="btn btn-default unlocked_inactive">ON</button>
                            </div>
                            <div class="col-md-6 alert alert-danger" id="switch_status_add_listing" style="padding: 9px;top: 26px;">Auction not Active.</div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="col-md-12 top-padding" >
        <div class="col-md-offset-2 col-md-2">
            <a herf="#" id="submit_add_listing" class="btn btn-primary blue-bg pull-right ">Create Listing</a>
        </div>
        <div class="col-md-1 ">
            <a href=""  data-toggle="tab"  class="btn white-font black-bg " id="cancelAddListing">Cancel</a>
        </div>
        <div class="col-md-2">
            <a href="#" id='info-set-auctions' class='tab-links btn btn-success' type="button">Set Auction</a>
            <a href="#setAuctionForAddListing" role="tab" data-toggle="tab" class='tab-links btn btn-success set-auction-for-add setAuctionButtonForAddListing' type="button" >Set as Auction</a>
            <a href="#previewAuctionAddForListing" role="tab" data-toggle="tab" class='tab-links btn btn-success previewAuctionButtonForAddListing' style="display: none;" type="button" >Preview Auction</a>
        </div>

    </div>

</div>




