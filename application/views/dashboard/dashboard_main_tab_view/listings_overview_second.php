<div class="col-md-12" id="listingOverviewTable">
    <div class="col-md-4 no-left-padding">
        <h3>Listings Overview</h3>
    </div>
    <div class="col-md-4 col-md-offset-4 no-right-padding">
        <a href="#addListing" data-toggle="tab" class='btn btn-primary blue-bg pull-right' id="addListingToStore">Create a Listing</a>
    </div>
    <table class="table table-striped medium-font col-md-6"  >
        <thead class="black-thead" style="font-size: 15px;"> <tr><th> </th><th>Item listing</th> <th>Item Price</th><th>Store Name</th><th>Date Added</th> <th></th> </tr> </thead>
        <tbody>

        <!-- product items listing starts here-->

        <?php foreach($product_listing_overview_data as $product_items):?>



            <tr><td></td>
                <td><img  class="img-thumbnail" src="<?=$product_items['image'];?>"  height="50" width="50"/>
                    <a href="#editListing" href="#editStore" data-toggle="tab" class='tab-links editLisitingProducts' data-id="<?php echo $product_items['product_id'];?>" id="<?php echo $product_items['product_id'];?>" ><?php echo $product_items['name'];?></a></td>


                <td>
                    <p class="no-margin"><?php echo money_format('%(#10n', $product_items['price']);?></p>


                </td>
                <td> <p  class="less-medium-font no-margin">Store Name</p> </td>

                <td>
                    <p class="no-margin"><?=date('h:i A', strtotime($product_items['added_date']));?></p>
                    <p  class="less-medium-font no-margin"> <?= $newDateTime = date('h:i A', strtotime($product_items['added_date']));  ;?></p>


                </td>


                <td><button class="btn" ><span class="glyphicon glyphicon-trash"></span> </button></td>
            </tr>
        <?php endforeach;?>

        </tbody> </table>
    <div class="col-md-12 view-more-border">
        <div class="col-md-4 col-md-offset-4"><a class="underline dark-grey-font text-center"><p>View all Listings<span class="fa fa-caret-down"></span></p></a></div>
    </div>
</div>

<div  class="tab-content container " >
    <div role="tabpanel" class="tab-pane" id="editListing">
        <h3>Edit Listing</h3>
        <div class="col-md-12 no-left-right-padding white-bg">

            <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_listing_view/edit_product_new.php');?>

        </div>

    </div>
    <div role="tabpanel" class="tab-pane" id="previewAuctionEditForListing">

        <?php $this->load->view('dashboard/dashboard_main_tab_view/edit_listing_view/auction_preview');?>

    </div>
    <div role="tabpanel" class="tab-pane " id="addListing">
        <h3>Add Listing</h3>
        <div class="col-md-12 no-left-right-padding white-bg">

            <?php $this->load->view('dashboard/dashboard_main_tab_view/add_listing_view/addproduct_new.php');?>

        </div>

    </div>
    <div role="tabpanel" class="tab-pane  " id="previewAuctionAddForListing">

        <?php $this->load->view('dashboard/dashboard_main_tab_view/add_listing_view/auction_preview');?>

    </div>

</div>
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
