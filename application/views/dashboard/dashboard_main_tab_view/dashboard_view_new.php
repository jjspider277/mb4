<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bid.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">

    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/seller.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/slimbox2.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/picedit/css/font-css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/picedit/css/picedit.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/jquery/css/colorPicker.css";?> rel="stylesheet">


</head>


<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>
    <?php $this->load->view($header_logo_white); ?>
    <!--load tab menu here -->
    <div class="row row-menu">
        <div class="container tab-menu-container">

            <div class="tab-menu col-md-12" id="tabs">
                <a href="#dashboard" class="selected tab-dashboard-link " aria-controls="listing" role="tab" data-toggle="tab" ><span id="dashboardImg" class="active"><i class="fa fa-tachometer"></i> DashBoard</a></span>
                <a href="#orders"data-toggle="tab"  class='tab-links'><span id="orderImg" class="span-right-padding"></span><span>My Orders</a></span>
                <a href="#purchases" data-toggle="tab" class='tab-links'><span id="purchaseImg" class="span-right-padding"></span><span>My Purchases </a></span>
                <a href="#stores"  data-toggle="tab" class='tab-links'><span id="storeImg" class="span-right-padding"></span><span>My Stores</a></span>
                <a href="#listings"  data-toggle="tab" class='tab-links'><span id="listingsImg" class="span-right-padding"></span><span>My Listings</a></span>
                <a href="#auctions"  data-toggle="tab" class='tab-links'><span id="auctionsImg" class="span-right-padding"></span><span>My Auctions</a></span>


            </div>

        </div>


    </div>


</header>
<!-- Responsive design
================================================== -->

<section id="responsive" style="background-color:#f5f5f5;">



    



    <?php $this->load->view('dashboard/dashboard_main_tab_view/dashboard_tab_content_new.php');?>



</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="<?php echo base_url()."assets/js/global_search.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/jscolor.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js"?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js"?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
<script src=<?php echo base_url()."assets/plugins/picedit/picedit.js";?>> </script>
<script src="<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.js";?>"></script>
<script src="<?php echo base_url()."assets/js/add_image_dialog_functions.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/jquery/colorPicker.js"?>"></script>

<!--
<script type="text/javascript" src="assets/plugins/bootstrap/js/tooltip.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-rating.min.js"></script>
-->

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    /***global base url path in javascript***/
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split('/');
    var base_url_complete = base_url+'/'+pathArray[1]+'/';
    var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;
    var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";
    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);


    /*    function rate_item($rating_input_id,$rating_value) {
     alert('in');
     $("#"+$rating_input_id).rating('rate', $rating_value);
     }

     function get_rating_value($rating_input_id,$to_rated_item_id) {

     save_rating($rating_input_id,"products",$to_rated_item_id);
     }


     $(function () {


     $('#programmatically-rating43').click(function () {

     $('#programmatically-rating43').rating('rate', $('#programmatically-value3').val());
     });
     $('#programmatically-rating43').change(function () {
     alert($('#programmatically-rating43').rating('rate'));
     });

     $('.rating-tooltip').rating({
     extendSymbol: function (rate) {
     $(this).tooltip({
     container: 'body',
     placement: 'bottom',
     title: 'Rate: ' + rate
     });
     }
     });



     });*/


</script>
<script>

    $(function(){

        var menu = $('.tab-menu');

        // Mark the clicked item as selected

        menu.on('click', 'a', function(){
            var a = $(this);

            a.siblings().removeClass('selected');
            a.addClass('selected');
        });
    });
</script>

<script type="text/javascript">

    $('#display-search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });
    $('#search-header').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });

    $('#home_page_search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });



</script>
<script type="text/javascript">
    $(".colorPicker").colorPicker();
    $(document).ready(function() {
        $("#tabs a").click(function(){
            $("#tabs span").removeClass("active");
            $("span",this).addClass("active");
        });

    });
</script>
<script>


        $("#editlistingsStore").click(function(){
            $("#storesOverviewTable").hide("slow");
        });
        $("#addToStoreListing").click(function(){
            $("#storesOverviewTable").hide("slow");
            $("#addStore").show("slow");
        });

        $("#productInStore").click(function(){
            $("#listingOverviewTable").hide("slow");
        });
        $("#addListingToStore").click(function(){
            $("#listingOverviewTable").hide("slow");
            $("#addListing").show("slow");
        });


        $("#cancelAuctionForAuction").click(function(){
            $("#auctionOverviewTable").show("slow");
            $("#editAuctionForAuction").hide("slow");
        });

        $("#cancelAddListing").click(function(){
            $("#listingOverviewTable").show("slow");
            $("#addListing").hide("slow");
        });
        $("#cancelEditListing").click(function(){
            $("#listingOverviewTable").show("slow");
            $("#editListing").hide("slow");
        });
        $("#cancelCreateStore").click(function(){
            $("#storesOverviewTable").show("slow");
            $("#addStore").hide("slow");
        });

        $("#cancelEditStore").click(function(){
            $("#storesOverviewTable").show("slow");
            $("#editStore").hide("slow");
        });

 </script>
<script>
$('.set-auction-for-edit').click(function(){

    $("#setAuction").css('display','block');
    var $this = $(this);
    $(this).css('display','none');
    $(".previewAuctionButtonForEditListing").css('display','block');
    //alert('jquery-clicked   ');

});
$('.previewAuctionButtonForEditListing').click(function(){
    //e.preventDefault();

    $("#setAuction").removeClass('active');
    $("#setAuction").css('display','none');
    $("#editListing").css('display','none');



});
    $('#returnToEditListing').click(function () {
        var $this = $(this);
        $('#editListing').show();
        $('#editListing').css('visibility','visible');
        $('#previewAuctionEditForListing').removeClass('active');
        $('#setAuction').addClass('active');
        $('#setAuction').css('display','block');

    });
$('.set-auction-for-add').click(function(){

    $("#setAuctionForAddListing").css('display','block');
    var $this = $(this);
    $(this).css('display','none');
    $(".previewAuctionButtonForAddListing").css('display','block');
    //alert('jquery-clicked   ');

});
$('.previewAuctionButtonForAddListing').click(function(){
    //e.preventDefault();

    $("#setAuctionForAddListing").removeClass('active');
    $("#setAuctionForAddListing").css('display','none');
    $("#addListing").css('display','none');
    //alert('call me whatever');
    previewAuction();
});

$('#returnToAddListing').click(function () {
    var $this = $(this);
    $('#addListing').show();
    $('#addListing').css('visibility','visible');
    $('#previewAuctionAddForListing').removeClass('active');
    $('#setAuctionForAddListing').addClass('active');
    $('#setAuctionForAddListing').css('display','block');

});
$('.previewAuctionButtonForEditAuction').click(function(){
    //e.preventDefault();
    $("#editAuctionForAuction").css('display','none');
   // console.log('bicth please');
});

$('#returnToEditAuction').click(function () {
    var $this = $(this);
    $('#editAuctionForAuction').show();
    $('#editAuctionForAuction').css('visibility','visible');
    $('#previewAuctionEditForAuction').removeClass('active');


});
</script>


<script type="text/javascript">

    //save all the lookup bullshits here

    $("#add-btn-category").click(function(){

        var $url = base_url_complete+'admin/save_category';
        var $category = $("#category_input").val();
        console.log($category);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'category_name': $category}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the category ($category) successfully saved!")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });


    });

    //save all the sub-categories

    $("#btn_save_subcategory").click(function(){

        var $url = base_url_complete+'admin/save_subcategory';
        var $sub_category = $("#subcategory_input").val();
        var $category_id = $("#category_list option:selected").val();
        console.log($sub_category);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'subcategory': $sub_category,'category_id':$category_id}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $(".showCategoryModal-sm").hide().delay(400);
                $("#message_display").html("<strong>Success!</strong> We have saved sub category ($subcategory) into category ($category) successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });

    });

    //save all the lookup bullshits here

    $("#add-btn-variation-color").click(function(){

        var $url = base_url_complete+'admin/save_variation_colors';
        var $variation_colors = $("#color").val();
        console.log($variation_colors);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'color': $variation_colors}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the color variation successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });


    });


    //save all the lookup bullshits here

    $("#add_btn_shipping_options").click(function(){

        var $url = base_url_complete+'admin/save_shipping_options';
        var $shipping_option = $("#shipping_input").val();
        console.log($shipping_option);
        $.ajax({
            url: $url,
            data: ({'madebyus4u_csrf_test_name':csrf_hash,'shipping_option': $shipping_option}),
            dataType: 'json',
            type: "post",
            success: function(data){
                console.log('success');
                $("#message_display").html("<strong>Success!</strong> We have saved the shipping option successfully !")
                $('#message_display').attr('style','display:block;').fadeOut(4000);
            }
        });


    });


</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){

       $(".set-auction-for-add").css('display','none');

        var product_id_auction = -1;
        var product_id_edit_lisiting = -1;


        $.getJSON(base_url_complete+'admin/get_all_categories','', function(data){

            var options = '';
            for (var x = 0; x < data.length; x++) {
                options += '<option value="' + data[x]['id'] + '">' + data[x]['value'] + '</option>';
            }
            $('#category_list').html(options); //AMAZING CODE

        });

    });
</script>

<?php $this->load->view('include/add_image_generic_dialog.html');?>

<!--*** BLACK Jesues ***/-->

<script>   //no need to specify the language

    $("#preview_edit_store_btn").click(function() {  // passing down the event
        //alert('console');
        $src = $('#store_img_edit').attr('src');
        $pname = $('#product_name_edit_store').val();
        $sprice = $('#price_before_tax_edit_store').val();
        $descritpion_paragraph = $('#product_description_edit_store').val();
        $('#product_preview_title_edit_store').html($pname);
        $('#descritpion_paragraph_edit_store').html($descritpion_paragraph);
        $('#price_tag_label_edit_store').html("<span id='preview_price_edit_store' class='grey-font'>$</span>"+$sprice+"&nbsp;<span class='blue-font'>USD</span>");
        $('#product_preview_image_edit').attr('src',$src);
        $('.preview_edit_store').slideToggle("fast");
        $("#preview_edit_store").val="Hide preview";
    });

    $("#preview_button").click(function() {  // passing down the event
        $src = $('#store_img').attr('src');
        $pname = $('#product_name').val();
        $sprice = $('#price').val();
        $description_paragraph = $('#product_descritpion').val();
        $('#product_preview_title').html($pname);

        $('#description_paragraph').html($descritpion_paragraph);
        $('#price_tag_label').html("<span id='preview_price' class='grey-font'>$</span>"+$sprice+"&nbsp;<span class='blue-font'>USD</span>");
        $('#product_preview_image').attr('src',$src);
        $('.preview_store').slideToggle("fast");
        $("#preview_store").val="Hide preview";
    });

    $(function(){
        $("#submit_Add_Store_Btn").click(function(e){  // passing down the event

           $storename =  $("#storename").val();
           $store_description =  $("#store_description").val();
           $product_name = $('#product_name').val();
           $product_description = $('#product_description').val();
           $quantity_add_store = $('#quantity_add_store').val();

           $price = $('#price').val();
           $sprice= $('#sprice').val();
           $bankbranch= $('#bankbranch').val();
           $account_owner = $('#account_owner').val();
           $routenumber = $('#routenumber').val();
           $accountnumber = $('#accountnumber').val();
           $reaccountnumber =$('#reaccountnumber').val();
           $category=$('#category :selected').text();
           $colors=$('#colors :selected').text();
           $sizes=$('#sizes :selected').text();
           $shipping_method = $('#shipping_method :selected').text();
           $account_type = $('#account_types :selected').text();
           $shipping_price = $('#shipping_price').val();

            $.ajax({
                url:base_url_complete+"dashboard/addStore",
                type: 'POST',
                data: {madebyus4u_csrf_test_name:csrf_hash,storename:$storename,
                    store_description:$store_description,product_name:$product_name,
                    product_description:$product_description,quantity:$quantity_add_store,
                    price:$price,sprice:$sprice,bankbranch:$bankbranch,
                    account_owner:$account_owner,routenumber:$routenumber,
                    accountnumber:$accountnumber,reaccountnumber:$reaccountnumber,
                    account_type:$account_type,categories:$category,colors:$colors,sizes:$sizes,
                    shipping_method:$shipping_method,shipping_price:$shipping_price},
                success: function(){
                    setTimeout(function() { sweetAlert("Good Job!", "Your Store has been Launched! ", "success");},1000);
                    $("#cancelCreateStore").click();
                    setTimeout(function(){location.reload(true)},3000);
                },
                error: function(){
                    setTimeout(function() { sweetAlert("Error", "Your Store has not been Launched! ", "error");},1000);
                }
            });
            e.preventDefault(); // could also use: return false;
        });
    });

    $(function(){
        $("#update_store_edit_btn").click(function(e){  // passing down the event

            $store_id =  $("#black_jezuez_id").val();
            $product_id =  $("#black_jezuez_product_id").val();
            $storename =  $("#store_name_edit_store").val();
            $store_description =  $("textarea#store_description_edit_store").val();
            $product_name = $('#product_name_edit_store').val();
            $product_description = $('textarea#product_description_edit_store').val();
            $quantity = $('#quantity_edit_store').val();
            $price = $('#price_before_tax_edit_store').val();
            $sprice= $('#sprice_edit_store').val();

            $bankbranch= $('#bankbranch_edit_store').val();
            $account_owner = $('#account_owner_edit_store').val();
            $routenumber = $('#routing_number_edit_store').val();
            $account_number = $('#account_number_edit_store').val();
            $reaccountnumber =$('#re_account_number_edit_store').val();
            $category=$('#category_edit_store :selected').text();
            $colors=$('#colors_edit_store :selected').text();
            $sizes=$('#sizes_edit_store :selected').text();
            $shipping_method = $('#method_edit_store :selected').text();
            $account_type = $('#account_type_edit_store :selected').text();
            $shipping_price = $('#shipping_price_edit_store').val();

            /*alert($store_description);
            alert($product_description);
            alert($quantity);
            alert($price);
            alert($sprice);
            alert($shipping_price);*/

            $.ajax({
                url:base_url_complete+"dashboard/updateStore/"+$store_id,
                type: 'POST',
                data: {madebyus4u_csrf_test_name:csrf_hash,product_id:$product_id,storename:$storename,
                    store_description:$store_description,product_name:$product_name,
                    product_description:$product_description,quantity:$quantity,
                    price:$price,sprice:$sprice,bankbranch:$bankbranch,
                    account_owner:$account_owner,routenumber:$routenumber,
                      accountnumber:$account_number,reaccountnumber:$reaccountnumber,
                    account_type:$account_type,categories:$category,colors:$colors,sizes:$sizes,
                    shipping_method:$shipping_method,shipping_price:$shipping_price},
                success: function(){
                    setTimeout(function() { sweetAlert("Good Job!", "Your Store has been Updated! ", "success");},1000);
                    setTimeout(function() {$("#cancelEditStore").click()},4000);
                    setTimeout(function() { window.location.reload(true)},5000);
                },
                error: function(){
                    setTimeout(function() { sweetAlert("Error", "Store Update Failed! ", "error");},1000);
                }
            });
            e.preventDefault(); // could also use: return false;
        });
    });
</script>

<script>

    $('#imageUpload').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        window.img_id = button.data('imageid') ;// Extract info from data-* attributes
        window.img_src = document.getElementById(window.img_id).getAttribute('src').toString();
        console.log ("image id global set to ="+ window.img_id);
        document.getElementById("img_src_id").value = window.img_id;

    });
</script>
<script>

    $(".editlistingsStore").click(function(){
        //e.preventDefault();
        var clicked_row = $(this);
        $store_id = clicked_row.attr('data-id');
        var result = getEditStoreData($store_id);

        if(true) {
            $("#storesOverviewTable").hide("slow");
            $("#editStore").show("slow");
        }
    });

    $(".editLisitingProducts").click(function(){
        //e.preventDefault();
        var clicked_row = $(this);
        product_id_edit_lisiting = clicked_row.attr('data-id');
        var result = getEditProductData(product_id_edit_lisiting);

        if(true) {
            $("#storesOverviewTable").hide("slow");
            $("#editStore").show("slow");
        }
    });

    function getEditStoreData($store_id) {
        //console.log('Who called the geditStore ? haa');
        //alert('Who called the geditStore ? haa');

        $.ajax({
            url:base_url_complete+"dashboard/getEditStoreData/"+$store_id,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash,},
            success: function(data){
                console.log(JSON.parse(data));
                var jsonData = JSON.parse(data);
                console.log(jsonData[0].store);
                console.log(jsonData[0].store.store_id);

                $("#black_jezuez_id").val(jsonData[0].store.store_id);
                $("#black_jezuez_product_id").val(jsonData[0].product.product_id);
                $("#store_name_edit_store").val(jsonData[0].store.name);
                $("textarea#store_description_edit_store").val(jsonData[0].store.desc);

                $("#store_img_edit").attr('src',jsonData[0].media_store.file_name);
                $("#img1_edit_store").attr('src',jsonData[0].media_products[0]);
                $("#img2_edit_store").attr('src',jsonData[0].media_products[1]);
                $("#img3_edit_store").attr('src',jsonData[0].media_products[2]);
                $("#img4_edit_store").attr('src',jsonData[0].media_products[3]);
                $("#img5_edit_store").attr('src',jsonData[0].media_products[4]);

                $('#product_name_edit_store').val(jsonData[0].product.name);
                $('textarea#product_description_edit_store').val(jsonData[0].product.desc);
                // alert(jsonData[0].product.quantity);
                $('#quantity_edit_store').val(jsonData[0].product.quantity);
                $('#price_before_tax_edit_store').val(jsonData[0].product.price);
                $('#shipping_price_edit_store').val(jsonData[0].product.shipping_price);
                $('#sprice_edit_store').val(jsonData[0].product.sprice);
                $('#bankbranch_edit_store').val(jsonData[0].get_paid.branch);
                $('#account_owner_edit_store').val(jsonData[0].get_paid.owner_name);
                $('#routing_number_edit_store').val(jsonData[0].get_paid.routing_number);
                $('#account_number_edit_store').val(jsonData[0].get_paid.account_number);
                $('#re_account_number_edit_store').val(jsonData[0].get_paid.account_number);

                $('#colors_edit_store').val(jsonData[0].product.color);
                $('#sizes_edit_store').val(jsonData[0].product.size);
                $('#method_edit_store').val(jsonData[0].product.shipping_method);
                $('#shipping_price_edit_store').val(jsonData[0].product.shipping_price);
                $('#account_type_edit_store').val(jsonData[0].get_paid.account_type);
                $('#category_edit_store').val(jsonData[0].product.categories);
                swal({   title: "Loading ...",   text: "Populating store data ,waiting for a second.",   timer: 1000,   showConfirmButton: false });

                return true;


            },
            error: function(){
                setTimeout(function() { sweetAlert("Error", "Your Store has not been Launched! ", "error");},1000);
            }
        });
        return false; // could also use: return false;


    }

    function getEditProductData($product_id) {
        //console.log('Who called the geditStore ? haa');
        //alert('Who called the geditStore ? haa');

        $.ajax({
            url:base_url_complete+"dashboard/getEditProductData/"+$product_id,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash},
            success: function(data){
                console.log(JSON.parse(data));
                var jsonData = JSON.parse(data);
                $('#product_name_edit_listing').val(jsonData[0].product.name);
                $('textarea#product_description_edit_listing').val(jsonData[0].product.desc);
                $('#quantity_edit_listing').val(jsonData[0].product.quantity);
                $('#price_edit_listing').val(jsonData[0].product.price);
                $('#sprice_edit_listing').val(jsonData[0].product.sprice);
                $('#category_edit_listing').val(jsonData[0].product.categories);
                $('#colors_edit_listing').val(jsonData[0].product.color);
                $('#sizes_edit_listing').val(jsonData[0].product.size);
                $('#shipping_price_edit_listing').val(jsonData[0].product.shipping_price);
                $('#shipping_method_edit_listing').val(jsonData[0].product.shipping_method);

                $('#img1_edit_listing').attr('src',jsonData[0].media_products[0])
                $('#img2_edit_listing').attr('src',jsonData[0].media_products[1]);
                $('#img3_edit_listing').attr('src',jsonData[0].media_products[2]);
                $('#img4_edit_listing').attr('src',jsonData[0].media_products[3]);
                $('#img5_edit_listing').attr('src',jsonData[0].media_products[4]);

                //auctions
                $('#name_of_auction_edit_listing').val(jsonData[0].auction.name);
                $('#starting_price_bid_edit_listing').val(jsonData[0].auction.starting_price);
                $('#reserve_price_edit_listing').val(jsonData[0].auction.reserve_price);
                $('#auction_startdate_edit_listing').val(jsonData[0].auction.start_date);
                $('#auction_start_time_edit_listing').val(jsonData[0].auction.start_time);
                $('#auction_enddate_edit_listing').val(jsonData[0].auction.end_date);
                $('#auction_endtime_edit_listing').val(jsonData[0].auction.end_time);

                product_id_edit_lisiting = jsonData[0].product.product_id;




                swal({   title: "Loading ...",   text: "Populating lisiting data ,waiting for a second.",   timer: 1000,   showConfirmButton: false });

                return true;


            },
            error: function(){
                setTimeout(function() { sweetAlert("Error", "Your Store has not been Launched! ", "error");},1000);
            }
        });
        return false; // could also use: return false;


    }
</script>
<script>

    $("#info-set-auctions").click(function(e) {
        e.preventDefault();
       sweetAlert("Info", "You need to create and save the listing first , Thanks! ", "error");

    });

    $(".editLisitingProducts").click(function(e){
        e.preventDefault();
        //alert('alert');
        $("#listingOverviewTable").hide("slow");
        $("#editListing").show("slow");
        var clicked_row = $(this);
       // $store_id = clicked_row.attr('data-id');
        //var result = getEditStoreData($store_id);

        if(true==true) {

        }
    });

///create listing
    $(function(){
        $("#submit_add_listing").click(function(e){  // passing down the event

            $product_name = $('#product_name_add_listing').val();
            $product_description = $('#product_description_add_listing').val();
            $quantity_add_store = $('#quantity_add_listing').val();

            $price = $('#price_add_listing').val();
            $sprice= $('#price_add_listing').val();
            $category=$('#category_add_listing :selected').text();
            $colors=$('#colors_add_listing :selected').text();
            $sizes=$('#sizes_add_listing :selected').text();
            $store_add_listing = $('#store_add_listing :selected').text();
            $store_id = $('#store_add_listing :selected').val();
            $shipping_price = $('#shipping_price_add_listing').val();
            $shipping_method = $('#shipping_method_add_listing :selected').text();

            $.ajax({
                url:base_url_complete+"dashboard/addListing/"+$store_id,
                type: 'POST',
                data: {madebyus4u_csrf_test_name:csrf_hash,product_name:$product_name,
                    product_description:$product_description,quantity:$quantity_add_store,
                    price:$price,sprice:$sprice,categories:$category,colors:$colors,sizes:$sizes,
                    shipping_method:$shipping_method,shipping_price:$shipping_price},
                success: function(data){
                    var datajson = JSON.parse(data);
                    //alert(datajson);
                    if(datajson.success==true) {

                        setTimeout(function () {
                            sweetAlert("Good Job!", "Your listing is add to " + $store_add_listing + " store !", "success");
                        }, 1500);
                        $('#info-set-auctions').css('display', 'none');
                        $('.set-auction-for-add').css('display', 'block');
                        setTimeout(function () { sweetAlert("Info", "You can create auctions for the listing now! ", "info");},3000);
                        setTimeout(function(){ $('.set-auction-for-add').click();},3000);
                        //disabled :) if  got locked away :(

                        $("#add_listing_div").find('input:text,input:password, input:file, select, textarea')
                            .each(function() {
                                $(this).attr('disabled',true)
                                // alert();
                            });
                        $("#add_listing_div").find('img, option,input:button,button')
                            .each(function() {
                                $(this).attr('disabled',true)
                                //alert();
                            });
                        $('#quantity_add_listing').attr('disabled','true');
                        $('#submit_add_listing').attr('disabled','true').fadeOut('10000');

                        product_id_auction = datajson.saved_product_id;

                    }
                   }
                ,
                error: function(data){
                    setTimeout(function() { sweetAlert("Error", "Your Store has not been Launched! ", "error");},1000);
                }
            });

            e.preventDefault(); // could also use: return false;
         });
        });


    function previewAuction() {


        $('#product_img_default').attr('src',$('#img1_add_listing').attr('src'))
        $('#product_img_1').attr('src',$('#img1_add_listing').attr('src'));

        $('#product_img_2').attr('src',$('#img2_add_listing').attr('src'));
        $('#product_img_3').attr('src',$('#img3_add_listing').attr('src'));
        $('#product_img_4').attr('src',$('#img4_add_listing').attr('src'));
        $('#product_img_5').attr('src',$('#img5_add_listing').attr('src'));

        $('#description_paragraph_auction_preview').html($('textarea#product_description_add_listing').val());
        $('#auction_end_datetime').val(  "date: "+$('#auction_end_date_add_listing').val()+" time: "+$('#auction_end_time_add_listing').val());
        $('#name_auction_preview').html($('#name_of_auction_add_listing').val());
    }

    $('#save_auction_btn').click(function(){

        $auction_name = $('#name_of_auction_add_listing').val();
        $preview_auction_name = $('#name_auction_preview').val();

        $starting_price = $('#starting_price_bid_add_listing').val();
        $reserve_price = $('#reserve_price_bid_add_listing').val();

        $auction_startdate = $('#auction_startdate_add_listing').val();
        $auction_start_time = $('#auction_start_time_add_listing').val();
        $auction_end_date = $('#auction_end_date_add_listing').val();
        $auction_end_time = $('#auction_end_time_add_listing').text();

        //alert('product id'+product_id_auction);

        $.ajax({
            url:base_url_complete+"dashboard/addAuctionForProduct/"+product_id_auction,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash,name:$auction_name,
                bid_price:$starting_price,reserve_price:$reserve_price,
               start_date:$auction_startdate,start_time:$auction_start_time,
               end_date:$auction_end_date,end_time:$auction_end_time},
            success: function(data){
                var datajson = JSON.parse(data);
                //alert(datajson);
                if(datajson.type=="success") {

                    setTimeout(function () {
                        sweetAlert("Good Job!", datajson.message, "success");
                    }, 1500);

                    setTimeout(function() { $("#cancelAddListing").click()},4000);
                    setTimeout(function() { window.location.reload(true)},5000);

                }
            }
            ,
            error: function(data){
                var datajson = JSON.parse(data);
                setTimeout(function() { sweetAlert("Error",datajson.message, "error");},2000);
            }
        });

        e.preventDefault(); // could also use: return false;
    });
 //update listing :) celebration - oldies - holiday! -celebrate!
    $('#update_listing_btn').click(function(e){



        $product_name = $('#product_name_edit_listing').val();
        $product_desc = $('textarea#product_description_edit_listing').val();
        $quantity = $('#quantity_edit_listing').val();
        $price_before_tax = $('#price_edit_listing').val();
        $sprice = $('#sprice_edit_listing').val();
        $category = $('#category_edit_listing').val();
        $color = $('#colors_edit_listing').val();
        $size = $('#sizes_edit_listing').val();
        $shipping_price = $('#shipping_price_edit_listing').val();
        $shipping_method = $('#shipping_method_edit_listing').val();


        //auctions
        $auction_name = $('#name_of_auction_edit_listing').val();
        $start_price= $('#starting_price_bid_edit_listing').val();
        $reserve_price=$('#reserve_price_edit_listing').val();
        $auction_startdate = $('#auction_startdate_edit_listing').val();
        $auction_start_time= $('#auction_start_time_edit_listing').val();
        $auction_end_date = $('#auction_enddate_edit_listing').val();
        $auction_end_time = $('#auction_endtime_edit_listing').val();

        //product_id_edit_lisiting = jsonData[0].product.product_id;

        //alert('product id'+product_id_edit_lisiting);

        $.ajax({
            url:base_url_complete+"dashboard/updateProductListing/"+product_id_edit_lisiting,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash,
                product_name:$product_name,
                product_description:$product_desc,quantity:$quantity,
                price:$price_before_tax,sprice:$sprice,categories:$category,colors:$color,sizes:$size,
                shipping_method:$shipping_method,shipping_price:$shipping_price,
                name:$auction_name,start_price:$start_price,reserve_price:$reserve_price,
                start_date:$auction_startdate,start_time:$auction_start_time,
                end_date:$auction_end_date,end_time:$auction_end_time,
            },
            success: function(data){
                var datajson = JSON.parse(data);
                //alert(datajson);
                if(datajson.type=="success") {

                    setTimeout(function () {
                        sweetAlert("Good Job!", datajson.message, "success");
                    }, 1500);

                   setTimeout(function() { $("#cancelAddListing").click()},4000);
                    setTimeout(function() { window.location.reload(true)},5000);

                }
            }
            ,
            error: function(data){
                var datajson = JSON.parse(data);
                setTimeout(function() { sweetAlert("Error",datajson.message, "error");},2000);
            }
        });

        e.preventDefault(); // could also use: return false;
    });


    //auctions
    $(".productInAuction").click(function(){

        //e.preventDefault();
        var clicked_row = $(this);
        $auction_id = clicked_row.attr('data-id');

        var result = getEditAuctionData($auction_id);

        if(true) {
            $("#auctionOverviewTable").hide("slow");
            $("#editAuctionForAuction").show("slow");
        }


    });

    function getEditAuctionData($auction_id) {
        //console.log('Who called the geditStore ? haa');;

        $.ajax({
            url:base_url_complete+"dashboard/getEditAuctionData/"+$auction_id,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash},
            success: function(data){
                console.log(JSON.parse(data));
                var jsonData = JSON.parse(data);

                $('#img1_edit_auction').attr('src',jsonData[0].media_products[0])
                $('#img2_edit_auction').attr('src',jsonData[0].media_products[1]);
                $('#img3_edit_auction').attr('src',jsonData[0].media_products[2]);
                $('#img4_edit_auction').attr('src',jsonData[0].media_products[3]);
                $('#img5_edit_auction').attr('src',jsonData[0].media_products[4]);

                //auctions
                $('#name_of_auction_edit_auction').val(jsonData[0].auction.name);
                $('#starting_bid_price_edit_auction').val(jsonData[0].auction.starting_price);
                $('#reserve_price_edit_auction').val(jsonData[0].auction.reserve_price);
                $('#auction_start_date_edit_auction').val(jsonData[0].auction.start_date);
                $('#auction_end_time_edit_auction').val(jsonData[0].auction.start_time);
                $('#auction_enddate_edit_auction').val(jsonData[0].auction.end_date);
                $('#auction_endtime_edit_auction').val(jsonData[0].auction.end_time);

                product_id_edit_lisiting = jsonData[0].auction.product_id;
                product_id_auction = jsonData[0].auction.auction_id;


                swal({   title: "Loading ...",   text: "Populating auctions data ,wait for a second please.",   timer: 1000,   showConfirmButton: false });

                return true;


            },
            error: function(){
                setTimeout(function() { sweetAlert("Error", "Loading auction data , please try again later! ", "error");},1000);
            }
        });
        return false; // could also use: return false;
    }


    //update listing :) celebration - oldies - holiday! -celebrate!
    $('#save_edited_auction').click(function(e){

        //auctions
        $auction_name = $('#name_of_auction_edit_auction').val();
        $start_price=  $('#starting_bid_price_edit_auction').val();
        $reserve_price=  $('#reserve_price_edit_auction').val();
        $auction_startdate=  $('#auction_start_date_edit_auction').val();
        $auction_start_time = $('#auction_start_time_edit_auction').val();
        $auction_end_date = $('#auction_enddate_edit_auction').val();
        $auction_end_time = $('#auction_endtime_edit_auction').val();



       // alert('product id'+product_id_edit_lisiting);

        //alert('auction id'+$auction_name);

        $.ajax({
            url:base_url_complete+"dashboard/updateAuction/"+product_id_auction,
            type: 'POST',
            data: {madebyus4u_csrf_test_name:csrf_hash,
                product_id:product_id_edit_lisiting,
                name:$auction_name,start_price:$start_price,reserve_price:$reserve_price,
                start_date:$auction_startdate,start_time:$auction_start_time,
                end_date:$auction_end_date,end_time:$auction_end_time,
            },
            success: function(data){
                var datajson = JSON.parse(data);
                //alert(datajson);
                if(datajson.type=="success") {

                    setTimeout(function () {
                        sweetAlert("Good Job!", datajson.message, "success");
                    }, 1500);

                    setTimeout(function() { $("#cancelAddListing").click()},4000);
                    setTimeout(function() { window.location.reload(true)},5000);

                }
            }
            ,
            error: function(data){
                var datajson = JSON.parse(data);
                setTimeout(function() { sweetAlert("Error",datajson.message, "error");},2000);
            }
        });

        e.preventDefault(); // could also use: return false;
    });


</script>





<script type="text/javascript">

    $(document).ready(function() {

        var current_position = 0;

        var  start_image_path = "<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>";

        //alert(current_position);
        var csrf_hash = "<?= $this->security->get_csrf_hash();?>"
        $('.img-thumbnail-large-0').show();


        $(document).on('click','#prev_button',function(){
            if(current_position>0) {
                current_position = current_position -1;
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                console.log('.img-thumbnail-small-'+current_position);
            }
            //alert(current_position);

        });

        $(document).on('click','#next_button',function(){

            if(current_position<=4){
                current_position = current_position+1;
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                console.log('.img-thumbnail-small-'+current_position);
            }

        });



        $(document).on('click','.img-thumbnail-small-1',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-1').attr('src'));

            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-2',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-2').attr('src'));
            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-3',function(){

            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-4',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-4').attr('src'));
            //replace image one on this pic place
            return false;
        });

        $(document).on('click','.img-thumbnail-small-0',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
            //replace image one on this pic place
            $('.img-thumbnail-small-3').attr('src',start_image_path);


            return false;
        });

        // Quantity buttons add and decerase
        $("#increment_button").click(function(){
            //alert("Increase quantity.");
            var qty = parseInt($("#p_quantity").val(),10);
            //alert(qty);
            if (qty < 10){
                newqty = qty + 1;
                $("#p_quantity").val(newqty.toString());
                $("#product_quantity").val(newqty.toString());
                //alert($("#product_quantity").val());
            }
        });

        $("#decrement_button").click(function(){
            //alert("Increase quantity.");
            var qty = parseInt($("#p_quantity").val(),10);
            //alert(qty);
            if (qty > 1){
                newqty = qty - 1;
                $("#p_quantity").val(newqty.toString());
                $("#product_quantity").val(newqty.toString());
                //alert($("#product_quantity").val());
            }
        });

    });//end of document ready

    $('#toggle_auction_status_for_add_listing button').click(function(){
        if($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')){
            /* code to do when unlocking */
            $('#switch_status_add_listing').html('Auction Acitve');
            $('#switch_status_add_listing' ).removeClass('alert-danger');
            $('#switch_status_add_listing' ).addClass(' alert-info');
        }else{
            /* code to do when locking */
            $('#switch_status_add_listing').html('Auction Not Active');
            $('#switch_status_add_listing' ).removeClass('alert-info');
            $('#switch_status_add_listing' ).addClass('alert-danger');

        }

        /* reverse locking status */
        $('#toggle_auction_status_for_add_listing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info ');
        $('#toggle_auction_status_for_add_listing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
    });


    ///

    $('#toggle_auction_status_for_edit_auction button').click(function(){
        if($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')){
            /* code to do when unlocking */
            $('#switch_status_edit_auction').html('Auction Acitve');
            $('#switch_status_edit_auction' ).removeClass('alert-danger');
            $('#switch_status_edit_auction' ).addClass(' alert-info');
        }else{
            /* code to do when locking */
            $('#switch_status_edit_auction').html('Auction Not Active');
            $('#switch_status_edit_auction' ).removeClass('alert-info');
            $('#switch_status_edit_auction' ).addClass('alert-danger');

        }

        /* reverse locking status */
        $('#toggle_auction_status_for_edit_auction button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info ');
        $('#toggle_auction_status_for_edit_auction button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
    });


    ///
    $('#toggle_auction_status_for_edit_listing button').click(function(){
        if($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')){
            /* code to do when unlocking */
            $('#switch_status_edit_listing').html('Auction Acitve');
            $('#switch_status_edit_listing' ).removeClass('alert-danger');
            $('#switch_status_edit_listing' ).addClass(' alert-info');
        }else{
            /* code to do when locking */
            $('#switch_status_edit_listing').html('Auction Not Active');
            $('#switch_status_edit_listing' ).removeClass('alert-info');
            $('#switch_status_edit_listing' ).addClass('alert-danger');

        }
        /* reverse locking status */
        $('#toggle_auction_status_for_edit_listing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info ');
        $('#toggle_auction_status_for_edit_listing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
    });
</script>


</body>
</html>

