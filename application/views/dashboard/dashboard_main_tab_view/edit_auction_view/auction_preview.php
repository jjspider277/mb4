<h3>Auction Preview</h3>
<div class="col-md-12 white-bg" style="padding-top: 10px;">
    <?php $product = array();?>
    <div class="col-md-7 no-left-padding" style="">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php


                // $image_found = isset($product->image)?true:false ;
                // = is_array($product->image);

                ?>
                <img class="img-thumbnail img-thumbnail-large-0 img-responsive" src="<?php echo base_url()."assets/images/Shawn_Tok_Profile.jpg" //$product->image[0];?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height: 420px;;">



            </div>

            <!-- Controls -->
            <a class="left carousel-control" id="prev_button" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" id="next_button" href="#carousel-example-generic" role="button" data-slide="next" style="right:4px;">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="col-md-12 col-md-large-image no-left-padding" style="">

            <div class="col-md-12 col-md-small-images no-left-right-padding" style="padding-top: 14px;">
                <div class="col-md-2 no-left-right-padding" style="width:16.66666667% !important;">
                    <button class="btn btn-sm transparent-bg" style="border:1px solid #c2c2c2;"><i class="glyphicon glyphicon-search "></i> </button><span style="padding-left:3px;">Zoom</span>
                </div>
                <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                    <img class="img-thumbnail-small-1 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo base_url()."assets/images/big-watch.jpg"?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                </div>
                <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                    <img class="img-thumbnail-small-2 img-thumbnail img-responsive small-images dark-blue-border"src="<?php echo base_url()."assets/images/Shawn_Tok_Profile.jpg"?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                </div>
                <div class="col-md-2  no-right-padding" style="width:16.66666667% !important;">
                    <img class="img-thumbnail-small-3 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo base_url()."assets/images/delivery.jpg"?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                </div>
                <div class="col-md-2 no-right-padding" style="width:16.66666667% !important;">
                    <img class="img-thumbnail-small-4 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo base_url()."assets/images/big-watch.jpg"?>" data-holder-rendered="true"  style="height: 72px;width: 80%;">

                </div>
                <div class="col-md-2 no-right-padding" style="width:16.66666667% !important;">
                    <img class="img-thumbnail-small-0 img-thumbnail img-responsive small-images" src="" data-holder-rendered="true"  width="100%" style="height:113px;width:100%;display: none;">

                </div>

            </div>
        </div>



    </div>
    <div class="col-md-5 blue-border"style="padding-bottom: 10px !important;">
        <h3 class="blue-font col-md-12">Grey Blancket Scarf</h3>
        <div class="col-md-12">
            <p>
                Rating Goes here
            </p>
            <hr>
        </div>
        <div class="col-md-12">
            <small class="grey-font">Description</small>
            <p class="black-font">Beautiful, Rustic, Grey Wrap. Blanket scarf, Outlander - inspired BOTH SIDES NOW shawl,
                wrap, Outlander - shawl, Claire's grey wrap, heavy shaw</p>
        </div>
        <div class="col-md-12">
            <div class="col-md-12 grey-bg border-radius">
                <div class="col-md-7 no-left-right-padding" style="padding-top: 14px;">
                    <h3 class="no-margin black-font pull-right">Current Bid Amount:</h3>

                </div>
                <div class="col-md-5 no-left-right-padding">
                    <span class="grey-font pull-left" style="font-size:36px;">&nbsp$</span><span style="font-size:36px;">&nbsp49.99</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-7 no-left-right-padding" style="">
                    <h4 class="no-margin black-font pull-right">Minimum Bid Amount:</h4>

                </div>
                <div class="col-md-5 no-left-right-padding">
                    <span class="grey-font pull-left" style="font-size:30px;">&nbsp$</span><span style="font-size:30px;">&nbsp49.99</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-7 no-left-right-padding">
                    <h4 class="no-margin black-font pull-right" style="padding-top: 7px;">Bid History:</h4>

                </div>
                <div class="col-md-5 no-left-right-padding">
                    <span class="green-font" style="font-size:24px;">&nbsp352</span>
                </div>
            </div>

        </div>
        <hr>
        <div class="col-md-12">
            <h4 class="black-font">Enter Your Bid Amount:</h4>
            <div  class="col-md-12 grey-bg border-radius" style="padding-top:10px;margin-bottom: 10px">
                <div class="col-md-12 no-left-right-padding" >
                    <div class="form-group">
                        <input type="text full-width" class="form-control" placeholder="$0.00">
                    </div>
                </div>
            </div>

            <div  class="col-md-12 grey-bg border-radius" style="margin-bottom: 10px">
                <div class="col-md-12 no-left-right-padding" >
                    <h4 class="text-center" style="padding-top:0px;"><span><img src="<?php echo base_url()."assets/icons/timer.png"; ?>"> </span> <span>Auction End Time: </span> <span class="green-font">15d 5h 23m</span></h4>
                </div>
            </div>
            <div class="col-md-12 no-left-right-padding" style="margin-bottom: 6px;">
                <button class="full-width btn btn-success "><span><img src="<?php echo base_url()."assets/icons/auctions_white.png "?>"></span><span style="font-size:18px;">&nbspSubmit Bid</span></button>

            </div>
            <div class="col-md-12 no-left-right-padding">
                <a role="tab" data-toggle="tab" id="returnToEditAuction" class="full-width btn black-btn white-font"><span style="font-size:18px;">&nbspCancel</span></a>

            </div>
        </div>


    </div>


</div>