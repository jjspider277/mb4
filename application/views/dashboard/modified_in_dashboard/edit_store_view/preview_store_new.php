<style>
    .glyphicon-user {
        padding-right: 11px;
        font-size: 16px;
    }
    .fa-shopping-cart,.fa-users{
        color:#ababab;
        font-size: 16px;
    }
</style>

<div class="col-md-12 col-md-store-content" >
    <h4 class="blue-font">Step 4. Preview Store</h4>
    <div class="col-md-12 ">
        <p>
        <span class='tab_content_title'>View Your Store Before You  Launch! </span>
        <p class="intro_table_text">
        Below is a preview of your store you have create in your store setup. If the information is correct the next step is to launch your live store. Members will be able to view
        and purchase your products.</p>
        </p>
    </div>
    <div class='col-md-12 grey-bg blue-border' style="padding-bottm:20px;">
        <div class="col-md-12 three-columns-preview" style="padding: 18px 0px;" >
            <div class="col-sm-4 col-md-4 member-container">
                <div class="thumbnail item-lisiting-inner col-sm-12">
                    <div class="col-sm-8 col-sm-offset-2">
                        <a href="<?php //echo base_url('sell/seller/').'/'.$profile->profile_id;?>">

                            <img  class="img-circle img-responsive" src="<?php //echo base_url($profile->media->file_name);?>" style="height: 135px;display: block;"></a></div>

                    <div class='text_content text-center col-sm-12'>
                        <div class="seller_name">
                            <h4><small><i class='glyphicon glyphicon-user'></i></small> <span class="member_name"><?php //echo $profile->full_name; ?>Norman Feichtmier</span></h4>
                        </div>

                        <div class="seller_location">
                            <h4><small><span class="member_location"><?php //echo $profile->city; ?>, <?php //echo $profile->state; ?> </span></small></h4>
                        </div>

                        <hr> <!--horizonal line-->

                        <div class="rating_social col-sm-12 white-background no-left-right-padding">
                            <div class="col-sm-3 col-sm-offset-1 no-left-padding"><p >Ratings:</p></div>
                            <div class="col-sm-8 ">
                                <p>
                                    <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?php// echo $profile->profile_id;?>" value="<?php //echo $profile->profile_rating ;?>5" data-type="profile"/>
                                </p>

                            </div>

                        </div>
                        <div class="col-sm-12 no-left-right-padding" style="padding-bottom: 10px;">
                            <div class="col-sm-6 friends no-left-padding"><span class="fa fa-users"></span>Friends: <span class="blue-font">(18)</span></div>
                            <div class="col-sm-6 no-right-padding "><span class="fa fa-shopping-cart"></span>Listings:<span class="blue-font">(10)</span></div>
                        </div>
                    </div> <!--end of text content -->


                </div>








            </div>
            <!--col2-->
            <div class="col-md-4 dashboard-product borders" style="margin-left:3px" >
                <div class="col-md-12 white-bg" style="padding: 10px 10px 0px;">
                    <img class="col-md-12 thumbnail" id="product_preview_image"src="<?php echo base_url('uploads/profile/no-photo.jpg');?>" alt="..." height="290">
                </div>
            </div>
            <!--col2-->
            <style type="text/css">
            .money {
            font-weight: 700;
            font-size: xx-large;
            }
            .save_btn {
            color: BLACK;
            background-color: #F0F0F0;
            border-color: #DFE0E0;
            }
            .sell_info .btn {
            margin-top: 10px;
            }
            .price_tag_label {
                font-size: smaller;
            }
            .tab_content_title{
            	margin-left:0px;
            }
            </style>

            <div class="col-md-4 sell_info blue-border" style="width: 32%;height: 322px;">
            <div class="col-md-12  ">
                <h3 class="blue-font no-margin">Grey Blanket Scarf</h3>
                <hr >
            </div>
            <div class="col-md-12 ">
                <small class="grey-font">Desciption</small>
                <p calss='description' id='descritpion_paragraph'>Lorem ipsum dolor sit amet, cocterut adipiscing elit. Loret
                    sem ipsum dolor sit adipiscing elit edam itis.  </p>
            <hr>


            </div>
                <div class='money col-md-12 '><label id='price_tag_label'><span class="grey-font">$</span> 35.00 <span class="blue-font">USD</span></label></div>






            </div>

        </div>


<div class="form-group col-md-2 col-md-offset-5 top-padding">
              <input id='btn_save' type='submit' class="btn btn-primary btn-lg" value="Launch Your Store">
</div>
</div>
</div>