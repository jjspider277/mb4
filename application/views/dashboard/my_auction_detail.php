<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>My Auction Detail</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">

	<link href=<?php echo base_url() ."assets/css/collective_common.css"; ?> rel="stylesheet">
	<link href=<?php echo base_url() ."assets/css/store_tabs.css"; ?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
   	<link href=<?php echo base_url()."assets/css/dashboard_listing_view.css";?> rel="stylesheet">
	

</head>
<style>

.img-thumbnail {
    opacity: 3.0;
    filter: alpha(opacity=100); /* For IE8 and earlier */
}

.auction-info p{
	padding: 10px 0;
}
.auction-buttons a{
	margin-left: 15px;
	margin-top: 20px;
	margin-right: 30px;
}
.img-thumbnail:hover {
  opacity: 0.4;
  background-color: #4682B4;

   background-image: url('../assets/images/zoom.png') s;
   
   
       
    filter: alpha(opacity=40); /* For IE8 and earlier */
   
}
.edit-delete-toggle {
 display: visible;
}
.edit-btn {
background-color: #3E3F3F;
color: white;
float: right;
border-radius: 2px;
}
</style>

<body>

<?php $this->load->view($notification_bar); ?>

<header>

	<?php $this->load->view($header_black_menu); ?>

	<?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	<div class="main-row">
		<div class="container">
	 <div class="col-md-12">
         <div class="head-text" style="float:left; padding-top: 23px;">
                       
               <span><h2><strong> Account</strong> <font color="grey">Overview</font></h2></span>
                                                   
                </div>
                <div class="head-text need_help" style="float:right; padding-top: 49px;">
                   <strong><font color="grey"></font> </strong><a href="<?php echo site_url('dashboard/my_auctions') ?>" class="btn btn-primary"> Manage Auctions</a>
                </div>
       
               <HR class='col-md-12 hr'>
                           </div>      
            <div class="dot"></div>
			<!--import side menu -->

			 <?php $this->load->view($dashboard_sidemenu); ?>
			 
			<div class="col-md-10 middle-section">
            <div class="view-product-listing">
				<h4><strong>Auction</strong> Summary</h4>
				<hr>
				<div class="dot"></div>
				</div>
				<div class='row product-liting-pages'>
					
					<div class="product-listing" style="padding:15px; background-color: #fff;">
						<div class="row">
							<div class="col-md-4" style="">
						<div class="thumbnail">
			                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>">

			                        <img  alt="<?php echo $product['product_name'];?>"  class="img-thumbnail img-responsive xpens" 
			                              src="<?php echo $product['image'][0] ;?>" 
			                              style=' text-align: center; width: 100%;height: 200px;'>
			               </a>
			                   
			                 <div class="row" style="padding-bottom: 10px">
			                       <div class="col-md-12 product_description text-center">
			                       <?php echo $product['pdescription'];?></div>
			                        <div class="col-md-6" >
			                          <span class='price'>$<?php echo $product['price'];?></span>
			                          
			                      	</div>
			                      <div class="col-md-6">
			                      	<span class='buy_btn viewDetails'> 
			                            <a href="#" class="btn btn-primary" role="button"><small>BUY NOW</small></a>
			                          </span>
			                      </div>
		                      </div>
		                      	<hr  style="">
			                  <div class="row">
			                  		<div class="col-md-6">
			                  		<small><i class="glyphicon glyphicon-user"></i> <?php echo $product['profile']->seller_name ?></small>
				                  	</div>
				                  	<div class="col-md-6">
				                  		<input class="rating" data-stars="5" data-step="1" data-size="xs" 
				                   id="rating_element-<?=$product['product_id'];?>" 
				                   value="<?= $product['p_avg_rating'] ;?>" data-type="products" /> 

				                  	</div>
			                   </div>
			                   <div class="row">
			                   	<div class="col-md-12 text-center">
			                   		End Time: <?php
                        
                       echo ($diff->d > 1)? $diff->d.' Days,' : $diff->d . ' Day,' ?> 
              <?php echo ($diff->h > 1)? $diff->h.' Hours' : $diff->h . ' Hour,' ?>
              <?php if($diff->h === 0): ?>
                  <?php echo  ($diff->i > 1)? $diff->i . ' Minutes' : $diff->i .' Minute'; ?>
              <?php endif;?>
			                   	</div>
			                   </div>
			                  
			              </div>          
			            </div>
			            <div class="col-md-8 auction-info">
			            	<p><strong><?php echo $product['product_name'] ?></strong></p>
			            	<p>Total # of Bids: <span class="pull-right blue"><?php echo $auctions->bids; ?></span></p>
			            	<p>Current Price: <span class="pull-right blue"><?php echo $auctions->current_bid; ?></span></p>
		            		<p>Starting Price: <span class="pull-right blue">
		            			<?php echo $auctions->bid_price ?>
		            		</span></p>
		            		<p>Ending Date: <span class="pull-right blue">
		            			<?php $end_date = date_create($auctions->end_date);
		            			echo date_format($end_date,"m/d/Y h:i:s A") ?>
		            		</span></p>
		            		<p>Action Status: <span class="pull-right blue">
		            			<?php echo ($auctions->status == true)? 'Active' : 'Closed' ?>
		            		</span></p>
			            </div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 auction-buttons">
							<a href="<?php echo site_url('dashboard/view_auction/'.$auctions->id); ?>" class="btn btn-primary">View Listing</a>
							<a href="<?php echo site_url('dashboard/edit_auction/'.$auctions->id);?>" class="btn btn-primary">Edit Listing</a>
							<a href="<?php echo site_url('dashboard/cancel_auction/'.$auctions->id) ?>" class="btn btn-primary">Cancel Listing</a>
						</div>
					</div>
			    </div>

				<div class="auction-page">			
				<?php $this->load->view($set_auction_page); ?>
				</div>
				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

</section>

<footer class="footer">

	<?php

	$this->load->view($footer_subscribe);
	$this->load->view($footer_privacy);
	?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script>
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
       
        $(".rating").rating('refresh', 
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {
          
          
           if(!readOnly) {
           //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");      
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

          }
           else {
            window.location.assign("<?=site_url('users/login');?>");
           }

          

        });

 
 
       
    });
</script>
<script type="text/javascript">
	/***
	 * Created by Daniel Adenew
	 * Submit email subscription using ajax
	 * Send email address
	 * Send contro  ller
	 * Recive response
	 */
	var url =  "<?php echo site_url('welcome/subscribe');?>";
	subscribe_using_ajax(url);
	//copied from store view
	 $(document).ready(function(){
		 $('.auction-page').hide();
		 
           $(document).on('click','.delete-image',function(){
            $('.product-listing,.need_help,.view-product-listing').hide();
            $('.auction-page').show();
            return false;
        });
      
    });

</script>

<script type="text/javascript">
	
$(document).ready(function(event){
     var p = $("#my_auctions_link").find('p');
     var images = $("#my_auctions_link").find('img');
     // console.log(p.first().removeClass("gray").addClass("white"));    
     // console.log(images[0].src='http://localhost/madebyus4u/assets/images/dashboard/auction1.png');
     // $("#my_auctions_link").find('div').first().addClass('side-active');
	});

</script>
	

</body>
</html>
