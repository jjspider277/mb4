<div class="col-md-2 sidebar-menu">

				
				<a id='dash_board_link' href="<?php echo base_url('dashboard/my_dashboard');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'dashboard.png';?>">
						<p class="gray">My Dashboard</p>
					</div>
				</a>
				<a id='my_orders_link' href="<?php echo base_url('dashboard/my_orders');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'orders.png';?>">
						<p class="gray">My Orders</p>
					</div>
				</a>
				<a id='my_purchase_link' href="<?php echo base_url('dashboard/my_purchases');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'purchases.png';?>">
						<p class="gray">My Purchases</p>
					</div>
				</a>

				<a id='my_store_link' href="<?php echo base_url('dashboard/my_stores');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'stores.png';?>">
						<p class="gray">My Stores</p>
					</div>
				</a>
				<a id='my_listing_link' href="<?php echo base_url('dashboard/my_listing');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'listings.png';?>">
						<p class="gray">My Listing</p>
					</div>
				</a>
				<a id='my_auctions_link' href="<?php echo base_url('dashboard/my_auctions');?>">
					<div class="mnu-item">
						<img src="<?php echo $image_path.'auction.png';?>">
						<p class="gray">My Auctions</p>
					</div>
				</a>
				<img width="" src="<?php echo $image_path.'down-arrow.png';?>">
			</div>