<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Welcome to MadebyUs4u.com | Sell</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles --><!-- Removed by GK 7/24/2016
  <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
  <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
  <link href=<?php echo base_url()."assets/css/sell.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
  <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
  <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
   <link href=<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.css";?> rel="stylesheet">
   
      <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet"> 
		<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
-->

    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">

	<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/checkout_new.css";?> rel="stylesheet">
</head>

<style type="text/css">

.right-style {
  background-color: white;
  text-align: center;
}

.left-style {
  background-color: #f5f5f5;
}

table {
  margin: 10px 0 30px 0;
}

table tr th,table tr td {
  background: black;
  color: white;
  padding: 7px 4px;
  text-align: left;
}

table tr td {
  background: white;
  color: #47433F;
  border-top: 1px solid #FFF;
}
}
.hr_border {
  border-top: 3px dotted #818181;
}

.processing,.paid,.track{
background-color: #DCDCD8;
height: 24px;
}
.grand{
background-color: black;
width: 100%;
height: 34px;
color: white;
}
.bottom-left {
float: right;
}

.add-tracking-btn {

color: black;
font-weight: bold;
}

.add-tracking-btn ,a:hover, a:focus {
    color: black;
    text-decoration: none;
}
.add-tracking-btn-container {
height:30px;
padding-top:1%;
text-align:center;
background-color: rgba(0, 0, 0, 0.31);
}
</style>



<body>

<?php $this->load->view($notification_bar); ?>

<header>

  <?php $this->load->view($header_black_menu); ?>

  <?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

   <!--load menu here -->
   <div class="row white-bg">
		<hr class="" style="margin: 0px;">
		<div class="container white-bg " style="">

			<home class="navbar navbar-default" role="navigation" style="min-height:40px;">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">

						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<style type="text/css">
						.main-navigation {
							background-color: #FAFAFA;
						}
						.navbar-default {
							background-color: inherit;
							border: none;
						}
						.blue-font {
							color: #2f97cc;
						}
						.grey-background{
							background: #ebebeb;
						}
						.bottom-blue-border{
							border-bottom: 4px solid #216da1;
						}
						.wrapper {
							text-align: center;
						}

						.start-shopping-btn {
							position: absolute;
							top: 83%;
							left: 42%;
							font-size: 27px;
							background: #0b69a0;
						}

						.black-btn{
							background:#303030;
						}
						.box-height{
							height: 251px;
						}
						.pagination{

							float: right !important;
						}
						.pull-right{

						}

					</style>
					<?php $this->load->view($column_main_menu);?>

				</div><!-- /.container-fluid -->
		</div>


	</div>
    <?php $profile_id = $this->session->userdata('profile_id'); ?>


        <div class="container">


      <div class="tab-content-panel" style="padding-left: 2%; padding-right: 2%;">
        <h3>
          <strong>Order Invoice Id: #<?php echo $order_details->invoice_id; //echo str_pad($order_details->o_id,5);?></strong>
        </h3>
      <HR class='hr_border'>
      </div>
      <br>
      <!--dashboard image hodlers START-->

      <div class="col-md-7">

        <div class="table-responsive right-style"
          style="border: 1px solid #e6e6e6;">
          <div class="pd-left" style="padding-left: 16px; padding-right: 16px; padding-top: 16px;">


            <div class="row">

              <div class="col-md-6">
               <p class="col-lg-6" style="text-align: left; padding:0px;">Purchased on:<br /><strong><?php echo $order_details->created_date;?></strong></p>
               
              <?php if($user_is_seller):?>
                
                 <p class="col-lg-6" style="text-align: left; padding: 0 0 0 6%;"> <font color="#336699"><strong><a href="<?php echo base_url('sell/seller/').'/'.$order_details->buyer_id;?>"><?php echo $order_details->buyer_full_name;?></a></strong></font>         <span class="label label-info">click to see<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></span></p>
        
              <?php else:?>
                 <p class="col-lg-6" style="text-align: left; padding:0 0 0 10%;"> <font color="#336699"><strong><?php echo $order_details->buyer_full_name;?></strong></font></p>
              <?php endif;?>

            </div>

              <div class="col-md-6">
                <p class="col-lg-6"> Sold by: </p> <p class="col-lg-6"> 
                <font color="#336699">
                <strong>
                    <?php if($user_is_seller==false):?>
                     <a href="<?php echo base_url('sell/seller/').'/'.$order_details->seller_id;?>">
                     <?php echo $order_details->seller_full_name;?></a>
                    <span class="label label-info">click to see<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></strong></font></p>
                    </span>
                  <?php else:?>
                     <?php echo $order_details->seller_full_name;?></a></strong></font></p>
                  <?php endif;?>
              </div>

            </div>

            <HR class='hr'>
            <div class="row">

              <div class="col-md-6">
                <p class="col-lg-6" style="text-align: left; padding:0px;">Payment Method: </p> <p class="col-lg-6" style="text-align: left; padding: 0 0 0 10%;"><?php echo isset($order_details->payment_type) ? $order_details->payment_type : 'Visa' ;?></p>
              </div>

              <div class="col-md-6">
                <p class="col-lg-6">Shipping Method: </p> <p class="col-lg-6 shipping_method_text" > <?php echo isset($order_details->shipping_method) ? $order_details->shipping_method : "seller's choice" ;?></p>
              </div>

            </div>
            <HR class='hr'>
            <div class="row">
  

              <div class="col-md-12">

                <p class="col-lg-3" style="text-align: left; padding:0px;">Shipping Address:</p>  
                 
                 <?php if($user_is_seller==false): ?>

                 <a href="#" class='label label-info btn-info' data-toggle="modal" data-target=".shipping_address_modal" >Edit Address </a>
                  
                  <?php endif; ?>

                  <p class="col-lg-7" style="text-align: left; padding:0 0 0 3%;" id='shipping_address_info'>


                  <?php echo "<strong> $order_details->shipto_fname  $order_details->shipto_lname  </strong><br/>
									   $order_details->shipto_street, $order_details->shipto_street2 <br/>  
										$order_details->shipto_city, ". strtoupper($order_details->shipto_state)." $order_details->shipto_zip"; 

                  ?> 

                </p>
              </div>


            </div>

          </div>
        </div>
      </div>


      <div class="col-md-5">

        <div class="table-responsive right-style"  style="border: 1px solid #e6e6e6;">
          <div class="content" >
            <dl class="dl-horizontal" style="height:100%;  margin:0px; padding:2%;">
             <dt style="padding-top: 8px;">
				<strong>Shipping Status:</strong>
             </dt>
             <dd>
                <p class="processing">
                           <?= form_dropdown('shippment_status', $shippment_status ,$order_details->shipping_status,'class="form-control"  tabindex="7" disabled=true style="font-weight:bold;"' ) ;?>  
				</p>
             </dd>
              <hr style="margin-top:8px;margin-bottom:8px;"class='hr'>
             <dt style="padding-top: 8px;">
				<strong>Payment Status</strong>
             </dt>
             <dd>
                <p class="paid">
                  <?= form_dropdown('payment_status', $payment_status , $order_details->payment_status,'class="form-control"  tabindex="7" disabled=true style="font-weight:bold;"' ) ;?>
                </p>
             </dd>

              <hr class='hr' style="margin: 2% 0 0 0;">
              <dl class="track" style="margin-top:4%;">
                <dt class="track">
					<p class="text-center"><strong  style="line-height: 1.6em;">Tracking #:</strong></p>
                </dt>

                <dd class="track">
					<p class="tracking_number text-center">
						<strong  style="line-height: 1.6em;">
							<?php echo isset($order_details->tracking_number)?$order_details->tracking_number:($user_is_seller?'buyer is waiting for shipping':'waiting seller to attach');?>
						</strong>
					</p>
                </dd>
                 
                 <?php if($user_is_seller==true && $order_details->shipping_status!='Shipped' && $order_details->shipping_status!='Pending') :?>
                     
                     <div class="add-tracking-btn-container">
                     
                        <a href="#" class='add-tracking-btn' data-toggle="modal" data-target=".add_tracking_modal">ADD TRACKING NUMBER </a>
                     
                    </div>

                <?php endif;?>

               

                 <?php if($user_is_seller==false): ?>

                  <?php if(isset($order_details->tracking_number)):?>
                    <a href="#" class='btn btn-info label-info form-control confirm-button' id='confirm-button' data-toggle="modal" data-target=".confirm_delivery_modal" data-order-details-id="<?php echo $order_details->od_id;?>"><strong>Report or Confirm Delivery</strong> </a>                  
                  <?php endif; ?>

                <?php endif; ?>

              </dl>

          </div>
        </div>
      </div>

    </div>
    <div class="container">
      <div class="tab-content-panel" style="padding-left: 2%; padding-right: 2%;">
    <div class="container" style="margin-top: 0px;">

        
      </div>
      <hr class='hr_border'>
          <table class="table table-bordered">          
			  <thead>
				  <tr>
					<th>Item</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Total</th>
					<!-- <th>Total Fees Paid</th> -->
					<!-- <th>Total Refund Amount</th> -->
				  </tr>
				</thead>
               <tbody>
                  <?php $sub_total=0;  foreach ($order_details->order_products as $key => $products): ?>
                  <tr>
                    <td><?php echo ucfirst($products['productname']); ?></td>
                    <td><?php echo ucfirst($order_details->price); ?></td>
                    <td><?php echo ucfirst($products['quantity']); ?></td>
                    <td><?php echo '$' . number_format(floatval($products['quantity'])*floatval($order_details->price), 2);?></td>
						<?php  $sub_total = $sub_total + floatval($products['quantity'])*floatval($order_details->price);?>
                  </tr>              
					<?php endforeach ?>
                </tbody>
            </table>      
		</div>
		<div class="col-md-4 bottom-left"  style="padding-left: 20px; padding-right: 19px;">
			<div class="table-responsive right-style" style="border: 1px solid #e6e6e6;">
				<div class="content" style="padding-left: 6px; padding-right: 6px; padding-top: 6px;">
					<dl class="dl-horizontal" style="margin-bottom:0px;">
						<dt     style="padding-top: 8px;">
							<strong>Shipping Method:</strong>
						</dt>
						<dd>
							<p class="track">
							<?= form_dropdown('shipping_method', $this->shipping_method , isset($order_details->shipping_method) ? $order_details->shipping_method : '','class="form-control" id="shipping_method"  tabindex="7" disabled=true style="font-weight:bold;"' ) ;?>
							</p>
						</dd>
						
						<hr class='hr' style="margin:5px 0px;">
						<dt class="text-right">
							<strong>Sub Total:</strong>
						</dt>
						<dd style="text-align: right; padding-right: 2%;">
							<strong id='sub_total_value'>
								<?php echo '$' . number_format($sub_total,2) ;?>
							</strong>
						</dd>
						<hr class='hr' style="margin:5px 0px;">
						<dt>
							<strong>Shipping Cost</strong>
						</dt>
						<dd class='shipping_cost_dd'   style="text-align: right; padding-right: 2%;">
							<strong id='shipping_value_text'>
								<?php echo "$".$order_details->shipping_cost;?></strong>
						</dd><hr class='hr' style="margin:5px 0px;">
					</dl>
				</div>
			</div>
            <dl class="dl-horizontal grand"style=" padding-top: 7px; text-align: right;  padding-right: 3%; font-size:1.2em;">
				<dt>
					<strong >Grand Total:</strong>
                </dt>
				<dd class='grand_total_dl' >
					<strong id='grand_total_value'>$<?php echo $sub_total + $order_details->shipping_cost;?></strong>
				</dd>
            </dl>
        </div>
		<div class="tab-content-panel" style="padding-left: 2%; padding-right:2%;">
			<hr class='hr_border'>
			<a href="<?php echo base_url('dashboard/my_dashboard/'.$profile_id);?>" class='btn btn-success btn-lg pull-right' style='margin-left:2%;'>Back </a>
        </div>
	</div>
</section>

  <footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

  </footer>

<?php if($user_is_seller==false) : ?>
  <style type="text/css">
  .modal .confirm_delivery_modal {
    width: 20%;
  }

  </style>
  <!-- confirm delivery modal -->

<div class="modal fade confirm_delivery_modal" id="confirm_delivery_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Confirm your delivery </h4>
      </div>
      <div class="modal-body">
       
        <img class='img img-responsive img-circle' width="80%" align="middle" style='margin-left: 10%;' src="<?php echo base_url().'assets/images/delivery.jpg';?>"/>
        <div class="delivery-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> Transaction compeleted!
          <p>We will also notify your seller.</p>
        </div>

        <div class="delivery_message alert alert-warning alert-dismissible" role="alert" style="display:block;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Are you satisfied with your delivery ?</strong>
          <p>We will also notify your seller.</p>
        </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" data-order-details-id="<?php echo $order_details->od_id;?>"  data-order-id="<?php echo $order_details->o_id;?>" class="btn btn-primary" id='yes_clicked'>Yes</button>
        <button type="button" data-order-details-id="<?php echo $order_details->od_id;?>"  data-order-id="<?php echo $order_details->o_id;?>" class="btn btn-danger" id='no_clicked'>NO</button>
      </div>
    </div>
  </div>
</div>

<?php endif; ?>

<?php if($user_is_seller==false) : ?>

 <!--shipping addreess -edit -->

  <!--start of modal window for add tracking number -->

<div class="modal fade shipping_address_modal" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Edit Shipping Address</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
         <!--show success message -->
        <div  class="shipping-address-edit-message alert alert-success alert-dismissible" role="alert" style='display:none;'>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> You have updated your shipping address succefully!
        </div>

       
       <div class="holder">

        <form  data-toggle="validator" class='form-inline' role="form" id='myShippingAddressForm'>
         <div class="form-group">
            <label for="first_name" class="control-label">First Name:</label>
            <input type="text" class="form-control"  data-error="Please enter first name" name="first_name_txt" id="first_name_txt" placeholder='enter your first name' required>
            <div class="help-block with-errors"></div>
          </div>
           <div class="form-group">
            <label for="last_name" class="control-label">Last Name:</label>
            <input type="text" class="form-control"  data-error="Please enter last name" name="last_name_txt" id="last_name_txt" placeholder='enter your last name' required>
            <div class="help-block with-errors"></div>
          </div>
           <div class="form-group">
            <label for="street_address_1" class="control-label">Street Address1:</label>
            <input type="text" class="form-control"  data-error="Please street address 1" name="street_address_1" id="street_address_1" placeholder='enter street addresse 2' required>
            <div class="help-block with-errors"></div>
          </div>
           <div class="form-group">
            <label for="street_address_2" class="control-label">Street Address 2:</label>
            <input type="text" class="form-control"  data-error="Please street address 2" name="street_address_2" id="street_address_2" placeholder='enter street addresse 2' >
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
            <label for="state" class="control-label">State:</label>
            <input type="text" class="form-control"  data-error="Please street address 2" name="state" id="state" placeholder='enter state'>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
            <label for="city" class="control-label">City:</label>
            <input type="text" class="form-control"  data-error="Please street city" name="city" id="city" placeholder='enter city'>
            <div class="help-block with-errors"></div>
          </div>


     
          <div class="form-group">
            <label for="message-text" class="control-label">Zip Code:</label>
            <input class="form-control"  id="zip_code" name="zip_code" data-error="Please enter valid zip code"  placeholder='enter zip code' required></input>
             <div class="help-block with-errors"></div>
          </div>
       
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-order-details-id="<?php echo $order_details->od_id;?>"  data-order-id="<?php echo $order_details->o_id;?>" id='update_now_btn'>
          Update now!
        </button>
      </div>
    </div><!-- /.modal-content -->

 
  </div><!-- /.modal-dialog -->

   </div> <!--modal holder -->
</div><!-- /.modal -->

  <!--end  of modal window -->

<?php endif; ?>


<?php if($user_is_seller==true) : ?>
  <!--start of modal window for add tracking number -->

<div class="modal fade add_tracking_modal" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Update Shipping Information</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
         <!--show success message -->
        <div  class="tracking-shipping-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> You have updated the order succefully!
          <p>We will also notify your buyer.</p>
        </div>


        <form  data-toggle="validator" role="form" id='myOrderDetailsForm'>
         <div class="form-group">
            <label for="tracking-number" class="control-label">Enter Shipping Tracking Number:</label>
            <input type="text" class="form-control"  data-error="Please enter tracking number" name="fedex_number_text" id="fedex_number_text" placeholder='enter tracking number' required>
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
            <label for="shipping-method" class="control-label">Shipping Method :</label>
            <?= form_dropdown('shipping_method_modal', $shipping_method ,isset($shipping_method->shipping_method) ? $shipping_method->shipping_method : 'Shipping Method Required','id="shipping_method_modal" class="form-control"  tabindex="7"  style="font-weight:bold;"' ) ;?>  
          </div>
       
          <div class="form-group">
            <label for="shipping-price" class="control-label">Total Shipping Price:</label>
            <input class="form-control"  id="shipping_price_text" data-error="Please enter total shipping price $" name="shipping_price_text" value="<?php echo $order_details->shipping_cost ;?>" placeholder='enter shipping total price' required></input>   
          </div>
       
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-order-details-id="<?php echo $order_details->od_id;?>" 
                id='add_now_btn'>Add now!</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <!--end  of modal window -->
<?php endif; ?>



 


  <!-- Bootstrap core JavaScript
================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script  src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
  <script src="<?php echo base_url()."assets/plugins/bootstrap/js/validator.min.js";?>"></script>
  <script src="<?php echo base_url()."assets/plugins/sweetalert/sweet-alert.js";?>"></script>

  <script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
		<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>
  <script type="text/javascript">

    /**global script variable to be taken out on global import file **/
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split( '/' );
    var base_url_complete =base_url+'/'+pathArray[1]+'/';
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
  

//add modal window javascript here

$( document ).ready(function() {

  //check order status
   var status = '<?php echo $order_details->ostatus == $this->config->item('order_status')['order_stage'];?>';

   var status_report = '<?php echo $order_details->ostatus == $this->config->item('order_status')['report_stage'];?>';
   //alert(status);
   if(!status) {

     $('#confirm-button').text('Transaction compeleted');
     $('#confirm-button').attr('disabled','true');
    }

    if(status_report) {

     $('#confirm-button').text('Being Inspected by Admin');
     $('#confirm-button').attr('disabled','true');
    }
 
 

  $('#yes_clicked').click(function(event){
   

                 var server_url = base_url_complete+"dashboard/update_buyer_satisfaction";
                 var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                 var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
                 var order_details_id = document.getElementById("yes_clicked").getAttribute("data-order-details-id");
                 var order_id = document.getElementById("yes_clicked").getAttribute("data-order-id");
                // alert("Please wait we are processing"); 
               
                 $.ajax({
                        
                        type:"POST",
                        url:server_url,
                        data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_id':order_id,'order_detail_id':order_details_id},
                        dataType: "json",
                        cache:false,
                        success:
                          function(data){

                                 if(data.success) {
                                  $('#confirm-button').text('Transaction compeleted');
                                  $('#confirm-button').attr('disabled','true');                                  
                                  $(".delivery_message ").css('display','none') ; 
                                  $("#yes_clicked").css('display','none') ;
                                  $("#no_clicked").css('display','none') ;
                                  $(".delivery-message ").css('display','block').fadeOut(1000);      
                                 
                                  setTimeout(function() { $(".confirm_delivery_modal").modal('hide');sweetAlert("Good Job!", "This transaction is now compeleted ", "success");},1000);
                                  

                               }
                    },

                                  
                });//end of ajax block 

          //setTimeout(function() { 
          //
      
  }); 


    $('#no_clicked').click(function(event){


                 var server_url = base_url_complete+"dashboard/report_transaction";
                 var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                 var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
                 var order_details_id = document.getElementById("no_clicked").getAttribute("data-order-details-id"); 
                 var order_id = document.getElementById("no_clicked").getAttribute("data-order-id");
                 $.ajax({
                        
                        type:"POST",
                        url:server_url,
                        data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_id':order_id,'order_detail_id':order_details_id},
                        dataType: "json",
                        cache:false,
                                  
                });//end of ajax block             

                  $('.confirm-button').text('Reported to admin');
                  $('.confirm-button').attr('disabled','true');

          setTimeout(function() {  $(".delivery_message ").css('display','none') ;
          $(".delivery-message").html('Transaction has been reported to admin.'); 
          $(".delivery-message ").css('display','block').fadeOut(6000);}, 2000);      
          setTimeout(function() { $("#yes_clicked").css('display','none') ;$("#no_clicked").css('display','none') ;}, 2000); 

  }); 


   /* swal({  
         title: "Are you satisfied with the delivery ?",   
         text: "To complete this transaction ,please give the seller <span style='color:#F8BB86'>Yes or No<span>.<p><a class='btn btn-info' id='close_window' onClick=document.getElementsByClassName('sweet-overlay')[0].style.display='none';document.getElementsByClassName('sweet-alert')[0].style.display='none'; value='closeX'>Click here to close [X]</a>",   
         //timer: 5000,
         animation: "slide-from-top",
         html:true,
         allowOutsideClick:true,
         type: "info",   
         showConfirmButton:true,
         showCancelButton: true,   confirmButtonColor: "rgb(51, 141, 218)",   
         confirmButtonText: "Yes,I m happy :)!",   
         cancelButtonText: "No,Am not happy :(!",   
         closeOnConfirm: false,   
         closeOnCancel: false 
       }, 


  function(isConfirm){ 

    if (isConfirm) {     
        
                 var server_url = base_url_complete+"dashboard/update_buyer_satisfaction";
                 var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                 var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
                 var order_details_id = document.getElementById("update_now_btn").getAttribute("data-order-details-id");
                 alert(order_details_id); 
               
                 $.ajax({
                        
                        type:"POST",
                        url:server_url,
                        data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_detail_id':order_details_id},
                        dataType: "json",
                        cache:false,
                        success:
                          function(data){

                                 if(data.success) {
                                  $('.btn-approve-shipping').text('Approved and compeleted');
                                  $('.btn-approve-shipping').attr('disabled','true');
                               }
                    },

                                  
                });//end of ajax block 

              setTimeout(function() { swal("Thanks!", "Your transaction is compeleted.", "success"); }, 2000);      
      

      } 
    /*else {     

       setTimeout(function() { 

                /*var server_url = base_url_complete+"dashboard/report_transaction";
                 var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                 var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
                 var order_details_id = document.getElementById("update_now_btn").getAttribute("data-order-details-id"); 
               
                 $.ajax({
                        
                        type:"POST",
                        url:server_url,
                        data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_detail_id':order_details_id},
                        dataType: "json",
                        cache:false,
                                  
                });//end of ajax block */


    //    swal("Cancelled", "Please , use chat or send message feature for communicating with ur seller , Thanks!", "error"); }, 2000);
  //    }

//  });



    
   $("#add_now_btn").click(function(event){
  //add now button 

      event.preventDefault();

        //$('#myOrderDetailsForm').validator();
      var tracking_number=$("#fedex_number_text").val();
      var shipping_cost=$("#shipping_price_text").val();
      var shipping_method= $("#shipping_method_modal").val();

      //alert(shipping_method);
      $('#myOrderDetailsForm').validator('validate');

    
     if( tracking_number=="" || shipping_cost=="" ||  $('#myOrderDetailsForm').validator().data('bs.validator').hasErrors() ) 
     {
         $('#myOrderDetailsForm').validator('validate');

    } else {

       var $t = $(this); //get this conetxt     
       //alert('accept btn');
       var server_url = base_url_complete+"dashboard/update_tracking_shipping_info";
       var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
       var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
       var order_details_id = document.getElementById((this).id).getAttribute("data-order-details-id"); 
     
            
            //if validation is OK! then call ajax
            $.ajax({
                type:"POST",
                url:server_url,
                data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_details_id':order_details_id,'tracking_number':tracking_number,'shipping_cost':shipping_cost,'shipping_method':shipping_method },
                dataType: "json",
                cache:false,
                success:
                 function(data){
                           if(data.success==true) {

                              $(".tracking-shipping-message").css('display','block');
                              $(".tracking_number").html('<strong>'+tracking_number+'</strong>');
                             //alert(shipping_method);
                              $(".shipping_method_text").html(shipping_method);
                              //start reecalaculate after the shipping cost updated
                              var subtotal = covert_currency_into_number('sub_total_value');
                              var scost = shipping_cost;
                              var grand_total = parseFloat(subtotal)+parseFloat(shipping_cost)
                              document.getElementById('grand_total_value').innerHTML = "";
                              document.getElementById('shipping_method').value = shipping_method;
                              document.getElementById('shipping_value_text').innerHTML = "$"+shipping_cost;
                              document.getElementById('grand_total_value').innerHTML = "$ "+grand_total;
                               $(".add_tracking_modal").modal('hide');
                               setTimeout(function() { sweetAlert("Good Job!", "Shipping information is now updated! ", "success");},1000);
                              /**end of calculation **/
                         }  
                    },
                error:
                    function(data) {
                     if(data.error_message) {
                         $('#tracking-shipping-message').html('<i class="glyphicon glyphicon-user"></i> Error:Unable to update tracking information').delay(2000);
                       }
                }
        });//end of ajax block  
} //end of validation else



});
//shipping code

$("#update_now_btn").click(function(event){
  //add now button 

      event.preventDefault();
      $(".holder").css('display','block').fadeIn(2000);

        //$('#myShippingAddressForm').validator();
      var fname=$("#first_name_txt").val();
      var lname=$("#last_name_txt").val();
      var street_address_1= $("#street_address_1").val();
      var street_address_2=$("#street_address_2").val();
      var state=$("#state").val();
      var city= $("#city").val();
      var zip_code= $("#zip_code").val();

      //alert(shipping_method);
      $('#myShippingAddressForm').validator('validate');
    
     if( $('#myShippingAddressForm').validator().data('bs.validator').hasErrors() ) 
     {
         $('#myShippingAddressForm').validator('validate');

    } else {

       var $t = $(this); //get this conetxt     
       //alert('accept btn');
       var server_url = base_url_complete+"dashboard/update_shipping_address";
       var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
       var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;  
       var order_id = document.getElementById((this).id).getAttribute("data-order-id"); 
       alert(order_id);
     
            
            //if validation is OK! then call ajax
            $.ajax({
                type:"POST",
                url:server_url,
                data:{madebyus4u_csrf_test_name:csrf_token_hash,'order_id':order_id,'first_name':fname,'last_name':lname,'street_address_1':street_address_1,'street_address_2':street_address_2,'city':city ,'state':state,'zip_code':zip_code},
                dataType: "json",
                cache:false,
                success:                
                function(data){

                           if(data.success==true) {
                              $(".shipping-address-edit-message").css('display','block');
                              $(".holder").css('display','none').fadeOut(2000);
                         }
                          sweetAlert("Good Job!", "succefully updated your shippment address! ", "success");
                },
                error:
                    function(data) {
                     if(data.error_message) {
                            $('#shipping-address-edit-message').html('<i class="glyphicon glyphicon-user"></i> Error:we are Unable to update your shipping information').delay(2000);
                       }
                }

        });//end of ajax block  


} //end of else


});


function covert_currency_into_number($currency_string_element_id) {
   var currency = document.getElementById($currency_string_element_id).innerHTML.toString();
   currency = currency.replace(/[^0-9\.]+/g, "");
   console.log(currency);
   return  parseFloat(currency);
}


});

</script>
</body>

</html>