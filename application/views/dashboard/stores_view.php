<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Sores</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/collective_common.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/store_tabs.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard_store_view.css";?> rel="stylesheet">

</head>


<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
        <div class="container">
        <div class="col-md-12">
         <div class="head-text" style="float:left; padding-top: 23px;">

               <span><h2><strong> Account</strong> <font color="grey">Overview</font></h2></span>

                </div>
                <div class="add-store-button need_help" style="float:right; padding-top: 49px;">
                   <strong><font color="grey"><u>Want to add another store?</u>  </font> </strong><a href="<?php echo base_url('dashboard/my_stores/add_store');?>" id="" class="btn btn-default btn-lg"> Add Store</a>
                </div>
            <hr class='col-md-12 hr'></div>
          
            <div class="dot"></div>
            
            <!--import side menu -->

             <?php $this->load->view($dashboard_sidemenu); ?>

        
            <div class="col-md-10">

                <div class="row add-store">
                    <div class="col-md-5 pull-left"><h4>Manage  (<?php echo count($all_store_data) ;?>)</strong> <font color="grey">Stores</font> </h4> </div>

                </div>

             <!--   <HR class='hr'>-->
              <!--   manage stores container begin -->
                <div class="manage-store">
                    <div class='row product-lisiting-pages'>


                        <?php  if(count($all_store_data)>0): ?>

                         <?php foreach($all_store_data as $store) : ?>

                            <div class="col-sm-4">


                                <div class="productsrow">
                                    <div class="product menu-category">
                                        <div class="menu-category-name list-group-item"><?php echo $store->store_name;?><input type="checkbox"  class="checkbox" id="check2" /></div>
                                        <div class="image-holder">
                                            &nbsp;

                                            <div class="numberCircle">
                                                <div class="height_fix"></div>
                                                <div class="content">

                                                    <p><font color="#606060"><strong><?php echo $store->store->number_of_products;?></strong></font></div>
                                            </div>

                     
                                            <p class="text-center"><font color="#606060"><strong>Listings</strong></font></p>
                                 
                                        </div>

                                        <div class="store-edit-buttons"> 
                                            <a href="<?php echo base_url('dashboard/my_stores/edit_store').'/'.$store->store_id;?>" class="edit-store-btn btn-sm btn-primary" role="button"><span>Edit</span></a>
                                            <a href="#" class="delete-store-btn btn-default btn-sm" role="button"><span>Delete</span></a>
                                      </div>

                                    </div>

                                </div>

                            </div>
                        <?php  endforeach; ?>
                        <?php  endif;?>

                    </div></div>
                   <!--  manage store container end -->


                <div class="clearfix"></div>

            </div>

        </div>



</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */

    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);


        var store_setup_completed = '<?php  echo $store_setup_completed; ?>';
            var  verified_identiy = '<?php  echo $tab_status; ?>';

</script>

<script type="text/javascript">
    

  $(document).ready(function(event){
      
     var p = $("#my_store_link").find('p');
     var images = $("#my_store_link").find('img');
     p.first().removeClass("gray").addClass("white");    
     images[0].src="<?php echo $image_path.'stores1.png';?>";
     $("#my_store_link").find('div').first().addClass('side-active');

    window.scrollTo(0, 140);
    var myDiv = $("div.sidebar-menu");
    var scrollto = myDiv.offset().top + (myDiv.height() / 2);
    myDiv.animate({ scrollTop:  scrollto});
   
 });
</script>
</body>
</html>
