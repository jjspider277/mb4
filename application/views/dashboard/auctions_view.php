<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Purchases</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">

    <link href=<?php echo base_url() ."assets/css/collective_common.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url() ."assets/css/store_tabs.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url() ."assets/css/dashboard_listing_view.css"; ?> rel="stylesheet">


</head>

<style type="text/css">
    .container-listing{
        background-color: white;
        width: 850px;;
    }
    /* new added for store name */
    .edit{
        background-color: red;
        color: white;
    }
  /*   .delete{
        color: white;
        background-color: blue;
    }
    .imag-panel{
        color: white;
        height: 24px;
        background-color: black;
    } */
    .productsrow {
        -moz-column-width: 18em;
        -webkit-column-width: 18em;
        -moz-column-gap: 1em;
        -webkit-column-gap: 1em;
    }
    .menu-category {
        display: inline-block;
        margin-bottom:  0.25rem;
        padding:  1rem;
        width:  54%;
    }
    .product-image {
        width: 100%;

    }
    .menu-category-name{
        background-color: black;
        width: 225px;

    }
    .product {
        padding-top:22px;
        color: white;
    }
    .checkbox{
        float: right;
    }
  /*   .delete-image{
        float: right;
        background-color: black;
        width: 60px;
        color: white;
        text-align: center;
    }
    .edit-image{
        float: left;
        text-align: center;
        background-color: #336699;
        width: 60px;
        color: white;
    } */
    .numberCircle {
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        marging-left: 32px;
        width: 80px;
        padding: 8px;
        font-size: 32px;
        line-height: 1em;
        border: 4px solid #336699;
        position: relative;
        vertical-align:middle;
        margin-left: auto;
        margin-right: auto;
        top: 12px;



    }
    .image-holder{
        background: grey;
        width: 225px;
        height: 150px;

    }
    .numberCircle .height_fix {
        margin-top: 100%;


    }
    .numberCircle .content {
        position: absolute;
        left: 0;
        top: 50%;
        height: 100%;
        width: 100%;
        text-align: center;
        margin-top: -15px; /* Note, this must be half the font size */
    }
    .container-auctions {
        background-color: white;

    }
    .form-group input {
        background: #F0F0E9;
        border: medium none;
        color: #696763;
        display: block;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 300;
        height: 40px;
        margin-bottom: 10px;
        outline: medium none;
        padding-left: 10px;
        width: 100%;

    }

</style>
<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>


</header>

<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
        <div class="container">
            <h2>Account Overview</h2>
            <div class="dot"></div>
                <!--import side menu -->

             <?php $this->load->view($dashboard_sidemenu); ?>
            
    <div class="main-row">
        <div class="container">

            <div class="col-md-10">

               <h4><strong>Set Auction</strong> for listing</h4>
                <HR class='hr'>
                <div class="row">

                    <div class="col-md-4">

                        <div class="thumbnail">
                            <img data-src="holder.js/100%x200" alt="100%x200"  class="img-thumbnail img-responsive"
                                 src=<?php echo base_url("assets/images/products/tshirt.jpg");?> data-holder-rendered="true" style="height: 200px;display: block;">
                            <div class="caption">
                                <h4>Dimonad Rings </h4>
                                <p><span class='price'>$66.99</span><span class='buy_btn pull-right'><a href="#" class="btn btn-primary" role="button">Buy Now</a></span></p>
                                <span class='span_buy_it_now'><p><a href="#">Buy It Now!</a></p></span>
                            </div>
                            <hr>

                            <p><span class='seller_name'><i class='glyphicon glyphicon-user'><b style="padding-left: 6px;color: #7F8B93;font-weight: 500;line-height: 2px;font-family: sans-serif;">Seller Name</b></span></i>
                                <span class='rating_class pull-right'><i class='glyphicon glyphicon-star'></i><i class='glyphicon glyphicon-star'></i><i class='glyphicon glyphicon-star'></i><i class='glyphicon glyphicon-star-empty'></i><i class='glyphicon glyphicon-star-empty'></i></span></span></p>
                        </div>



                    </div>

                    <div class="col-md-7 container-auctions">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="name">Name Of Auction</label>
                            <input type="text" class="form-control input-sm" id="name" name="name">
                        </div>
                    </div>


                    <div class="col-md-7">

                        <div class="form-group">
                            <label for="start-bid">starting Bid Prices</label>
                            <input type="text" class="form-control input-sm" id="start-bid" name="start-bid">
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="reserve-price">Reserve Price</label>
                            <input type="text" class="form-control input-sm" id="reserve-price" name="reserve-price">
                        </div>
                    </div>


                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="price">Buy Now Price</label>
                            <input type="text" class="form-control input-sm" id="price" name="price">
                        </div>
                    </div>
                    <div class="col-md-12"><hr></div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date">Start Date / Time</label>
                            <input type="date" class="form-control input-sm" 
                            id="date">
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            &nbsp;
                            <input type="text" class="form-control input-sm" id="date-blank" name="date-blank">
                            <label for="date-blank"><small>leave blank to start immediately</small></label>
                        </div>

                    </div>
                    <div class="col-md-12"><hr></div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date">End Date / Time</label>
                            <input type="date" class="form-control input-sm" id="date">

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            &nbsp;
                            <input type="text" class="form-control input-sm" id="date-blank" name="date-blank">
                            <label for="date-blank"><small>leave blank to start immediately</small></label>
                        </div>




                    </div>
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-6">

                        <div class='save'>
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class='launch pull-right'>
                            <button type="submit" class="btn btn-success btn-lg">Launch Auction</button>
                        </div>
                    </div>
</div>

                </div>
            </div>




            <div class="clearfix"></div>

        </div>

    </div>



</section>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<script type="text/javascript">
    

$(document).ready(function(event){
      
     var p = $("#my_auctions_link").find('p');
     var images = $("#my_auctions_link").find('img');
     p.first().removeClass("gray").addClass("white");    
     images[0].src="<?php echo $image_path.'auction1.png';?>";
     $("#my_auctions_link").find('div').first().addClass('side-active');
    
    window.scrollTo(0, 140);
    var myDiv = $("div.sidebar-menu");
    var scrollto = myDiv.offset().top + (myDiv.height() / 2);
    myDiv.animate({ scrollTop:  scrollto});

 });

</script>

</body>
</html>


