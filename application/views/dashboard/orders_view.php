<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Orders</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">
	 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	 <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


</head>

<style type="text/css">
.main-nav nav{
background-color: white;
}
</style>
<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>

     <?php $this->load->view($header_logo_white); ?>


  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
	 <div class="container"> 
		<h2>Account Overview</h2>
		<div class="dot"></div>
		
			<!--import side menu -->

			 <?php $this->load->view($dashboard_sidemenu); ?>
	
	 <?php if(count($sales_data)>0) :?>
		<div class="col-md-10 middle-section">
			<h3>Search For Orders</h3>
			<div class="dot"></div>
			<form class="form-inline search-form" role="form" method="" action="">

				<div class="form-group">
					<div class="input-group" style='width: 221px;'>
					
					<div class="input-group-addon input-group-date-picker">
						<img src="<?php echo $image_path.'date_picker.jpg';?>"></div>
						<input class='form-control' width="50%" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
					</div>
				  </div>	
			  <div class="form-group">
				<input type="text" class="form-control" id="invoiceId" placeholder="Invoice ID or Email">
			  </div>
			  <div class="form-group">
				 <?= form_dropdown('payment_status', array_merge(array('#'=>'All payment status'), $payment_status) ,'#','class="form-control"  tabindex="7"' ) ;?>
			     <?= form_dropdown('shippment_status', array_merge(array('#'=>'All shipment status'), $shippment_status) ,'#','class="form-control"  tabindex="7"' ) ;?>		
				
					<button type="submit" class="btn btn-primary">
						<img src="<?php echo $image_path.'search.png';?>">
					</button>			  
			  </div>		  
			</form>
			<h3>My Sales Orders</h3>
			<div class="dot"></div>
			<table class="table order-tbl">
				<thead>
					<tr>
					
					<th>Invoice #</th>
					<th>Date</th>
					<th>Customer</th>
					<th>Payment status</th>
					<th>Shipping status</th>
					<th>Amount</th>
					<th></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($sales_data as $sale): ?>
				   
					<?php $order_link = base_url().'/dashboard/order_detail/'.$sale->o_id; ?>
					<tr> <?php //var_dump($sale);exit;?>
						
						<th><a href="<?php echo base_url('dashboard/order_detail/'.$sale->o_id);?>"><?php echo $sale->invoice_id;?></a></th>
						<th><?php echo $sale->created_date;?></th>
						<?php if($sale->seller_id == $this->profile_id):?>
						<th><?php echo $sale->buyer_fname.' '.$sale->buyer_lname;?><br/> <span class="gray table-stext"><?php echo $sale->buyer_email;?></span></th>				
						 <?php else: ?>
						 <th><?php echo $sale->seller_fname.' '.$sale->seller_lname;?><br/> <span class="gray table-stext"><?php echo $sale->seller_email;?></span></th>				
						 <?php endif;?>
						<?php if($sale->seller_id == $this->profile_id):?>
						<th><span class="btn-green"><?php echo ucwords($sale->seller_payment_status);?></span></th>
					    <?php else: ?>
					     <th><span class="btn-green"><?php echo ucwords($sale->buyer_payment_status);?></span></th>
					    <?php endif;?>
						
						<th><span class="btn-green"><?php echo ucwords($sale->shipping_status);?></span></th>
						<th><?php echo $sale->price;?><br/> 
						<span class="gray table-stext"><?php echo $sale->quantity;?></span></th>
						<th><img src="<?php echo $image_path.'trash.jpg';?>"></th>
					</tr>
				<?php endforeach ;?>
				</tbody>
			</table>
		</div>
	<div class="clearfix"></div>	
	 </div>
	</div>
<?php else:?>
     <div class="col-md-8 middle-section">
     
     	<div class="alert alert-danger" role="alert"> <p><h4 style='text-align:center;'>We are sorry , You have no orders , relax for now!</h4> </p></div>
    </div>
	<?php endif;?>


</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


	
<script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>

<script type="text/javascript">
	

	$(document).ready(function(event){
    
  
     var p = $("#my_orders_link").find('p');
     var images = $("#my_orders_link").find('img');
     p.first().removeClass("gray").addClass("white");    
     images[0].src="<?php echo $image_path.'orders1.png';?>";
     $("#my_orders_link").find('div').first().addClass('side-active');

    window.scrollTo(0, 140);
	var myDiv = $("div.sidebar-menu");
	myDiv.animate({ scrollTop:  130});

  
 
   /*  $("#my_listing_link").find('div').first().removeClass('side-active');
     var images = $('#my_listing_link').find('img');
     console.log(images[0].src='http://localhost/madebyus4u/assets/images/dashboard/purchases.png');  
     var p = $("#my_listing_link").find('p');
     console.log(p.first().removeClass("white").addClass("gray"));
     //document.location.href = $(this).attr("href");*/
	});

</script>

<script type="text/javascript">
$(function() {
    $('input[name="daterange"]').daterangepicker();
});
</script>
</body>
</html>



