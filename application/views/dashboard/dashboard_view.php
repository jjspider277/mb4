<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Main Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">
    

</head>

<style type="text/css">
.main-nav nav{
background-color: white;
}
</style>
<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>

     <?php $this->load->view($header_logo_white); ?>


  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
		<div class="container"> 
			<h2>Account Overview</h2>
			<div class="dot"></div>
			<?php 

                 setlocale(LC_MONETARY, 'en_US');
               
			;?>

			<!--import side menu -->

			 <?php $this->load->view($dashboard_sidemenu); ?>

		<?php if(count($order_data)>0) :?> 
		<div class="col-md-8 middle-section">
			<table class="table order-tbl">
				<thead>
					<tr>
					<th>Order #</th>
					<th>Payment status</th>
					<th>Shipping status</th>
					<th>Customer</th>
					<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($order_data as $sale): ?>
				   
					<?php $order_link = base_url().'/dashboard/order_detail/'.$sale->o_id; ?>
					<tr> <?php //var_dump($sale);exit;?>						
						<th><a href="<?php echo base_url('dashboard/order_detail/'.$sale->o_id);?>"><?php echo $sale->invoice_id;?></a></th>					
						<th><span class="btn-green"><?php echo ucwords($sale->seller_payment_status);?></span></th>
						<th><span class="btn-green"><?php echo ucwords($sale->shipping_status);?></span></th>
							<th><?php echo $sale->buyer_fname.' '.$sale->buyer_lname;?><br/> <span class="gray table-stext"><?php echo $sale->email;?></span></th>				
						<th><?php echo money_format('%(#10n', $sale->price)."\n" ;?><br/> 
						<span class="gray table-stext"><?php echo $sale->quantity;?></span></th>

					</tr>
				<?php endforeach ;?>			
				</tbody>
			</table>

		</div>	
		<div class="col-md-2 right-sidebar">
			<div class="panel panel-default">
			  <div class="panel-heading panel-heading-blck">Sales Statistics</div>
			  <ul class="list-group">
				<li class="list-group-item">Revenue:<?php echo money_format('%(#10n', $revenu_sum)."\n";?></li>
				<li class="list-group-item">Orders: <?php echo $order_data[0]->orders_count;?></li>
				<li class="list-group-item">Money Owed: <?php echo money_format('%(#10n', doubleval($order_data[0]->money_owned))."\n" ;?></li>
			  </ul>
			</div>	
		</div>	
		<div class="clearfix"></div>	
	 </div>
	</div>

<?php else:?>
    <div class="col-md-8 middle-section">
			<table class="table order-tbl">
				<thead>
					<tr>
					<th>Order #</th>
					<th>Payment status</th>
					<th>Shipping status</th>
					<th>Customer</th>
					<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					
				   
					
					<tr> <?php //var_dump($sale);exit;?>						
						<th><a href="#"><?php echo "-"?></a></th>					
						<th><span class="btn-green">No Transaction</span></th>
						<th><span class="btn-green">No Transaction</span></th>
							<th><?php echo "-";?><br/> <span class="gray table-stext"><?php echo "-"?></span></th>				
						<th><?php echo "$0.00";?><br/> 
						<span class="gray table-stext"><?php echo "-"?></span></th>

					</tr>
						
				</tbody>
			</table>

		</div>	
		<div class="col-md-2 right-sidebar">
			<div class="panel panel-default">
			  <div class="panel-heading panel-heading-blck">Sales Statistics</div>
			  <ul class="list-group">
				<li class="list-group-item"><strong>Revenue: $0.00</strong></li>
				<li class="list-group-item"><strong>Orders: <?php echo "0.00";?></strong></li>
				<li class="list-group-item"><strong>Money Owed: $<?php echo "0.00";?></strong></li>
			  </ul>
			</div>	
		</div>	
		<div class="clearfix"></div>	
	 </div>
	</div>
	<?php endif;?>




</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script src=<?php echo base_url()."assets/plugins/picedit/picedit.js";?>> </script>

  <script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>

<script type="text/javascript">
	

	$(document).ready(function(event){
    
     var p = $("#dash_board_link").find('p');
     var images = $("#dash_board_link").find('img');
     p.first().removeClass("gray").addClass("white");    
     images[0].src="<?php echo $image_path.'dashboard1.png';?>";
     $("#dash_board_link").find('div').first().addClass('side-active'); 

     window.scrollTo(0, 140);

	});




</script>

</body>
</html>

