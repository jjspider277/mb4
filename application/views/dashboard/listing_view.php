<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>My Products</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">

	<link href=<?php echo base_url() ."assets/css/collective_common.css"; ?> rel="stylesheet">
	<link href=<?php echo base_url() ."assets/css/store_tabs.css"; ?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
   	<link href=<?php echo base_url()."assets/css/dashboard_listing_view.css";?> rel="stylesheet">
	

</head>
<style>

.img-thumbnail {
    opacity: 3.0;
    filter: alpha(opacity=100); /* For IE8 and earlier */
}


.img-thumbnail:hover {
  opacity: 0.4;
  background-color: #4682B4;

   background-image: url('../assets/images/zoom.png') s;
   
   
       
    filter: alpha(opacity=40); /* For IE8 and earlier */
   
}
.edit-delete-toggle {
 display: visible;
}
.edit-btn {
background-color: #3E3F3F;
color: white;
float: right;
border-radius: 2px;
}
</style>

<body>

<?php $this->load->view($notification_bar); ?>

<header>

	<?php $this->load->view($header_black_menu); ?>

	<?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	<div class="main-row">
		<div class="container">
	 <div class="col-md-12">
         <div class="head-text" style="float:left; padding-top: 23px;">
                       
               <span><h2><strong> Account</strong> <font color="grey">Overview</font></h2></span>
                                                   
                </div>
                <div class="head-text need_help" style="float:right; padding-top: 49px;">
                   <strong><font color="grey"><u>Want to add another item?</u>  </font> </strong><button type="submit" class="btn btn-default btn-lg"> Add a Listing</button>
                </div>
       
               <HR class='col-md-12 hr'>
                           </div>      
            <div class="dot"></div>
			<!--import side menu -->

			 <?php $this->load->view($dashboard_sidemenu); ?>
			 
			<div class="col-md-10 middle-section">
            <div class="view-product-listing">
				<h4><strong>Viewing all (<?php  var_dump($products); echo count($products) ;?>)</strong> listing</h4>
				<hr>
				<div class="dot"></div>
				</div>
				<div class='row product-liting-pages'>
					
					<div class="product-listing">
						<?php $this->load->view($product_listing_page); ?>
						<style type="text/css">
				.edit-delete-toggle {
 display: block;
}
						</style>
		
					</div>
					
			    </div>

								
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

</section>

<footer class="footer">

	<?php

	$this->load->view($footer_subscribe);
	$this->load->view($footer_privacy);
	?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script>
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
       
        $(".rating").rating('refresh', 
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {
          
          
           if(!readOnly) {
           //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");      
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

          }
           else {
            window.location.assign("<?=site_url('users/login');?>");
           }

          

        });

 
 
       
    });
</script>
<script type="text/javascript">
	/***
	 * Created by Daniel Adenew
	 * Submit email subscription using ajax
	 * Send email address
	 * Send contro  ller
	 * Recive response
	 */
	var url =  "<?php echo site_url('welcome/subscribe');?>";
	subscribe_using_ajax(url);
	//copied from store view

</script>

<script type="text/javascript">
	

 $(document).ready(function(event){
      
     var p = $("#my_listing_link").find('p');
     var images = $("#my_listing_link").find('img');
     p.first().removeClass("gray").addClass("white");    
     images[0].src="<?php echo $image_path.'listing1.png';?>";
     $("#my_listing_link").find('div').first().addClass('side-active');
    
    window.scrollTo(0, 140);
	var myDiv = $("div.sidebar-menu");
	var scrollto = myDiv.offset().top + (myDiv.height() / 2);
	myDiv.animate({ scrollTop:  scrollto});

	
	});



</script>
	

</body>
</html>
