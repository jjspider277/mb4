<style type="text/css">
.validation-text {

 color: #737373;

 margin-top: 5px;
 margin-bottom: 10px;
 margin-left: -87px;
}
.shipping-col {
    margin-left: 4%;
}
</style>   

<div class="col-md-12 col-md-store-content">

 
       <!-- my store setup begin-->
           
              <!--ERROR MESSAGE-->
              <style type="text/css">
              .error-message {
                margin-top: 1%;
         
              }
              </style>

              <div class="error-message col-md-12">  
                         
                <?php $data['message'] = $this->session->flashdata('message');?>     
                <?php $this->load->view($show_error_page,$data);?> 
              </div>
      <!--END OF ERROR MESSAGE-->


    <div class="col-md-12 pull-left">
        <p>
        <span class='tab_content_title'>Name your Store</span>
        <span class='tab_content_title pull-right'></span>

        </p>
    </div>

    <div class='row row-table-p col-md-12'>


     <small class="col-md-12 panel">Your store name appears in the mbu4u store community. Pick a name that relates to you or helps identify whats in your store.</small>

        <div class='col-md-6'>
            <div class="col-md-10 col-md-offset-1" style="margin-top:5%;">                                       
                
                <?php 
                    
                    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'UpdateStoreForm', 'name' => 'UpdateStoreForm',);
                    
                    $store_post_id= isset($store_id) ? $store_id : $store_id ; 
                  
                    echo form_open_multipart('dashboard/update_store/'.$store_post_id, $attributes);

                ?>
              
                <div class="form-group">
                  <div class="thumbnail col-md-8"> 
                   <img id = 'store_img' name="store_img" data-src="holder.js/100%x200" alt="100%x200" src="<?php echo $img_h;?>" data-holder-rendered="true" style="height: 170px; width: 100%; display: block;" data>
                     <div class="caption"> 
                            <button id="store_img_btn" data-imageid="store_img" type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#imageUpload" >
                              <span class=" glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            </button>
                            <button id="store_img_edit_btn" data-imageid="store_img" type="button" class="btn btn-default btn-lg">
                              <span class=" glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-lg">
                              <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 
                            </button>
                      </div> 
                     </div> 
                      
                </div>
                  


                <div class="col-md-12 col-md-select"  style="margin-top: 13px;">
                    <div class="form-group row">

                        <span class='required_star'>*</span>
                        <label for="storename">Edit Your Store Name</label>
                        <input type="text" id="storename" name='storename' value="<?php echo set_value('storename',$storename);?>" id='storename' length="10" class="form-control" required >
                        <p class="help-block">You can change your store name later.</p>
                    </div>


                </div>

                        
            </div>

        </div>

        <div class='col-md-5' style="margin-top:2%;">
            <div class="form-group">
                <span class='required_star'>*</span><label for="store_descritpion">Edit Your Store Description</label>
                <textarea class="form-control" name='store_description' id='store_description' rows="4"><?php echo set_value('store_description',$store_description);?> </textarea>
            </div>

        </div>
        
       
          <!--edit shipping address information-div with row -->   
       <!--
                <div class="row shipping-col col-md-12">

                   <label for="">Edit Your Shipping Information</label>
                         
                        
                          <p></p>
                          
                          <div class="form-group row"  style="font-weight:500;">

                                  <div class="col-xs-5">
                                    <input type="text" name="address_line_1" value="<?php echo set_value('address_line_1',$address_line_1);?>" id="address_line_1" class="form-control" placeholder="Address line 1">
                                  </div>
                                  <div class="col-xs-6">
                                    <input type="text" name="address_line_2" value="<?php echo set_value('address_line_2',$address_line_2);?>" id="address_line_2" class="form-control" placeholder="Address line 2">
                                  </div>                       
                        </div>

                           <div class="form-group row" style="font-weight:500;">

                                  <div class="col-xs-5">
                                    <input type="text" id="city" value="<?php echo set_value('city',$city);?>" name="city" class="form-control" placeholder="City">
                                  </div>
                                  <div class="col-xs-3">                        
                                      <?= form_dropdown('state', array_merge(array('#'=>'Please select state'), $states) ,'','class="form-control"  tabindex="-1" placeholder="State" style="color:grey;"' ) ;?>
                                  </div>
                                   <div class="col-xs-3">
                                    <input type="text" id="zip_code" value="<?php echo set_value('zip_code',$zip_code);?>" name="zip_code" class="form-control" placeholder="Zip/Postal Code">
                                  </div>
                                  
                                 

                        </div>

                         
                    </div>  
                    -->
                    
            <!--end of edit shipping address information-div with row -->   

        <hr  class='hr_store_form'>
        <div class="form-group col-md-2 col-md-offset-5">
            <a href='#' id='btn_store_next_page' class="btn btn-primary btn-lg">Update Store Info</a>                
        </div>
      
      </form> <!--end of store update form -->


    </div>


    </div>    

 