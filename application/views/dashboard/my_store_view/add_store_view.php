<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Sores</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/collective_common.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/store_tabs.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard_store_view.css";?> rel="stylesheet">

</head>
<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
        <div class="container">
       
         <div class="col-md-12">
            
              <div class="head-text" style="float:left; padding-top: 23px;">

                   <span><h2><strong>Account</strong> <font color="grey">Overview</font></h2></span>

              </div>
                
            <hr class='col-md-12 hr'>

          </div>

           <div class="dot"></div>
            
            <!--import side menu -->

             <?php $this->load->view($dashboard_sidemenu);?>

        
            <div class="col-md-10">

                <div class="row add-store">
                    <div class="col-md-5 pull-left"><h4>Manage </strong> <font color="grey">Store</font> </h4> </div>

                </div>
                       
                <div class="col-md-10 store-tabs">
                    <section id="responsive" style="background-color:#f5f5f5;">

                            <div role="tabpanel" class="container-listing"   style="padding-right: 12px;">

                                <div class="row-storesetup row">
                                    <div id="divsetup-content" name="divsetup-content" class="divsetup-content col-md-12" style="display:block;">
                                        <h2 style="font-size: 20px;font-weight: 600;">Add <span style='font-weight: 400;'><font color="grey">Store</font></span>
                                           
                                           
                                            <sup id='tab_lables_sup'class='tab_lables_sup' id='verify-text'><?php echo $tab_status==true ? "" :"- Step 1. Validate Identity"  ;?></sup>
                                            <small class='pull-right' style='color:rgb(216, 62, 62);margin-top: 8px;font-size: small;'>* ( Mandatory filed )</small>  </h3>
                                             <hr class="hr_border">                                        
                                             
                                             <div role="tabpanel" class="tabpane-storesetup">
                                                <!-- Nav tabs -->

                                                <div role="tabpanel" class="tab-pane active" id="store">
                                                <ul class="tabs nav nav-pills" role="tablist" id='createNotTab'>
                                                    <!--this is a form for all tab submission after verfiication-->

                                                    <li role="presentation"><a href="#store-tab" aria-controls="store-tab" class="first-store" role="tab" data-toggle="tab">1. Store Name<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#product-tab" aria-controls="product-tab" role="tab" data-toggle="tab">2. Listing<i class="fa listing"></i></a></li>
                                                    <li role="presentation"><a href="#getpaid-tab" aria-controls="getpaid-tab" role="tab" data-toggle="tab">3. Get Paid<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#openstore-tab" aria-controls="openstore-tab" role="tab" data-toggle="tab" >4. Preview Store<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#launchstore-tab" aria-controls="launchstore-tab" role="tab" data-toggle="tab" >5. Launch Store<i class="fa"></i></a></li>

                                                </ul>

                    <?php 
                    
                    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'addStoreForm', 'name' => 'addStoreForm',);
                    $form_post_id= isset($profile_id) ? $profile_id : $profile->id ; 
                  
                    echo form_open_multipart('store/save_store/'.$form_post_id, $attributes);

                    ?>
                     <?php echo "<input type='hidden' name='source' id='source' value='addStoreForm' />"; ?>
                          
                                         <div class="tab-content">
                                                    <!--DISABLE TAB -->
                         <!-- listing for product tab end-->

                                                    <div role="tabpanel" class="tab-pane fade in tab-pane active" id="store-tab"><?php $this->load->view($store_page); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="product-tab"><?php $this->load->view($addproduct_page); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="getpaid-tab"><?php $this->load->view($getpaid_page); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="openstore-tab"><?php $this->load->view($previewstore_page); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="launchstore-tab"><?php $this->load->view($launchstore_page); ?></div>

                                                </div>
                                            </div>
                                   
                                    </div>
                                </div>

                                &nbsp;
                            </div>
                    </section>

                </div>
             <!--  my store tab end -->
                


                <div class="clearfix"></div>

            </div>

        </div>



</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Placed at the end of the document so the pages load faster -->
            <script src="<?php echo base_url() . "assets/plugins/jquery/jquery.min.js"; ?>"></script>
            <!-- Latest compiled and minified JavaScript -->
            <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/tooltip.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/bootstrap.min.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/subscribe_ajax.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/multiple_image_uploads.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/bootstrapValidator.js"; ?>"></script>
            <script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script>
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $(".rating").rating('refresh',
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {


           if(!readOnly) {
           //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

          }
          else {

             window.location.assign("<?=site_url('users/login');?>");
           }

        });




    });
</script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */

    $(document).ready(function(){
        $(document).on('click','.delete-image',function(){
              $('.product-listing').show();
            return false;
        });

    });


    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);


    /*copied from store setup begin*/
    $(document).ready(function() {
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    });

    /*copied from store-setup end*/
    $(document).ready(function(){
        $('.add-product').hide();
        $(document).on('click','.add-store-button',function(){
            $('.manage-store,.add-store-button,.add-product').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });

        $(document).on('click','.edit-store-image',function(){
            $('.manage-store,.add-store-button').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });



        $(document).on('click','.add-listing',function(){
            $('.manage-store,.add-store-button').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });



        $(document).on('click','.edit-image',function(){
            $('.store-tabs,.add-store').hide();
            $('.add-product,.add-store-button').show();
            return false;
        });

    });

</script>
<!--copied from store setup-->
<script type="text/javascript">


    function disable_tabs_when_completed() {
        var store_setup_completed = '<?php  echo $store_setup_completed; ?>';;

        if (store_setup_completed) {
            $('.nav-pills a[href="#' + 'launchstore-tab' + '"]').tab('show');
            $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
        } else {
            $('.nav-pills a[href="#' + 'verify-tab' + '"]').tab('show');
            $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
        }
    }
    function misellenous_help_preview_store_and_image() {
        //change the produt name div conetnt to same as this one
        // $('.product_name').text($(e.currentTarget).data('asdasdasd');
        $("#product_name").change(function(e) {
            $('#product_name_h4').text($(this).val());
        });
        $("#product_descritpion").change(function(e) {
            $('#descritpion_paragraph').text($(this).val());
        });
        $("#price").change(function(e) {
            $('#price_tag_label').text("$" + $(this).val());
        });
        $("#category").change(function(e) {
            $('#category_label').text($(this).val());
        });
        //display the image on previw store page as
        $(document).on('change', '#product_image', function(event) {
            var output = document.getElementById("product_preview_image");
            output.src = URL.createObjectURL(event.target.files[0]);
        });
    }
    function change_labels_when_tabs_activated() {
    
      $('a[href="#verify-tab"]').click(function() {
            var  verified_identiy = '<?php  echo $tab_status; ?>';
            if(verified_identiy){
                $('#tab_lables_sup').empty();
                $('#tab_lables_sup').append(document.createTextNode(""));
            } else {

                $('#tab_lables_sup').empty();
                $('#tab_lables_sup').append(document.createTextNode("- Step 1. Validate Identity"));
            }
        });

        $('a[href="#store-tab"]').click(function() {
            alert('hi');
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 1. Create Store"));
        });

        $('a[href="#product-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 2. Add Product Lisiting"));
        });

        $('a[href="#getpaid-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 3. Get Paid Wire Your Account"));
        });

        $('a[href="#openstore-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 4. Preview Your Store"));

        });

        $('a[href="#launchstore-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 5. Launch Your Store"));
        });
    }
    $( document ).ready(function() {
        disable_tabs_when_completed();
        misellenous_help_preview_store_and_image();
        change_labels_when_tabs_activated();
        //disable tabs not active so that enable them when validated

        /**
         this a continue button after successfull identity verfication
         **/
        $('#btn_next_page').click( function() {

            $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');

        });
        //next page for store
        $('#btn_store_next_page').click( function() {
            var self = this;


            //TODOD: check store image file attached or not
            $store_image_status = $("#imgfile_store").val();

            if( ($store_image_status!=undefined) & ($store_image_status!='') )  {

                self.store_image = true;
                $('#imgfile_store').closest('.form-group').removeClass('has-error').addClass('has-success');
                $('#store_validation_message').css('color','#3c763d');

            } else if($store_image_status==undefined || $store_image_status=='') {
                self.store_image = false;
                $('#imgfile_store').closest('.form-group').removeClass('has-success').addClass('has-error');
                $('#store_validation_message').css('color','#a94442');
            }


            if(self.store_image==true) {
                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }


        });
        //next page for product
        $('#btn_product_next_page').click( function() {

            var self = this;

            //TODOD: check store image file attached or not
            $product_image_status = $("#product_image").val();

            if( ($product_image_status != undefined) & ($product_image_status != '') )  {

                self.product_image=true;
                $('#preview_product_image').css('border','1px solid #3c763d');
                $(".product_validation_message").css('color','#3c763d');

            } else if( $product_image_status == undefined || $product_image_status == '' ) {

                // self.product_image=false;
                $('#preview_product_image').css('border','1px solid #a94442');
                $(".product_validation_message").css('color','#a94442');
            }


            if(self.product_image == true ) {

                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }

        });
        //next page for store
        $('#btn_next_page_getpaid').click( function() {

            var self = this;
            self.isValid=true;


            if(self.isValid==true) {
                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }

        });

        //call function for image upload
        // multiple_image_upload();

    });


    /***
     * Created by Daniel Adenew , 2014
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Receive response
     */
    var url =  "<?php echo site_url('welcome/subscribe'); ?>";
    subscribe_using_ajax(url);

    $('#btnstoresetup').click( function() {
        $('#divstoresetup').hide("400",function(){
            // alert( "Animation complete." );
            // $('#divsetup-content').css('display','block');
            window.location="<?php echo base_url('store'); ?>";
        });
    }) ;
    /*care for cascading drop down boxes **/
    $('#category').change(function(){
        var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

        var categorey_selected = $("#category option:selected").text();

        $("#variation > option").remove(); //first of all clear select items
        // alert(categorey_selected);
        $.ajax({
            url: "<?php echo base_url('store/get_categories_variation_ajax'); ?>",
            async: false,
            cache: false,
            data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected},
            type: 'post',
            datatype:'json',
            success: function(variation_array_json){
                // alert(variation_array_json);
                $.each(variation_array_json,function(id,value) //here we're doing a foeach loop round each city with id as the key and city as the value
                {

                    var opt = $('<option />'); // here we're creating a new select option with for each city
                    opt.val(value);
                    opt.text(value);
                    $('#variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                });
                $('#variation').val('#');
            }
        });
        //sub variation
        $('#variation').change(function(){
            var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

            var variation_selected = $("#variation option:selected").text();
            var categorey_selected = $("#category option:selected").text();

            $("#sub_variation > option").remove(); //first of all clear select items
            // alert(categorey_selected);
            $.ajax({
                url: "<?php echo base_url('store/get_sub_variation_ajax'); ?>",
                async: false,
                cache: false,
                data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected,variation:variation_selected},
                type: 'post',
                datatype:'json',
                success: function(variation_array_json){
                    //alert(variation_array_json);
                    $.each(variation_array_json,function(id,value) //here we're doing a foreach loop round each category with id as the key and city as the value
                    {

                        var opt = $('<option />'); // here we're creating a new select option with for each city
                        opt.val(value);
                        opt.text(value);
                        $('#sub_variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                    });
                    $('#sub_variation').val('#');
                }          });
        });
    });

</script>


<script type="text/javascript">
    

  $(document).ready(function(event){
      
     var p = $("#my_store_link").find('p');
     var images = $("#my_store_link").find('img');
     console.log(p.first().removeClass("gray").addClass("white"));    
     console.log(images[0].src="<?php echo $image_path.'stores1.png';?>");
     $("#my_store_link").find('div').first().addClass('side-active');
 });

</script>
</body>
</html>
