<div class="col-md-12 col-md-store-content">
    <div class="col-md-12 pull-left">
        <p>
        <span class='tab_content_title'> Edit Your Products</span>

        </p>
    </div>
   
<style type="text/css">
  
.xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
.thumbnail .caption h4 {
  margin-top: 0px;
  font-size: larger;
  overflow: hidden;
}
.delete_btn {
   background-color: #3E3F3F;
  color: white;
  float:right;
  border-radius: 2px;
}
.thumbnail .caption {
  padding: 2px;
}
 .rating_class {
  float: right;
}
.rating-xs {
  font-size: 1.6em;
}

.rating-gly-star {
  font-family: 'Glyphicons Halflings';
  padding-left: 2px;
  font-size: smaller;
  }

.star-rating .caption, .star-rating-rtl .caption {
  color: #999;
  display: inline-block;
  vertical-align: middle;
  float: right;
  font-size: 80%;
}

.edit-delete-toggle {
 display: none;
}

.buttons .btn {
    color: white;
    text-transform: uppercase;
    border-radius: 2px;
    font-size: 10px;
    font-style: normal;
    line-height: 10px;
    font-weight: 600;
}

.btn-delete{
     background-color: #777777;
    border-color: #F5F5F5;
}

</style>

<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php if(count($products)>0): ?>

    <?php foreach($products as $product): ?>

      <div class="col-sm-4 col-md-4">

            <div class="thumbnail">
                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>">

                        <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $product['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: 200px;'>
               </a>
                   
                 <div class="caption">
                       <h4 class='product_name_heading'><?php echo $product['name'];?></h4>
                       <p class="product_description"><?php echo $product['desc'];?></p>
                        <p>
                          <span class='price'>$<?php echo $product['price'];?></span>
                          <span class='buy_btn viewDetails'> 
                            <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>" class="btn btn-primary" role="button">View Details</a>
                          </span>
                      </p>
                      <hr>

                  <p>
                   <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['product_id'];?>" value="<?= $product['product_rating'] ;?>" data-type="products" />                   </p>
                  <hr>
                  <p class='buttons'>
                   <a href="<?php echo base_url('dashboard/my_store/edit_store/edit_lisiting/').'/'.$product['product_id'];?>" class="btn btn-default btn-default btn-primary btn-sm " role="button">Edit</a>
                   <a href="<?php echo base_url().'dashboard/set_auction/'.$product['product_id'] ?>" class="btn btn-sm pull-right" role="button" style="border-color: #777777;background-color:#777777">Delete</a> 
                  </p>
                  
              </div>          
            </div>

      </div>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>

</div>
      
    </div>
</div>