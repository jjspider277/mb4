<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Edit Sores</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard.css";?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/collective_common.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url() . "assets/css/store_tabs.css"; ?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/dashboard_store_view.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/picedit/css/font-css.css";?> rel="stylesheet">
   <link href=<?php echo base_url()."assets/plugins/picedit/css/picedit.min.css";?> rel="stylesheet">
 
</head>
<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>


</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <div class="main-row">
        <div class="container">
       
         <div class="col-md-12">
            
              <div class="head-text" style="float:left; padding-top: 23px;">

                   <span><h2><strong>Account</strong> <font color="grey">Overview</font></h2></span>
                 
              </div>
              <div class="head-text need_help add_lisiting_button" style="float:right; padding-top: 49px;display:none;">
                   <strong><font color="grey"><u>Want to add another item?</u>  </font> </strong><button type="submit" class="btn btn-default btn-lg"> Add a Listing</button>
                </div>
                
            <hr class='col-md-12 hr'>

          </div>

           <div class="dot"></div>
            
            <!--import side menu -->

             <?php $this->load->view($dashboard_sidemenu);?>

        
            <div class="col-md-10">

                <div class="row add-store">
                    <div class="col-md-5 pull-left"><h4>Manage </strong> <font color="grey">Store</font> </h4> </div>

                </div>
                       
                <div class="col-md-10 store-tabs">
                    <section id="responsive" style="background-color:#f5f5f5;">

                            <div role="tabpanel" class="container-listing"   style="padding-right: 12px;">

                                <div class="row-storesetup row">
                                    <div id="divsetup-content" name="divsetup-content" class="divsetup-content col-md-12" style="display:block;">
                                        <h2 style="font-size: 20px;font-weight: 600;">Edit <span style='font-weight: 400;'><font color="grey">Store</font></span>
                                           
                                           
                                            <sup id='tab_lables_sup'class='tab_lables_sup' id='verify-text'><?php echo $tab_status==true ? "" :"- Edit Store"  ;?></sup>
                                            <small class='pull-right' style='color:rgb(216, 62, 62);margin-top: 8px;font-size: small;'>* ( Mandatory filed )</small>  </h3>
                                             <hr class="hr_border">                                        
                                             
                                             <div role="tabpanel" class="tabpane-storesetup">
                                                <!-- Nav tabs -->

                                                <div role="tabpanel" class="tab-pane active" id="store">
                                                <ul class="tabs nav nav-pills" role="tablist" id='createNotTab'>
                                                    <!--this is a form for all tab submission after verfiication-->

                                                    <li role="presentation"><a href="#store-tab" aria-controls="store-tab" class="first-store" role="tab" data-toggle="tab">1. Store Name<i class="fa"></i></a></li>
                                                    <li role="presentation"><a href="#product-tab" aria-controls="product-tab" role="tab" data-toggle="tab">2. Listing<i class="fa listing"></i></a></li>
                                                    <li role="presentation"><a href="#getpaid-tab" aria-controls="getpaid-tab" role="tab" data-toggle="tab">3. Get Paid<i class="fa"></i></a></li>

                                                    <li role="presentation"><a href="#launchstore-tab" aria-controls="launchstore-tab" role="tab" data-toggle="tab" >4. Launch Store<i class="fa"></i></a></li>

                                                </ul>

                    
                          
                                         <div class="tab-content">
                                                    <!--DISABLE TAB -->
                         <!-- listing for product tab end-->

                                                    <div role="tabpanel" class="tab-pane fade in tab-pane active" id="store-tab"><?php $this->load->view($store_page); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="product-tab"><?php $this->load->view($edit_product_lisiting); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="getpaid-tab"><?php $this->load->view($getpaid_page); ?></div>
                                                   
                                                    <div role="tabpanel" class="tab-pane fade tab-pane" id="launchstore-tab"><?php $this->load->view($launchstore_page); ?></div>

                                                </div>
                                            </div>
                                   
                                    </div>
                                </div>

                                &nbsp;
                            </div>
                    </section>

                </div>
             <!--  my store tab end -->
                


                <div class="clearfix"></div>

            </div>

        </div>



</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Placed at the end of the document so the pages load faster -->
            <script src="<?php echo base_url() . "assets/plugins/jquery/jquery.min.js"; ?>"></script>
            <!-- Latest compiled and minified JavaScript -->
            <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/tooltip.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/bootstrap.min.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/subscribe_ajax.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/multiple_image_uploads.js"; ?>"></script>
            <script src="<?php echo base_url() . "assets/js/bootstrapValidator.js"; ?>"></script>
            <script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
            <script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
             <script src=<?php echo base_url()."assets/plugins/picedit/picedit.js";?>> </script>
<script>

/***global base url path in javascript***/
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split('/');
  var base_url_complete = base_url+'/'+pathArray[1]+'/';
  var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ; 
  var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

   // $('.lightbox').lightBox();

 
  /***gloabl base_url path_end***/
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $(".rating").rating('refresh',
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {


           if(!readOnly) {
           //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

          }
          else {

             window.location.assign("<?=site_url('users/login');?>");
           }

        });




    });
</script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send contro  ller
     * Recive response
     */

    $(document).ready(function(){
        $(document).on('click','.delete-image',function(){
              $('.product-listing').show();
            return false;
        });

    });


    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);


    /*copied from store setup begin*/
    $(document).ready(function() {
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    });

    /*copied from store-setup end*/
    $(document).ready(function(){
        $('.add-product').hide();
        $(document).on('click','.add-store-button',function(){
            $('.manage-store,.add-store-button,.add-product').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });

        $(document).on('click','.edit-store-image',function(){
            $('.manage-store,.add-store-button').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });



        $(document).on('click','.add-listing',function(){
            $('.manage-store,.add-store-button').hide();
            $('.store-tabs,.add-listing').show();
            return false;
        });



        $(document).on('click','.edit-image',function(){
            $('.store-tabs,.add-store').hide();
            $('.add-product,.add-store-button').show();
            return false;
        }); 

    });

</script>
<!--copied from store setup-->
<script type="text/javascript">


    function disable_tabs_when_completed() {
        var store_setup_completed = '<?php  echo $store_setup_completed; ?>';;

        if (store_setup_completed) {
            $('.nav-pills a[href="#' + 'launchstore-tab' + '"]').tab('show');
            $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
        } else {
            $('.nav-pills a[href="#' + 'verify-tab' + '"]').tab('show');
            $('.nav-pills li').not('.active').find('a').removeAttr("data-toggle");
        }
    }
    function misellenous_help_preview_store_and_image() {
        //change the produt name div conetnt to same as this one
        // $('.product_name').text($(e.currentTarget).data('asdasdasd');
        $("#product_name").change(function(e) {
            $('#product_name_h4').text($(this).val());
        });
        $("#product_descritpion").change(function(e) {
            $('#descritpion_paragraph').text($(this).val());
        });
        $("#price").change(function(e) {
            $('#price_tag_label').text("$" + $(this).val());
        });
        $("#category").change(function(e) {
            $('#category_label').text($(this).val());
        });
        //display the image on previw store page as
        $(document).on('change', '#product_image', function(event) {
            var output = document.getElementById("product_preview_image");
            output.src = URL.createObjectURL(event.target.files[0]);
        });
    }
    function change_labels_when_tabs_activated() {
    
      $('a[href="#verify-tab"]').click(function() {
            var  verified_identiy = '<?php  echo $tab_status; ?>';
            if(verified_identiy){
                $('#tab_lables_sup').empty();
                $('#tab_lables_sup').append(document.createTextNode(""));
            } else {

                $('#tab_lables_sup').empty();
                $('#tab_lables_sup').append(document.createTextNode("- Step 1. Validate Identity"));
            }
        });

        $('a[href="#store-tab"]').click(function() {
           $(".add_lisiting_button").css('display',"none");
            $('#tab_lables_sup').empty();
            $('#tab_lables_sup').append(document.createTextNode("- Step 1. Create Store"));
        });

        $('a[href="#product-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
     
            $(".add_lisiting_button").css('display',"block");
            $('#tab_lables_sup').append(document.createTextNode("- Step 2. Add Product Lisiting"));
        });

        $('a[href="#getpaid-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
             $(".add_lisiting_button").css('display',"none");
            $('#tab_lables_sup').append(document.createTextNode("- Step 3. Get Paid Wire Your Account"));
        });

       /* $('a[href="#openstore-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
             $(".add_lisiting_button").css("display","none");
            $('#tab_lables_sup').append(document.createTextNode("- Step 4. Preview Your Store"));

        });*/

        $('a[href="#launchstore-tab"]').on('shown.bs.tab', function (e) {
            $('#tab_lables_sup').empty();
             $(".add_lisiting_button").css('display',"none");
            $('#tab_lables_sup').append(document.createTextNode("- Step 4. Launch Your Store"));
        });
    }
    $( document ).ready(function() {
       // disable_tabs_when_completed();
        //misellenous_help_preview_store_and_image();
        change_labels_when_tabs_activated();
        //disable tabs not active so that enable them when validated

        /**
         this a continue button after successfull identity verfication
         **/
        $('#btn_next_page').click( function() {

            $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');

        });



        //next page for store
        $('#btn_store_next_page').click( function(event) {
           
            event.preventDefault(); //prevent href event or click 

            //TODO:load 

           
            //TODOD: check store image file attached or not
            $store_image_status = "asd";
            
            if( ($store_image_status!=undefined) & ($store_image_status!='') )  {

                self.store_image = false;
                $('#imgfile_store').closest('.form-group').removeClass('has-error').addClass('has-success');
                $('#store_validation_message').css('color','#3c763d');

                //call ajax update
                $("#UpdateStoreForm").submit();

            } else if($store_image_status==undefined || $store_image_status=='') {
                self.store_image = false;
                $('#imgfile_store').closest('.form-group').removeClass('has-success').addClass('has-error');
                $('#store_validation_message').css('color','#a94442');
            }


            if(self.store_image==true) {
                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }


        });
        //next page for product
        $('#btn_product_next_page').click( function() {

            var self = this;

            //TODOD: check store image file attached or not
            $product_image_status = $("#product_image").val();

            if( ($product_image_status != undefined) & ($product_image_status != '') )  {

                self.product_image=true;
                $('#preview_product_image').css('border','1px solid #3c763d');
                $(".product_validation_message").css('color','#3c763d');

            } else if( $product_image_status == undefined || $product_image_status == '' ) {

                // self.product_image=false;
                $('#preview_product_image').css('border','1px solid #a94442');
                $(".product_validation_message").css('color','#a94442');
            }


            if(self.product_image == true ) {

                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }

        });
        //next page for store
        $('#btn_next_page_getpaid').click( function() {

            var self = this;
            self.isValid=true;
            if(self.isValid==true) {
                $('.nav-pills > .active').next('li').find('a').attr("data-toggle","tab").trigger('click');
            }

        });

        //call function for image upload
        // multiple_image_upload();

    });


    /***
     * Created by Daniel Adenew , 2014
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Receive response
     */
    var url =  "<?php echo site_url('welcome/subscribe'); ?>";
    subscribe_using_ajax(url);

    $('#btnstoresetup').click( function() {
        $('#divstoresetup').hide("400",function(){
            // alert( "Animation complete." );
            // $('#divsetup-content').css('display','block');
            window.location="<?php echo base_url('store'); ?>";
        });
    }) ;
    /*care for cascading drop down boxes **/
    $('#category').change(function(){
        var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

        var categorey_selected = $("#category option:selected").text();

        $("#variation > option").remove(); //first of all clear select items
        // alert(categorey_selected);
        $.ajax({
            url: "<?php echo base_url('store/get_categories_variation_ajax'); ?>",
            async: false,
            cache: false,
            data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected},
            type: 'post',
            datatype:'json',
            success: function(variation_array_json){
                // alert(variation_array_json);
                $.each(variation_array_json,function(id,value) //here we're doing a foeach loop round each city with id as the key and city as the value
                {

                    var opt = $('<option />'); // here we're creating a new select option with for each city
                    opt.val(value);
                    opt.text(value);
                    $('#variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                });
                $('#variation').val('#');
            }
        });
        //sub variation
        $('#variation').change(function(){
            var csrf = $('input[name="madebyus4u_csrf_test_name"]').val();  // <- get token value from hidden form input

            var variation_selected = $("#variation option:selected").text();
            var categorey_selected = $("#category option:selected").text();

            $("#sub_variation > option").remove(); //first of all clear select items
            // alert(categorey_selected);
            $.ajax({
                url: "<?php echo base_url('store/get_sub_variation_ajax'); ?>",
                async: false,
                cache: false,
                data: {madebyus4u_csrf_test_name:csrf,category:categorey_selected,variation:variation_selected},
                type: 'post',
                datatype:'json',
                success: function(variation_array_json){
                    //alert(variation_array_json);
                    $.each(variation_array_json,function(id,value) //here we're doing a foreach loop round each category with id as the key and city as the value
                    {

                        var opt = $('<option />'); // here we're creating a new select option with for each city
                        opt.val(value);
                        opt.text(value);
                        $('#sub_variation').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                    });
                    $('#sub_variation').val('#');
                }          });
        });
    });

</script>


<script type="text/javascript">
    

  $(document).ready(function(event){
      
     var p = $("#my_store_link").find('p');
     var images = $("#my_store_link").find('img');
     console.log(p.first().removeClass("gray").addClass("white"));    
     console.log(images[0].src="<?php echo $image_path.'stores1.png';?>");
     $("#my_store_link").find('div').first().addClass('side-active');

       //next page for store
                        $('#btn_get_paid').click( function() {

                          //prepare ajax
                          var $this = $(this);

                          var account_type = $("#account_type option:selected" ).text();
                          var bankbranch = $("#bankbranch").val();
                          var account_owner = $("#account_owner").val();
                          var routenumber = $("#routenumber").val();
                          var accountnumber = $("#accountnumber").val();

                             
                          $url = base_url_complete+'dashboard/update_get_paid_ajax';
                          $.ajax({
                              url: $url,
                              data: ({'madebyus4u_csrf_test_name':csrf_hash,"account_type":account_type,"bankbranch":bankbranch,"account_owner":account_owner,"routenumber":routenumber,"accountnumber":accountnumber}),
                              dataType: 'json', 
                              type: "post",
                              success: function(data){
                                         //response = jQuery.parseJSON(data); 
                                         //TODOD:CHECK RESULT      

                                         alert(data.success);     
                                         
                                       }             
                          });

    
           

                        });

 });

</script>


<!-- Modal -->
<div class="modal fade" id="imageUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog" role="document">
  
    <div class="modal-content">
         
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Image Upload</h4>
          </div>

      <div>
     
      <form></form>
     
      <div contenteditable="true" style="position: absolute; left: -999px; width: 0px; height: 0px; overflow: hidden; outline: 0px; opacity: 0;"></div>
     <?php 
                    
    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'myForms', 'name' => 'myForms',);
           
     echo form_open_multipart('store/post_image/', $attributes);
    ?>

<input type="hidden" name="img_src_id" id="img_src_id">

<div style="margin:10% auto 0 auto; display: table;">

 <!-- begin_picedit_box -->
 <div class="picedit_box">
    <!-- Placeholder for messaging -->
    <div class="picedit_message">
         <span class="picedit_control ico-picedit-close" data-action="hide_messagebox"></span>
        <div></div>
    </div>
    <!-- Picedit navigation -->
    <div class="picedit_nav_box picedit_gray_gradient">
        <div class="picedit_pos_elements"></div>
       <div class="picedit_nav_elements">
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-pencil" title="Pen Tool"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_3">
                    <label class="picedit_colors">
                      <span title="Black" class="picedit_control picedit_action picedit_black active" data-action="toggle_button" data-variable="pen_color" data-value="black"></span>
                      <span title="Red" class="picedit_control picedit_action picedit_red" data-action="toggle_button" data-variable="pen_color" data-value="red"></span>
                      <span title="Green" class="picedit_control picedit_action picedit_green" data-action="toggle_button" data-variable="pen_color" data-value="green"></span>
                    </label>
                    <label>
                        <span class="picedit_separator"></span>
                    </label>
                    <label class="picedit_sizes">
                      <span title="Large" class="picedit_control picedit_action picedit_large" data-action="toggle_button" data-variable="pen_size" data-value="16"></span>
                      <span title="Medium" class="picedit_control picedit_action picedit_medium" data-action="toggle_button" data-variable="pen_size" data-value="8"></span>
                      <span title="Small" class="picedit_control picedit_action picedit_small" data-action="toggle_button" data-variable="pen_size" data-value="3"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
                <span class="picedit_control picedit_action ico-picedit-insertpicture" title="Crop" data-action="crop_open"></span>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-redo" title="Rotate"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_1">
                    <label>
                      <span>90° CW</span>
                      <span class="picedit_control picedit_action ico-picedit-redo" data-action="rotate_cw"></span>
                    </label>
                    <label>
                      <span>90° CCW</span>
                      <span class="picedit_control picedit_action ico-picedit-undo" data-action="rotate_ccw"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
           <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-arrow-maximise" title="Resize"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_2">
                    <label>
                        <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="resize_image"></span>
                        <span class="picedit_control picedit_action ico-picedit-close" data-action=""></span>
                    </label>
                    <label>
                      <span>Width (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_width" value="0">
                    </label>
                    <label class="picedit_nomargin">
                        <span class="picedit_control ico-picedit-link" data-action="toggle_button" data-variable="resize_proportions"></span>
                    </label>
                    <label>
                      <span>Height (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_height" value="0">
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
       </div>
    </div>
    <!-- Picedit canvas element -->
    <div class="picedit_canvas_box">
        <div class="picedit_painter">
            <canvas></canvas>
        </div>
        <div class="picedit_canvas">
            <canvas></canvas>
        </div>
        <div class="picedit_action_btns active">
          <div class="picedit_control ico-picedit-picture" data-action="load_image"></div>
          <div class="picedit_control ico-picedit-camera" data-action="camera_open"></div>
          <div class="center">or copy/paste image here</div>
        </div>
    </div>
    <!-- Picedit Video Box -->
    <div class="picedit_video">
        <video autoplay=""></video>
        <div class="picedit_video_controls">
            <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="take_photo"></span>
            <span class="picedit_control picedit_action ico-picedit-close" data-action="camera_close"></span>
        </div>
    </div>
    <!-- Picedit draggable and resizeable div to outline cropping boundaries -->
    <div class="picedit_drag_resize">
        <div class="picedit_drag_resize_canvas"></div>
        <div class="picedit_drag_resize_box">
            <div class="picedit_drag_resize_box_corner_wrap">
            <div class="picedit_drag_resize_box_corner"></div>
            </div>
            <div class="picedit_drag_resize_box_elements">
                <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="crop_image"></span>
                <span class="picedit_control picedit_action ico-picedit-close" data-action="crop_close"></span>
            </div>
       </div>
    </div>
</div>
<input type="file" style="display:none" accept="image/*">
<!-- end_picedit_box -->
</div>
 <div class="modal-footer" style="margin-top:30px; text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="btnSubmit" class="btn btn-primary">Upload</button>
      </div>
</form>


      </div>
      
    </div>
  </div>
</div>


<script type="text/javascript">
    var img_id; //global
     var img_src; //global

        $( "#img1_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img2_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img3_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $("#img4_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         $( "#img5_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         //store

          $( "#store_img_edit_btn" ).click(function() {
          
          var $this = $(this);
         console.log($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });




      $('#imageUpload').on('show.bs.modal', function (event) {
       // alert('xx');
             var button = $(event.relatedTarget); // Button that triggered the modal
             window.img_id = button.data('imageid') ;// Extract info from data-* attributes
             window.img_src = document.getElementById(window.img_id).getAttribute('src').toString();
             console.log ("image id global set to ="+ window.img_id);
             console.log("hello docrore"+window.img_src);
             document.getElementById("img_src_id").value = window.img_id;        
             
     });

       $('#imageUpload').on('shown.bs.modal', function (event) {;
               
            // $('.picedit_box').picEdit("set_default_image",window.img_src);
       });

    
    $(function() {
        document.getElementById('myForms').reset();
        console.log("DOc"+'http://localhost/madebyus4u/uploads/no-photo.jpg');

       

        $('.picedit_box').picEdit({
            imageUpdated: function(img){
                //console.log('when image changed ');
              //  console.log (img);
            },
            formSubmitted: function(){
                //console.log('when image submitted ');
               // document.getElementById('img1').setAttribute( 'src', '' );
            },
            imageUpdated: function(img){
            //console.log('Image updated!'+img);
            },
            formSubmitted: function(response){
            document.getElementById("myForms").reset();
            console.log('Form submitted! with response'+response.responseText);
            document.getElementById(window.img_id).setAttribute('src',response.responseText );
            },
            maxWidth: 600, 
            maxHeight: 'auto',
            aspectRatio: true, 
            redirectUrl: false,
            defaultImage: false,
           
        });
                                
    

    });

    
</script>
<script type="text/javascript">
 $("#btn_save").click(function(){

  alert('xx');

 }) ;  
 

</script>
</body>
</html>
