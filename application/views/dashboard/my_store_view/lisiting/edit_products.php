<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to MadebyUs4u.com | Seller Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Le styles -->
        <link href=<?php echo base_url() . "assets/plugins/bootstrap/css/bootstrap.min.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/common.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/collective_common.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/css/store_tabs.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
        <link href=<?php echo base_url() . "assets/plugins/jquery/css/color-box.css"; ?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/plugins/picedit/css/font-css.css";?> rel="stylesheet">
        <link href=<?php echo base_url()."assets/plugins/picedit/css/picedit.min.css";?> rel="stylesheet">
         <!--start of row-->

        <style type="text/css">
                            
                             .tab_lables_sup {
                                font-weight:400;
                                top:-3.79px;
                                font-size:13px;'
                                }
        </style>

    </head>
                    <body>
                        
                        <?php $this->load->view($notification_bar); ?>

                        <header> 
                            <?php $this->load->view($header_black_menu); ?>
                            <?php $this->load->view($header_logo_white); ?>
                        </header>

                        <!-- Responsive design
                        ================================================== -->
                        <section id="responsive" style="background-color:#f5f5f5;">
                            <!--load menu here -->
                            <?php $this->load->view($main_menu); ?>
                            <div class="container container-listing">
                                <!--store setup-->
                                <div role="tabpanel" class="tab-pane active" id="store">
                                    <div class="row-storesetup row">
                                        <div id="divsetup-content" name="divsetup-content" class="divsetup-content col-md-12" style="display:block;">
                                            <h2 style="font-size: 26px;font-weight: 600;">Product  <span style='font-weight: 400;'>Managment</span>
                                            <small class='pull-right' style='color:rgb(216, 62, 62);margin-top: 8px;font-size: small;'>* ( Mandatory filed )</small>  </h3>
                                            <hr>
                                             <!-- my store setup begin-->
           
              <!--ERROR MESSAGE-->
              <style type="text/css">
              .error-message {
                margin-top: 1%;
         
              }
              </style>

              <div class="error-message col-md-12">  
                <?php $data['message'] = $this->session->flashdata('message');?>
                <?php $this->load->view($show_error_page,$data);?> 
              </div>
      <!--END OF ERROR MESSAGE-->

                                            <div role="tabpanel" class="tabpane-storesetup">
                                                <!-- Nav tabs -->

                                                <ul class="nav nav-pills" role="tablist" id='createNotTab'>                                       
                                                    <!--this is a form for all tab submission after verfiication-->
                                                    <li role="presentation" class="active"><a href="#edit-product-tab" aria-controls="edit-product-tab" role="tab" data-toggle="tab" ><!--store page--> Edit Product<i class="fa"></i></a></li>
                                                    <li role="presentation" ><a href="#add-product-tab" aria-controls="add-product-tab" role="tab" data-toggle="tab" ><!--store page--> Add Product<i class="fa"></i></a></li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade in tab-pane active " id="edit-product-tab"><?php $this->load->view($edit_product); ?></div>
                                                    <div role="tabpanel" class="tab-pane fade in tab-pane" id="add-product-tab"><?php $this->load->view($add_product); ?></div>
                                                </div>

                                        </div>

                                    </div>
                                </div>
                                </div><!--end of row-->
                                </div><!--end of store setup-->
                            </div>
                        </section>

                        <footer class="footer">
                            <?php  
                            $this->load->view($footer_subscribe); 
                            $this->load->view($footer_privacy); 
                            ?>
                        </footer>
                        <!-- Bootstrap core JavaScript
                        ================================================== -->
                        <!-- Placed at the end of the document so the pages load faster -->
                        <script src="<?php echo base_url() . "assets/plugins/jquery/jquery.min.js"; ?>"></script>
                         <script src="<?php echo base_url() . "assets/plugins/jquery/jquery.colorbox-min.js"; ?>"></script>
                        <!-- Latest compiled and minified JavaScript -->
                        <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/tooltip.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/plugins/bootstrap/js/bootstrap.min.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/subscribe_ajax.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/multiple_image_uploads.js"; ?>"></script>
                        <script src="<?php echo base_url() . "assets/js/bootstrapValidator.js"; ?>"></script>
                        <script src=<?php echo base_url()."assets/plugins/picedit/picedit.js";?>> </script>

                                                                    
                                             
<script type="text/javascript">
    
    $('a.colorbox').colorbox({rel:'colorbox'});
</script>
                       



                       
<!-- Modal -->
<div class="modal fade" id="imageUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog" role="document">
  
    <div class="modal-content">
         
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Image Upload</h4>
          </div>

      <div>
     
      <form></form>
     
      <div contenteditable="true" style="position: absolute; left: -999px; width: 0px; height: 0px; overflow: hidden; outline: 0px; opacity: 0;"></div>
     <?php 
                    
    $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'myForms', 'name' => 'myForms',);
           
     echo form_open_multipart('store/post_image/', $attributes);
    ?>

<input type="hidden" name="img_src_id" id="img_src_id">

<div style="margin:10% auto 0 auto; display: table;">

 <!-- begin_picedit_box -->
 <div class="picedit_box">
    <!-- Placeholder for messaging -->
    <div class="picedit_message">
         <span class="picedit_control ico-picedit-close" data-action="hide_messagebox"></span>
        <div></div>
    </div>
    <!-- Picedit navigation -->
    <div class="picedit_nav_box picedit_gray_gradient">
        <div class="picedit_pos_elements"></div>
       <div class="picedit_nav_elements">
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-pencil" title="Pen Tool"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_3">
                    <label class="picedit_colors">
                      <span title="Black" class="picedit_control picedit_action picedit_black active" data-action="toggle_button" data-variable="pen_color" data-value="black"></span>
                      <span title="Red" class="picedit_control picedit_action picedit_red" data-action="toggle_button" data-variable="pen_color" data-value="red"></span>
                      <span title="Green" class="picedit_control picedit_action picedit_green" data-action="toggle_button" data-variable="pen_color" data-value="green"></span>
                    </label>
                    <label>
                        <span class="picedit_separator"></span>
                    </label>
                    <label class="picedit_sizes">
                      <span title="Large" class="picedit_control picedit_action picedit_large" data-action="toggle_button" data-variable="pen_size" data-value="16"></span>
                      <span title="Medium" class="picedit_control picedit_action picedit_medium" data-action="toggle_button" data-variable="pen_size" data-value="8"></span>
                      <span title="Small" class="picedit_control picedit_action picedit_small" data-action="toggle_button" data-variable="pen_size" data-value="3"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
                <span class="picedit_control picedit_action ico-picedit-insertpicture" title="Crop" data-action="crop_open"></span>
           </div>
           <!-- Picedit button element end -->
            <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-redo" title="Rotate"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_1">
                    <label>
                      <span>90° CW</span>
                      <span class="picedit_control picedit_action ico-picedit-redo" data-action="rotate_cw"></span>
                    </label>
                    <label>
                      <span>90° CCW</span>
                      <span class="picedit_control picedit_action ico-picedit-undo" data-action="rotate_ccw"></span>
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
           <!-- Picedit button element begin -->
            <div class="picedit_element">
             <span class="picedit_control picedit_action ico-picedit-arrow-maximise" title="Resize"></span>
                 <div class="picedit_control_menu">
                <div class="picedit_control_menu_container picedit_tooltip picedit_elm_2">
                    <label>
                        <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="resize_image"></span>
                        <span class="picedit_control picedit_action ico-picedit-close" data-action=""></span>
                    </label>
                    <label>
                      <span>Width (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_width" value="0">
                    </label>
                    <label class="picedit_nomargin">
                        <span class="picedit_control ico-picedit-link" data-action="toggle_button" data-variable="resize_proportions"></span>
                    </label>
                    <label>
                      <span>Height (px)</span>
                      <input type="text" class="picedit_input" data-variable="resize_height" value="0">
                    </label>
                  </div>
               </div>
           </div>
           <!-- Picedit button element end -->
       </div>
    </div>
    <!-- Picedit canvas element -->
    <div class="picedit_canvas_box">
        <div class="picedit_painter">
            <canvas></canvas>
        </div>
        <div class="picedit_canvas">
            <canvas></canvas>
        </div>
        <div class="picedit_action_btns active">
          <div class="picedit_control ico-picedit-picture" data-action="load_image"></div>
          <div class="picedit_control ico-picedit-camera" data-action="camera_open"></div>
          <div class="center">or copy/paste image here</div>
        </div>
    </div>
    <!-- Picedit Video Box -->
    <div class="picedit_video">
        <video autoplay=""></video>
        <div class="picedit_video_controls">
            <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="take_photo"></span>
            <span class="picedit_control picedit_action ico-picedit-close" data-action="camera_close"></span>
        </div>
    </div>
    <!-- Picedit draggable and resizeable div to outline cropping boundaries -->
    <div class="picedit_drag_resize">
        <div class="picedit_drag_resize_canvas"></div>
        <div class="picedit_drag_resize_box">
            <div class="picedit_drag_resize_box_corner_wrap">
            <div class="picedit_drag_resize_box_corner"></div>
            </div>
            <div class="picedit_drag_resize_box_elements">
                <span class="picedit_control picedit_action ico-picedit-checkmark" data-action="crop_image"></span>
                <span class="picedit_control picedit_action ico-picedit-close" data-action="crop_close"></span>
            </div>
       </div>
    </div>
</div>
<input type="file" style="display:none" accept="image/*">
<!-- end_picedit_box -->
</div>
 <div class="modal-footer" style="margin-top:30px; text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="btnSubmit" class="btn btn-primary">Upload</button>
      </div>
</form>


      </div>
      
    </div>
  </div>
</div>


<script type="text/javascript">
    var img_id; //global
     var img_src; //global

        $( "#img1_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img2_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $( "#img3_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

        $("#img4_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         $( "#img5_btn_edit" ).click(function() {
          
          var $this = $(this);
          //alert($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });

         //store

          $( "#store_img_edit_btn" ).click(function() {
          
          var $this = $(this);
         console.log($this);
          
       
          console.log($(this).data('imageid'));
          window.img_id = $(this).data('imageid');
          $( "#"+window.img_id+"_btn").trigger( "click" );
          if(window.img_src!==null)
           $('.picedit_box').picEdit("set_default_image",window.img_src);
          
        });




      $('#imageUpload').on('show.bs.modal', function (event) {
       // alert('xx');
             var button = $(event.relatedTarget); // Button that triggered the modal
             window.img_id = button.data('imageid') ;// Extract info from data-* attributes
             window.img_src = document.getElementById(window.img_id).getAttribute('src').toString();
             console.log ("image id global set to ="+ window.img_id);
             console.log("hello docrore"+window.img_src);
             document.getElementById("img_src_id").value = window.img_id;        
             
     });

       $('#imageUpload').on('shown.bs.modal', function (event) {;
               
            // $('.picedit_box').picEdit("set_default_image",window.img_src);
       });

    
    $(function() {
        document.getElementById('myForms').reset();
        console.log("DOc"+'http://localhost/madebyus4u/uploads/no-photo.jpg');

       

        $('.picedit_box').picEdit({
            imageUpdated: function(img){
                //console.log('when image changed ');
              //  console.log (img);
            },
            formSubmitted: function(){
                //console.log('when image submitted ');
               // document.getElementById('img1').setAttribute( 'src', '' );
            },
            imageUpdated: function(img){
            //console.log('Image updated!'+img);
            },
            formSubmitted: function(response){
            document.getElementById("myForms").reset();
            console.log('Form submitted! with response'+response.responseText);
            document.getElementById(window.img_id).setAttribute('src',response.responseText );
            },
            maxWidth: 600, 
            maxHeight: 'auto',
            aspectRatio: true, 
            redirectUrl: false,
            defaultImage: false,
           
        });
                                
    

    });

    
</script>
<script type="text/javascript">
 $("#btn_save").click(function(){

  alert('xx');

 }) ;  
 

</script>