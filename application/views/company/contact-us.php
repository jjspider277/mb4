<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Bid</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bid.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">

</head>

<body>

  <?php $this->load->view($notification_bar); ?>

	<header>

	 <?php $this->load->view($header_black_menu); ?>
     <?php $this->load->view($header_logo_white); ?>

	</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">
    
    <!--load menu here -->
    <?php $this->load->view($main_menu); ?>

<div class="container">
   
            <div class="col-md-3 left-catag-twitt">
<br/>
                 <div class="row row-newline">
                     <div class="col-md-2">
                         <img src="<?php echo base_url()."assets/images/bid/delivery.png"?>" height="200"/>
                     </div> 
                 </div>
 
                 <div class="row twitter-loads" style="margin-left:4px;text-align:LEFT;">
                 <?php $this->load->view($tweets_page,$data); ?>
                </div>
                 
                 <div class="row video-display">
                <?php $this->load->view($video_page); ?>
                </div>
            
            </div>

               <div class="sfd">
            <div class="col-md-9 showing_text">
     
                    <div class="col-md-offset-1 ">
                     
                      <span class='showing'>Showing &nbsp;( <span class='element_count'><?=$total_rows;?>,<?=$per_page;?></span> )&nbsp;&nbsp;results </span>
                        

                      <span class='pull-right showing-item'>ITEM PER PAGE:&nbsp; (<span class='element_count'><?=$per_page;?>,<?=$total_rows;?>,</span>)&nbsp;&nbsp;<a href="#">12</a>&nbsp;<a href="#">15</a>&nbsp;<a href="#">18</a>&nbsp;<a href="#">21</a></span>
                    
                    </div>
                    
                   
            
            </div>
            </div>   

 
    <hr class="hr_border">

            <div class="company-content">                 
          
             <?php echo $html; ?>
           

        </div>

           

        </div>


    
    </div>


</div>





</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script  type="text/javascript" src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script type="text/javascript" src="<?php echo base_url()."assets/js/global_search.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="assets/plugins/rating/js/star-rating.min.js"></script>
<script type="text/javascript" src="assets/js/rating_ajax.js"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>

  <!--
  <script type="text/javascript" src="assets/plugins/bootstrap/js/tooltip.js"></script>
  <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-rating.min.js"></script>
  -->

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
        /***global base url path in javascript***/
  var base_url = window.location.origin;
  var pathArray = window.location.pathname.split('/');
  var base_url_complete = base_url+'/'+pathArray[1]+'/';
  var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ; 
  var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>";
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
      
         
    /*    function rate_item($rating_input_id,$rating_value) {
            alert('in');
            $("#"+$rating_input_id).rating('rate', $rating_value);
         }   

         function get_rating_value($rating_input_id,$to_rated_item_id) {          
       
            save_rating($rating_input_id,"products",$to_rated_item_id);
         }  

       
        $(function () {
       
       
        $('#programmatically-rating43').click(function () {
            
          $('#programmatically-rating43').rating('rate', $('#programmatically-value3').val());
        });
        $('#programmatically-rating43').change(function () {
          alert($('#programmatically-rating43').rating('rate'));
        });

        $('.rating-tooltip').rating({
          extendSymbol: function (rate) {
            $(this).tooltip({
              container: 'body',
              placement: 'bottom',
              title: 'Rate: ' + rate
            });
          }
        });
         
      
       
      });*/


</script>
<script>
jQuery(document).ready(function () {

       var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
       

        $(".rating").rating('refresh', 
            {showClear: false,diabled:true,hoverEnabled:false, showCaption: false,size: 'xs',starCaptions: {5.0:'5 Stars'},
          });


         $('.rating').on('rating.change', function() {
          
          if(!readOnly) {
           var value =  $(this).val();
           var static_id_text=("rating_element-").length;       
           var profile_id =  ((this).id).slice(static_id_text);
           var rated = $(this).val();             
           var server_url = "<?php echo base_url('rating/rate');?>";
           var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
           var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

           save_rating(profile_id,value,'profile',profile_id,server_url,csrf_token,csrf_hash);

          }
           else {
            window.location.assign("<?=site_url('users/login');?>");
           }


          

        });

 
 
       
    });
</script>

<script type="text/javascript">
  
$('#display-search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});
$('#search-header').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});

$('#home_page_search').keypress(function (e) {
  if (e.which == 13) {
    $('form#search_frm').submit();
    return false;    //<---- Add this line
  }
});



</script>


</body>
</html>
