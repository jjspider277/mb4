<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Checkout - Card and Billing Info</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/checkout_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">


</head>

<style type="text/css">
/*to be taken out */
.col-md-2 a, .col-md-2 a:hover
{

 text-decoration:none;

}

.tab-links.active{

 border-bottom: 2px solid #428bca;
 }

 .cc-billing-error {
	text-align:center;
	color:#a94442;
	width: 30em;
	margin: 0 auto;
 }
	.review-btn{
		font-size:18px;
	}
.navbar {
	margin-bottom: 0px;
}
.row5 {
	padding: 2px;
	border: 2px solid #F3F3F2;
	border-top: 7px solid grey;
	border-radius: 11px;
}
.menu-column_submenu  {
	margin: 0;
	padding: 0;
	font-family: 'Oswald',sans-serif;
	font-weight: 300;
	font-size:medium ;

}


#search{
	background: #d3d3d3;
	cursor: pointer;
	font-size: 24px;
	font-weight: bold;
	text-transform: lowercase;
	padding: 20px 2%;
	width: 96%;
}
#search-overlay{
	background: black;
	background: rgba(255, 255, 255, 255);
	color: black;
	display: none;
	font-size: 18px;
	height: 200px;
	padding: 0px;
	margin-top:28px;
	position: absolute;
	width: 436px;
	z-index: 100;
	opacity: 0.95;
	border-radius: 4%;
	border: 2px solid #efefef;
	overflow: auto;
}
#display-search{
	border: none;
	color: black;
	font-size: 14px;
	margin: 5px 0 0 0;
	width: 400px;
	height: 20px;
	padding: 0 0 0 10px;
	display: none;
}

#hidden-search{
	left: -10000px;
	position: absolute;

}

#results{
	display: none;
	width: 300px;
	list-style: none;
}
#results ul {
	list-style:none;
	padding-left:0;
}​
 #results ul li{
	 list-style: none;
	 padding-left:0;
 }

#results ul li a{
	color:#2676af;
	font-size: 12px;
	font-weight: bold;
}
</style>

<?php
	$expmonth = array('' => 'Select',
					'01' => '01', '02' => '02',
					'03' => '03', '04' => '04',
					'05' => '05', '06' => '06',
					'07' => '07', '08' => '08',
					'09' => '09', '10' => '10',
					'11' => '11', '12' => '12');

	$expyear = array('' => 'Select',
					'2015' => '2015', '2016' => '2016',
					'2017' => '2017', '2018' => '2018',
					'2019' => '2019', '2020' => '2020',
					'2021' => '2021', '2022' => '2022');

?>
<body>

    <?php $this->load->view($notification_bar); ?>

  <header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">


	<!--load menu here -->
	<div class="row white-bg">
		<hr class="" style="margin: 0px;">
		<div class="container white-bg " style="">

			<home class="navbar navbar-default" role="navigation" style="min-height:40px;">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">

						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<style type="text/css">
						.main-navigation {
							background-color: #FAFAFA;
						}
						.navbar-default {
							background-color: inherit;
							border: none;
						}
						.blue-font {
							color: #2f97cc;
						}
						.grey-background{
							background: #ebebeb;
						}
						.bottom-blue-border{
							border-bottom: 4px solid #216da1;
						}
						.wrapper {
							text-align: center;
						}

						.start-shopping-btn {
							position: absolute;
							top: 83%;
							left: 42%;
							font-size: 27px;
							background: #0b69a0;
						}

						.black-btn{
							background:#303030;
						}
						.box-height{
							height: 251px;
						}
						.pagination{

							float: right !important;
						}
						.pull-right{

						}
						.pmt_label{
							text-align: center;
							width: 100%;
							display: block;
							
						}

					</style>
					<?php $this->load->view($column_main_menu);?>

				</div><!-- /.container-fluid -->
		</div>
	</div>

	<div class="cc-billing">

		<?php
			// GET THE USER/SHIPPING ADDRESS DATA
			$user_first = $this->session->userdata('profile_fname');
			$user_last = $this->session->userdata('profile_lname');
			$user_zip = $this->session->userdata('profile_zip');

		?>
		<div class="cc-billing-error" style="font-size: 12px;" >
			<?php echo validation_errors(); ?>
		</div>

		<?php echo form_open('shopcart/validate_billing_info', 'class="ship-address-form col-md-6"'); ?>
		<div class="col-md-12">
			<h3 class="blue-font">2. Enter Your Payment Details</h3>
			<hr class="no-top-margin">
		</div> <div class="col-md-12 "><input id="hiddenpmt" type="hidden" name="payment" value="">
		<!--		<div class="col-md-12 padding-top-6 no-left-padding">
			<p class=" font-size-16 bottom-margin-10">Payment Methods</p>
				<div class="row bottom-margin-10">
				

						<div class="col-md-2 radio no-margin">
							<label>
								<input type="radio" id="payment_visa" value="1" name="payment"><span><img id="payment_visa" src="<?php echo base_url()."assets/images/cart_and_checkout/visa.png";?>"></span>
							</label>

						</div>
						<div class="col-md-2 radio">
							<label>
								<input type="radio" id="payment_mc" value="2" name="payment"><span><img   id="payment_mc" src="<?php echo base_url()."assets/images/cart_and_checkout/master.png";?>"></span>
							</label>
						</div>
						<div class="col-md-2 radio">
							<label>
								<input type="radio" id="payment_discover" value="3" name="payment"><span><img id="payment_amex" src="<?php echo base_url()."assets/images/cart_and_checkout/pay1.png";?>"></span>
							</label>
						</div>
						<div class="col-md-2 radio">
							<label>
								<input type="radio" id="payment" value="4" name="payment"><span><img  id="payment_jcb" src="<?php echo base_url()."assets/images/cart_and_checkout/pay2.png";?>"></span>
							</label>
						</div>
						<div class="col-md-2 radio">
							<label>
								<input type="radio" id="payment_discover" value="5" name="payment"><span><img id="payment_discover" src="<?php echo base_url()."assets/images/cart_and_checkout/discover.png";?>"></span>
							</label>
						</div>


				</div>

			</div>-->

		</div>
		
			<div class="form-group col-md-10">
				<label for="cardnum">Card Number</label>
				<input type="text" class="cardnum form-control" name="cardnum" id="cardnum"
					value="<?php
							if ( $this->session->userdata('cardinfo_set') === TRUE ){
								echo $this->session->userdata('cc_cardnum');
							} elseif ( isset($post['cardnum']) ) {
								echo $post['cardnum'];
							} ?>" onKeyUp="checkCC(this);" />
			</div>
			<div id="payment_visa" class="form-group col-md-2"  style="display:none; padding: 0px; margin-bottom:5px;">
				<span class="pmt_label">Visa</span>
				<img src="<?php echo base_url()."assets/images/cart_and_checkout/visa.png";?>"  style="width: 80%;display: block; margin: 5px auto 0px auto;">
			</div>
			<div id="payment_mc" class="form-group col-md-2"  style="display:none; padding: 0px; margin-bottom:5px;">
				<span class="pmt_label">Mastercard</span>
				<img src="<?php echo base_url()."assets/images/cart_and_checkout/master.png";?>"  style="width: 80%;display: block; margin: 5px auto 0px auto;" >
			</div>
			<div id="payment_amex" class="form-group col-md-2"  style="display:none; padding: 0px; margin-bottom:5px;">
				<span class="pmt_label">Amex</span>
				<img src="<?php echo base_url()."assets/images/cart_and_checkout/pay1.png";?>"   style="width: 80%;display: block; margin: 5px auto 0px auto;">
			</div>
			<div id="payment_jcb" class="form-group col-md-2"  style="display:none; padding: 0px; margin-bottom:5px;">
      			<span class="pmt_label">JCB</span>
				<img src="<?php echo base_url()."assets/images/cart_and_checkout/pay2.png";?>"  style="width: 80%;display: block; margin: 5px auto 0px auto;" >
		    </div>
			<div id="payment_discover" class="form-group col-md-2" style="display:none; padding: 0px; margin-bottom:5px;">
				<span class="pmt_label">Discover</span>
				<img src="<?php echo base_url()."assets/images/cart_and_checkout/discover.png";?>"  style="width: 80%;display: block; margin: 5px auto 0px auto;">
			</div>
			<!-- 4223567890123445 -->
			<script>
			var isInt = function(n) { return parseInt(n) === n };
			
			function reset_card_img(){
					document.getElementById('payment_visa').style.display = 'none';
					document.getElementById('payment_mc').style.display = 'none';
					document.getElementById('payment_jcb').style.display = 'none';
					document.getElementById('payment_amex').style.display = 'none';
					document.getElementById('payment_discover').style.display = 'none';
					 document.getElementById('hiddenpmt').value = '';				
			}
			function visa_math(single_num){
				var multiplied;
				multiplied = ((parseInt(single_num))*2);
				if( multiplied > 9){
					return  -9 ;
				}else{
					return multiplied;
				}				
			}
			
			function mod10(single_num){
				var multiplied;	
				multiplied = ((parseInt(single_num))*2);				
				if( parseInt(single_num) > 4){
					console.log(((parseInt(multiplied.substr(0,1))) + (parseInt(multiplied.substr(1,1)))));
					return  ((parseInt(multiplied.substr(0,1))) + (parseInt(multiplied.substr(1,1)))) ;
				}else{
					return multiplied;
				}				
			}
			
			function visa_checksum(num){
				if(num.length==16){
					var seta;
					var setb;
					seta = parseInt(num.substr(0,1)) + parseInt(num.substr(2,1)) + parseInt(num.substr(4,1)) + parseInt(num.substr(6,1)) + parseInt(num.substr(8,1)) + parseInt(num.substr(10,1)) + parseInt(num.substr(12,1)) + parseInt(num.substr(14,1));
					setb = visa_math(num.substr(1,1)) + visa_math(num.substr(3,1)) +  visa_math(num.substr(5,1)) + visa_math(num.substr(7,1)) + visa_math(num.substr(9,1)) + visa_math(num.substr(11,1)) + visa_math(num.substr(13,1)) + visa_math(num.substr(15,1));
					if(isInt((seta + setb)/10)){
						return 'valid';
					}else{
						return 'invalid';
					}					
				}else{
					return 'invalid';
				}				
			}
			function cc_checksum(num){
					var seta;
					var setb;
					seta = parseInt(num.substr(0,1)) + parseInt(num.substr(2,1)) + parseInt(num.substr(4,1)) + parseInt(num.substr(6,1)) + parseInt(num.substr(8,1)) + parseInt(num.substr(10,1)) + parseInt(num.substr(12,1)) + parseInt(num.substr(14,1));
					setb = mod10(num.substr(1,1)) + mod10(num.substr(3,1)) +  mod10(num.substr(5,1)) + mod10(num.substr(7,1)) + mod10(num.substr(9,1)) + mod10(num.substr(11,1)) + mod10(num.substr(13,1)) + mod10(num.substr(15,1));
					console.log(seta + '  ' +setb);
					if(isInt((seta + setb)/10)){
						return 'valid';
					}else{
						return 'invalid';
					}
			}
			 
		    function checkCC(elem){
				var input;
				var digits;
				var len;
				var card;
				var hiddenpmt;
				var img;
				var validate;
				validate = '';
				input = elem.value;
				len = input.length;
				digits  = input.substr(0,len);
				hiddenpmt = document.getElementById('hiddenpmt');
				if(hiddenpmt.value == '' ){
					card = '';					
					if(parseInt(len) == 1 && digits == '4'){card = 'visa';}
					if(parseInt(len) == 2 ){
						if (parseInt(digits) >= 51 && parseInt(digits) <= 55){card = 'mc';}
						if (digits == '34' || digits == '37'){card = 'amex';}
						if (digits == '36'||digits == '38'){card = 'diners';}
					}
					if(parseInt(len) == 4){
						if(digits == '3088'||digits == '3096'|| digits == '3112' ||digits == '3158'||digits == '3337'){
							card = 'jcb';	
						}else if (digits.substr(0,2) == '30'){
							card = 'diners';
						}
					}
					if(parseInt(len) == 8 && ((parseInt(digits)>=60110000 && parseInt(digits)<=60119999) || (parseInt(digits)>=65000000 && parseInt(digits)<=65999999) || (parseInt(digits)>=62212600 && parseInt(digits)<=62292599))){
						card = 'discover';
					}
					if(card != ''){
						img = document.getElementById('payment_' + card);
						img.style.display = "inline-block";
						if(card == 'visa'){hiddenpmt.value = '1';	}
						else if (card == 'mc'){ hiddenpmt.value = '2';}
						else if (card == 'amex'){ hiddenpmt.value = '3';}
						else if (card == 'jcb'){ hiddenpmt.value = '4';}
						else if (card == 'discover'){ hiddenpmt.value = '5';}
					}
				}
				if(input == ''){
					reset_card_img();
					hiddenpmt.value = '';
					card = '';
				}
				if(parseInt(len) == 15 && hiddenpmt.value == '3'){				
						validate = cc_checksum(input); console.log(validate);				
				}
				if(parseInt(len) == 16 && (hiddenpmt.value == '1'  || hiddenpmt.value == '2' || hiddenpmt.value == '4' || hiddenpmt.value == '5')){
					if(hiddenpmt.value == '1'){ 
						validate = visa_checksum(input);
					}else{
						validate = cc_checksum(input);
						
					}
				}
				console.log(validate);
				if(validate == 'invalid'){
					reset_card_img();
					alert('Invalid card number!');					
				}
			}
			</script>
			<div class="form-group col-md-6">
				<div>
					<label for="expmonth">Expiration Date (MM-YYYY)</label>
				</div>
				<?php
					$selected_expmonth = "";
					if ( $this->session->userdata('cardinfo_set') === TRUE ){
						$selected_expmonth = $this->session->userdata('cc_expmonth');
					} elseif (isset($_POST['expmonth'])){
						$selected_expmonth = $_POST['expmonth'];
					} else {
						$selected_expmonth = '';
					}
				?>
				<select name="expmonth" class="expdate form-control" id="expmonth">
					<?php
						foreach($expmonth as $id=>$name){
							if($selected_expmonth == $id){
								$sel = 'selected="selected"';
							}
							else{
								$sel = '';
							}
							echo "<option $sel value=\"$id\">$name</option>";
						}
					?>
				</select>
					<?php
						$selected_expyear = "";
						if ( $this->session->userdata('cardinfo_set') === TRUE ){
							$selected_expyear = $this->session->userdata('cc_expyear');
						} elseif (isset($_POST['expyear'])){
							$selected_expyear = $_POST['expyear'];
						} else {
							$selected_expyear = '';
						}
					?>
				<select name="expyear" class="expyr form-control" id="expyear" >
					<?php
						foreach($expyear as $id=>$name){
							if($selected_expyear == $id){
								$sel = 'selected="selected"';
							}
							else{
								$sel = '';
							}
							echo "<option $sel value=\"$id\">$name</option>";
						}
					?>
				</select>
				<div class="clearfix"></div>
			</div>
			<div class="form-group col-md-6">
				<div><label for="cvv">Security Code</label></div>
				<input type="text" name="cardcvv" class="scode form-control" id="scode" maxlength="4"
					value="<?php
							if ( $this->session->userdata('cardinfo_set') === TRUE ){
								echo $this->session->userdata('cc_cardcvv');
							} elseif ( isset($post['cardcvv']) ) {
								echo $post['cardcvv'];
							} ?>" />
				<img alt="" src="<?php echo $image_path.'que.png';?>" />
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>

			<div class="form-group col-md-6">
			<label for="cardname_first">First Name on Card</label>
				<input type="text" name="cardname_first" class="cardname form-control" id="cardname_first"
					value="<?php
							if ( $this->session->userdata('cardinfo_set') === TRUE ){
								echo $this->session->userdata('cc_cardname_first');
							} elseif ( isset($post['cardname_first']) ) {
								echo $post['cardname_first'];
							} ?>" />
			</div>
			<div class="form-group col-md-6">
				<label for="cardname_last">Last Name on Card</label>
				<input type="text" name="cardname_last" class="cardname form-control" id="cardname_first"
					value="<?php
							if ( $this->session->userdata('cardinfo_set') === TRUE ){
								echo $this->session->userdata('cc_cardname_last');
							} elseif ( isset($post['cardname_last']) ) {
								echo $post['cardname_last'];
							} ?>" />
			</div>


				<div class="form-group col-md-12">
					<label for="cardemail">Email</label>
					<input type="email" name="cardemail" class="cardname form-control" id="cardemail"
						value="<?php
								if ( $this->session->userdata('cardinfo_set') === TRUE ){
									echo $this->session->userdata('cc_cardemail');
								} elseif ( isset($post['cardemail']) ) {
									echo $post['cardemail'];
								} ?>" />
				</div>

			<div class="clearfix"></div>
			<div class="submit-btn col-md-12">
				<?php
					echo form_submit('', 'Review You Checkout Cart', 'class="review-btn submit-btn btn btn-primary"');
				?>
			</div>
			<div class="clearfix"></div>
		<?php echo form_close(); ?>

	<!-- END NEW SHIPPING ADDRESS FORM -->

	</div> <!-- close shopcart -->
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
	<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>

<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>
