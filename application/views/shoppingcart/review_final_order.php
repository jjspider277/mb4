<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Checkout - Review Final Order</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/checkout_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">


</head>

<style type="text/css">
/*to be taken out */  
.col-md-2 a, .col-md-2 a:hover
{

 text-decoration:none;

}
 
.tab-links.active{

 border-bottom: 2px solid #428bca;
 }
 
 .cc-billing-error {
	text-align:center;
	color:#a94442;
	width: 30em;
	margin: 0 auto;
 }
 
 .checkout-error
 {
	text-align:center;
	color:#a94442;
	width: 30em;
	margin: 0 auto;
 }
.navbar {
	margin-bottom: 0px;
}
.row5 {
	padding: 2px;
	border: 2px solid #F3F3F2;
	border-top: 7px solid grey;
	border-radius: 11px;
}
.menu-column_submenu  {
	margin: 0;
	padding: 0;
	font-family: 'Oswald',sans-serif;
	font-weight: 300;
	font-size:medium ;

}


#search{
	background: #d3d3d3;
	cursor: pointer;
	font-size: 24px;
	font-weight: bold;
	text-transform: lowercase;
	padding: 20px 2%;
	width: 96%;
}
#search-overlay{
	background: black;
	background: rgba(255, 255, 255, 255);
	color: black;
	display: none;
	font-size: 18px;
	height: 200px;
	padding: 0px;
	margin-top:28px;
	position: absolute;
	width: 436px;
	z-index: 100;
	opacity: 0.95;
	border-radius: 4%;
	border: 2px solid #efefef;
	overflow: auto;
}
#display-search{
	border: none;
	color: black;
	font-size: 14px;
	margin: 5px 0 0 0;
	width: 400px;
	height: 20px;
	padding: 0 0 0 10px;
	display: none;
}

#hidden-search{
	left: -10000px;
	position: absolute;

}

#results{
	display: none;
	width: 300px;
	list-style: none;
}
#results ul {
	list-style:none;
	padding-left:0;
}​
 #results ul li{
	 list-style: none;
	 padding-left:0;
 }

#results ul li a{
	color:#2676af;
	font-size: 12px;
	font-weight: bold;
}

#search-data{
	font-size: 14px;
	line-height: 20%;
	padding: 0 0 0 20px;

}

h2.search-data {
	margin: 10px 0 30px 0;
}
 
</style>

<body>
	
    <?php $this->load->view($notification_bar); ?>
     
  <header>

    <?php $this->load->view($header_black_menu); ?>

    <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	
	<!--load menu here -->
	<div class="row white-bg">
		<hr class="" style="margin: 0px;">
		<div class="container white-bg " style="">

			<home class="navbar navbar-default" role="navigation" style="min-height:40px;">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">

						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<style type="text/css">
						.main-navigation {
							background-color: #FAFAFA;
						}
						.navbar-default {
							background-color: inherit;
							border: none;
						}
						.blue-font {
							color: #2f97cc;
						}
						.grey-background{
							background: #ebebeb;
						}
						.bottom-blue-border{
							border-bottom: 4px solid #216da1;
						}
						.wrapper {
							text-align: center;
						}

						.start-shopping-btn {
							position: absolute;
							top: 83%;
							left: 42%;
							font-size: 27px;
							background: #0b69a0;
						}

						.black-btn{
							background:#303030;
						}
						.box-height{
							height: 251px;
						}
						.pagination{

							float: right !important;
						}
						.pull-right{

						}

					</style>
					<?php $this->load->view($column_main_menu);?>

				</div><!-- /.container-fluid -->
		</div>


	</div>
    
	<div class="final-review">
	
		<!-- Order Processing Messages -->
		<div class="checkout-error" style="font-size: 12px;" >
			<?php 
				if ( isset($messages) ){
					foreach ($messages as $msg){
						echo $msg.'<br />';
					}
				}
			
			?>
		</div>
		
		<?php
			// GET THE USER/SHIPPING ADDRESS DATA
			$user_first = $this->session->userdata('profile_fname');
			$user_last = $this->session->userdata('profile_lname');
			$user_zip = $this->session->userdata('profile_zip');
			
			$shipping_name = $this->session->userdata('shipping_firstname').' '.$this->session->userdata('shipping_lastname');
			$shipping_addr = $this->session->userdata('shipping_street').'<br />'.
							 $this->session->userdata('shipping_city').', '.
							 $this->session->userdata('shipping_state').' '.
							 $this->session->userdata('shipping_zip');
							 
			$cc_last4 = substr($this->session->userdata('cc_cardnum'), -4);
			
			$cc_info = $cc_last4.'<br />'.'Exp:  '.$this->session->userdata('cc_expmonth').'/'.$this->session->userdata('cc_expyear');
		?>

		<?php echo form_open('shopcart/process_order', 'class="ship-address-form col-md-10"'); ?>
		<h3 class="blue-font">3. Review and Submit Your Order  </h3>
		<hr>
		<div class="col-md-12">
		<div class="col-md-4 no-left-padding ">

			<h4>Shipping Adress <span class="blue-font font-size-11 bold"><?php echo anchor('shopcart/checkout_address', 'Change'); ?></span></h4>
			<div class="col-md-12 border border-radius">
				<p class="dark-grey-font no-margin">	<?php echo $shipping_name; ?> <br/></p>
				<p class="dark-grey-font">jona@email.com</p>

				<p class="dark-grey-font bold no-margin">	<?php echo $this->session->userdata('shipping_street') ;?></p>
				<p class="dark-grey-font"><?php echo $this->session->userdata('shipping_city');?>,<?php echo	$this->session->userdata('shipping_state');?> <?php echo $this->session->userdata('shipping_zip');?></p>
			</div>


		</div>
		<div class="col-md-4 ">

			<h4>Payment Method <span class="blue-font font-size-11 bold"><?php echo anchor('shopcart/address_verified', 'Change'); ?></span></h4>
			<div class="col-md-12 border border-radius padding-top-6 fixed-hegiht-94">
				<div class="col-md-1 no-left-padding">
					<img src="<?php echo base_url()."assets/images/cart_and_checkout/card-icon.png"; ?>">
				</div>
				<div class="col-md-4">
					<p class=""><span class="dark-grey-font">Visa...</span><span class="bold black-font"><?= $cc_last4 ?></span></p>
					<p class=""><span class="dark-grey-font">Exp:<?= $this->session->userdata('cc_expmonth').'/'.$this->session->userdata('cc_expyear') ?></span></p>

				</div>


			</div>


		</div>
			<div class="col-md-4">

				<h4>Choose a shipping method</h4>
				<div class="col-md-12 border border-radius padding-top-6 fixed-hegiht-94">
					<div class="radio col-md-12 no-bottom-margin">
						<label>
							<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							FedEx
						</label>
						<span class="pull-right">$26.45</span>
					</div>
					<div class="radio col-md-12">
						<label>
							<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							UPS
						</label>
						<span class="pull-right">$26.45</span>
					</div>
					<div class="radio col-md-12">
						<label>
							<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							DHL
						</label>
						<span class="pull-right">$26.45</span>
					</div>


				</div>


			</div>
		</div>
		<div class="col-md-12">
			<h3 class="bold"><?php echo count($this->cart->contents()) ;?> Items in Shopping Cart</h3>
		</div>
		<div class="col-md-8 padding-top border border-radius">
			<!-- start of loop -->

			<?php foreach($this->cart->contents() as $items): ?>
				<div class="col-md-12 no-left-right-padding">
					<div class="col-md-3 no-left-padding">
						<div class="thumbnail col-md-12">
							<img class="thumbnail-img-height" src="<?php echo $items['image']; ?>" style="height: 141px;width: 162px;">
						</div>


					</div>

					<div class="col-md-9 no-left-right-padding">
						<div class="col-md-8 no-left-padding">
							<p class="blue-font no-top-margin font-size-20"><?php echo $items['name']; ?></p>
						</div>
						<div class="col-md-4 no-left-right-padding">


							<button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

							<span class="red-font">Remove this Item</span>


						</div>


						<div class="col-md-12 padding-top no-left-padding">
							<div class="col-md-2 no-left-padding padding-top-6">
								<p class="font-size-16 dark-grey-font">Quantity</p>
							</div>

							<div class="col-md-2 no-left-padding">

								<select name="item-qty" class="form-control">
									<option value="<?php echo $items['qty']; ?>"><?php echo $items['qty']; ?></option>
								</select>
							</div>
							<div class="col-md-2 no-left-padding ">
								<p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24"><?php echo $this->cart->format_number($items['price']); ?></span><span class="blue-font">&nbspUSD</span></p>
							</div>

						</div>
						<div class="col-md-12 padding-top no-left-padding">
							<p class="padding-top-6">
							<div class="col-md-3"><span class="dark-grey-font ">Item Listed By:</span></div>
							<div class="col-md-5 no-left-padding">
								<span class="pull-left"><img src="<?php echo base_url()."assets/icons/user_sm.png"?>"></span>
								<span class="bold">&nbsp <?php echo $items['seller']; ?></span>
								</p>
							</div>




						</div>
					</div>

					<hr class="col-md-12 no-left-right-padding">

				</div>
			<?php endforeach; ?>
			<!--end of loop -->



		</div>
		<div class="col-md-4">

			<?php $sum=0;
			foreach ($this->cart->contents() as $items ) {
				$sum = $sum+floatval($items['shipping_price']);
			} ?>

			<?php

			$shipping = $sum;
			$order_total = $this->cart->total();
			$final_order_total = $order_total + $shipping;

			//Set order total in session var for cc processing
			$this->session->set_userdata('final_order_total', $final_order_total);
			?>
			<div class="col-md-12 waterish-blue-border border-radius no-left-right-padding padding-bottom-16">
				<div class="col-md-12 blue-bg">
					<h4 class="white-font">Shopping Cart Summary <span class="
glyphicon glyphicon-chevron-down font-size-11"></span></h4>

				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-5"><p class="font-size-16 pull-right padding-top-6">Item Total:</p></div>
						<div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$<?php echo $this->cart->format_number($order_total); ?> </span><span class="blue-font bold">USD</span></p></div>
					</div>
					<div class="row">
						<div class="col-md-5 no-left-padding"><p class="font-size-16 pull-right padding-top-6">Shipping Cost:</p></div>
						<div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$<?php echo $this->cart->format_number($shipping); ?> </span><span class="blue-font bold">USD</span></p></div>
					</div>
					<!--<div class="row">
                        <div class="col-md-5 no-left-padding"><p class="font-size-16 pull-right padding-top-6">Shipping Total:</p></div>
                        <div class="col-md-7"><p class="pull-right"><span class="font-size-20 ">$425.00 </span><span class="blue-font bold  ">USD</span></p></div>
                    </div>-->

					<div class="col-md-12 grey-bg padding-top border-radius">
						<div class="col-md-5 no-left-padding padding-top-6">
							<p><span class="font-size-16 bold">Order Total:</span></p>
						</div>
						<div class="col-md-6 col-md-offset-1">
							<p><span class="font-size-24 dark-grey-font">$</span><span class="font-size-24"><?php echo $final_order_total;?></span><span class="blue-font bold font-size-16">USD</span></p>
						</div>
					</div>

					<div class="col-md-12 no-left-right-padding padding-top-6">
						<hr  class="no-top-margin">
					</div>
					<div class="col-md-12 no-left-right-padding">
						<button type="submit" class="btn btn-primary full-width"><h4><span><img src="<?php echo base_url()."assets/icons/shopping-cart-big-white.png";?>"></span>&nbsp Confirm Order</h3></button>
					</div>


				</div>

			</div>
			<div class="col-md-12 padding-top padding-bottom-10">
				<div class="col-md-2 no-left-padding"><img src="<?php echo base_url()."assets/images/cart_and_checkout/user-circle.png"; ?>"> </div>
				<div class="col-md-10 no-left-padding">Add an optioal note to the seller</div>

			</div>
			<div class="col-md-12 border border-radius padding-top"><p class="dark-grey-font">Write your note to a seller here  by tagging there username.</p></div>
		</div>
		<div class="clearfix"></div>
		<?php echo form_close(); ?>




			
	<!-- END FINAL REVIEW FORM -->

	</div> <!-- close final-review -->
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
	<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>

	<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>
