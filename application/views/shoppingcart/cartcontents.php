<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Seller Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?>

		  </head>

<style type="text/css">
/*to be taken out */  
.col-md-2 a, .col-md-2 a:hover
{

 text-decoration:none;

}
 
.tab-links.active{

 border-bottom: 2px solid #428bca;
 }
 
</style>

<body>

     <?php $this->load->view($notification_bar); ?>
     
  <header>

      <?php $this->load->view($header_black_menu); ?>

      <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	
	<!--load menu here -->
	<?php $this->load->view($main_menu); ?>
    
	<div class="shopcart"> 
	<?php 
		if(!$this->cart->contents()):
			echo '<p align="center"><span class="cartheading">Your shopping cart is empty.</span></p>';
			echo '<p align="center"><a href="'.base_url('buy').'">Keep Shopping</a></p>';
		else:
		
	?>
	
	<?php echo form_open('shopcart/update_cart'); ?>
	<table width="80%" align="center" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td>Prod ID</td>
				<td>Image</td>
				<td>Item</td>
				<td>Seller</td>
				<td>Qty</td>
				<td>Item Price</td>
				
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; ?>
			<?php foreach($this->cart->contents() as $items): ?>
			 
			<?php echo form_hidden('rowid[]', $items['rowid']); ?>
			<tr <?php if($i&1){ echo 'class="alt"'; }?>>
				<td><?php echo $items['id']; ?></td> 
				
				<td><img src="<?php echo $items['image']; ?>" height="100px" width="100px" alt="Product Image" /></td> 
				<td><?php echo $items['name']; ?></td>
				<td><?php echo $items['seller']; ?></td> 
				<td>
					<?php echo form_input(array('name' => 'qty[]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?>
				</td>
				<td><?php echo $this->cart->format_number($items['price']); ?></td>	
			</tr>
			 
			<?php $i++; ?>
			<?php endforeach; ?>
			
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><strong>Total</strong></td>
				<td><?php echo $this->cart->format_number($this->cart->total()); ?></td>
			</tr>
		</tbody>
	</table>
	<br /><br /> 
	<p align="center">
		<?php 
			echo form_submit('', 'Update your Cart', 'class="btn btn-primary"');
			echo '&nbsp;&nbsp;&nbsp;';
			//echo anchor('shopcart/empty_cart', 'Empty Cart', 'class="empty"');
			echo anchor('shopcart/empty_cart', 'Empty Cart', 'class="btn btn-primary"');
		?>
		<br /><small>If the quantity is set to zero, the item will be removed from the cart.</small>
	</p>
	<?php 
	echo form_close(); 
	endif;
	?>
	</div> <!-- close shopcart -->
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
	 <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>
