<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Shopping Cart Contents</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">

	<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/checkout_new.css";?> rel="stylesheet">


</head>

<style type="text/css">
/*to be taken out */  
.col-md-2 a, .col-md-2 a:hover
{

 text-decoration:none;

}
 
.tab-links.active{

 border-bottom: 2px solid #428bca;
 }
.navbar {
	margin-bottom: 0px;
}
.row5 {
	padding: 2px;
	border: 2px solid #F3F3F2;
	border-top: 7px solid grey;
	border-radius: 11px;
}
.menu-column_submenu  {
	margin: 0;
	padding: 0;
	font-family: 'Oswald',sans-serif;
	font-weight: 300;
	font-size:medium ;

}


#search{
	background: #d3d3d3;
	cursor: pointer;
	font-size: 24px;
	font-weight: bold;
	text-transform: lowercase;
	padding: 20px 2%;
	width: 96%;
}
#search-overlay{
	background: black;
	background: rgba(255, 255, 255, 255);
	color: black;
	display: none;
	font-size: 18px;
	height: 200px;
	padding: 0px;
	margin-top:28px;
	position: absolute;
	width: 436px;
	z-index: 100;
	opacity: 0.95;
	border-radius: 4%;
	border: 2px solid #efefef;
	overflow: auto;
}
#display-search{
	border: none;
	color: black;
	font-size: 14px;
	margin: 5px 0 0 0;
	width: 400px;
	height: 20px;
	padding: 0 0 0 10px;
	display: none;
}

#hidden-search{
	left: -10000px;
	position: absolute;

}

#results{
	display: none;
	width: 300px;
	list-style: none;
}
#results ul {
	list-style:none;
	padding-left:0;
}​
 #results ul li{
	 list-style: none;
	 padding-left:0;
 }

#results ul li a{
	color:#2676af;
	font-size: 12px;
	font-weight: bold;
}
}
#search-data{
	font-size: 14px;
	line-height: 20%;
	padding: 0 0 0 20px;

}

h2.search-data{
	margin: 10px 0 30px 0;

</style>

<body>

     <?php $this->load->view($notification_bar); ?>
     
  <header>

      <?php $this->load->view($header_black_menu); ?>

      <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	<!--load menu here -->
	<div class="row white-bg">
		<hr class="" style="margin: 0px;">
		<div class="container white-bg " style="">

			<home class="navbar navbar-default" role="navigation" style="min-height:40px;">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">

						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<style type="text/css">
						.main-navigation {
							background-color: #FAFAFA;
						}
						.navbar-default {
							background-color: inherit;
							border: none;
						}
						.blue-font {
							color: #2f97cc;
						}
						.grey-background{
							background: #ebebeb;
						}
						.bottom-blue-border{
							border-bottom: 4px solid #216da1;
						}
						.wrapper {
							text-align: center;
						}

						.start-shopping-btn {
							position: absolute;
							top: 83%;
							left: 42%;
							font-size: 27px;
							background: #0b69a0;
						}

						.black-btn{
							background:#303030;
						}
						.box-height{
							height: 251px;
						}
						.pagination{

							float: right !important;
						}
						.pull-right{

						}

					</style>
					<?php $this->load->view($column_main_menu);?>

				</div><!-- /.container-fluid -->
		</div>

	</div>
	<?php

		$cart_item_count = $this->session->userdata('cart_item_count');

		if(!$this->cart->contents()):
			echo '<p align="center"><span class="cartheading">Your shopping cart is empty.</span></p>';
			echo '<p align="center"><a href="'.base_url('buy').'">Keep Shopping</a></p>';
		else:
	?>



	<!--new checkout interface -->
	<div class="tab-content">
		<div class="row tab-pane fade in active" role="tabpanel">
			<div class="container" style="margin-top: 32px;">
				<div class="col-md-12 blue-top-border white-bg no-left-right-padding padding-top  bottom-margin-28">
					<div class="col-md-1">
						<img src="<?php echo base_url();?>assets/images/cart_and_checkout/chk.png"/>

					</div>

					<div class="col-md-3 no-left-padding">
						<h4><span class="blue-font"><?php echo $cart_item_count;?> Item</span> in your MadeByUs4u</h4>

					</div>
					<div class="col-md-3  col-md-offset-3">
						<h4 class="pull-right italic"> Not finished Shopping?</h4>
					</div>
					<div class="col-md-2 no-right-padding">
						
						<?php echo anchor('buy', 'Continue Shopping', 'class="btn btn-primary black-bg no-border"');?>
					</div>

				</div>




				<div class="col-md-12 white-bg padding-top" style="margin-bottom: 25px;padding-bottom: 29px;">
					<?php echo form_open('shopcart/update_cart'); ?>
					<div class="col-md-8 padding-top border border-radius">
						<!-- Loop through cart contents -->
						<?php foreach($this->cart->contents() as $items): ?>
							<?php echo form_hidden('rowid[]', $items['rowid']); ?>
						<div class="col-md-12 no-left-right-padding">
							<div class="col-md-3 no-left-padding">
								<div class="thumbnail col-md-12">
									<img class="thumbnail-img-height" src="<?php echo $items['image']; ?>" height="100px" width="141px">
								</div>
								<div class="col-md-12 no-left-padding">


									<button class="btn btn-sm bg-transparent red-font border-red remove-btn"> <span class="glyphicon glyphicon-remove red-font remove-icon-btn"></span></button>

									<span class="red-font">Remove this Item</span>


								</div>

							</div>

							<div class="col-md-9">
								<div class="col-md-11 no-left-padding">
									<h3 class="blue-font no-top-margin"><?php echo $items['name']; ?></h3>
								</div>


								<div class="col-md-12 padding-top no-left-padding">
									<div class="col-md-2 no-left-padding padding-top-6">
										<p class="font-size-16 dark-grey-font">Quantity</p>
									</div>

									<div class="col-md-2 no-left-padding">
										<?php echo form_input(array('name' => 'qty[]','class'=>'form-control', 'id' =>'quantity', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?>
										<br />
										<!--<select class="form-control" id="quantity">
											<option>1</option>
										</select>-->
									</div>
									<div class="col-md-1 ">
										<p class="font-size-22 dark-grey-font">|</p>
									</div>
									<p class="padding-top-6">
										<span class="dark-grey-font">Item Listed By:</span>
										<span><img src="<?php echo"assets/icons/user_sm.png"?>"></span>
										<span class="bold"><?php echo $items['seller']; ?></span>
									</p>
								</div>
								<div class="col-md-12 padding-top no-left-padding">
									<div class="col-md-2 no-left-padding ">
										<p class="font-size-16"><span class="dark-grey-font font-size-24">$</span><span class="font-size-24"><?php echo $this->cart->format_number($items['price']); ?></span><span class="blue-font">&nbspUSD</span></p>
									</div>


									<div class="col-md-1 col-md-offset-2 ">
										<p class="font-size-22 dark-grey-font">|</p>
									</div>
									<p class="padding-top-6">
										<span class="dark-grey-font">Shipping Cost:</span>

										<span class="bold">$<?php echo $this->cart->format_number($items['shipping_price']); ?></span>
									</p>
								</div>
							</div>

							<hr class="col-md-12 no-left-right-padding">

						</div>
						<?php endforeach; ?>
						<!--end of loop -->
						<p align="center">
							<?php
							echo form_submit('', 'Update your Cart', 'class="btn btn-primary"');
							echo '&nbsp;&nbsp;&nbsp;';
							//echo anchor('shopcart/empty_cart', 'Empty Cart', 'class="empty"');
							echo anchor('shopcart/empty_cart', 'Empty Cart', 'class="btn btn-info"');
							//echo '<br /><br />'.anchor('shopcart/checkout_address', 'Checkout', 'class="btn btn-primary"');
							?>
							<br /><small>If the quantity is set to zero, the item will be removed from the cart.</small>
						</p>

					</div>
					<div class="col-md-4">
						<div class="col-md-12 waterish-blue-border border-radius no-left-right-padding padding-bottom-16">
							<div class="col-md-12 blue-bg">
								<h4 class="white-font">Shopping Cart Summary <span class="
glyphicon glyphicon-chevron-down font-size-11"></span></h4>

							</div>
							<div class="col-md-12">
								<div class="col-md-12 padding-top-6 no-left-padding">
									<p class="dark-grey-font font-size-16 no-bottom-margin">Payment Methods</p>
									<img src="<?php echo base_url()."assets/images/cart_and_checkout/payment.jpg";?>">

								</div>
								<div class="col-md-12 no-left-right-padding padding-top-6">
									<hr  class="no-top-margin">
								</div>
								<div class="col-md-12 grey-bg padding-top border-radius">
									<div class="col-md-4 no-left-padding padding-top-6">
										<p><span class="font-size-16 dark-grey-font">Item Total:</span></p>
									</div>
									<div class="col-md-7 col-md-offset-1">
										<p><span class="font-size-24 dark-grey-font">$</span><span class="font-size-24"><?php echo $this->cart->format_number($this->cart->total()); ?></span><span class="blue-font bold">USD</span></p>
									</div>
								</div>
								<div class="col-md-12 g padding-top border-radius">
									<div class="col-md-1 no-left-padding ">
										<img src="<?php echo base_url()."assets/icons/shipping-ico.png";?>">
									</div>
									<div class="col-md-10">
										<p><span class="blue-font bold">Get Shipping Cost</span></p>
										<?php /**$sum=0;
										foreach ($this->cart->contents() as $items ) {
											$sum = $sum+floatval($items['shipping_price']);
										} echo '$'.$sum;*/?>
									</div>
								</div>
								<div class="col-md-12 no-left-right-padding padding-top-6">
									<hr  class="no-top-margin">
								</div>
								<div class="col-md-12 no-left-right-padding">
									<a  href="<?php echo site_url('shopcart/checkout_address');?>" role="button" class="btn btn-primary full-width"><h4><span><img src="<?php echo base_url()."assets/icons/shopping-cart-big-white.png";?>"></span>&nbspProceed to Checkout</h3></a>
								</div>


							</div>

						</div>
					</div>


				</div>

			</div>


		</div>




	</div>
<?php endif; ?>
	<!--end of new checkout interface -->
	
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
	 <script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>
	 <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>


	 <script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>