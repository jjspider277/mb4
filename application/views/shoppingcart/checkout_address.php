<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Checkout - Shipping Address</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/collective_common_new.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/shopcart.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">

	<link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">
	<link href=<?php echo base_url()."assets/css/checkout_new.css";?> rel="stylesheet">


</head>

<style type="text/css">
/*to be taken out */  
.col-md-2 a, .col-md-2 a:hover
{

 text-decoration:none;

}
 
.tab-links.active{

 border-bottom: 2px solid #428bca;
 }
 
 .checkout-error {
	text-align:center;
	color:#a94442;
	width: 30em;
	margin: 0 auto;
 }
.navbar {
	margin-bottom: 0px;
}
.row5 {
	padding: 2px;
	border: 2px solid #F3F3F2;
	border-top: 7px solid grey;
	border-radius: 11px;
}
.menu-column_submenu  {
	margin: 0;
	padding: 0;
	font-family: 'Oswald',sans-serif;
	font-weight: 300;
	font-size:medium ;

}


#search{
	background: #d3d3d3;
	cursor: pointer;
	font-size: 24px;
	font-weight: bold;
	text-transform: lowercase;
	padding: 20px 2%;
	width: 96%;
}
#search-overlay{
	background: black;
	background: rgba(255, 255, 255, 255);
	color: black;
	display: none;
	font-size: 18px;
	height: 200px;
	padding: 0px;
	margin-top:28px;
	position: absolute;
	width: 436px;
	z-index: 100;
	opacity: 0.95;
	border-radius: 4%;
	border: 2px solid #efefef;
	overflow: auto;
}
#display-search{
	border: none;
	color: black;
	font-size: 14px;
	margin: 5px 0 0 0;
	width: 400px;
	height: 20px;
	padding: 0 0 0 10px;
	display: none;
}

#hidden-search{
	left: -10000px;
	position: absolute;

}

#results{
	display: none;
	width: 300px;
	list-style: none;
}
#results ul {
	list-style:none;
	padding-left:0;
}​
 #results ul li{
	 list-style: none;
	 padding-left:0;
 }

#results ul li a{
	color:#2676af;
	font-size: 12px;
	font-weight: bold;
}
}
#search-data{
	font-size: 14px;
	line-height: 20%;
	padding: 0 0 0 20px;

}

h2.search-data {
	margin: 10px 0 30px 0;
}
</style>

<body>
	<?php
		$states = array('' => 'Select',
							'Al' => 'Alabama', 'Ak' => 'Alaska', 
							'Az' => 'Arizona', 'Ar' => 'Arkansas', 
							'Ca' => 'California', 'Co' => 'Colorado', 
							'Ct' => 'Connecticut', 'De' => 'Delaware', 
							'Dc' => 'District Of Columbia', 'Fl' => 'Florida', 
							'Ga' => 'Georgia', 'Hi' => 'Hawaii', 
							'Id' => 'Idaho', 'Il' => 'Illinois', 
							'In' => 'Indiana', 'Ia' => 'Iowa', 
							'Ks' => 'Kansas', 'Ky' => 'Kentucky', 
							'La' => 'Louisiana', 'Me' => 'Maine', 
							'Md' => 'Maryland', 'Ma' => 'Massachusetts', 
							'Mi' => 'Michigan', 'Mn' => 'Minnesota', 
							'Ms' => 'Mississippi', 'Mo' => 'Missouri', 
							'Mt' => 'Montana', 'Ne' => 'Nebraska', 
							'Nv' => 'Nevada', 'Nh' => 'New Hampshire', 
							'Nj' => 'New Jersey', 'Nm' => 'New Mexico', 
							'Ny' => 'New York', 'Nc' => 'North Carolina', 
							'Nd' => 'North Dakota', 'Oh' => 'Ohio', 
							'Ok' => 'Oklahoma', 'Or' => 'Oregon', 
							'Pa' => 'Pennsylvania', 'Ri' => 'Rhode Island', 
							'Sc' => 'South Carolina', 'Sd' => 'South Dakota', 
							'Tn' => 'Tennessee', 'Tx' => 'Texas', 
							'Ut' => 'Utah', 'Vt' => 'Vermont', 
							'Va' => 'Virginia', 'Wa' => 'Washington', 
							'Wv' => 'West Virginia', 'Wi' => 'Wisconsin', 
							'Wy' => 'Wyoming');
	
	?>
     <?php $this->load->view($notification_bar); ?>
     
  <header>

      <?php $this->load->view($header_black_menu); ?>

      <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

	
	<!--load menu here -->
	<div class="row white-bg">
		<hr class="" style="margin: 0px;">
		<div class="container white-bg " style="">

			<home class="navbar navbar-default" role="navigation" style="min-height:40px;">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">

						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<style type="text/css">
						.main-navigation {
							background-color: #FAFAFA;
						}
						.navbar-default {
							background-color: inherit;
							border: none;
						}
						.blue-font {
							color: #2f97cc;
						}
						.grey-background{
							background: #ebebeb;
						}
						.bottom-blue-border{
							border-bottom: 4px solid #216da1;
						}
						.wrapper {
							text-align: center;
						}

						.start-shopping-btn {
							position: absolute;
							top: 83%;
							left: 42%;
							font-size: 27px;
							background: #0b69a0;
						}

						.black-btn{
							background:#303030;
						}
						.box-height{
							height: 251px;
						}
						.pagination{

							float: right !important;
						}
						.pull-right{

						}

					</style>
					<?php $this->load->view($column_main_menu);?>

				</div><!-- /.container-fluid -->
		</div>


	</div>

    
	<div class="shopcart">
	<?php 
		if(!$this->cart->contents()):
			echo '<p align="center"><span class="cartheading">Your shopping cart is empty.</span></p>';
			echo '<p align="center"><a href="'.base_url('buy').'">Keep Shopping</a></p>';
		else:
		
	?>
	<?php
		// GET THE USER/SHIPPING ADDRESS DATA
		$user_first = $this->session->userdata('profile_fname');
		$user_last = $this->session->userdata('profile_lname');
		$user_zip = $this->session->userdata('profile_zip');
		$user_street = $user_street2 = $user_city = $user_state = $user_zip = $buyers_email = '';
		
		$buyers_email = $buyer_email;
		
		if ( isset($shipping_address) ){
			foreach ($shipping_address as $sa){
				$user_street = $sa['shipping_address_line_1'];
				$user_street2 = $sa['shipping_address_line_2'];
				$user_city = $sa['shipto_city'];
				$user_state = $sa['shipto_state'];
				$user_zip = $sa['shipto_zip'];
				// Selected state value
				$selected = ($this->input->post('ship_state')) ? $this->input->post('ship_state') : $user_state;
			}
		}
		
	?>
	<div class="checkout-error" style="font-size: 12px;" >
		<?php echo validation_errors(); ?>
	</div>
	
	
	<!-- NEW CHECKOUT SHIPPING-ADDRESS FORM -->
	<?php echo form_open('shopcart/validate_checkout_address', 'class="ship-address-form col-md-6"'); ?>
		<div class="col-md-12">
			<h3 class="blue-font">1. Enter Your Shipping Adress</h3>
			<hr class="no-top-margin">
		</div>
		<div class="col-md-12">
			<div class="row bottom-margin-10 no-margin">
				<?php echo form_open('shopcart/validate_checkout_address', 'class="ship-address-form "'); ?>
				<div class="col-md-6">
					<label for="firstName">First Name</label>
					<input type="text" class="form-control" name="ship_first" id="ship_first"
						   value="<?php
						   if ( $this->session->userdata('shippinginfo_set') === TRUE ){
							   echo $this->session->userdata('shipping_firstname');
						   } elseif (isset($post['ship_first'])){
							   echo $post['ship_first'];
						   } else {
							   echo $user_first;
						   }?>" />
				</div>

				<div class="col-md-6">
					<label for="lastName">Last Name</label>
					<input class="form-control" name="ship_last"  id="ship_last" value="<?php
					if ( $this->session->userdata('shippinginfo_set') === TRUE ){
						echo $this->session->userdata('shipping_lastname');
					} elseif ( isset($post['ship_last']) ){
						echo $post['ship_last'];
					} else {
						echo $user_last;
					}?>">
				</div>
			</div>
			<div class="row bottom-margin-10 no-margin">

				<div class="col-md-12">
					<label for="emailAdress">Email Adress</label>
					<input class="form-control" name="ship_email"  id="ship_email" value="<?php
					if ( $this->session->userdata('shippinginfo_set') === TRUE ){
						echo $this->session->userdata('shipping_email');
					} elseif ( isset($post['ship_email']) ){
						echo $post['ship_email'];
					} else {
						echo $buyers_email;
					}?>">
				</div>
			</div>
			<div class="row bottom-margin-10 no-margin">
				<div class="col-md-7">
					<label for="street">Street</label>
					<input class="form-control" name="ship_street"  id="ship_street"   value="<?php
					if ( $this->session->userdata('shippinginfo_set') === TRUE ){
						echo $this->session->userdata('shipping_street');
					} elseif ( isset($post['ship_street']) ){
						echo $post['ship_street'];
					} else {
						echo $user_street;
					}?>">
				</div>
				<div class="col-md-5">
					<label class="col-md-12 no-left-right-padding" for="street">Apt/Suit/Other<span class="dark-grey-font pull-right font-size-11 padding-top-6">optional</span></label>
					<input class="form-control" name="ship_first" id="ship_first"
						   value="<?php
						   if ( $this->session->userdata('shippinginfo_set') === TRUE ){
							   echo $this->session->userdata('shipping_firstname');
						   } elseif (isset($post['ship_first'])){
							   echo $post['ship_first'];
						   } else {
							   echo $user_first;
						   }?>" />
				</div>
			</div>
			<div class="row bottom-margin-10 no-margin">
				<div class="col-md-4">
					<label for="city">City</label>
					<input class="form-control"name="ship_city"  id="ship_city"
						   value="<?php
						   if ( $this->session->userdata('shippinginfo_set') === TRUE ){
							   echo $this->session->userdata('shipping_city');
						   } elseif ( isset($post['ship_city']) ) {
							   echo $post['ship_city'];
						   } else {
							   echo $user_city;
						   }?>"/>
				</div>
				<div class="col-md-4">
					<label for="state">State</label>
					<?php
					$selected_state = "";
					if ( $this->session->userdata('shippinginfo_set') === TRUE ){
						$selected_state = $this->session->userdata('shipping_state');
					} elseif (isset($_POST['ship_state'])){
						$selected_state = $_POST['ship_state'];
					} else {
						$selected_state = $user_state;
					}
					?>
					<select id="ship_state" name="ship_state" class="form-control" >
						<?php
						foreach($states as $id=>$name){
							if($selected_state == $id){
								$sel = 'selected="selected"';
							}
							else{
								$sel = '';
							}
							echo "<option $sel value=\"$id\">$name</option>";
						}
						?>
					</select>
				</div>
				<div class="col-md-4">
					<label for="postalCode">Postal Code</label>
					<input class="form-control" name="ship_zip"  id="ship_zip"
						   value="<?php
						   if ( $this->session->userdata('shippinginfo_set') === TRUE ){
							   echo $this->session->userdata('shipping_zip');
						   } elseif ( isset($post['ship_zip']) ) {
							   echo $post['ship_zip'];
						   } else {
							   echo $user_zip;
						   }?>">
				</div>
			</div>
			</div>
		<div class="clearfix"></div>
		<div class="submit-btn col-md-12">
		  <?php echo form_submit('', 'Continue ', 'class="submit-btn btn btn-primary fill-width"');?>
		</div>
		<div class="clearfix"></div>
	<?php 
	echo form_close(); 
	endif;
	?>
	<!-- END NEW SHIPPING ADDRESS FORM -->
	
	</div> <!-- close shopcart -->

	
</section>

<footer class="footer">

    <?php
      $this->load->view($footer_subscribe);
      $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
	<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>


	<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
  
</script>
</body>
</html>

