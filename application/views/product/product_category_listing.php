<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to MadebyUs4u.com | Buy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/buy.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">

    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">


</head>


<body>
<style>
    .navbar {
        margin-bottom: 0px;
    }
    .row5 {
        padding: 2px;
        border: 2px solid #F3F3F2;
        border-top: 7px solid grey;
        border-radius: 11px;
    }
    .menu-column_submenu  {
        margin: 0;
        padding: 0;
        font-family: 'Oswald',sans-serif;
        font-weight: 300;
        font-size:medium ;

    }


    #search{
        background: #d3d3d3;
        cursor: pointer;
        font-size: 24px;
        font-weight: bold;
        text-transform: lowercase;
        padding: 20px 2%;
        width: 96%;
    }
    #search-overlay{
        background: black;
        background: rgba(255, 255, 255, 255);
        color: black;
        display: none;
        font-size: 18px;
        height: 200px;
        padding: 0px;
        margin-top:28px;
        position: absolute;
        width: 436px;
        z-index: 100;
        opacity: 0.95;
        border-radius: 4%;
        border: 2px solid #efefef;
        overflow: auto;
    }
    #display-search{
        border: none;
        color: black;
        font-size: 14px;
        margin: 5px 0 0 0;
        width: 400px;
        height: 20px;
        padding: 0 0 0 10px;
        display: none;
    }

    #hidden-search{
        left: -10000px;
        position: absolute;

    }

    #results{
        display: none;
        width: 300px;
        list-style: none;
    }
    #results ul {
        list-style:none;
        padding-left:0;
    }​
     #results ul li{
         list-style: none;
         padding-left:0;
     }

    #results ul li a{
        color:#2676af;
        font-size: 12px;
        font-weight: bold;
    }
    }
    #search-data{
        font-size: 14px;
        line-height: 20%;
        padding: 0 0 0 20px;

    }

    h2.search-data{
        margin: 10px 0 30px 0;

    }
</style>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>
    <?php $this->load->view($header_logo_white); ?>

</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">

    <!--load menu here -->
    <div class="row white-bg">
        <hr class="no-margin">
        <div class="container white-bg " style="">

            <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">

                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <style type="text/css">
                        .main-navigation {
                            background-color: #FAFAFA;
                        }
                        .navbar-default {
                            background-color: inherit;
                            border: none;
                        }
                        .blue-font {
                            color: #2f97cc;
                        }
                        .grey-background{
                            background: #ebebeb;
                        }
                        .bottom-blue-border{
                            border-bottom: 4px solid #216da1;
                        }
                        .wrapper {
                            text-align: center;
                        }

                        .start-shopping-btn {
                            position: absolute;
                            top: 83%;
                            left: 42%;
                            font-size: 27px;
                            background: #0b69a0;
                        }

                        .black-btn{
                            background:#303030;
                        }
                        .box-height{
                            height: 251px;
                        }
                        .pagination{

                            float: right !important;
                        }
                        .pull-right{

                        }

                    </style>
                    <?php $this->load->view($column_main_menu);?>

                </div><!-- /.container-fluid -->
        </div>

    </div>
    <!-- -->

    <div class="container">
        <h4 class="container">Browse Product Per Category</h4>
        <hr>
        <div class="products "  >
            <div class="col-md-12">
                <!--                        <hr class="hr_border_bottom" style="margin-top:0px;">-->
                <div class="pull-right">
                    <?php $this->load->view($paginate_page,$links); ?>
                </div>
            </div>
            <div class="row row-no-margin">
<?php
                 $this->load->view($product_listing,$products); ?>




            </div><!--nio margin row end-->
            <!--            <div class="col-md-12" style="padding-bottom:6%;">-->
            <!--                     <hr class="hr_border_bottom" style="margin-top:0px;">-->
            <!--                        <div class="pull-right">-->
            <!--                                --><?php //$this->load->view($paginate_page,$links); ?>
            <!--                        </div>    -->
            <!--                </div>-->

        </div>



    </div>


    </div>





</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
<script>
    jQuery(document).ready(function () {

        var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $(".rating").rating('refresh',
            {showClear: false, showCaption: false,size: 'xs',starCaptions: {5.0:'5 Stars'},
            });


        $('.rating').on('rating.change', function() {


            if(!readOnly) {
                //var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
                var value =  $(this).val();
                var static_id_text=("rating_element-").length;
                var profile_id =  ((this).id).slice(static_id_text);
                var rated = $(this).val();
                var server_url = "<?php echo base_url('rating/rate');?>";
                var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

                save_rating(profile_id,value,"products",profile_id,server_url,csrf_token,csrf_hash);

            }
            else {
                window.location.assign("<?=site_url('users/login');?>");
            }



        });




    });
</script>
<script type="text/javascript">
    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Recive response
     */
    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);

</script>

<script type="text/javascript">

    $('#display-search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });

    $('#search-header').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });

    $('#home_page_search').keypress(function (e) {
        if (e.which == 13) {
            $('form#search_frm').submit();
            return false;    //<---- Add this line
        }
    });



</script>

</body>
</html>
