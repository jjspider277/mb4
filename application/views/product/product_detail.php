<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Welcome to MadebyUs4u.com | Product Details </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/product.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">    
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">

</head>
<style type="text/css">
  div.sub-comment-popup{
  
  position: absolute;
  width: 550px;
  background-color: #ffffff;
  border: 9px solid #ffffff;
  border-radius: 5px;
  color: #3c3c3c;
  padding: 10px 0;
  margin-top: 1000px;
  margin-left: 400px;
 }
 
 div.overlay {
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.7);
  width: 100%;
  height: 100%;
  min-width: 1160px;
}
.close-buttons{
position:absolute;
right:-30px;
top:-25px;
cursor:pointer;
}

#my_image { position: relative; }
.classic_button_next{ position: absolute; right: 5px; top: 5px; }
.classic_button_prev { position: absolute; right: 88px; top: 5px; }
</style>

<body>

  <?php $this->load->view($notification_bar); ?>

  <header>

     <?php $this->load->view($header_black_menu); ?>
     <?php $this->load->view($header_logo_white); ?>

  </header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">
<?php $this->load->view($main_menu); ?>
  
<div class="container">


    <div class="col-md-7" style="">
    
      <div class="row col-md-9 col-md-large-image" style="">
        <div class="thumbnail rec thmbnail-large">

            <?php 


            $image_found = isset($product->image)?true:false ;
            $image_many = is_array($product->image);

            ?>



             <img class="img-thumbnail img-thumbnail-large-0 img-responsive" src="<?php echo $product->image[0];?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height:auto;">
             <input type ="button" class="classic_button_next btn btn-primary btn-large" id="next_button" value="Next >>"/>
             <input type ="button" class="classic_button_prev btn btn-primary btn-large" id="prev_button" value="Previous <<"/>            

        </div>
      </div>

     <div class="col-md-3 col-md-small-images" style="margin-left:12px;">
       <img class="img-thumbnail-small-1 img-thumbnail img-responsive small-images" src="<?php echo !empty($product->image[1])?$product->image[1]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:122px;width:100%;"> 
       <img class="img-thumbnail-small-2 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[2])? $product->image[2]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:122px;width:100%;">       
       <img class="img-thumbnail-small-3 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[3])? $product->image[3]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:122px;width:100%;">   
       <img class="img-thumbnail-small-4 img-thumbnail img-responsive small-images" src="<?php echo  !empty($product->image[4])? $product->image[4]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:122px;width:100%;">   
      <img class="img-thumbnail-small-0 img-thumbnail img-responsive small-images" src="<?php echo $product->image[0];?>" data-holder-rendered="true"  width="100%" style="height:113px;width:100%;display: none;">  
           
    </div>

    </div>
  
    <div class="row col-md-5">
          
         
          <h3><?php echo $product->product_name;?></h3>
            <p style="margin-top:-2px;margin-bottom:4px;">
            <span class='seller_name'> 
            <i class='glyphicon glyphicon-user'>
            <b style='padding-left:4px;color: #3C4144;;'>By <?php echo $product->profile->seller_name;?></b>
            </i>
            </span>
           
            <input class="rating" data-stars="5" data-step="1" data-size="sm" id="rating_element-<?=$product->profile->id;?>" value="<?= $product->profile->profile_rating ;?>" data-type="profile" />
                         
                                        
        </p>
        <hr class="hr_border">

        <p class="text-justify" style="font-family:"> <?php echo $product->pdescription;?>  </p>
        <hr class="hr_line">
      
      <?php if($is_bid_page):?>
          <strong style="color:#515151;">
          <strong>Latest Bid:</strong> 
          <span class="">$<?php echo $product->sprice;?>         
          <span class="timeleft"><strong style="color: #a8a8a8;">Time Left:</strong>
          <span class="left_text">00:04:14</span> </span> 
          </strong>
       <?php else:?>
          <strong style="color:#515151;">
          <strong>Price:</strong> 
          <span class="">$ <?php echo $product->sprice;?>
          <small style="text-decoration: line-through;"><?php echo $product->price;?></small>
          <small style="text-left">You save $<?= floatval($product->price) - floatval($product->sprice) ."!" ;?></small></span>
         
          </strong>
      <?php endif;?>
        

      <?php if($is_buy_page):?>                 
         <hr class="hr_line">  
         <strong style='color:#515151;'>Quantity:</strong> 
               
            <div class="center" >
          
                   <div class="input-group col-md-4">
                  
                       <span class="input-group-btn">
                          <button type="button" id="decrement_button" class="btn btn-default btn-number btn-sm" data-type="minus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-minus"></span>
                          </button>
                       </span>

            <input type="text" id="p_quantity" name="p_quantity" class="form-control input-number input-sm" value="1" min="1" max="10">
                    
              <span class="input-group-btn">
              <button type="button" id="increment_button" class="btn btn-default btn-number btn-sm" data-type="plus" data-field="quant[1]">
                <span class="glyphicon glyphicon-plus"></span>
              </button>
              </span>
              
                  </div>
              </div>       

            <?php endif;?>     
     
          <hr class="hr_line">
          <div class="buttons" style="padding:10px;">
      
      <!-- dkf BUY NOW BUTTON - Put form to add item to cart here -->
           <?php if($is_buy_page):?>
       <!-- dkf 06/10/2015 form here with hiddens to add to cart -->
      <?php echo form_open('shopcart/add_cart_item'); ?>
        <input type="hidden" name="product_quantity" id="product_quantity" value="1" />
        <?php echo form_hidden('product_id', $product->product_id); ?>
        <?php echo form_hidden('product_image', $product->image[0]); ?>
        <?php echo form_hidden('product_name', $product->product_name); ?>
        <?php echo form_hidden('product_price', $product->sprice); ?>
        <?php echo form_hidden('seller_profile_id', $product->profile->id); ?>
        <?php echo form_hidden('seller_name', $product->profile->seller_name); ?>
        <?php echo form_hidden('shipping_price', $product->shipping_price); ?>
        <input type="submit" class="btn btn-primary pull-left btn-lg btn-bid" style="width: 45%;" name="Add" value="Buy Now" />   
      <?php echo form_close(); ?>
          <!--<button class="btn btn-primary pull-left btn-lg btn-bid" style="width: 45%;">
          <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
          Buy Now
          </button>-->
          <?php endif;?>
          <?php if($is_bid_page):?>
          <button class="btn btn-primary pull-left btn-lg btn-bid" data-bidurl="<?php echo site_url('bid/product/'.$product->product_id) ?>" style="width: 45%;">
          <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
          Bid Now!
          </button>
           <?php endif;?>
          
        <?php if($is_logged_in===TRUE && $is_liked==false && $is_the_product_current_user==false) :?>
          <a href="#" class="btn btn-primary btn_black btn-lg" id="like_link_btn" data-product-id="<?php echo $product->product_id;?>" style="float:left;margin-left:20px;">
            <span class="glyphicon glyphicon glyphicon-heart" aria-hidden="true"></span>
          Like</a>
        <?php endif;?>
        <?php if($is_logged_in===TRUE && $is_liked==true && $is_the_product_current_user==false) :?>
          <a href="#" disabled='true' class="btn btn-primary btn_black btn-lg" id="like_link_btn" data-product-id="<?php echo $product->product_id;?>" style="margin-left:20px;">
            <span class="glyphicon glyphicon glyphicon-heart" aria-hidden="true"></span>
          Liked</a>
        <?php endif;?>
  

         <button type="button"class="btn btn-primary btn_black btn-lg" style="float:left;margin-left:18px;" 
                 data-html="true" title="Product Rating"  data-container="body" data-toggle="popover" data-placement="top">
          <span class="glyphicon glyphicon glyphicon-star" aria-hidden="true"></span>Rate
        </button>

          <div class="content" style="display:none;"> 
             <input class="rating_product" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product->product_id;?>" value="<?= $product->p_avg_rating;?>" data-type="products" />                
          </div>
               




      </div>
      </div>



    <?php

    $this->db->select('quantity');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("order_detail");
    $product_order = $query->result_array();

    $quantity = 0;

    if(count($product_order) != 0){
       foreach($product_order as $order){
           $quantity+=$order['quantity'];

        }
     }

    $this->db->select('*');
    $this->db->where("like_product_id", $product->product_id);
    $query = $this->db->get("likes");
    $product_order = $query->result_array();
    
    $no_of_recommendation = count($product_order);

    $this->db->select('*');
    $query = $this->db->get("users");
    $total_users = $query->result_array();

    $recommendation_in_percent = ($no_of_recommendation/count($total_users))*100;

    $this->db->select('rating');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("ratings");
    $product_ratings = $query->result_array();

    $no_of_ratings = count( $product_ratings);
    $total_ratings = 0;
    $average_rating = 0;

    if(count($product_ratings) != 0){
       foreach($product_ratings as $ratings){
           $total_ratings+=$ratings['rating'];

        }
        $average_rating = ($total_ratings/$no_of_ratings);
     }



     ?>
  
  <div class="col-md-12 col-md-12-2" style="margin-top:5%;border:1px solid #eaeaea;background-color:white;padding:8px;">
  <div class="col-md-4">
       <div class="col-sm-6">
        <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon01.png";?>">&nbsp;<?php echo $quantity; ?>+</p>
        </div>
        <div class="col-sm-6"><span class="short_text">Customers bought
        this product</span>
      </div>
  </div>
  <div class="col-md-4">

    <div class="col-sm-6">
          <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon02.png";?>">&nbsp;<?php echo round($recommendation_in_percent); ?>%</p>
          </div>
          <div class="col-sm-6"><span class="short_text">people Recommend
          this product</span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="col-sm-6">
          <p class="Numbers"> <img src="<?php echo base_url()."assets/images/products/icon03.png";?>">&nbsp;<?php print round($average_rating, 1);?>/5</p>
        </div>
          <div class="col-sm-6"><span class="short_text">Is average rating
        for this product</span>
        </div>

    </div>
</div>

<!--main containet-->
    <div class="col-md-9">
    
    <h3 style="color:#595959;font-weight:600;">Additional Details</h3>
    <hr class="hr_border" >
        <div class="main_content_text">
        <p>
        <?php echo isset($product->product_details)?$product->product_details :"no product detail info";?>
        </p>
        
        </div>

     <!--view-comment-=-->
   

     <?php $this->load->view($comment_view); ?>
         


     </div><!--main-end=-->
        
     <!--left container--listedby-->
      <div class="col-md-3" style="padding:10px;border:1px solid #ececec;background-color:white;">
           
          <?php 
       
          $this->load->view($product_user_details); ?>

          <?php $this->load->view($product_other_details,$products); ?>

    </div><!--left-end-->

</div>






</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script> 
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/likes.js";?>"></script>
  <script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>

<script type="text/javascript">
//globals
var server_url = "<?php echo base_url('rating/rate');?>";
var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;  
var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
</script>

<script>

jQuery(document).ready(function () {
  
var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

$('[data-toggle=popover]').popover({
    delay: 200, // this is definitely needed !
    html:true,
    placement:'top',   
     content: function() {
          return $('.content').html();
        },
});
  // Listen for inserted template to DOM
$('[data-toggle=popover]').on('inserted.bs.popover', function () {
   console.log('popover insetred'); 
   
})

$('[data-toggle="popover"]').on('shown.bs.popover', function () {

  $(".rating_product").rating('refresh', 
      {showClear: false,hoverEnabled:true, hoverChangeStars:true,showCaption: true,starCaptions: {5.0:'5 Stars'}
    });
 
 $('.rating_product').on('rating.change', function() {
    
     if(!readOnly) {
     var value =  $(this).val();

      var value =  $(this).val();
           
     var result = confirm("Are you sure you want to rate this product"+value+" ?");
     
     if(result==true){

     var static_id_text=("rating_element-").length;       
     var profile_id =  ((this).id).slice(static_id_text);
     var rated = $(this).val();             
     var server_url = "<?php echo base_url('rating/rate');?>";
     var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
     var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

        save_rating(profile_id,value,'product',profile_id,server_url,csrf_token,csrf_hash);
     }
    }
     else {
      window.location.assign("<?=site_url('users/login');?>");
     }    

  });
    
    
})

              
$(".btn-bid").click(function(e){
  window.location.href = $(this).data('bidurl');
});

$(".sub-comment-popup").hide();

// $(".comment-reply a").click(function(){
  $(document).on('click',".comment-reply a",function(event){
      //console.log("are we here");
      event.preventDefault();
      var comment_id = $(this).data("comment-id");
      //console.log("parent");
      //console.log(comment_id);
      $(".sub-comment-popup").each(function(){
          var c_id = $(this).data("comment-id");
          //console.log("child");
          console.log("parent"+comment_id);
          console.log("child"+c_id);

          if(c_id === comment_id){
            
            console.log("this is the one");
            $("body").append("<div class='overlay'></div>");
            $(".overlay").height($('body').height());
            $(".overlay").css({
            'z-index': '3'
            })
            $(".overlay").append($(this));

          }else{
            console.log("this is not the one");
          }

      });
      $("#more-comment-"+ comment_id).show();

});

$(".close-buttons").click(function(){
      var comment_id = $(this).data("comment-id");

      $(".sub-comment-popup").each(function(){
          var c_id = $(this).data("comment-id");
          console.log(c_id);

          if(c_id === comment_id){
            $(this).css('display','none');          
            $(".comment_body").append($(this));
             $(".overlay").detach();

          }else{
            console.log("this is not the one");
          }

      });

       
});

$("#like-thumb a").click(function(){
    var comment_id = $(this).data("comment-id");
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split( '/' );
    var base_url_complete =base_url+'/'+pathArray[1]+'/';
    $url = base_url_complete+'product/comment_like';
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

    console.log(comment_id);
    console.log("i am here");


    $.ajax({
    url: $url,
    data: ({'madebyus4u_csrf_test_name':csrf_hash,'comment_id': comment_id}),
    dataType:'json', 
    type: "post",
    success: function(data) {
    if(data.success) {
    if(data.past_like){
    var new_like = data.like;
    var the_link = "#like-link-"+comment_id;
    var the_link_text = "Liked";

    var the_span = "#count-of-like-"+comment_id;
    var the_like = "("+ new_like +")";

    console.log(the_link_text);
    $(the_link).text(the_link_text);
    $(the_span).html(the_like);

    }else{

    console.log("the like is now");
    var new_like = data.like;
    var the_link = "#like-link-"+comment_id;
    var the_link_text = "Liked";

    var the_span = "#count-of-like-"+comment_id;
    var the_like = "("+ new_like +")";

    $(the_link).text(the_link_text);
    $(the_span).html(the_like);
    }

    }

    }             

});


});
       
$(".rating").rating('refresh', 
    {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
  });


$('.rating').on('rating.change', function() {

if(!readOnly) {

 var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");      
 var value =  $(this).val();
 var result = confirm("Are you sure you want to rate this product"+value+" ?");
     

if(result==true){
 var static_id_text=("rating_element-").length;       
 var profile_id =  ((this).id).slice(static_id_text);
 var rated = $(this).val();             
 save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

}
}
 else {
  window.location.assign("<?=site_url('users/login');?>");
}
  });
});



</script>

<script type="text/javascript">

    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Receive response
     */
       var url =  "<?php echo site_url('welcome/subscribe');?>";
       subscribe_using_ajax(url);
</script>

<script type="text/javascript">

$(document).ready(function() {

  var current_position = 0;

   var  start_image_path = "<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>";

      //alert(current_position);
  var csrf_hash = "<?= $this->security->get_csrf_hash();?>"
    $('.img-thumbnail-large-0').show();


$(document).on('click','#prev_button',function(){
   if(current_position>0) {
    current_position = current_position -1;
    $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
    console.log('.img-thumbnail-small-'+current_position);
   }
    //alert(current_position);

  });

$(document).on('click','#next_button',function(){
  
   if(current_position<=4){
    current_position = current_position+1;
    $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
    console.log('.img-thumbnail-small-'+current_position);
  }

  });


 
  $(document).on('click','.img-thumbnail-small-1',function(){
    $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-1').attr('src'));
     
     //replace image one on this pic place
  
    
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-2',function(){
   $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-2').attr('src'));
     //replace image one on this pic place
  
     
      return false;          
  });
  
  $(document).on('click','.img-thumbnail-small-3',function(){
   
   $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
     //replace image one on this pic place

        
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-4',function(){
   $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-4').attr('src'));
     //replace image one on this pic place
     return false;          
  });

  $(document).on('click','.img-thumbnail-small-0',function(){
   $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
     //replace image one on this pic place
    $('.img-thumbnail-small-3').attr('src',start_image_path);
     
     
     return false;          
  });

  // Quantity buttons add and decerase
    $("#increment_button").click(function(){
      //alert("Increase quantity.");
      var qty = parseInt($("#p_quantity").val(),10);
      //alert(qty);
      if (qty < 10){
        newqty = qty + 1;
        $("#p_quantity").val(newqty.toString());
        $("#product_quantity").val(newqty.toString());
        //alert($("#product_quantity").val());
      }
    });
    
    $("#decrement_button").click(function(){
      //alert("Increase quantity.");
      var qty = parseInt($("#p_quantity").val(),10);
      //alert(qty);
      if (qty > 1){
        newqty = qty - 1;
        $("#p_quantity").val(newqty.toString());
        $("#product_quantity").val(newqty.toString());
        //alert($("#product_quantity").val());
      }
    });
   
});//end of document ready

</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>
