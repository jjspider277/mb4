
<style type="text/css">
  
.xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
.thumbnail .caption h4 {
  font-size: larger;
  overflow: hidden;
  height: 35px;
  margin-top: 7px;
  margin-bottom:5px;
}
.delete_btn {
   background-color: #3E3F3F;
  color: white;
  float:right;
  border-radius: 2px;
}
.thumbnail .caption {
  padding: 2px 7px !important;
}
 .rating_class {
  float: right;
}
.rating-xs {
  font-size: 1.6em;
}

.rating-gly-star {
  font-family: 'Glyphicons Halflings';
  padding-left: 2px;
  font-size: smaller;
  }

.star-rating .caption, .star-rating-rtl .caption {
  color: #999;
  display: inline-block;
  vertical-align: middle;
  float: right;
  font-size: 80%;
}

.edit-delete-toggle {
 display: none;
}
.seller-name{
	padding-left:4px;color: #1f72ad;
}
</style>

<div class='row product-lisiting-pages'>

<div class='col-md-12 col-sm-6'>

<?php if(count($products)>0): ?>

    <?php foreach($products as $product): ?>

      <div class="col-sm-3 col-md-3">

            <div class="thumbnail item-lisiting-inner">
            <a href="<?php echo $product['image'];?>" data-lightbox="image-1" data-title="My caption">
                

                        <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $product['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: 230px;'>
               </a>
                   
                 <div class="caption">
                       <h4 class='product_name_heading'><?php echo $product['name'];?></h4>
                       <!--<p class="product_description"><?php //echo $product['desc'];?></p> -->				  
					<div>
					  <div class="pull-left">
						 <span class='price'>$<?php echo $product['price'];?></span> <br/>
						  <small class="buy-text">Buy It Now</small>
					  </div>
					  <div class="pull-right" >
						  <span class="buy_btn viewDetails"> 
							<a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>" class="btn btn-primary" role="button">BUY NOW</a>
						  </span>
					  </div>
					  <div class="clearfix"></div>
					</div>					  
                      <hr>
				   <span class="seller_name">
				   <i class="glyphicon glyphicon-user"></i>
					<a href="<?php echo base_url('sell/seller').'/'.$product['profile_id'];?>"> 
					<b class="selle-name"><?php  echo $product['seller_name'] ;?></b></a>
				   </span>
                  <p>
                   <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['product_id'];?>" value="<?= $product['product_rating'] ;?>" data-type="product" />                   </p>
											   
                   <p class='edit-delete-toggle'>
                   <a href="#" class="btn btn-default btn-default btn-sm " role="button">Edit</a>
                   <a href="<?php echo base_url().'dashboard/set_auction/'.$product['product_id'] ?>" class="btn  btn-primary btn-sm pull-right" role="button">Set as Auction</a> 
                  </p>
                  
              </div>          
            </div>

      </div>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12 col-md-offset-4" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>

</div>
</div>