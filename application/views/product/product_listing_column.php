
<style type="text/css">

.xpens {

    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }

.thumbnail .caption h4 {
  margin-top: 0px;
  font-size: larger;
  overflow: hidden;
}

.thumbnail .caption {
  padding: 2px;
}

</style>
<!--bid related to be taken out seperate css -->
<style type="text/css">
    .price {
     padding-left: 6px;
    }

  .bid_count_text {
      display: block;
      color: #428bca;
      padding-left: 6px;
    }
    .rating_class {
      float: right;

    }
.rating-xs {
font-size: 1.4em;
}

.rating-gly-star {
  font-family: 'Glyphicons Halflings';
  padding-left: 2px;
  font-size: smaller;
  }

.star-rating .caption, .star-rating-rtl .caption {
  color: #999;
    float: right;
  display: inline-block;
  vertical-align: middle;
  font-size: 80%;
}

.glyphicon-user {
margin-top: 7px;
}

                          </style>

<?php $loop_ctr = 0; ?>
<?php if(count($products)>0): ?>

    <?php foreach($products as $product): ?>

      <div class="col-sm-3 col-md-4 col-md-images">

            <div class="thumbnail">
                <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url(array('product', 'detail', $product['product_id'],'bid')) ;?>">

                     <?php if(!empty($button_text)):?>
                        <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens"
                              src="<?php echo $product['image'] ;?>"
                              style=' text-align: center; width: 100%;height:250px;'>
                        <?php else:?>
                           <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens"
                              src="<?php echo $product['image'] ;?>"
                              style=' text-align: center; width: 100%;height:200px;'>

                     <?php endif;?>

               </a>

            <div class="caption">
                       <h4 class='product_name_heading'><?php echo $product['name'];?></h4>
                       <p class="product_description"><?php echo $product['desc'];?></p>

                        <p>

                          <span class='row buy_btn btn-sm' style="padding-right:15px;float: right;">
                            <a href="<?php echo empty($button_text) ? site_url(array('product', 'detail', $product['product_id'],'buy')): site_url('bid/product/'.$product['auction_id']) ;?>"
                            class="btn btn-primary" role="button"><?php echo !empty($button_text)?"Bid Now!":"Buy Now!";?></a>
                          </span>
						  <span class='price'>$<?php echo $product['price'];?></span>

                           <?php if(!empty($button_text)):?>
                                <small class='bid_count_text text-left'><strong><?php echo $product['bids']?> Bids</strong></small>
                            <?php endif;?>

                      </p>
            </div>
             <hr style="clear: both;">
             <?php if(isset($product['auctions_end'])):?>
                      <span>End Time: <strong><?php

                        $now = new DateTime();
              $diff = $now->diff(new DateTime($product['auctions_end']));
                       echo ($diff->d > 1)? $diff->d.' Days,' : $diff->d . ' Day,' ?>
              <?php echo ($diff->h > 1)? $diff->h.' Hours' : $diff->h . ' Hour,' ?>
              <?php if($diff->h === 0): ?>
                  <?php echo  ($diff->i > 1)? $diff->i . ' Minutes' : $diff->i .' Minute'; ?>
              <?php endif; endif;?></strong></span>

                  <p>
                      <span class='seller_name'><i class='glyphicon glyphicon-user'></i>
                        <a href="<?php echo base_url('sell/seller').'/'.$product['profile_id'];?>">
                        <b style='padding-left:4px;color: #1f72ad;'><?php echo $product['seller_name'];?></b></a>
                        <?php if(isset($my_auction)): ?>
                          <a href="<?php echo base_url().'dashboard/view_auction/'.$product['auction_id'] ?>"
                   class="btn  btn-primary btn-sm pull-right" role="button">Edit Auction</a>
                        <?php endif;?>
                      </span>


                      <span class='rating_class'>
                        <input class="rating" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product['product_id'];?>" value="<?= $product['product_rating'] ;?>"/>
                      </span>
                      <!--<a href="#" data-toggle="tooltip" data-placement="top" id="rating-tooltip<?=$product['product_id'];?>">
                       <span class='rating_class' >
                          rating class
                          <input type="hidden"  class="rating-tooltip" data-filled="fa fa-star fa-3x" data-empty="fa fa-star-o fa-3x" data-fractions="3" id="programmatically-rating<?=$product['product_id'];?>" value="<?= '2.5' ;?>" onchange="get_rating_value('programmatically-rating<?=$product['product_id'];?>','<?=$product['product_id'];?>');">

                        <input id="input-22"  value="0" type="number" class="rating" min=0 max=5 step=0.5 data-rtl=1 data-container-class='text-right' data-glyphicon=0>
                        <div class="clearfix"></div>
                        </a>-->



                   </p>
            </div>
      </div>
	  <?php
	  $loop_ctr++;
	  if($loop_ctr % 3 == 0) {echo '<div class="clearfix" style="clear:both;"></div>'; }
	  ?>
<?php endforeach ;?>
<?php else: ?>

  <div class="col-md-12" ><h4>We are sorry , no products added yet!</h4></div>
<?php endif ;?>

