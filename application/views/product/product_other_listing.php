<div class="col-sm-12 col-give-margin-top">
          <p class='listedby'>
              <strong>OtherListing By<i class='caret'></i>
              </strong>
              <b class='username'><?php echo $profile_username_data ;?></b>   
          
          </p>
</div>
<style type="text/css">

.thumbnail {
padding: 0px;
margin-bottom: 15px;
border: none;
border-radius: 0px;
background-color: #f5f5f5;
}
@media (min-width: 992px){
.col-other-listing .col-md-6 {width: 48%;}
}
.description {
  padding-top: 4px;
font-size: 15px;
font-weight: 500;
color: #226da0;
text-align: left;
}
.price {
  margin-top: 0px;
font-size: 19px;
font-weight: 500;
color: #111111;
text-align: left;
}

</style>
<row>
    <div class="col-md-12 col-other-listing" style="background-color:#f5f5f5;padding:10px;text-align: center;">
        <row>

    <?php if(count($products)>0): ?>
    <?php $count = 1; ?>

    <?php foreach($products as $product): ?>

        <div class="col-md-6 thumbnail">
            <img  alt="<?php echo $product['name'];?>"  class="img-thumbnail img-responsive xpens" 
                              src="<?php echo $product['image'] ;?>" 
                              style=' text-align: center; width: 100%;height: auto;'>
           <p class='description'><?php echo $product['desc'];?>
           <!--<?php echo "$".$product['price'];?></p>-->
        </div>  
      
        <?php $count++; ?>
        <?php if($count==4) {
          break;
        };?>
<?php endforeach ;?>

<?php endif ;?>  
        
        <div class="col-md-6 thumbnail" style="background-color:white;">
           <span class="badge" style='font-size: 17px;padding: 6px 11px;margin-top:15%;background-color:#226da0;color:white;'><?php echo ($count==4) ? count($product)-$count : "has no";?></span>
           <p class='description' style="padding-top:10px;text-align:center;color:#595959;">More Listings</p>
          
        </div>  

        <a href="<?php echo base_url('buy');?>" class='btn btn-primary' style="width: 48%;font-size: 11px;float: right;font-weight: bold;">View Other Listings</a>
        </row>
    </div>  

</row> 
