<div class="col-sm-12 no-margin blue-bg" style="padding: 6px 16px 0px;">
    <p class='listedby white-font'>
        <strong>Item Listed By:<i class='caret'></i>
        </strong>
        <b><?php
            echo ucfirst($product->profile->seller_name);?></b>
    </p>
</div>
<row>
    <div class="col-md-12" style="background-color:#f5f5f5;padding:10px;text-align: center;">
        <div class="media col-md-12 no-left-right-padding"  style="padding-bottom: 13px;">
            <div class="col-md-4 no-left-right-padding">
                <a class="media-left" href="<?php echo site_url('sell/seller/'.$product->profile->id) ?>">
                    <img class="img-circle" src="<?php echo $product->profile->profile_image;?>" height='90' alt="..." style='width:100%;'>
                </a>
            </div>
            <div class="media-body col-md-8 no-left-right-padding">
                <div class="col-md-1 no-left-padding"> <span class="user-sm"></span></div>
                <div class="col-md-11">
                    <h4 class="media-heading pull-left blue-font">
                        <?php  echo ucfirst($product->profile->seller_name);?>
                    </h4>

                </div>
                <div class="col-md-11 col-md-offset-1">
                    <p class="pull-left"><small><?=$product->profile->state.', United States';?></small></p>
                </div>


                <p></p>
                <p>


                    <!-- <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==false):  ?>

              <?php if($this_user_has_sent_request==true && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
               <a href="#" id='friend_request_btn' data-status="<?= ($this_user_has_sent_request==true)? 'SENT' : 'REQUEST'; ?>" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i><?= ($this_user_has_sent_request==true)? 'CANCEL FRIEND REQUEST' : 'SEND FRIEND REQUEST'; ?></a>
              <?php endif;?>

              <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==true && $this_user_is_friend==false) :?>
               <a href="#" id='friend_accept_btn' data-status="Accept" data-sender-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i>ACCEPT FRIEND REQUEST</a>
              <?php endif;?>

              <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==false) :?>
               <a href="#" id='friend_request_btn' data-status="REQUEST" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i>SEND FRIEND REQUEST</a>
              <?php endif;?>

                            <?php if($this_user_has_sent_request==false && $is_pending_friend_requets==false && $this_user_is_friend==true): ?>




                <a href="#" id='' data-status="" data-reciever-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm btn-gray margin-btm"><i class="block-icon"></i>BLOCK FRIEND</a>

                            <?php endif;?>
                          <?php else:?>
                              <a href="#" id='friend_accept_btn' data-status="Accept" data-sender-id="<?php echo $profile->id;?>" class="btn btn-default btn-sm" style="padding-top: 0px;"><i class='glyphicon glyphicon-user'></i>User is Owner!</a>
       <?php endif; ?>
``-->

                </p>
                <div class="col-md-11 no-left-padding">
                     <input class="rating pull-left" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product->profile->id;?>" value="<?= $product->profile->profile_rating ;?>" data-type="profile"/>
                </div>
            </div>

            <hr style="padding:0px;border-top:2px dotted #b3b3b3;margin-top: 5px;margin-bottom: 2px;">
        </div>





    </div>

</row>
