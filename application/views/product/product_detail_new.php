<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Welcome to MadebyUs4u.com | Product Details </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href=<?php echo base_url()."assets/plugins/bootstrap/css/bootstrap.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/common_new.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/product.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/main_menu_css_logged.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/font-awesome/css/font-awesome.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/plugins/rating/css/star-rating.min.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/notification_window.css";?> rel="stylesheet">

    <link href=<?php echo base_url()."assets/css/main_menu_css.css";?> rel="stylesheet">
    <link href=<?php echo base_url()."assets/css/bootstrap_submenu.css";?> rel="stylesheet">


</head>
<style type="text/css">
    div.sub-comment-popup{

        position: absolute;
        width: 550px;
        background-color: #ffffff;
        border: 9px solid #ffffff;
        border-radius: 5px;
        color: #3c3c3c;
        padding: 10px 0;
        margin-top: 1000px;
        margin-left: 400px;
    }

    div.overlay {
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        min-width: 1160px;
    }
    .close-buttons{
        position:absolute;
        right:-30px;
        top:-25px;
        cursor:pointer;
    }
    .col-md-large-image{
        height:auto;
    }
    .rating-xs {
        font-size: 20px;
    }
    div.caption >span.label{
        font-size:80%;
    }

    #my_image { position: relative; }
    .classic_button_next{ position: absolute; right: 5px; top: 5px; }
    .classic_button_prev { position: absolute; right: 88px; top: 5px; }
    .navbar {
        margin-bottom: 0px;
    }
    .row5 {
        padding: 2px;
        border: 2px solid #F3F3F2;
        border-top: 7px solid grey;
        border-radius: 11px;
    }
    .menu-column_submenu  {
        margin: 0;
        padding: 0;
        font-family: 'Oswald',sans-serif;
        font-weight: 300;
        font-size:medium ;

    }


    #search{
        background: #d3d3d3;
        cursor: pointer;
        font-size: 24px;
        font-weight: bold;
        text-transform: lowercase;
        padding: 20px 2%;
        width: 96%;
    }
    #search-overlay{
        background: black;
        background: rgba(255, 255, 255, 255);
        color: black;
        display: none;
        font-size: 18px;
        height: 200px;
        padding: 0px;
        margin-top:28px;
        position: absolute;
        width: 436px;
        z-index: 100;
        opacity: 0.95;
        border-radius: 4%;
        border: 2px solid #efefef;
        overflow: auto;
    }
    #display-search{
        border: none;
        color: black;
        font-size: 14px;
        margin: 5px 0 0 0;
        width: 400px;
        height: 20px;
        padding: 0 0 0 10px;
        display: none;
    }

    #hidden-search{
        left: -10000px;
        position: absolute;

    }

    #results{
        display: none;
        width: 300px;
        list-style: none;
    }
    #results ul {
        list-style:none;
        padding-left:0;
    }​
     #results ul li{
         list-style: none;
         padding-left:0;
     }

    #results ul li a{
        color:#2676af;
        font-size: 12px;
        font-weight: bold;
    }
    }
    #search-data{
        font-size: 14px;
        line-height: 20%;
        padding: 0 0 0 20px;

    }

    h2.search-data{
        margin: 10px 0 30px 0;

</style>

<body>

<?php $this->load->view($notification_bar); ?>

<header>

    <?php $this->load->view($header_black_menu); ?>
    <?php $this->load->view($header_logo_white); ?>

</header>
<!-- Responsive design
================================================== -->
<section id="responsive" style="background-color:#f5f5f5;">
    <div class="row white-bg">
        <hr class="" style="margin: 0px;">
        <div class="container white-bg " style="">

            <home class="navbar navbar-default" role="navigation" style="min-height:40px;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#madebyus4u-mobile-responsive-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">

                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <style type="text/css">
                        .main-navigation {
                            background-color: #FAFAFA;
                        }
                        .navbar-default {
                            background-color: inherit;
                            border: none;
                        }
                        .blue-font {
                            color: #2f97cc;
                        }
                        .grey-background{
                            background: #ebebeb;
                        }
                        .bottom-blue-border{
                            border-bottom: 4px solid #216da1;
                        }
                        .wrapper {
                            text-align: center;
                        }

                        .start-shopping-btn {
                            position: absolute;
                            top: 83%;
                            left: 42%;
                            font-size: 27px;
                            background: #0b69a0;
                        }

                        .black-btn{
                            background:#303030;
                        }
                        .box-height{
                            height: 251px;
                        }
                        .pagination{

                            float: right !important;
                        }
                        .pull-right{

                        }

                    </style>
                    <?php $this->load->view($column_main_menu);?>

                </div><!-- /.container-fluid -->
        </div>

    </div>
        <div class="container white-bg" style="padding: 15px;margin-top:20px;margin-bottom: 20px;">


        <div class="col-md-7" style="">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php


                    $image_found = @isset($product->image)?true:false ;
                    $image_many = @is_array($product->image);

                    ?>
                    <img class="img-thumbnail img-thumbnail-large-0 img-responsive" src="<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height: 420px;;">



                </div>

                <!-- Controls -->
                <a class="left carousel-control" id="prev_button" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" id="next_button" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="col-md-11 col-md-large-image no-left-padding" style="">
                <!--<div class="thumbnail rec thmbnail-large">

                    <?php


                $image_found = isset($product->image)?true:false ;
                $image_many = is_array($product->image);

                ?>



                    <img class="img-thumbnail img-thumbnail-large-0 img-responsive" src="<?php echo base_url()."assets/images/Shawn_Tok_Profile.jpg" //$product->image[0];?>" data-holder-rendered="true"  width="100%" style="margin-left:0px;height: 390px;;">
                    <input type ="button" class="classic_button_next btn btn-primary btn-large" id="next_button" value="" style="right: 1%;top: 37%;"/>
                    <input type ="button" class="classic_button_prev btn btn-primary btn-large" id="prev_button" value="Previous <<" style="left: 1%;top: 37%;"/>

                </div>-->
                <div class="col-md-12 col-md-small-images" style="margin-left:12px;padding-top: 14px;">
                    <div class="col-md-2 no-left-right-padding">
                        <button class="btn btn-sm transparent-bg" style="border:1px solid #c2c2c2;"><i class="glyphicon glyphicon-search "></i> </button><span style="padding-left:3px;">Zoom</span>
                    </div>
                    <div class="col-md-2  no-right-padding">
                        <img class="img-thumbnail-small-1 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:80px;width:100%;">

                    </div>
                    <div class="col-md-2  no-right-padding">
                        <img class="img-thumbnail-small-2 img-thumbnail img-responsive small-images dark-blue-border"src="<?php echo !empty($product->image[1])?$product->image[1]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:80px;width:100%;">

                    </div>
                    <div class="col-md-2  no-right-padding">
                        <img class="img-thumbnail-small-3 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo !empty($product->image[2])?$product->image[2]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:80px;width:100%;">

                    </div>
                    <div class="col-md-2  no-right-padding">
                        <img class="img-thumbnail-small-4 img-thumbnail img-responsive small-images dark-blue-border" src="<?php echo !empty($product->image[3])?$product->image[3]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  style="height:80px;width:100%;">

                    </div>
                    <div class="col-md-2 no-right-padding">
                        <img class="img-thumbnail-small-0 img-thumbnail img-responsive small-images" src="<?php echo !empty($product->image[4])?$product->image[4]:base_url().'/uploads/no-photo.jpg';?>" data-holder-rendered="true"  width="100%" style="height:113px;width:100%;display: none;">

                    </div>

                </div>
            </div>



        </div>

        <div class="col-md-4 col-md-offset-1 blue-border" style="padding-bottom: 0px;border-radius: 5px;">


            <h3 class="blue-font"><?php echo $product->product_name;?></h3>
            <p  style="margin-top:-2px;margin-bottom:4px;">
                <!--<span class='seller_name'>
            <i class='glyphicon glyphicon-user'>
            <b style='padding-left:4px;color: #3C4144;;'>By <?php// echo $product->profile->seller_name;?></b>
            </i>
            </span>-->

                <input class="rating" data-stars="5" data-step="1" data-size="sm" id="rating_element-<?echo $product->product_id;?>" value="<? echo $product->p_avg_rating ;?>" data-type="product" />


            </p>
            <hr class="no-margin">
            <p><small class="grey-font">Description</small></p>

            <p class="text-justify" style="font-family:"> <?php echo $product->pdescription;?>  </p>

            <?php if($is_bid_page):?>
                <strong style="color:#515151;">
                <strong>Latest Bid:</strong>
               <span class="">$<?php echo $product->sprice;?>
               <span class="timeleft"><strong style="color: #a8a8a8;">Time Left:</strong>
               <span class="left_text">00:04:14</span> </span>
                </strong>
            <?php else:?>

                <p style="font-size:30px;"><span class="grey-font">$</span><span style="padding-left:2px;"><?php echo $product->sprice;?></span><span class="blue-font" style="font-size: 21px;padding-left:2px;">USD</span></p>

            <?php endif;?>


            <?php if($is_buy_page):?>
                <hr class="" style="margin: 8px;">
                <div class="col-md-4">
                    <div class="input-group">
                        <label for="p_quantity  grey-font">Quantity</label>
                        <select id="p_quantity" name="p_quantity" class="form-control">
                            <option>1</option>
                            <option >3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <label for="p_size  grey-font">Size</label>
                        <select id="p_size" name="p_size" class="form-control">
                            <option >Small</option>
                            <option>Medium</option>
                            <option >Large</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <label for="p_color grey-font">Color</label>
                        <select id="p_color" name="p_color" class="form-control">
                            <option  class="fa fa-circle">Black</option>
                            <option >Blue</option>
                            <option >Red</option>
                            <option >Green</option>
                            <option >Black</option>
                            <option >White</option>
                        </select>
                    </div>
                </div>



            <?php endif;?>

            <div class="buttons col-md-12" style="padding:10px;">

                <!-- dkf BUY NOW BUTTON - Put form to add item to cart here -->
                <?php if($is_buy_page):?>
                    <!-- dkf 06/10/2015 form here with hiddens to add to cart -->
                    <?php echo form_open('shopcart/add_cart_item'); ?>
                    <input type="hidden" name="product_quantity" id="product_quantity" value="1" />
                    <?php echo form_hidden('product_id', $product->product_id); ?>
                    <?php echo form_hidden('product_image', @$product->image[0]); ?>
                    <?php echo form_hidden('product_name', $product->product_name); ?>
                    <?php echo form_hidden('product_price', $product->sprice); ?>
                    <?php echo form_hidden('seller_profile_id', $product->profile->id); ?>
                    <?php echo form_hidden('seller_name', $product->profile->seller_name); ?>
                    <?php echo form_hidden('shipping_price', $product->shipping_price); ?>
                    <div class="col-md-12 no-left-right-padding" style="padding: 10px 0px;">
                        <button type="submit" class="btn btn-primary pull-left  btn-bid full-width" name="Add" value="" ><span class="glyphicon glyphicon-shopping-cart"></span> Buy Now</button>
                    </div>
                    <div class="col-md-12 no-left-right-padding">
                        <button type="submit" class="btn btn-primary pull-left green-bg  no-border full-width" name="Add" value=""><span class="glyphicon glyphicon-plus"></span> Add To Cart</button>
                    </div>
                    <?php echo form_close(); ?>
                    <!--<button class="btn btn-primary pull-left btn-lg btn-bid" style="width: 45%;">
                    <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                    Buy Now
                    </button>-->
                <?php endif;?>
                <?php if($is_bid_page):?>

                <button class="btn btn-primary pull-left  btn-bid" data-bidurl="<?php echo site_url('bid/product/'.$product->product_id) ?>" style="width: 45%;">
                    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                    Bid Now!
                </button>
            </div>
        <?php endif;?>

            <?php if($is_logged_in===TRUE && $is_liked==false && $is_the_product_current_user==false) :?>
                <div class="col-md-6" style="padding: 12px 0px;">
                    <button href="#" class="btn transparent-bg" id="like_link_btn" data-product-id="<?php echo $product->product_id;?>" style="border:1px solid #c2c2c2;float:left;margin-left:20px;padding: 1px 6px;">
                        <span class="glyphicon glyphicon glyphicon-heart grey-font" aria-hidden="true"></span>
                    </button><span style="padding: 6px;">Add to Whishlist</span>
                </div>
            <?php endif;?>
            <?php if($is_logged_in===TRUE && $is_liked==true && $is_the_product_current_user==false) :?>

                <a href="#" disabled='true' class="btn btn-primary btn_black btn-lg" id="like_link_btn" data-product-id="<?php echo $product->product_id;?>" style="margin-left:20px;">
                    <span class="glyphicon glyphicon glyphicon-heart" aria-hidden="true"></span>
                    Liked</a>
            <?php endif;?>

            <div class="col-md-6" style="padding: 12px 0px;">
                <button type="button"class="btn transparent-bg" style="border:1px solid #c2c2c2;float:left;margin-left:20px;padding: 1px 6px;"
                        data-html="true" title="Product Rating"  data-container="body" data-toggle="popover" data-placement="top">
                    <span class="glyphicon glyphicon glyphicon-star grey-font" aria-hidden="true"></span></button><span style="padding: 10px;">Rate this Item</span>

            </div>

            <div class="content" style="display:none;">
                <input class="rating_product" data-stars="5" data-step="1" data-size="xs" id="rating_element-<?=$product->product_id;?>" value="<?= $product->p_avg_rating;?>" data-type="products" />
            </div>





        </div>
    </div>



    <?php

    $this->db->select('quantity');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("order_detail");
    $product_order = $query->result_array();

    $quantity = 0;

    if(count($product_order) != 0){
        foreach($product_order as $order){
            $quantity+=$order['quantity'];

        }
    }

    $this->db->select('*');
    $this->db->where("like_product_id", $product->product_id);
    $query = $this->db->get("likes");
    $product_order = $query->result_array();

    $no_of_recommendation = count($product_order);

    $this->db->select('*');
    $query = $this->db->get("users");
    $total_users = $query->result_array();

    $recommendation_in_percent = ($no_of_recommendation/count($total_users))*100;

    $this->db->select('rating');
    $this->db->where("product_id", $product->product_id);
    $query = $this->db->get("ratings");
    $product_ratings = $query->result_array();

    $no_of_ratings = count( $product_ratings);
    $total_ratings = 0;
    $average_rating = 0;

    if(count($product_ratings) != 0){
        foreach($product_ratings as $ratings){
            $total_ratings+=$ratings['rating'];

        }
        $average_rating = ($total_ratings/$no_of_ratings);
    }



    ?>
    <div class="col-md-7">
        <div class="col-md-12 col-md-12-2 blue-border grey-bg" style="margin-top:5%;border:1px solid #eaeaea;border-radius:4px;padding:8px;">
            <div class="col-md-4  no-left-right-padding">
                <div class="col-sm-12 no-left-right-padding">
                    <div class="col-sm-4">
                        <img src="<?php echo base_url()."assets/images/products/icon01.png";?>">
                    </div>
                    <div class="col-sm-8 no-left-padding">
                        <p class="Numbers">   <?php echo $quantity; ?>+</p>
                    </div>
                    <div class="col-sm-12  no-left-right-padding">
           <span class="short_text">Customers bought
            this product</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-left-right-padding">
                <div class="col-sm-12 no-left-right-padding">
                    <div class="col-sm-4">

                        <img src="<?php echo base_url()."assets/images/products/icon02.png";?>"></p>
                    </div>
                    <div class="col-sm-8 no-left-padding">
                        <p class="Numbers"><?php echo round($recommendation_in_percent); ?>%</p>
                    </div>
                    <div class="col-sm-12  no-left-right-padding">
              <span class="short_text">people Recommend
          this product</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4  no-left-right-padding">
                <div class="col-sm-12 no-left-right-padding">
                    <div class="col-sm-4">

                        <img src="<?php echo base_url()."assets/images/products/icon03.png";?>">
                    </div>
                    <div class="col-sm-8 no-left-padding">
                        <p class="Numbers"><?php print round($average_rating, 1);?>/5</p>
                    </div>

                    <div class="col-sm-12  no-left-right-padding">
            <span class="short_text">Is average rating
        for this product</span>
                    </div>
                </div>

            </div>
        </div>

        <!--main containet-->
        <div class="col-md-12 no-left-right-padding">


            <div class=" col-md-12 alert alert-warning alert-dismissible fade in blue-bg white-font no-margin" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <p>Want to leave your feedback on this product?<a class="white-font" href="<?php echo base_url()."users/login"; ?>"> <strong class="underline">Sign In</strong></a> to leave a comment today.</p>

            </div>




            <!--view-comment-=-->


            <?php $this->load->view($comment_view); ?>




        </div><!--main-end=-->

        <!--left container--listedby-->
        <div class="col-md-4 col-md-offset-1 blue-border no-left-right-padding" style="border:1px solid #ececec;background-color:white;border-radius:4px;">

            <?php

            $this->load->view($product_user_details); ?>


            <?php $this->load->view($product_other_details,$products); ?>

        </div><!--left-end-->

    </div>






</section>

<footer class="footer">

    <?php

    $this->load->view($footer_subscribe);
    $this->load->view($footer_privacy);
    ?>

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url()."assets/plugins/jquery/jquery.min.js";?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url()."assets/plugins/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/subscribe_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/plugins/rating/js/star-rating.min.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/rating_ajax.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/likes.js";?>"></script>
<script type="text/javascript" src="<?php echo base_url()."assets/js/community-scripts/notification.js";?>"></script>
<script  src="<?php echo base_url()."assets/js/categoryMenu.js";?>"></script>


<script type="text/javascript">
    //globals
    var server_url = "<?php echo base_url('rating/rate');?>";
    var readOnly = "<?= $this->is_logged_in==true?false:true;?>";
    /***global base url path in javascript***/
    var base_url = window.location.origin;
    var pathArray = window.location.pathname.split('/');
    var base_url_complete = base_url+'/'+pathArray[1]+'/';
    var csrf_token_hash = "<?= $this->security->get_csrf_hash();?>" ;
    var csrf_token = "<?= $this->security->get_csrf_token_name();?>";
    var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

    // $('.lightbox').lightBox();


    /***gloabl base_url path_end***/
</script>

<script>

    jQuery(document).ready(function () {

        var readOnly = "<?= $this->is_logged_in==true?false:true;?>";

        $('[data-toggle=popover]').popover({
            delay: 200, // this is definitely needed !
            html:true,
            placement:'top',
            content: function() {
                return $('.content').html();
            },
        });
        // Listen for inserted template to DOM
        $('[data-toggle=popover]').on('inserted.bs.popover', function () {
            console.log('popover insetred');

        })

        $('[data-toggle="popover"]').on('shown.bs.popover', function () {

            $(".rating_product").rating('refresh',
                {showClear: false,hoverEnabled:true, hoverChangeStars:true,showCaption: true,starCaptions: {5.0:'5 Stars'}
                });

            $('.rating_product').on('rating.change', function() {

                if(!readOnly) {
                    var value =  $(this).val();

                    var value =  $(this).val();

                    var result = confirm("Are you sure you want to rate this product"+value+" ?");

                    if(result==true){

                        var static_id_text=("rating_element-").length;
                        var profile_id =  ((this).id).slice(static_id_text);
                        var rated = $(this).val();
                        var server_url = "<?php echo base_url('rating/rate');?>";
                        var csrf_token = "<?php echo $this->security->get_csrf_token_name();?>";
                        var csrf_hash = "<?= $this->security->get_csrf_hash();?>" ;

                        save_rating(profile_id,value,'product',profile_id,server_url,csrf_token,csrf_hash);
                    }
                }
                else {
                    window.location.assign("<?=site_url('users/login');?>");
                }

            });


        })


        $(".btn-bid").click(function(e){
            window.location.href = $(this).data('bidurl');
        });

        $(".sub-comment-popup").hide();

// $(".comment-reply a").click(function(){
        $(document).on('click',".comment-reply a",function(event){
            //console.log("are we here");
            event.preventDefault();
            var comment_id = $(this).data("comment-id");
            //console.log("parent");
            //console.log(comment_id);
            $(".sub-comment-popup").each(function(){
                var c_id = $(this).data("comment-id");
                //console.log("child");
                console.log("parent"+comment_id);
                console.log("child"+c_id);

                if(c_id === comment_id){

                    console.log("this is the one");
                    $("body").append("<div class='overlay'></div>");
                    $(".overlay").height($('body').height());
                    $(".overlay").css({
                        'z-index': '3'
                    })
                    $(".overlay").append($(this));

                }else{
                    console.log("this is not the one");
                }

            });
            $("#more-comment-"+ comment_id).show();

        });

        $(".close-buttons").click(function(){
            var comment_id = $(this).data("comment-id");

            $(".sub-comment-popup").each(function(){
                var c_id = $(this).data("comment-id");
                console.log(c_id);

                if(c_id === comment_id){
                    $(this).css('display','none');
                    $(".comment_body").append($(this));
                    $(".overlay").detach();

                }else{
                    console.log("this is not the one");
                }

            });


        });

        $("#like-thumb a").click(function(){
            var comment_id = $(this).data("comment-id");
            var base_url = window.location.origin;
            var pathArray = window.location.pathname.split( '/' );
            var base_url_complete =base_url+'/'+pathArray[1]+'/';
            $url = base_url_complete+'product/comment_like';
            var csrf_hash = "<?= $this->security->get_csrf_hash();?>";

            console.log(comment_id);
            console.log("i am here");


            $.ajax({
                url: $url,
                data: ({'madebyus4u_csrf_test_name':csrf_hash,'comment_id': comment_id}),
                dataType:'json',
                type: "post",
                success: function(data) {
                    if(data.success) {
                        if(data.past_like){
                            var new_like = data.like;
                            var the_link = "#like-link-"+comment_id;
                            var the_link_text = "Liked";

                            var the_span = "#count-of-like-"+comment_id;
                            var the_like = "("+ new_like +")";

                            console.log(the_link_text);
                            $(the_link).text(the_link_text);
                            $(the_span).html(the_like);

                        }else{

                            console.log("the like is now");
                            var new_like = data.like;
                            var the_link = "#like-link-"+comment_id;
                            var the_link_text = "Liked";

                            var the_span = "#count-of-like-"+comment_id;
                            var the_like = "("+ new_like +")";

                            $(the_link).text(the_link_text);
                            $(the_span).html(the_like);
                        }

                    }

                }

            });


        });

        $(".rating").rating('refresh',
            {showClear: false, showCaption: true,size: 'xs',starCaptions: {5.0:'5 Stars'},
            });


        $('.rating').on('rating.change', function() {

            if(!readOnly) {

                var type_of_item_rated = document.getElementById((this).id).getAttribute("data-type");
                var value =  $(this).val();
                var result = confirm("Are you sure you want to rate this product"+value+" ?");


                if(result==true){
                    var static_id_text=("rating_element-").length;
                    var profile_id =  ((this).id).slice(static_id_text);
                    var rated = $(this).val();
                    save_rating(profile_id,value,type_of_item_rated,profile_id,server_url,csrf_token,csrf_hash);

                }
            }
            else {
                window.location.assign("<?=site_url('users/login');?>");
            }
        });
    });



</script>

<script type="text/javascript">

    /***
     * Created by Daniel Adenew
     * Submit email subscription using ajax
     * Send email address
     * Send controller
     * Receive response
     */
    var url =  "<?php echo site_url('welcome/subscribe');?>";
    subscribe_using_ajax(url);
</script>

<script type="text/javascript">

    $(document).ready(function() {

        var current_position = 0;

        var  start_image_path = "<?php echo !empty($product->image[0])?$product->image[0]:base_url().'/uploads/no-photo.jpg';?>";

        //alert(current_position);
        var csrf_hash = "<?= $this->security->get_csrf_hash();?>"
        $('.img-thumbnail-large-0').show();


        $(document).on('click','#prev_button',function(){
            if(current_position>0) {
                current_position = current_position -1;
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                console.log('.img-thumbnail-small-'+current_position);
            }
            //alert(current_position);

        });

        $(document).on('click','#next_button',function(){

            if(current_position<=4){
                current_position = current_position+1;
                $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-'+current_position).attr('src'));
                console.log('.img-thumbnail-small-'+current_position);
            }

        });



        $(document).on('click','.img-thumbnail-small-1',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-1').attr('src'));

            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-2',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-2').attr('src'));
            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-3',function(){

            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
            //replace image one on this pic place


            return false;
        });

        $(document).on('click','.img-thumbnail-small-4',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-4').attr('src'));
            //replace image one on this pic place
            return false;
        });

        $(document).on('click','.img-thumbnail-small-0',function(){
            $('.img-thumbnail-large-0').attr('src', $('.img-thumbnail-small-3').attr('src'));
            //replace image one on this pic place
            $('.img-thumbnail-small-3').attr('src',start_image_path);


            return false;
        });

        // Quantity buttons add and decerase
        $("#increment_button").click(function(){
            //alert("Increase quantity.");
            var qty = parseInt($("#p_quantity").val(),10);
            //alert(qty);
            if (qty < 10){
                newqty = qty + 1;
                $("#p_quantity").val(newqty.toString());
                $("#product_quantity").val(newqty.toString());
                //alert($("#product_quantity").val());
            }
        });

        $("#decrement_button").click(function(){
            //alert("Increase quantity.");
            var qty = parseInt($("#p_quantity").val(),10);
            //alert(qty);
            if (qty > 1){
                newqty = qty - 1;
                $("#p_quantity").val(newqty.toString());
                $("#product_quantity").val(newqty.toString());
                //alert($("#product_quantity").val());
            }
        });

    });//end of document ready

</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>
