
<style type="text/css">
  
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
   .hr_border {
border-top: 2px dotted #818181;

}
.left-menu{
border: 1px solid #e5e5e5;
border-radius: 3px;
background-color: white;
padding: 12px;
margin-bottom: 7%;
margin-left: -27px;
}
.left-choice-1 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_dadboard.png);
background-repeat: no-repeat;
}

.left-choice-2 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_orders.png);
background-repeat: no-repeat;
}

.left-choice-3 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_pursche.png);
background-repeat: no-repeat;
}

.left-choice {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #ffffff;
margin-bottom: 2%;
/*width: 14.667%;*/
}
.table_containers
{

background-color: white;
padding: 10px;
border: 1px solid #e6e6e6;
}
.nav-pills a {
    color: #85888A;
    text-decoration: none;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    color: #fff;
    background-color: #423E3E;
}
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }

.media-object {

  width: 111px;
  height: 103px;
  float: leftpadding:5;
  float: left;
  padding: 8px;
}
.full_name {
    padding-top: 20px;
    font-weight: 800;
    float: left;
    text-align: left;
}
.message_content {
  padding-top: 20px;
  font-weight: 800;
}
.state {
  font-weight: 400;
}
.subject {
  font-weight: 600;
}
.messages {
  font-weight: 200;
}
.logged_in_status {
font-weight: 100px;
}
.date_column {
   padding-top: 20px;
  font-weight: 200;
}
.column_border {
  border-right: 1px dotted rgb(231, 226, 226) ;
}
</style>

<div class="container">
<!--dashboard image hodlers START-->
  <div class="col-md-2 left-choice left-menu pull-left"> 

      <ul class="nav nav-pills nav-stacked">

      <li role="presentation" class="active"><a data-target="#composeNew" data-toggle="modal" aria-controls="inbox"><i class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true" style='padding-right:5px;'></i>Compose New</a></li>
      <li role="presentation"><a href="#inbox" data-toggle="tab" aria-controls="inbox" role="tab"><i class="glyphicon glyphicon glyphicon-inbox" aria-hidden="true" style='padding-right:5px;'></i> Inbox<span class="badge"><?php echo count($inbox_messages);?></span></a></li>
      <li role="presentation"><a href="#sent" data-toggle="tab" aria-controls="orders" role="tab"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i> Sent<span class="badge"><?php echo count($sent_messages);?></span></a></li>
      <li role="presentation"><a href="#trash" data-toggle="tab" aria-controls="orders" role="tab"><i class="glyphicon glyphicon glyphicon-trash" aria-hidden="true" style='padding-right:5px;'></i>Trash<span class="badge"><?php echo count($trashed_messages);?></span></a></li>

      </ul>     

  </div>      

  <div class="col-md-10">
    
    <div id="messagesTabContent" class="tab-content "> 

    <!--dashboard table-->      
      <div class="tab-pane active" id="inbox"><?php $this->load->view('messages/inbox_view_new');?></div>
        <div class="tab-pane"  id="inbox_replay_view"><?php $this->load->view('messages/inbox_replay_view_new');?></div>
      <div class="tab-pane"  id="sent"><?php $this->load->view($sent_view);?></div>
      <div class="tab-pane"  id="trash"><?php $this->load->view($trash_view);?></div>

    </div>


             
          </div>  </div>

<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="composeNew" tabindex="-1" role="dialog" aria-labelledby="composeNew">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="composeNew"><i class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true" style='padding-right:5px;'></i> Compose a Message</h4>
      </div>
      <div class="modal-body" style="font-color:grey;font-style:italic;">
      <!--show success message -->
        <div  class="sent-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> Your message sent succesfully!
        </div>

        <form>
          <div class="input-group">

             <span class="input-group-addon">
             <img id="memeber_image" class='' src="<?php echo base_url().'/uploads/no-photo.jpg';?>" height=80 width=80/>
             </span>
          <?php  
                    $members_list['#'] = 'Select a Memeber';
                    echo form_dropdown('selected_member', $members_list,'#','id="selected_member"
                          class="form-control" required style="color:grey;font-style:italic;"'); ?>
          </div>
          <p></p>
          <div class="form-group">           
            <input class="form-control" id="subject" placeholder="Subject:" required></input>
          </div>
          <div class="form-group">           
            <textarea class="form-control" rows='5' id="message_text" required placeholder="Type your message here.."></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer ">

        <button type="button" id="send_message" name="send_message" class="btn btn-primary"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i>Send message</button>
      </div>
    </div>
  </div>
</div>

<!---modal-->
<div class="modal fade" id="interact_with_message_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Message thread</h4>
      </div>
      <div class="modal-body">
        <form>
           <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea class="form-control" id="message-text"><?php echo 'not message';?></textarea>
          </div>
        </form>
        <div class="form-group">
            <label for="replay-message-text" class="control-label">Replay:</label>
            <textarea class="form-control" id="replay-message-text"><?php echo 'comming soon';?></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" readOnly="true" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" readOnly="true" class="btn btn-primary">Replay</button>
      </div>
    </div>
  </div> <!--end of modal-->

  </div>
