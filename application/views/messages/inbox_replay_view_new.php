
<div class="col-md-12 panel panel-default no-left-right-padding" >
  <!-- Default panel contents -->
        <div class="col-md-12 blue-bg no-left-right-padding">
            <div class="col-md-2 panel-heading  white-font" style="font-weight:700;">Inbox</div>
                 <div class="col-md-5 col-md-offset-5" style="padding: 8px;">
                         <div class="col-md-5">
                             <input type="checkbox"><span  style="padding-left: 10px;" class="white-font">Select All</span>
                         </div>
                        <div class="col-md-6"><button class="btn btn-sm"><i class="glyphicon glyphicon-trash" style="color:#d9312b;"></i></button><span style="padding-left: 10px;" class="white-font">Delete Selected</span></div>
                 </div>
       </div>
   <!--show success message -->
        <div  class="trash-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> Your message will be stored in the trash for 30 day before deletion!
  </div>

    <div class="important-message alert alert-success alert-dismissible" role="alert" style="display:none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong>Selected Message is marked as important!
    </div>



  <!-- Table -->
<div class="col-md-12 white-bg no-left-right-padding">
  <table class="table table-striped no-left-right-padding">
  <?php foreach ($inbox_messages as $message) { ?>
  <tr>
    <td style="width: 42px;padding: 22px;"> 
    
        <input type="checkbox">
     
    </td>
    <td style="width: 80px;">
      <a href="#">
          <img class="img-circle" s src="<?php //echo base_url($message->media->file_name);?>../../assets/images/Shawn_Tok_Profile.jpg" data-holder-rendered="true" style="width: 58px; height: 58px;float:left:padding:5;">
      </a>
    </td>
    <td  style="width:150px;">
      <a href="<?php echo base_url('sell/seller').'/'.$message->profile_id;?>" class="full_name blue-font">
          <?php echo $message->full_name;?>
      </a>
      
         
      <p><span class="date black-font"> <?php echo $message->date;?></span><p>
     
      
     
    </td>
    <td style="width: 437px;">
     
        
          
         
          <p class='messages black-font'>
            <?php 
            $limit = 120;
              
              $output = htmlspecialchars(strtolower($message->message_text), ENT_QUOTES, 'UTF-8');
                if (strlen($output) > $limit) {
                  $output = substr($output, 0, $limit) . ' <a href="#">... read more</a>';
                }

             echo $output;;?>
          </p>
          <p><i class="fa fa-star grey-font" style="padding-right: 5px;font-size: 18px;"  aria-hidden="true"></i><span class="save_as_important black-font" style="font-weight:600;" id="save_message_sent+<?php echo $message->id?>"
                                                                                                                       data-message-id="<?php echo $message->id;?>" >Save as Important</span>
         
          
         
    </td>
    <td>
      
    </td>
    <td>
       <a href="" class="remove"  data-id="<?= $message->id;?>">
        <i class='glyphicon glyphicon-trash' style='color:red;padding-right:2px;'></i>
        </a>
    </td>
    </tr>
    <?php } ?>
    
    </table>
    <div class="col-md-12 " style="padding: 13px;">
        <div class="form-group col-md-12 grey-bg" style="padding: 9px 0px 0px;">
                <div class="col-md-1" style="padding-left: 39px;">
                    <a class="media-left media-top" href="#" style="padding-bottom: 9px">
                        <img class="img-rounded pull-left user-small-image" src="../../assets/images/Shawn_Tok_Profile.jpg" alt="..." height="33">
                    </a>
                </div>
                <div class="col-md-9 no-left-padding">
                <input type="text" class="form-control" placeholder="Write a comment..."/>
                </div>

                <div class="col-md-2">
                <button class="btn btn-primary">Reply</button>
                 </div>
        </div>



    </div>
 
    

 
 
    

  
 
 </div>

 
</div>






