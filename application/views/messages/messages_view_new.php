
<style type="text/css">
  
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }
 
   .hr_border {
border-top: 2px dotted #818181;

}
.left-menu{
border: 1px solid #e5e5e5;
border-radius: 3px;
background: transparent none repeat scroll 0% 0%;
padding: 0px;


/*margin-bottom: 7%;
margin-left: -27px;*/
}
.left-choice-1 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_dadboard.png);
background-repeat: no-repeat;
}

.left-choice-2 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_orders.png);
background-repeat: no-repeat;
}

.left-choice-3 {
border: 1px solid #e5e5e5;
border-radius: 2px;
background-color: #f7f7f7;
margin-bottom: 2%;
height: 135px;
width: 140px;
background: url(../assets/images/payment/my_pursche.png);
background-repeat: no-repeat;
}

.left-choice {
border: 1px solid #e5e5e5;
border-radius: 2px;

margin-bottom: 2%;
/*width: 14.667%;*/
}
.table_containers
{

background-color: white;
padding: 10px;
border: 1px solid #e6e6e6;
}
.nav-pills a {
    color: #85888A;
    text-decoration: none;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
  
    color:#428bca;
   
}
  .xpens {
    
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
  }

.media-object {

  width: 111px;
  height: 103px;
 
  float: left;
  padding: 8px;
}
.full_name {
    padding-top: 0px;
    font-weight: 600;
    float: none;
    text-align: left;
    font-size: 15px;
}
.message_content {
  padding-top: 20px;
  font-weight: 800;
}
.state {
  font-weight: 400;
}
.subject {
  font-weight: 600;
}
.messages {
  font-weight: 200;
}

.nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
    
    background: transparent;
}
.a-tag-padding{

    padding: 24px 15px !important;
}
.bottom-border{
  border-bottom: 1px solid rgb(224, 224, 224);
}


</style>

<div class="container">
    <!--START-->
    <div class="col-md-2 left-choice left-menu pull-left">

        <ul class="nav nav-pills nav-stacked">

            <li role="presentation" class="active" style="background: rgb(255, 255, 255);padding: 15px;"><a data-target="#composeNew" data-toggle="modal" aria-controls="inbox" class="blue-bg white-font" style=" padding: 4px 15px;" >Compose Message</a></li>
            <li role="presentation"><a href="#inbox" data-toggle="tab" aria-controls="inbox" role="tab" class="a-tag-padding bottom-border"><i class="glyphicon glyphicon glyphicon-inbox" aria-hidden="true" style='padding-right:5px;'></i> Inbox<span class="badge"><?php echo count($inbox_messages);?></span></a></li>
            <li role="presentation"><a href="#sent" data-toggle="tab" aria-controls="orders" role="tab" class="a-tag-padding bottom-border"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i> Sent<span class="badge"><?php echo count($sent_messages);?></span></a></li>
            <li role="presentation"><a href="#important" data-toggle="tab" aria-controls="orders" role="tab" class="a-tag-padding bottom-border"><i class="fa fa-star" aria-hidden="true" style='padding-right:5px;'></i> Important<span class="badge"><?php echo count($important_messages);?></span></a></li>
            <li role="presentation"><a href="#drafts" data-toggle="tab" aria-controls="orders" role="tab" class="a-tag-padding bottom-border"><i class="glyphicon glyphicon-edit" aria-hidden="true" style='padding-right:5px;'></i> Drafts<span class="badge"><?php echo count($draft_messages);?></span></a></li>
            <li role="presentation"><a href="#trash" data-toggle="tab" aria-controls="orders" role="tab" class="a-tag-padding bottom-border"><i class="glyphicon glyphicon glyphicon-trash" aria-hidden="true" style='padding-right:5px;'></i>Trash<span class="badge"><?php echo count($trashed_messages);?></span></a></li>
        </ul>

    </div>

    <div class="col-md-10">

        <div id="messagesTabContent" class="tab-content ">

            <!--dashboard table-->
            <div class="tab-pane active" id="inbox"><?php $this->load->view('messages/inbox_view_new');?></div>
            <div class="tab-pane"  id="inbox_replay_view"><?php $this->load->view('messages/inbox_replay_view_new');?></div>
            <div class="tab-pane"  id="sent"><?php $this->load->view($sent_view);?></div>
            <div class="tab-pane"  id="important"><?php $this->load->view($important_view);?></div>
            <div class="tab-pane"  id="drafts"><?php $this->load->view($drafts_view);?></div>
            <div class="tab-pane"  id="trash"><?php $this->load->view($trash_view);?></div>

        </div>



    </div>  </div>

<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="composeNew" tabindex="-1" role="dialog" aria-labelledby="composeNew">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="composeNew"><i class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true" style='padding-right:5px;'></i> Compose a Message</h4>
            </div>
            <div class="modal-body" style="font-color:grey;font-style:italic;">
                <!--show success and darft saved message -->
                <div  class="sent-message alert alert-success alert-dismissible" role="alert" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> Your message sent successfully!
                </div>
                <div class="draft-message alert alert-success"><span class="alertMessage">
                <span id="msg">Auto Save for messages is turned on!<p>
                        <strong>Saved message will be listed on Draft Box , if not sent</strong>
                    </p>
                </span></span>
                </div>




        <?php

        $attributes = array('class' => 'form', 'novalidate' => 'novalidate', 'id' => 'compose_form', 'name' => 'compose_form',);
        echo form_open_multipart('message/auto_save/', $attributes);
        ?>
                    <div class="input-group">

             <span class="input-group-addon">
             <img id="memeber_image" class='' src="<?php echo base_url().'/uploads/no-photo.jpg';?>" height=80 width=80/>
             </span>
              <input type="hidden" name="last_saved_message_id" id="last_saved_message_id" value="-1"/>
                        <?php
                        $members_list['#'] = 'Select a Memeber';
                        echo form_dropdown('selected_member', $members_list,'#','id="selected_member"
                          class="form-control" required style="color:grey;font-style:italic;"'); ?>
                    </div>
                    <p></p>
                    <div class="form-group">
                        <input class="form-control" id="subject" name="subject" placeholder="Subject:" required/>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows='5' name="message_text2" id="message_text2" required placeholder="Type your message here.."></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer ">

                <button type="button" class="send_message" id="send_message" name="send_message" class="btn btn-primary"><i class="glyphicon glyphicon glyphicon-send" aria-hidden="true" style='padding-right:5px;'></i>Send message</button>
            </div>
        </div>
    </div>
</div>

<!---modal-->
<div class="modal fade" id="interact_with_message_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Message thread</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea class="form-control" id="message-text"><?php echo 'not message';?></textarea>
                    </div>
                </form>
                <div class="form-group">
                    <label for="replay-message-text" class="control-label">Replay:</label>
                    <textarea class="form-control" id="replay-message-text"><?php echo 'comming soon';?></textarea>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" readOnly="true" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" readOnly="true" class="btn btn-primary">Replay</button>
            </div>
        </div>
    </div> <!--end of modal-->

</div>
