
<div class="panel panel-default">
  <!-- Default panel contents -->
    <div class="col-md-12 blue-bg no-left-right-padding" style="padding-top: 12px;">
        <div class="col-md-2 panel-heading  white-font" style="font-weight:700;">Inbox</div>
        <div class="col-md-5 col-md-offset-5" style="padding: 8px;">
            <div class="col-md-5">
                <input type="checkbox"><span  style="padding-left: 10px;" class="white-font">Select All</span>
            </div>
            <div class="col-md-6"><button class="btn btn-sm"><i class="glyphicon glyphicon-trash" style="color:#d9312b;"></i></button><span style="padding-left: 10px;" class="white-font">Delete Selected</span></div>
        </div>
    </div>



  <!-- Table -->
<div class="col-md-12 white-bg no-left-right-padding">
  <table class="table table-striped no-left-right-padding">
  <?php foreach ($inbox_messages as $message) { ?>
  <tr>
    <td style="width: 42px;padding: 22px;"> 
    
        <input type="checkbox">
     
    </td>
    <td style="width: 80px;">
      <a href="#">
          <img class="img-circle"  src="<?php echo base_url($message->media->file_name);?>" data-holder-rendered="true" style="width: 50px; height: 50px;float:left:padding:5;">
      </a>
    </td>
    <td  style="width:150px;">
      <a href="<?php echo base_url('sell/seller').'/'.$message->profile_id;?>" class="full_name blue-font">
          <?php echo $message->full_name;?>
      </a>
      
         
      
     
      
     
    </td>
    <td>
    	<p><i class="fa fa-star grey-font" style="padding-right: 5px;font-size: 18px;"  aria-hidden="true"></i>
            <a class="save_as_important black-font" style="font-size: 13px;font-weight:600;" id="save_message_inbox+<?php echo $message->id?>" data-message-id="<?php echo $message->id;?>">
                Save as Important</a>
    </td>
    <td>
     
        
          
         
          <p class='messages black-font'>
            <?php 
            $limit = 20;
              
              $output = htmlspecialchars(strtolower($message->message_text), ENT_QUOTES, 'UTF-8');
                if (strlen($output) > $limit) {
                  $output = substr($output, 0, $limit) . ' <a href="#">... read more</a>';
                }

             echo $output;;?>
          </p>

          
         
    </td>
    <td>
      <p><span class="date black-font"> <?php echo $message->date;?></span><p>
    </td>
        <td>
            <a href="#inbox_replay_view" data-toggle="tab" aria-controls="inbox" role="tab" class=" btn  btn-primary btn-sm  bottom-border">View</a

        </td>
      <td>
       <a href="" class="remove"  data-id="<?= $message->id;?>">
        <i class='glyphicon glyphicon-trash' style='color:red;padding-right:2px;'></i>
        </a>
    </td>
    </tr>

    <?php } ?>
    
    </table>
 
    

 
 
    

  
 
 </div>

 
</div>






