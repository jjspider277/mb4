<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading" style="background-color:rgb(66, 62, 62);color:white;font-weight:700;">Trash</div>
   <!--show success message -->
        <div  class="deleted-message alert alert-success alert-dismissible" role="alert" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> Your message has been deleted from the system! 
        </div>

<?php foreach ($trashed_messages as $message) { ?>

 <!-- Table -->
<div class="col-md-12" style="background-color:white;padding:15px;">

 <div class="col-md-2">
   <a href="#">
          <img class="media-object" style="" src="<?php echo base_url($message->media->file_name);?>" data-holder-rendered="true" style="width: 80px; height: 80px;float:left:padding:5;">
        </a>
 </div>

    <div class="col-md-2 column_border">

      
        <div class="full_name">
         <a href="<?php echo base_url('sell/seller').'/'.$message->profile_id;?>">
          <?php echo $message->full_name;?></a>
         <p class='state' style="font-weight:100;"> <?php echo $message->city.' '.$message->state;?></p>
         <?php if($is_logged_in===TRUE && $loaded_profile_same_as_user==false && in_array($message->profile_id,$loggedin_profile_lists) ): ?>
         <p>
         <i class='messages glyphicon glyphicon-record' style='color:#69DA32;padding-right:2px;'></i>User is online
        </p>
        <?php endif?>
        </div>
     
    </div>

  <div class="col-md-5">

   <div class="message_content column_border">
        <span class="subject"> <?php echo $message->subject;?></span>
      <a href="#" id="interact_lablel" class='interact_lablel' data-message-id="<?php echo $message->id;?>" class='label label-info btn-info' >click to see </a>
          <p class='messages'>
            <?php 
            $limit = 20;
              
              $output = htmlspecialchars(strtolower($message->message_text), ENT_QUOTES, 'UTF-8');
                if (strlen($output) > $limit) {
                  $output = substr($output, 0, $limit) . ' <a href="#">... read more</a>';
                }

             echo $output;;?>
          </p>
         
        
        </div>
 </div>
  <div class="col-md-2">
   <div class="date_column">
        <span class="date"> <?php echo $message->date;?></span>
        <p class='messages'> <?php echo $message->time;?></p>
         
        </div>
 </div>
 <div class="col-md-1" >
   <div class="message_content">
       
     <a href="" class="delete"  data-id="<?= $message->id;?>">
    <i class='glyphicon glyphicon-remove' style='color:red;padding-right:2px;'></i>
</a>

        </div>
     
 </div>
</div>
 <?php } ?>

</div>  



<div class="container col-md-12" style="border-bottom:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center;padding:5px;border-top:1px solid rgba(41, 41, 41, 0.26);background-color:#ffffff;text-align:center">
<strong>View more</strong>
</div>




