<?php
/**
 * Created by Daniel Adenew.
 * Date: 3/27/2015
 * Time: 12:48 PM
 * This a wrapper library class for
 * image resize and other processing utility
 *
 */

class ImageUtility_service {

    private $CI;

    public function __construct() {

        $this->CI =& get_instance();

    }

  public function resize_profile_images($srcImagePath,$config){

        $config = $this->CI->load->config('image_lib');
       
        $config = $this->CI->config->item('image_resize_config_profile');

        $config['source_image'] = $srcImagePath;
       
        $this->CI->load->library('image_lib',$config);

        $this->CI->image_lib->resize();

        echo $this->CI->image_lib->display_errors();
    }

    public function resize_upload_images($srcImagePath,$config){

        var_dump($srcImagePath);

        $config = $this->CI->load->config('image_lib');
        $config = $this->CI->config->item('image_resize_config');

        $config['source_image'] = $srcImagePath;
        $config['new_image'] = './uploads/profile/3/products/';    
        $config['master_dim'] = 'height';
        $config['new_image'] = "./uploads/profile/4/products/productss_tumb.png";

        $this->CI->load->library('image_lib',$config);

        $this->CI->image_lib->resize();

        $this->CI->image_lib->clear();


        echo $this->CI->image_lib->display_errors();
    }

     public function resize_store_image ($src_image,$path) {

        $config = $this->CI->load->config('image_lib');
        $config = $this->CI->config->item('image_resize_store_config');
        $config['source_image'] = $path.$src_image;
        $config['master_dim'] = 'height';            

        $config['source_image'] = $src_image;
        $config['new_image'] = $path;    
        $config['master_dim'] = 'height';

        $this->CI->load->library('image_lib',$config);

        $this->image_lib->initialize($config);
        
        if ( ! $this->image_lib->resize())
        {
            echo json_encode($this->CI->image_lib->display_errors('<p>', '</p>'));
            return ;
        }

        $this->image_lib->clear();

         $json_data = array(
                'success'     =>true,
                'thumbnail_created'  =>$path.$config['new_image'],
                'resized_image' => $path,
  
        );

         $json_success_data =null;
         echo json_encode($json_success_data);
         return $newfilename;
    }
   

   public function resize_product_image() {

        $config = $this->CI->load->config('image_lib');
        $config = $this->CI->config->item('image_resize_store_config');
        $config['source_image'] = $path.$src_image;
        $config['master_dim'] = 'height';            

        $config['source_image'] = $srcImagePath;
        $config['new_image'] = $path;    
        $config['master_dim'] = 'height';

        $this->CI->load->library('image_lib',$config);

        $this->image_lib->initialize($config);
        
        if ( ! $this->image_lib->resize())
        {
            echo json_encode($this->CI->image_lib->display_errors('<p>', '</p>'));
            return ;
        }

        $this->image_lib->clear();

         $json_data = array(
                'success'     =>true,
                'thmbnail_created'  =>$path.$newfilename,
                'resized_image' => $path,
  
        );

        echo json_encode($json_success_data);

        return $newfilename;


   }

    function resizeImg($imgsrc ,$maxW='*', $maxH='*', $allowScaleUp=0,$returnHTML="alt='image'") {
     if($s=getimagesize($imgsrc)){
      $oW=$s[0];$oH=$s[1];
      if(($oW>$maxW && $maxW!='*') || ($oH>$maxH && $maxH!='*') || $allowScaleUp){//if resize is needed:
       if($maxW && $maxH=='*'){ //constrain by width:
        $proportion=$oH/$oW;
        $w=$maxW;
        $h=$maxW*$proportion;   
       }else if($maxH && $maxW=='*'){ //constrain by height:
        $proportion=$oW/$oH;
        $h=$maxH;
        $w=$maxH*$proportion;
       }else if(!$maxW && $maxH){ //constrain by smallest side:
        return($oW>$oH ? $this->resizeImg($imgsrc, '*', $maxH, $allowScaleUp, $returnHTML) : resizeImg($imgsrc, $maxW, '*', $allowScaleUp, $returnHTML));
       }else if($maxW && !$maxH){ //constrain by largest side:
        return($oW>$oH ? $this->resizeImg($imgsrc, $maxW, '*', $allowScaleUp, $returnHTML) : resizeImg($imgsrc, '*', $maxH, $allowScaleUp, $returnHTML));
       }else{
        return($maxW>$maxH ? $this->resizeImg($imgsrc, '*', $maxH, $allowScaleUp, $returnHTML) : resizeImg($imgsrc, $maxW, '*', $allowScaleUp, $returnHTML));
       }
      }else{
       $w=$oW;$h=$oH;
      }
      echo "orig: ".$oW."x:".$oH."<br />max: ".$maxW."x".$maxH."<br />new: ".$w."x".$h."<br />"; //debug
      $w=round($w); $h=round($h);
      return array(0=>$w,1=>$h,"width"=>$w,"height"=>$h);
     }else{//file does not exist or is not an image:
      return false;
     }
}

} 