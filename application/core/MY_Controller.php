<?php
/**
 * Created by Daniel Adenew.
 * User: Craig Robinson
 * Date: 12/11/14
 * Time: 2:19 PM
 */

class MY_Controller extends CI_Controller {

    public $message = array('type'=>'','message'=>'');
    public $user_id ;
    public $is_admin;
    public $profile_id;
    public $profile_fname;
    public $profile_lname;
    public $profile_email;
    public $error = array();
    public $data = array();
    public $is_logged_in;
    public $is_store_created=0;
    private $current_store_ids=array();
    private $current_profile_products_ids = array();
    public static $online_profile_user_ids = array();
    public $current_message_notifications_counts = 0;
    public $current_friend_notifications_counts = 0;
    public $current_notify_notifications_counts = 0;
    protected $current_profile_pending_requests_id= array();
    public $my_requests_to_approve_id = array();
    public $current_friends_count ;
    public $current_profile_friend_lists = array();
    public $current_profile_likes = array();

   

    //dkf 06/11/2015
    public $cartcount = 0;

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('profile_model','profile');
        $this->load->model('notification_model','notifications');
        $this->load->model('friend_model','friends');
        $this->load->model('like_model','likes');
        //dkf
        $this->load->model('cart_model');
        $cartcount = $this->cart_count();
        $this->session->set_userdata('cart_item_count', $cartcount);
        $this->get_logged_in_users();
        
       /***
        * DO this thing when you want to secure
        * evey resource with url
        */
        $this->logged_in();



       
       if ($this->session->userdata('is_admin') != TRUE) {
          
      
       //make available useful variables
       $this->get_session_variables();
 
        if($this->is_logged_in==true) {
         //count notifications
          $this->current_message_notifications_counts =$this->notifications->count_messages_notification($this->profile_id);
          //var_dump($this->current_message_notifications_counts);exit;
          
          //get freind request notifications
           //count notifications for friend request
          $this->current_friend_notifications_counts = $this->notifications->count_freinds_request_notification($this->profile_id);

          //count notification for notifications
          $this->current_notify_notifications_counts = $this->notifications->count_all_notifications($this->profile_id);

          //get_pending_friend_requests
          //var_dump($this->profile_id);
           $this->current_profile_pending_requests_id = $this->get_pending_friend_request($this->profile_id);        

          //get to be accepted
          //var_dump($this->profile_id);
          $this->my_requests_to_approve_id = $this->get_apporavl_friend_request($this->profile_id);         
        
          //get actual freinds accepted or requested accepted
          $this->current_profile_friend_lists = $this->get_current_profile_friends_profile_id($this->profile_id);
  
          //count friends?
          $this->current_friends_count = $this->friends->count_friends($this->profile_id);
          //var_dump($this->current_friends_count);exit;

          //count likes?
          //count_likes_notification
          $this->current_profile_likes = $this->notifications->count_likes_notification($this->profile_id); 
          //dump($this->current_profile_likes);exit
            $this->_notification_information_for_seller($this->profile_id);
        }


      }
        else {
          $this->is_admin = true;
         }





    }
    
    //dkf 06/11/2015
    public function cart_count(){
        return $this->cart->total_items();
    }
    /***
     * check if resource requires login or not
     * only listed url are accessible without login
     */

    public function logged_in(){

        $this->load->library('ion_auth');
        $public_allowed_urls = $this->config->item('public_urls');

        /*** this works but better below code
        //get the current URL
        $controller= $this->uri->segment(1); // controller
        $action = $this->uri->segment(2); // action
        $url = $controller . '/' . $action;
       **/
     

        $controller= $this->router->class ; //controller
        $action =$this->router->method; // action
        $url = $controller . '/' . $action;


        if(  ($this->ion_auth->logged_in()== false ) && !( in_array($url,$public_allowed_urls) ) ) {
             redirect('users/login');
        }
     
        $this->is_logged_in = (bool) $this->session->userdata('logged_in');

       // if($this->is_logged_in) {
         // $sql = ""
        //}
        

    }

    //TODO:

    function _render_page($view, $data=null, $render=false)
    {

        $tweets_page = 'include/tweets';
        $video_page = 'include/video_page';
        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';
        $header_logo_white = 'include/header_logo_white';
        $main_menu = 'include/main_menu';

        $paginate_page = 'include/paginate_page';
        $notification_bar = 'include/notification_bar';

        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['header_black_menu'] = 'include/header_black_menu';
        $data['paginate_page'] = $paginate_page;
        $data['notification_bar'] = $notification_bar;

        $data['header_black_menu'] = 'include/header_black_menu';
        $data['header_logo_white'] = 'include/header_logo_white';
        $data['show_error_page'] = 'include/show_error_page';
        $data['footer_privacy'] = 'include/footer_privacy';
        $data['footer_subscribe'] = 'include/footer_subscribe';
        $data['signin_form'] = 'include/signin_form';
        $data['new_to_madeby'] = 'include/new_to_madeby';
        $data['navigation_top'] = 'include/navigation_top';
        $data['notification_bar'] = 'include/notification_bar';

        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $render);

        if (!$render) return $view_html;
    }

    public function get_session_variables(){

        $profile_id = $this->session->userdata('profile_id');
        $user_id = $this->session->userdata('user_id');     
       


        //get the current user profile id to upload / create a folder by its id
        $this->profile_id =  isset($profile_id) ? $profile_id : null ;
        //user_id
        $this->user_id =  isset($user_id) ? $user_id : null ;
        //user_id

        //check if current user has created at least one store or not
        $result = $this->profile->with('stores')->get($profile_id);
        
        //var_dump($this->get_current_profile_stores(3));;
        //var_dump($this->get_current_profile_products(3));exit;
      
        if(count($result)>0){
             

            if( !empty($result->stores) ) {
                
                 $this->is_store_created =  1 ;

            } else {
                $this->is_store_created =  0 ;
            }
               
        }

       $this->is_logged_in = (bool) $this->session->userdata('logged_in');

       if($this->is_logged_in) {

        //get fname and last name of the profile
        $this->profile_fname = ucfirst($this->session->userdata('profile_fname'));
        $this->profile_lname = ucfirst($this->session->userdata('profile_lname'));
        $this->is_admin = $this->session->userdata('is_admin'); 
       }
     



       
    }

    /**
     * Sanitize function for input post variables
     * @param $value
     * @return string
     */
    function sanitize($value) {
        return trim(strip_tags($value));
    }

   public function load_profile_detail_with_image($id)
    {

       // isset($value) ?$this->profile_id=$value : $this->profile_id ;

           
        if(!empty($id)) {
            
               $profile = $this->profile->with('media')->get($id);

                $this->data['fname']  = $profile->fname ;               
                $this->data['lname']  = $profile->lname ;
                $this->data['zipcode']  = $profile->zipcode ;
                $this->data['profile_rating'] = $profile->avg_rating;
                $this->data['profile_id'] = $profile->id;

               if(!empty($profile) && count($profile->media) > 0)  {

                $profile_image = "uploads/profile/" . $profile->id . "/avatar/" . $profile->media->file_name;
              
               
               } else {

                $profile_image = "uploads/no-photo.jpg";
               
               }

                $this->data['profile']  = $profile ;               
                $this->data['profile_image']  = $profile_image ;
               
          }
         

            return $this->data;       
          
    }

    public function get_current_profile_stores($profile_id) {

        //check if current user has created at least one store or not
        $profile_with_stores = $this->profile->with('stores')->get($profile_id);

        if(!empty($profile_with_stores->stores)){

           foreach ($profile_with_stores->stores as $key => $store_object) {
              array_push($this->current_store_ids,$store_object->id);
            }
        }

     
     return $this->current_store_ids;



    }

    public function get_current_profile_products($profile_id){

        //check if current user has created at least one store or not
        $profile_with_products = $this->profile->with('products')->get($profile_id);


        if(!empty($profile_with_products->products)){

           foreach ($profile_with_products->products as $key => $products_object) {
              array_push($this->current_profile_products_ids,$products_object->id);
            }
        }

        return $this->current_profile_products_ids;
    }

   /* USB */
   public function get_select_list_of_memebers() {

      $select_options = $this->profile->build_memeber_list($this->profile_id);
      return $select_options;
  }

   public function get_image_file_path($id) {
     
     $this->db->cache_on();
     $profile = $this->profile->with('media')->get($id);
     $this->db->cache_off();
     if(!empty($profile->media)) {

       $profile->media->file_name = '/uploads/profile/'.$profile->id .'/avatar/'. $profile->media->file_name;
     } 
     else {

          $profile->media->file_name = '/uploads/no-photo.jpg';
     }

      return $profile->media->file_name;
  }



     public function get_logged_in_users() {
     $this->load->model('session_model','session_manager');
     self::$online_profile_user_ids = $this->session_manager->get_logged_in_users();
     //array of lists
    }

    public function get_pending_friend_request($reciever_profile_id) {

      $pending_profile = $this->friends->get_many_by(array('friend_status'=>'REQUEST','reciever_profile_id'=>$reciever_profile_id));
  

       foreach ($pending_profile as $key => $friend_profile) {
          $this->current_profile_pending_requests_id[$key] = $friend_profile->sender_profile_id;
       }
     //stored return => current_profile_pending_request

           $this->current_profile_pending_requests_id = array_unique($this->current_profile_pending_requests_id);
         

           return $this->current_profile_pending_requests_id;

    }

     public function get_apporavl_friend_request($sender_profile_id) {

      $pending_profile = $this->friends->get_many_by(array('friend_status'=>'REQUEST','sender_profile_id'=>$sender_profile_id));
  

       foreach ($pending_profile as $key => $friend_profile) {
          $this->my_requests_to_approve_id[$key] = $friend_profile->reciever_profile_id;
       }
     //stored return => current_profile_pending_request

           $this->my_requests_to_approve_id = array_unique($this->my_requests_to_approve_id);
          // var_dump($this->my_requests_to_approve_id);

           return $this->my_requests_to_approve_id;

    }

    public function get_current_profile_friends_profile_id($profile_id) {

        $friend_list = $this->friends->get_my_friends($profile_id);
        $freinds_id = array();
        
        foreach ($friend_list as $key => $value) {
         $freinds_id[$key]=$value->profile_id;
        }

        return $freinds_id;
    }

    private function _notification_information_for_seller($profile_id) {

        /***notification-listing***/
        $notifications= $this->notifications->get_all_notifications($profile_id);
        $notification_view = 'notifications/notification_view';
        $this->data['notification_view'] =  $notification_view ;
        $this->data['notifications'] =  $notifications;
        $this->data['count_notifications'] =  count($notifications);
        $this->data['friend_notifications'] = $this->notifications->list_friend_request_notification($profile_id);
        $this->data['message_notifications'] = $this->notifications->list_message_notification($profile_id);
        $this->data['like_notifications']=$this->notifications->list_likes_notification($profile_id);
       // dump( $this->data['friend_notifications']);exit;

        return $this->data;

    }



    public function clean_up_may_day($profile_id) {
        //i will be back again :)

        $where_data = array('profile_id'=>$profile_id);
        $result_data = $this->db->get_where('temp_files', $where_data);

        if(count($result_data->result()) > 0){
            foreach ($result_data->result() as $data_images) {
                @unlink($data_images->path . $data_images->file_name);
                $this->db->from('temp_files')->where('id', $data_images->id)->delete();
            }
        }
    }




}
